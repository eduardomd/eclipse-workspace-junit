package com.altec.bsbr.app.hyb.dto;

public class CadastroOsSLinhaObjRsCanal {
	
	private String NR_SEQU_ORDE_SERV;
	private String NM_PARP;
	private String NR_SEQU_PARP_PROC;
	private String NR_CNTA;
	private String NR_CPF_CNPJ_TITL;
	private String DT_FRAU;
	private String NR_TELF_CLIE;
	private String strTelClieDDD;
	private String IN_BLOQ_TELF;
	private String DT_BLOQ_TELF;
	private String HR_BLOQ_TELF;
	private String DT_SOLI_ASSN_ELET;
	private String NR_TELF_SOLI_ASSN_ELET;
	private String strAssTelSolicDDD;
	private String HR_SOLI_ASSN_ELET;
	private String DT_CADR_ASSN_ELET;
	private String NR_TELF_CADR_ASSN_ELET;
	private String strAssTelCadDDD;
	private String HR_CADR_ASSN_ELET;
	private String TP_CNTA_BNCR;
	private String DT_ABER_CNTA_BNCR;
	private String NM_GERE_RESP_CNTA_BNCR;
	private String IN_CRTO_SEGR;
	private String DT_SOLI_CRTO_SEGR;
	private String DT_CANC_CRTO_SEGR;
	private String DT_ATIV_CRTO_SEGR;
	private String DT_PRMR_FRAU;
	private String NR_SEQU_CNTA_BNCR;
	private String NR_SEQU_CRTO;
	private String NR_SEQU_FRAU_EVEN;
	private String NR_SEQU_FRAU_CNAL;
	private String CD_CNAL;
	private String IN_ITSS_COME;
	private String CD_BANC;
	private String DT_EMIS;
	private String DT_ATIV;
	private String DT_CANC;
	private String NM_LOG_RECU_SOLI;
	private String NM_SITU_CNTA_BNCR;
	private String NM_ENDE_ENVI;
	private String NR_QNTD_EVEN;
	private String CD_CENT_CUST_ORIG;
	private String CD_CENT_CUST_DEST;
	private String CD_CENT_CUST_OPNT;
	private String CONTABILIZADO;
	

	public CadastroOsSLinhaObjRsCanal(String nR_SEQU_ORDE_SERV, String nM_PARP, String nR_SEQU_PARP_PROC,
			String nR_CNTA, String nR_CPF_CNPJ_TITL, String dT_FRAU, String nR_TELF_CLIE, String strTelClieDDD,
			String iN_BLOQ_TELF, String dT_BLOQ_TELF, String hR_BLOQ_TELF, String dT_SOLI_ASSN_ELET,
			String nR_TELF_SOLI_ASSN_ELET, String strAssTelSolicDDD, String hR_SOLI_ASSN_ELET, String dT_CADR_ASSN_ELET,
			String nR_TELF_CADR_ASSN_ELET, String strAssTelCadDDD, String hR_CADR_ASSN_ELET, String tP_CNTA_BNCR,
			String dT_ABER_CNTA_BNCR, String nM_GERE_RESP_CNTA_BNCR, String iN_CRTO_SEGR, String dT_SOLI_CRTO_SEGR,
			String dT_CANC_CRTO_SEGR, String dT_ATIV_CRTO_SEGR, String dT_PRMR_FRAU, String nR_SEQU_CNTA_BNCR,
			String nR_SEQU_CRTO, String nR_SEQU_FRAU_EVEN, String nR_SEQU_FRAU_CNAL, String cD_CNAL,
			String iN_ITSS_COME, String cD_BANC, String dT_EMIS, String dT_ATIV, String dT_CANC,
			String nM_LOG_RECU_SOLI, String nM_SITU_CNTA_BNCR, String nM_ENDE_ENVI, String nR_QNTD_EVEN,
			String cD_CENT_CUST_ORIG, String cD_CENT_CUST_DEST, String cD_CENT_CUST_OPNT, String cONTABILIZADO) {
		NR_SEQU_ORDE_SERV = nR_SEQU_ORDE_SERV;
		NM_PARP = nM_PARP;
		NR_SEQU_PARP_PROC = nR_SEQU_PARP_PROC;
		NR_CNTA = nR_CNTA;
		NR_CPF_CNPJ_TITL = nR_CPF_CNPJ_TITL;
		DT_FRAU = dT_FRAU;
		NR_TELF_CLIE = nR_TELF_CLIE;
		this.strTelClieDDD = strTelClieDDD;
		IN_BLOQ_TELF = iN_BLOQ_TELF;
		DT_BLOQ_TELF = dT_BLOQ_TELF;
		HR_BLOQ_TELF = hR_BLOQ_TELF;
		DT_SOLI_ASSN_ELET = dT_SOLI_ASSN_ELET;
		NR_TELF_SOLI_ASSN_ELET = nR_TELF_SOLI_ASSN_ELET;
		this.strAssTelSolicDDD = strAssTelSolicDDD;
		HR_SOLI_ASSN_ELET = hR_SOLI_ASSN_ELET;
		DT_CADR_ASSN_ELET = dT_CADR_ASSN_ELET;
		NR_TELF_CADR_ASSN_ELET = nR_TELF_CADR_ASSN_ELET;
		this.strAssTelCadDDD = strAssTelCadDDD;
		HR_CADR_ASSN_ELET = hR_CADR_ASSN_ELET;
		TP_CNTA_BNCR = tP_CNTA_BNCR;
		DT_ABER_CNTA_BNCR = dT_ABER_CNTA_BNCR;
		NM_GERE_RESP_CNTA_BNCR = nM_GERE_RESP_CNTA_BNCR;
		IN_CRTO_SEGR = iN_CRTO_SEGR;
		DT_SOLI_CRTO_SEGR = dT_SOLI_CRTO_SEGR;
		DT_CANC_CRTO_SEGR = dT_CANC_CRTO_SEGR;
		DT_ATIV_CRTO_SEGR = dT_ATIV_CRTO_SEGR;
		DT_PRMR_FRAU = dT_PRMR_FRAU;
		NR_SEQU_CNTA_BNCR = nR_SEQU_CNTA_BNCR;
		NR_SEQU_CRTO = nR_SEQU_CRTO;
		NR_SEQU_FRAU_EVEN = nR_SEQU_FRAU_EVEN;
		NR_SEQU_FRAU_CNAL = nR_SEQU_FRAU_CNAL;
		CD_CNAL = cD_CNAL;
		IN_ITSS_COME = iN_ITSS_COME;
		CD_BANC = cD_BANC;
		DT_EMIS = dT_EMIS;
		DT_ATIV = dT_ATIV;
		DT_CANC = dT_CANC;
		NM_LOG_RECU_SOLI = nM_LOG_RECU_SOLI;
		NM_SITU_CNTA_BNCR = nM_SITU_CNTA_BNCR;
		NM_ENDE_ENVI = nM_ENDE_ENVI;
		NR_QNTD_EVEN = nR_QNTD_EVEN;
		CD_CENT_CUST_ORIG = cD_CENT_CUST_ORIG;
		CD_CENT_CUST_DEST = cD_CENT_CUST_DEST;
		CD_CENT_CUST_OPNT = cD_CENT_CUST_OPNT;
		CONTABILIZADO = cONTABILIZADO;
	}


	public String getStrAssTelCadDDD() {
		return strAssTelCadDDD;
	}


	public void setStrAssTelCadDDD(String strAssTelCadDDD) {
		this.strAssTelCadDDD = strAssTelCadDDD;
	}


	public CadastroOsSLinhaObjRsCanal() {
	}


	public String getNR_SEQU_ORDE_SERV() {
		return NR_SEQU_ORDE_SERV;
	}


	public void setNR_SEQU_ORDE_SERV(String nR_SEQU_ORDE_SERV) {
		NR_SEQU_ORDE_SERV = nR_SEQU_ORDE_SERV;
	}


	public String getNR_SEQU_PARP_PROC() {
		return NR_SEQU_PARP_PROC;
	}


	public void setNR_SEQU_PARP_PROC(String nR_SEQU_PARP_PROC) {
		NR_SEQU_PARP_PROC = nR_SEQU_PARP_PROC;
	}


	public String getNR_CNTA() {
		return NR_CNTA;
	}


	public void setNR_CNTA(String nR_CNTA) {
		NR_CNTA = nR_CNTA;
	}


	public String getStrAssTelSolicDDD() {
		return strAssTelSolicDDD;
	}


	public void setStrAssTelSolicDDD(String strAssTelSolicDDD) {
		this.strAssTelSolicDDD = strAssTelSolicDDD;
	}


	public String getNR_CPF_CNPJ_TITL() {
		return NR_CPF_CNPJ_TITL;
	}


	public void setNR_CPF_CNPJ_TITL(String nR_CPF_CNPJ_TITL) {
		NR_CPF_CNPJ_TITL = nR_CPF_CNPJ_TITL;
	}


	public String getDT_FRAU() {
		return DT_FRAU;
	}


	public void setDT_FRAU(String dT_FRAU) {
		DT_FRAU = dT_FRAU;
	}

	public String getStrTelClieDDD() {
		return strTelClieDDD;
	}

	public void setStrTelClieDDD(String strTelClieDDD) {
		this.strTelClieDDD = strTelClieDDD;
	}

	public String getNR_TELF_CLIE() {
		return NR_TELF_CLIE;
	}


	public void setNR_TELF_CLIE(String nR_TELF_CLIE) {
		NR_TELF_CLIE = nR_TELF_CLIE;
	}


	public String getIN_BLOQ_TELF() {
		return IN_BLOQ_TELF;
	}


	public void setIN_BLOQ_TELF(String iN_BLOQ_TELF) {
		IN_BLOQ_TELF = iN_BLOQ_TELF;
	}


	public String getDT_BLOQ_TELF() {
		return DT_BLOQ_TELF;
	}


	public void setDT_BLOQ_TELF(String dT_BLOQ_TELF) {
		DT_BLOQ_TELF = dT_BLOQ_TELF;
	}


	public String getHR_BLOQ_TELF() {
		return HR_BLOQ_TELF;
	}


	public void setHR_BLOQ_TELF(String hR_BLOQ_TELF) {
		HR_BLOQ_TELF = hR_BLOQ_TELF;
	}


	public String getDT_SOLI_ASSN_ELET() {
		return DT_SOLI_ASSN_ELET;
	}


	public void setDT_SOLI_ASSN_ELET(String dT_SOLI_ASSN_ELET) {
		DT_SOLI_ASSN_ELET = dT_SOLI_ASSN_ELET;
	}


	public String getNR_TELF_SOLI_ASSN_ELET() {
		return NR_TELF_SOLI_ASSN_ELET;
	}


	public void setNR_TELF_SOLI_ASSN_ELET(String nR_TELF_SOLI_ASSN_ELET) {
		NR_TELF_SOLI_ASSN_ELET = nR_TELF_SOLI_ASSN_ELET;
	}


	public String getHR_SOLI_ASSN_ELET() {
		return HR_SOLI_ASSN_ELET;
	}


	public void setHR_SOLI_ASSN_ELET(String hR_SOLI_ASSN_ELET) {
		HR_SOLI_ASSN_ELET = hR_SOLI_ASSN_ELET;
	}


	public String getDT_CADR_ASSN_ELET() {
		return DT_CADR_ASSN_ELET;
	}


	public void setDT_CADR_ASSN_ELET(String dT_CADR_ASSN_ELET) {
		DT_CADR_ASSN_ELET = dT_CADR_ASSN_ELET;
	}


	public String getNR_TELF_CADR_ASSN_ELET() {
		return NR_TELF_CADR_ASSN_ELET;
	}


	public void setNR_TELF_CADR_ASSN_ELET(String nR_TELF_CADR_ASSN_ELET) {
		NR_TELF_CADR_ASSN_ELET = nR_TELF_CADR_ASSN_ELET;
	}


	public String getHR_CADR_ASSN_ELET() {
		return HR_CADR_ASSN_ELET;
	}


	public void setHR_CADR_ASSN_ELET(String hR_CADR_ASSN_ELET) {
		HR_CADR_ASSN_ELET = hR_CADR_ASSN_ELET;
	}


	public String getTP_CNTA_BNCR() {
		return TP_CNTA_BNCR;
	}


	public void setTP_CNTA_BNCR(String tP_CNTA_BNCR) {
		TP_CNTA_BNCR = tP_CNTA_BNCR;
	}


	public String getDT_ABER_CNTA_BNCR() {
		return DT_ABER_CNTA_BNCR;
	}


	public void setDT_ABER_CNTA_BNCR(String dT_ABER_CNTA_BNCR) {
		DT_ABER_CNTA_BNCR = dT_ABER_CNTA_BNCR;
	}


	public String getNM_GERE_RESP_CNTA_BNCR() {
		return NM_GERE_RESP_CNTA_BNCR;
	}


	public void setNM_GERE_RESP_CNTA_BNCR(String nM_GERE_RESP_CNTA_BNCR) {
		NM_GERE_RESP_CNTA_BNCR = nM_GERE_RESP_CNTA_BNCR;
	}


	public String getIN_CRTO_SEGR() {
		return IN_CRTO_SEGR;
	}


	public void setIN_CRTO_SEGR(String iN_CRTO_SEGR) {
		IN_CRTO_SEGR = iN_CRTO_SEGR;
	}


	public String getDT_SOLI_CRTO_SEGR() {
		return DT_SOLI_CRTO_SEGR;
	}


	public void setDT_SOLI_CRTO_SEGR(String dT_SOLI_CRTO_SEGR) {
		DT_SOLI_CRTO_SEGR = dT_SOLI_CRTO_SEGR;
	}


	public String getDT_CANC_CRTO_SEGR() {
		return DT_CANC_CRTO_SEGR;
	}


	public void setDT_CANC_CRTO_SEGR(String dT_CANC_CRTO_SEGR) {
		DT_CANC_CRTO_SEGR = dT_CANC_CRTO_SEGR;
	}


	public String getDT_ATIV_CRTO_SEGR() {
		return DT_ATIV_CRTO_SEGR;
	}


	public void setDT_ATIV_CRTO_SEGR(String dT_ATIV_CRTO_SEGR) {
		DT_ATIV_CRTO_SEGR = dT_ATIV_CRTO_SEGR;
	}


	public String getDT_PRMR_FRAU() {
		return DT_PRMR_FRAU;
	}


	public void setDT_PRMR_FRAU(String dT_PRMR_FRAU) {
		DT_PRMR_FRAU = dT_PRMR_FRAU;
	}


	public String getNR_SEQU_CNTA_BNCR() {
		return NR_SEQU_CNTA_BNCR;
	}


	public void setNR_SEQU_CNTA_BNCR(String nR_SEQU_CNTA_BNCR) {
		NR_SEQU_CNTA_BNCR = nR_SEQU_CNTA_BNCR;
	}


	public String getNR_SEQU_CRTO() {
		return NR_SEQU_CRTO;
	}


	public void setNR_SEQU_CRTO(String nR_SEQU_CRTO) {
		NR_SEQU_CRTO = nR_SEQU_CRTO;
	}


	public String getNR_SEQU_FRAU_EVEN() {
		return NR_SEQU_FRAU_EVEN;
	}


	public void setNR_SEQU_FRAU_EVEN(String nR_SEQU_FRAU_EVEN) {
		NR_SEQU_FRAU_EVEN = nR_SEQU_FRAU_EVEN;
	}


	public String getNR_SEQU_FRAU_CNAL() {
		return NR_SEQU_FRAU_CNAL;
	}


	public void setNR_SEQU_FRAU_CNAL(String nR_SEQU_FRAU_CNAL) {
		NR_SEQU_FRAU_CNAL = nR_SEQU_FRAU_CNAL;
	}


	public String getCD_CNAL() {
		return CD_CNAL;
	}


	public void setCD_CNAL(String cD_CNAL) {
		CD_CNAL = cD_CNAL;
	}


	public String getIN_ITSS_COME() {
		return IN_ITSS_COME;
	}


	public void setIN_ITSS_COME(String iN_ITSS_COME) {
		IN_ITSS_COME = iN_ITSS_COME;
	}


	public String getCD_BANC() {
		return CD_BANC;
	}


	public void setCD_BANC(String cD_BANC) {
		CD_BANC = cD_BANC;
	}


	public String getDT_EMIS() {
		return DT_EMIS;
	}


	public void setDT_EMIS(String dT_EMIS) {
		DT_EMIS = dT_EMIS;
	}


	public String getDT_ATIV() {
		return DT_ATIV;
	}


	public void setDT_ATIV(String dT_ATIV) {
		DT_ATIV = dT_ATIV;
	}


	public String getDT_CANC() {
		return DT_CANC;
	}


	public void setDT_CANC(String dT_CANC) {
		DT_CANC = dT_CANC;
	}


	public String getNM_LOG_RECU_SOLI() {
		return NM_LOG_RECU_SOLI;
	}


	public void setNM_LOG_RECU_SOLI(String nM_LOG_RECU_SOLI) {
		NM_LOG_RECU_SOLI = nM_LOG_RECU_SOLI;
	}


	public String getNM_SITU_CNTA_BNCR() {
		return NM_SITU_CNTA_BNCR;
	}


	public void setNM_SITU_CNTA_BNCR(String nM_SITU_CNTA_BNCR) {
		NM_SITU_CNTA_BNCR = nM_SITU_CNTA_BNCR;
	}


	public String getNM_ENDE_ENVI() {
		return NM_ENDE_ENVI;
	}


	public void setNM_ENDE_ENVI(String nM_ENDE_ENVI) {
		NM_ENDE_ENVI = nM_ENDE_ENVI;
	}


	public String getNR_QNTD_EVEN() {
		return NR_QNTD_EVEN;
	}


	public void setNR_QNTD_EVEN(String nR_QNTD_EVEN) {
		NR_QNTD_EVEN = nR_QNTD_EVEN;
	}


	public String getCD_CENT_CUST_ORIG() {
		return CD_CENT_CUST_ORIG;
	}


	public void setCD_CENT_CUST_ORIG(String cD_CENT_CUST_ORIG) {
		CD_CENT_CUST_ORIG = cD_CENT_CUST_ORIG;
	}


	public String getCD_CENT_CUST_DEST() {
		return CD_CENT_CUST_DEST;
	}


	public void setCD_CENT_CUST_DEST(String cD_CENT_CUST_DEST) {
		CD_CENT_CUST_DEST = cD_CENT_CUST_DEST;
	}


	public String getCD_CENT_CUST_OPNT() {
		return CD_CENT_CUST_OPNT;
	}


	public void setCD_CENT_CUST_OPNT(String cD_CENT_CUST_OPNT) {
		CD_CENT_CUST_OPNT = cD_CENT_CUST_OPNT;
	}


	public String getCONTABILIZADO() {
		return CONTABILIZADO;
	}


	public void setCONTABILIZADO(String cONTABILIZADO) {
		CONTABILIZADO = cONTABILIZADO;
	}


	public String getNM_PARP() {
		return NM_PARP;
	}


	public void setNM_PARP(String nM_PARP) {
		NM_PARP = nM_PARP;
	}
	
}
