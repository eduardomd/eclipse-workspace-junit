package com.altec.bsbr.app.hyb.dto;

public class RestDtAberturaModel {
	
	private String sq_restricao;
	private String instituicao;
	private String especificacao;
	private String valor;
	private String data;
	
	public RestDtAberturaModel() {
		
	}
	
	public RestDtAberturaModel(String sq_restricao, String instituicao, String especificacao, String valor,
			String data) {
		super();
		this.sq_restricao = sq_restricao;
		this.instituicao = instituicao;
		this.especificacao = especificacao;
		this.valor = valor;
		this.data = data;
	}
	public String getSq_restricao() {
		return sq_restricao;
	}
	public void setSq_restricao(String sq_restricao) {
		this.sq_restricao = sq_restricao;
	}
	public String getInstituicao() {
		return instituicao;
	}
	public void setInstituicao(String instituicao) {
		this.instituicao = instituicao;
	}
	public String getEspecificacao() {
		return especificacao;
	}
	public void setEspecificacao(String especificacao) {
		this.especificacao = especificacao;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
}
