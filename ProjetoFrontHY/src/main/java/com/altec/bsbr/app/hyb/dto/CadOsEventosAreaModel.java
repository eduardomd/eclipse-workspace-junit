package com.altec.bsbr.app.hyb.dto;

public class CadOsEventosAreaModel {

	private String codigoEvento;
	private String nomeEvento;

	public String getCodigoEvento() {
		return codigoEvento;
	}
	public void setCodigoEvento(String codigoEvento) {
		this.codigoEvento = codigoEvento;
	}

	public String getNomeEvento() {
		return nomeEvento;
	}
	public void setNomeEvento(String nomeEvento) {
		this.nomeEvento = nomeEvento;
	}

}
