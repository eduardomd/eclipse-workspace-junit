package com.altec.bsbr.app.hyb.dto;

public class RelCapaOSModel {

	private String nrMatrPrsv;
	private String nmRecuOcorEspc;
	private String tpDesi;
	private String nmDesignacao;
	private String cdCarg;
	
	public RelCapaOSModel() {
		
	}
	
	public RelCapaOSModel(String nrMatrPrsv, String nmRecuOcorEspc, String tpDesi, String nmDesignacao, String cdCarg) {
		this.nrMatrPrsv = nrMatrPrsv;
		this.nmRecuOcorEspc = nmRecuOcorEspc;
		this.tpDesi = tpDesi;
		this.nmDesignacao = nmDesignacao;
		this.cdCarg = cdCarg;
	}

	public String getNrMatrPrsv() {
		return nrMatrPrsv;
	}

	public void setNrMatrPrsv(String nrMatrPrsv) {
		this.nrMatrPrsv = nrMatrPrsv;
	}

	public String getNmRecuOcorEspc() {
		return nmRecuOcorEspc;
	}

	public void setNmRecuOcorEspc(String nmRecuOcorEspc) {
		this.nmRecuOcorEspc = nmRecuOcorEspc;
	}

	public String getTpDesi() {
		return tpDesi;
	}

	public void setTpDesi(String tpDesi) {
		this.tpDesi = tpDesi;
	}

	public String getNmDesignacao() {
		return nmDesignacao;
	}

	public void setNmDesignacao(String nmDesignacao) {
		this.nmDesignacao = nmDesignacao;
	}

	public String getCdCarg() {
		return cdCarg;
	}

	public void setCdCarg(String cdCarg) {
		this.cdCarg = cdCarg;
	}
	
	
}
