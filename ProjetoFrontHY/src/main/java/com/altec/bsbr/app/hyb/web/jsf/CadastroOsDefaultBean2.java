package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

import javax.faces.context.FacesContext;

import com.altec.bsbr.app.hyb.dto.CadastroOsDefaultModel;
import com.altec.bsbr.app.hyb.web.util.CadOsEventosUtil;



import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

//import com.altec.bsbr.app.hyb.webclient.XHYAdmGer.XHYAdmGerEndPoint;
//import com.altec.bsbr.app.hyb.webclient.XHYAdmGer.WebServiceException;



@Component("CadastroOsDefaultBean")
@Scope("session")
public class CadastroOsDefaultBean2 extends BasicBBean {

/*	#include file="../Inc/HY_Usuario.inc"
	#include file="../Inc/Hy_Util.inc"*/
	
	private static final long serialVersionUID = 1L;
	private static final String empty = new String();
	private String qsCanal;
	private String qsEvento;
	private String nrSeqDetalhe;
	private ArrayList<CadastroOsDefaultModel> objRsEventoValida;
	private ArrayList<CadastroOsDefaultModel> objRsEvento;
	
	CadastroOsDefaultModel cadastroOsDefault = new CadastroOsDefaultModel();
	
	CadOsEventosUtil objEvento = new CadOsEventosUtil();
	
	//@Autowired
    //private XHYAdmGerEndPoint admgerEndPoint;

	
	public CadastroOsDefaultBean2() throws IOException{
		
		// block to get params from url
		FacesContext fc = FacesContext.getCurrentInstance();
		@SuppressWarnings("unchecked")
		Map<String, String> params = fc.getExternalContext().getRequestParameterMap();

		qsCanal = params.get("qsCanal");
		qsEvento = params.get("qsEvento");
		nrSeqDetalhe = params.get("NrSeqDetalhe"); //número sequencial da Fraude Canal
		
		cadastroOsDefault.setCdCanal(qsCanal);
		cadastroOsDefault.setStrEvento(qsEvento);
		cadastroOsDefault.setStrNrSeqId(nrSeqDetalhe);
		
		objRsEventoValida = objEvento.ConsultarFraudeCanalDetalhe(cadastroOsDefault.getStrNrSeqId());
		
		//String nada = TestarWebServices();
		
		if(!objRsEventoValida.get(0).getStrErro().equals(empty)) {
			objEvento = new CadOsEventosUtil();
			objRsEventoValida = null;
			
			System.out.println("Exibe mensagem de erro");
		}
		
		if(objRsEventoValida.isEmpty() == true) {
			System.out.println("empty");
			objEvento = new CadOsEventosUtil();
			objRsEventoValida = null;
		}else {
			System.out.println("mostrar dados");
			objRsEvento = objRsEventoValida;
		}
	
		
	}
	
	public String getQsCanal() {
		return cadastroOsDefault.getCdCanal();
	}

	public String getQsEvento() {
		return cadastroOsDefault.getStrEvento();
	}

	public ArrayList<CadastroOsDefaultModel> getObjRsEvento() {
		return objRsEvento;
	}

	public void setObjRsEvento(ArrayList<CadastroOsDefaultModel> objRsEvento) {
		this.objRsEvento = objRsEvento;
	}
	

}
