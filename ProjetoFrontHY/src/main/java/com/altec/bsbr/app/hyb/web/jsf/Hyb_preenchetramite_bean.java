package com.altec.bsbr.app.hyb.web.jsf;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.altec.bsbr.app.hyb.dto.Hyb_preenchetramite_model;

@ManagedBean(name="hyb_preenchetramite_bean")
@ViewScoped
public class Hyb_preenchetramite_bean {
	
	private Object wid_OS;
	private Hyb_preenchetramite_model vrstTramite;
	private String strErro;
	
	public Hyb_preenchetramite_bean() {
		wid_OS = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id_OS");
	}
	
	public Hyb_preenchetramite_bean(Object wid_OS, Hyb_preenchetramite_model vrstTramite, String strErro) {
		this.wid_OS = wid_OS;
		this.vrstTramite = vrstTramite;
		this.strErro = strErro;
	}
	
	@PostConstruct
	public void init() {
		String wid_OSstr = this.getWid_OS().toString().trim();
		
		if (!wid_OSstr.equals("0")) {
				// MOCK
					this.vrstTramite = new Hyb_preenchetramite_model();
					this.vrstTramite.setMaior("Maior test");
				// END MOCK
		}
	}

	public Hyb_preenchetramite_model getVrstTramite() {
		return vrstTramite;
	}

	public void setVrstTramite(Hyb_preenchetramite_model vrstTramite) {
		this.vrstTramite = vrstTramite;
	}

	public Object getWid_OS() {
		return wid_OS;
	}

	public void setWid_OS(Object wid_OS) {
		this.wid_OS = wid_OS;
	}
	
	public String getStrErro() {
		return strErro;
	}

	public void setStrErro(String strErro) {
		this.strErro = strErro;
	}

	public void verificaErro() {
		
		if (!this.strErro.equals("")) {
			// @TODO - Ajustar o redirect (bate na exception quando chega no ec.redirect comentado abaixo)
	/*			try {
				ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
				ec.redirect("hy_erro.xhtml?strErro=" + this.strMsgErro);
			} catch(IOException e) {
				e.printStackTrace();
			} */
		}
	}
	
}
