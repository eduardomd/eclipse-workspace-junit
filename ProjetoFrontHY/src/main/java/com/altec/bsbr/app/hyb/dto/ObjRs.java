package com.altec.bsbr.app.hyb.dto;

public class ObjRs {
	
	private String NOME;
	private String HORA;
	private String REMUNERACAO;
	private String GASTO_AD;
	private String TIPO_RECURSO;
	private String DATA_ATUAL;
	private String HORA_ATUAL;
	private String DATA;
	private String CD_SITUACAO;
	private String NM_SITUACAO;
	private String NOTIFICACAO;
	private String Cd_Banc;	
	private String CD_ENTI_CNTR;
	private String CD_PROD;
	private String NM_PROD_CORP;
	private String NR_CNTA;
	private String NR_CNTR;
	private String Nr_Crto;
	private String NR_SEQU_CNTA_BNCR;
	private String NR_SEQU_CNTR;
	private String NR_SEQU_CRTO;
	private String NR_SEQU_FRAU_CNAL;
	private String NR_SEQU_ORDE_SERV;
	private String NR_SEQU_PARP_PROC;
	private String QTD_LANC;
	private String TP_OPER_PARP;
	private String SQ_TIPO_MANI;
	private String DE_TIPO_MANI;
	
	public ObjRs(){ }
	
	public ObjRs(String sQ_TIPO_MANI, String dE_TIPO_MANI) {
		super();
		SQ_TIPO_MANI = sQ_TIPO_MANI;
		DE_TIPO_MANI = dE_TIPO_MANI;
	}

	public ObjRs(String dATA_ATUAL, String hORA_ATUAL, String dATA, String cD_SITUACAO, String nM_SITUACAO,
			String nOTIFICACAO) {
		super();
		DATA_ATUAL = dATA_ATUAL;
		HORA_ATUAL = hORA_ATUAL;
		DATA = dATA;
		CD_SITUACAO = cD_SITUACAO;
		NM_SITUACAO = nM_SITUACAO;
		NOTIFICACAO = nOTIFICACAO;
	}
	
	public String getNOME() {
		return NOME;
	}

	public void setNOME(String nOME) {
		NOME = nOME;
	}
	
	public String getHORA() {
		return HORA;
	}

	public void setHORA(String hORA) {
		HORA = hORA;
	}

	public String getREMUNERACAO() {
		return REMUNERACAO;
	}

	public void setREMUNERACAO(String rEMUNERACAO) {
		REMUNERACAO = rEMUNERACAO;
	}

	public String getGASTO_AD() {
		return GASTO_AD;
	}

	public void setGASTO_AD(String gASTO_AD) {
		GASTO_AD = gASTO_AD;
	}

	public String getTIPO_RECURSO() {
		return TIPO_RECURSO;
	}

	public void setTIPO_RECURSO(String tIPO_RECURSO) {
		TIPO_RECURSO = tIPO_RECURSO;
	}

	public String getDATA_ATUAL() {
		return DATA_ATUAL;
	}

	public void setDATA_ATUAL(String dATA_ATUAL) {
		DATA_ATUAL = dATA_ATUAL;
	}

	public String getHORA_ATUAL() {
		return HORA_ATUAL;
	}

	public void setHORA_ATUAL(String hORA_ATUAL) {
		HORA_ATUAL = hORA_ATUAL;
	}

	public String getDATA() {
		return DATA;
	}

	public void setDATA(String dATA) {
		DATA = dATA;
	}

	public String getCD_SITUACAO() {
		return CD_SITUACAO;
	}

	public void setCD_SITUACAO(String cD_SITUACAO) {
		CD_SITUACAO = cD_SITUACAO;
	}

	public String getNM_SITUACAO() {
		return NM_SITUACAO;
	}

	public void setNM_SITUACAO(String nM_SITUACAO) {
		NM_SITUACAO = nM_SITUACAO;
	}

	public String getNOTIFICACAO() {
		return NOTIFICACAO;
	}

	public void setNOTIFICACAO(String nOTIFICACAO) {
		NOTIFICACAO = nOTIFICACAO;
	}

	public String getCd_Banc() {
		return Cd_Banc;
	}
	public void setCd_Banc(String cd_Banc) {
		Cd_Banc = cd_Banc;
	}
	public String getCD_ENTI_CNTR() {
		return CD_ENTI_CNTR;
	}
	public void setCD_ENTI_CNTR(String cD_ENTI_CNTR) {
		CD_ENTI_CNTR = cD_ENTI_CNTR;
	}
	public String getCD_PROD() {
		return CD_PROD;
	}
	public void setCD_PROD(String cD_PROD) {
		CD_PROD = cD_PROD;
	}
	public String getNM_PROD_CORP() {
		return NM_PROD_CORP;
	}
	public void setNM_PROD_CORP(String nM_PROD_CORP) {
		NM_PROD_CORP = nM_PROD_CORP;
	}
	public String getNR_CNTA() {
		return NR_CNTA;
	}
	public void setNR_CNTA(String nR_CNTA) {
		NR_CNTA = nR_CNTA;
	}
	public String getNR_CNTR() {
		return NR_CNTR;
	}
	public void setNR_CNTR(String nR_CNTR) {
		NR_CNTR = nR_CNTR;
	}
	public String getNr_Crto() {
		return Nr_Crto;
	}
	public void setNr_Crto(String nr_Crto) {
		Nr_Crto = nr_Crto;
	}
	public String getNR_SEQU_CNTA_BNCR() {
		return NR_SEQU_CNTA_BNCR;
	}
	public void setNR_SEQU_CNTA_BNCR(String nR_SEQU_CNTA_BNCR) {
		NR_SEQU_CNTA_BNCR = nR_SEQU_CNTA_BNCR;
	}
	public String getNR_SEQU_CNTR() {
		return NR_SEQU_CNTR;
	}
	public void setNR_SEQU_CNTR(String nR_SEQU_CNTR) {
		NR_SEQU_CNTR = nR_SEQU_CNTR;
	}
	public String getNR_SEQU_CRTO() {
		return NR_SEQU_CRTO;
	}
	public void setNR_SEQU_CRTO(String nR_SEQU_CRTO) {
		NR_SEQU_CRTO = nR_SEQU_CRTO;
	}
	public String getNR_SEQU_FRAU_CNAL() {
		return NR_SEQU_FRAU_CNAL;
	}
	public void setNR_SEQU_FRAU_CNAL(String nR_SEQU_FRAU_CNAL) {
		NR_SEQU_FRAU_CNAL = nR_SEQU_FRAU_CNAL;
	}
	public String getNR_SEQU_ORDE_SERV() {
		return NR_SEQU_ORDE_SERV;
	}
	public void setNR_SEQU_ORDE_SERV(String nR_SEQU_ORDE_SERV) {
		NR_SEQU_ORDE_SERV = nR_SEQU_ORDE_SERV;
	}
	public String getNR_SEQU_PARP_PROC() {
		return NR_SEQU_PARP_PROC;
	}
	public void setNR_SEQU_PARP_PROC(String nR_SEQU_PARP_PROC) {
		NR_SEQU_PARP_PROC = nR_SEQU_PARP_PROC;
	}
	public String getQTD_LANC() {
		return QTD_LANC;
	}
	public void setQTD_LANC(String qTD_LANC) {
		QTD_LANC = qTD_LANC;
	}
	public String getTP_OPER_PARP() {
		return TP_OPER_PARP;
	}
	public void setTP_OPER_PARP(String tP_OPER_PARP) {
		TP_OPER_PARP = tP_OPER_PARP;
	}

	public String getSQ_TIPO_MANI() {
		return SQ_TIPO_MANI;
	}

	public void setSQ_TIPO_MANI(String sQ_TIPO_MANI) {
		SQ_TIPO_MANI = sQ_TIPO_MANI;
	}

	public String getDE_TIPO_MANI() {
		return DE_TIPO_MANI;
	}

	public void setDE_TIPO_MANI(String dE_TIPO_MANI) {
		DE_TIPO_MANI = dE_TIPO_MANI;
	}    
	
}
