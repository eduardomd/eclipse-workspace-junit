package com.altec.bsbr.app.hyb.web.util;

public class OS {
	
	private String ORDEM;
	private String AREA;
	private String STATUS;
	
	public OS() {
		
	}

	public String getORDEM() {
		return ORDEM;
	}

	public void setORDEM(String oRDEM) {
		ORDEM = oRDEM;
	}

	public String getAREA() {
		return AREA;
	}

	public void setAREA(String aREA) {
		AREA = aREA;
	}

	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
}
