package com.altec.bsbr.app.hyb.dto;

public class EncerrarOsMotiPenaModel {	
	private String NOME;
	private int CODIGO;
	
	public EncerrarOsMotiPenaModel() {}

	public String getNOME() {
		return NOME;
	}

	public void setNOME(String nOME) {
		NOME = nOME;
	}

	public int getCODIGO() {
		return CODIGO;
	}

	public void setCODIGO(int cODIGO) {
		CODIGO = cODIGO;
	}
	
}
