package com.altec.bsbr.app.hyb.dto;

public class Hyb_excel_pesquisa_resultado_model {
	
	private String CODIGO;
	private String CD_SITUACAO;
	private String NM_SITUACAO;
	private String ABERTURA;
	private String AREA;
	private String NOME_AN_DESIG;
	private String VALOR_ENVOLVIDO;
	private String PREJUIZO;
	private String VALOR_RECUP;
	private String CUSTO_OS;
	private String NM_PV;
	private String NM_REGI;
	private String NM_REDE;
	private String EVENTO;
	private String CANAL;
	private String OPERACAO;
	
	public Hyb_excel_pesquisa_resultado_model() {
		
	}
	
	public Hyb_excel_pesquisa_resultado_model(String cODIGO, String cD_SITUACAO, String nM_SITUACAO, String aBERTURA, String aREA,
			String nOME_AN_DESIG, String vALOR_ENVOLVIDO, String pREJUIZO, String vALOR_RECUP, String cUSTO_OS,
			String nM_PV, String nM_REGI, String nM_REDE, String eVENTO, String cANAL, String oPERACAO) {
		CODIGO = cODIGO;
		CD_SITUACAO = cD_SITUACAO;
		NM_SITUACAO = nM_SITUACAO;
		ABERTURA = aBERTURA;
		AREA = aREA;
		NOME_AN_DESIG = nOME_AN_DESIG;
		VALOR_ENVOLVIDO = vALOR_ENVOLVIDO;
		PREJUIZO = pREJUIZO;
		VALOR_RECUP = vALOR_RECUP;
		CUSTO_OS = cUSTO_OS;
		NM_PV = nM_PV;
		NM_REGI = nM_REGI;
		NM_REDE = nM_REDE;
		EVENTO = eVENTO;
		CANAL = cANAL;
		OPERACAO = oPERACAO;
	}
	public String getCODIGO() {
		return CODIGO;
	}
	public void setCODIGO(String cODIGO) {
		CODIGO = cODIGO;
	}
	public String getCD_SITUACAO() {
		return CD_SITUACAO;
	}
	public void setCD_SITUACAO(String cD_SITUACAO) {
		CD_SITUACAO = cD_SITUACAO;
	}
	public String getNM_SITUACAO() {
		return NM_SITUACAO;
	}
	public void setNM_SITUACAO(String nM_SITUACAO) {
		NM_SITUACAO = nM_SITUACAO;
	}
	public String getABERTURA() {
		return ABERTURA;
	}
	public void setABERTURA(String aBERTURA) {
		ABERTURA = aBERTURA;
	}
	public String getAREA() {
		return AREA;
	}
	public void setAREA(String aREA) {
		AREA = aREA;
	}
	public String getNOME_AN_DESIG() {
		return NOME_AN_DESIG;
	}
	public void setNOME_AN_DESIG(String nOME_AN_DESIG) {
		NOME_AN_DESIG = nOME_AN_DESIG;
	}
	public String getVALOR_ENVOLVIDO() {
		return VALOR_ENVOLVIDO;
	}
	public void setVALOR_ENVOLVIDO(String vALOR_ENVOLVIDO) {
		VALOR_ENVOLVIDO = vALOR_ENVOLVIDO;
	}
	public String getPREJUIZO() {
		return PREJUIZO;
	}
	public void setPREJUIZO(String pREJUIZO) {
		PREJUIZO = pREJUIZO;
	}
	public String getVALOR_RECUP() {
		return VALOR_RECUP;
	}
	public void setVALOR_RECUP(String vALOR_RECUP) {
		VALOR_RECUP = vALOR_RECUP;
	}
	public String getCUSTO_OS() {
		return CUSTO_OS;
	}
	public void setCUSTO_OS(String cUSTO_OS) {
		CUSTO_OS = cUSTO_OS;
	}
	public String getNM_PV() {
		return NM_PV;
	}
	public void setNM_PV(String nM_PV) {
		NM_PV = nM_PV;
	}
	public String getNM_REGI() {
		return NM_REGI;
	}
	public void setNM_REGI(String nM_REGI) {
		NM_REGI = nM_REGI;
	}
	public String getNM_REDE() {
		return NM_REDE;
	}
	public void setNM_REDE(String nM_REDE) {
		NM_REDE = nM_REDE;
	}
	public String getEVENTO() {
		return EVENTO;
	}
	public void setEVENTO(String eVENTO) {
		EVENTO = eVENTO;
	}
	public String getCANAL() {
		return CANAL;
	}
	public void setCANAL(String cANAL) {
		CANAL = cANAL;
	}
	public String getOPERACAO() {
		return OPERACAO;
	}
	public void setOPERACAO(String oPERACAO) {
		OPERACAO = oPERACAO;
	}
	
	
	

}
