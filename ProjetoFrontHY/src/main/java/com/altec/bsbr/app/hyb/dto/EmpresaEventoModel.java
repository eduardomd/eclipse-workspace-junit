package com.altec.bsbr.app.hyb.dto;

public class EmpresaEventoModel {

	private String nomeEmpresa;
	private String codigoEmpresa;
	private String strEmpr;


	public EmpresaEventoModel() {}
	
	public EmpresaEventoModel(String codigoEmpresa, String nomeEmpresa) {
		this.nomeEmpresa = nomeEmpresa;
		this.codigoEmpresa = codigoEmpresa;
	}

	public String getNomeEmpresa() {
		return nomeEmpresa;
	}
	public void setNomeEmpresa(String nomeEmpresa) {
		this.nomeEmpresa = nomeEmpresa;
	}

	public String getCodigoEmpresa() {
		return codigoEmpresa;
	}
	public void setCodigoEmpresa(String codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	public String getStrEmpr() {
		return strEmpr;
	}
	public void setStrEmpr(String strEmpr) {
		this.strEmpr = strEmpr;
	}

}
