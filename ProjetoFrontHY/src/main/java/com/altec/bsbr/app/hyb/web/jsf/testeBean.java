package com.altec.bsbr.app.hyb.web.jsf;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.fw.web.jsf.BasicBBean;

@Component("testeB")
@Scope("session")
public class testeBean extends BasicBBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private UploadedFile file;
	 
    public UploadedFile getFile() {
        return file;
    }
    
 
    public void setFile(UploadedFile file) {
        this.file = file;
    }
     
    public void upload(FileUploadEvent event) {
        System.out.println("Glory cheguei : " + event.getFile().getContents());
    	/*if(file != null) {
            System.out.println(file.getFileName() + " is uploaded!!!");
           // FacesContext.getCurrentInstance().addMessage(null, message);
        }*/
        String t =  uploadArquivo(event.getFile().getFileName(), event.getFile().getContents(), "P", "x217347", "C:\\Users\\x217347\\Desktop\\");
        System.out.println(t);
        
    }
    
    @PostConstruct
	public void init() {
    	System.out.println("Cheguei!!!");
    }
    
    public testeBean()
    {
    	
    }
    
    public void handleFileUpload(FileUploadEvent event) {
        FacesMessage msg = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public String uploadArquivo(String strFileName, byte[] varFileData, String strTpAnexo, String strMatrUsua,
			String strCaminhoServidor) {
		
		 String newUrl="";
		 try {
			
		 String strPath = strCaminhoServidor + strMatrUsua + "_temp" + "\\";
		
		 File objFS = new File(strPath);
		 if(!objFS.exists())
		 {
			 objFS.mkdir();
		 }
		
		 File newFile = new File(strPath, strFileName);
	
			FileOutputStream out = new FileOutputStream(newFile);
			out.write(varFileData);
			out.close();
			
			newUrl = strPath + strFileName;
			newUrl.replace("\\", "/");
			
		
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			return newUrl;
	}

	public void excluiAnexoServ(String strCaminhoServidor, String strTpExclusao) {
		
		File objFS = new File(strCaminhoServidor);
		
		if(strTpExclusao == "P")
		{
			if(objFS.exists())
			{
				objFS.delete();
			}
		}
		else
		{
			objFS.delete();
		}
	}
	
	public void deletarFile()
	{
		excluiAnexoServ("C:\\Users\\x217347\\Desktop\\x217347_temp\\senha_jirah.txt", "F");
	}

}
