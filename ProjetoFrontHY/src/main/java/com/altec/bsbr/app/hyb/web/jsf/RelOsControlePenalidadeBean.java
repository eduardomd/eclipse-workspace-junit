package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.RelControlePenalidade;
import com.altec.bsbr.app.jab.hyb.webclient.XHYRelatorios.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYRelatorios.XHYRelatoriosEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;

@Component("relOsControlePenalidadeBean")
@Scope("session")
public class RelOsControlePenalidadeBean extends BasicBBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String pDtIni;
	private String pDtFim;
	private List<RelControlePenalidade> objRsRelat;

	ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
	JSONArray pcursorObjRs = new JSONArray();

	@Autowired
	private XHYRelatoriosEndPoint objRelat;

	@PostConstruct
	public void init() throws IOException {
		FacesContext fc = FacesContext.getCurrentInstance();
		Map<String, String> params = fc.getExternalContext().getRequestParameterMap();

		this.pDtIni = params.get("pDtIni");
		this.pDtFim = params.get("pDtFim");

		try {
			String objRs = objRelat.fnRelPenalidade(this.pDtIni, this.pDtFim);
			JSONObject objResTemp = new JSONObject(objRs);
			pcursorObjRs = objResTemp.getJSONArray("PCURSOR");
			objRsRelat = new ArrayList<RelControlePenalidade>();

			if (pcursorObjRs.length() > 0) {

				for (int i = 0; i < pcursorObjRs.length(); i++) {
					JSONObject temp = pcursorObjRs.getJSONObject(i);

					objRsRelat.add(
							new RelControlePenalidade(
									temp.isNull("OS") ? "" : temp.get("OS").toString(),
									temp.isNull("EVENTO") ? "" : temp.get("EVENTO").toString(),
									temp.isNull("PARTICIPACAO") ? "" : temp.get("PARTICIPACAO").toString(),
									temp.isNull("TIPO_COLABORADOR") ? "" : temp.get("TIPO_COLABORADOR").toString(),
									temp.isNull("CARGO") ? "" : temp.get("CARGO").toString(),
									temp.isNull("MATRICULA") ? "" : temp.get("MATRICULA").toString(),
									temp.isNull("PENALIDADE") ? "" : temp.get("PENALIDADE").toString(),
									temp.isNull("MOTIVO") ? "" : temp.get("MOTIVO").toString(),
									temp.isNull("NUM_PV") ? "" : temp.get("NUM_PV").toString(),
									temp.isNull("NM_PV") ? "" : temp.get("NM_PV").toString(),
									temp.isNull("NM_REGI") ? "" : temp.get("NM_REGI").toString(),
									temp.isNull("NM_REDE") ? "" : temp.get("NM_REDE").toString(),
									temp.isNull("DT_ENVIO_RH") ? "" : temp.get("DT_ENVIO_RH").toString(),
									temp.isNull("DT_RETN_JURI") ? "" : temp.get("DT_RETN_JURI").toString()));
				}
			}
			/*if (objRsRelat.isEmpty()) {
				RelControlePenalidade linha = new RelControlePenalidade();
				linha.setCargo("cargo");
				linha.setDtEnvioRH("dtEnvioRH");
				linha.setDtRetnJuri("dtRetnJuri");
				linha.setEvento("evento");
				linha.setMatricula("matricula");
				linha.setMotivo("motivo");
				linha.setNmPv("nmPv");
				linha.setOs("os");
				linha.setParticipacao("participacao");
				linha.setPenalidade("penalidade");
				linha.setTipoColaborador("tipoColaborador");
				objRsRelat.add(linha);
				objRsRelat.add(linha);
				objRsRelat.add(linha);
				objRsRelat.add(linha);
				objRsRelat.add(linha);
				objRsRelat.add(linha);
				objRsRelat.add(linha);
			}*/
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		cleanSession();
	}

	public void cleanSession() {
		FacesContext context = FacesContext.getCurrentInstance();
		if (context.getExternalContext().getSessionMap().get("relatorioosBean") != null) {
			context.getExternalContext().getSessionMap().remove("relatorioosBean");
		}
	}

	public String getpDtIni() {
		return pDtIni;
	}

	public void setpDtIni(String pDtIni) {
		this.pDtIni = pDtIni;
	}

	public String getpDtFim() {
		return pDtFim;
	}

	public void setpDtFim(String pDtFim) {
		this.pDtFim = pDtFim;
	}

	public List<RelControlePenalidade> getObjRsRelat() {
		return objRsRelat;
	}

	public void setObjRsRelat(List<RelControlePenalidade> objRsRelat) {
		this.objRsRelat = objRsRelat;
	}

	public String now() {
		DateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		Date date = new Date();
		return dateformat.format(date);
	}

	public void postProcessExcel(Object doc) {
		try {
			XSSFWorkbook wb = (XSSFWorkbook) doc;
			XSSFSheet sheet = wb.getSheetAt(0);
			sheet.setDisplayGridlines(false);

			formatarCabecalho(wb, sheet);
			formatarCabecalhoTabela(wb, sheet);
			formatarRegistrosTabela(wb, sheet);
			criarRodape(wb, sheet);
			
			sheet.shiftRows(3, sheet.getLastRowNum() + 1, 2);
			sheet.shiftRows(6, sheet.getLastRowNum() + 1, 1);
		} catch (Exception e) {
			System.out.println(e);
		}

	}

	private void formatarCabecalho(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyle1 = wb.createCellStyle();
		headerStyle1.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle1.setWrapText(true);
		headerStyle1.setBorderBottom(CellStyle.BORDER_NONE);
		headerStyle1.setBorderLeft(CellStyle.BORDER_NONE);
		headerStyle1.setBorderRight(CellStyle.BORDER_NONE);
		headerStyle1.setBorderTop(CellStyle.BORDER_NONE);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 14);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		headerStyle1.setFont(font);

		XSSFRow primeiraLinha = sheet.getRow(1);
		if (primeiraLinha != null) {
			XSSFCell primeiraLinhaCell = primeiraLinha.getCell(0);
			primeiraLinhaCell.setCellStyle(headerStyle1);
			primeiraLinha.setHeightInPoints((short) 19);
		}
		XSSFRow segundaLinha = sheet.getRow(2);
		if (segundaLinha != null) {
			XSSFCell segundaLinhaCell = segundaLinha.getCell(0);
			segundaLinhaCell.setCellStyle(headerStyle1);
			segundaLinha.setHeightInPoints((short) 19);
		}
	}

	private void formatarCabecalhoTabela(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setWrapText(true);
		headerStyle.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyle.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyle.setBorderRight(CellStyle.BORDER_THIN);
		headerStyle.setBorderTop(CellStyle.BORDER_THIN);
		headerStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.WHITE.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(font);

		XSSFRow linhaCabecalhoTabela = sheet.getRow(4);
		if (linhaCabecalhoTabela != null) {
			for (int i = 0; i <= linhaCabecalhoTabela.getLastCellNum(); i++) {
				XSSFCell celulaHeader = linhaCabecalhoTabela.getCell(i);
				if (celulaHeader != null) {
					celulaHeader.setCellStyle(headerStyle);
				}
			}
		}
	}

	private void formatarRegistrosTabela(XSSFWorkbook wb, XSSFSheet sheet) {

		if (objRsRelat != null && !objRsRelat.isEmpty()) {
			formatarLinhasDetalhe(wb, sheet);
		} else {
			formatarDetalheVazio(wb, sheet);
		}

		for (int i = 0; i <= sheet.getRow(4).getLastCellNum(); i++) {
			sheet.autoSizeColumn(i);
		}
	}

	private void formatarDetalheVazio(XSSFWorkbook wb, XSSFSheet sheet) {

		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setWrapText(true);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(font);

		CellRangeAddress detalheVazio = new CellRangeAddress(5, 5, 0, 13);
		sheet.addMergedRegion(detalheVazio);

		XSSFRow linhaVazia = sheet.getRow(5);
		if (linhaVazia == null) {
			linhaVazia = sheet.createRow(5);
		}
		XSSFCell celulaVazia = linhaVazia.createCell(0);
		linhaVazia.setHeightInPoints((short) 20);
		celulaVazia.setCellValue("Não existem dados para o período selecionado.");
		celulaVazia.setCellStyle(headerStyle);

		RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
		RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
		RegionUtil.setBorderRight(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
		RegionUtil.setBorderTop(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
	}

	private void formatarLinhasDetalhe(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyleLeft = wb.createCellStyle();
		headerStyleLeft.setAlignment(CellStyle.ALIGN_LEFT);
		headerStyleLeft.setWrapText(true);
		headerStyleLeft.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderTop(CellStyle.BORDER_THIN);
		
		XSSFCellStyle headerStyleRight = wb.createCellStyle();
		headerStyleRight.setAlignment(CellStyle.ALIGN_RIGHT);
		headerStyleRight.setWrapText(true);
		headerStyleRight.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderTop(CellStyle.BORDER_THIN);
		
		
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		headerStyleLeft.setFont(font);
		headerStyleRight.setFont(font);
		
		int linhaInicial = 5;
		int linhasDetalhe = objRsRelat.size();
		int linhaFinal = linhaInicial + linhasDetalhe;

		for (int linha = linhaInicial; linha <= linhaFinal; linha++) {
			XSSFRow detalheTabela = sheet.getRow(linha);
			if (detalheTabela != null) {
				for (int i = 0; i <= detalheTabela.getLastCellNum(); i++) {
					XSSFCell celulaDetalhe = detalheTabela.getCell(i);
					if (celulaDetalhe != null) {
						if (i == 1 || i == 9 || i == 12 || i == 13) {
							celulaDetalhe.setCellStyle(headerStyleRight);
						} else {
							celulaDetalhe.setCellStyle(headerStyleLeft);
						}
						
					}
				}
			}
		}
	}

	private void criarRodape(XSSFWorkbook wb, XSSFSheet sheet) {
		Font font;
		/* Criação, inclusão e formatação dos dados do footer do excel */
		XSSFRow ultimaLinha = sheet.createRow(sheet.getLastRowNum() + 1);

		XSSFCellStyle footerStyle = wb.createCellStyle();
		footerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		footerStyle.setWrapText(true);
		font = footerStyle.getFont();
		font.setFontHeight((short) 150);
		font.setFontName("Arial");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		footerStyle.setFont(font);

		XSSFCell cell1 = ultimaLinha.createCell(0);
		cell1.setCellValue(now());
		cell1.setCellStyle(footerStyle);

		XSSFCell cell2 = ultimaLinha.createCell(3);
		cell2.setCellValue("Superintêndencia de Ocorrências Especiais");
		cell2.setCellStyle(footerStyle);

		CellRangeAddress region1 = new CellRangeAddress(ultimaLinha.getRowNum(), ultimaLinha.getRowNum(), 0, 2);
		CellRangeAddress region2 = new CellRangeAddress(ultimaLinha.getRowNum(), ultimaLinha.getRowNum(), 3, 13);
		sheet.addMergedRegion(region1);
		sheet.addMergedRegion(region2);
	}

}
