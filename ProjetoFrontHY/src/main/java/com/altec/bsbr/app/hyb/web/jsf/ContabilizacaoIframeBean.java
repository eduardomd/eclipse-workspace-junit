package com.altec.bsbr.app.hyb.web.jsf;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import com.altec.bsbr.app.hyb.dto.ObjRsCont;


@Component("contabilizacaoIframeBean")
@Scope("session")
public class ContabilizacaoIframeBean extends BasicBBean {
	private static final long serialVersionUID = 1L;
    
	private String strNrSeqOs;
	//private Object objRsNrSeqNoti;
	
	private String txtHdAction="";

	ObjRsCont objRs = new ObjRsCont();
	
	@PostConstruct
	public void init() {
		this.strNrSeqOs = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs");
	}

	public ObjRsCont getObjRs() {
		objRs.setCODIGO("1234");
		objRs.setSITUACAO("Aprovado");
		objRs.setCD_NOTI("6010");
		objRs.setMOT_REC_APROV("Aprovado pelo sistema");
		return objRs;
	}
	
	public void recusarLancamento() {
		this.txtHdAction="R";
		
	    //set objRsNrSeqNoti = server.CreateObject("ADODB.RECORDSET")
	    //set objRsNrSeqNoti = objAcoesOs.IncluirProxWorkFlow(strNrSeqOs, "E", "6030", "DC", objRs.getMOT_REC_APROV(), strErro)
	    //VerificaErro(strErro)     
			    
		RequestContext.getCurrentInstance().execute("alert('Lan�amento Cont�bil da Ordem de Servi�o n� " + strNrSeqOs + " foi Reprovada!\\nNotifica��o enviada aos respons�veis.')");		
	}
    
    public String getStrNrSeqOs() {
		return this.strNrSeqOs;
	}

	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}
    
	public String getTxtHdAction() {
		return this.txtHdAction;
	}
	
	public void setTxtHdAction(String txtHdAction) {
		  this.txtHdAction = txtHdAction;
	}

}