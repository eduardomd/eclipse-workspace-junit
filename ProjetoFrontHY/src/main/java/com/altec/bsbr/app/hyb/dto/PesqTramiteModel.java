package com.altec.bsbr.app.hyb.dto;

import org.springframework.beans.factory.annotation.Autowired;

import com.altec.bsbr.app.jab.hyb.webclient.XHYTramite.XHYTramiteEndPoint;

public class PesqTramiteModel {
	
	private String OS;
	private String NR_TRA;
	private String DT_RETN;
	private String DT_REG;
	private String Flag;
	private String DT_RETN_PLAN;
	
	public PesqTramiteModel() {
		
	}
		
	public PesqTramiteModel(String oS, String nR_TRA, String dT_RETN, String dT_REG, String flag,
			String dT_RETN_PLAN) {
		super();
		this.OS = oS;
		this.NR_TRA = nR_TRA;
		this.DT_RETN = dT_RETN;
		this.DT_REG = dT_REG;
		this.Flag = flag;
		this.DT_RETN_PLAN = dT_RETN_PLAN;
	}



	public String getOS() {
		return OS;
	}
	public void setOS(String oS) {
		OS = oS;
	}
	public String getNR_TRA() {
		return NR_TRA;
	}
	public void setNR_TRA(String nR_TRA) {
		NR_TRA = nR_TRA;
	}
	public String getDT_RETN() {
		return DT_RETN;
	}
	public void setDT_RETN(String dT_RETN) {
		DT_RETN = dT_RETN;
	}
	public String getDT_REG() {
		return DT_REG;
	}
	public void setDT_REG(String dT_REG) {
		DT_REG = dT_REG;
	}
	public String getFlag() {
		return Flag;
	}
	public void setFlag(String flag) {
		Flag = flag;
	}
	public String getDT_RETN_PLAN() {
		return DT_RETN_PLAN;
	}
	public void setDT_RETN_PLAN(String dT_RETN_PLAN) {
		DT_RETN_PLAN = dT_RETN_PLAN;
	}

	
}
