package com.altec.bsbr.app.hyb.web.jsf;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.altec.bsbr.app.hyb.dto.Hyb_cados_dados_contabeis_exe_model;
import com.altec.bsbr.app.hyb.dto.Hyb_cados_dados_contabeis_exe_model_objRst;

@ManagedBean(name="hyb_cados_dados_contabeis_exe_bean")
@ViewScoped
public class Hyb_cados_dados_contabeis_exe implements Serializable {
	
	private String strReturn;
	
	// MOCK	
	Hyb_cados_dados_contabeis_exe_model_objRst objRst;
	Hyb_cados_dados_contabeis_exe_model objRs;
	
	private String strNmObj;
	private String strValue;
	private String strValuePai;
	private String strNrSequOS;
	private String strDtLancContab;
	private String strCntaContab;
	private String strEmpr;
	private String strAreaComp;
	private String strCatgContab1;
	private String strCatgContab2;
	private String strCatgContab3;
	private String strLinhNego1;
	private String strLinhNego2;
	private String strFatRisc;
	private String strTpCaptura;
	private String strCausaEvento;
	private String strTpMani;
	private String strOSRela;
	
	private String strCtrContbOUR;
	private String strCtrOrigOUR;
	private String strPTVD;
	// END MOCK
	
	public Hyb_cados_dados_contabeis_exe() {
			
	}
	
	@PostConstruct
	public void init() {
		objRs = new Hyb_cados_dados_contabeis_exe_model();
		objRst = new Hyb_cados_dados_contabeis_exe_model_objRst();
		
		// MOCK
		objRs.setDE_LINH_NEGO("DE_LINH_NEGO Test");
		objRs.setNO_DEPE("NO_DEPE Test");
		objRs.setNO_TIPO_EVEN_PERD("NO_TIPO_EVEN_PERD Test");
		objRs.setSQ_DEPE("SQ_DEPE Test");
		objRs.setSQ_LINH_NEGO("SQ_LINH_NEGO Test");
		objRs.setSQ_TIPO_EVEN_PERD("SQ_TIPO_EVEN_PERD Test");
		objRs.setSQ_TIPO_EVPD_PAI("SQ_TIPO_EVPD_PAI Test");
		
		objRst.setCD_MANI("2");
		
		this.setStrCtrContbOUR("null");
		this.setStrCtrOrigOUR("null");
		this.setStrPTVD("null");
		
		this.setStrNmObj("cboLinhaNego1");
		
		if (!strNmObj.equals("UPD")) {
			this.setStrValue("Teste strValue UPD");
			this.setStrValuePai("Teste strValuePai UPD");
			
		} else {
			this.setStrValue("StrValue Teste");
			this.setStrValuePai("StrValuePai Teste");
			this.setStrNrSequOS("NrSequOS Teste");
			this.setStrDtLancContab("17/01/19");
			this.setStrCntaContab("CntaContab Teste");
			this.setStrEmpr("StrEmpr Teste");
			this.setStrAreaComp("StrAreaComp Teste");
			this.setStrCatgContab1("StrCatgContab1 Teste");
			this.setStrCatgContab2("StrCatgContab2 Teste");
			this.setStrCatgContab3("StrCatgContab2 Teste");
			this.setStrLinhNego1("StrLinhNego1 Teste");
			this.setStrLinhNego2("StrLinhNego2 Teste");
			this.setStrFatRisc("StrFatRisc Teste");
			this.setStrTpCaptura("StrTpCaptura Teste");
			this.setStrCausaEvento("StrCausaEvento Teste");
			this.setStrTpMani("1"); // Se for 2 e cd_mani for diferente de 1 -> 'A OS relacionada n�o � do tipo TM0'
			this.setStrOSRela("StrOSRela Teste");
		
		}
		// END MOCK
		switch (strNmObj) {
		case "cboEmpresa":
			this.setStrReturn(fnGetAreComp(this.getStrAreaComp()));
			break;
		case "cboCatgNv3":
			this.setStrReturn(fnGetCatgNv(this.getStrValue(), this.getStrValuePai()));
			break;
		case "cboLinhaNego1":
			this.setStrReturn(fnLinhaNego2());
			break;
		case "UPD":
			this.setStrReturn(
					fnUpdDadosContabeis(strNrSequOS, strDtLancContab, strCntaContab, strEmpr, strAreaComp,
							strCatgContab1, strCatgContab2, strCatgContab3, strLinhNego1, strLinhNego2,
							strFatRisc, strTpCaptura, strCtrContbOUR, strCtrOrigOUR, strPTVD, strCausaEvento, strTpMani, strOSRela)
			);
			break;
		}
	}
	
	public String fnGetAreComp(String strValue) {
		/*
		 	set objCadOsDadosContab = server.CreateObject ("xHY_CadOsDadosContab.clsCadOsDadosCntb")	
        	set objRs = objCadOsDadosContab.fnSelValAreaComp(strValue, strMsgErro)
        	VerificaErro(strErro)
		 */
		strReturn = "<select id='cboAreaComp' name='cboAreaComp' style='width:285px;'>" + "\n";
		strReturn += "<option value='-1'>Selecione</option>" + "\n";
		
		for(int i = 0; i <= 3; i++) {
			strReturn += "<option value='" + objRs.getSQ_DEPE() + "'>" + objRs.getNO_DEPE() + "</option>" + "\n";
		}
		
		strReturn += "</select>";
		this.setStrReturn(strReturn);
		System.out.println("--- FN fnGetAreComp --- \n" + this.getStrReturn());
		return strReturn;
		
	}
	
	public String fnGetCatgNv(String strValue, String strValuePai) {
		if (strValuePai != "-1") {
			/*
			 Set objCadOsDadosContab = server.CreateObject ("xHY_CadOsDadosContab.clsCadOsDadosCntb")
	         Set objRs = objCadOsDadosContab.fnSelValCatgNv(strValuePai, "", "2", strMsgErro)
			 VerificaErro(strErro)
			*/
			
			// Monta Categ Nivel 2
			strReturn = "<select id='cboCatgNv2' name='cboCatgNv2' style='width:360px;' >";
			for(int i = 0; i <= 3; i++) {
				if (objRs.getSQ_TIPO_EVPD_PAI() == null) {
					objRs.setSQ_TIPO_EVPD_PAI("");
				} else {
					objRs.setSQ_TIPO_EVPD_PAI(objRs.getSQ_TIPO_EVPD_PAI());
				}
				
				 strReturn += "<option value='" + objRs.getSQ_TIPO_EVEN_PERD() + "' nvpai='" + objRs.getSQ_TIPO_EVPD_PAI() + "' >" + objRs.getNO_TIPO_EVEN_PERD() + "</option>";
			}
			strReturn += "</select>";
			
			// Monta Categ Nivel 1
			/*
			 Set objCadOsDadosContab = server.CreateObject ("xHY_CadOsDadosContab.clsCadOsDadosCntb")
	         Set objRs = objCadOsDadosContab.fnSelValCatgNv(trim(strSQ_TIPO_EVPD_PAI), "", "1", strMsgErro)
	         VerificaErro(strErro)
			*/
			
			strReturn += "@<select id='cboCatgNv1' name='cboCatgNv1' style='width:325px;' >" + "\n";
			
			for(int i = 0; i <= 3; i++) {
				if (objRs != null) {
					strReturn += "<option value='" + objRs.getSQ_TIPO_EVEN_PERD() + "' nvpai='" + objRs.getSQ_TIPO_EVPD_PAI() + "' >" + objRs.getNO_TIPO_EVEN_PERD() + "</option>" + "\n";
				}
			}
			
            strReturn += "</select>";
            
		} else {
			strReturn = "<select id='cboCatgNv1' name='cboCatgNv1' style='width:auto;' disabled ></select>@<select id='cboCatgNv2' name='cboCatgNv2' style='width:auto;' disabled></select>";
		}
		this.setStrReturn(strReturn);
		System.out.println("--- FN fnGetCatgNv --- \n" + this.getStrReturn());
		return strReturn;
	}
	
	public String fnLinhaNego2() {
		/*
		set objCadOsDadosContab = server.CreateObject ("xHY_CadOsDadosContab.clsCadOsDadosCntb")	
        set objRs = objCadOsDadosContab.fnSelValLinhaNego2(strValue, strMsgErro)
        VerificaErro(strErro)  
		*/
		
		strReturn = "<select id='cboLinhaNego2' name='cboLinhaNego2' style='width:248px;'>" + "\n" ;
        strReturn += "<option value='-1'>Selecione</option>" + "\n" ;
	       		
        for(int i = 0; i <= 3; i++) {
        	if (objRs != null) {
        		strReturn += "<option value='" + objRs.getSQ_LINH_NEGO() + "'>" + objRs.getDE_LINH_NEGO() + "</option>" + "\n";
        	}
        }
        
        strReturn += "</select>";
        
        this.setStrReturn(strReturn);
        System.out.println("--- FN fnLinhaNego2 --- \n" + this.getStrReturn());
		return strReturn;
	}
	
	public String fnUpdDadosContabeis(String strNrSequOS, String strDtLancContab, String strCntaContab,
            String strEmpr, String strAreaComp, String strCatgContab1, String strCatgContab2,
            String strCatgContab3, String strLinhNego1, String strLinhNego2, String strFatRisc, String strTpCaptura, 
            String strCtrContbOUR, String strCtrOrigOUR, String strPTVD, String strCausaEvento, String strTpMani, String strOSRela) {
		// set objCadOsDadosContab = server.CreateObject("xHY_CadOsDadosContab.clsCadOsDadosCntb")

		if (strTpMani == "2") {
			// set objRst = objCadOsDadosContab.fnSelOSRelacionada(strOSRela, strMsgErro)
			// VerificaErro(strMsgErro)
			if (objRs == null) {
				return "A OS relacionada n�o encontrada ou n�o esta encerrada!";
			} else {
				if (objRst.getCD_MANI() != "1") {
					return "A OS relacionada n�o � do tipo TM01!";
				}
			}
			
		}
		
		/*	objCadOsDadosContab.fnUpdDadosContabeis (strNrSequOS, strDtLancContab, strCntaContab, _
                strEmpr, strAreaComp, strCatgContab1, strCatgContab2, _
                strCatgContab3, strLinhNego1, strLinhNego2, strFatRisc, strTpCaptura, _ 
              	strCausaEvento, strTpMani, strOSRela, strMsgErro) */
		this.setStrReturn("Retorno da fun��o objCadOsDadosContab.fnUpdDadosContabeis do back-end");
		
		return this.getStrReturn();
	}

	public String getStrReturn() {
		return strReturn;
	}

	public void setStrReturn(String strReturn) {
		this.strReturn = strReturn;
	}

	public Hyb_cados_dados_contabeis_exe_model_objRst getObjRst() {
		return objRst;
	}

	public void setObjRst(Hyb_cados_dados_contabeis_exe_model_objRst objRst) {
		this.objRst = objRst;
	}

	public Hyb_cados_dados_contabeis_exe_model getObjRs() {
		return objRs;
	}

	public void setObjRs(Hyb_cados_dados_contabeis_exe_model objRs) {
		this.objRs = objRs;
	}

	public String getStrNmObj() {
		return strNmObj;
	}

	public void setStrNmObj(String strNmObj) {
		this.strNmObj = strNmObj;
	}

	public String getStrValue() {
		return strValue;
	}

	public void setStrValue(String strValue) {
		this.strValue = strValue;
	}

	public String getStrValuePai() {
		return strValuePai;
	}

	public void setStrValuePai(String strValuePai) {
		this.strValuePai = strValuePai;
	}

	public String getStrNrSequOS() {
		return strNrSequOS;
	}

	public void setStrNrSequOS(String strNrSequOS) {
		this.strNrSequOS = strNrSequOS;
	}

	public String getStrDtLancContab() {
		return strDtLancContab;
	}

	public void setStrDtLancContab(String strDtLancContab) {
		this.strDtLancContab = strDtLancContab;
	}

	public String getStrCntaContab() {
		return strCntaContab;
	}

	public void setStrCntaContab(String strCntaContab) {
		this.strCntaContab = strCntaContab;
	}

	public String getStrEmpr() {
		return strEmpr;
	}

	public void setStrEmpr(String strEmpr) {
		this.strEmpr = strEmpr;
	}

	public String getStrAreaComp() {
		return strAreaComp;
	}

	public void setStrAreaComp(String strAreaComp) {
		this.strAreaComp = strAreaComp;
	}

	public String getStrCatgContab1() {
		return strCatgContab1;
	}

	public void setStrCatgContab1(String strCatgContab1) {
		this.strCatgContab1 = strCatgContab1;
	}

	public String getStrCatgContab2() {
		return strCatgContab2;
	}

	public void setStrCatgContab2(String strCatgContab2) {
		this.strCatgContab2 = strCatgContab2;
	}

	public String getStrCatgContab3() {
		return strCatgContab3;
	}

	public void setStrCatgContab3(String strCatgContab3) {
		this.strCatgContab3 = strCatgContab3;
	}

	public String getStrLinhNego1() {
		return strLinhNego1;
	}

	public void setStrLinhNego1(String strLinhNego1) {
		this.strLinhNego1 = strLinhNego1;
	}

	public String getStrLinhNego2() {
		return strLinhNego2;
	}

	public void setStrLinhNego2(String strLinhNego2) {
		this.strLinhNego2 = strLinhNego2;
	}

	public String getStrFatRisc() {
		return strFatRisc;
	}

	public void setStrFatRisc(String strFatRisc) {
		this.strFatRisc = strFatRisc;
	}

	public String getStrTpCaptura() {
		return strTpCaptura;
	}

	public void setStrTpCaptura(String strTpCaptura) {
		this.strTpCaptura = strTpCaptura;
	}

	public String getStrCausaEvento() {
		return strCausaEvento;
	}

	public void setStrCausaEvento(String strCausaEvento) {
		this.strCausaEvento = strCausaEvento;
	}

	public String getStrTpMani() {
		return strTpMani;
	}

	public void setStrTpMani(String strTpMani) {
		this.strTpMani = strTpMani;
	}

	public String getStrOSRela() {
		return strOSRela;
	}

	public void setStrOSRela(String strOSRela) {
		this.strOSRela = strOSRela;
	}

	public String getStrCtrContbOUR() {
		return strCtrContbOUR;
	}

	public void setStrCtrContbOUR(String strCtrContbOUR) {
		this.strCtrContbOUR = strCtrContbOUR;
	}

	public String getStrCtrOrigOUR() {
		return strCtrOrigOUR;
	}

	public void setStrCtrOrigOUR(String strCtrOrigOUR) {
		this.strCtrOrigOUR = strCtrOrigOUR;
	}

	public String getStrPTVD() {
		return strPTVD;
	}

	public void setStrPTVD(String strPTVD) {
		this.strPTVD = strPTVD;
	}
	

}
