package com.altec.bsbr.app.hyb.web.jsf;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;

import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.ObjRsCombo;
import com.altec.bsbr.app.hyb.dto.StatusOS;
import com.altec.bsbr.app.hyb.web.util.XHYUsuarioIncService;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAcoesOs.XHYAcoesOsEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsDadosIniciais.XHYCadOsDadosIniciaisEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYPesquisa.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYPesquisa.XHYPesquisaEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;

@Component("alterarStatusOs")
@Scope("session")
public class AlterarStatusOsBean extends BasicBBean {

	private static final long serialVersionUID = 1L;
	private StatusOS objRsOs;
	private List<ObjRsCombo> objRsComboStatus;
	private List<ObjRsCombo> objRsComboWorkflow;
	private String strRetorno;
	private String cboStatus = "1";
	private String cboSituacaoOsSelecionada;
	private String cboSituacaoWorkFlowSelecionada;
	private LinkedHashMap<String, String> cboSituacaoOs;
	private LinkedHashMap<String, String> cboSituacaoWorkFlow;
	private String strNrSeqOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs");

	@Autowired
	private XHYCadOsDadosIniciaisEndPoint objDadosInicias;
	
	@Autowired
	private XHYPesquisaEndPoint objPesquisa;
	
	@Autowired
	private XHYAcoesOsEndPoint objAcoes;
	
	@Autowired 
	private XHYUsuarioIncService userServ;
	
	@PostConstruct
	public void init() {
		System.out.println("init  12");
		//System.out.println(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs"));
		carregarOs();
		carregarComboStatus();
		carregarComboWorkflow();
	}

	public void verificarPermissao(ComponentSystemEvent event) {
		String[] perfis = {"SUPERINT","GER","ADM"};
		
		userServ.verificarPermissao(perfis);
	}
	
	private void carregarOs() {
		
		String objRs;
		
		try {

			objRs = objDadosInicias.consultarOs(this.strNrSeqOs);

			JSONObject objRsTemp = new JSONObject(objRs);
			JSONArray pcursor = objRsTemp.getJSONArray("PCURSOR");
			JSONObject f = pcursor.getJSONObject(0);
			
			objRsOs = new StatusOS();
			
			objRsOs.setCD_NOTI(f.get("CD_NOTI").toString());
			objRsOs.setCODIGO(f.get("CODIGO").toString());
			objRsOs.setNM_NOTI(f.get("NM_NOTI").toString());
			objRsOs.setSITUACAO(f.get("CD_SITUACAO").toString());
			
			System.out.println(f.get("CD_NOTI").toString()); 
			System.out.println(f.get("NM_NOTI").toString()); 
			
		} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsDadosIniciais.WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void carregarComboStatus() {
		
		cboSituacaoOs = new LinkedHashMap<>();
        try {
			String retorno = objPesquisa.consultarSituacaoOs();
		
			JSONObject consultaArea = new JSONObject(retorno);
			JSONArray pCursorArea = consultaArea.getJSONArray("PCURSOR");

			for (int i = 0; i < pCursorArea.length(); i++) {
				JSONObject f = pCursorArea.getJSONObject(i);

				cboSituacaoOs.put(f.get("CODIGO").toString(), f.get("NOME").toString());
			} 
			
			} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
	}
	
	private void carregarComboWorkflow() {
		
		cboSituacaoWorkFlow = new LinkedHashMap<>();
        try {
			String retorno = objPesquisa.consultarWorkFlow();
		
			JSONObject consultaArea = new JSONObject(retorno);
			JSONArray pCursorArea = consultaArea.getJSONArray("PCURSOR");

			for (int i = 0; i < pCursorArea.length(); i++) {
				JSONObject f = pCursorArea.getJSONObject(i);

				cboSituacaoWorkFlow.put(f.get("NR_NOTI").toString(), f.get("NM_NOTI").toString());
				
			} 
			
			
			} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
	}
	
	public void teste() {
		System.out.println("Teste 001");
	}
	
	public void alterar() {
		String retorno = "";
		
		if (cboSituacaoOsSelecionada.isEmpty() || cboSituacaoWorkFlowSelecionada.isEmpty()) {
			RequestContext.getCurrentInstance().execute("alert('Selecione o Status OS e seu WorkFlow')");
			return;
		}
		
		try {
			retorno = objAcoes.alterarStatusWorkFlowOs(strNrSeqOs, cboSituacaoOsSelecionada, cboSituacaoWorkFlowSelecionada);
		} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYAcoesOs.WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		RequestContext.getCurrentInstance().execute("alert('Alterações realizadas com sucesso!')");
		carregarOs();
		RequestContext.getCurrentInstance().execute("parent.window.document.location.href = '';");

	}
	
	public StatusOS getObjRsOs() {
		return objRsOs;
	}
	public void setObjRsOs(StatusOS objRsOs) {
		this.objRsOs = objRsOs;
	}
	public List<ObjRsCombo> getObjRsComboStatus() {
		//System.out.println(objRsComboStatus.size()); 
		return objRsComboStatus;
	}
	public void setObjRsComboStatus(List<ObjRsCombo> objRsComboStatus) {
		this.objRsComboStatus = objRsComboStatus;
	}
	public List<ObjRsCombo> getObjRsComboWorkflow() {
		return objRsComboWorkflow;
	}
	public void setObjRsComboWorkflow(List<ObjRsCombo> objRsComboWorkflow) {
		this.objRsComboWorkflow = objRsComboWorkflow;
	}
	public String getStrRetorno() {
		return strRetorno;
	}
	public void setStrRetorno(String strRetorno) {
		this.strRetorno = strRetorno;
	}
	public String getStrNrSeqOs() {
		return strNrSeqOs;
	}
	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}
	public String getCboStatus() {
		return cboStatus;
	}
	public void setCboStatus(String cboStatus) {
		this.cboStatus = cboStatus;
	}

	public String getCboSituacaoOsSelecionada() {
		return cboSituacaoOsSelecionada;
	}

	public void setCboSituacaoOsSelecionada(String cboSituacaoOsSelecionada) {
		this.cboSituacaoOsSelecionada = cboSituacaoOsSelecionada.split("=")[0];
	}
	
	public LinkedHashMap<String, String> getCboSituacaoOs() {
		return cboSituacaoOs;
	}
	
	public void setCboSituacaoOs(LinkedHashMap<String, String> cboSituacaoOs) {
		this.cboSituacaoOs = cboSituacaoOs;
	}
	
	public LinkedHashMap<String, String> getCboSituacaoWorkFlow() {
		return cboSituacaoWorkFlow;
	}
	
	public void setCboSituacaoWorkFlow(LinkedHashMap<String, String> cboSituacaoWorkFlow) {
		this.cboSituacaoWorkFlow = cboSituacaoWorkFlow;
	}

	public String getCboSituacaoWorkFlowSelecionada() {
		return cboSituacaoWorkFlowSelecionada;
	}

	public void setCboSituacaoWorkFlowSelecionada(String cboSituacaoWorkFlowSelecionada) {
		this.cboSituacaoWorkFlowSelecionada = cboSituacaoWorkFlowSelecionada.split("=")[0];
	}
}
