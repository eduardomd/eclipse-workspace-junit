package com.altec.bsbr.app.hyb.web.jsf;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name="hyb_cados_detalheriscopot_t_exe")
@ViewScoped
public class Hyb_cados_detalheriscopot_t_exe {
	
	private String strDados;
	
	public Hyb_cados_detalheriscopot_t_exe() {
	
	}
	
	public Hyb_cados_detalheriscopot_t_exe(String strDados) {
		this.strDados = strDados;
	}

	public String getStrDados() {
		return strDados;
	}

	public void setStrDados(String strDados) {
		this.strDados = strDados;
	}
	
	public void setStrDadosStr() {
		String strDadosStr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strDadosStr").toString();
		this.setStrDados(strDadosStr);
		System.out.println("--- DADOS ---");
		System.out.println(this.getStrDados());
		
	}

}
