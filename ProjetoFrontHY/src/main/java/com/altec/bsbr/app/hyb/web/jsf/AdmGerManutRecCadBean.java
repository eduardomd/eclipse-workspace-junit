package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.ObjRsArea;
import com.altec.bsbr.app.hyb.dto.ObjRsCargo;
import com.altec.bsbr.app.hyb.dto.ObjRsSituacaoRecurso;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmGerRecurso.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmGerRecurso.XHYAdmGerRecursoEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;

@Component("admGerManutRecCadBean")
@Scope("request")
public class AdmGerManutRecCadBean extends BasicBBean {
	
	private static final long serialVersionUID = 1L;


	@Autowired
	private XHYAdmGerRecursoEndPoint admGerRecursoEndPoint;
	
	
	public String ver = "1.2eq1";
	
	//public String objAdmGerRecurso;
	//public String objRsArea;
	//public String objRsSituacaoRecurso;
	public String objRsCargo;
	public String objRsRec;
	public String strCpf;
	public String strCdCargo;
	public String strNome;
	public String strMatricula;
	public String strLogin;
	public String strTpRecurso;
	public String strCdSituacao;
	public String strCdArea;
	public String strEmail;
	public String strVlRemuneracao;
	public String strTpRemuneracao;
	public String strErro;
	public String strMsg;
	public String blnRet;
	public String strAction;
	public String strCpfUsuario;
	private String cpfSuperior = "";
	private String nomeSuperior = "";
	private String retornoSuperior;
	private String strCpfQueryString;
	private boolean alteracao;
	
	private List<ObjRsCargo> objRsCargos;
	private List<ObjRsArea> objRsArea;
	private List<ObjRsSituacaoRecurso> objRsSituacaoRecurso;
	private String retorno;
	
	@PostConstruct
    public void init() {
		buscarObjRsCargos();
		buscarObjRsArea();
		buscarObjRsSituacaoRecurso();
		
		verificaQueryString();
	}
	
	public List<ObjRsArea> getObjRsArea() {
		return objRsArea;
	}
	
	//SELECTS DA PAGINA 
	public void setObjRsArea(List<ObjRsArea> objRsArea) {
		this.objRsArea = objRsArea;
	}

	public List<ObjRsCargo> getObjRsCargos() {
		return objRsCargos;
	}

	public void setObjRsCargos(List<ObjRsCargo> objRsCargos) {
		this.objRsCargos = objRsCargos;
	}
	
	public List<ObjRsSituacaoRecurso> getObjRsSituacaoRecurso() {
		return objRsSituacaoRecurso;
	}
		
	public void setObjRsSituacaoRecurso(List<ObjRsSituacaoRecurso> objRsSituacaoRecurso) {
		this.objRsSituacaoRecurso = objRsSituacaoRecurso;
	}
	
	public void buscarObjRsCargos() {
		try {			
			retorno = admGerRecursoEndPoint.consultarCargoOcesp();
	        this.objRsCargos = new ArrayList<ObjRsCargo>();
	        
	        JSONObject consultarCargo= new JSONObject(retorno);
	        JSONArray pcursor = consultarCargo.getJSONArray("PCURSOR");
	        
	        for (int i = 0; i < pcursor.length(); i++) {
	        	JSONObject item = pcursor.getJSONObject(i);	   
	        	this.objRsCargos.add(new ObjRsCargo(item.getString("CODIGO"), item.getString("NOME")));
	        }
	
		} catch (Exception e) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
	         } catch (IOException e1) {
	               // TODO Auto-generated catch block
	               e1.printStackTrace();
	         }
    	}
	}
	
	public void buscarObjRsArea() {
		try {			
			retorno = admGerRecursoEndPoint.consultarArea();
	        this.objRsArea = new ArrayList<ObjRsArea>();
	        
	        JSONObject consultarArea= new JSONObject(retorno);
	        JSONArray pcursor = consultarArea.getJSONArray("PCURSOR");
	        
	        for (int i = 0; i < pcursor.length(); i++) {
	        	JSONObject item = pcursor.getJSONObject(i);	   
	        	this.objRsArea.add(new ObjRsArea(item.getString("CODIGO"), item.getString("NOME")));
	        }
	
		} catch (Exception e) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
	         } catch (IOException e1) {
	               // TODO Auto-generated catch block
	               e1.printStackTrace();
	         }
    	}
	}
	
	public void buscarObjRsSituacaoRecurso() {
		try {			
			retorno = admGerRecursoEndPoint.consultarSituacaoRecurso();
	        this.objRsSituacaoRecurso = new ArrayList<ObjRsSituacaoRecurso>();
	        
	        JSONObject consultarSituacaoRecurso = new JSONObject(retorno);
	        JSONArray pcursor = consultarSituacaoRecurso.getJSONArray("PCURSOR");
	        
	        for (int i = 0; i < pcursor.length(); i++) {
	        	JSONObject item = pcursor.getJSONObject(i);	   
	        	this.objRsSituacaoRecurso.add(new ObjRsSituacaoRecurso(item.getString("CODIGO"), item.getString("NOME")));
	        }
	
		} catch (Exception e) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
	         } catch (IOException e1) {
	               // TODO Auto-generated catch block
	               e1.printStackTrace();
	         }
    	}
	}
	
	public void cadastrar() {
		
		try {
			this.strCpf = this.strCpf.replaceAll("[^0-9]", "");
			retorno = admGerRecursoEndPoint.consultarRecurso(this.strCpf, "", "");
			
			JSONObject consultarSituacaoRecurso = new JSONObject(retorno);
			JSONArray pcursor = consultarSituacaoRecurso.getJSONArray("PCURSOR");
			if(pcursor.length() > 0) {
				RequestContext.getCurrentInstance().execute("alert('Cpf já existente! Os recursos não foi incluído!')");
			} else {
				if(!this.cpfSuperior.isEmpty()) {
					this.cpfSuperior = this.cpfSuperior.replaceAll("[^0-9]", "");
				}
				
				admGerRecursoEndPoint.inserirRecurso(this.getStrCpf(), this.getStrCdCargo(), this.getStrNome(), this.getStrMatricula(), this.getStrLogin(), this.getStrTpRecurso(), this.getStrCdSituacao(), this.getStrCdArea(), this.getCpfSuperior(), this.getStrEmail(), this.getStrVlRemuneracao(), this.getStrTpRemuneracao(), "", "");
				RequestContext.getCurrentInstance().execute("alert('Recurso incluído com sucesso!')");
				RequestContext.getCurrentInstance().execute("limpar()");
				this.resetaCampos();
			}
			
		} catch (Exception e) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
	         } catch (IOException e1) {
	               // TODO Auto-generated catch block
	               e1.printStackTrace();
	         }
    	}
	}
	
	public void buscarRecurso() {
		try {
			retorno = admGerRecursoEndPoint.consultarRecurso(this.getStrCpfQueryString(), "", "");
			
			JSONObject consultarSituacaoRecurso = new JSONObject(retorno);
			JSONArray pcursor = consultarSituacaoRecurso.getJSONArray("PCURSOR");
			if(pcursor.length() > 0) {
	        	JSONObject item = pcursor.getJSONObject(0);
	        	
	        	this.setStrCpf(item.getString("CPF"));
	        	this.setStrNome(item.getString("NOME"));
	        	
	        	if(!item.get("LOGIN").equals(null)) {
	        		this.setStrLogin(item.get("LOGIN").toString());
	        	}
	        		
	        	if(!item.get("MATRICULA").equals(null)) {
	        		this.setStrMatricula(item.getString("MATRICULA"));	        		
	        	}
	        	
	        	if(!item.get("CODIGO_CARGO").equals(null)) {
	        		this.setStrCdCargo(item.getString("CODIGO_CARGO"));	        		
	        	}
	        	
	        	if(!item.get("CODIGO_TIPO_RECURSO").equals(null)) {
	        		this.setStrTpRecurso(item.getString("CODIGO_TIPO_RECURSO"));
	        	}
	        	
	        	this.setStrCdSituacao(item.getString("CODIGO_SITUACAO"));
	        	this.setStrCdArea(item.getString("CODIGO_AREA"));
	        	this.setStrEmail(item.getString("EMAIL"));
	        	this.setStrVlRemuneracao( String.valueOf(item.get("REMU")));
	        	this.setStrTpRemuneracao(String.valueOf(item.getBigDecimal("TIPO_REMU")));
	        	
	        	if(!item.get("CPF_SUPERIOR").equals(null)) {
	        		this.setCpfSuperior(item.get("CPF_SUPERIOR").toString());
	        	}
	        	
	        	if(!item.get("NOME_SUPERIOR").equals(null)) {
	        		this.setNomeSuperior(item.getString("NOME_SUPERIOR"));
	        	}
			}
			
		} catch (Exception e) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
	         } catch (IOException e1) {
	               // TODO Auto-generated catch block
	               e1.printStackTrace();
	         }
    	}
		
	}
	
	public void alterar() {
		System.out.println("Chama alterar");
		try {
			
			this.strCpf = this.strCpf.replaceAll("[^0-9]", "");
				if(!this.cpfSuperior.isEmpty()) {
					this.cpfSuperior = this.cpfSuperior.replaceAll("[^0-9]", "");
				}
				
				admGerRecursoEndPoint.alterarRecurso(this.getStrCpf(), this.getStrCdCargo(), this.getStrNome(), this.getStrMatricula(), this.getStrLogin(), this.getStrTpRecurso(), this.getStrCdSituacao(), this.getStrCdArea(), this.getCpfSuperior(), this.getStrEmail(), this.getStrVlRemuneracao(), this.getStrTpRemuneracao(), "", "");
				RequestContext.getCurrentInstance().execute("alert('Dados alterados com sucesso!')");
				
		} catch (Exception e) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
	         } catch (IOException e1) {
	               // TODO Auto-generated catch block
	               e1.printStackTrace();
	         }
    	}
	}
	
	public void paginaPesquisa() {
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_adm_ger_manut_rec_pesq.xhtml");
		} catch (Exception e) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
	         } catch (IOException e1) {
	               // TODO Auto-generated catch block
	               e1.printStackTrace();
	         }
    	}
	}
	
	
	public void verificaQueryString() {
		this.strCpfQueryString = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strCpf");
		
		if(strCpfQueryString == null || strCpfQueryString.isEmpty()) {
			this.setAlteracao(false);
		}else {
			this.setAlteracao(true);
			buscarRecurso();
			RequestContext.getCurrentInstance().execute("document.getElementById(\"frm:txtVlRemuneracao_input\").type = \"password\"");			
		}
	}
	
	public void resetaCampos() {
		this.strCpf = "";
		this.strNome = "";
		this.strLogin = "";
		this.strMatricula = "";
		this.strCdCargo = "";
		this.strTpRecurso = "";
		this.strCdSituacao = "";
		this.strCdArea = "";
		this.strEmail = "";
		this.strVlRemuneracao = "";
		this.strTpRemuneracao = null;		
		this.cpfSuperior = "";
		this.nomeSuperior = "";
	}
	
	public void pesquisar() {
    	
    	try {
    		this.cpfSuperior = this.cpfSuperior.replace("-", "");
    		this.cpfSuperior = this.cpfSuperior.replace(".", "");
    		
    		retornoSuperior = admGerRecursoEndPoint.consultarRecurso(this.getCpfSuperior(), "", "");
    		
    		JSONObject consultarRecurso = new JSONObject(retornoSuperior);
	        JSONArray pcursor = consultarRecurso.getJSONArray("PCURSOR");
	        
	        
	        if(pcursor.length() == 0) {
	        	this.setCpfSuperior("");
	        	this.setNomeSuperior("");
	        	RequestContext.getCurrentInstance().execute("alert('Superior não Encontrado !!')");
	        }else {
		        nomeSuperior =  pcursor.getJSONObject(0).getString("NOME");
	        }
	        
    	} catch (Exception e) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
	         } catch (IOException e1) {
	               // TODO Auto-generated catch block
	               e1.printStackTrace();
	         }
    	}    	
    }

	public String getObjRsCargo() {
		return objRsCargo;
	}

	public void setObjRsCargo(String objRsCargo) {
		this.objRsCargo = objRsCargo;
	}

	public String getObjRsRec() {
		return objRsRec;
	}

	public void setObjRsRec(String objRsRec) {
		this.objRsRec = objRsRec;
	}

	public String getStrCpf() {
		return strCpf;
	}

	public void setStrCpf(String strCpf) {
		this.strCpf = strCpf;
	}

	public String getStrCdCargo() {
		return strCdCargo;
	}

	public void setStrCdCargo(String strCdCargo) {
		this.strCdCargo = strCdCargo;
	}

	public String getStrNome() {
		return strNome;
	}

	public void setStrNome(String strNome) {
		this.strNome = strNome;
	}

	public String getStrMatricula() {
		return strMatricula;
	}

	public void setStrMatricula(String strMatricula) {
		this.strMatricula = strMatricula;
	}

	public String getStrLogin() {
		return strLogin;
	}

	public void setStrLogin(String strLogin) {
		this.strLogin = strLogin;
	}

	public String getStrTpRecurso() {
		return strTpRecurso;
	}

	public void setStrTpRecurso(String strTpRecurso) {
		this.strTpRecurso = strTpRecurso;
	}

	public String getStrCdSituacao() {
		return strCdSituacao;
	}

	public void setStrCdSituacao(String strCdSituacao) {
		this.strCdSituacao = strCdSituacao;
	}

	public String getStrCdArea() {
		return strCdArea;
	}

	public void setStrCdArea(String strCdArea) {
		this.strCdArea = strCdArea;
	}

	public String getStrEmail() {
		return strEmail;
	}

	public void setStrEmail(String strEmail) {
		this.strEmail = strEmail;
	}

	public String getStrVlRemuneracao() {
		return strVlRemuneracao;
	}

	public void setStrVlRemuneracao(String strVlRemuneracao) {
		this.strVlRemuneracao = strVlRemuneracao;
	}

	public String getStrTpRemuneracao() {
		return strTpRemuneracao;
	}

	public void setStrTpRemuneracao(String strTpRemuneracao) {
		this.strTpRemuneracao = strTpRemuneracao;
	}

	public String getStrErro() {
		return strErro;
	}

	public void setStrErro(String strErro) {
		this.strErro = strErro;
	}

	public String getStrMsg() {
		return strMsg;
	}

	public void setStrMsg(String strMsg) {
		this.strMsg = strMsg;
	}

	public String getBlnRet() {
		return blnRet;
	}

	public void setBlnRet(String blnRet) {
		this.blnRet = blnRet;
	}

	public String getStrAction() {
		return strAction;
	}

	public void setStrAction(String strAction) {
		this.strAction = strAction;
	}

	public String getStrCpfUsuario() {
		return strCpfUsuario;
	}

	public void setStrCpfUsuario(String strCpfUsuario) {
		this.strCpfUsuario = strCpfUsuario;
	}

	public String getCpfSuperior() {
		return cpfSuperior;
	}

	public void setCpfSuperior(String cpfSuperior) {
		this.cpfSuperior = cpfSuperior;
	}

	public String getNomeSuperior() {
		return nomeSuperior;
	}

	public void setNomeSuperior(String nomeSuperior) {
		this.nomeSuperior = nomeSuperior;
	}

	public String getStrCpfQueryString() {
		return strCpfQueryString;
	}

	public void setStrCpfQueryString(String strCpfQueryString) {
		this.strCpfQueryString = strCpfQueryString;
	}

	public boolean isAlteracao() {
		return alteracao;
	}

	public void setAlteracao(boolean alteracao) {
		this.alteracao = alteracao;
	}
	
	
	
}
