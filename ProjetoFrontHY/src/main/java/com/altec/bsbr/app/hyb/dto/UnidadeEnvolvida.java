package com.altec.bsbr.app.hyb.dto;
/*SIMULA��O BACK*/
public class UnidadeEnvolvida {
	public String CD_UOR;
	public String TP_UOR;
	public String NM_REDE;
	public String NM_REGI;
	public String NM_UOR;
	public Boolean EOF;
	public String strErro;
	
	public UnidadeEnvolvida() {}
	
	public String getCD_UOR() {
		return CD_UOR;
	}
	public void setCD_UOR(String cD_UOR) {
		CD_UOR = cD_UOR;
	}
	public String getTP_UOR() {
		return TP_UOR;
	}
	public void setTP_UOR(String tP_UOR) {
		TP_UOR = tP_UOR;
	}
	public String getNM_REDE() {
		return NM_REDE;
	}
	public void setNM_REDE(String nM_REDE) {
		NM_REDE = nM_REDE;
	}
	public String getNM_REGI() {
		return NM_REGI;
	}
	public void setNM_REGI(String nM_REGI) {
		NM_REGI = nM_REGI;
	}
	public String getNM_UOR() {
		return NM_UOR;
	}
	public void setNM_UOR(String nM_UOR) {
		NM_UOR = nM_UOR;
	}

	public Boolean getEOF() {
		return EOF;
	}

	public void setEOF(Boolean eOF) {
		EOF = eOF;
	}

	public String getStrErro() {
		return strErro;
	}

	public void setStrErro(String strErro) {
		this.strErro = strErro;
	}
	
	
	
	
}
