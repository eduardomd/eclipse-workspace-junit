package com.altec.bsbr.app.hyb.web.jsf;

import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


@Component("CadosEquipatribPopupBean")
@Scope("session")
public class CadosEquipatribPopupBean extends BasicBBean{

	private String srcIframe;
	private String strNrSeqOs;

	public CadosEquipatribPopupBean() {	
		System.out.println("Iniciou");
	
	// block to get params from url
	FacesContext fc = FacesContext.getCurrentInstance();
	@SuppressWarnings("unchecked")
	Map<String, String> params = fc.getExternalContext().getRequestParameterMap();

	strNrSeqOs = params.get("strNrSeqOs");
	
	setStrNrSeqOs(strNrSeqOs);
	
	srcIframe = "hyb_cados_equipatrib_popup_iframe.xhtml?strNrSeqOs=" + getStrNrSeqOs();
	
	setSrcIframe(srcIframe);
	
	}

	public String getSrcIframe() {
		return srcIframe;
	}

	public void setSrcIframe(String srcIframe) {
		this.srcIframe = srcIframe;
	}

	public String getStrNrSeqOs() {
		return strNrSeqOs;
	}

	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}
	
}
