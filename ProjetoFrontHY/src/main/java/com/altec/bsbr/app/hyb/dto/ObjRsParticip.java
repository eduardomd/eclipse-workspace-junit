package com.altec.bsbr.app.hyb.dto;

public class ObjRsParticip {
	
	private String TP_PESSOA;
	private String MATRICULA;
	private String NOME;
	private String CARGO;
	private String CPF_CNPJ;
	private String SEGMENTO;
	private String PARTICIPACAO;
	private String SQ_PARP;
	private int CBO_DESIG;
	private String strCDSEG;
	private String PENUMPE;
	
	private int TP_PARP;
	private String NM_PARP;
	
	public ObjRsParticip() {
		
	}
	
	public ObjRsParticip(String tP_PESSOA, String mATRICULA, String nOME, String cARGO, String cPF_CNPJ,
			String sEGMENTO, String pARTICIPACAO, String sQ_PARP) {
		super();
		TP_PESSOA = tP_PESSOA;
		MATRICULA = mATRICULA;
		NOME = nOME;
		CARGO = cARGO;
		CPF_CNPJ = cPF_CNPJ;
		SEGMENTO = sEGMENTO;
		PARTICIPACAO = pARTICIPACAO;
		SQ_PARP = sQ_PARP;
	}

	public ObjRsParticip(String cPF_CNPJ, int cBO_DESIG, int tP_PARP, String nM_PARP) {
		super();
		CPF_CNPJ = cPF_CNPJ;
		CBO_DESIG = cBO_DESIG;
		TP_PARP = tP_PARP;
		NM_PARP = nM_PARP;
	}

	public String getTP_PESSOA() {
		return TP_PESSOA;
	}

	public void setTP_PESSOA(String tP_PESSOA) {
		TP_PESSOA = tP_PESSOA;
	}

	public String getMATRICULA() {
		return MATRICULA;
	}

	public void setMATRICULA(String mATRICULA) {
		MATRICULA = mATRICULA;
	}

	public String getNOME() {
		return NOME;
	}

	public void setNOME(String nOME) {
		NOME = nOME;
	}

	public String getCARGO() {
		return CARGO;
	}

	public void setCARGO(String cARGO) {
		CARGO = cARGO;
	}

	public String getCPF_CNPJ() {
		return CPF_CNPJ;
	}

	public void setCPF_CNPJ(String cPF_CNPJ) {
		CPF_CNPJ = cPF_CNPJ;
	}

	public String getSEGMENTO() {
		return SEGMENTO;
	}

	public void setSEGMENTO(String sEGMENTO) {
		SEGMENTO = sEGMENTO;
	}

	public String getPARTICIPACAO() {
		return PARTICIPACAO;
	}

	public void setPARTICIPACAO(String pARTICIPACAO) {
		PARTICIPACAO = pARTICIPACAO;
	}

	public String getSQ_PARP() {
		return SQ_PARP;
	}

	public void setSQ_PARP(String sQ_PARP) {
		SQ_PARP = sQ_PARP;
	}

	public int getCBO_DESIG() {
		return CBO_DESIG;
	}

	public void setCBO_DESIG(int cBO_DESIG) {
		CBO_DESIG = cBO_DESIG;
	}


	public int getTP_PARP() {
		return TP_PARP;
	}


	public void setTP_PARP(int tP_PARP) {
		TP_PARP = tP_PARP;
	}


	public String getNM_PARP() {
		return NM_PARP;
	}


	public void setNM_PARP(String nM_PARP) {
		NM_PARP = nM_PARP;
	}

	public String getStrCDSEG() {
		return strCDSEG;
	}

	public void setStrCDSEG(String strCDSEG) {
		this.strCDSEG = strCDSEG;
	}

	public String getPENUMPE() {
		return PENUMPE;
	}

	public void setPENUMPE(String pENUMPE) {
		PENUMPE = pENUMPE;
	}

}
