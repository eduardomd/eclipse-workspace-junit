package com.altec.bsbr.app.hyb.dto;

public class RelOsFraudeEncerradaModel {
	private String EVENTO;
	private String BANCO;
	private String QUANTIDADE;
	private String VL_PREJ;
	private String VL_PERD_EFET;
	private String CD_CNAL;
	private String CANAL;
	private String VALOR;
	
	public RelOsFraudeEncerradaModel(
			String EVENTO,
			String CD_CNAL,
			String BANCO,
			String QUANTIDADE,
			String VL_PREJ,
			String VL_PERD_EFET,
			String CANAL,
			String VALOR) {
		this.EVENTO = EVENTO;
		this.CD_CNAL = CD_CNAL;
		this.BANCO = BANCO;
		this.QUANTIDADE = QUANTIDADE;
		this.VL_PREJ = VL_PREJ;
		this.VL_PERD_EFET = VL_PERD_EFET;
		this.CANAL = CANAL;
		this.VALOR = VALOR;
	}
	
	public String getEVENTO() {
		return EVENTO;
	}
	public void setEVENTO(String eVENTO) {
		EVENTO = eVENTO;
	}
	public String getCD_CNAL() {
		return CD_CNAL;
	}
	public void setCD_CNAL(String cD_CNAL) {
		CD_CNAL = cD_CNAL;
	}
	public String getBANCO() {
		return BANCO;
	}
	public void setBANCO(String bANCO) {
		BANCO = bANCO;
	}
	public String getQUANTIDADE() {
		return QUANTIDADE;
	}
	public void setQUANTIDADE(String qUANTIDADE) {
		QUANTIDADE = qUANTIDADE;
	}
	public String getVL_PREJ() {
		return VL_PREJ;
	}
	public void setVL_PREJ(String vL_PREJ) {
		VL_PREJ = vL_PREJ;
	}
	public String getVL_PERD_EFET() {
		return VL_PERD_EFET;
	}
	public void setVL_PERD_EFET(String vL_PERD_EFET) {
		VL_PERD_EFET = vL_PERD_EFET;
	}
	public String getCANAL() {
		return CANAL;
	}
	public void setCANAL(String cANAL) {
		CANAL = cANAL;
	}

	public String getVALOR() {
		return VALOR;
	}

	public void setVALOR(String vALOR) {
		VALOR = vALOR;
	}
}
