package com.altec.bsbr.app.hyb.dto;

public class HyAdmGerManutPenaModel {
	private int CODIGO;
	private String NOME;
	private boolean EXCLUSAO;
	
	public HyAdmGerManutPenaModel(int cODIGO, String nOME, boolean eXCLUSAO) {
		super();
		CODIGO = cODIGO;
		NOME = nOME;
		EXCLUSAO = eXCLUSAO;
	}

	public int getCODIGO() {
		return CODIGO;
	}
	
	public void setCODIGO(int cODIGO) {
		CODIGO = cODIGO;
	}
	public String getNOME() {
		return NOME;
	}
	public void setNOME(String nOME) {
		NOME = nOME;
	}
	public boolean isEXCLUSAO() {
		return EXCLUSAO;
	}
	public void setEXCLUSAO(boolean eXCLUSAO) {
		EXCLUSAO = eXCLUSAO;
	}
}
