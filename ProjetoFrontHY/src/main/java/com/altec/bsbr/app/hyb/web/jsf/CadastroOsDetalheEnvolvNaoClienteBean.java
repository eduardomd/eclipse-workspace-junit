package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.apache.commons.lang.StringUtils;
import org.icefaces.impl.component.Redirect;
import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.DadosCadastraisModel;
import com.altec.bsbr.app.hyb.dto.EnderecoApuradoModel;
import com.altec.bsbr.app.hyb.dto.OsAssociadaModel;
import com.altec.bsbr.app.hyb.dto.PosRiscoDtAberturaOsModel;
import com.altec.bsbr.app.hyb.dto.RestDtAberturaModel;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsParticip.XHYCadOsParticipEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.ibm.icu.text.NumberFormat;
import com.ibm.icu.text.SimpleDateFormat;


@Component("CadastroOsDetalheEnvolvNaoClienteBean")
@Scope("session")
public class CadastroOsDetalheEnvolvNaoClienteBean extends BasicBBean {

	private static final long serialVersionUID = 1L;
	
	//Query String
	private String strgNrSeqPart;	
	private String strgNrSeqOs;
	
	
	private List<OsAssociadaModel> osAssociada;
	
	private DadosCadastraisModel dadosCadastrais;
	private EnderecoApuradoModel enderecoApurado;
	private PosRiscoDtAberturaOsModel posRisco;
	private List<RestDtAberturaModel> restricoesDtAbertura;
	
	private Locale ptBR;
	
	@Autowired
	private XHYCadOsParticipEndPoint cadOsParticip;
		
	@PostConstruct
	public void init() {
		this.ptBR = new Locale("pt", "BR");
		this.osAssociada = new ArrayList<OsAssociadaModel>();
		this.restricoesDtAbertura = new ArrayList<RestDtAberturaModel>();
		this.dadosCadastrais = new DadosCadastraisModel();
		this.enderecoApurado = new EnderecoApuradoModel();
		this.posRisco = new PosRiscoDtAberturaOsModel();
		this.strgNrSeqPart = "";
		this.strgNrSeqOs = "";
		
		if (FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("NrSeqPart") != null)
			this.strgNrSeqPart = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("NrSeqPart").toString();
		if (FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs") != null)
			this.strgNrSeqOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs").toString();
		
		System.out.println("strgNrSeqPart "+this.strgNrSeqPart);
		System.out.println("strgNrSeqOs "+this.strgNrSeqOs);
		
		if (!strgNrSeqPart.isEmpty()) {
			fillDetalhePart();
			fillObjRestricao(Double.valueOf(strgNrSeqPart));
			fillTblOsAssociada(this.dadosCadastrais.getCpfCnpj(), this.strgNrSeqOs);				
			fillCboEndereco(strgNrSeqPart);
		}
	}
	
	public void fillCboEndereco(String strgNrSeqPart) {
		try {
			String retorno = cadOsParticip.consultarEndeCliente(Double.valueOf(strgNrSeqPart), 0);

			JSONObject consultaOsAssociada = new JSONObject(retorno);
			JSONArray pCursorArea = consultaOsAssociada.getJSONArray("PCURSOR");

			for (int i = 0; i < pCursorArea.length(); i++) {
				JSONObject f = pCursorArea.getJSONObject(i);
				if (!f.isNull("TIPO"))
					if (f.getInt("TIPO") == 1) { 
						this.dadosCadastrais.setEndereco(f.isNull("LOGRADOURO") ? "" : f.getString("LOGRADOURO"));
						this.dadosCadastrais.setNumEndereco(f.isNull("NUMERO") ? "" : f.getString("NUMERO"));
						this.dadosCadastrais.setComplementoEndereco(f.isNull("COMPLEMENTO") ? "" : f.getString("COMPLEMENTO"));
						this.dadosCadastrais.setBairroEndereco(f.isNull("BAIRRO") ? "" : f.getString("BAIRRO"));
						this.dadosCadastrais.setCepEndereco(f.isNull("CEP") ? "" : String.valueOf(f.getInt("CEP")));
						this.dadosCadastrais.setCidadeEndereco(f.isNull("CIDADE") ? "" : f.getString("CIDADE"));
						this.dadosCadastrais.setEstadoEndereco(f.isNull("ESTADO") ? "" : f.getString("ESTADO"));
						this.dadosCadastrais.setTelefone(f.isNull("TELEFONE") ? "" : f.getString("TELEFONE"));
						this.dadosCadastrais.setCelular(f.isNull("CELULAR") ? "" : f.getString("CELULAR"));
						
						dadosCadastrais.setCepEndereco(StringUtils.leftPad(dadosCadastrais.getCepEndereco(), 8, "0"));
						dadosCadastrais.setCepEndereco(dadosCadastrais.getCepEndereco().substring(0, 5) + "-" + dadosCadastrais.getCepEndereco().substring(5, 8));
					}
			}
								
		} catch (Exception e) {
			e.printStackTrace();
            try {
            	FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
	         } catch (IOException e1) {
	               // TODO Auto-generated catch block
	               e1.printStackTrace();
	         }
		}
		
	}
	
	public void fillTblOsAssociada(String strgNrDocto, String strNrOs) {
		try {
			String retorno = cadOsParticip.consultarOSAssociada(strgNrDocto);

			JSONObject consultaOsAssociada = new JSONObject(retorno);
			JSONArray pCursorArea = consultaOsAssociada.getJSONArray("PCURSOR");

			for (int i = 0; i < pCursorArea.length(); i++) {
				JSONObject f = pCursorArea.getJSONObject(i);
				if(!f.isNull("ORDEM"))
					if (!f.getString("ORDEM").equals(this.strgNrSeqOs)) 
						this.osAssociada.add(new OsAssociadaModel(
								f.getString("ORDEM"),
								f.isNull("AREA") ? "" : f.getString("AREA"),
								f.isNull("STATUS") ? "" : f.getString("STATUS")
								));								
			}
								
		} catch (Exception e) {
			e.printStackTrace();
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
	         } catch (IOException e1) {
	               // TODO Auto-generated catch block
	               e1.printStackTrace();
	         }
		}
	}
	
	public void fillDetalhePart() {
		try {
			String retorno = cadOsParticip.consultarDetalhePart(Double.valueOf(strgNrSeqPart));

			JSONObject consultaDetalhe = new JSONObject(retorno);
			JSONArray pCursorArea = consultaDetalhe.getJSONArray("PCURSOR");

			for (int i = 0; i < pCursorArea.length(); i++) {
				JSONObject f = pCursorArea.getJSONObject(i);
				
				if (f.get("TP_PARTICIPACAO") != null)
					this.dadosCadastrais.setDesig(String.valueOf(f.getInt("TP_PARTICIPACAO")));
				
				this.strgNrSeqOs = f.get("OS") == null ? "" : f.getString("OS");
				
				this.dadosCadastrais.setTipoPessoa(f.getString("TP_PESSOA"));
				//this.dadosCadastrais.setTipoPessoa(f.getInt("TP_PARP") == 5 ? "J" : "F");
				System.out.println("tipoPessoa "+this.dadosCadastrais.getTipoPessoa());
				System.out.println("f.getString(\"TP_PESSOA\") "+f.getString("TP_PESSOA"));
				System.out.println("f.getInt(\"TP_PARP\") "+f.getInt("TP_PARP"));
				
				this.dadosCadastrais.setNome(f.isNull("NOME") ? "" : f.getString("NOME"));
				this.dadosCadastrais.setCpfCnpj(f.isNull("CPF_CNPJ") ? "" : f.getString("CPF_CNPJ"));
				this.dadosCadastrais.setRg(f.isNull("RG") ? "" : f.getString("RG"));
				this.dadosCadastrais.setOrgaoEmissor(f.isNull("ORGAO_EMISSOR") ? "" : f.getString("ORGAO_EMISSOR"));
				this.dadosCadastrais.setDtEmissao(f.isNull("DT_EMISSAO_RG") ? "" : f.getString("DT_EMISSAO_RG"));
				this.dadosCadastrais.setProfissao(f.isNull("PROFISSAO") ? "" : f.getString("PROFISSAO"));
				this.dadosCadastrais.setRenda(f.isNull("RENDA") ? "" : this.formatarMoeda(String.valueOf(f.getInt("RENDA"))));
				
				this.enderecoApurado.setRuaAvenida(f.isNull("ENDERECO") ? "" : f.getString("ENDERECO"));
				this.enderecoApurado.setNum(f.isNull("NUMERO") ? "" : f.get("NUMERO").toString());
				this.enderecoApurado.setComplemento(f.isNull("COMPLEMENTO") ? "" : f.getString("COMPLEMENTO"));
				this.enderecoApurado.setBairro(f.isNull("BAIRRO") ? "" : f.getString("BAIRRO"));
				this.enderecoApurado.setCep(f.isNull("CEP") ? "" : String.valueOf(f.getInt("CEP")));
				String telefone = f.isNull("TELEFONE") ? "" : f.getString("TELEFONE");
				String celular = f.isNull("CELULAR") ? "" : f.getString("CELULAR");
				if (telefone.length() > 2) {
					this.enderecoApurado.setTelefoneDDD(telefone.substring(0, 2));
					this.enderecoApurado.setTelefoneNum(telefone.substring(2,  telefone.length()));
				}
				else
					this.enderecoApurado.setTelefoneNum(telefone);
				
				if (celular.length() > 2) {
					this.enderecoApurado.setCelularDDD(celular.substring(0, 2));
					this.enderecoApurado.setCelularNum(celular.substring(2,  celular.length()));
				}
				else
					this.enderecoApurado.setCelularNum(celular);				
				
				this.posRisco.setDtAbertura(f.isNull("DT_ABERTURA") ? "" : f.getString("DT_ABERTURA"));
				this.posRisco.setOper_vencer(f.isNull("OPER_VENCER") ? "" : this.formatarMoeda(String.valueOf(f.getInt("OPER_VENCER"))));
				this.posRisco.setOper_vencida(f.isNull("OPER_VENCIDA") ? "" : this.formatarMoeda(String.valueOf(f.getInt("OPER_VENCIDA"))));
				this.posRisco.setOper_creli(f.isNull("OPER_CRELI") ? "" : this.formatarMoeda(String.valueOf(f.getInt("OPER_CRELI"))));
				this.posRisco.setOper_preju(f.isNull("OPER_PREJU") ? "" : this.formatarMoeda(String.valueOf(f.getInt("OPER_PREJU"))));
				this.posRisco.setOper_passiva(f.isNull("OPER_PASSIVA") ? "" : this.formatarMoeda(String.valueOf(f.getInt("OPER_PASSIVA"))));
				this.posRisco.setReciprocidade(f.isNull("RECIPROCIDADE") ? "" : this.formatarMoeda(String.valueOf(f.getInt("RECIPROCIDADE"))));
				
				//Tratamentos para formatação
				
				
				enderecoApurado.setCep(StringUtils.leftPad(enderecoApurado.getCep(), 8, "0"));
				enderecoApurado.setCep(enderecoApurado.getCep().substring(0, 5) + "-" + enderecoApurado.getCep().substring(5, 8));
				
				if (this.enderecoApurado.getTelefoneNum().isEmpty()) {
					this.enderecoApurado.setTelefoneDDD("00");
					this.enderecoApurado.setTelefoneNum("000000000");
				}
				
				if (this.enderecoApurado.getCelularNum().isEmpty()) {
					this.enderecoApurado.setCelularDDD("00");
					this.enderecoApurado.setCelularNum("000000000");
				}				
				
				if (this.dadosCadastrais.getRenda().equals("0"))
					this.dadosCadastrais.setRenda("0,00");
			} 

		} catch (Exception e) {
			e.printStackTrace();
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
	         } catch (IOException e1) {
	               // TODO Auto-generated catch block
	               e1.printStackTrace();
	         }
		}			
	}

	public void fillObjRestricao(Double NrSeqPart) {
		try {
			this.restricoesDtAbertura.clear();
			String retorno = cadOsParticip.consultarRestricaoPart(NrSeqPart);

			JSONObject consultaDetalhe = new JSONObject(retorno);
			JSONArray pCursorArea = consultaDetalhe.getJSONArray("PCURSOR");

			for (int i = 0; i < pCursorArea.length(); i++) {
				JSONObject f = pCursorArea.getJSONObject(i);
								
				this.restricoesDtAbertura.add(new RestDtAberturaModel(
						f.isNull("SQ_RESTRICAO") ? "" : String.valueOf(f.getInt("SQ_RESTRICAO")),
						f.isNull("INSTITUICAO") ? "" : f.getString("INSTITUICAO"),
						f.isNull("ESPECIFICACAO") ? "" : f.getString("ESPECIFICACAO"),
						f.isNull("VALOR") ? "" : this.formatarMoeda(String.valueOf(f.getDouble("VALOR"))),
						f.isNull("DATA") ? "" : f.getString("DATA")
						));

			} 

		} catch (Exception e) {
			e.printStackTrace();
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
	         } catch (IOException e1) {
	               // TODO Auto-generated catch block
	               e1.printStackTrace();
	         }
		}			
	}	

	public void salvar() {
		String stMetodo = "";
		SimpleDateFormat oldFormat = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		try {			
			//SalvarParticipante
			stMetodo = "Incluir Participante";
			System.out.println("salvarParticipante::.. ");
			System.out.println("strgNrSeqPart "+strgNrSeqPart);
			System.out.println("this.dadosCadastrais.getCpfCnpj() "+this.dadosCadastrais.getCpfCnpj());
			System.out.println("this.dadosCadastrais.getNome() "+this.dadosCadastrais.getNome());
			System.out.println("this.dadosCadastrais.getDesig() "+this.dadosCadastrais.getDesig());
			System.out.println("this.dadosCadastrais.getTipoPessoa() "+this.dadosCadastrais.getTipoPessoa());
			
			Integer hdnTipo = this.dadosCadastrais.getTipoPessoa().equals("J") ? 5 : 4;
			System.out.println("strTpParticipante "+this.dadosCadastrais.getTipoPessoa());
			System.out.println("hdnTipo "+hdnTipo);
			Integer iDesig = Integer.valueOf(this.dadosCadastrais.getDesig());
			Integer dblGravar = 0;
			String jsonRetorno = "";
			Boolean bRet;
			
			
			if (strgNrSeqPart.equals("0") || strgNrSeqPart.isEmpty()) {
				jsonRetorno = cadOsParticip.inserirParticipante("", 
												  this.dadosCadastrais.getCpfCnpj(), 
												  strgNrSeqOs, 
												  hdnTipo, 
												  this.dadosCadastrais.getNome(), 
												  iDesig, 
												  "", 
												  "", 
												  0);
				JSONObject json = new JSONObject(jsonRetorno);
				dblGravar = json.getInt("PNR_SEQU_PARP");
				System.out.println("true");
				System.out.println(json);
				System.out.println(dblGravar);
				this.strgNrSeqPart = dblGravar.toString();
			}
			else {
				dblGravar = Integer.valueOf(
								cadOsParticip.atualizarPartNaoCliente(strgNrSeqPart, 
					  							      this.dadosCadastrais.getCpfCnpj(), 
													  hdnTipo,
													  this.dadosCadastrais.getNome(),
													  iDesig));
				System.out.println("false");
				System.out.println(dblGravar);
			}
			
			if(dblGravar <= 0) {
				RequestContext.getCurrentInstance().execute("alert('Não foi possível atualizar o(s) registro(s) - Incluir Participante.')");
				throw new Exception("");
			}
			
			
			//SalvarInfCliente
			stMetodo = "Informação";
			System.out.println("salvarInfCliente: ");
			System.out.println("strgNrSeqPart "+strgNrSeqPart);
			System.out.println("this.dadosCadastrais.getRg() "+this.dadosCadastrais.getRg());
			System.out.println("this.dadosCadastrais.getOrgaoEmissor() "+this.dadosCadastrais.getOrgaoEmissor());
			System.out.println("this.dadosCadastrais.getDtEmissao() "+this.dadosCadastrais.getDtEmissao());
			System.out.println("this.dadosCadastrais.getProfissao() "+this.dadosCadastrais.getProfissao());
			System.out.println("this.dadosCadastrais.getRenda() "+this.dadosCadastrais.getRenda());
			
			if (!this.dadosCadastrais.getDtEmissao().isEmpty()) {
				Date dt = oldFormat.parse(this.dadosCadastrais.getDtEmissao());
				this.dadosCadastrais.setDtEmissao(newFormat.format(dt));
			}
			
			//Retorna vazio
			jsonRetorno = cadOsParticip.atualizarPartPersonas(Double.valueOf(strgNrSeqPart),
												this.dadosCadastrais.getRg(),
												this.dadosCadastrais.getOrgaoEmissor(),
												this.dadosCadastrais.getDtEmissao(),
												this.dadosCadastrais.getProfissao(),
												Double.valueOf("0"+this.dadosCadastrais.getRenda().replace(".", "").replace(",", ".")),
												"");
			
			System.out.println("retorno atualizarPartPersonas "+jsonRetorno);
							
			//fnSalvarEnderecoNaoCliente
			stMetodo = "Endereco não cliente/outros";
			System.out.println("fnSalvarEnderecoNaoCliente");
			System.out.println("strgNrSeqPart "+strgNrSeqPart);
			System.out.println("this.dadosCadastrais.getEndereco() "+this.dadosCadastrais.getEndereco());
			System.out.println("this.dadosCadastrais.getNumEndereco() "+this.dadosCadastrais.getNumEndereco());
			System.out.println("this.dadosCadastrais.getComplementoEndereco() "+this.dadosCadastrais.getComplementoEndereco());
			System.out.println("this.dadosCadastrais.getBairroEndereco() "+this.dadosCadastrais.getBairroEndereco());
			System.out.println("this.dadosCadastrais.getCepEndereco() "+this.dadosCadastrais.getCepEndereco().replace("-", ""));
			System.out.println("this.dadosCadastrais.getCidadeEndereco() "+this.dadosCadastrais.getCidadeEndereco());
			System.out.println("this.dadosCadastrais.getEstadoEndereco() "+this.dadosCadastrais.getEstadoEndereco());
			System.out.println("this.dadosCadastrais.getTelefone() "+this.dadosCadastrais.getTelefone());
			System.out.println("this.dadosCadastrais.getCelular() "+this.dadosCadastrais.getCelular());
			
			jsonRetorno = cadOsParticip.inserirEnderecoCliente(Double.valueOf(strgNrSeqPart),
												 1,
												 this.dadosCadastrais.getEndereco(),
												 this.dadosCadastrais.getBairroEndereco(),
												 this.dadosCadastrais.getNumEndereco(),
												 Double.parseDouble("0"+this.dadosCadastrais.getCepEndereco().replace("-", "")),
												 this.dadosCadastrais.getTelefone(),
												 this.dadosCadastrais.getCelular(),
												 this.dadosCadastrais.getComplementoEndereco(),
												 this.dadosCadastrais.getCidadeEndereco(),
												 this.dadosCadastrais.getEstadoEndereco(),
												 "");
			System.out.println("inserirEnderecoCliente "+jsonRetorno);
		
			
			//fnSalvarEnderecoCliente
			stMetodo = "Endereco Apurado";
			System.out.println("fnSalvarEnderecoCliente");
			System.out.println("strgNrSeqPart "+strgNrSeqPart);
			System.out.println("this.enderecoApurado.getRuaAvenida() " +this.enderecoApurado.getRuaAvenida());
			System.out.println("this.enderecoApurado.getNum() "+this.enderecoApurado.getNum());
			System.out.println("this.enderecoApurado.getComplemento() "+this.enderecoApurado.getComplemento());
			System.out.println("this.enderecoApurado.getBairro() "+this.enderecoApurado.getBairro());
			System.out.println("this.enderecoApurado.getCep() "+this.enderecoApurado.getCep().replace("-", ""));
			System.out.println("this.enderecoApurado.getTelefoneDDD()+this.enderecoApurado.getTelefoneNum()  "+this.enderecoApurado.getTelefoneDDD()+this.enderecoApurado.getTelefoneNum());
			System.out.println("this.enderecoApurado.getCelularDDD()+this.enderecoApurado.getCelularNum()  "+this.enderecoApurado.getCelularDDD()+this.enderecoApurado.getCelularNum());
			
			bRet = cadOsParticip.atualizarEnderecoParticipante(strgNrSeqPart,
									this.enderecoApurado.getRuaAvenida(),
									this.enderecoApurado.getNum(),
									this.enderecoApurado.getComplemento(),
									this.enderecoApurado.getBairro(),
									this.enderecoApurado.getCep().replace("-", ""),
									"",
									"",
									this.enderecoApurado.getTelefoneDDD()+this.enderecoApurado.getTelefoneNum(),
									this.enderecoApurado.getCelularDDD()+this.enderecoApurado.getCelularNum());
			System.out.println("bRet "+bRet);
			if (!bRet) 
				throw new Exception("Não foi possível atualizar o(s) registro(s) -"+stMetodo+".");			
						
			//SalvarPosicaoRisco
			stMetodo = "PosicaoRisco";
			System.out.println("salvarPosicaoRisco");
			System.out.println("strgNrSeqPart "+strgNrSeqPart);
			System.out.println("this.posRisco.getOper_vencer() "+this.posRisco.getOper_vencer());
			System.out.println("this.posRisco.getOper_vencida() "+this.posRisco.getOper_vencida());
			System.out.println("this.posRisco.getOper_creli() "+this.posRisco.getOper_creli());
			System.out.println("this.posRisco.getOper_preju() "+this.posRisco.getOper_preju());
			System.out.println("this.posRisco.getOper_passiva() "+this.posRisco.getOper_passiva());
			System.out.println("this.posRisco.getReciprocidade() "+this.posRisco.getReciprocidade());
			
			bRet = cadOsParticip.fnUpdposicaoRisco(strgNrSeqPart,
					this.posRisco.getOper_vencer().replace(".", ""),
					this.posRisco.getOper_vencida().replace(".", ""),
					this.posRisco.getOper_creli().replace(".", ""),
					this.posRisco.getOper_preju().replace(".", ""),
					this.posRisco.getOper_passiva().replace(".", ""),
					this.posRisco.getReciprocidade().replace(".", ""));
			
			System.out.println("bRet "+bRet);
			if (!bRet) 
				throw new Exception("Não foi possível atualizar o(s) registro(s) -"+stMetodo+".");
			
			//SalvarRestricoes
			stMetodo = "Restricoes";
			
			for(int i = 0; i < this.restricoesDtAbertura.size(); i++) {				
				System.out.println("atualizaRestricoes "+i);
				System.out.println("strgNrSeqPart "+strgNrSeqPart);
				System.out.println("this.restricoesDtAbertura.get(i).getSq_restricao() "+this.restricoesDtAbertura.get(i).getSq_restricao());
				System.out.println("this.restricoesDtAbertura.get(i).getInstituicao() "+this.restricoesDtAbertura.get(i).getInstituicao());
				System.out.println("this.restricoesDtAbertura.get(i).getEspecificacao() "+this.restricoesDtAbertura.get(i).getEspecificacao());
				System.out.println("this.restricoesDtAbertura.get(i).getValor().replace(\".\", \"\") "+this.restricoesDtAbertura.get(i).getValor().replace(".", ""));
				System.out.println("this.restricoesDtAbertura.get(i).getValor() "+this.restricoesDtAbertura.get(i).getValor());
				System.out.println("this.restricoesDtAbertura.get(i).getData() "+this.restricoesDtAbertura.get(i).getData());
				
				
				if (this.restricoesDtAbertura.get(i).getSq_restricao().isEmpty()) {
					System.out.println("SQ_RESTRICAO EMPTY!!");
					if (!this.restricoesDtAbertura.get(i).getData().isEmpty()) {
						Date dt = oldFormat.parse(this.restricoesDtAbertura.get(i).getData());
						this.restricoesDtAbertura.get(i).setData(newFormat.format(dt));
					}
					
					bRet = cadOsParticip.inserirRestricaoParticipante(Integer.valueOf(strgNrSeqPart),
							this.restricoesDtAbertura.get(i).getInstituicao(),
							this.restricoesDtAbertura.get(i).getEspecificacao(),
							this.restricoesDtAbertura.get(i).getValor().replace(".", "").replace(",", "."),
							this.restricoesDtAbertura.get(i).getData());
				}
				else 
					bRet = cadOsParticip.fnUpdRestrParticipante(this.restricoesDtAbertura.get(i).getSq_restricao(),
							this.restricoesDtAbertura.get(i).getInstituicao(),
							this.restricoesDtAbertura.get(i).getEspecificacao(),
							this.restricoesDtAbertura.get(i).getValor().replace(".", ""),
							this.restricoesDtAbertura.get(i).getData());
				
				System.out.println("bRet "+bRet);
			}
			
			System.out.println("final");
			stMetodo = "";
			RequestContext.getCurrentInstance().execute("alert('Registro(s) atualizado(s) com sucesso.');");
		} catch (Exception e) {
			e.printStackTrace();
			RequestContext.getCurrentInstance().execute(stMetodo.isEmpty() ? 
					"alert('Não foi possível atualizar o(s) registro(s).');" :					
					"alert('Não foi possível atualizar o(s) registro(s) - "+stMetodo+".');");
//			try {
//				FacesContext.getCurrentInstance().getExternalContext()
//						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
//			} catch (IOException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
		}
	}
		
	public void excluir() {
		try {
			String strIdRestPart = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strIdRestPart").toString();
			System.out.println("strIdRestPart "+strIdRestPart);
			cadOsParticip.excluirRestricaoPart(Double.valueOf(strIdRestPart));
			fillObjRestricao(Double.valueOf(strgNrSeqPart));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	public void insereLinha() {
		this.restricoesDtAbertura.add(new RestDtAberturaModel("", "", "", "", ""));
	}
	
	public String formatarMoeda(String valor) {
		return NumberFormat.getCurrencyInstance(ptBR).format(Double.parseDouble(valor)).replace("R$", "");
	}
	
	public void voltar() {
		if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("cadosParticipacaoProcessoBeanBean") != null)
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("cadosParticipacaoProcessoBeanBean");
		RequestContext.getCurrentInstance().execute("history.back(-1);");
	}
	
	public String getStrgNrSeqOs() {
		return strgNrSeqOs;
	}

	public void setStrgNrSeqOs(String strgNrSeqOs) {
		this.strgNrSeqOs = strgNrSeqOs;
	}

	public List<OsAssociadaModel> getOsAssociada() {
		return osAssociada;
	}

	public void setOsAssociada(List<OsAssociadaModel> osAssociada) {
		this.osAssociada = osAssociada;
	}

	public DadosCadastraisModel getDadosCadastrais() {
		return dadosCadastrais;
	}

	public void setDadosCadastrais(DadosCadastraisModel dadosCadastrais) {
		this.dadosCadastrais = dadosCadastrais;
	}

	public String getStrgNrSeqPart() {
		return strgNrSeqPart;
	}

	public void setStrgNrSeqPart(String strgNrSeqPart) {
		this.strgNrSeqPart = strgNrSeqPart;
	}

	public EnderecoApuradoModel getEnderecoApurado() {
		return enderecoApurado;
	}

	public void setEnderecoApurado(EnderecoApuradoModel enderecoApurado) {
		this.enderecoApurado = enderecoApurado;
	}

	public PosRiscoDtAberturaOsModel getPosRisco() {
		return posRisco;
	}

	public void setPosRisco(PosRiscoDtAberturaOsModel posRisco) {
		this.posRisco = posRisco;
	}

	public List<RestDtAberturaModel> getRestricoesDtAbertura() {
		return restricoesDtAbertura;
	}

	public void setRestricoesDtAbertura(List<RestDtAberturaModel> restricoesDtAbertura) {
		this.restricoesDtAbertura = restricoesDtAbertura;
	}
		
}

