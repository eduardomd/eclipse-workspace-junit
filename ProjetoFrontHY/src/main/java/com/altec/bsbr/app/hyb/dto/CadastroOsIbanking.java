package com.altec.bsbr.app.hyb.dto;

public class CadastroOsIbanking {
	
	private String strNrSeqOs;		// String armazena o n�mero da OS	
	private String blnSalvar;		// Indicador de grava��o
	private String strTitular;		// Nome do Titular
	private String strSeqConta;		// N�mero Sequencial do Cart�o
	private String strSeqCartao;	// N�mero Sequencial do Cart�o
	private String strAgencia;		// N�mero da Ag�ncia
	private String strConta;		// N�mero da Conta
	private String strCartao;		// N�mero Cart�o
	private String strCdCanal;		// C�digo do Canal
	private String strSeqEvento;	// N�mero Sequencial da Fraude Evento
	private String strCPF_CNPJ;		// N�mero do CPF ou CNPJ
	private String blnInteComl;	// Indicador de Interesse Comercial
	private String strIPCliente;	// IP do Cliente
	private String strIPFraude;		// IP da Fraude
	private String blnCrtoSegr;	// Indicador de Cart�o de Seguran�a
	private String strDtSoliCrto;	// Data solicita��o do cart�o de seguran�a
	private String strDtCancCrto;	// Data Cancelamento do cart�o de seguran�a
	private String strDtAtivCrto;	// Data Ativa��o do cart�o de seguran�a
	private String strDtFrauCrto;	// Data da 1a Fraude do cart�o de seguran�a
	private int intInfrPopup;		// Indicador de informa��o de PopUp

	private String strNrQntdEven;
	private String strCdCentroOrigem;	//Centro de origem		pCD_CTRO_ORIG
	private String strCdCentroDestino;	//Centro de destino		pCD_CTRO_DEST
	private String strCdCentroOperante;	//Centro operante		pCD_CTRO_OPER
	private String strContabilizado;	//valida contabliza��o
	
	private String strErro;				//String de Erro

	public CadastroOsIbanking() {}
	
	public CadastroOsIbanking(
			String strNrSeqOs,
			String blnSalvar,
			String strTitular,
			String strSeqConta,
			String strSeqCartao,
			String strAgencia,
			String strConta,
			String strCartao,
			String strCdCanal,
			String strSeqEvento,
			String strCPF_CNPJ,
			String blnInteComl,
			String strIPCliente,
			String strIPFraude,
			String blnCrtoSegr,
			String strDtSoliCrto,
			String strDtCancCrto,
			String strDtAtivCrto,
			String strDtFrauCrto,
			int intInfrPopup,
			String strNrQntdEven,
			String strCdCentroOrigem,
			String strCdCentroDestino,
			String strCdCentroOperante,
			String strContabilizado) {
		this.strNrSeqOs = strNrSeqOs;
		this.blnSalvar = blnSalvar;
		this.strTitular = strTitular;
		this.strSeqConta = strSeqConta;
		this.strSeqCartao = strSeqCartao;
		this.strAgencia = strAgencia;
		this.strConta = strConta;
		this.strCartao = strCartao;
		this.strCdCanal = strCdCanal;
		this.strSeqEvento = strSeqEvento;
		this.strCPF_CNPJ = strCPF_CNPJ;
		this.blnInteComl = blnInteComl;
		this.strIPCliente = strIPCliente;
		this.strIPFraude = strIPFraude;
		this.blnCrtoSegr = blnCrtoSegr;
		this.strDtSoliCrto = strDtSoliCrto;
		this.strDtCancCrto = strDtCancCrto;
		this.strDtAtivCrto = strDtAtivCrto;
		this.strDtFrauCrto = strDtFrauCrto;
		this.intInfrPopup = intInfrPopup;
		this.strNrQntdEven = strNrQntdEven;
		this.strCdCentroOrigem = strCdCentroOrigem;
		this.strCdCentroDestino = strCdCentroDestino;
		this.strCdCentroOperante = strCdCentroOperante;
		this.strContabilizado = strContabilizado;
	}

	public String getStrNrSeqOs() {
		return strNrSeqOs;
	}

	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}

	public String getBlnSalvar() {
		return blnSalvar;
	}

	public void setBlnSalvar(String blnSalvar) {
		this.blnSalvar = blnSalvar;
	}

	public String getStrTitular() {
		return strTitular;
	}

	public void setStrTitular(String strTitular) {
		this.strTitular = strTitular;
	}

	public String getStrSeqConta() {
		return strSeqConta;
	}

	public void setStrSeqConta(String strSeqConta) {
		this.strSeqConta = strSeqConta;
	}

	public String getStrSeqCartao() {
		return strSeqCartao;
	}

	public void setStrSeqCartao(String strSeqCartao) {
		this.strSeqCartao = strSeqCartao;
	}

	public String getStrAgencia() {
		return strAgencia;
	}

	public void setStrAgencia(String strAgencia) {
		this.strAgencia = strAgencia;
	}

	public String getStrConta() {
		return strConta;
	}

	public void setStrConta(String strConta) {
		this.strConta = strConta;
	}

	public String getStrCartao() {
		return strCartao;
	}

	public void setStrCartao(String strCartao) {
		this.strCartao = strCartao;
	}

	public String getStrCdCanal() {
		return strCdCanal;
	}

	public void setStrCdCanal(String strCdCanal) {
		this.strCdCanal = strCdCanal;
	}

	public String getStrSeqEvento() {
		return strSeqEvento;
	}

	public void setStrSeqEvento(String strSeqEvento) {
		this.strSeqEvento = strSeqEvento;
	}

	public String getStrCPF_CNPJ() {
		return strCPF_CNPJ;
	}

	public void setStrCPF_CNPJ(String strCPF_CNPJ) {
		this.strCPF_CNPJ = strCPF_CNPJ;
	}

	public String getBlnInteComl() {
		return blnInteComl;
	}

	public void setBlnInteComl(String blnInteComl) {
		this.blnInteComl = blnInteComl;
	}

	public String getStrIPCliente() {
		return strIPCliente;
	}

	public void setStrIPCliente(String strIPCliente) {
		this.strIPCliente = strIPCliente;
	}

	public String getStrIPFraude() {
		return strIPFraude;
	}

	public void setStrIPFraude(String strIPFraude) {
		this.strIPFraude = strIPFraude;
	}

	public String getBlnCrtoSegr() {
		return blnCrtoSegr;
	}

	public void setBlnCrtoSegr(String blnCrtoSegr) {
		this.blnCrtoSegr = blnCrtoSegr;
	}

	public String getStrDtSoliCrto() {
		return strDtSoliCrto;
	}

	public void setStrDtSoliCrto(String strDtSoliCrto) {
		this.strDtSoliCrto = strDtSoliCrto;
	}

	public String getStrDtCancCrto() {
		return strDtCancCrto;
	}

	public void setStrDtCancCrto(String strDtCancCrto) {
		this.strDtCancCrto = strDtCancCrto;
	}

	public String getStrDtAtivCrto() {
		return strDtAtivCrto;
	}

	public void setStrDtAtivCrto(String strDtAtivCrto) {
		this.strDtAtivCrto = strDtAtivCrto;
	}

	public String getStrDtFrauCrto() {
		return strDtFrauCrto;
	}

	public void setStrDtFrauCrto(String strDtFrauCrto) {
		this.strDtFrauCrto = strDtFrauCrto;
	}

	public int getIntInfrPopup() {
		return intInfrPopup;
	}

	public void setIntInfrPopup(int intInfrPopup) {
		this.intInfrPopup = intInfrPopup;
	}

	public String getStrNrQntdEven() {
		return strNrQntdEven;
	}

	public void setStrNrQntdEven(String strNrQntdEven) {
		this.strNrQntdEven = strNrQntdEven;
	}

	public String getStrCdCentroOrigem() {
		return strCdCentroOrigem;
	}

	public void setStrCdCentroOrigem(String strCdCentroOrigem) {
		this.strCdCentroOrigem = strCdCentroOrigem;
	}

	public String getStrCdCentroDestino() {
		return strCdCentroDestino;
	}

	public void setStrCdCentroDestino(String strCdCentroDestino) {
		this.strCdCentroDestino = strCdCentroDestino;
	}

	public String getStrCdCentroOperante() {
		return strCdCentroOperante;
	}

	public void setStrCdCentroOperante(String strCdCentroOperante) {
		this.strCdCentroOperante = strCdCentroOperante;
	}

	public String getStrContabilizado() {
		return strContabilizado;
	}

	public void setStrContabilizado(String strContabilizado) {
		this.strContabilizado = strContabilizado;
	}
	
	public String getStrErro() {
		return strErro;
	}

	public void setStrErro(String strErro) {
		this.strErro = strErro;
	}
	
}