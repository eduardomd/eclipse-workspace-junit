package com.altec.bsbr.app.hyb.dto;

public class CnalOrigModel {
	private String codigo;
	private String nome;
	private String exclusao;

	public CnalOrigModel(String codigo, String nome, String exclusao) {
		this.codigo = codigo;
		this.nome = nome;
		this.exclusao = exclusao;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getExclusao() {
		return exclusao;
	}

	public void setExclusao(String exclusao) {
		this.exclusao = exclusao;
	}

}
