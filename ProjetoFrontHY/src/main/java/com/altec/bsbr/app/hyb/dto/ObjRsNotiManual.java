package com.altec.bsbr.app.hyb.dto;

import javax.faces.bean.ManagedBean;

public class ObjRsNotiManual {
	private String NR_SEQU_NOTI_MANL_ENVI;
	private String IN_ENVI_RETN;
	private String DATA;
	private String NM_RECU_OCOR_ESPC;
	private String NR_SEQU_ORDE_SERV;
	private String IN_PRZ_ESTR;
	private String DATA_RET;
	private String CD_SITU_ENVI;
	private String CD_AREA;
	
	public ObjRsNotiManual() {}
	
	public ObjRsNotiManual(String nR_SEQU_NOTI_MANL_ENVI, String iN_ENVI_RETN, String dATA, String nM_RECU_OCOR_ESPC,
			String nR_SEQU_ORDE_SERV, String iN_PRZ_ESTR, String dATA_RET, String cD_SITU_ENVI, String cD_AREA) {
		super();
		NR_SEQU_NOTI_MANL_ENVI = nR_SEQU_NOTI_MANL_ENVI;
		IN_ENVI_RETN = iN_ENVI_RETN;
		DATA = dATA;
		NM_RECU_OCOR_ESPC = nM_RECU_OCOR_ESPC;
		NR_SEQU_ORDE_SERV = nR_SEQU_ORDE_SERV;
		IN_PRZ_ESTR = iN_PRZ_ESTR;
		DATA_RET = dATA_RET;
		CD_SITU_ENVI = cD_SITU_ENVI;
		CD_AREA = cD_AREA;
	}

	public String getNR_SEQU_NOTI_MANL_ENVI() {
		return NR_SEQU_NOTI_MANL_ENVI;
	}

	public void setNR_SEQU_NOTI_MANL_ENVI(String nR_SEQU_NOTI_MANL_ENVI) {
		NR_SEQU_NOTI_MANL_ENVI = nR_SEQU_NOTI_MANL_ENVI;
	}

	public String getIN_ENVI_RETN() {
		return IN_ENVI_RETN;
	}

	public void setIN_ENVI_RETN(String iN_ENVI_RETN) {
		IN_ENVI_RETN = iN_ENVI_RETN;
	}

	public String getDATA() {
		return DATA;
	}

	public void setDATA(String dATA) {
		DATA = dATA;
	}

	public String getNM_RECU_OCOR_ESPC() {
		return NM_RECU_OCOR_ESPC;
	}

	public void setNM_RECU_OCOR_ESPC(String nM_RECU_OCOR_ESPC) {
		NM_RECU_OCOR_ESPC = nM_RECU_OCOR_ESPC;
	}

	public String getNR_SEQU_ORDE_SERV() {
		return NR_SEQU_ORDE_SERV;
	}

	public void setNR_SEQU_ORDE_SERV(String nR_SEQU_ORDE_SERV) {
		NR_SEQU_ORDE_SERV = nR_SEQU_ORDE_SERV;
	}

	public String getIN_PRZ_ESTR() {
		return IN_PRZ_ESTR;
	}

	public void setIN_PRZ_ESTR(String iN_PRZ_ESTR) {
		IN_PRZ_ESTR = iN_PRZ_ESTR;
	}

	public String getDATA_RET() {
		return DATA_RET;
	}

	public void setDATA_RET(String dATA_RET) {
		DATA_RET = dATA_RET;
	}

	public String getCD_SITU_ENVI() {
		return CD_SITU_ENVI;
	}

	public void setCD_SITU_ENVI(String cD_SITU_ENVI) {
		CD_SITU_ENVI = cD_SITU_ENVI;
	}

	public String getCD_AREA() {
		return CD_AREA;
	}

	public void setCD_AREA(String cD_AREA) {
		CD_AREA = cD_AREA;
	}
	
	
}
