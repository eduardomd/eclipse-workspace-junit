package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import com.altec.bsbr.app.hyb.dto.Hyb_excel_pesquisa_resultado_model;

@ManagedBean(name="hyb_excel_pesquisa_resultado_bean")
@ViewScoped
public class Hyb_excel_pesquisa_resultado_bean {
	
	private Object strXml = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strXml");
	private String strMsgErro;
	
	private List<Hyb_excel_pesquisa_resultado_model> objRs;
	
	public Hyb_excel_pesquisa_resultado_bean() {
		this.setStrMsgErro("s");
		/*this.pageLoad();*/
		/* Setando a str de erro para teste */
		this.objRs = new ArrayList<Hyb_excel_pesquisa_resultado_model>();
		
		for(int i = 0; i < 3; i++) {
			Hyb_excel_pesquisa_resultado_model excelModel = new Hyb_excel_pesquisa_resultado_model();
			excelModel.setCODIGO("Teste1");
			excelModel.setCD_SITUACAO("Teste2");
			excelModel.setNM_SITUACAO("Teste3");
			excelModel.setABERTURA("Teste4");
			excelModel.setAREA("Teste5");
			excelModel.setNOME_AN_DESIG("Teste6");
			excelModel.setVALOR_ENVOLVIDO("Teste7");
			excelModel.setPREJUIZO("Teste8");
			excelModel.setVALOR_RECUP("Teste9");
			excelModel.setCUSTO_OS("Teste10");
			excelModel.setNM_PV("Teste11");
			excelModel.setNM_REGI("Teste12");
			excelModel.setNM_REDE("Teste13");
			excelModel.setEVENTO("Teste14");
			excelModel.setCANAL("Teste15");
			excelModel.setOPERACAO("Teste16");
			this.objRs.add(excelModel);
		}
	}

	public Hyb_excel_pesquisa_resultado_bean(List<Hyb_excel_pesquisa_resultado_model> objRs) {
		this.objRs = objRs;
	}

	public List<Hyb_excel_pesquisa_resultado_model> getObjRs() {
		return objRs;
	}

	public void setObjRs(List<Hyb_excel_pesquisa_resultado_model> objRs) {
		this.objRs = objRs;
	}
	
	public String getStrMsgErro() {
		return strMsgErro;
	}

	public void setStrMsgErro(String strMsgErro) {
		this.strMsgErro = strMsgErro;
	}
	
	@PostConstruct
	public void init() {
		/* VB component
		set objPesquisa = server.CreateObject ("xHY_Pesquisa.clsPesquisa")
	    set objRs = objPesquisa.PesquisarOsExcel(strXml, strMsgErro) */
		if (!this.strMsgErro.equals("")) {
			// @TODO - Ajustar o redirect (bate na exception quando chega no ec.redirect comentado abaixo)
/*			try {
				ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
				ec.redirect("hy_erro.xhtml?strErro=" + this.strMsgErro);
			} catch(IOException e) {
				e.printStackTrace();
			} */
		}
		
	}
	
	public void redirect() throws IOException {
		if (objRs.size() > 0) {
		    ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
		    externalContext.redirect("hyb_excel_pesquisa_resultado.xhtml");
		} else {
			RequestContext.getCurrentInstance().execute("alert('Nao existe dados para essa consulta!');history.back();");
		}
	}

}
	
	

