package com.altec.bsbr.app.hyb.web.jsf;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.web.util.XHYUsuarioIncService;

@Component("aprovarOSBean")
@Scope("request")
public class AprovarOSBean {
	
	private String strNrSeqOs;

	@Autowired 
	private XHYUsuarioIncService userServ;
	
	@PostConstruct
	public void init() {		
		strNrSeqOs = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs");
	}	

	public void verificarPermissao(ComponentSystemEvent event) {
		String[] perfis = {"SUPERINT","GER","COORD"};
		
		userServ.verificarPermissao(perfis);
	}
	
	public Object getStrNrSeqOs() {
		return strNrSeqOs;
	}
	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}	
	
}
