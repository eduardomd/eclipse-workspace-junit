package com.altec.bsbr.app.hyb.web.jsf;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.apache.commons.lang.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.CadastroOsDetalheEnvolvFunc;
import com.altec.bsbr.app.hyb.dto.ObjRsOsAssociada;
import com.altec.bsbr.app.hyb.web.util.rsRest;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsParticip.XHYCadOsParticipEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;

@Component("CadastroosDetalheEnvolvFuncBean")
@Scope("session")
public class CadastroosDetalheenvolvfuncBean extends BasicBBean{

	private static final long serialVersionUID = 1L;
	
	private CadastroOsDetalheEnvolvFunc cadastroOsDetalheEnvolvFunc;
	
	private String iCountLinhas;
	private String Request;
	private List<rsRest> objRsRest;
	private String NrSeqPart;
	private List<ObjRsOsAssociada> objRsOsAssociadas;
	private Locale ptBR = new Locale("pt", "BR");
	
	public String getRequest() {
		return Request;
	}

	public void setRequest(String request) {
		Request = request;
	}
	
	@Autowired
	private XHYCadOsParticipEndPoint cadOsParticipEndPoint;
	
	@PostConstruct
	public void init() {
		this.NrSeqPart = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("NrSeqPart");
		consultarDetalheParticipacao(Double.parseDouble(this.NrSeqPart));
		consultarRestricaoPart(Double.parseDouble(this.NrSeqPart));
		fillTblOsAssociada(cadastroOsDetalheEnvolvFunc.getCpf_cnpj(), cadastroOsDetalheEnvolvFunc.getOs());
		
	}
	
	public void fillTblOsAssociada(String cpf_cnpj, String os) {
		try {
			System.out.println("fillTblOsAssociada ->");
			objRsOsAssociadas = new ArrayList<ObjRsOsAssociada>();
			JSONObject json = new JSONObject(cadOsParticipEndPoint.consultarOSAssociada(cpf_cnpj));
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			
			for (int i = 0; i < pcursor.length(); i++) {
				JSONObject item = pcursor.getJSONObject(i);
				ObjRsOsAssociada OsAssociadaItem = new ObjRsOsAssociada();
				OsAssociadaItem.setORDEM(item.isNull("ORDEM")? "" : item.get("ORDEM").toString());
				OsAssociadaItem.setAREA(item.isNull("AREA")? "" : item.get("AREA").toString());
				OsAssociadaItem.setCD_STATUS(item.isNull("CD_STATUS")? "" : item.get("CD_STATUS").toString());
				OsAssociadaItem.setSTATUS(item.isNull("STATUS")? "" : item.get("STATUS").toString());

				if(!os.equals(OsAssociadaItem.getORDEM())) {
					objRsOsAssociadas.add(OsAssociadaItem);
				}
			}
			System.out.println(objRsOsAssociadas.size());
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void consultarRestricaoPart(Double NrSeqPart) {
		
		try {
			objRsRest = new ArrayList<rsRest>();
			
			JSONObject json = new JSONObject(cadOsParticipEndPoint.consultarRestricaoPart(NrSeqPart));
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			
			for (int i = 0; i < pcursor.length(); i++) {
				JSONObject item = pcursor.getJSONObject(i);
				rsRest objRsRestItem = new rsRest();				
				objRsRestItem.setDATA(item.isNull("DATA")? "" : item.get("DATA").toString());
				objRsRestItem.setESPECIFICACAO(item.isNull("ESPECIFICACAO")? "" : item.get("ESPECIFICACAO").toString());
				objRsRestItem.setINSTITUICAO(item.isNull("INSTITUICAO")? "" : item.get("INSTITUICAO").toString());
				objRsRestItem.setSQ_RESTRICAO(item.isNull("SQ_RESTRICAO")? "" : item.get("SQ_RESTRICAO").toString());
				objRsRestItem.setVALOR(item.isNull("VALOR")? "" : item.getBigDecimal("VALOR").toString());
		
				if(!objRsRestItem.getVALOR().isEmpty()) {
					objRsRestItem.setVALOR(formataValorRecebido(objRsRestItem.getVALOR()));
				}
				this.objRsRest.add(objRsRestItem);
			}
			System.out.println(objRsRest.size() + " size");
					
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void consultarDetalheParticipacao(Double NrSeqPart) {
		System.out.println("--------->>");
		try {
			JSONObject json = new JSONObject(cadOsParticipEndPoint.consultarDetalhePart(NrSeqPart));
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			
			if(pcursor.length() > 0) {
				cadastroOsDetalheEnvolvFunc = new CadastroOsDetalheEnvolvFunc();
				JSONObject item = pcursor.getJSONObject(0);
				cadastroOsDetalheEnvolvFunc.setMatricula(item.isNull("MATRICULA")? "": item.get("MATRICULA").toString());
				cadastroOsDetalheEnvolvFunc.setNome(item.isNull("NOME")? "": item.get("NOME").toString());
				cadastroOsDetalheEnvolvFunc.setOs(item.isNull("OS")? "": item.get("OS").toString());
				cadastroOsDetalheEnvolvFunc.setCpf_cnpj(item.isNull("CPF_CNPJ")? "": item.get("CPF_CNPJ").toString());
				cadastroOsDetalheEnvolvFunc.setDepartamento(item.isNull("DEPARTAMENTO")? "": item.get("DEPARTAMENTO").toString());
				cadastroOsDetalheEnvolvFunc.setSuperintendencia(item.isNull("SUPERINTENDENCIA")? "": item.get("SUPERINTENDENCIA").toString());
				cadastroOsDetalheEnvolvFunc.setSup_imediato(item.isNull("SUP_IMEDIATO")? "": item.get("SUP_IMEDIATO").toString());
				cadastroOsDetalheEnvolvFunc.setStatus(item.isNull("STATUS")? "": item.get("STATUS").toString());
				cadastroOsDetalheEnvolvFunc.setEndereco(item.isNull("ENDERECO")? "": item.get("ENDERECO").toString());
				cadastroOsDetalheEnvolvFunc.setNumero(item.isNull("NUMERO")? "": item.get("NUMERO").toString());
				cadastroOsDetalheEnvolvFunc.setComplemento(item.isNull("COMPLEMENTO")? "": item.get("COMPLEMENTO").toString());
				cadastroOsDetalheEnvolvFunc.setBairro(item.isNull("BAIRRO")? "": item.get("BAIRRO").toString());
				cadastroOsDetalheEnvolvFunc.setCep(item.isNull("CEP")? "": item.get("CEP").toString());
				cadastroOsDetalheEnvolvFunc.setTelefone(item.isNull("TELEFONE")? "": item.get("TELEFONE").toString());
				cadastroOsDetalheEnvolvFunc.setCelular(item.isNull("CELULAR")? "": item.get("CELULAR").toString());
				
				cadastroOsDetalheEnvolvFunc.setOper_vencer(item.isNull("OPER_VENCER")? "": item.getBigDecimal("OPER_VENCER").toString());
				cadastroOsDetalheEnvolvFunc.setOper_vencida(item.isNull("OPER_VENCIDA")? "": item.getBigDecimal("OPER_VENCIDA").toString());
				cadastroOsDetalheEnvolvFunc.setOper_creli(item.isNull("OPER_CRELI")? "": item.getBigDecimal("OPER_CRELI").toString());
				cadastroOsDetalheEnvolvFunc.setOper_preju(item.isNull("OPER_PREJU")? "": item.getBigDecimal("OPER_PREJU").toString());
				cadastroOsDetalheEnvolvFunc.setOper_passiva(item.isNull("OPER_PASSIVA")? "": item.getBigDecimal("OPER_PASSIVA").toString());
				cadastroOsDetalheEnvolvFunc.setReciprocidade(item.isNull("RECIPROCIDADE")? "": item.getBigDecimal("RECIPROCIDADE").toString());
				
				cadastroOsDetalheEnvolvFunc.setAgencia(item.isNull("AGENCIA")? "": item.get("AGENCIA").toString());
				cadastroOsDetalheEnvolvFunc.setRegional(item.isNull("REGIONAL")? "": item.get("REGIONAL").toString());
				cadastroOsDetalheEnvolvFunc.setRede(item.isNull("REDE")? "": item.get("REDE").toString());
				cadastroOsDetalheEnvolvFunc.setStrTpPart(item.isNull("TP_PARTICIPACAO")? "": item.get("TP_PARTICIPACAO").toString());
				cadastroOsDetalheEnvolvFunc.setParticipacao(item.isNull("PARTICIPACAO")? "": item.get("PARTICIPACAO").toString());
				cadastroOsDetalheEnvolvFunc.setDt_abertura(item.isNull("DT_ABERTURA")? "": item.get("DT_ABERTURA").toString());
								
				if(!cadastroOsDetalheEnvolvFunc.getOper_vencer().isEmpty()) {
					cadastroOsDetalheEnvolvFunc.setOper_vencer(formataValorRecebido(cadastroOsDetalheEnvolvFunc.getOper_vencer()));
				}
				if(!cadastroOsDetalheEnvolvFunc.getOper_vencida().isEmpty()) {
					cadastroOsDetalheEnvolvFunc.setOper_vencida(formataValorRecebido(cadastroOsDetalheEnvolvFunc.getOper_vencida()));
				}
				if(!cadastroOsDetalheEnvolvFunc.getOper_creli().isEmpty()) {
					cadastroOsDetalheEnvolvFunc.setOper_creli(formataValorRecebido(cadastroOsDetalheEnvolvFunc.getOper_creli()));
				}
				if(!cadastroOsDetalheEnvolvFunc.getOper_preju().isEmpty()) {
					cadastroOsDetalheEnvolvFunc.setOper_preju(formataValorRecebido(cadastroOsDetalheEnvolvFunc.getOper_preju()));
				}
				if(!cadastroOsDetalheEnvolvFunc.getOper_passiva().isEmpty()) {
					cadastroOsDetalheEnvolvFunc.setOper_passiva(formataValorRecebido(cadastroOsDetalheEnvolvFunc.getOper_passiva()));
				}
				if(!cadastroOsDetalheEnvolvFunc.getReciprocidade().isEmpty()) {
					cadastroOsDetalheEnvolvFunc.setReciprocidade(formataValorRecebido(cadastroOsDetalheEnvolvFunc.getReciprocidade()));
				}
				
				cadastroOsDetalheEnvolvFunc.setCep(StringUtils.leftPad(cadastroOsDetalheEnvolvFunc.getCep(), 8, "0"));
				cadastroOsDetalheEnvolvFunc.setCep(cadastroOsDetalheEnvolvFunc.getCep().substring(0, 5) + "-" + cadastroOsDetalheEnvolvFunc.getCep().substring(5, 8));
				
				cadastroOsDetalheEnvolvFunc.setTelefone(StringUtils.leftPad(cadastroOsDetalheEnvolvFunc.getTelefone(), 11, "0"));
				cadastroOsDetalheEnvolvFunc.setCelular(StringUtils.leftPad(cadastroOsDetalheEnvolvFunc.getCelular(), 11, "0"));

				cadastroOsDetalheEnvolvFunc.setDddTelefone(cadastroOsDetalheEnvolvFunc.getTelefone().substring(0, 2));
				cadastroOsDetalheEnvolvFunc.setNumeroTelefone(cadastroOsDetalheEnvolvFunc.getTelefone().substring(2, 11));
				
				cadastroOsDetalheEnvolvFunc.setDddCelular(cadastroOsDetalheEnvolvFunc.getCelular().substring(0, 2));
				cadastroOsDetalheEnvolvFunc.setNumeroCelular(cadastroOsDetalheEnvolvFunc.getCelular().substring(2, 11));
						

			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String formataValorParaEnvio(String valorRecebido) {
		String valor = valorRecebido.replace(",", "").replace(".", "");
		int size = valor.length() - 2;
		return valor.substring(0, size).concat(".").concat(valor.substring(size, valor.length()));
	}
	
	public String formataValorRecebido(String valorRecebido) {
		String valor = NumberFormat.getCurrencyInstance(ptBR).format(Double.parseDouble(valorRecebido));
		return valor.replace("R$", "").trim();
	}
	
	
	public void salvar() {
		System.out.println("chamou o salvar -->>");
		try {
			
			System.out.println(this.NrSeqPart +","+ this.cadastroOsDetalheEnvolvFunc.getEndereco()+","+ this.cadastroOsDetalheEnvolvFunc.getNumero()+","+ this.cadastroOsDetalheEnvolvFunc.getComplemento()+","+ this.cadastroOsDetalheEnvolvFunc.getBairro()+","+ this.cadastroOsDetalheEnvolvFunc.getCep().replace("-", "")+","+this.cadastroOsDetalheEnvolvFunc.getTelefone()+","+ this.cadastroOsDetalheEnvolvFunc.getCelular());
			if(cadOsParticipEndPoint.atualizarEnderecoParticipante(this.NrSeqPart, this.cadastroOsDetalheEnvolvFunc.getEndereco(), this.cadastroOsDetalheEnvolvFunc.getNumero(), this.cadastroOsDetalheEnvolvFunc.getComplemento(), this.cadastroOsDetalheEnvolvFunc.getBairro(), this.cadastroOsDetalheEnvolvFunc.getCep().replace("-", ""), "", "", this.cadastroOsDetalheEnvolvFunc.getDddTelefone() + this.cadastroOsDetalheEnvolvFunc.getNumeroTelefone(), this.cadastroOsDetalheEnvolvFunc.getDddCelular() + this.cadastroOsDetalheEnvolvFunc.getNumeroCelular())) {
				System.out.println(this.NrSeqPart+","+ this.cadastroOsDetalheEnvolvFunc.getOper_vencer().replace(".", "") +","+ this.cadastroOsDetalheEnvolvFunc.getOper_vencida().replace(".", "")+","+ this.cadastroOsDetalheEnvolvFunc.getOper_creli().replace(".", "")+","+ this.cadastroOsDetalheEnvolvFunc.getOper_preju().replace(".", "")+","+ this.cadastroOsDetalheEnvolvFunc.getOper_passiva().replace(".", "")+","+ this.cadastroOsDetalheEnvolvFunc.getReciprocidade().replace(".", ""));
				if(cadOsParticipEndPoint.fnUpdposicaoRisco(this.NrSeqPart, this.cadastroOsDetalheEnvolvFunc.getOper_vencer().replace(".", ""), this.cadastroOsDetalheEnvolvFunc.getOper_vencida().replace(".", ""), this.cadastroOsDetalheEnvolvFunc.getOper_creli().replace(".", ""), this.cadastroOsDetalheEnvolvFunc.getOper_preju().replace(".", ""), this.cadastroOsDetalheEnvolvFunc.getOper_passiva().replace(".", ""), this.cadastroOsDetalheEnvolvFunc.getReciprocidade().replace(".", ""))) {
					
					System.out.println("Chamar for");
					SimpleDateFormat newFormat = new SimpleDateFormat("dd/MM/yyyy");
					SimpleDateFormat newFormat2 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
					
					for (int i = 0; i < this.objRsRest.size(); i++) {
						this.objRsRest.get(i).getSQ_RESTRICAO();
						this.objRsRest.get(i).getINSTITUICAO();
						this.objRsRest.get(i).getESPECIFICACAO();
						this.objRsRest.get(i).getVALOR();
												
						System.out.println(this.objRsRest.get(i).getVALOR());

						if(this.objRsRest.get(i).getSQ_RESTRICAO().isEmpty()) {

							if(!this.objRsRest.get(i).getDATA().isEmpty()) {
								Date data = newFormat.parse(this.objRsRest.get(i).getDATA());
								this.objRsRest.get(i).setDATA(newFormat2.format(data));
							}
							
							if(!this.objRsRest.get(i).getVALOR().isEmpty()) {
								this.objRsRest.get(i).setVALOR(formataValorParaEnvio(this.objRsRest.get(i).getVALOR()));
							}
							
							System.out.println(Integer.valueOf(this.NrSeqPart)+" ,"+ this.objRsRest.get(i).getINSTITUICAO()+" ,"+ this.objRsRest.get(i).getESPECIFICACAO()+" ,"+ this.objRsRest.get(i).getVALOR()+" ,"+ this.objRsRest.get(i).getDATA());
							cadOsParticipEndPoint.inserirRestricaoParticipante(Integer.valueOf(this.NrSeqPart), this.objRsRest.get(i).getINSTITUICAO(), this.objRsRest.get(i).getESPECIFICACAO(), this.objRsRest.get(i).getVALOR(), this.objRsRest.get(i).getDATA());
						}else {
							System.out.println(this.objRsRest.get(i).getSQ_RESTRICAO() +" ,"+ this.objRsRest.get(i).getINSTITUICAO()+" ,"+ this.objRsRest.get(i).getESPECIFICACAO()+" ,"+ this.objRsRest.get(i).getVALOR().replace(".", "")+" ,"+ this.objRsRest.get(i).getDATA());
							cadOsParticipEndPoint.fnUpdRestrParticipante(this.objRsRest.get(i).getSQ_RESTRICAO(), this.objRsRest.get(i).getINSTITUICAO(), this.objRsRest.get(i).getESPECIFICACAO(), this.objRsRest.get(i).getVALOR().replace(".", ""), this.objRsRest.get(i).getDATA());
						}
					}

					RequestContext.getCurrentInstance().execute("alert('Registro(s) atualizado(s) com sucesso.')");
					
					consultarRestricaoPart(Double.parseDouble(this.NrSeqPart));
					
				}else
					RequestContext.getCurrentInstance().execute("alert('Não foi possível atualizar o(s) registro(s).')");	
			}else
				RequestContext.getCurrentInstance().execute("alert('Não foi possível atualizar o(s) registro(s).')");
			
		} catch (Exception e) {
			e.printStackTrace();
			RequestContext.getCurrentInstance().execute("alert('Não foi possível atualizar o(s) registro(s).')");
		}
	}
	
	public void insereLinha() {
		objRsRest.add(new rsRest("", "", "", "", "") );
	}
	
	public void fnExcluiRestricao() {
		
		String strIdRestPart = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("sqRestricao");
		
		try {
			System.out.println("fnExcluiRestricao ---->>" + strIdRestPart);
			cadOsParticipEndPoint.excluirRestricaoPart(Double.parseDouble(strIdRestPart));
			consultarRestricaoPart(Double.parseDouble(this.NrSeqPart));
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public CadastroOsDetalheEnvolvFunc getCadastroOsDetalheEnvolvFunc() {
		return cadastroOsDetalheEnvolvFunc;
	}

	public void setCadastroOsDetalheEnvolvFunc(CadastroOsDetalheEnvolvFunc cadastroOsDetalheEnvolvFunc) {
		this.cadastroOsDetalheEnvolvFunc = cadastroOsDetalheEnvolvFunc;
	}

	public List<rsRest> getObjRsRest() {
		return objRsRest;
	}

	public void setObjRsRest(List<rsRest> objRsRest) {
		this.objRsRest = objRsRest;
	}

	public String getNrSeqPart() {
		return NrSeqPart;
	}

	public void setNrSeqPart(String nrSeqPart) {
		NrSeqPart = nrSeqPart;
	}

	public List<ObjRsOsAssociada> getObjRsOsAssociadas() {
		return objRsOsAssociadas;
	}

	public void setObjRsOsAssociadas(List<ObjRsOsAssociada> objRsOsAssociadas) {
		this.objRsOsAssociadas = objRsOsAssociadas;
	}
	
}
