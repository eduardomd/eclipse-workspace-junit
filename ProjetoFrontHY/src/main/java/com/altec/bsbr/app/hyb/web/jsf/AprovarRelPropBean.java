package com.altec.bsbr.app.hyb.web.jsf;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;

import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.ObjRsEquipe;
import com.altec.bsbr.app.hyb.dto.ObjRsOs;
import com.altec.bsbr.app.hyb.dto.ObjRsParticipante;
import com.altec.bsbr.app.hyb.web.util.XHYUsuarioIncService;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAcoesOs.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAcoesOs.XHYAcoesOsEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;

@Component("aprovarRelPropBean")
@Scope("session")
public class AprovarRelPropBean extends BasicBBean {

    @Autowired
    private XHYAcoesOsEndPoint acoesOs;
 
	private static final long serialVersionUID = 1L;
	private ObjRsOs objRsOs = new ObjRsOs();
	private List<ObjRsParticipante> objRsParticipantes;
	private List<ObjRsEquipe> objRsEquipes;
	private String strNrSeqOs = "";

	@Autowired 
	private XHYUsuarioIncService userServ;
	
	@PostConstruct
	public void init() {
		carregarNrSeqOs();
	}

	public void verificarPermissao(ComponentSystemEvent event) {
		String[] perfis = {"SUPERINT","GER"};
		
		userServ.verificarPermissao(perfis);
	}
	
	private void carregarNrSeqOs() {
		strNrSeqOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs");
	}
	
	public ObjRsOs getObjRsOs() {
		
		carregarNrSeqOs();
		String retorno = "";
		try {
			strNrSeqOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs");
			retorno = acoesOs.consultarOS(strNrSeqOs);
			
			JSONObject consultaParticipanteOS = new JSONObject(retorno);
			JSONArray pcursor = consultaParticipanteOS.getJSONArray("PCURSOR");
			for (int i = 0; i < pcursor.length(); i++) {
				JSONObject f = pcursor.getJSONObject(i);
				
				objRsOs.setAREA(f.isNull("AREA") == true? "" : f.get("AREA").toString() );
				objRsOs.setCD_NOTI(f.isNull("CD_NOTI") == true? "" : f.get("CD_NOTI").toString());
				objRsOs.setCODIGO(f.isNull("CODIGO") == true? "" : f.get("CODIGO").toString());
				objRsOs.setDT_ABERTURA(f.isNull("DT_ABERTURA") == true? "" : f.get("DT_ABERTURA").toString());
				objRsOs.setDT_RELATORIO(f.isNull("DT_RELATORIO") == true? "" : f.get("DT_RELATORIO").toString());
				objRsOs.setNM_NOTI(f.isNull("NM_NOTI") == true? "" : f.get("NM_NOTI").toString());
				objRsOs.setPARECER_JURI(f.isNull("PARECER_JURI") == true? "" : f.get("PARECER_JURI").toString());
			
				objRsOs.setSITUACAO(f.isNull("SITUACAO") == true? "" : f.get("SITUACAO").toString());
				objRsOs.setTX_ABERTURA(f.isNull("DT_ABERTURA") == true? "" : f.get("DT_ABERTURA").toString());
				objRsOs.setTX_ENCERRAMENTO(f.isNull("TX_ENCERRAMENTO") == true? "" : f.get("TX_ENCERRAMENTO").toString());
				objRsOs.setTX_FALHAS(f.isNull("TX_FALHAS") == true? "" : f.get("TX_FALHAS").toString());
				objRsOs.setTX_OUTRAS_PROP(f.isNull("TX_OUTRAS_PROP") == true? "" : f.get("TX_OUTRAS_PROP").toString());
				objRsOs.setVL_ENVOLVIDO(f.isNull("VL_ENVOLVIDO") == true? "" : f.get("VL_ENVOLVIDO").toString());
				objRsOs.setVL_PREJUIZO(f.isNull("VL_PREJUIZO") == true? "" : f.get("VL_PREJUIZO").toString());
				objRsOs.setVL_RECUPERADO(f.isNull("VL_RECUPERADO") == true? "" : f.get("VL_RECUPERADO").toString());

			}

		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return objRsOs;
	}

	public void aprovarRelatorio() {
		//TODO: fazer chamada com backend para aprovar
		
		carregarNrSeqOs();
		String retorno = "";
		try {
						
			retorno = acoesOs.incluirProxWorkFlow(strNrSeqOs, "E", "4020", "RP", "", false);
			
			retorno = acoesOs.incluirProxWorkFlow(strNrSeqOs, "E", "5010", "", "", false);

			RequestContext.getCurrentInstance().execute("alert('\"Relatório de Proposiçăo aprovado com sucesso. A notificaçăo foi enviada aos responsáveis!\"')");
			

		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
	}

	public List<ObjRsParticipante> getObjRsParticipantes() {
		// lista de ObjRsParticipante
		
		carregarNrSeqOs();
		String retorno = "";
		try {
						
			retorno = acoesOs.consultarParticipanteOs(strNrSeqOs);
			
			objRsParticipantes = new ArrayList<ObjRsParticipante>();
			
			JSONObject consultarParticipanteOs = new JSONObject(retorno);
			JSONArray pcursor = consultarParticipanteOs.getJSONArray("PCURSOR");
			
			
			for (int i = 0; i < pcursor.length(); i++) {
				JSONObject f = pcursor.getJSONObject(i);
				
				objRsParticipantes.add(new ObjRsParticipante(f.get("POSICAO").toString(), f.get("NOME").toString(), 
									                         f.get("CPF_CNPJ").toString(), f.get("DESC_TIPO").toString(), 
									                         f.get("PENALIDADE").toString(), f.get("MOTIVO").toString()));

			}

		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return objRsParticipantes;
	}

	public List<ObjRsEquipe> getObjRsEquipes() {
		// lista de ObjRsEquipe
		
		carregarNrSeqOs();
		objRsEquipes = new ArrayList<ObjRsEquipe>();
		
		String retorno = "";
		try {
			retorno = acoesOs.consultarEquipeOs(strNrSeqOs, "");
			
			JSONObject consultaEquipeOS = new JSONObject(retorno);
			JSONArray pcursor = consultaEquipeOS.getJSONArray("PCURSOR");
			
			for (int i = 0; i < pcursor.length(); i++) {
				JSONObject f = pcursor.getJSONObject(i);
				if(f.get("TP_DESI").toString() == "AD" || f.get("TP_DESI").toString() == "AA" || f.get("TP_DESI").toString() == "PSD" || 
				   f.get("TP_DESI").toString() == "PSA" || f.get("TP_DESI").toString() == "NA") {
					
					objRsEquipes.add(new ObjRsEquipe(f.get("NR_MATR_PRSV").toString(), f.get("NM_RECU_OCOR_ESPC").toString(), f.get("NM_DESIGNACAO").toString() , f.get("TP_DESI").toString() ,f.get("CD_CARG").toString() ));
				}
							}
		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return objRsEquipes;
	}

}
