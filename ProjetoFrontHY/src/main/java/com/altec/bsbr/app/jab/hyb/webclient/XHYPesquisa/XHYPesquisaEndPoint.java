//
// Generated By:JAX-WS RI 2.2.9-b130926.1035 (JAXB RI IBM 2.2.8-b130911.1802)
//


package com.altec.bsbr.app.jab.hyb.webclient.XHYPesquisa;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;

@WebService(name = "xHY_PesquisaEndPoint", targetNamespace = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/")
@SOAPBinding(style = SOAPBinding.Style.RPC)
@XmlSeeAlso({
    ObjectFactory.class
})
public interface XHYPesquisaEndPoint {


    /**
     * 
     * @return
     *     returns java.lang.String
     * @throws WebServiceException
     */
    @WebMethod(operationName = "ConsultarSituacaoRecurso")
    @WebResult(partName = "return")
    @Action(input = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/ConsultarSituacaoRecursoRequest", output = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/ConsultarSituacaoRecursoResponse", fault = {
        @FaultAction(className = WebServiceException.class, value = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/ConsultarSituacaoRecurso/Fault/WebServiceException")
    })
    public String consultarSituacaoRecurso()
        throws WebServiceException
    ;

    /**
     * 
     * @return
     *     returns java.lang.String
     * @throws WebServiceException
     */
    @WebMethod(operationName = "ConsultarCanalOrigem")
    @WebResult(partName = "return")
    @Action(input = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/ConsultarCanalOrigemRequest", output = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/ConsultarCanalOrigemResponse", fault = {
        @FaultAction(className = WebServiceException.class, value = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/ConsultarCanalOrigem/Fault/WebServiceException")
    })
    public String consultarCanalOrigem()
        throws WebServiceException
    ;

    /**
     * 
     * @return
     *     returns java.lang.String
     * @throws WebServiceException
     */
    @WebMethod(operationName = "ConsultarEvento")
    @WebResult(partName = "return")
    @Action(input = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/ConsultarEventoRequest", output = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/ConsultarEventoResponse", fault = {
        @FaultAction(className = WebServiceException.class, value = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/ConsultarEvento/Fault/WebServiceException")
    })
    public String consultarEvento()
        throws WebServiceException
    ;

    /**
     * 
     * @return
     *     returns java.lang.String
     * @throws WebServiceException
     */
    @WebMethod(operationName = "ConsultarPCorp")
    @WebResult(partName = "return")
    @Action(input = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/ConsultarPCorpRequest", output = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/ConsultarPCorpResponse", fault = {
        @FaultAction(className = WebServiceException.class, value = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/ConsultarPCorp/Fault/WebServiceException")
    })
    public String consultarPCorp()
        throws WebServiceException
    ;

    /**
     * 
     * @return
     *     returns java.lang.String
     * @throws WebServiceException
     */
    @WebMethod(operationName = "ConsultarSituacaoOs")
    @WebResult(partName = "return")
    @Action(input = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/ConsultarSituacaoOsRequest", output = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/ConsultarSituacaoOsResponse", fault = {
        @FaultAction(className = WebServiceException.class, value = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/ConsultarSituacaoOs/Fault/WebServiceException")
    })
    public String consultarSituacaoOs()
        throws WebServiceException
    ;

    /**
     * 
     * @return
     *     returns java.lang.String
     * @throws WebServiceException
     */
    @WebMethod(operationName = "ConsultarCriticidadeOs")
    @WebResult(partName = "return")
    @Action(input = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/ConsultarCriticidadeOsRequest", output = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/ConsultarCriticidadeOsResponse", fault = {
        @FaultAction(className = WebServiceException.class, value = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/ConsultarCriticidadeOs/Fault/WebServiceException")
    })
    public String consultarCriticidadeOs()
        throws WebServiceException
    ;

    /**
     * 
     * @return
     *     returns java.lang.String
     * @throws WebServiceException
     */
    @WebMethod(operationName = "ConsultarPAux")
    @WebResult(partName = "return")
    @Action(input = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/ConsultarPAuxRequest", output = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/ConsultarPAuxResponse", fault = {
        @FaultAction(className = WebServiceException.class, value = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/ConsultarPAux/Fault/WebServiceException")
    })
    public String consultarPAux()
        throws WebServiceException
    ;

    /**
     * 
     * @return
     *     returns java.lang.String
     * @throws WebServiceException
     */
    @WebMethod(operationName = "ConsultarFaseAnaliseOs")
    @WebResult(partName = "return")
    @Action(input = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/ConsultarFaseAnaliseOsRequest", output = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/ConsultarFaseAnaliseOsResponse", fault = {
        @FaultAction(className = WebServiceException.class, value = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/ConsultarFaseAnaliseOs/Fault/WebServiceException")
    })
    public String consultarFaseAnaliseOs()
        throws WebServiceException
    ;

    /**
     * 
     * @param arg7
     * @param arg6
     * @param arg9
     * @param arg8
     * @param arg28
     * @param arg29
     * @param arg24
     * @param arg25
     * @param arg26
     * @param arg27
     * @param arg20
     * @param arg21
     * @param arg22
     * @param arg23
     * @param arg3
     * @param arg17
     * @param arg2
     * @param arg18
     * @param arg5
     * @param arg19
     * @param arg4
     * @param arg13
     * @param arg14
     * @param arg1
     * @param arg15
     * @param arg0
     * @param arg16
     * @param arg31
     * @param arg10
     * @param arg32
     * @param arg11
     * @param arg12
     * @param arg30
     * @return
     *     returns java.lang.String
     * @throws WebServiceException
     */
    @WebMethod(operationName = "PesquisarOsExcel")
    @WebResult(partName = "return")
    @Action(input = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/PesquisarOsExcelRequest", output = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/PesquisarOsExcelResponse", fault = {
        @FaultAction(className = WebServiceException.class, value = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/PesquisarOsExcel/Fault/WebServiceException")
    })
    public String pesquisarOsExcel(
        @WebParam(name = "arg0", partName = "arg0")
        String arg0,
        @WebParam(name = "arg1", partName = "arg1")
        String arg1,
        @WebParam(name = "arg2", partName = "arg2")
        String arg2,
        @WebParam(name = "arg3", partName = "arg3")
        String arg3,
        @WebParam(name = "arg4", partName = "arg4")
        String arg4,
        @WebParam(name = "arg5", partName = "arg5")
        String arg5,
        @WebParam(name = "arg6", partName = "arg6")
        String arg6,
        @WebParam(name = "arg7", partName = "arg7")
        String arg7,
        @WebParam(name = "arg8", partName = "arg8")
        String arg8,
        @WebParam(name = "arg9", partName = "arg9")
        String arg9,
        @WebParam(name = "arg10", partName = "arg10")
        String arg10,
        @WebParam(name = "arg11", partName = "arg11")
        String arg11,
        @WebParam(name = "arg12", partName = "arg12")
        String arg12,
        @WebParam(name = "arg13", partName = "arg13")
        String arg13,
        @WebParam(name = "arg14", partName = "arg14")
        String arg14,
        @WebParam(name = "arg15", partName = "arg15")
        String arg15,
        @WebParam(name = "arg16", partName = "arg16")
        String arg16,
        @WebParam(name = "arg17", partName = "arg17")
        String arg17,
        @WebParam(name = "arg18", partName = "arg18")
        String arg18,
        @WebParam(name = "arg19", partName = "arg19")
        String arg19,
        @WebParam(name = "arg20", partName = "arg20")
        String arg20,
        @WebParam(name = "arg21", partName = "arg21")
        String arg21,
        @WebParam(name = "arg22", partName = "arg22")
        String arg22,
        @WebParam(name = "arg23", partName = "arg23")
        String arg23,
        @WebParam(name = "arg24", partName = "arg24")
        String arg24,
        @WebParam(name = "arg25", partName = "arg25")
        String arg25,
        @WebParam(name = "arg26", partName = "arg26")
        String arg26,
        @WebParam(name = "arg27", partName = "arg27")
        String arg27,
        @WebParam(name = "arg28", partName = "arg28")
        String arg28,
        @WebParam(name = "arg29", partName = "arg29")
        String arg29,
        @WebParam(name = "arg30", partName = "arg30")
        String arg30,
        @WebParam(name = "arg31", partName = "arg31")
        String arg31,
        @WebParam(name = "arg32", partName = "arg32")
        String arg32)
        throws WebServiceException
    ;

    /**
     * 
     * @return
     *     returns java.lang.String
     * @throws WebServiceException
     */
    @WebMethod(operationName = "ConsultarPenalidade")
    @WebResult(partName = "return")
    @Action(input = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/ConsultarPenalidadeRequest", output = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/ConsultarPenalidadeResponse", fault = {
        @FaultAction(className = WebServiceException.class, value = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/ConsultarPenalidade/Fault/WebServiceException")
    })
    public String consultarPenalidade()
        throws WebServiceException
    ;

    /**
     * 
     * @return
     *     returns java.lang.String
     * @throws WebServiceException
     */
    @WebMethod(operationName = "ConsultarMotivoPenalidade")
    @WebResult(partName = "return")
    @Action(input = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/ConsultarMotivoPenalidadeRequest", output = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/ConsultarMotivoPenalidadeResponse", fault = {
        @FaultAction(className = WebServiceException.class, value = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/ConsultarMotivoPenalidade/Fault/WebServiceException")
    })
    public String consultarMotivoPenalidade()
        throws WebServiceException
    ;

    /**
     * 
     * @return
     *     returns java.lang.String
     * @throws WebServiceException
     */
    @WebMethod(operationName = "ConsultarCanal")
    @WebResult(partName = "return")
    @Action(input = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/ConsultarCanalRequest", output = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/ConsultarCanalResponse", fault = {
        @FaultAction(className = WebServiceException.class, value = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/ConsultarCanal/Fault/WebServiceException")
    })
    public String consultarCanal()
        throws WebServiceException
    ;

    /**
     * 
     * @return
     *     returns java.lang.String
     * @throws WebServiceException
     */
    @WebMethod(operationName = "ConsultarWorkFlow")
    @WebResult(partName = "return")
    @Action(input = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/ConsultarWorkFlowRequest", output = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/ConsultarWorkFlowResponse", fault = {
        @FaultAction(className = WebServiceException.class, value = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/ConsultarWorkFlow/Fault/WebServiceException")
    })
    public String consultarWorkFlow()
        throws WebServiceException
    ;

    /**
     * 
     * @param arg7
     * @param arg6
     * @param arg9
     * @param arg8
     * @param arg28
     * @param arg29
     * @param arg24
     * @param arg25
     * @param arg26
     * @param arg27
     * @param arg20
     * @param arg21
     * @param arg22
     * @param arg23
     * @param arg3
     * @param arg17
     * @param arg2
     * @param arg18
     * @param arg5
     * @param arg19
     * @param arg4
     * @param arg13
     * @param arg14
     * @param arg1
     * @param arg15
     * @param arg0
     * @param arg16
     * @param arg31
     * @param arg10
     * @param arg32
     * @param arg11
     * @param arg12
     * @param arg30
     * @return
     *     returns java.lang.String
     * @throws WebServiceException
     */
    @WebMethod(operationName = "PesquisarOs")
    @WebResult(partName = "return")
    @Action(input = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/PesquisarOsRequest", output = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/PesquisarOsResponse", fault = {
        @FaultAction(className = WebServiceException.class, value = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_PesquisaEndPoint/PesquisarOs/Fault/WebServiceException")
    })
    public String pesquisarOs(
        @WebParam(name = "arg0", partName = "arg0")
        String arg0,
        @WebParam(name = "arg1", partName = "arg1")
        String arg1,
        @WebParam(name = "arg2", partName = "arg2")
        String arg2,
        @WebParam(name = "arg3", partName = "arg3")
        String arg3,
        @WebParam(name = "arg4", partName = "arg4")
        String arg4,
        @WebParam(name = "arg5", partName = "arg5")
        String arg5,
        @WebParam(name = "arg6", partName = "arg6")
        String arg6,
        @WebParam(name = "arg7", partName = "arg7")
        String arg7,
        @WebParam(name = "arg8", partName = "arg8")
        String arg8,
        @WebParam(name = "arg9", partName = "arg9")
        String arg9,
        @WebParam(name = "arg10", partName = "arg10")
        String arg10,
        @WebParam(name = "arg11", partName = "arg11")
        String arg11,
        @WebParam(name = "arg12", partName = "arg12")
        String arg12,
        @WebParam(name = "arg13", partName = "arg13")
        String arg13,
        @WebParam(name = "arg14", partName = "arg14")
        String arg14,
        @WebParam(name = "arg15", partName = "arg15")
        String arg15,
        @WebParam(name = "arg16", partName = "arg16")
        String arg16,
        @WebParam(name = "arg17", partName = "arg17")
        String arg17,
        @WebParam(name = "arg18", partName = "arg18")
        String arg18,
        @WebParam(name = "arg19", partName = "arg19")
        String arg19,
        @WebParam(name = "arg20", partName = "arg20")
        String arg20,
        @WebParam(name = "arg21", partName = "arg21")
        String arg21,
        @WebParam(name = "arg22", partName = "arg22")
        String arg22,
        @WebParam(name = "arg23", partName = "arg23")
        String arg23,
        @WebParam(name = "arg24", partName = "arg24")
        String arg24,
        @WebParam(name = "arg25", partName = "arg25")
        String arg25,
        @WebParam(name = "arg26", partName = "arg26")
        String arg26,
        @WebParam(name = "arg27", partName = "arg27")
        String arg27,
        @WebParam(name = "arg28", partName = "arg28")
        String arg28,
        @WebParam(name = "arg29", partName = "arg29")
        String arg29,
        @WebParam(name = "arg30", partName = "arg30")
        String arg30,
        @WebParam(name = "arg31", partName = "arg31")
        String arg31,
        @WebParam(name = "arg32", partName = "arg32")
        String arg32)
        throws WebServiceException
    ;

}
