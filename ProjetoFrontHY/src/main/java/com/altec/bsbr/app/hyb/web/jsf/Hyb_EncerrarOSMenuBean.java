package com.altec.bsbr.app.hyb.web.jsf;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;


@ManagedBean(name="encerrarOS")
@ViewScoped
public class Hyb_EncerrarOSMenuBean {

	public String strNrSeqOs = "strNrSeqOs";
	public String strReadOnly = "strReadOnly";
	public String strTitulo = "strTitulo";
	
}
