package com.altec.bsbr.app.hyb.web.jsf;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.HistPesqAnexoModel;
import com.altec.bsbr.app.hyb.dto.HistResultPesqModel;
import com.altec.bsbr.app.jab.hyb.webclient.XHYHistorico.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYHistorico.XHYHistoricoEndPoint;

@Component("histResulPesquisa")
@Scope("session")
public class HistResultPesqBean {
	
	private List<HistResultPesqModel> results;
	
	private String strCodEven = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strCodEven")  == null ? "" : FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strCodEven").toString() ;
	private String strCodAgen = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strCodAgen") == null ? "" : FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strCodAgen").toString() ;
	private String strDsTitu = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strDsTitu") == null ? "" : FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strDsTitu").toString();
	
	private String descModal = "test3";
	
	private List<HistPesqAnexoModel> resultAnxs;
	private StreamedContent file;
	
	@Autowired
	private XHYHistoricoEndPoint histControll;
	
	public HistResultPesqBean()
	{
		
	}
	@PostConstruct
	public void init()
	{
	
		System.out.println("Cheguei   glor" + strCodEven);
		Pesquisar();
		//PesquisarAnexo();
	}

	public void Pesquisar()
	{
		String retorno;
		
		try {
			
			retorno = histControll.consultarHistoricoOper(getStrCodEven(), getStrCodAgen(), getStrDsTitu());
			System.out.println(retorno);
			
			JSONObject pesqHistorico = new JSONObject(retorno);
			JSONArray pcursor = pesqHistorico.getJSONArray("PCURSOR");
			
			results = new ArrayList<HistResultPesqModel>();
			
			for(int i = 0; i < pcursor.length(); i++)
			{
				JSONObject item = pcursor.getJSONObject(i);
				HistResultPesqModel dadosGeral = new HistResultPesqModel(); 
				
				dadosGeral.setCD_EVEN(item.isNull("CD_EVEN") == true ? "" : item.get("CD_EVEN").toString());
				dadosGeral.setCD_AGEN(item.isNull("CD_AGEN") == true ? "" : item.get("CD_AGEN").toString());
				dadosGeral.setNM_RESP(item.isNull("NM_RESP") == true ? "" : item.get("NM_RESP").toString());
				dadosGeral.setDT_ENTR(item.isNull("DT_ENTR") == true ? "" : item.get("DT_ENTR").toString());
				dadosGeral.setNM_SITU(item.isNull("NM_SITU") == true ? "" : item.get("NM_SITU").toString());
				dadosGeral.setDS_TITU(item.isNull("DS_TITU") == true ? "" : item.get("DS_TITU").toString());
				results.add(dadosGeral);
			}
			
		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void PesquisarAnexo(String cdEvento)
	{
		String retorno;
		
		try {
			retorno = histControll.consultarHistoricoOperAnexo(cdEvento);
			
			System.out.println("CD EVENTO: " + cdEvento);
			
			JSONObject pesqAnx = new JSONObject(retorno);
			JSONArray pcursor = pesqAnx.getJSONArray("PCURSOR");
			
			resultAnxs = new ArrayList<HistPesqAnexoModel>();
			
			for(int i = 0; i < pcursor.length(); i++)
			{
				
				JSONObject item = pcursor.getJSONObject(i);
				HistPesqAnexoModel dadosAnx = new HistPesqAnexoModel();
				
				dadosAnx.setNM_ARQ(item.isNull("NM_ARQU") == true ? "" : item.get("NM_ARQU").toString());
				dadosAnx.setTP_ANEXO(item.isNull("TP_ANEX") == true ? "" : item.get("TP_ANEX").toString());
				
				resultAnxs.add(dadosAnx);
			}
			
		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void cleanSession() {
        System.out.println("\n --C LEA  dsNs s SESSI ON  --");
        
        //FacesContext context = FacesContext.getCurrentInstance(); 
        //context.getExternalContext().getSessionMap().remove("histResulPesquisa");
        
        String desc = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("desc").toString();
        this.descModal = desc;
        System.out.println("DESC: s"
        		+ " " + this.descModal);
        
        RequestContext.getCurrentInstance().execute("PF('descTitulo').show();");
	}
	
	public void downloadModal() {
        //FacesContext context = FacesContext.getCurrentInstance(); 
        //context.getExternalContext().getSessionMap().remove("histResulPesquisa");
		String cdEvento = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("cdEvento").toString();
        this.PesquisarAnexo(cdEvento);
		RequestContext.getCurrentInstance().execute("PF('descDownload').show();");
	}
	
	public void downloadFile(String nomeArq) {
		String serverUrl = "C:/Temp/";
		//File fileLocal = new File(serverUrl+nomeArq);
		File fileLocal = new File(serverUrl+"test.txt");
		
		String filePath = fileLocal.getPath().toString();
		String fileName = fileLocal.getName().toString();
		String contentType = FacesContext.getCurrentInstance().getExternalContext().getMimeType(filePath);
		
		InputStream stream;
		try {
			stream = new FileInputStream(filePath);
			this.file = new DefaultStreamedContent(stream, contentType, fileName);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	
	public String getStrCodEven() {
		return strCodEven;
	}


	public void setStrCodEven(String strCodEven) {
		this.strCodEven = strCodEven;
	}


	public String getStrCodAgen() {
		return strCodAgen;
	}


	public void setStrCodAgen(String strCodAgen) {
		this.strCodAgen = strCodAgen;
	}


	public String getStrDsTitu() {
		return strDsTitu;
	}


	public void setStrDsTitu(String strDsTitu) {
		this.strDsTitu = strDsTitu;
	}


	public XHYHistoricoEndPoint getHistControll() {
		return histControll;
	}


	public void setHistControll(XHYHistoricoEndPoint histControll) {
		this.histControll = histControll;
	}
	public List<HistResultPesqModel> getResults() {
		return results;
	}
	public void setResults(List<HistResultPesqModel> results) {
		this.results = results;
	}
	public String getDescModal() {
		return descModal;
	}
	public void setDescModal(String descModal) {
		this.descModal = descModal;
	}
	public List<HistPesqAnexoModel> getResultAnxs() {
		return resultAnxs;
	}
	public void setResultAnxs(List<HistPesqAnexoModel> resultAnxs) {
		this.resultAnxs = resultAnxs;
	}
	public StreamedContent getFile() {
		return file;
	}
	public void setFile(StreamedContent file) {
		this.file = file;
	}
	
	
}
