package com.altec.bsbr.app.hyb.dto;

public class ObjRsCriticidadeOs {
	private String CODIGO;
	private String CRITICIDADE;
	private String NOME;
	
	public ObjRsCriticidadeOs() {}
	
	public ObjRsCriticidadeOs(String cODIGO, String nOme) {
		super();
		CODIGO = cODIGO;
		NOME = nOme;
	}

	public String getCODIGO() {
		return CODIGO;
	}
	public void setCODIGO(String cODIGO) {
		CODIGO = cODIGO;
	}
	public String getCRITICIDADE() {
		return CRITICIDADE;
	}
	public void setCRITICIDADE(String cRITICIDADE) {
		CRITICIDADE = cRITICIDADE;
	}

	public String getNOME() {
		return NOME;
	}

	public void setNOME(String nOME) {
		NOME = nOME;
	}
	
}
