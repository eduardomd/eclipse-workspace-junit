package com.altec.bsbr.app.hyb.dto;

public class RsOs {
	
	//TODO Conferir os .Fields(numero) com o nome do campo para arrumar o nome das propriedades no java
	//     visto que no VBScript estava como .Fields(0).value
	private String NR_OS; //correspondente ao .Fields(0)
	private String TRAMITE; //correspondente ao .Fields(1)?
	
	private String DT_REG;
	private String DT_RETN_PLAN;
	private String DT_RETN;
	private String DS_DEST;
	private String DS_Envi; 
	private String DS_RETN;
	
	public String getDS_RETN() {
		return DS_RETN;
	}
	public void setDS_RETN(String dS_RETN) {
		DS_RETN = dS_RETN;
	}
	public String getNR_OS() {
		return NR_OS;
	}
	public void setNR_OS(String nR_OS) {
		NR_OS = nR_OS;
	}
	public String getTRAMITE() {
		return TRAMITE;
	}
	public void setTRAMITE(String tRAMITE) {
		TRAMITE = tRAMITE;
	}
	public String getDT_REG() {
		return DT_REG;
	}
	public void setDT_REG(String dT_REG) {
		DT_REG = dT_REG;
	}
	public String getDT_RETN_PLAN() {
		return DT_RETN_PLAN;
	}
	public void setDT_RETN_PLAN(String dT_RETN_PLAN) {
		DT_RETN_PLAN = dT_RETN_PLAN;
	}
	public String getDT_RETN() {
		return DT_RETN;
	}
	public void setDT_RETN(String dT_RETN) {
		DT_RETN = dT_RETN;
	}
	public String getDS_DEST() {
		return DS_DEST;
	}
	public void setDS_DEST(String dS_DEST) {
		DS_DEST = dS_DEST;
	}
	public String getDS_Envi() {
		return DS_Envi;
	}
	public void setDS_Envi(String ds_Envi) {
		DS_Envi = ds_Envi;
	}
	
}