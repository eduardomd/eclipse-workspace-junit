package com.altec.bsbr.app.hyb.dto;

public class RsOut {

    private String DT_CNTB;
    private String CD_CNAL_ORIG;
    private String NM_CNAL_ORIG;
    private String CD_CNTA_CNTB;
    private String CD_EMPR_GRUP;
    private String CD_AREA_COMP;
    private String CD_CATG_CNTB_NIVE_1;
    private String CD_CATG_CNTB_NIVE_2;
    private String CD_CATG_CNTB_NIVE_3;
    private String CD_LINH_NEGO_NIVE_1;
    private String CD_LINH_NEGO_NIVE_2;
    private String CD_FATR_RISC;
    private String CD_CPTU;
    private String NM_CENT_CNTB_UOR;
    private String NM_CENT_ORIG_UOR;
    private String NM_PV;
    private String NM_CAUS_EVEN;
    private String CD_MANI;
    private String NR_SEQU_ORDE_SERV_RELA;
    
    public RsOut() { 

    }
    
    
	public RsOut(String dT_CNTB, String cD_CNAL_ORIG, String nM_CNAL_ORIG, String cD_CNTA_CNTB, String cD_EMPR_GRUP,
			String cD_AREA_COMP, String cD_CATG_CNTB_NIVE_1, String cD_CATG_CNTB_NIVE_2, String cD_CATG_CNTB_NIVE_3,
			String cD_LINH_NEGO_NIVE_1, String cD_LINH_NEGO_NIVE_2, String cD_FATR_RISC, String cD_CPTU,
			String nM_CENT_CNTB_UOR, String nM_CENT_ORIG_UOR, String nM_PV, String nM_CAUS_EVEN, String cD_MANI,
			String nR_SEQU_ORDE_SERV_RELA) {
		super();
		DT_CNTB = dT_CNTB;
		CD_CNAL_ORIG = cD_CNAL_ORIG;
		NM_CNAL_ORIG = nM_CNAL_ORIG;
		CD_CNTA_CNTB = cD_CNTA_CNTB;
		CD_EMPR_GRUP = cD_EMPR_GRUP;
		CD_AREA_COMP = cD_AREA_COMP;
		CD_CATG_CNTB_NIVE_1 = cD_CATG_CNTB_NIVE_1;
		CD_CATG_CNTB_NIVE_2 = cD_CATG_CNTB_NIVE_2;
		CD_CATG_CNTB_NIVE_3 = cD_CATG_CNTB_NIVE_3;
		CD_LINH_NEGO_NIVE_1 = cD_LINH_NEGO_NIVE_1;
		CD_LINH_NEGO_NIVE_2 = cD_LINH_NEGO_NIVE_2;
		CD_FATR_RISC = cD_FATR_RISC;
		CD_CPTU = cD_CPTU;
		NM_CENT_CNTB_UOR = nM_CENT_CNTB_UOR;
		NM_CENT_ORIG_UOR = nM_CENT_ORIG_UOR;
		NM_PV = nM_PV;
		NM_CAUS_EVEN = nM_CAUS_EVEN;
		CD_MANI = cD_MANI;
		NR_SEQU_ORDE_SERV_RELA = nR_SEQU_ORDE_SERV_RELA;
	}



	public String getDT_CNTB() {
		return DT_CNTB;
	}
	public void setDT_CNTB(String dT_CNTB) {
		DT_CNTB = dT_CNTB;
	}
	public String getCD_CNAL_ORIG() {
		return CD_CNAL_ORIG;
	}
	public void setCD_CNAL_ORIG(String cD_CNAL_ORIG) {
		CD_CNAL_ORIG = cD_CNAL_ORIG;
	}
	public String getNM_CNAL_ORIG() {
		return NM_CNAL_ORIG;
	}
	public void setNM_CNAL_ORIG(String nM_CNAL_ORIG) {
		NM_CNAL_ORIG = nM_CNAL_ORIG;
	}
	public String getCD_CNTA_CNTB() {
		return CD_CNTA_CNTB;
	}
	public void setCD_CNTA_CNTB(String cD_CNTA_CNTB) {
		CD_CNTA_CNTB = cD_CNTA_CNTB;
	}
	public String getCD_EMPR_GRUP() {
		return CD_EMPR_GRUP;
	}
	public void setCD_EMPR_GRUP(String cD_EMPR_GRUP) {
		CD_EMPR_GRUP = cD_EMPR_GRUP;
	}
	public String getCD_AREA_COMP() {
		return CD_AREA_COMP;
	}
	public void setCD_AREA_COMP(String cD_AREA_COMP) {
		CD_AREA_COMP = cD_AREA_COMP;
	}
	public String getCD_CATG_CNTB_NIVE_1() {
		return CD_CATG_CNTB_NIVE_1;
	}
	public void setCD_CATG_CNTB_NIVE_1(String cD_CATG_CNTB_NIVE_1) {
		CD_CATG_CNTB_NIVE_1 = cD_CATG_CNTB_NIVE_1;
	}
	public String getCD_CATG_CNTB_NIVE_2() {
		return CD_CATG_CNTB_NIVE_2;
	}
	public void setCD_CATG_CNTB_NIVE_2(String cD_CATG_CNTB_NIVE_2) {
		CD_CATG_CNTB_NIVE_2 = cD_CATG_CNTB_NIVE_2;
	}
	public String getCD_CATG_CNTB_NIVE_3() {
		return CD_CATG_CNTB_NIVE_3;
	}
	public void setCD_CATG_CNTB_NIVE_3(String cD_CATG_CNTB_NIVE_3) {
		CD_CATG_CNTB_NIVE_3 = cD_CATG_CNTB_NIVE_3;
	}
	public String getCD_LINH_NEGO_NIVE_1() {
		return CD_LINH_NEGO_NIVE_1;
	}
	public void setCD_LINH_NEGO_NIVE_1(String cD_LINH_NEGO_NIVE_1) {
		CD_LINH_NEGO_NIVE_1 = cD_LINH_NEGO_NIVE_1;
	}
	public String getCD_LINH_NEGO_NIVE_2() {
		return CD_LINH_NEGO_NIVE_2;
	}
	public void setCD_LINH_NEGO_NIVE_2(String cD_LINH_NEGO_NIVE_2) {
		CD_LINH_NEGO_NIVE_2 = cD_LINH_NEGO_NIVE_2;
	}
	public String getCD_FATR_RISC() {
		return CD_FATR_RISC;
	}
	public void setCD_FATR_RISC(String cD_FATR_RISC) {
		CD_FATR_RISC = cD_FATR_RISC;
	}
	public String getCD_CPTU() {
		return CD_CPTU;
	}
	public void setCD_CPTU(String cD_CPTU) {
		CD_CPTU = cD_CPTU;
	}
	public String getNM_CENT_CNTB_UOR() {
		return NM_CENT_CNTB_UOR;
	}
	public void setNM_CENT_CNTB_UOR(String nM_CENT_CNTB_UOR) {
		NM_CENT_CNTB_UOR = nM_CENT_CNTB_UOR;
	}
	public String getNM_CENT_ORIG_UOR() {
		return NM_CENT_ORIG_UOR;
	}
	public void setNM_CENT_ORIG_UOR(String nM_CENT_ORIG_UOR) {
		NM_CENT_ORIG_UOR = nM_CENT_ORIG_UOR;
	}
	public String getNM_PV() {
		return NM_PV;
	}
	public void setNM_PV(String nM_PV) {
		NM_PV = nM_PV;
	}
	public String getNM_CAUS_EVEN() {
		return NM_CAUS_EVEN;
	}
	public void setNM_CAUS_EVEN(String nM_CAUS_EVEN) {
		NM_CAUS_EVEN = nM_CAUS_EVEN;
	}
	public String getCD_MANI() {
		return CD_MANI;
	}
	public void setCD_MANI(String cD_MANI) {
		CD_MANI = cD_MANI;
	}
	public String getNR_SEQU_ORDE_SERV_RELA() {
		return NR_SEQU_ORDE_SERV_RELA;
	}
	public void setNR_SEQU_ORDE_SERV_RELA(String nR_SEQU_ORDE_SERV_RELA) {
		NR_SEQU_ORDE_SERV_RELA = nR_SEQU_ORDE_SERV_RELA;
	}
       
}