package com.altec.bsbr.app.hyb.web.jsf;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.CnalOrigModel;
import com.altec.bsbr.app.hyb.dto.PrintObjRsCadOSArqAnex;
import com.altec.bsbr.app.hyb.dto.UsuarioIncModel;
import com.altec.bsbr.app.hyb.web.util.LogIncUtil;
import com.altec.bsbr.app.hyb.web.util.XHYUsuarioIncService;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmLog.XHYAdmLogEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYTreinamento.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYTreinamento.XHYTreinamentoEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;

@Component("treinamentoCadastroBean")
@Scope("session")
public class TreinamentoCadastroBean extends BasicBBean {

	private static final long serialVersionUID = 1L;

	private String strSeqTrei;
	private String strNome;
	private String strDtTrein;
	private String strTotal;
	private String cboLocalSelecionado;
	private String strHdAction;
	private String strHdParamNmArquivo;
	private String strHdSeqTrei;
	private boolean existeSeqTrei = false;

	private List<PrintObjRsCadOSArqAnex> objRsArqAnex = new ArrayList<PrintObjRsCadOSArqAnex>();;
	private StreamedContent file;
	private String srcIfrmImpArq;

	private LinkedHashMap<String, String> objLocal;

	@Autowired
	private XHYTreinamentoEndPoint objTreinamento;

	private UsuarioIncModel usuarioIncModel;

	@Autowired
	private XHYUsuarioIncService usuario;
	
	@Autowired
	private XHYAdmLogEndPoint admLog;
	
	@PostConstruct
	public void init() throws IOException {
		carregarInformacoes();
	}
	
	private void carregarInformacoes()
	{	
		System.out.println("Carregar Informações: 1");
		
		carregarUsuario();
		carregarStrSeqTrei();
		carregarLocalTreinamento();
		System.out.println("hyb_uploadprocessa.xhtml?qstrTpAnexo=T&qstrSeqTrei=" + this.strSeqTrei);
		srcIfrmImpArq = "hyb_uploadprocessa.xhtml?qstrTpAnexo=T&qstrSeqTrei=" + this.strSeqTrei;
	}
	
	private void carregarUsuario() {
		try {
			usuarioIncModel = usuario.getDadosUsuario();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	private void carregarStrSeqTrei() {
		this.strSeqTrei = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
				.get("strSeqTrei");

		if (this.strSeqTrei != null) {
			existeSeqTrei = true;
			carregarTreinamento();
		}
	}

	
	private void carregarTreinamento() {
		
		Date data = new Date();
		String dataFim = DateFormat.getDateInstance(DateFormat.MEDIUM).format(data).toString();
		String dataInicio = "01/01/2000";
		JSONArray pcursorTreinamento = new JSONArray();

		try {

			if (this.strDtTrein == null) {
				this.strSeqTrei = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
						.get("strSeqTrei");
			}
			
			String treinamento = objTreinamento.consultarTreinamento("0" + this.strSeqTrei, "", convertData(dataInicio),
					convertData(dataFim), 0);

			JSONObject objTreinamento = new JSONObject(treinamento);
			pcursorTreinamento = objTreinamento.getJSONArray("PCURSOR");
			JSONObject temp = pcursorTreinamento.getJSONObject(0);

			strNome = temp.get("NOME").toString();
			strDtTrein = temp.get("DATA").toString();
			strTotal = temp.get("QTDE").toString();
			cboLocalSelecionado = temp.get("CODLOCAL").toString() + "=" + temp.get("LOCAL").toString();

			carregarAnexos();

		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void cleanSession() {
		FacesContext context = FacesContext.getCurrentInstance();
		context.getExternalContext().getSessionMap().remove("uploadprocessa");
		context.getExternalContext().getSessionMap().remove("treinamentoCadastroBean");
		carregarInformacoes();
	}

	private void carregarAnexos() {
		
		JSONArray pcursorAnexo = new JSONArray();

		try {
			String anexos = objTreinamento.consultarAnexo(this.strSeqTrei);

			JSONObject objTreinamento = new JSONObject(anexos);
			pcursorAnexo = objTreinamento.getJSONArray("PCURSOR");

			for (int i = 0; i < pcursorAnexo.length(); i++) {
				JSONObject temp = pcursorAnexo.getJSONObject(i);

				PrintObjRsCadOSArqAnex obj = new PrintObjRsCadOSArqAnex();
				obj.setNM_CAMI_ARQU_ANEX(temp.isNull("NM_CAMI_ANEX") ? "" : temp.getString("NM_CAMI_ANEX"));
				this.objRsArqAnex.add(obj);

			}

		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// Download File
	public void saveFile() throws FileNotFoundException {
		
		String idArquivo = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("fileId")
				.toString();
		String filePath = this.objRsArqAnex.get(Integer.parseInt(idArquivo)).getCaminhoArquivo();
		String fileName = this.objRsArqAnex.get(Integer.parseInt(idArquivo)).getNomeArquivo();
		String contentType = FacesContext.getCurrentInstance().getExternalContext().getMimeType(filePath);

		InputStream stream = new FileInputStream(filePath);
		file = new DefaultStreamedContent(stream, contentType, fileName);

		this.carregarAnexos();
		
	}

	public void deletarArquivo() {
		
		String idArquivo = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
				.get("idArquivo").toString();
		String currArq = this.objRsArqAnex.get(Integer.parseInt(idArquivo)).getNM_CAMI_ARQU_ANEX();

		try {
			String result = this.objTreinamento.excluirAnexo(this.strSeqTrei, currArq);
			// objRsCadOSArqAnex.remove(Integer.parseInt(idArquivo));
			objRsArqAnex = new ArrayList<PrintObjRsCadOSArqAnex>();
			this.carregarAnexos();
		} catch (WebServiceException e) {
			e.printStackTrace();
		}
	}

	public void resetObjRsArq() {
		objRsArqAnex = new ArrayList<PrintObjRsCadOSArqAnex>();
		this.carregarAnexos();
	}

	private void carregarLocalTreinamento() {
		
		JSONArray pcursorLocalTreinamento = new JSONArray();

		objLocal = new LinkedHashMap<>();

		try {
			String localTreinamento = objTreinamento.consultarLocalTreinamento();

			JSONObject objTreinamento = new JSONObject(localTreinamento);
			pcursorLocalTreinamento = objTreinamento.getJSONArray("PCURSOR");

			for (int i = 0; i < pcursorLocalTreinamento.length(); i++) {
				JSONObject temp = pcursorLocalTreinamento.getJSONObject(i);

				objLocal.put(temp.get("CODIGO").toString(), temp.get("NOME").toString());


			}

		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void salvar() {
		
		String retorno;
		JSONArray pcursorTreinamento = new JSONArray();

		setStrDtTrein(convertData(strDtTrein));

		try {
			retorno = objTreinamento.inserirTreinamentoRetSeq(strNome, strDtTrein, strTotal, cboLocalSelecionado);

			JSONObject objTreinamento = new JSONObject(retorno);
			pcursorTreinamento = objTreinamento.getJSONArray("PCURSOR");
			JSONObject temp = pcursorTreinamento.getJSONObject(0);

			String novoSeqTrei = temp.get("SEQUENCIAL").toString();

			try {
				admLog.incluirAcaoLog("", Integer.toString(LogIncUtil.CADASTRAR_TREINAMENTO),
						usuarioIncModel.getCpfUsuario(), "");
			} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYAdmLog.WebServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			cleanSession();
			
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_treinamento_cadastro.xhtml?strSeqTrei=" + novoSeqTrei);
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (WebServiceException e) {
			e.printStackTrace();
		}
		
	}

	public void alterar() {
		
		setStrDtTrein(convertData(strDtTrein));

		try {
			objTreinamento.alterarTreinamentoRetSeq(strSeqTrei, strNome, strDtTrein, strTotal,
					cboLocalSelecionado.split("=")[0]);
			
			System.out.println("log: " + strSeqTrei + "," + Integer.toString(LogIncUtil.ALTERAR_TREINAMENTO) + "," + usuarioIncModel.getCpfUsuario());
			
			try {
				admLog.incluirAcaoLog("", Integer.toString(LogIncUtil.ALTERAR_TREINAMENTO),
						usuarioIncModel.getCpfUsuario(), "");
			} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYAdmLog.WebServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} catch (WebServiceException e) {
			e.printStackTrace();
		}
	}

	public static String convertData(String dataInput) {
		String dataInput2 = "";

		if (dataInput.matches("([0-9]{2}/[0-9]{2}/[0-9]{4})")) {
			Date initDate = null;
			try {
				initDate = new SimpleDateFormat("dd/MM/yyyy").parse(dataInput);
			} catch (java.text.ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			dataInput2 = formatter.format(initDate);

		} else {

			try {

				dataInput2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
						.format(new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyy", Locale.US).parse(dataInput));

			} catch (ParseException e) {
				try {
					FacesContext.getCurrentInstance().getExternalContext()
							.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
		return dataInput2;
	}

	public String getCboLocalSelecionado() {
		return cboLocalSelecionado;
	}

	public void setCboLocalSelecionado(String cboLocalSelecionado) {
		this.cboLocalSelecionado = cboLocalSelecionado.split("=")[0];
	}

	public LinkedHashMap<String, String> getObjLocal() {
		return objLocal;
	}

	public void setObjLocal(LinkedHashMap<String, String> objLocal) {
		this.objLocal = objLocal;
	}

	public String getStrSeqTrei() {
		return strSeqTrei;
	}

	public void setStrSeqTrei(String strSeqTrei) {
		this.strSeqTrei = strSeqTrei;
	}

	public String getStrNome() {
		return strNome;
	}

	public void setStrNome(String strNome) {
		this.strNome = strNome;
	}

	public String getStrDtTrein() {
		return strDtTrein;
	}

	public void setStrDtTrein(String strDtTrein) {
		this.strDtTrein = strDtTrein;
	}

	public String getStrTotal() {
		return strTotal;
	}

	public void setStrTotal(String strTotal) {
		this.strTotal = strTotal;
	}

	public String getStrHdAction() {
		return strHdAction;
	}

	public void setStrHdAction(String strHdAction) {
		this.strHdAction = strHdAction;
	}

	public String getStrHdParamNmArquivo() {
		return strHdParamNmArquivo;
	}

	public void setStrHdParamNmArquivo(String strHdParamNmArquivo) {
		this.strHdParamNmArquivo = strHdParamNmArquivo;
	}

	public String getStrHdSeqTrei() {
		return strHdSeqTrei;
	}

	public void setStrHdSeqTrei(String strHdSeqTrei) {
		this.strHdSeqTrei = strHdSeqTrei;
	}

	public XHYTreinamentoEndPoint getObjTreinamento() {
		return objTreinamento;
	}

	public void setObjTreinamento(XHYTreinamentoEndPoint objTreinamento) {
		this.objTreinamento = objTreinamento;
	}

	public boolean isExisteSeqTrei() {
		return existeSeqTrei;
	}

	public void setExisteSeqTrei(boolean existeSeqTrei) {
		this.existeSeqTrei = existeSeqTrei;
	}

	public StreamedContent getFile() {
		return file;
	}

	public void setFile(StreamedContent file) {
		this.file = file;
	}

	public String getSrcIfrmImpArq() {
		return srcIfrmImpArq;
	}

	public void setSrcIfrmImpArq(String srcIfrmImpArq) {
		this.srcIfrmImpArq = srcIfrmImpArq;
	}

	public List<PrintObjRsCadOSArqAnex> getObjRsArqAnex() {
		return objRsArqAnex;
	}

	public void setObjRsArqAnex(List<PrintObjRsCadOSArqAnex> objRsArqAnex) {
		this.objRsArqAnex = objRsArqAnex;
	}

}