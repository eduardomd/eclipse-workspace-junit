package com.altec.bsbr.app.hyb.web.jsf;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.RsOs;
import com.altec.bsbr.app.jab.hyb.webclient.XHYTramite.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYTramite.XHYTramiteEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;

@Component("ImprimirOsTramiteBean")
@Scope("request")
public class ImprimirOsTramiteBean extends BasicBBean {

	@Autowired
	private XHYTramiteEndPoint tramite;
	
	private static final long serialVersionUID = 1L;
	
	//Query String - valor usado na consulta backend
	private Object strNrSeqOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs");
	        
	private List<RsOs> objRsOs;
		
	public void setupObjRsOs() {
		String retorno = "";		
		try {
			retorno = tramite.consultarTramites(this.strNrSeqOs.toString());
			System.out.println("retorno consultarTramites"+retorno);
		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
        JSONObject jsonRetorno = new JSONObject(retorno);
        JSONArray pcursor = jsonRetorno.getJSONArray("PCURSOR");
        for(int i = 0; i < pcursor.length(); i++) {        	
        	JSONObject curr = pcursor.getJSONObject(i);
			RsOs rsOS = new RsOs();
			rsOS.setDS_DEST(curr.isNull("DS_DEST") ? "" : curr.getString("DS_DEST"));
			rsOS.setDT_REG(curr.isNull("DT_REG") ? "" : curr.getString("DT_REG"));
			rsOS.setDT_RETN(curr.isNull("DT_RETN") ? "" : curr.getString("DT_RETN"));
			rsOS.setDT_RETN_PLAN(curr.isNull("DT_RETN_PLAN") ? "" : curr.getString("DT_RETN_PLAN"));
			rsOS.setTRAMITE(curr.isNull("NR_TRA") ? "" : String.valueOf(curr.getInt("NR_TRA")));
			rsOS.setDS_Envi(curr.isNull("DS_ENVI") ? "" : curr.getString("DS_ENVI"));
			rsOS.setDS_RETN(curr.isNull("DS_RETN") ? "" : curr.getString("DS_RETN"));
			rsOS.setNR_OS(curr.isNull("OS") ? "" : curr.getString("OS"));			
			objRsOs.add(rsOS);
        }
	}
	
	@PostConstruct
	public void init() {
		objRsOs = new ArrayList<RsOs>();
		if (this.strNrSeqOs != null)
			setupObjRsOs();		
	}

	public List<RsOs> getObjRsOs() {
		return objRsOs;
	}

	public Object getStrNrSeqOs() {
		return strNrSeqOs;
	}

	public void setStrNrSeqOs(Object strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}
	
	

}