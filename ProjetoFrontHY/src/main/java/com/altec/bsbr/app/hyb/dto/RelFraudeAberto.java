package com.altec.bsbr.app.hyb.dto;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;


public class RelFraudeAberto{

	private String banco;
	private Integer qtdOS;
	private float valor;
	private String canal;
	
	public RelFraudeAberto() {
		
	}
	
	public RelFraudeAberto(String banco, Integer qtdOS, float valor, String canal) {
		super();
		this.banco = banco;
		this.qtdOS = qtdOS;
		this.valor = valor;
		this.canal = canal;
	}
	public String getBanco() {
		return banco;
	}
	public void setBanco(String banco) {
		this.banco = banco;
	}
	public Integer getQtdOS() {
		return qtdOS;
	}
	public void setQtdOS(Integer qtdOS) {
		this.qtdOS = qtdOS;
	}
	public float getValor() {
		return valor;
	}
	public void setValor(float valor) {
		this.valor = valor;
	}
	public String getCanal() {
		return canal;
	}
	public void setCanal(String canal) {
		this.canal = canal;
	}
}