package com.altec.bsbr.app.hyb.web.jsf;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;

import com.altec.bsbr.app.hyb.dto.hyPesquisaOsModel;

@ManagedBean(name="PesquisaOsBean")
public class hyPesquisaOsBean implements Serializable {	
	
	private String pageVersion = "1.48";
	
	private hyPesquisaOsModel objPesquisa = new hyPesquisaOsModel();
	
	private List<SelectItem> objRsSituacaoOs = objPesquisa.consultarSituacaoOS();
	private List<SelectItem> objRsFaseOs = objPesquisa.consultarFaseAnaliseOS();
	private List<SelectItem> objRsCriticidadeOs = objPesquisa.consultarCriticidadeOs();
	private List<SelectItem> objRsPenalidade = objPesquisa.consultarPenalidade();
	private List<SelectItem> objRsMotivoPenalidade = objPesquisa.consultarMotivoPenalidade();
	private List<SelectItem> objRsSituacaoRecurso = objPesquisa.consultarSituacaoRecurso();
	private List<SelectItem> objRsCanalOrigem = objPesquisa.consultarCanalOrigem();
	private List<SelectItem> objRsCanal = objPesquisa.consultarCanal();
	private List<SelectItem> objRsEvento = objPesquisa.consultarEvento();
	private List<SelectItem> objRsPCorp = objPesquisa.consultarPCorp();
	private List<SelectItem> objRsPAux = objPesquisa.consultarPAux();
	
	private Date txtDataPocIni;
	private Date txtDataPocFin;
	private Date txtDataEventoIni;
	private Date txtDataEventoFin;
	private Date txtPeriPesqIni;
	private Date txtPeriPesqFin;
	
	private String txtNrSeqOs;
	private Number txtCustoOsIni;
	private Number txtCustoOsFin;
	private String txtCpfEnvolvido;
	private String txtNomeEnvolvido;
	private String txtMatrAnDesig;
	private String txtNomeAnDesig;
	
	private String tipoDocumento;
	private String cboSituacaoOs;
	private String cboFaseOs;
	private String cboCriticidadeOs;
	private String cboPenalidade;
	private String cboMotivoPenalidade;
	private String cboSituacaoRec;
	private String cboCanalOrigem;
	private String cboEvento;
	private String cboCanal;
	private String cboPCorp;
	private String cboPAux;
	
	private boolean chkInteresseCom = false;
	private boolean chkRessarcimento = false;	
	
	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public Date getTxtDataEventoFin() {
		return txtDataEventoFin;
	}

	public void setTxtDataEventoFin(Date txtDataEventoFin) {
		this.txtDataEventoFin = txtDataEventoFin;
	}

	public Date getTxtDataPocIni() {
		return txtDataPocIni;
	}

	public void setTxtDataPocIni(Date txtDataPocIni) {
		this.txtDataPocIni = txtDataPocIni;
	}

	public Date getTxtDataPocFin() {
		return txtDataPocFin;
	}

	public void setTxtDataPocFin(Date txtDataPocFin) {
		this.txtDataPocFin = txtDataPocFin;
	}

	public Date getTxtDataEventoIni() {
		return txtDataEventoIni;
	}

	public void setTxtDataEventoIni(Date txtDataEventoIni) {
		this.txtDataEventoIni = txtDataEventoIni;
	}

	public Date getTxtPeriPesqIni() {
		return txtPeriPesqIni;
	}

	public void setTxtPeriPesqIni(Date txtPeriPesqIni) {
		this.txtPeriPesqIni = txtPeriPesqIni;
	}

	public Date getTxtPeriPesqFin() {
		return txtPeriPesqFin;
	}

	public void setTxtPeriPesqFin(Date txtPeriPesqFin) {
		this.txtPeriPesqFin = txtPeriPesqFin;
	}

	public boolean isChkInteresseCom() {
		return chkInteresseCom;
	}

	public void setChkInteresseCom(boolean chkInteresseCom) {
		this.chkInteresseCom = chkInteresseCom;
	}

	public boolean isChkRessarcimento() {
		return chkRessarcimento;
	}

	public void setChkRessarcimento(boolean chkRessarcimento) {
		this.chkRessarcimento = chkRessarcimento;
	}

	public String getCboPenalidade() {
		return cboPenalidade;
	}

	public void setCboPenalidade(String cboPenalidade) {
		this.cboPenalidade = cboPenalidade;
	}

	public String getCboMotivoPenalidade() {
		return cboMotivoPenalidade;
	}

	public void setCboMotivoPenalidade(String cboMotivoPenalidade) {
		this.cboMotivoPenalidade = cboMotivoPenalidade;
	}

	public String getCboSituacaoRec() {
		return cboSituacaoRec;
	}

	public void setCboSituacaoRec(String cboSituacaoRec) {
		this.cboSituacaoRec = cboSituacaoRec;
	}

	public String getCboCanalOrigem() {
		return cboCanalOrigem;
	}

	public void setCboCanalOrigem(String cboCanalOrigem) {
		this.cboCanalOrigem = cboCanalOrigem;
	}

	public String getCboEvento() {
		return cboEvento;
	}

	public void setCboEvento(String cboEvento) {
		this.cboEvento = cboEvento;
	}

	public String getCboCanal() {
		return cboCanal;
	}

	public void setCboCanal(String cboCanal) {
		this.cboCanal = cboCanal;
	}

	public String getCboPCorp() {
		return cboPCorp;
	}

	public void setCboPCorp(String cboPCorp) {
		this.cboPCorp = cboPCorp;
	}

	public String getCboPAux() {
		return cboPAux;
	}

	public void setCboPAux(String cboPAux) {
		this.cboPAux = cboPAux;
	}

	public String getCboCriticidadeOs() {
		return cboCriticidadeOs;
	}

	public void setCboCriticidadeOs(String cboCriticidadeOs) {
		this.cboCriticidadeOs = cboCriticidadeOs;
	}

	public String getCboFaseOs() {
		return cboFaseOs;
	}

	public void setCboFaseOs(String cboFaseOs) {
		this.cboFaseOs = cboFaseOs;
	}

	public List<SelectItem> getObjRsPenalidade() {
		return objRsPenalidade;
	}

	public List<SelectItem> getObjRsMotivoPenalidade() {
		return objRsMotivoPenalidade;
	}

	public List<SelectItem> getObjRsSituacaoRecurso() {
		return objRsSituacaoRecurso;
	}

	public List<SelectItem> getObjRsCanalOrigem() {
		return objRsCanalOrigem;
	}

	public List<SelectItem> getObjRsCanal() {
		return objRsCanal;
	}

	public List<SelectItem> getObjRsEvento() {
		return objRsEvento;
	}

	public List<SelectItem> getObjRsPCorp() {
		return objRsPCorp;
	}

	public List<SelectItem> getObjRsPAux() {
		return objRsPAux;
	}

	public List<SelectItem> getObjRsFaseOs() {
		return objRsFaseOs;
	}
	
	public List<SelectItem> getObjRsCriticidadeOs() {
		return objRsCriticidadeOs;
	}

	public String getCboSituacaoOs() {
		return cboSituacaoOs;
	}

	public void setCboSituacaoOs(String cboSituacaoOs) {
		this.cboSituacaoOs = cboSituacaoOs;
	}

	public String getTxtNrSeqOs() {
		return txtNrSeqOs;
	}

	public void setTxtNrSeqOs(String txtNrSeqOs) {
		this.txtNrSeqOs = txtNrSeqOs;
	}

	public Number getTxtCustoOsIni() {
		return txtCustoOsIni;
	}

	public void setTxtCustoOsIni(Number txtCustoOsIni) {
		this.txtCustoOsIni = txtCustoOsIni;
	}

	public Number getTxtCustoOsFin() {
		return txtCustoOsFin;
	}

	public void setTxtCustoOsFin(Number txtCustoOsFin) {
		this.txtCustoOsFin = txtCustoOsFin;
	}

	public String getTxtCpfEnvolvido() {
		return txtCpfEnvolvido;
	}

	public void setTxtCpfEnvolvido(String txtCpfEnvolvido) {
		this.txtCpfEnvolvido = txtCpfEnvolvido;
	}

	public String getTxtNomeEnvolvido() {
		return txtNomeEnvolvido;
	}

	public void setTxtNomeEnvolvido(String txtNomeEnvolvido) {
		this.txtNomeEnvolvido = txtNomeEnvolvido;
	}

	public String getTxtMatrAnDesig() {
		return txtMatrAnDesig;
	}

	public void setTxtMatrAnDesig(String txtMatrAnDesig) {
		this.txtMatrAnDesig = txtMatrAnDesig;
	}

	public String getTxtNomeAnDesig() {
		return txtNomeAnDesig;
	}

	public void setTxtNomeAnDesig(String txtNomeAnDesig) {
		this.txtNomeAnDesig = txtNomeAnDesig;
	}

	public List<SelectItem> getObjRsSituacaoOs() {
		return objRsSituacaoOs;
	}
	
	@PostConstruct
	public void init() {
	}

	public void pesquisar() {
		System.out.println("Inserir l�gica de pesquisa aqui.");
	}
}
