package com.altec.bsbr.app.hyb.dto;

import javax.faces.bean.ManagedBean;
@ManagedBean(name = "objRsEqpAtrbOs")
public class PrintObjRsEqpAtrbOs {
	
	private String MATRICULA;
	private String NOME;
	private String CARGO;
	private String NM_DESIGNACAO;
	
	public PrintObjRsEqpAtrbOs() {}

	public String getMATRICULA() {
		return MATRICULA;
	}

	public void setMATRICULA(String mATRICULA) {
		MATRICULA = mATRICULA;
	}

	public String getNOME() {
		return NOME;
	}

	public void setNOME(String nOME) {
		NOME = nOME;
	}

	public String getCARGO() {
		return CARGO;
	}

	public void setCARGO(String cARGO) {
		CARGO = cARGO;
	}

	public String getNM_DESIGNACAO() {
		return NM_DESIGNACAO;
	}

	public void setNM_DESIGNACAO(String nM_DESIGNACAO) {
		NM_DESIGNACAO = nM_DESIGNACAO;
	}
}






