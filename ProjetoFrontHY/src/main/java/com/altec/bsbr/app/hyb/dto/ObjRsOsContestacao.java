package com.altec.bsbr.app.hyb.dto;

import javax.faces.bean.ManagedBean;
@ManagedBean(name="objRsOs")
public class ObjRsOsContestacao {
	
	public String SITUACAO;
	public String CD_NOTI;
	public String NM_NOTI;
	public String CODIGO;
	public String DT_ABERTURA;
	public String DT_RELATORIO;
	public String VL_ENVOLVIDO;
	public String VL_RECUPERADO;
	public String VL_PREJUIZO;
	public String AREA;
	public String TX_ABERTURA;
	public String TX_FALHAS;
	public String TX_ENCERRAMENTO;
	public String TX_OUTRAS_PROP;
	public String PARECER_JURI;
	public String CD_SITU;
	public String NR_SEQU_TRAN_FRAU_CNAL;
	public String RecordCount;
	
	public ObjRsOsContestacao() {};
	
	public String getRecordCount() {
		return RecordCount;
	}

	public void setRecordCount(String recordCount) {
		RecordCount = recordCount;
	}

	public String getNR_SEQU_TRAN_FRAU_CNAL() {
		return NR_SEQU_TRAN_FRAU_CNAL;
	}

	public void setNR_SEQU_TRAN_FRAU_CNAL(String nR_SEQU_TRAN_FRAU_CNAL) {
		NR_SEQU_TRAN_FRAU_CNAL = nR_SEQU_TRAN_FRAU_CNAL;
	}

	public String getSITUACAO() {
		return SITUACAO;
	}

	public void setSITUACAO(String sITUACAO) {
		SITUACAO = sITUACAO;
	}

	public String getCD_NOTI() {
		return CD_NOTI;
	}

	public void setCD_NOTI(String cD_NOTI) {
		CD_NOTI = cD_NOTI;
	}

	public String getNM_NOTI() {
		return NM_NOTI;
	}

	public void setNM_NOTI(String nM_NOTI) {
		NM_NOTI = nM_NOTI;
	}

	public String getCODIGO() {
		return CODIGO;
	}

	public void setCODIGO(String cODIGO) {
		CODIGO = cODIGO;
	}

	public String getDT_ABERTURA() {
		return DT_ABERTURA;
	}

	public void setDT_ABERTURA(String dT_ABERTURA) {
		DT_ABERTURA = dT_ABERTURA;
	}

	public String getDT_RELATORIO() {
		return DT_RELATORIO;
	}

	public void setDT_RELATORIO(String dT_RELATORIO) {
		DT_RELATORIO = dT_RELATORIO;
	}

	public String getVL_ENVOLVIDO() {
		return VL_ENVOLVIDO;
	}

	public void setVL_ENVOLVIDO(String vL_ENVOLVIDO) {
		VL_ENVOLVIDO = vL_ENVOLVIDO;
	}

	public String getVL_RECUPERADO() {
		return VL_RECUPERADO;
	}

	public void setVL_RECUPERADO(String vL_RECUPERADO) {
		VL_RECUPERADO = vL_RECUPERADO;
	}

	public String getVL_PREJUIZO() {
		return VL_PREJUIZO;
	}

	public void setVL_PREJUIZO(String vL_PREJUIZO) {
		VL_PREJUIZO = vL_PREJUIZO;
	}

	public String getAREA() {
		return AREA;
	}

	public void setAREA(String aREA) {
		AREA = aREA;
	}

	public String getTX_ABERTURA() {
		return TX_ABERTURA;
	}

	public void setTX_ABERTURA(String tX_ABERTURA) {
		TX_ABERTURA = tX_ABERTURA;
	}

	public String getTX_FALHAS() {
		return TX_FALHAS;
	}

	public void setTX_FALHAS(String tX_FALHAS) {
		TX_FALHAS = tX_FALHAS;
	}

	public String getTX_ENCERRAMENTO() {
		return TX_ENCERRAMENTO;
	}

	public void setTX_ENCERRAMENTO(String tX_ENCERRAMENTO) {
		TX_ENCERRAMENTO = tX_ENCERRAMENTO;
	}

	public String getTX_OUTRAS_PROP() {
		return TX_OUTRAS_PROP;
	}

	public void setTX_OUTRAS_PROP(String tX_OUTRAS_PROP) {
		TX_OUTRAS_PROP = tX_OUTRAS_PROP;
	}

	public String getPARECER_JURI() {
		return PARECER_JURI;
	}

	public void setPARECER_JURI(String pARECER_JURI) {
		PARECER_JURI = pARECER_JURI;
	}

	public String getCD_SITU() {
		return CD_SITU;
	}

	public void setCD_SITU(String cD_SITU) {
		CD_SITU = cD_SITU;
	}
	
	

}
