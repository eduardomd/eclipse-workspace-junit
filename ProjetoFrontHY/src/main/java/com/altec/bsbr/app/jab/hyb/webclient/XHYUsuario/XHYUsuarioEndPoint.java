//
// Generated By:JAX-WS RI 2.2.9-b130926.1035 (JAXB RI IBM 2.2.8-b130911.1802)
//


package com.altec.bsbr.app.jab.hyb.webclient.XHYUsuario;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;

@WebService(name = "xHY_UsuarioEndPoint", targetNamespace = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/")
@SOAPBinding(style = SOAPBinding.Style.RPC)
@XmlSeeAlso({
    ObjectFactory.class
})
public interface XHYUsuarioEndPoint {


    /**
     * 
     * @param arg0
     * @return
     *     returns java.lang.String
     * @throws WebServiceException
     */
    @WebMethod(operationName = "RecuperarDadosUsuario")
    @WebResult(partName = "return")
    @Action(input = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_UsuarioEndPoint/RecuperarDadosUsuarioRequest", output = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_UsuarioEndPoint/RecuperarDadosUsuarioResponse", fault = {
        @FaultAction(className = WebServiceException.class, value = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_UsuarioEndPoint/RecuperarDadosUsuario/Fault/WebServiceException")
    })
    public String recuperarDadosUsuario(
        @WebParam(name = "arg0", partName = "arg0")
        String arg0)
        throws WebServiceException
    ;

}
