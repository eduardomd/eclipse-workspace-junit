package com.altec.bsbr.app.hyb.dto;

import java.util.Date;

public class Hyb_arquivamentoos_model {
	
	private String ARQUIVO_GERAL;
	private String PASTA_L;
	private String DT_ARQUIVAMENTO_L;
	private String RESP_ARQ;
	private String DT_PRESC_L;
	private String CAIXA;
	private String PASTA_G;
	private String DT_ARQUIVAMENTO_G;
	private String EMPRESA_RESP;
	private String DT_PRESC_G;
	private String PARECER_JURI;
	private String CD_NOTI;
	private String CD_SITU;

	public Hyb_arquivamentoos_model() {
		
	}

	public Hyb_arquivamentoos_model(String aRQUIVO_GERAL, String pASTA_L, String dT_ARQUIVAMENTO_L, String rESP_ARQ,
			String dT_PRESC_L, String cAIXA, String pASTA_G, String dT_ARQUIVAMENTO_G, String eMPRESA_RESP, String dT_PRESC_G,
			String pARECER_JURI, String cD_NOTI, String cD_SITU) {
		ARQUIVO_GERAL = aRQUIVO_GERAL;
		PASTA_L = pASTA_L;
		DT_ARQUIVAMENTO_L = dT_ARQUIVAMENTO_L;
		RESP_ARQ = rESP_ARQ;
		DT_PRESC_L = dT_PRESC_L;
		CAIXA = cAIXA;
		PASTA_G = pASTA_G;
		DT_ARQUIVAMENTO_G = dT_ARQUIVAMENTO_G;
		EMPRESA_RESP = eMPRESA_RESP;
		DT_PRESC_G = dT_PRESC_G;
		PARECER_JURI = pARECER_JURI;
		CD_NOTI = cD_NOTI;
		CD_SITU = cD_SITU;
	}

	public String getARQUIVO_GERAL() {
		return ARQUIVO_GERAL;
	}
	public void setARQUIVO_GERAL(String aRQUIVO_GERAL) {
		ARQUIVO_GERAL = aRQUIVO_GERAL;
	}
	public String getPASTA_L() {
		return PASTA_L;
	}
	public void setPASTA_L(String pASTA_L) {
		PASTA_L = pASTA_L;
	}
	public String getRESP_ARQ() {
		return RESP_ARQ;
	}
	public void setRESP_ARQ(String rESP_ARQ) {
		RESP_ARQ = rESP_ARQ;
	}
	public String getCAIXA() {
		return CAIXA;
	}
	public void setCAIXA(String cAIXA) {
		CAIXA = cAIXA;
	}
	public String getPASTA_G() {
		return PASTA_G;
	}
	public void setPASTA_G(String pASTA_G) {
		PASTA_G = pASTA_G;
	}
	public String getEMPRESA_RESP() {
		return EMPRESA_RESP;
	}
	public void setEMPRESA_RESP(String eMPRESA_RESP) {
		EMPRESA_RESP = eMPRESA_RESP;
	}
	public String getPARECER_JURI() {
		return PARECER_JURI;
	}
	public void setPARECER_JURI(String pARECER_JURI) {
		PARECER_JURI = pARECER_JURI;
	}
	public String getCD_NOTI() {
		return CD_NOTI;
	}
	public void setCD_NOTI(String cD_NOTI) {
		CD_NOTI = cD_NOTI;
	}
	public String getCD_SITU() {
		return CD_SITU;
	}
	public void setCD_SITU(String cD_SITU) {
		CD_SITU = cD_SITU;
	}
	public String getDT_ARQUIVAMENTO_L() {
		return DT_ARQUIVAMENTO_L;
	}
	public void setDT_ARQUIVAMENTO_L(String dT_ARQUIVAMENTO_L) {
		DT_ARQUIVAMENTO_L = dT_ARQUIVAMENTO_L;
	}
	public String getDT_PRESC_L() {
		return DT_PRESC_L;
	}
	public void setDT_PRESC_L(String dT_PRESC_L) {
		DT_PRESC_L = dT_PRESC_L;
	}
	public String getDT_ARQUIVAMENTO_G() {
		return DT_ARQUIVAMENTO_G;
	}
	public void setDT_ARQUIVAMENTO_G(String dT_ARQUIVAMENTO_G) {
		DT_ARQUIVAMENTO_G = dT_ARQUIVAMENTO_G;
	}
	public String getDT_PRESC_G() {
		return DT_PRESC_G;
	}
	public void setDT_PRESC_G(String dT_PRESC_G) {
		DT_PRESC_G = dT_PRESC_G;
	}
	

}
