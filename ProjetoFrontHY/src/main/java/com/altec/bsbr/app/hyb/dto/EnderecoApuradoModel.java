package com.altec.bsbr.app.hyb.dto;

public class EnderecoApuradoModel {
	private String ruaAvenida;
	private String num;
	private String complemento;
	private String bairro;
	private String cep;
	//private String telefone;
	//private String celular;
	private String cidade;
	private String estado;
	
	//Tela
	private String telefoneDDD;
	private String telefoneNum;
	private String celularDDD;
	private String celularNum;
	
	public EnderecoApuradoModel() {
		
	}
		
	public EnderecoApuradoModel(String ruaAvenida, String num, String complemento, String bairro, String cep,
			String cidade, String estado, String telefoneDDD, String telefoneNum, String celularDDD,
			String celularNum) {
		super();
		this.ruaAvenida = ruaAvenida;
		this.num = num;
		this.complemento = complemento;
		this.bairro = bairro;
		this.cep = cep;
		this.cidade = cidade;
		this.estado = estado;
		this.telefoneDDD = telefoneDDD;
		this.telefoneNum = telefoneNum;
		this.celularDDD = celularDDD;
		this.celularNum = celularNum;
	}



	public String getRuaAvenida() {
		return ruaAvenida;
	}
	public void setRuaAvenida(String ruaAvenida) {
		this.ruaAvenida = ruaAvenida;
	}
	public String getNum() {
		return num;
	}
	public void setNum(String num) {
		this.num = num;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	
	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getTelefoneDDD() {
		return telefoneDDD;
	}

	public void setTelefoneDDD(String telefoneDDD) {
		this.telefoneDDD = telefoneDDD;
	}

	public String getTelefoneNum() {
		return telefoneNum;
	}

	public void setTelefoneNum(String telefoneNum) {
		this.telefoneNum = telefoneNum;
	}

	public String getCelularDDD() {
		return celularDDD;
	}

	public void setCelularDDD(String celularDDD) {
		this.celularDDD = celularDDD;
	}

	public String getCelularNum() {
		return celularNum;
	}

	public void setCelularNum(String celularNum) {
		this.celularNum = celularNum;
	}
		
}
