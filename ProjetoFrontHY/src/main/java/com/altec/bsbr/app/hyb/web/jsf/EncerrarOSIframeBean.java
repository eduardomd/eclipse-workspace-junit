package com.altec.bsbr.app.hyb.web.jsf;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.AprovarOsModel;
import com.altec.bsbr.app.hyb.dto.UsuarioIncModel;
import com.altec.bsbr.app.hyb.web.util.LogIncUtil;
import com.altec.bsbr.app.hyb.web.util.XHYUsuarioIncService;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAcoesOs.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAcoesOs.XHYAcoesOsEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmLog.XHYAdmLogEndPoint;

@Component("encerrarOSIframeBean")
@Scope("session")
public class EncerrarOSIframeBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String strNrSeqOs;
	private String retorno;
	private String cpfUsuario = "";
	
	private AprovarOsModel objRsModel;

	@Autowired
    private XHYAcoesOsEndPoint acoesOS;
	@Autowired
    private  XHYAdmLogEndPoint admLog;
	@Autowired
	private XHYUsuarioIncService user;
	
	@PostConstruct
	public void init() {
		//TODO Checar permissão
		carregarPagina();
	}
	
	public void carregarPagina() {
		strNrSeqOs = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs");
    	objRsModel = new AprovarOsModel();
		
        getObjRs();
		
		if (user != null) {			
			try {
				UsuarioIncModel dadosUser = user.getDadosUsuario();
				setCpfUsuario(dadosUser.getCpfUsuario());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}	
	}

	public void getObjRs() {
		try {
	        retorno = acoesOS.consultarOS(strNrSeqOs);
	        JSONObject rstemp = new JSONObject(retorno);
	        JSONArray pcursor = rstemp.getJSONArray("PCURSOR");
	        JSONObject f = pcursor.getJSONObject(0);
	        
	        objRsModel.setCODIGO(f.isNull("CODIGO") ? 0 : f.getInt("CODIGO"));
			objRsModel.setSITUACAO(f.isNull("SITUACAO") ? "" : f.getString("SITUACAO"));
			objRsModel.setCD_NOTI(f.isNull("CD_NOTI") ? 0 : f.getInt("CD_NOTI"));
			objRsModel.setMOT_REC_APROV(f.isNull("MOT_REC_APROV") ? "" : f.getString("MOT_REC_APROV"));
		} catch (WebServiceException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		    FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "hyb_erro.xhtml?&strErro=" + e.getMessage());
        }
	}

	public void recusarOS() {	
		try {
	        acoesOS.incluirProxWorkFlow(strNrSeqOs, "I", "3030", "TE", objRsModel.getMOT_REC_APROV(), false);
	        getObjRs();
	        
			admLog.incluirAcaoLog(strNrSeqOs, Integer.toString(LogIncUtil.REPROVAR_OS), cpfUsuario,  "");
			
			RequestContext.getCurrentInstance().execute("alert('O Encerramento da Ordem de Serviço nº "+ strNrSeqOs + " foi Reprovado!\\nNotificação enviada aos responsáveis.')");
		} catch (WebServiceException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		    FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "hyb_erro.xhtml?&strErro=" + e.getMessage());
        } catch (com.altec.bsbr.app.jab.hyb.webclient.XHYAdmLog.WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
    
    public String getStrNrSeqOs() {
		return this.strNrSeqOs;
	}
	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}

	public String getCpfUsuario() {
		return cpfUsuario;
	}
	public void setCpfUsuario(String cpfUsuario) {
		this.cpfUsuario = cpfUsuario;
	}

	public AprovarOsModel getObjRsModel() {
		return objRsModel;
	}
	public void setObjRsModel(AprovarOsModel objRsModel) {
		this.objRsModel = objRsModel;
	}

}