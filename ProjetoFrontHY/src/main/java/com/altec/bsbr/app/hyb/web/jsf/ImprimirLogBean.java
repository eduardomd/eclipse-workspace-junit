package com.altec.bsbr.app.hyb.web.jsf;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.altec.bsbr.app.hyb.dto.RsAdmLog;


@ManagedBean(name="ImprimirLogBean")
@ViewScoped
public class ImprimirLogBean implements Serializable {
    
	final Boolean MOCK = true;
	
	private static final long serialVersionUID = 1L;
	private List<RsAdmLog> objRsAdmLog;

	//Query String - valores usados na consulta backend
	private Object dteInicio = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("qsPerInicio");
	private Object dteFim = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("qsPerFim");
	private Object strCodUsua = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("qsCodUsua");
	private Object intCodAcaoUsua = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("qsCodAcaoUsua");
	
	//Data do rodap�
	private Date dtNow = new Date();
	
	public Date getDtNow() {
		return dtNow;
	}

	public List<RsAdmLog> getObjRsAdmLog() {
		return objRsAdmLog;
	}
	
	public Integer randInt(Integer inicio, Integer fim) {
		return inicio + (int)Math.round(Math.random() * (fim - inicio));
	}
	
	public Date randomDate() {
        GregorianCalendar gc = new GregorianCalendar();

        gc.set(gc.YEAR, randInt(2000, 2010));
        gc.set(gc.DAY_OF_YEAR, randInt(1, gc.getActualMaximum(gc.DAY_OF_YEAR)));
        gc.set(gc.HOUR, randInt(00, 23));
        gc.set(gc.MINUTE, randInt(0, 60));
        gc.set(gc.SECOND, randInt(0, 60));
        return gc.getTime();
	}
	
	public ImprimirLogBean() {
		objRsAdmLog = new ArrayList<RsAdmLog>();
		
		//Teste da query string
		System.out.println(this.dteInicio+"-"+this.dteFim+"-"+this.strCodUsua+"-"+this.intCodAcaoUsua);

		/* Mock do backend */
		if (this.MOCK) {
			for(int i = 1; i <= 5; i++) {
				RsAdmLog admlog = new RsAdmLog();
				admlog.setDT_ACAO(new SimpleDateFormat("dd/MM/yyyy").format(this.randomDate()));
				admlog.setDT_HORA(new SimpleDateFormat("HH:mm:ss").format(this.randomDate()));
				admlog.setCOD_USUARIO("login_teste_numero"+i);
				admlog.setNM_ACAO("teste"+i);
				admlog.setNR_OS(String.valueOf(i));
				objRsAdmLog.add(admlog);
			}
		}
	}
         

}