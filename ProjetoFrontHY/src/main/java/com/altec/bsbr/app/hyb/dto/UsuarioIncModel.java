package com.altec.bsbr.app.hyb.dto;

public class UsuarioIncModel {
	
	private String matriculaUsuario;
	private String nomeUsuario;
	private String cpfUsuario;
	private String loginUsuario;
	private String cdAreaUsuario;
	private String nomeAreaUsuario;
	private String nmEmailusuario;
	private String cdCargoUsuario;
	
	public UsuarioIncModel() {}
	
	public UsuarioIncModel(String matriculaUsuario, String nomeUsuario, String cpfUsuario, String loginUsuario, String cdAreaUsuario, 
			String nomeAreaUsuario, String nmEmailusuario, String cdCargoUsuario) {
		this.matriculaUsuario = matriculaUsuario;
		this.nomeUsuario = nomeUsuario;
		this.cpfUsuario = cpfUsuario;
		this.loginUsuario = loginUsuario;
		this.cdAreaUsuario = cdAreaUsuario;
		this.nomeAreaUsuario = nomeAreaUsuario;
		this.nmEmailusuario = nmEmailusuario;
		this.cdCargoUsuario = cdCargoUsuario;
	}
	
	public String getMatriculaUsuario() {
		return matriculaUsuario;
	}

	public void setMatriculaUsuario(String matriculaUsuario) {
		this.matriculaUsuario = matriculaUsuario;
	}

	public String getNomeUsuario() {
		return nomeUsuario;
	}

	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}

	public String getCpfUsuario() {
		return cpfUsuario;
	}

	public void setCpfUsuario(String cpfUsuario) {
		this.cpfUsuario = cpfUsuario;
	}

	public String getLoginUsuario() {
		return loginUsuario;
	}

	public void setLoginUsuario(String loginUsuario) {
		this.loginUsuario = loginUsuario;
	}

	public String getCdAreaUsuario() {
		return cdAreaUsuario;
	}

	public void setCdAreaUsuario(String cdAreaUsuario) {
		this.cdAreaUsuario = cdAreaUsuario;
	}

	public String getNomeAreaUsuario() {
		return nomeAreaUsuario;
	}

	public void setNomeAreaUsuario(String nomeAreaUsuario) {
		this.nomeAreaUsuario = nomeAreaUsuario;
	}

	public String getNmEmailusuario() {
		return nmEmailusuario;
	}

	public void setNmEmailusuario(String nmEmailusuario) {
		this.nmEmailusuario = nmEmailusuario;
	}

	public String getCdCargoUsuario() {
		return cdCargoUsuario;
	}

	public void setCdCargoUsuario(String cdCargoUsuario) {
		this.cdCargoUsuario = cdCargoUsuario;
	}


	

}
