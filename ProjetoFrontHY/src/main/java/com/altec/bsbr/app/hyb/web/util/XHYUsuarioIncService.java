package com.altec.bsbr.app.hyb.web.util;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;

import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.altec.bsbr.app.hyb.dto.UsuarioIncModel;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAcoesOs.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAcoesOs.XHYAcoesOsEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYUsuario.XHYUsuarioEndPoint;
import com.altec.bsbr.fw.security.SecurityInfo;
import com.altec.bsbr.fw.security.authorization.Authorization;

@Service("XHYUsuarioInc")
public class XHYUsuarioIncService {
	
	@Autowired
	private XHYUsuarioEndPoint usuario;
	@Autowired
	private Authorization authorization;
	
	public UsuarioIncModel getDadosUsuario() throws Exception {
		// Recupera o usuario corrente da rede
		SecurityInfo secInfo = SecurityInfo.getCurrent();
		String userName = secInfo.getUserName().replaceAll("[^0-9]", "");
		return this.getDadosUsuario(userName);
	}

	public UsuarioIncModel getDadosUsuario(String username) throws Exception {
		UsuarioIncModel usuarioIncModel = new UsuarioIncModel();
		if (username.isEmpty()) 
			throw new Exception("Sessão Expirada!");

		String retorno = "";
		try {
			retorno = usuario.recuperarDadosUsuario(username);
			
			JSONObject dadosUsuario = new JSONObject(retorno);
			JSONArray pcursor = dadosUsuario.getJSONArray("PCURSOR");
			for (int i = 0; i < pcursor.length(); i++) {
				JSONObject f = pcursor.getJSONObject(i);
 
				usuarioIncModel
						.setMatriculaUsuario(f.isNull("MATRICULA") == true ? "" : f.get("MATRICULA").toString());
				usuarioIncModel
						.setNomeUsuario(f.isNull("NOME_USUARIO") == true ? "" : f.get("NOME_USUARIO").toString());
				usuarioIncModel
						.setCpfUsuario(f.isNull("CPF") == true ? "" : f.get("CPF").toString());
				usuarioIncModel
						.setLoginUsuario(f.isNull("LOGIN") == true ? "" : f.get("LOGIN").toString());
				usuarioIncModel
						.setCdAreaUsuario(f.isNull("CODIGO_AREA") == true ? "" : f.get("CODIGO_AREA").toString());
				usuarioIncModel
						.setNomeAreaUsuario(f.isNull("NOME_AREA") == true ? "" : f.get("NOME_AREA").toString());
				usuarioIncModel
						.setNmEmailusuario(f.isNull("EMAIL") == true ? "" : f.get("EMAIL").toString());
				usuarioIncModel
						.setCdCargoUsuario(f.isNull("CODIGO_CARGO") == true ? "" : f.get("CODIGO_CARGO").toString());
			}
		} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYUsuario.WebServiceException e) {
			throw new Exception(e.getMessage());
		}
		

		return usuarioIncModel; 
	}

	public void verificarPermissao(String[] perfis) {				
		SecurityInfo securityInfo = new SecurityInfo();
		
		securityInfo = SecurityInfo.getCurrent();
		
		for (String perfil : perfis) {
	  		if (authorization.isUserAuthorized(SecurityInfo.getCurrent(), perfil)) {
	  			return;		  			
	  		};
		}

		try {
			FacesContext.getCurrentInstance().getExternalContext()
				.redirect("hyb_erro.xhtml?strErro=Acesso negado!!! Caso necessite acessar essa página, entre em contato com o administrador do sistema");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

//	public Boolean verificaPermissaoEquipe(UsuarioIncModel usuario, String nrOs) throws Exception {
//		Boolean ret = false;
//		String retornoAcoes = "";
//		JSONArray pCursor = null;
//		
//		retornoAcoes = acoes.consultarEquipeOs(nrOs, "");
//		JSONObject consulta = new JSONObject(retornoAcoes);
//		pCursor = consulta.getJSONArray("PCURSOR");
//	
//		if (retornoAcoes.toUpperCase().contains("XHY")) {
//			ret = false;
//			throw new Exception(retornoAcoes);
//		}
//
//		if (!usuario.getCdCargoUsuario().equals("B70555")) {
//			for (int i = 0; i < pCursor.length(); i++) 
//				if(pCursor.getJSONObject(i).getString("CPF").equals(usuario.getCpfUsuario()))
//					ret = true;
//		} else {
//			ret = true;
//		}
//				
//		return ret;
//	}
	
//	public Boolean verificaPermissaoArea(UsuarioIncModel usuario, String cdArea) throws Exception {
//		Boolean ret = false;
//		if (!usuario.getCdCargoUsuario().equals("B70555")) {
//			ret = cdArea.equals(usuario.getCdAreaUsuario()) ? true : false;
//			if (ret) 
//				throw new Exception("Permissão de acesso à esta Ordem de Serviço negada! \n - Área do usuário diferente da área da Ordem de Serviço");			
//		}
//		else {
//			ret = true;
//		}
//		return ret;		
//	}
}
