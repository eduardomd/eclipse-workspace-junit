package com.altec.bsbr.app.hyb.dto;

public class PEB6_PEM8340AeaResponse {

	public PEB6_PEM8340AeaResponse() {}
	
	public PEB6_PEM8340AeaResponse(String dC_FORMATO, String pESUCOP, String pESUBES, String fECINI) {
		super();
		DC_FORMATO = dC_FORMATO;
		PESUCOP = pESUCOP;
		PESUBES = pESUBES;
		FECINI = fECINI;
	}

	private String DC_FORMATO;

	private String PESUCOP;
	
	private String PESUBES;
	
	private String FECINI;
	
	public String getDC_FORMATO() {
		return DC_FORMATO;
	}
	
	public void setDC_FORMATO(String dC_FORMATO) {
		DC_FORMATO = dC_FORMATO;
	}

	public String getPESUCOP() {
		return PESUCOP;
	}

	public void setPESUCOP(String pESUCOP) {
		PESUCOP = pESUCOP;
	}

	public String getPESUBES() {
		return PESUBES;
	}

	public void setPESUBES(String pESUBES) {
		PESUBES = pESUBES;
	}
	
	public String getFECINI() {
		return FECINI;
	}

	public void setFECINI(String fECINI) {
		FECINI = fECINI;
	}
}
