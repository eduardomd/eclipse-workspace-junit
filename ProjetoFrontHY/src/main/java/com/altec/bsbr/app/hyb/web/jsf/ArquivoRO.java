package com.altec.bsbr.app.hyb.web.jsf;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

@ManagedBean(name="arquivoRO")
@ViewScoped
public class ArquivoRO {
	
	private static final long serialVersionUID = 1L;
	
	private String dateMov;
	
	private String area;  
    private Map<String,String> areas = new HashMap<String, String>();
    
    @PostConstruct
    public void init() {
    	areas = new HashMap<String, String>();
    	areas.put("Contábil", "accounting");
    	areas.put("Economica","economic");
    	areas.put("Investimento","investment");
    	areas.put("Negocio","business");
    	areas.put("Auditoria","audit");
    	areas.put("Juridico","legal");
    }
    
    public String getDateMov() {
    	return dateMov;
    }
    
    public void setDateMov(String dateMov) {
    	this.dateMov = dateMov;
    }
    
    public String getArea() {
        return area;
    }
 
    public void setArea(String area) {
        this.area = area;
    }
    
    public Map<String, String> getAreas() {
        return areas;
    }
  
    public void btnSubmit() {
    	RequestContext context = RequestContext.getCurrentInstance(); 
    	context.execute("PF('dlg1').show();");
    }
}
