package com.altec.bsbr.app.hyb.web.jsf;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
/*import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;*/

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("TesteProvider")
@Scope("session")
public class TesteProvider {

	public String getHTTP_ACCEPT_LANGUAGE() {
		return System.getenv("HTTP_ACCEPT_LANGUAGE");
	}


}
