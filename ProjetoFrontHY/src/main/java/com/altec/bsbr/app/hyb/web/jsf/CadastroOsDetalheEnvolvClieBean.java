package com.altec.bsbr.app.hyb.web.jsf;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.apache.commons.lang.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.CadastroOsDetalheEnvolvClie;
import com.altec.bsbr.app.hyb.dto.CadastroOsDetalheEnvolvClieEnde;
import com.altec.bsbr.app.hyb.dto.ObjRsOsAssociada;
import com.altec.bsbr.app.hyb.web.util.rsRest;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsParticip.XHYCadOsParticipEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;

@Component("cadastroOsDetalheEnvolvClieBean")
@Scope("session")
public class CadastroOsDetalheEnvolvClieBean extends BasicBBean {
private static final long serialVersionUID = 1L;
	
	private CadastroOsDetalheEnvolvClie cadastroOsDetalheEnvolvClie;
	
	private String iCountLinhas;
	private String Request;
	private List<rsRest> objRsRest;
	private String NrSeqPart;
	private List<ObjRsOsAssociada> objRsOsAssociadas;
	private List<CadastroOsDetalheEnvolvClieEnde> objRsEndeClienteSelect;
	private CadastroOsDetalheEnvolvClieEnde objRsEndeCliente;
	
	private Locale ptBR = new Locale("pt", "BR");
	
	private String selectedEndereco;
	
	public String getRequest() {
		return Request;
	}

	public void setRequest(String request) {
		Request = request;
	}
	
	@Autowired
	private XHYCadOsParticipEndPoint cadOsParticipEndPoint;
	
	@PostConstruct
	public void init() {
		this.NrSeqPart = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("NrSeqPart");
		consultarDetalheParticipacao(Double.parseDouble(this.NrSeqPart));
		fillTblOsAssociada(cadastroOsDetalheEnvolvClie.getCpf_cnpj(), cadastroOsDetalheEnvolvClie.getOs());
		consultarRestricaoPart(Double.parseDouble(this.NrSeqPart));
		consultarEndeClienteSelect(Double.parseDouble(this.NrSeqPart));
		consultarEndeCliente();
		System.out.println("INIT  aaa");
		
	}
	
	public void fillTblOsAssociada(String cpf_cnpj, String os) {
		try {
			System.out.println("fillTblOsAssociada");
			objRsOsAssociadas = new ArrayList<ObjRsOsAssociada>();
			JSONObject json = new JSONObject(cadOsParticipEndPoint.consultarOSAssociada(cpf_cnpj));
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			
			for (int i = 0; i < pcursor.length(); i++) {
				JSONObject item = pcursor.getJSONObject(i);
				ObjRsOsAssociada OsAssociadaItem = new ObjRsOsAssociada();
				OsAssociadaItem.setORDEM(item.isNull("ORDEM")? "" : item.get("ORDEM").toString());
				OsAssociadaItem.setAREA(item.isNull("AREA")? "" : item.get("AREA").toString());
				OsAssociadaItem.setCD_STATUS(item.isNull("CD_STATUS")? "" : item.get("CD_STATUS").toString());
				OsAssociadaItem.setSTATUS(item.isNull("STATUS")? "" : item.get("STATUS").toString());

				if(!os.equals(OsAssociadaItem.getORDEM())) {
					objRsOsAssociadas.add(OsAssociadaItem);
				}
			}
			System.out.println(objRsOsAssociadas.size());
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void consultarRestricaoPart(Double NrSeqPart) {
		
		try {
			objRsRest = new ArrayList<rsRest>();
			
			JSONObject json = new JSONObject(cadOsParticipEndPoint.consultarRestricaoPart(NrSeqPart));
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			
			for (int i = 0; i < pcursor.length(); i++) {
				JSONObject item = pcursor.getJSONObject(i);
				rsRest objRsRestItem = new rsRest();				
				objRsRestItem.setDATA(item.isNull("DATA")? "" : item.get("DATA").toString());
				objRsRestItem.setESPECIFICACAO(item.isNull("ESPECIFICACAO")? "" : item.get("ESPECIFICACAO").toString());
				objRsRestItem.setINSTITUICAO(item.isNull("INSTITUICAO")? "" : item.get("INSTITUICAO").toString());
				objRsRestItem.setSQ_RESTRICAO(item.isNull("SQ_RESTRICAO")? "" : item.get("SQ_RESTRICAO").toString());
				objRsRestItem.setVALOR(item.isNull("VALOR")? "" : item.getBigDecimal("VALOR").toString());
				
				
				if(!objRsRestItem.getVALOR().isEmpty()) {
					objRsRestItem.setVALOR(formataValorRecebido(objRsRestItem.getVALOR()));
				}
				this.objRsRest.add(objRsRestItem);
		
			}
			System.out.println(objRsRest.size() + " sizes   ");
					
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void consultarEndeClienteSelect(Double NrSeqPart) {
		try {
			objRsEndeClienteSelect = new ArrayList<CadastroOsDetalheEnvolvClieEnde>();
			
			JSONObject json = new JSONObject(cadOsParticipEndPoint.consultarEndeCliente(NrSeqPart, 0));
			JSONObject jsonPrincipal = new JSONObject(cadOsParticipEndPoint.consultarEndeCliente(NrSeqPart, 1));
			
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			JSONArray pcursorPrincipal = jsonPrincipal.getJSONArray("PCURSOR");
			
			JSONObject itemPrincipal = pcursorPrincipal.getJSONObject(0);
			
			// Set the default selected item as the main address
			this.selectedEndereco = itemPrincipal.get("SQ_ENDE").toString();
			
			System.out.println("ENDE CURaSOR  : " + pcursor);
			
			if(pcursor.length() > 0) {
				for (int i = 0; i < pcursor.length(); i++) {
					CadastroOsDetalheEnvolvClieEnde clienteEndeModel = new CadastroOsDetalheEnvolvClieEnde();
					JSONObject item = pcursor.getJSONObject(i);
					
					String tipo = item.get("TIPO").toString();
					String sqEnde = item.get("SQ_ENDE").toString();
					clienteEndeModel.setTipo(tipo);
					clienteEndeModel.setSqEnde(sqEnde);
					objRsEndeClienteSelect.add(clienteEndeModel);
			
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void consultarEndeCliente() {
		System.out.println("SELECTED ITEM: " + this.selectedEndereco);
		try {
			objRsEndeCliente = new CadastroOsDetalheEnvolvClieEnde();
			
			JSONObject json = new JSONObject(cadOsParticipEndPoint.consultarEndeCliente(Double.parseDouble(NrSeqPart), 0));
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			
			System.out.println("ENDE CURSOR: " + pcursor);
			
			if(pcursor.length() > 0) {
				for (int i = 0; i < pcursor.length(); i++) {

					JSONObject item = pcursor.getJSONObject(i);
					System.out.println();
					if (item.get("SQ_ENDE").toString().equals(this.selectedEndereco)) {
						String tipo = item.isNull("TIPO") ? "" : item.get("TIPO").toString();
						String sqEnde = item.isNull("SQ_ENDE") ? "" : item.get("SQ_ENDE").toString();
						String logradouro = item.isNull("LOGRADOURO") ? "" : item.get("LOGRADOURO").toString();
						String numero = item.isNull("NUMERO") ? "" : item.get("NUMERO").toString();
						String complemento = item.isNull("COMPLEMENTO") ? "" : item.get("COMPLEMENTO").toString();
						String bairro = item.isNull("BAIRRO") ? "" : item.get("BAIRRO").toString();
						String cep = item.isNull("CEP") ? "" : item.get("CEP").toString();
						String cidade = item.isNull("CIDADE") ? "" : item.get("CIDADE").toString();
						String uf = item.isNull("UF") ? "" : item.get("UF").toString();
						String telefone = item.isNull("TELEFONE") ? "" : item.get("TELEFONE").toString();
						String celular = item.isNull("CELULAR") ? "" : item.get("CELULAR").toString();
						
						System.out.println("SQ_ENDE: " + sqEnde + " - " + bairro);
						
						objRsEndeCliente.setTipo(tipo);
						objRsEndeCliente.setSqEnde(sqEnde);
						objRsEndeCliente.setTelefone(telefone);
						objRsEndeCliente.setBairro(bairro);
						objRsEndeCliente.setCelular(celular);
						objRsEndeCliente.setCep(cep);
						objRsEndeCliente.setCidade(cidade);
						objRsEndeCliente.setComplemento(complemento);
						objRsEndeCliente.setLogradouro(logradouro);
						objRsEndeCliente.setNumero(numero);
						objRsEndeCliente.setUf(uf);
					}
					
				}
		
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void consultarDetalheParticipacao(Double NrSeqPart) {
		try {
			System.out.println("FUNCTION");
			JSONObject json = new JSONObject(cadOsParticipEndPoint.consultarDetalhePart(NrSeqPart));
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			System.out.println("CURSOR: " + pcursor);
			if(pcursor.length() > 0) {
				cadastroOsDetalheEnvolvClie = new CadastroOsDetalheEnvolvClie();
				JSONObject item = pcursor.getJSONObject(0);
				cadastroOsDetalheEnvolvClie.setMatricula(item.isNull("MATRICULA")? "": item.get("MATRICULA").toString());
				cadastroOsDetalheEnvolvClie.setNome(item.isNull("NOME")? "": item.get("NOME").toString());
				cadastroOsDetalheEnvolvClie.setOs(item.isNull("OS")? "": item.get("OS").toString());
				cadastroOsDetalheEnvolvClie.setCpf_cnpj(item.isNull("CPF_CNPJ")? "": item.get("CPF_CNPJ").toString());
				
				cadastroOsDetalheEnvolvClie.setRg(item.isNull("RG")? "": item.get("RG").toString());
				cadastroOsDetalheEnvolvClie.setOrgaoEmissor(item.isNull("ORGAO_EMISSOR")? "": item.get("ORGAO_EMISSOR").toString());
				cadastroOsDetalheEnvolvClie.setDtEmissaoRg(item.isNull("DT_EMISSAO_RG")? "": item.get("DT_EMISSAO_RG").toString());
				cadastroOsDetalheEnvolvClie.setProfissao(item.isNull("PROFISSAO")? "": item.get("PROFISSAO").toString());
				cadastroOsDetalheEnvolvClie.setRenda(item.isNull("RENDA")? "": formataValorRecebido(item.get("RENDA").toString()));
				cadastroOsDetalheEnvolvClie.setDtUltiAltrEnde(item.isNull("DT_ULTI_ALTR_ENDE")? "": item.get("DT_ULTI_ALTR_ENDE").toString());

				cadastroOsDetalheEnvolvClie.setEndereco(item.isNull("ENDERECO")? "": item.get("ENDERECO").toString());
				cadastroOsDetalheEnvolvClie.setNumero(item.isNull("NUMERO")? "": item.get("NUMERO").toString());
				cadastroOsDetalheEnvolvClie.setComplemento(item.isNull("COMPLEMENTO")? "": item.get("COMPLEMENTO").toString());
				cadastroOsDetalheEnvolvClie.setBairro(item.isNull("BAIRRO")? "": item.get("BAIRRO").toString());
				cadastroOsDetalheEnvolvClie.setCep(item.isNull("CEP")? "": item.get("CEP").toString());
				cadastroOsDetalheEnvolvClie.setTelefone(item.isNull("TELEFONE")? "": item.get("TELEFONE").toString());
				cadastroOsDetalheEnvolvClie.setCelular(item.isNull("CELULAR")? "": item.get("CELULAR").toString());
				
				cadastroOsDetalheEnvolvClie.setOper_vencer(item.isNull("OPER_VENCER")? "": item.getBigDecimal("OPER_VENCER").toString());
				cadastroOsDetalheEnvolvClie.setOper_vencida(item.isNull("OPER_VENCIDA")? "": item.getBigDecimal("OPER_VENCIDA").toString());
				cadastroOsDetalheEnvolvClie.setOper_creli(item.isNull("OPER_CRELI")? "": item.getBigDecimal("OPER_CRELI").toString());
				cadastroOsDetalheEnvolvClie.setOper_preju(item.isNull("OPER_PREJU")? "": item.getBigDecimal("OPER_PREJU").toString());
				cadastroOsDetalheEnvolvClie.setOper_passiva(item.isNull("OPER_PASSIVA")? "": item.getBigDecimal("OPER_PASSIVA").toString());
				cadastroOsDetalheEnvolvClie.setReciprocidade(item.isNull("RECIPROCIDADE")? "": item.getBigDecimal("RECIPROCIDADE").toString());
				
				cadastroOsDetalheEnvolvClie.setStrTpPart(item.isNull("TP_PARTICIPACAO")? "": item.get("TP_PARTICIPACAO").toString());
				cadastroOsDetalheEnvolvClie.setParticipacao(item.isNull("PARTICIPACAO")? "": item.get("PARTICIPACAO").toString());
				cadastroOsDetalheEnvolvClie.setDt_abertura(item.isNull("DT_ABERTURA")? "": item.get("DT_ABERTURA").toString());
								
				if(!cadastroOsDetalheEnvolvClie.getOper_vencer().isEmpty()) {
					cadastroOsDetalheEnvolvClie.setOper_vencer(formataValorRecebido(cadastroOsDetalheEnvolvClie.getOper_vencer()));
				}
				if(!cadastroOsDetalheEnvolvClie.getOper_vencida().isEmpty()) {
					cadastroOsDetalheEnvolvClie.setOper_vencida(formataValorRecebido(cadastroOsDetalheEnvolvClie.getOper_vencida()));
				}
				if(!cadastroOsDetalheEnvolvClie.getOper_creli().isEmpty()) {
					cadastroOsDetalheEnvolvClie.setOper_creli(formataValorRecebido(cadastroOsDetalheEnvolvClie.getOper_creli()));
				}
				if(!cadastroOsDetalheEnvolvClie.getOper_preju().isEmpty()) {
					cadastroOsDetalheEnvolvClie.setOper_preju(formataValorRecebido(cadastroOsDetalheEnvolvClie.getOper_preju()));
				}
				if(!cadastroOsDetalheEnvolvClie.getOper_passiva().isEmpty()) {
					cadastroOsDetalheEnvolvClie.setOper_passiva(formataValorRecebido(cadastroOsDetalheEnvolvClie.getOper_passiva()));
				}
				if(!cadastroOsDetalheEnvolvClie.getReciprocidade().isEmpty()) {
					cadastroOsDetalheEnvolvClie.setReciprocidade(formataValorRecebido(cadastroOsDetalheEnvolvClie.getReciprocidade()));
				}
				
				
				cadastroOsDetalheEnvolvClie.setCep(StringUtils.leftPad(cadastroOsDetalheEnvolvClie.getCep(), 8, "0"));
				cadastroOsDetalheEnvolvClie.setCep(cadastroOsDetalheEnvolvClie.getCep().substring(0, 5) + "-" + cadastroOsDetalheEnvolvClie.getCep().substring(5, 8));
				
				cadastroOsDetalheEnvolvClie.setTelefone(StringUtils.leftPad(cadastroOsDetalheEnvolvClie.getTelefone(), 11, "0"));
				cadastroOsDetalheEnvolvClie.setCelular(StringUtils.leftPad(cadastroOsDetalheEnvolvClie.getCelular(), 11, "0"));

				cadastroOsDetalheEnvolvClie.setDddTelefone(cadastroOsDetalheEnvolvClie.getTelefone().substring(0, 2));
				cadastroOsDetalheEnvolvClie.setNumeroTelefone(cadastroOsDetalheEnvolvClie.getTelefone().substring(2, 11));
				
				cadastroOsDetalheEnvolvClie.setDddCelular(cadastroOsDetalheEnvolvClie.getCelular().substring(0, 2));
				cadastroOsDetalheEnvolvClie.setNumeroCelular(cadastroOsDetalheEnvolvClie.getCelular().substring(2, 11));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String formataValorParaEnvio(String valorRecebido) {
		String valor = valorRecebido.replace(",", "").replace(".", "");
		int size = valor.length() - 2;
		return valor.substring(0, size).concat(".").concat(valor.substring(size, valor.length()));
	}
	
	public String formataValorRecebido(String valorRecebido) {
		String valor = NumberFormat.getCurrencyInstance(ptBR).format(Double.parseDouble(valorRecebido));
		return valor.replace("R$", "").trim();
	}
	
	public void salvar() {
		System.out.println("chamou o salvar ----->>");
		try {
			
			System.out.println(this.NrSeqPart +","+ this.cadastroOsDetalheEnvolvClie.getEndereco()+","+ this.cadastroOsDetalheEnvolvClie.getNumero()+","+ this.cadastroOsDetalheEnvolvClie.getComplemento()+","+ this.cadastroOsDetalheEnvolvClie.getBairro()+","+ this.cadastroOsDetalheEnvolvClie.getCep().replace("-", "")+","+this.cadastroOsDetalheEnvolvClie.getTelefone()+","+ this.cadastroOsDetalheEnvolvClie.getCelular());
			if(cadOsParticipEndPoint.atualizarEnderecoParticipante(this.NrSeqPart, this.cadastroOsDetalheEnvolvClie.getEndereco(), this.cadastroOsDetalheEnvolvClie.getNumero(), this.cadastroOsDetalheEnvolvClie.getComplemento(), this.cadastroOsDetalheEnvolvClie.getBairro(), this.cadastroOsDetalheEnvolvClie.getCep().replace("-", ""), "", "", this.cadastroOsDetalheEnvolvClie.getDddTelefone() + this.cadastroOsDetalheEnvolvClie.getNumeroTelefone(), this.cadastroOsDetalheEnvolvClie.getDddCelular() + this.cadastroOsDetalheEnvolvClie.getNumeroCelular())) {
				System.out.println(this.NrSeqPart+","+ this.cadastroOsDetalheEnvolvClie.getOper_vencer().replace(".", "") +","+ this.cadastroOsDetalheEnvolvClie.getOper_vencida().replace(".", "")+","+ this.cadastroOsDetalheEnvolvClie.getOper_creli().replace(".", "")+","+ this.cadastroOsDetalheEnvolvClie.getOper_preju().replace(".", "")+","+ this.cadastroOsDetalheEnvolvClie.getOper_passiva().replace(".", "")+","+ this.cadastroOsDetalheEnvolvClie.getReciprocidade().replace(".", ""));
				if(cadOsParticipEndPoint.fnUpdposicaoRisco(this.NrSeqPart, this.cadastroOsDetalheEnvolvClie.getOper_vencer().replace(".", ""), this.cadastroOsDetalheEnvolvClie.getOper_vencida().replace(".", ""), this.cadastroOsDetalheEnvolvClie.getOper_creli().replace(".", ""), this.cadastroOsDetalheEnvolvClie.getOper_preju().replace(".", ""), this.cadastroOsDetalheEnvolvClie.getOper_passiva().replace(".", ""), this.cadastroOsDetalheEnvolvClie.getReciprocidade().replace(".", ""))) {
					
					System.out.println("Chamar for");
					SimpleDateFormat newFormat = new SimpleDateFormat("dd/MM/yyyy");
					SimpleDateFormat newFormat2 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
					
					for (int i = 0; i < this.objRsRest.size(); i++) {
						this.objRsRest.get(i).getSQ_RESTRICAO();
						this.objRsRest.get(i).getINSTITUICAO();
						this.objRsRest.get(i).getESPECIFICACAO();
						this.objRsRest.get(i).getVALOR();
												
						System.out.println(this.objRsRest.get(i).getVALOR());

						if(this.objRsRest.get(i).getSQ_RESTRICAO().isEmpty()) {

							if(!this.objRsRest.get(i).getDATA().isEmpty()) {
								Date data = newFormat.parse(this.objRsRest.get(i).getDATA());
								this.objRsRest.get(i).setDATA(newFormat2.format(data));
							}
							
							if(!this.objRsRest.get(i).getVALOR().isEmpty()) {
								this.objRsRest.get(i).setVALOR(formataValorParaEnvio(this.objRsRest.get(i).getVALOR()));
							}
							
							System.out.println(Integer.valueOf(this.NrSeqPart)+" ,"+ this.objRsRest.get(i).getINSTITUICAO()+" ,"+ this.objRsRest.get(i).getESPECIFICACAO()+" ,"+ this.objRsRest.get(i).getVALOR()+" ,"+ this.objRsRest.get(i).getDATA());
							cadOsParticipEndPoint.inserirRestricaoParticipante(Integer.valueOf(this.NrSeqPart), this.objRsRest.get(i).getINSTITUICAO(), this.objRsRest.get(i).getESPECIFICACAO(), this.objRsRest.get(i).getVALOR(), this.objRsRest.get(i).getDATA());
						}else {
							System.out.println(this.objRsRest.get(i).getSQ_RESTRICAO() +" ,"+ this.objRsRest.get(i).getINSTITUICAO()+" ,"+ this.objRsRest.get(i).getESPECIFICACAO()+" ,"+ this.objRsRest.get(i).getVALOR().replace(".", "")+" ,"+ this.objRsRest.get(i).getDATA());
							cadOsParticipEndPoint.fnUpdRestrParticipante(this.objRsRest.get(i).getSQ_RESTRICAO(), this.objRsRest.get(i).getINSTITUICAO(), this.objRsRest.get(i).getESPECIFICACAO(), this.objRsRest.get(i).getVALOR().replace(".", ""), this.objRsRest.get(i).getDATA());
						}
					}

					RequestContext.getCurrentInstance().execute("alert('Registro(s) atualizado(s) com sucesso.')");
					
					consultarRestricaoPart(Double.parseDouble(this.NrSeqPart));
					
				}else
					RequestContext.getCurrentInstance().execute("alert('Não foi possível atualizar o(s) registro(s).')");	
			}else
				RequestContext.getCurrentInstance().execute("alert('Não foi possível atualizar o(s) registro(s).')");
			
		} catch (Exception e) {
			e.printStackTrace();
			RequestContext.getCurrentInstance().execute("alert('Não foi possível atualizar o(s) registro(s).')");
		}
		
	}
	
	public void insereLinha() {
		System.out.println("Chamou insere Linha");
		objRsRest.add(new rsRest("", "", "", "", "") );
		
	}
	
	public void fnExcluiRestricao() {
		try {
			String strIdRestPart = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strIdRestPart").toString();
			cadOsParticipEndPoint.excluirRestricaoPart(Double.parseDouble(strIdRestPart));
			consultarRestricaoPart(Double.parseDouble(this.NrSeqPart));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	

	public CadastroOsDetalheEnvolvClie getCadastroOsDetalheEnvolvClie() {
		return cadastroOsDetalheEnvolvClie;
	}

	public void setCadastroOsDetalheEnvolvClie(CadastroOsDetalheEnvolvClie cadastroOsDetalheEnvolvClie) {
		this.cadastroOsDetalheEnvolvClie = cadastroOsDetalheEnvolvClie;
	}

	public List<rsRest> getObjRsRest() {
		return objRsRest;
	}

	public void setObjRsRest(List<rsRest> objRsRest) {
		this.objRsRest = objRsRest;
	}

	public String getNrSeqPart() {
		return NrSeqPart;
	}

	public void setNrSeqPart(String nrSeqPart) {
		NrSeqPart = nrSeqPart;
	}

	public List<ObjRsOsAssociada> getObjRsOsAssociadas() {
		return objRsOsAssociadas;
	}

	public void setObjRsOsAssociadas(List<ObjRsOsAssociada> objRsOsAssociadas) {
		this.objRsOsAssociadas = objRsOsAssociadas;
	}

	public XHYCadOsParticipEndPoint getCadOsParticipEndPoint() {
		return cadOsParticipEndPoint;
	}

	public void setCadOsParticipEndPoint(XHYCadOsParticipEndPoint cadOsParticipEndPoint) {
		this.cadOsParticipEndPoint = cadOsParticipEndPoint;
	}

	public String getSelectedEndereco() {
		return selectedEndereco;
	}

	public void setSelectedEndereco(String selectedEndereco) {
		this.selectedEndereco = selectedEndereco;
	}

	public List<CadastroOsDetalheEnvolvClieEnde> getObjRsEndeClienteSelect() {
		return objRsEndeClienteSelect;
	}

	public void setObjRsEndeClienteSelect(List<CadastroOsDetalheEnvolvClieEnde> objRsEndeClienteSelect) {
		this.objRsEndeClienteSelect = objRsEndeClienteSelect;
	}

	public CadastroOsDetalheEnvolvClieEnde getObjRsEndeCliente() {
		return objRsEndeCliente;
	}

	public void setObjRsEndeCliente(CadastroOsDetalheEnvolvClieEnde objRsEndeCliente) {
		this.objRsEndeCliente = objRsEndeCliente;
	}
}
