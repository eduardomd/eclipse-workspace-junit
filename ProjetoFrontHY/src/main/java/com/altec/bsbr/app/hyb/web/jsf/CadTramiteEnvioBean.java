package com.altec.bsbr.app.hyb.web.jsf;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("hybCadTramiteEnvioBean")
@Scope("session")
public class CadTramiteEnvioBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String strNrOs;
	private String strNrTramite;
	private String strHistEnvio;
	private String strReadOnly = "true";	
	private String strTitulo;
	
	@PostConstruct
	public void init() {
		this.strNrOs = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrOs");
		this.strNrTramite = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("NrTramite");
		this.strTitulo = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strTitulo");
	}
	
	public String getStrNrOs() {
		return strNrOs;
	}
	
	public void setStrNrOs(String strNrOs) {
		this.strNrOs = strNrOs;
	}
	
	public String getStrNrTramite() {
		return strNrTramite;
	}
	
	public void setStrNrTramite(String strNrTramite) {
		this.strNrTramite = strNrTramite;
	}

	public String getStrHistEnvio() {
		return strHistEnvio;
	}

	public void setStrHistEnvio(String strHistEnvio) {
		this.strHistEnvio = strHistEnvio;
	}

	public String getStrReadOnly() {
		return strReadOnly;
	}

	public void setStrReadOnly(String strReadOnly) {
		this.strReadOnly = strReadOnly;
	}

	public String getStrTitulo() {
		return strTitulo;
	}

	public void setStrTitulo(String strTitulo) {
		this.strTitulo = strTitulo;
	}
	
	public void salvar() {
		System.out.println(getStrHistEnvio());
		//TODO: Backend chamada InserirTramiteHistoricoEnvio
		RequestContext.getCurrentInstance().execute("alert(\"Histórico de envio alterado com sucesso!\") ");
	}
	
	
	
}
