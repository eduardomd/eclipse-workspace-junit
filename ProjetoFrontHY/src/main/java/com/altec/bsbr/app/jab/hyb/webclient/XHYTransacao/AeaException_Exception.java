//
// Generated By:JAX-WS RI 2.2.9-b130926.1035 (JAXB RI IBM 2.2.8-b130911.1802)
//


package com.altec.bsbr.app.jab.hyb.webclient.XHYTransacao;

import javax.xml.ws.WebFault;

@WebFault(name = "AeaException", targetNamespace = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/")
public class AeaException_Exception
    extends Exception
{

    /**
     * Java type that goes as soapenv:Fault detail element.
     * 
     */
    private AeaException faultInfo;

    /**
     * 
     * @param faultInfo
     * @param message
     */
    public AeaException_Exception(String message, AeaException faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @param faultInfo
     * @param cause
     * @param message
     */
    public AeaException_Exception(String message, AeaException faultInfo, Throwable cause) {
        super(message, cause);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @return
     *     returns fault bean: com.altec.bsbr.app.jab.hyb.webclient.XHYTransacao.AeaException
     */
    public AeaException getFaultInfo() {
        return faultInfo;
    }

}
