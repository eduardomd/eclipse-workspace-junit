package com.altec.bsbr.app.hyb.dto;

public class RsNotiManual {

	private String NM_EMAIL_RECU;
	private String NM_RECU_OCOR_ESPC;
	
	
	public RsNotiManual() {}
	
	public RsNotiManual(String nM_RECU_OCOR_ESPC, String nM_EMAIL_RECU) {
		this.NM_RECU_OCOR_ESPC = nM_RECU_OCOR_ESPC;
		this.NM_EMAIL_RECU = nM_EMAIL_RECU;
	}
	
	
	public String getNM_EMAIL_RECU() {
		return NM_EMAIL_RECU;
	}
	public void setNM_EMAIL_RECU(String nM_EMAIL_RECU) {
		NM_EMAIL_RECU = nM_EMAIL_RECU;
	}
	public String getNM_RECU_OCOR_ESPC() {
		return NM_RECU_OCOR_ESPC;
	}
	public void setNM_RECU_OCOR_ESPC(String nM_RECU_OCOR_ESPC) {
		NM_RECU_OCOR_ESPC = nM_RECU_OCOR_ESPC;
	}
	
	

       
}