package com.altec.bsbr.app.hyb.dto;

import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.fw.web.jsf.BasicBBean;

@Component("histResultPesq")
@Scope("session")
public class HistResultPesq extends BasicBBean {

	private String evento;
	private String responsavel;
	private Date data;
	private String agencia;
	private String situacao;
	private String titulo;
	private String anexo;
	
	public HistResultPesq() {
		
	}
	
	public HistResultPesq (String evento, String responsavel, Date data, String agencia, String situacao,
			String titulo, String anexo) {
		super();
		this.evento= evento;
		this.responsavel = responsavel;
		this.data = data;
		this.agencia = agencia;
		this. situacao = situacao;
		this.titulo = titulo;
		this.anexo = anexo;
	}
	
	public String getEvento() {
		return evento;
	}
	public void setEvento(String evento) {
		this.evento = evento;
	}
	public String getResponsavel() {
		return responsavel;
	}
	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public String getAgencia() {
		return agencia;
	}
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}
	public String getSituacao() {
		return situacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getAnexo() {
		return anexo;
	}
	public void setAnexo(String anexo) {
		this.anexo = anexo;
	}
	
	
	
	
}
