package com.altec.bsbr.app.hyb.web.jsf;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.HistPesqAnexoModel;
import com.altec.bsbr.app.jab.hyb.webclient.XHYHistorico.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYHistorico.XHYHistoricoEndPoint;


@Component("histPesqAnexo")
@Scope("session")
public class HistPesqAnexoBean {

	private String strCodEven = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strEvento")  == null ? "" : FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strEvento").toString() ;
	private List<HistPesqAnexoModel> resultAnxs;
	@Autowired
	private XHYHistoricoEndPoint histControll;
	
	private StreamedContent file;
	
	@PostConstruct
	public void init()
	{
	 
		System.out.println("Chegu aedi  glor" + strCodEven);
		Pesquisar(); 
	}
	
	public void Pesquisar()
	{
		String retorno;
		
		try {
			retorno = histControll.consultarHistoricoOperAnexo(strCodEven);
			
			System.out.println(retorno);
			
			JSONObject pesqAnx = new JSONObject(retorno);
			JSONArray pcursor = pesqAnx.getJSONArray("PCURSOR");
			
			resultAnxs = new ArrayList<HistPesqAnexoModel>();
			
			for(int i = 0; i < pcursor.length(); i++)
			{
				
				JSONObject item = pcursor.getJSONObject(i);
				HistPesqAnexoModel dadosAnx = new HistPesqAnexoModel();
				
				dadosAnx.setNM_ARQ(item.isNull("NM_ARQU") == true ? "" : item.get("NM_ARQU").toString());
				dadosAnx.setTP_ANEXO(item.isNull("TP_ANEX") == true ? "" : item.get("TP_ANEX").toString());
				
				resultAnxs.add(dadosAnx);
			}
			
		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void downloadFile(String nomeArq) {
		String serverUrl = "C:/Users/x217347/Documents/HotDeploy/";
		File fileLocal = new File(serverUrl+"test.txt");
		
		String filePath = fileLocal.getPath().toString();
		String fileName = fileLocal.getName().toString();
		String contentType = FacesContext.getCurrentInstance().getExternalContext().getMimeType(filePath);
		
		InputStream stream;
		try {
			stream = new FileInputStream(filePath);
			this.file = new DefaultStreamedContent(stream, contentType, fileName);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	
	public List<HistPesqAnexoModel> getResultAnxs() {
		return resultAnxs;
	}

	public void setResultAnxs(List<HistPesqAnexoModel> resultAnxs) {
		this.resultAnxs = resultAnxs;
	}

	public StreamedContent getFile() {
		return file;
	}

	public void setFile(StreamedContent file) {
		this.file = file;
	}
	
}
