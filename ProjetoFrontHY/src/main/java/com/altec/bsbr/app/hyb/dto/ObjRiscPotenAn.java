package com.altec.bsbr.app.hyb.dto;

public class ObjRiscPotenAn {
	
	private String NR_SEQU_RECR_EXCL_ANLI;
	private String DT_EXCL_PRCL;
	private String VL_EXCL_PRCL;
	private String DT_RECR_MES_PRCL;
	private String VL_RECR_MES_PRCL;
	private String DT_RECR_FORA_MES_PRCL;
	private String VL_RECR_FORA_MES_PRCL;
	private String DT_COMP;
	private String VL_COMP;
	private String DT_TRST;
	private String VL_TRST;
	private String DT_NEGA;
	private String VL_NEGA;
	
	public ObjRiscPotenAn(
			 String NR_SEQU_RECR_EXCL_ANLI,
			 String DT_EXCL_PRCL,
			 String VL_EXCL_PRCL,
			 String DT_RECR_MES_PRCL,
			 String VL_RECR_MES_PRCL,
			 String DT_RECR_FORA_MES_PRCL,
			 String VL_RECR_FORA_MES_PRCL,
			 String DT_COMP,
			 String VL_COMP,
			 String DT_TRST,
			 String VL_TRST,
			 String DT_NEGA,
			 String VL_NEGA) {
		this.NR_SEQU_RECR_EXCL_ANLI = NR_SEQU_RECR_EXCL_ANLI;
		this.DT_EXCL_PRCL = DT_EXCL_PRCL;
		this.VL_EXCL_PRCL = VL_EXCL_PRCL;
		this.DT_RECR_MES_PRCL = DT_RECR_MES_PRCL;
		this.VL_RECR_MES_PRCL = VL_RECR_MES_PRCL;
		this.DT_RECR_FORA_MES_PRCL = DT_RECR_FORA_MES_PRCL;
		this.VL_RECR_FORA_MES_PRCL = VL_RECR_FORA_MES_PRCL;
		this.DT_COMP = DT_COMP;
		this.VL_COMP = VL_COMP;
		this.DT_TRST = DT_TRST;
		this.VL_TRST = VL_TRST;
		this.DT_NEGA = DT_NEGA;
		this.VL_NEGA = VL_NEGA;
	}
	
	public String getNR_SEQU_RECR_EXCL_ANLI() {
		return NR_SEQU_RECR_EXCL_ANLI;
	}
	public void setNR_SEQU_RECR_EXCL_ANLI(String nR_SEQU_RECR_EXCL_ANLI) {
		NR_SEQU_RECR_EXCL_ANLI = nR_SEQU_RECR_EXCL_ANLI;
	}
	public String getDT_EXCL_PRCL() {
		return DT_EXCL_PRCL;
	}
	public void setDT_EXCL_PRCL(String dT_EXCL_PRCL) {
		DT_EXCL_PRCL = dT_EXCL_PRCL;
	}
	public String getVL_EXCL_PRCL() {
		return VL_EXCL_PRCL;
	}
	public void setVL_EXCL_PRCL(String vL_EXCL_PRCL) {
		VL_EXCL_PRCL = vL_EXCL_PRCL;
	}
	public String getDT_RECR_MES_PRCL() {
		return DT_RECR_MES_PRCL;
	}
	public void setDT_RECR_MES_PRCL(String dT_RECR_MES_PRCL) {
		DT_RECR_MES_PRCL = dT_RECR_MES_PRCL;
	}
	public String getVL_RECR_MES_PRCL() {
		return VL_RECR_MES_PRCL;
	}
	public void setVL_RECR_MES_PRCL(String vL_RECR_MES_PRCL) {
		VL_RECR_MES_PRCL = vL_RECR_MES_PRCL;
	}
	public String getDT_RECR_FORA_MES_PRCL() {
		return DT_RECR_FORA_MES_PRCL;
	}
	public void setDT_RECR_FORA_MES_PRCL(String dT_RECR_FORA_MES_PRCL) {
		DT_RECR_FORA_MES_PRCL = dT_RECR_FORA_MES_PRCL;
	}
	public String getVL_RECR_FORA_MES_PRCL() {
		return VL_RECR_FORA_MES_PRCL;
	}
	public void setVL_RECR_FORA_MES_PRCL(String vL_RECR_FORA_MES_PRCL) {
		VL_RECR_FORA_MES_PRCL = vL_RECR_FORA_MES_PRCL;
	}
	public String getDT_COMP() {
		return DT_COMP;
	}
	public void setDT_COMP(String dT_COMP) {
		DT_COMP = dT_COMP;
	}
	public String getVL_COMP() {
		return VL_COMP;
	}
	public void setVL_COMP(String vL_COMP) {
		VL_COMP = vL_COMP;
	}
	public String getDT_TRST() {
		return DT_TRST;
	}
	public void setDT_TRST(String dT_TRST) {
		DT_TRST = dT_TRST;
	}
	public String getVL_TRST() {
		return VL_TRST;
	}
	public void setVL_TRST(String vL_TRST) {
		VL_TRST = vL_TRST;
	}
	public String getDT_NEGA() {
		return DT_NEGA;
	}
	public void setDT_NEGA(String dT_NEGA) {
		DT_NEGA = dT_NEGA;
	}
	public String getVL_NEGA() {
		return VL_NEGA;
	}
	public void setVL_NEGA(String vL_NEGA) {
		VL_NEGA = vL_NEGA;
	}

	
}
