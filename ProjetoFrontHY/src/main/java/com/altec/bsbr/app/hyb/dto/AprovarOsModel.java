package com.altec.bsbr.app.hyb.dto;

public class AprovarOsModel {

	private int CODIGO;
	private String SITUACAO;
	private String MOT_REC_APROV="";
	private int CD_NOTI;
	
	public AprovarOsModel() {};
	
	public int getCODIGO() {
		return CODIGO;
	}
	public void setCODIGO(int CODIGO) {
		this.CODIGO = CODIGO;
	}
	
	public String getSITUACAO() {
		return SITUACAO;
	}
	public void setSITUACAO(String SITUACAO) {
		this.SITUACAO = SITUACAO;
	}

	public String getMOT_REC_APROV() {
		return MOT_REC_APROV;
	}
	public void setMOT_REC_APROV(String MOT_REC_APROV) {
		this.MOT_REC_APROV = MOT_REC_APROV;
	}

	public int getCD_NOTI() {
		return CD_NOTI;
	}
	public void setCD_NOTI(int CD_NOTI) {
		this.CD_NOTI = CD_NOTI;
	}
}
