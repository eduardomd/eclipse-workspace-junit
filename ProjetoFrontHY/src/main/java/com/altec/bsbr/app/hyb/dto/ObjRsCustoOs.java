package com.altec.bsbr.app.hyb.dto;

public class ObjRsCustoOs {
	
	private String nome;
	private String hora;
	private String remuneracao;
	private String gastoAd;
	private String tipoRemuneracao;
	private String tipoRecurso;
	private String cpf;
	private String matricula;
	private String designacao;
	private String area;
	
	private String dblRemu;
	private int dblHora;
	private String dblGasto;
	
	public String getHora() {
		return hora;
	}
	public void setHora(String hora) {
		this.hora = hora;
	}
	public String getRemuneracao() {
		return remuneracao;
	}
	public void setRemuneracao(String remuneracao) {
		this.remuneracao = remuneracao;
	}
	public String getGastoAd() {
		return gastoAd;
	}
	public void setGastoAd(String gastoAd) {
		this.gastoAd = gastoAd;
	}
	public String getTipoRemuneracao() {
		return tipoRemuneracao;
	}
	public void setTipoRemuneracao(String tipoRemuneracao) {
		this.tipoRemuneracao = tipoRemuneracao;
	}
	public String getTipoRecurso() {
		return tipoRecurso;
	}
	public void setTipoRecurso(String tipoRecurso) {
		this.tipoRecurso = tipoRecurso;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public String getDesignacao() {
		return designacao;
	}
	public void setDesignacao(String designacao) {
		this.designacao = designacao;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDblRemu() {
		return dblRemu;
	}
	public void setDblRemu(String dblRemu) {
		this.dblRemu = dblRemu;
	}
	public int getDblHora() {
		return dblHora;
	}
	public void setDblHora(int dblHora) {
		this.dblHora = dblHora;
	}
	public String getDblGasto() {
		return dblGasto;
	}
	public void setDblGasto(String dblGasto) {
		this.dblGasto = dblGasto;
	}
}
