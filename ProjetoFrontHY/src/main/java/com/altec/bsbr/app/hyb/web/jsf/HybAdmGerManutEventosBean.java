

package com.altec.bsbr.app.hyb.web.jsf;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.altec.bsbr.fw.web.jsf.BasicBBean;

import com.altec.bsbr.app.hyb.dto.EventosModel;


@Component("hyAdmGerManutEventosBean")
@Scope("session")
public class HybAdmGerManutEventosBean extends BasicBBean {
	private Object strMsg = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strMsg");
    private String strErro;
    
    private List<EventosModel> objAdmGer = new ArrayList<EventosModel>();
    private List<EventosModel> objRsEvento = new ArrayList<EventosModel>();
    private String[] objRsAreaEvento;
    
    private String txtAddEvento;	
    private String txtHdEvento;
    private String txtHdParamAltEvento;
	private String txtHdParamAltRelac;

	@PostConstruct
    public void init() {
		//----- mock
		this.objRsAreaEvento = new String[3];
		
		this.objRsAreaEvento[0] = "Área 01";
		this.objRsAreaEvento[1] = "Área 02";
		this.objRsAreaEvento[2] = "Área 03";
		
		for(int c = 1; c <= 5; c++) {
			this.objRsEvento.add(new EventosModel(c, "Evento " + c, (c % 2 == 0)));
		}
		//-----
    }
		
	public boolean VerificaAreaEvento(int intCodArea, int intCodEvento) {
		
	    //boolean objRsVerificaAreaEvento = objAdmGer.VerificarAreaEvento(intCodArea, intCodEvento, strErro);
	    // verifcaErro(strErro);
	    
	    //if(objRsVerificaAreaEvento == true) {
	    	return true;
	    //}
	    
	    //return false;
	}
	
	public void adicionar() {
		Map<String, String> requestParamMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		this.objRsEvento.add(new EventosModel(this.objRsEvento.size(), requestParamMap.get("frm:txtAddEvento"), true));
		System.out.println("Adicionar: " + requestParamMap.get("frm:txtAddEvento"));
	}
	
	public void deletar() {
		Map<String, String> requestParamMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		this.objRsEvento.remove(Integer.parseInt(requestParamMap.get("frm:txtHdEvento")) - 1);
		System.out.println("Deletar: " + requestParamMap.get("frm:txtHdEvento"));
	}
	
	public void salvar() {
		Map<String, String> requestParamMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		System.out.println("Salvar: " + requestParamMap.get("frm:txtHdParamAltEvento"));
		System.out.println("Salvar: " + requestParamMap.get("frm:txtHdParamAltRelac"));
	}
	
	public int gerarId() {
		return (int) (Math.random() * 1000);
	}
	
	public int getObjRsAreaEventoLength() {
		return this.objRsAreaEvento.length;
	}

	public String getStrMsg() {
		if(strMsg == null)
			return "";
		
		return strMsg.toString();
	}

	public void setStrMsg(String strMsg) {
		this.strMsg = strMsg;
	}

	public String getStrErro() {
		return strErro;
	}

	public void setStrErro(String strErro) {
		this.strErro = strErro;
	}

	public List<EventosModel> getObjAdmGer() {
		return objAdmGer;
	}

	public void setObjAdmGer(List<EventosModel> objAdmGer) {
		this.objAdmGer = objAdmGer;
	}

	public List<EventosModel> getObjRsEvento() {
		return objRsEvento;
	}

	public void setObjRsEvento(List<EventosModel> objRsEvento) {
		this.objRsEvento = objRsEvento;
	}

	public String[] getObjRsAreaEvento() {
		return objRsAreaEvento;
	}

	public void setObjRsAreaEvento(String[] objRsArea) {
		this.objRsAreaEvento = objRsArea;
	}

	public String getTxtAddEvento() {
		return txtAddEvento;
	}

	public void setTxtAddEvento(String txtAddEvento) {
		this.txtAddEvento = txtAddEvento;
	}

	public String getTxtHdEvento() {
		return txtHdEvento;
	}

	public void setTxtHdEvento(String txtHdEvento) {
		this.txtHdEvento = txtHdEvento;
	}

	public String getTxtHdParamAltEvento() {
		return txtHdParamAltEvento;
	}

	public void setTxtHdParamAltEvento(String txtHdParamAltEvento) {
		this.txtHdParamAltEvento = txtHdParamAltEvento;
	}

	public String getTxtHdParamAltRelac() {
		return txtHdParamAltRelac;
	}

	public void setTxtHdParamAltRelac(String txtHdParamAltRelac) {
		this.txtHdParamAltRelac = txtHdParamAltRelac;
	}

}
