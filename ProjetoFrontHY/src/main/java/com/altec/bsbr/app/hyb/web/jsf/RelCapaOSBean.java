package com.altec.bsbr.app.hyb.web.jsf;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.ObjRsRelCapaOs;
import com.altec.bsbr.app.hyb.dto.RelCapaOSModel;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAcoesOs.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAcoesOs.XHYAcoesOsEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYUsuario.XHYUsuarioEndPoint;
import com.altec.bsbr.fw.security.SecurityInfo;
import com.altec.bsbr.fw.web.jsf.BasicBBean;


@Component("relCapaOSBean")
@Scope("request")
public class RelCapaOSBean extends BasicBBean {

	private static final long serialVersionUID = 1L;
	private List<RelCapaOSModel> capaOs;
	private List<RelCapaOSModel> capaOsCord;
	private List<RelCapaOSModel> capaOsGerente;
	private ObjRsRelCapaOs objRsRelCapaOs;
	private String strNrSeqOs;
	private String matriculaUsuario;
	
	@Autowired
	private XHYAcoesOsEndPoint acoesOs;
	
	@Autowired
	private XHYUsuarioEndPoint usuario;
		
	@PostConstruct
	public void init() {
		this.strNrSeqOs = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs") == null ? "" : (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs");
		consultarOs();
		montaGridEquipe();
		getMatricula();
	}
	
	public RelCapaOSBean() {
		
	} 
	
	public void consultarOs() {
		try {
			this.objRsRelCapaOs = new ObjRsRelCapaOs();
			System.out.println("QUERYs O S: " + strNrSeqOs);
			String retorno = acoesOs.consultarOS(strNrSeqOs);
			
			JSONObject consultarOs = new JSONObject(retorno);
            JSONArray pcursor = consultarOs.getJSONArray("PCURSOR");
            
            if (pcursor.length() > 0) {
	            JSONObject item = pcursor.getJSONObject(0);
	            this.objRsRelCapaOs.setDataAtual(item.isNull("DATA_ATUAL") ? "" : item.getString("DATA_ATUAL"));
	            this.objRsRelCapaOs.setHoraAtual(item.isNull("HORA_ATUAL") ? "" : item.getString("HORA_ATUAL"));
	            this.objRsRelCapaOs.setDtAbertura(item.isNull("DT_ABERTURA") ? "" : item.getString("DT_ABERTURA"));
	            this.objRsRelCapaOs.setTxAbertura(item.isNull("TX_ABERTURA") ? "" : item.getString("TX_ABERTURA"));
	            this.objRsRelCapaOs.setTxEncerramento(item.isNull("TX_ENCERRAMENTO") ? "" : item.getString("TX_ENCERRAMENTO"));
            }
            
		} catch (WebServiceException e) {
			e.printStackTrace();
			FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "hyb_erro.xhtml?&strErro=" + e.getMessage());
		}
	}
	
	public void montaGridEquipe() {
		try {
			capaOs = new ArrayList<RelCapaOSModel>();
			capaOsCord = new ArrayList<RelCapaOSModel>();
			capaOsGerente = new ArrayList<RelCapaOSModel>();
			
			String retorno = acoesOs.consultarEquipeOs(strNrSeqOs, "");
			
			JSONObject consultarEquipeOs = new JSONObject(retorno);
            JSONArray pcursor = consultarEquipeOs.getJSONArray("PCURSOR");
            System.out.println("CURSOR1: " + pcursor);
            if (pcursor.length() > 0) {
		        for (int i = 0; i < pcursor.length(); i++) {
		        	JSONObject item = pcursor.getJSONObject(i);
		        	RelCapaOSModel model = new RelCapaOSModel();
		        	
		        	String tpDesi = item.isNull("TP_DESI") ? "" : item.get("TP_DESI").toString();
		            String nmDesi = item.isNull("NM_DESIGNACAO") ? "" : item.get("NM_DESIGNACAO").toString();
		            String nrMatrPrsv = item.isNull("NR_MATR_PRSV") ? "" : item.get("NR_MATR_PRSV").toString();
		            String nmRecuOcor = item.isNull("NM_RECU_OCOR_ESPC") ? "" : item.get("NM_RECU_OCOR_ESPC").toString();
		            String cdCarg = item.isNull("CD_CARG") ? "" : item.get("CD_CARG").toString();
		            
		            model.setTpDesi(tpDesi);
		            model.setNmDesignacao(nmDesi);
		            model.setNrMatrPrsv(nrMatrPrsv);
		            model.setNmRecuOcorEspc(nmRecuOcor);
		            model.setCdCarg(cdCarg);
		            
		            System.out.println("JSON: " + ", " + tpDesi + ", " + nmDesi + ", " + nrMatrPrsv + ", " + nmRecuOcor + ", " + cdCarg);
		            System.out.println("TEST" + model.getNrMatrPrsv());
		            if ( model.getTpDesi().equals("AD") ||
	            		model.getTpDesi().equals("AA") ||
	            		model.getTpDesi().equals("PSD") ||
	            		model.getTpDesi().equals("PSA") ||
	            		model.getTpDesi().equals("NA")) {
		            	
		            	capaOs.add(model);
		            }
		            
		            if (model.getCdCarg().equals("B70823")) {
		            	capaOsCord.add(model);
		            }
		            
		            if (model.getCdCarg().equals("B70555")) {
		            	capaOsGerente.add(model);
	            	}
		        }
            }
		        
		} catch (WebServiceException e) {
			e.printStackTrace();
			FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "hyb_erro.xhtml?&strErro=" + e.getMessage());
		}
		
	}
	
	public void getMatricula() {
		SecurityInfo secInfo = SecurityInfo.getCurrent(); 
        String userName = secInfo.getUserName().replaceAll("[^0-9]", "");         
	             
         if (userName.isEmpty()) {     
          FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(
                       FacesContext.getCurrentInstance(), null, "hyb_erro.xhtml?&strErro=Sessão Expirada!");
         } else {
	        String retorno = "";
        
	         try {
	               retorno = usuario.recuperarDadosUsuario(userName);
	
	               JSONObject dadosUsuario = new JSONObject(retorno);
	               JSONArray pcursor = dadosUsuario.getJSONArray("PCURSOR");
	
	               JSONObject f = pcursor.getJSONObject(0);
	               matriculaUsuario =  f.isNull("MATRICULA") == true ? "" : f.get("MATRICULA").toString();
	                
	         } catch (com.altec.bsbr.app.jab.hyb.webclient.XHYUsuario.WebServiceException e) {
	            	 FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(
	            	 FacesContext.getCurrentInstance(), null, "hyb_erro.xhtml?&strErro=" + e.getMessage());
	         }
         }

	}

	public List<RelCapaOSModel> getCapaOs() {
		return capaOs;
	}

	public void setCapaOs(List<RelCapaOSModel> capaOs) {
		this.capaOs = capaOs;
	}

	public ObjRsRelCapaOs getObjRsRelCapaOs() {
		return objRsRelCapaOs;
	}

	public void setObjRsRelCapaOs(ObjRsRelCapaOs objRsRelCapaOs) {
		this.objRsRelCapaOs = objRsRelCapaOs;
	}

	public String getStrNrSeqOs() {
		return strNrSeqOs;
	}

	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}

	public XHYAcoesOsEndPoint getAcoesOs() {
		return acoesOs;
	}

	public void setAcoesOs(XHYAcoesOsEndPoint acoesOs) {
		this.acoesOs = acoesOs;
	}

	public List<RelCapaOSModel> getCapaOsCord() {
		return capaOsCord;
	}

	public void setCapaOsCord(List<RelCapaOSModel> capaOsCord) {
		this.capaOsCord = capaOsCord;
	}

	public List<RelCapaOSModel> getCapaOsGerente() {
		return capaOsGerente;
	}

	public void setCapaOsGerente(List<RelCapaOSModel> capaOsGerente) {
		this.capaOsGerente = capaOsGerente;
	}

	public String getMatriculaUsuario() {
		return matriculaUsuario;
	}

	public void setMatriculaUsuario(String matriculaUsuario) {
		this.matriculaUsuario = matriculaUsuario;
	}

	public XHYUsuarioEndPoint getUsuario() {
		return usuario;
	}

	public void setUsuario(XHYUsuarioEndPoint usuario) {
		this.usuario = usuario;
	}
	
}
 