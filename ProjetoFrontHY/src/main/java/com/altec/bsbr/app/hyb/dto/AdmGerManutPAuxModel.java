package com.altec.bsbr.app.hyb.dto;

public class AdmGerManutPAuxModel {
	private int CODIGO;
	private String NOME;
	private int SLA;
	private boolean OPERACAODEB;
	private boolean OPERACAOCRED;
	private boolean EXCLUSAO;
	
	public AdmGerManutPAuxModel(int cODIGO, String nOME, int sLA, boolean OPDEB, boolean OPCRED, boolean eXCLUSAO) {
		setCODIGO(cODIGO);
		setNOME(nOME);
		setSLA(sLA);
		setOPERACAODEB(OPDEB);
		setOPERACAOCRED(OPCRED);
		setEXCLUSAO(eXCLUSAO);
	}

	public AdmGerManutPAuxModel() {
	}

	public int getCODIGO() {
		return CODIGO;
	}	
	public void setCODIGO(int cODIGO) {
		CODIGO = cODIGO;
	}
	
	public String getNOME() {
		return NOME;
	}
	public void setNOME(String nOME) {
		NOME = nOME;
	}
	
	public int getSLA() {
		return SLA;
	}
	public void setSLA(int sLA) {
		SLA = sLA;
	}

	public boolean getOPERACAODEB() {
		return OPERACAODEB;
	}
	public void setOPERACAODEB(boolean oPERACAODEB) {
		OPERACAODEB = oPERACAODEB;
	}

	public boolean getOPERACAOCRED() {
		return OPERACAOCRED;
	}
	public void setOPERACAOCRED(boolean oPERACAOCRED) {
		OPERACAOCRED = oPERACAOCRED;
	}

	public boolean getEXCLUSAO() {
		return EXCLUSAO;
	}
	public void setEXCLUSAO(boolean eXCLUSAO) {
		EXCLUSAO = eXCLUSAO;
	}
}

