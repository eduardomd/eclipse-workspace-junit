package com.altec.bsbr.app.hyb.web.jsf;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.HistResultPesq;
import com.altec.bsbr.fw.web.jsf.BasicBBean;


@Component("resultBean")
@Scope("session")
public class ResultBean extends BasicBBean{
	
	public List<HistResultPesq> results = new ArrayList<HistResultPesq>();
	
	public ResultBean() {
		try {
		Map<String, String> params = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();
		  String CodEven = params.get("CodEven"); 
		  String CodAgen = params.get("CodAgen"); 
		  String DsTitu  = params.get("DsTitu"); 
		  if(!( CodEven.isEmpty() || CodAgen.isEmpty() || DsTitu.isEmpty())) {	 
			  System.out.println("entrou " + DsTitu.getClass().getTypeName() + "!");
	        for (int i = 0; i < 20; i++) {
	               results.add(generateRandomResult(CodAgen, CodAgen, DsTitu));
	        }
		  }
		} catch(Exception e) {
			logger.debug("Error", e);
		}
	}
   
	  public String [] nomes = {"Eduardo", "Luiz", "Henrique", "Felipe", "Bruna", "Brianda", "Sonia"};
	         
	  public List<HistResultPesq> getResults() {
	        return results;           
	  }
	  
	  public HistResultPesq generateRandomResult(String cdEven, String cdAgen, String dsTitu) {
		  
	      int indice = (int) Math.floor(Math.random()*7);
	      HistResultPesq result = new HistResultPesq();
	      result.setEvento(cdEven);
	      result.setResponsavel("Rua " + indice);
		  result.setAgencia(cdAgen);
		  result.setSituacao("Teste");
		  result.setTitulo(dsTitu);
		  result.setAnexo("arquivo");
	      result.setData(new Date());
	      return result;
	                       
	  }

}
