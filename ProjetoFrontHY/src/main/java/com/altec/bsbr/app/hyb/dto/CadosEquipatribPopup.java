package com.altec.bsbr.app.hyb.dto;

public class CadosEquipatribPopup {

	private String strNrSeqOs;
	private String strNome;
	private String strCpf;
	private String strMatricula;
	private String strCdArea;
	//private String arrAddCpf;
	private String strAction;
	private String strErro;
	private String strBgColor;
	//private int intCont;
	private int intRegistro;
	private int intPagina;
	private int intPaginaLinkCont;
	private int intPaginaLinkMax;
	private int intRestoTotalReg;
	private String txtHdAction;
	private String txtHdAddParam;
	private String strCargo;
	
	public CadosEquipatribPopup() {
		
	}
	
	public CadosEquipatribPopup(String strNrSeqOs, String strCpf, String strMatricula, String strNome, String strCargo) {
		this.strNrSeqOs = strNrSeqOs;
		this.strCpf = strCpf;
		this.strMatricula = strMatricula;
		this.strNome = strNome;
		this.strCargo = strCargo;
	}
	
	public String getStrNrSeqOs() {
		return strNrSeqOs;
	}
	public String getStrNome() {
		return strNome;
	}
	public String getStrCpf() {
		return strCpf;
	}
	public String getStrMatricula() {
		return strMatricula;
	}
	public String getStrCdArea() {
		return strCdArea;
	}
//	public String getArrAddCpf() {
//		return arrAddCpf;
//	}
	public String getStrAction() {
		return strAction;
	}
	public String getStrErro() {
		return strErro;
	}
	public String getStrBgColor() {
		return strBgColor;
	}
//	public int getIntCont() {
//		return intCont;
//	}
	public int getIntRegistro() {
		return intRegistro;
	}
	public int getIntPagina() {
		return intPagina;
	}
	public int getIntPaginaLinkCont() {
		return intPaginaLinkCont;
	}
	public int getIntPaginaLinkMax() {
		return intPaginaLinkMax;
	}
	public int getIntRestoTotalReg() {
		return intRestoTotalReg;
	}
	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}
	public void setStrNome(String strNome) {
		this.strNome = strNome;
	}
	public void setStrCpf(String strCpf) {
		this.strCpf = strCpf;
	}
	public void setStrMatricula(String strMatricula) {
		this.strMatricula = strMatricula;
	}
	public void setStrCdArea(String strCdArea) {
		this.strCdArea = strCdArea;
	}
//	public void setArrAddCpf(String arrAddCpf) {
//		this.arrAddCpf = arrAddCpf;
//	}
	public void setStrAction(String strAction) {
		this.strAction = strAction;
	}
	public void setStrErro(String strErro) {
		this.strErro = strErro;
	}
	public void setStrBgColor(String strBgColor) {
		this.strBgColor = strBgColor;
	}
//	public void setIntCont(int intCont) {
//		this.intCont = intCont;
//	}
	public void setIntRegistro(int intRegistro) {
		this.intRegistro = intRegistro;
	}
	public void setIntPagina(int intPagina) {
		this.intPagina = intPagina;
	}
	public void setIntPaginaLinkCont(int intPaginaLinkCont) {
		this.intPaginaLinkCont = intPaginaLinkCont;
	}
	public void setIntPaginaLinkMax(int intPaginaLinkMax) {
		this.intPaginaLinkMax = intPaginaLinkMax;
	}
	public void setIntRestoTotalReg(int intRestoTotalReg) {
		this.intRestoTotalReg = intRestoTotalReg;
	}
	public String getTxtHdAction() {
		return txtHdAction;
	}

	public void setTxtHdAction(String txtHdAction) {
		this.txtHdAction = txtHdAction;
	}

	public String getTxtHdAddParam() {
		return txtHdAddParam;
	}
	public void setTxtHdAddParam(String txtHdAddParam) {
		this.txtHdAddParam = txtHdAddParam;
	}

	public String getStrCargo() {
		return strCargo;
	}

	public void setStrCargo(String strCargo) {
		this.strCargo = strCargo;
	}
	
}
