package com.altec.bsbr.app.hyb.dto;

public class B402_BGM431SAeaResponse {
	
	public B402_BGM431SAeaResponse(){};

	public B402_BGM431SAeaResponse(String dC_FORMATO, String pRCHEQ, String fHAENT) {
		super();
		DC_FORMATO = dC_FORMATO;
		PRCHEQ = pRCHEQ;
		FHAENT = fHAENT;
	}

	private String DC_FORMATO;
	
	private String PRCHEQ;
	
	private String FHAENT;

	public String getDC_FORMATO() {
		return DC_FORMATO;
	}
	
	public void setDC_FORMATO(String dC_FORMATO) {
		DC_FORMATO = dC_FORMATO;
	}
	
	public String getPRCHEQ() {
		return PRCHEQ;
	}

	public void setPRCHEQ(String pRCHEQ) {
		PRCHEQ = pRCHEQ;
	}

	public String getFHAENT() {
		return FHAENT;
	}

	public void setFHAENT(String fHAENT) {
		FHAENT = fHAENT;
	}
	
}
