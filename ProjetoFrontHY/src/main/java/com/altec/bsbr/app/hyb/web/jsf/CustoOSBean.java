package com.altec.bsbr.app.hyb.web.jsf;


import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;

import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.ObjRsCustoOs;
import com.altec.bsbr.app.hyb.web.util.XHYUsuarioIncService;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAcoesOs.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAcoesOs.XHYAcoesOsEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;


@Component("custoBean")
@Scope("session")
public class CustoOSBean extends BasicBBean {
	
	private static final long serialVersionUID = 1L;
	
	private String strNrSeqOs;
	private boolean objRsEOF;
	private List<ObjRsCustoOs> objRs;
	
	private String dblCustoOs;

	@Autowired 
	private XHYUsuarioIncService userServ;
	
	@Autowired
	private XHYAcoesOsEndPoint custoos;

	@PostConstruct
	public void init () {
		this.dblCustoOs = "0";
		this.objRsEOF = false;
		this.strNrSeqOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs");
		
		this.setupObjRs();
	}

	public void verificarPermissao(ComponentSystemEvent event) {
		String[] perfis = {"SUPERINT","GER"};
		
		userServ.verificarPermissao(perfis);
	}
	
	public String setupObjRs() {
		String retorno = "";
		try {
			retorno = custoos.recuperarCustoOs(this.strNrSeqOs);
			JSONObject jsonRetorno = new JSONObject(retorno);
            JSONArray pcursor = jsonRetorno.getJSONArray("PCURSOR");
            
            this.objRs = new ArrayList<ObjRsCustoOs>();
            double dblCustoFormat = 0;
            
            if (pcursor.length() > 0) {
	            for(int i = 0; i < pcursor.length(); i++) {
	                JSONObject curr = pcursor.getJSONObject(i);
	               
	                ObjRsCustoOs model = new ObjRsCustoOs();
	                
	                String area = curr.isNull("AREA") ? "" : curr.get("AREA").toString();
	                String cpf = curr.isNull("CPF") ? "" : curr.get("CPF").toString();
	                String designacao = curr.isNull("DESIGNACAO") ? "" : curr.get("DESIGNACAO").toString();
	                String gastoAd = "0.0";
	                String hora = "0";
	                String remuneracao = "0.0";
	                
	                if (curr.isNull("GASTO_AD")) {
	                	gastoAd = "0.0";
	                } else {
	                	gastoAd = curr.get("GASTO_AD").toString();
	                }
	                
	                if (curr.isNull("HORA")) {
	                	hora = "0";
	                } else {
	                	hora = curr.get("HORA").toString();
	                }
	                
	                if (curr.isNull("REMUNERACAO")) {
	                	remuneracao = "0.0";
	                } else {
	                	remuneracao = curr.get("REMUNERACAO").toString();
	                }
	                
	                Locale ptBR = new Locale("pt", "BR");

	                String matricula = curr.isNull("MATRICULA") ? "" : curr.get("MATRICULA").toString();
	                String tipoRecurso = curr.isNull("TIPO_RECURSO") ? "" : curr.get("TIPO_RECURSO").toString();
	                String nome = curr.isNull("NOME") ? "" : curr.get("NOME").toString();
	                
	                if (curr.get("TIPO_REMUNERACAO").toString().equals("1")) {
	            		dblCustoFormat += ( (Double.parseDouble(remuneracao)/160) * Integer.parseInt(hora) ) + Double.parseDouble(gastoAd);
	            	} else {
	            		dblCustoFormat += (Double.parseDouble(remuneracao) * Integer.parseInt(hora) ) + Double.parseDouble(gastoAd);
	            	}
	            	
	            	this.dblCustoOs = NumberFormat.getCurrencyInstance(ptBR).format(dblCustoFormat);

	            	String dblGastoD = NumberFormat.getCurrencyInstance(ptBR).format(Double.parseDouble(gastoAd));
	            	String dblRemuD = NumberFormat.getCurrencyInstance(ptBR).format(Double.parseDouble(remuneracao));
	            	
	            	
	            	model.setDblRemu(dblRemuD);
	            	model.setDblHora(Integer.parseInt(hora));
	            	model.setDblGasto(dblGastoD);
	            	
	            	model.setNome(nome);
	                model.setArea(area);
	                model.setCpf(cpf);
	                model.setDesignacao(designacao);
	                model.setGastoAd(gastoAd);
	                model.setHora(hora);
	                model.setMatricula(matricula);
	                model.setRemuneracao(remuneracao);
	                model.setTipoRecurso(tipoRecurso);

	                this.objRs.add(model);
	            };

            } else {
            	this.objRsEOF = true;
            }
            
		} catch (WebServiceException e) {
            e.printStackTrace();
      }
		return retorno;
		
	}

	public String getStrNrSeqOs() {
		return strNrSeqOs;
	}

	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}

	public XHYAcoesOsEndPoint getCustoos() {
		return custoos;
	}

	public void setCustoos(XHYAcoesOsEndPoint custoos) {
		this.custoos = custoos;
	}

	public String getDblCustoOs() {
		return dblCustoOs;
	}

	public void setDblCustoOs(String dblCustoOs) {
		this.dblCustoOs = dblCustoOs;
	}
	
	public boolean isObjRsEOF() {
		return objRsEOF;
	}

	public void setObjRsEOF(boolean objRsEOF) {
		this.objRsEOF = objRsEOF;
	}

	public List<ObjRsCustoOs> getObjRs() {
		return objRs;
	}

	public void setObjRs(List<ObjRsCustoOs> objRs) {
		this.objRs = objRs;
	}
	
}