package com.altec.bsbr.app.hyb.web.util;

/*
  [ ***************************************************************
  [ Avanade - Equipe de Desenvolvimento
  [ ***************************************************************
  [ Desenvolvedor.....: Priscilla Crisafulli
  [ Descricao.........: Fun��es para tratamento de mensagens XML 
  [ Obs: Imigra��o de tecnologia antiga para java
  [ Dt Inicio.........: 13/12/2018
  [ ***************************************************************
  [ Alteracao.........:
  [ Descricao.........:
  [ Dt Alteracao......:
  [ ***************************************************************
*/

import org.dom4j.Element;
import org.dom4j.Node;

public class XmlUtil {

	public static String getText(Node objNode, String strField) throws Exception {

		String strValue;
		Node objAtributo;

		if (!(objNode == null)){
			objAtributo = objNode.selectSingleNode("field[@id='" + strField + "']/@value");

			if (!(objAtributo == null)) {
				strValue = objAtributo.getText();

				if (strValue != "") {
					return strValue;
				}
			} else {
				throw new Exception("Erro:-66600 em GetText. O atributo especificado (\""+ strField + " \") n�o pode ser encontrado");
				// Err.Raise -66600, "GetText", "O atributo especificado (" + strField + ") n�o
				// pode ser encontrado";
			}
		} else{
			throw new Exception("Erro:-66601 em GetText. O node especificado � inv�lido");
			// Err.Raise -66601, "GetText", "O node especificado � inv�lido"
		}
		return "";
	}

	/*public static String getNumber(Node objNode, String strField, int intDecimais) throws Exception {
		String strValue;
		Node objAtributo;
		String strAux;

		if (!(objNode == null)) {
			objAtributo = objNode.selectSingleNode("field[@id='" + strField + "']/@value");

			if (!(objAtributo == null)) {
				strValue = objAtributo.getText();

				if (strValue != "") {
					if (!NumberUtils.isNumber(strValue)) {
						strValue = "0";
					}
					double result = Double.valueOf(strValue) / (Math.pow(10, Integer.valueOf("0" + intDecimais)));
					return Double.toString(result);

				}
			} else {
				throw new Exception("Erro:-66600 em GetNumber. O atributo especificado (\""+ strField + " \") n�o pode ser encontrado");
				// Err.Raise -66600, "GetNumber", "O atributo especificado (" & strField & ")
				// n�o pode ser encontrado"
			}
		} else {
			throw new Exception("Erro:-66601 em GetNumber. O node especificado � inv�lido");
			// Err.Raise -66601, "GetNumber", "O node especificado � inv�lido"
		}
		return "";
	}*/

	public String getDate(Node objNode, String strField) throws Exception {
		String strValue;
		Node objAtributo;

		if (!(objNode == null)) {
			objAtributo = objNode.selectSingleNode("field[@id='" + strField + "']/@value");

			if (!(objAtributo == null)) {
				strValue = objAtributo.getText();

				if (strValue != "") {
					if (strValue.length() == 8) {
						return strValue.substring(strValue.length() - 2) + "/" + strValue.substring(4, 6) + "/"
								+ strValue.substring(0, 4);
					} else {
						return strValue.substring(strValue.length() - 2) + "/" + strValue.substring(5, 7) + "/"
								+ strValue.substring(0, 4);
					}

				}
			} else {
				throw new Exception("Erro:-66600 em GetDate. O atributo especificado (\""+ strField + " \") n�o pode ser encontrado");
				// Err.Raise -66600, "GetDate", "O atributo especificado (" & strField & ") n�o
				// pode ser encontrado"
			}
		} else {
			throw new Exception("Erro:-66601 em GetDate. O node especificado � inv�lido");
			// Err.Raise -66601, "GetDate", "O node especificado � inv�lido"
		}
		return "";
	}

	public static String getDateMonthYear(Node objNode, String strField) throws Exception {
	
		String strValue;
		Node objAtributo;
		
		if(!(objNode == null)){
			objAtributo = objNode.selectSingleNode("field[@id='" + strField + "']/@value");
				
			if(!(objAtributo == null)){
				strValue = objAtributo.getText();
							
				if(strValue != "") {
					if(strValue.length() == 8){
						if(strValue.substring(4, 6) != "" && strValue.substring(4, 6) != null) {
							return strValue.substring(4, 6) + "/" +
									strValue.substring(0,4);
						} else {
							return "00/" + strValue.substring(0,4);
						}
					} else {
						if(strValue.length() == 6) {
							if(strValue.substring(4, 6) != "" && strValue.substring(4, 6) != null) {
								return strValue.substring(4, 6) + "/" +
										strValue.substring(0,4);
							} else {
								return "00/" + strValue.substring(0,4);
							}			
						}
					}
				}
			} else {
				throw new Exception("Erro:-66600 em GetDateMonthYear. O atributo especificado (\""+ strField + " \") n�o pode ser encontrado");
				
		//				Err.Raise -66600, "GetDateMonthYear", "O atributo especificado (" & strField & ") n�o pode ser encontrado" 
			}
				
		}
		else {
			throw new Exception("Erro:-66601 em GetDateMonthYear. O node especificado � inv�lido");
		//		Err.Raise -66601, "GetDateMonthYear", "O node especificado � inv�lido" 
		}
	
				
		return "";
		
	}
	
	

	public static String getDateMonthYear_AELD(Node objNode, String strField) throws Exception {

		String strValue;
		Node objAtributo;
		
		if(!(objNode == null)){
				objAtributo = objNode.selectSingleNode("field[@id='" + strField + "']/@value");
				
				if(!(objAtributo == null)){
						strValue = objAtributo.getText();

						if(strValue != "") {
								return strValue.substring(2, 4) + "/" + strValue.substring(strValue.length() - 4);
						}
				}else {
				throw new Exception("Erro:-66600 em GetDateMonthYear_AELD. O atributo especificado (\""+ strField + " \") n�o pode ser encontrado");
				//		Err.Raise -66600, "GetDateMonthYear_AELD", "O atributo especificado (" & strField & ") n�o pode ser encontrado" 
				}
		}else {
			throw new Exception("Erro:-66601 em GetDateMonthYear_AELD. O node especificado � inv�lido");
				//Err.Raise -66601, "GetDateMonthYear_AELD", "O node especificado � inv�lido" 
		}	
		return "";
	}

}
