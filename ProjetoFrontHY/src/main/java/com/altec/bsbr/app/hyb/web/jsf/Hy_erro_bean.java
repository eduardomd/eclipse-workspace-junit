package com.altec.bsbr.app.hyb.web.jsf;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name="hyb_erro_bean")
@ViewScoped
public class Hy_erro_bean {
	
	private Object strErro = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strErro");

	public Hy_erro_bean() {
		
	}
	
	public Hy_erro_bean(Object strErro) {
		this.strErro = strErro;
	}

	public Object getStrErro() {
		return strErro;
	}

	public void setStrErro(Object strErro) {
		this.strErro = strErro;
	}

	
}
