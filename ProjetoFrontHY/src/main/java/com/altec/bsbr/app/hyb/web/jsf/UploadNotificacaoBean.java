package com.altec.bsbr.app.hyb.web.jsf;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;


@ManagedBean(name="UploadNotificacaoBean")
@ViewScoped
public class UploadNotificacaoBean implements Serializable {
     
	private static final long serialVersionUID = 1L;
    
	/* TODO Ver o path correto para salvar arquivos */
	private final String strCaminhoServidor = "C:\\backlevel\\anexos\\";
	//private final String strCaminhoServidor = System.getProperty("user.dir");
	
	private UploadedFile arquivoUpload;
	
	private String strNmArqAnex;
	private String strNmArquivo;
	private String strCaminho;
	private String strNumOS;
	
	/* Query String */
	private Object strTpAnexo = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strTpAnexo");
	private Object strNrSeqOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs");
	private Object strFlg = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strFlg");
	private Object strSeqTrei = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strSeqTrei");
	
	
	public void formSubmit(FileUploadEvent event) {
		/* Salvando arquivo no path */
		try {
			
			arquivoUpload = event.getFile();
			System.out.println(this.getStrNumOS());
			if (arquivoUpload == null)
				throw new Exception("Arquivo nulo");
			
			if (arquivoUpload.getSize() > 1024000) {
				RequestContext.getCurrentInstance().execute("alert('O arquivo enviado ultrapassa o limite de 1MB permitido para envio de arquivos do Portal Corporativo, favor verificar!')");
				throw new Exception("O arquivo enviado ultrapassa o limite de 1MB permitido para envio de arquivos do Portal Corporativo, favor verificar!");
			}
			 System.out.println("entrou " + this.arquivoUpload.getFileName());
		
			this.strNmArqAnex = this.arquivoUpload.getFileName();

			File folder = new File(strCaminhoServidor, "anexos");
			File file = new File(folder, arquivoUpload.getFileName());
						
			if (!folder.exists()) {				
				folder.mkdir();
			}

			OutputStream out = new FileOutputStream(file);
			out.write(arquivoUpload.getContents());
			out.close();

			System.out.println("O arquivo '"+arquivoUpload.getFileName()+"' foi salvo com sucesso no path '"+this.strCaminhoServidor+"'"); //apenas para debug
			
			/* TODO Chamada ao backend, obter novo path, strCaminhoMudado */
			
			
			strCaminho = strCaminhoServidor + arquivoUpload.getFileName();
			
			RequestContext.getCurrentInstance().execute("alert('O arquivo foi salvo com sucesso!')");
			RequestContext.getCurrentInstance().execute("document.getElementById('Form1:txtOS').value = " + " ' " +strCaminho + "';");
			RequestContext.getCurrentInstance().execute("retornoUpload('"+strCaminho+"')");
		} catch (Exception e) {
			/* TODO Tratamento de erro */
			System.out.println("Aconteceu o seguinte erro no Upload de arquivos: "+e.getMessage());
		}		
	}		
	

	public String getStrCaminho() {
		return strCaminho;
	}

	public void setStrCaminho(String strCaminho) {
		this.strCaminho = strCaminho;
	}

	public Object getStrFlg() {
		return strFlg;
	}

	public void setStrFlg(Object strFlg) {
		this.strFlg = strFlg;
	}

	public UploadedFile getArquivoUpload() {
		return arquivoUpload;
	}

	public void setArquivoUpload(UploadedFile arquivoUpload) {
		this.arquivoUpload = arquivoUpload;
	}

	public String getStrNmArqAnex() {
		return strNmArqAnex;
	}

	public void setStrNmArqAnex(String strNmArqAnex) {
		this.strNmArqAnex = strNmArqAnex;
	}
	
	public Object getStrTpAnexo() {
		return strTpAnexo;
	}


	public void setStrTpAnexo(Object strTpAnexo) {
		this.strTpAnexo = strTpAnexo;
	}


	public Object getStrNrSeqOs() {
		return strNrSeqOs;
	}


	public void setStrNrSeqOs(Object strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}


	public Object getStrSeqTrei() {
		return strSeqTrei;
	}


	public void setStrSeqTrei(Object strSeqTrei) {
		this.strSeqTrei = strSeqTrei;
	}


	public String getStrNmArquivo() {
		return strNmArquivo;
	}


	public void setStrNmArquivo(String strNmArquivo) {
		this.strNmArquivo = strNmArquivo;
	}


	public String getStrNumOS() {
		return strNumOS;
	}


	public void setStrNumOS(String strNumOS) {
		this.strNumOS = strNumOS;
	}

}