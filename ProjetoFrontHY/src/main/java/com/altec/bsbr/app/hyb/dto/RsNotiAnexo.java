package com.altec.bsbr.app.hyb.dto;

import java.io.File;

public class RsNotiAnexo {
	
	private String NM_CAMI_ARQU;
	
	//propriedades que eram adquirdas dinamicamente na pagina
	private String strArquivo;
	private String strCaminho;
	
	public String getStrArquivo() {
		//strArquivo = Right(objRsNotiAnexo("NM_CAMI_ARQU").value,Instr(strReverse(objRsNotiAnexo("NM_CAMI_ARQU").Value),"/")-1)
		
		//String reverse = new StringBuilder(NM_CAMI_ARQU).reverse().toString();
		//return NM_CAMI_ARQU.substring(NM_CAMI_ARQU.length() - (reverse.indexOf('/') + 1) - 1);
		File f = new File(this.NM_CAMI_ARQU);
		return f.getName();
	}

	public String getStrCaminho() {
		//strCaminho = Left(objRsNotiAnexo("NM_CAMI_ARQU").value,Len(objRsNotiAnexo("NM_CAMI_ARQU").Value) - Instr(strReverse(objRsNotiAnexo("NM_CAMI_ARQU").Value),"/") + 1)
		
		//String reverse = new StringBuilder(NM_CAMI_ARQU).reverse().toString();
		//return NM_CAMI_ARQU.substring(0, ((NM_CAMI_ARQU.length() - (reverse.indexOf('/') + 1)) + 1));
		File f = new File(this.NM_CAMI_ARQU);
		return f.getParent();		
	}
	

	public String getNM_CAMI_ARQU() {
		return NM_CAMI_ARQU;
	}

	public void setNM_CAMI_ARQU(String nM_CAMI_ARQU) {
		NM_CAMI_ARQU = nM_CAMI_ARQU;
	}
	
}
