package com.altec.bsbr.app.hyb.dto;

public class CadOsEventosContaCanalModel {

	private String contaNrSequCntaBncr; 
	private String contaNrCnta;
	private String contaNrSequParpProc;		
	private String contaNrSequFrauCnal;
	private String contaCdBanc;
	private String contaTpOperParp;
	private String contaQtdLanc;
	
	public String getContaNrSequCntaBncr() {
		return contaNrSequCntaBncr;
	}
	public void setContaNrSequCntaBncr(String contaNrSequCntaBncr) {
		this.contaNrSequCntaBncr = contaNrSequCntaBncr;
	}
	public String getContaNrCnta() {
		return contaNrCnta;
	}
	public void setContaNrCnta(String contaNrCnta) {
		this.contaNrCnta = contaNrCnta;
	}
	public String getContaNrSequParpProc() {
		return contaNrSequParpProc;
	}
	public void setContaNrSequParpProc(String contaNrSequParpProc) {
		this.contaNrSequParpProc = contaNrSequParpProc;
	}
	public String getContaNrSequFrauCnal() {
		return contaNrSequFrauCnal;
	}
	public void setContaNrSequFrauCnal(String contaNrSequFrauCnal) {
		this.contaNrSequFrauCnal = contaNrSequFrauCnal;
	}
	public String getContaCdBanc() {
		return contaCdBanc;
	}
	public void setContaCdBanc(String contaCdBanc) {
		this.contaCdBanc = contaCdBanc;
	}
	public String getContaTpOperParp() {
		return contaTpOperParp;
	}
	public void setContaTpOperParp(String contaTpOperParp) {
		this.contaTpOperParp = contaTpOperParp;
	}
	public String getContaQtdLanc() {
		return contaQtdLanc;
	}
	public void setContaQtdLanc(String contaQtdLanc) {
		this.contaQtdLanc = contaQtdLanc;
	}
}
