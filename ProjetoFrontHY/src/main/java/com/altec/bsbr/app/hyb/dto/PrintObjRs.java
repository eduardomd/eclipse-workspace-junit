package com.altec.bsbr.app.hyb.dto;

import javax.faces.bean.ManagedBean;
@ManagedBean(name = "objRs")
public class PrintObjRs {
	
	private String TP_PESSOA;
	private String MATRICULA;
	private String NOME;
	private String CARGO;
	private String CPF_CNPJ;
	private String PARTICIPACAO;
	private String agencia;
	private String ccorrente;
	private String SEGMENTO;
	
	public PrintObjRs() {}
	
	public String getTP_PESSOA() {
		return TP_PESSOA;
	}
	public void setTP_PESSOA(String tP_PESSOA) {
		TP_PESSOA = tP_PESSOA;
	}
	public String getMATRICULA() {
		return MATRICULA;
	}
	public void setMATRICULA(String mATRICULA) {
		MATRICULA = mATRICULA;
	}
	public String getNOME() {
		return NOME;
	}
	public void setNOME(String nOME) {
		NOME = nOME;
	}
	public String getCARGO() {
		return CARGO;
	}
	public void setCARGO(String cARGO) {
		CARGO = cARGO;
	}
	public String getCPF_CNPJ() {
		return CPF_CNPJ;
	}
	public void setCPF_CNPJ(String cPF_CNPJ) {
		CPF_CNPJ = cPF_CNPJ;
	}
	public String getPARTICIPACAO() {
		return PARTICIPACAO;
	}
	public void setPARTICIPACAO(String pARTICIPACAO) {
		PARTICIPACAO = pARTICIPACAO;
	}
	public String getAgencia() {
		return agencia;
	}
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}
	public String getCcorrente() {
		return ccorrente;
	}
	public void setCcorrente(String ccorrente) {
		this.ccorrente = ccorrente;
	}
	public String getSEGMENTO() {
		return SEGMENTO;
	}
	public void setSEGMENTO(String sEGMENTO) {
		SEGMENTO = sEGMENTO;
	}
	
	public String getAgenciaCCorrente() {
    	String retorno = "";
		if (this.getAgencia() == null) {
			agencia = "";
		} else {
			retorno="0000" + this.getAgencia();
			retorno= retorno.substring(retorno.length() - 4) + "/" + this.getCcorrente();
		}
		return retorno;
	}
}



