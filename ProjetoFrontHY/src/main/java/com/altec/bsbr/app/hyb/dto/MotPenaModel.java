package com.altec.bsbr.app.hyb.dto;

public class MotPenaModel {
	private String id;
	private String motivo;
	private String EXCLUSAO;
	
	public MotPenaModel(String id, String motivo, String EXCLUSAO) {		
		this.id = id;
		this.motivo = motivo;
		this.EXCLUSAO = EXCLUSAO;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public String getEXCLUSAO() {
		return EXCLUSAO;
	}

	public void setEXCLUSAO(String eXCLUSAO) {
		EXCLUSAO = eXCLUSAO;
	}	
	
}
