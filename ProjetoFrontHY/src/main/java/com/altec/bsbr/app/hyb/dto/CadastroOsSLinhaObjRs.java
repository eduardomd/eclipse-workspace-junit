package com.altec.bsbr.app.hyb.dto;

public class CadastroOsSLinhaObjRs {
	
	private String CODIGO;
	private String NOME;
	private String SQ_EMPR;
	private String NO_EMPR;
	private String SQ_PILA;
	private String DE_PILA;
	private String SQ_SUB_FATR_RISC;
	private String NO_SUB_FATR_RISC;
	
	public CadastroOsSLinhaObjRs() {
		
	}
	
	public String getCODIGO() {
		return CODIGO;
	}
	public void setCODIGO(String cODIGO) {
		CODIGO = cODIGO;
	}
	public String getNOME() {
		return NOME;
	}
	public void setNOME(String nOME) {
		NOME = nOME;
	}
	public String getSQ_EMPR() {
		return SQ_EMPR;
	}
	public void setSQ_EMPR(String sQ_EMPR) {
		SQ_EMPR = sQ_EMPR;
	}
	public String getNO_EMPR() {
		return NO_EMPR;
	}
	public void setNO_EMPR(String nO_EMPR) {
		NO_EMPR = nO_EMPR;
	}
	public String getSQ_PILA() {
		return SQ_PILA;
	}
	public void setSQ_PILA(String sQ_PILA) {
		SQ_PILA = sQ_PILA;
	}
	public String getDE_PILA() {
		return DE_PILA;
	}
	public void setDE_PILA(String dE_PILA) {
		DE_PILA = dE_PILA;
	}
	public String getSQ_SUB_FATR_RISC() {
		return SQ_SUB_FATR_RISC;
	}
	public void setSQ_SUB_FATR_RISC(String sQ_SUB_FATR_RISC) {
		SQ_SUB_FATR_RISC = sQ_SUB_FATR_RISC;
	}
	public String getNO_SUB_FATR_RISC() {
		return NO_SUB_FATR_RISC;
	}
	public void setNO_SUB_FATR_RISC(String nO_SUB_FATR_RISC) {
		NO_SUB_FATR_RISC = nO_SUB_FATR_RISC;
	}

}
