package com.altec.bsbr.app.hyb.dto;

public class ObjRsArea {
	private String CODIGO;
	private String NOME;
	
	public ObjRsArea() {}
	
	public ObjRsArea(String cODIGO, String nOME) {
		super();
		CODIGO = cODIGO;
		NOME = nOME;
	}

	public String getCODIGO() {
		return CODIGO;
	}
	public void setCODIGO(String cODIGO) {
		CODIGO = cODIGO;
	}
	public String getNOME() {
		return NOME;
	}
	public void setNOME(String nOME) {
		NOME = nOME;
	}
	
	
}
