package com.altec.bsbr.app.hyb.dto;

public class RelOSCustoTotalOSModel {

	private String ROWNUM;
	private String NR_SEQU_ORDE_SERV;
	private String NM_SITU_ORDE_SERV;
	private String DT_ABER_ORDE_SERV;
	private String DT_ENCERRAMENTO;
	private String VL_ENVO;
	private String VL_PERD_EFET;
	private String VL_CUST_ORDE_DE_SERV;
	private String VL_ENVO_FORMATADO;
	private String VL_PERD_EFET_FORMATADO;
	private String VL_CUST_ORDE_DE_SERV_FORMATADO;
	
	public RelOSCustoTotalOSModel(
			String ROWNUM,
			String NR_SEQU_ORDE_SERV,
			String NM_SITU_ORDE_SERV,
			String DT_ABER_ORDE_SERV,
			String DT_ENCERRAMENTO,
			String VL_ENVO,
			String VL_PERD_EFET,
			String VL_CUST_ORDE_DE_SERV,
			String VL_ENVO_FORMATADO,
			String VL_PERD_EFET_FORMATADO,
			String VL_CUST_ORDE_DE_SERV_FORMATADO) {
		this.setROWNUM(ROWNUM);
		this.setNR_SEQU_ORDE_SERV(NR_SEQU_ORDE_SERV);
		this.setNM_SITU_ORDE_SERV(NM_SITU_ORDE_SERV);
		this.setDT_ABER_ORDE_SERV(DT_ABER_ORDE_SERV);
		this.setDT_ENCERRAMENTO(DT_ENCERRAMENTO);
		this.setVL_ENVO(VL_ENVO);
		this.setVL_PERD_EFET(VL_PERD_EFET);
		this.setVL_CUST_ORDE_DE_SERV(VL_CUST_ORDE_DE_SERV);
		this.setVL_ENVO_FORMATADO(VL_ENVO_FORMATADO);
		this.setVL_PERD_EFET_FORMATADO(VL_PERD_EFET_FORMATADO);
		this.setVL_CUST_ORDE_DE_SERV_FORMATADO(VL_CUST_ORDE_DE_SERV_FORMATADO);
	}

	public String getROWNUM() {
		return ROWNUM;
	}

	public void setROWNUM(String rOWNUM) {
		ROWNUM = rOWNUM;
	}

	public String getNR_SEQU_ORDE_SERV() {
		return NR_SEQU_ORDE_SERV;
	}

	public void setNR_SEQU_ORDE_SERV(String nR_SEQU_ORDE_SERV) {
		NR_SEQU_ORDE_SERV = nR_SEQU_ORDE_SERV;
	}

	public String getNM_SITU_ORDE_SERV() {
		return NM_SITU_ORDE_SERV;
	}

	public void setNM_SITU_ORDE_SERV(String nM_SITU_ORDE_SERV) {
		NM_SITU_ORDE_SERV = nM_SITU_ORDE_SERV;
	}

	public String getDT_ABER_ORDE_SERV() {
		return DT_ABER_ORDE_SERV;
	}

	public void setDT_ABER_ORDE_SERV(String dT_ABER_ORDE_SERV) {
		DT_ABER_ORDE_SERV = dT_ABER_ORDE_SERV;
	}

	public String getDT_ENCERRAMENTO() {
		return DT_ENCERRAMENTO;
	}

	public void setDT_ENCERRAMENTO(String dT_ENCERRAMENTO) {
		DT_ENCERRAMENTO = dT_ENCERRAMENTO;
	}

	public String getVL_ENVO() {
		return VL_ENVO;
	}

	public void setVL_ENVO(String vL_ENVO) {
		VL_ENVO = vL_ENVO;
	}

	public String getVL_PERD_EFET() {
		return VL_PERD_EFET;
	}

	public void setVL_PERD_EFET(String vL_PERD_EFET) {
		VL_PERD_EFET = vL_PERD_EFET;
	}

	public String getVL_CUST_ORDE_DE_SERV() {
		return VL_CUST_ORDE_DE_SERV;
	}

	public void setVL_CUST_ORDE_DE_SERV(String vL_CUST_ORDE_DE_SERV) {
		VL_CUST_ORDE_DE_SERV = vL_CUST_ORDE_DE_SERV;
	}

	public String getVL_ENVO_FORMATADO() {
		return VL_ENVO_FORMATADO;
	}

	public void setVL_ENVO_FORMATADO(String vL_ENVO_FORMATADO) {
		VL_ENVO_FORMATADO = vL_ENVO_FORMATADO;
	}

	public String getVL_PERD_EFET_FORMATADO() {
		return VL_PERD_EFET_FORMATADO;
	}

	public void setVL_PERD_EFET_FORMATADO(String vL_PERD_EFET_FORMATADO) {
		VL_PERD_EFET_FORMATADO = vL_PERD_EFET_FORMATADO;
	}

	public String getVL_CUST_ORDE_DE_SERV_FORMATADO() {
		return VL_CUST_ORDE_DE_SERV_FORMATADO;
	}

	public void setVL_CUST_ORDE_DE_SERV_FORMATADO(String vL_CUST_ORDE_DE_SERV_FORMATADO) {
		VL_CUST_ORDE_DE_SERV_FORMATADO = vL_CUST_ORDE_DE_SERV_FORMATADO;
	}
}