package com.altec.bsbr.app.hyb.web.jsf;

import java.io.Serializable;
import java.util.Map;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.altec.bsbr.app.hyb.dto.CadastroOsPesquisaEnvolv;

@Component("cadastroOsPesquisaEnvolvBean")
@Scope("session")
public class CadastroOsPesquisaEnvolvBean extends BasicBBean {

	private static final long serialVersionUID = 1L;
	private CadastroOsPesquisaEnvolv cadastroOsPesquisaEnvolv;
	
	public CadastroOsPesquisaEnvolvBean() {
		
		CadastroOsPesquisaEnvolv cadastroOsPesquisaEnvolv = new CadastroOsPesquisaEnvolv();

	}


	public CadastroOsPesquisaEnvolv getCadastroOsPesquisaEnvolv() {
		return cadastroOsPesquisaEnvolv;
	}

	public void setCadastroOsPesquisaEnvolv(CadastroOsPesquisaEnvolv cadastroOsPesquisaEnvolv) {
		this.cadastroOsPesquisaEnvolv = cadastroOsPesquisaEnvolv;
	}

}
