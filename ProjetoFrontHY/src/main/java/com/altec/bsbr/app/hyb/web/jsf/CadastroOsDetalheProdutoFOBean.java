package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.CadastroOSDetalheProdutoFO;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsCanais.XHYCadOsCanaisEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;


@Component("CadastroOsDetalheProdutoFOBean")
@Scope("session")
public class CadastroOsDetalheProdutoFOBean extends BasicBBean {

	private static final long serialVersionUID = 1L;

	private String strNrSeqTranFrau;
	
	private CadastroOSDetalheProdutoFO cadastroOSDetalheProdutoFO;

	@Autowired
	private XHYCadOsCanaisEndPoint cadOsCanais;
	
	private Locale ptBR = new Locale("pt", "BR");
	
	@PostConstruct
	public void init() {
		System.out.println("init CadastroOsDetalheProdutoFoBean:: "+FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqTranFrau"));

		if (FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqTranFrau") != null) {
			this.strNrSeqTranFrau = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqTranFrau").toString();
			retornaOperacaoEspecifica();
		}
	}
	
	public void retornaOperacaoEspecifica() {
		try {
			JSONObject json = new JSONObject(
					cadOsCanais.retornaOperacaoEspecifica(this.strNrSeqTranFrau));
			JSONArray pcursor = json.getJSONArray("PCURSOR");

			for(int i = 0; i < pcursor.length(); i++)			
				this.cadastroOSDetalheProdutoFO = new CadastroOSDetalheProdutoFO(
								pcursor.getJSONObject(i).isNull("VL_TRAN") ? "" : this.formataValorRecebido(pcursor.getJSONObject(i).getString("VL_TRAN")) , 
								pcursor.getJSONObject(i).isNull("TP_ESPE") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("TP_ESPE")) , 
								pcursor.getJSONObject(i).isNull("NM_BANC_CRED") ? "" : pcursor.getJSONObject(i).getString("NM_BANC_CRED") , 
								pcursor.getJSONObject(i).isNull("TP_OUTR_FRAU") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("TP_OUTR_FRAU")) , 
								pcursor.getJSONObject(i).isNull("NR_SEQU_ORDE_SERV") ? "" : pcursor.getJSONObject(i).getString("NR_SEQU_ORDE_SERV") , 
								pcursor.getJSONObject(i).isNull("DESC_TRAN") ? "" : pcursor.getJSONObject(i).getString("DESC_TRAN") , 
								pcursor.getJSONObject(i).isNull("NM_LOCA_TRAN") ? "" : pcursor.getJSONObject(i).getString("NM_LOCA_TRAN") , 
								pcursor.getJSONObject(i).isNull("TX_DADO_COMP") ? "" : pcursor.getJSONObject(i).getString("TX_DADO_COMP") , 
								pcursor.getJSONObject(i).isNull("NM_CLIE_FAVD") ? "" : pcursor.getJSONObject(i).getString("NM_CLIE_FAVD") , 
								pcursor.getJSONObject(i).isNull("DT_PGTO_CHEQ") ? "" : pcursor.getJSONObject(i).getString("DT_PGTO_CHEQ") , 
								pcursor.getJSONObject(i).isNull("TP_FRAU_CHEQ") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("TP_FRAU_CHEQ")) , 
								pcursor.getJSONObject(i).isNull("TP_CNAL_PGTO") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("TP_CNAL_PGTO")) , 
								pcursor.getJSONObject(i).isNull("NR_DEPO") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("NR_DEPO")) , 
								pcursor.getJSONObject(i).isNull("TP_SUBP_DEPO") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("TP_SUBP_DEPO")) , 
								pcursor.getJSONObject(i).isNull("TP_ABER_CNTA") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("TP_ABER_CNTA")) , 
								pcursor.getJSONObject(i).isNull("NR_CHEQ") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("NR_CHEQ")) , 
								pcursor.getJSONObject(i).isNull("TP_FORM_DEPO") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("TP_FORM_DEPO")) , 
								pcursor.getJSONObject(i).isNull("TP_SUBP") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("TP_SUBP")) , 
								pcursor.getJSONObject(i).isNull("NR_CDC_LEAS_GRVM") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("NR_CDC_LEAS_GRVM")) , 
								pcursor.getJSONObject(i).isNull("NM_OPED_CDC_LEAS_GRVM") ? "" : pcursor.getJSONObject(i).getString("NM_OPED_CDC_LEAS_GRVM") , 
								pcursor.getJSONObject(i).isNull("NR_PARC_CDC_LEAS_GRVM") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("NR_PARC_CDC_LEAS_GRVM")) , 
								pcursor.getJSONObject(i).isNull("TP_CNAL_ORIG_CDC_LEAS_GRVM") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("TP_CNAL_ORIG_CDC_LEAS_GRVM")) , 
								pcursor.getJSONObject(i).isNull("VL_PAGO_CDC_LEAS_GRVM") ? "" : this.formataValorRecebido(String.valueOf(pcursor.getJSONObject(i).getDouble("VL_PAGO_CDC_LEAS_GRVM"))) , 
								pcursor.getJSONObject(i).isNull("NR_EQPT_DEPO") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("NR_EQPT_DEPO")) , 
								pcursor.getJSONObject(i).isNull("NR_DEBT") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("NR_DEBT")) , 
								pcursor.getJSONObject(i).isNull("NM_DEBT") ? "" : pcursor.getJSONObject(i).getString("NM_DEBT") , 
								pcursor.getJSONObject(i).isNull("CD_DEBT") ? "" : pcursor.getJSONObject(i).getString("CD_DEBT") , 
								pcursor.getJSONObject(i).isNull("NR_TITU") ? "" : pcursor.getJSONObject(i).getString("NR_TITU") , 
								pcursor.getJSONObject(i).isNull("NM_CEDE_TITU") ? "" : pcursor.getJSONObject(i).getString("NM_CEDE_TITU") , 
								pcursor.getJSONObject(i).isNull("IN_CONF_ENDE_REFE") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("IN_CONF_ENDE_REFE")) , 
								pcursor.getJSONObject(i).isNull("IN_DOCT_PROC_COPT") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("IN_DOCT_PROC_COPT")) , 
								pcursor.getJSONObject(i).isNull("NM_BANC_CRED_00") ? "" : pcursor.getJSONObject(i).getString("NM_BANC_CRED_00") , 
								pcursor.getJSONObject(i).isNull("NM_BANC_CRED_01") ? "" : pcursor.getJSONObject(i).getString("NM_BANC_CRED_01") , 
								pcursor.getJSONObject(i).isNull("VL_ABER_CDC_LEAS_GRVM") ? "" : this.formataValorRecebido(String.valueOf(pcursor.getJSONObject(i).getDouble("VL_ABER_CDC_LEAS_GRVM"))) , 
								pcursor.getJSONObject(i).isNull("TP_FRAU_AUTO_ATEN") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("TP_FRAU_AUTO_ATEN")) , 
								pcursor.getJSONObject(i).isNull("TP_OPER_FRAU") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("TP_OPER_FRAU")) , 
								pcursor.getJSONObject(i).isNull("VL_CNTA_CDC_LEAS_GRVM") ? "" : this.formataValorRecebido(String.valueOf(pcursor.getJSONObject(i).getDouble("VL_CNTA_CDC_LEAS_GRVM"))) , 
								pcursor.getJSONObject(i).isNull("TP_FLOAT") ? "" : String.valueOf(pcursor.getJSONObject(i).getDouble("TP_FLOAT")) 
								);
			//formatacao q anteriormente era feita na tela...
			this.cadastroOSDetalheProdutoFO.setNM_BANC_CRED_00(this.cadastroOSDetalheProdutoFO.getNM_BANC_CRED().isEmpty() ? 
			   "" : 
			   this.cadastroOSDetalheProdutoFO.getNM_BANC_CRED().substring(0, this.cadastroOSDetalheProdutoFO.getNM_BANC_CRED().indexOf("|")));

			this.cadastroOSDetalheProdutoFO.setNM_BANC_CRED_01(this.cadastroOSDetalheProdutoFO.getNM_BANC_CRED().isEmpty() ?
				"" :
				this.cadastroOSDetalheProdutoFO.getNM_BANC_CRED().substring(this.cadastroOSDetalheProdutoFO.getNM_BANC_CRED().length() - (this.cadastroOSDetalheProdutoFO.getNM_BANC_CRED().length() - (this.cadastroOSDetalheProdutoFO.getNM_BANC_CRED().indexOf("|") + 1))));
			
			System.out.println("cadastroOSDetalheProdutoFO objeto "+ToStringBuilder.reflectionToString(cadastroOSDetalheProdutoFO));
			
		} catch (Exception e) {
			e.printStackTrace();
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	public void salvar() {
		try {
			this.cadastroOSDetalheProdutoFO.setNM_BANC_CRED(
					this.cadastroOSDetalheProdutoFO.getNM_BANC_CRED_00() + "|" + this.cadastroOSDetalheProdutoFO.getNM_BANC_CRED_01());
			
			Date dtPgto = new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyy", Locale.US).parse(this.cadastroOSDetalheProdutoFO.getDT_PGTO_CHEQ());
			System.out.println("dtPgto "+dtPgto);
			String dtPgtoStr = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(dtPgto);
			System.out.println("dtPgtoStr "+dtPgtoStr);
			
			System.out.println("Model "+ToStringBuilder.reflectionToString(this.cadastroOSDetalheProdutoFO));
			JSONObject json = new JSONObject(
					cadOsCanais.salvarDetalheOperacaoFO(
							this.strNrSeqTranFrau, 
							this.cadastroOSDetalheProdutoFO.getNM_LOCA_TRAN(), 
							this.cadastroOSDetalheProdutoFO.getTP_ESPE(), 
							this.cadastroOSDetalheProdutoFO.getTX_DADO_COMP(), 
							this.cadastroOSDetalheProdutoFO.getTP_ABER_CNTA(), 
							this.cadastroOSDetalheProdutoFO.getIN_CONF_ENDE_REFE(), 
							this.cadastroOSDetalheProdutoFO.getIN_DOCT_PROC_COPT(), 
							this.cadastroOSDetalheProdutoFO.getNM_CLIE_FAVD(), 
							dtPgtoStr, 
							this.cadastroOSDetalheProdutoFO.getNR_CHEQ(), 
							this.cadastroOSDetalheProdutoFO.getTP_FRAU_CHEQ(), 
							this.cadastroOSDetalheProdutoFO.getTP_CNAL_PGTO(), 
							this.cadastroOSDetalheProdutoFO.getNR_DEPO(), 
							this.cadastroOSDetalheProdutoFO.getTP_SUBP_DEPO(), 
							this.cadastroOSDetalheProdutoFO.getTP_FORM_DEPO(), 
							this.cadastroOSDetalheProdutoFO.getNM_BANC_CRED(), 
							this.cadastroOSDetalheProdutoFO.getNR_EQPT_DEPO(), 
							this.cadastroOSDetalheProdutoFO.getNR_TITU(), 
							this.cadastroOSDetalheProdutoFO.getTP_FLOAT(), 
							this.cadastroOSDetalheProdutoFO.getNM_CEDE_TITU(), 
							this.cadastroOSDetalheProdutoFO.getTP_SUBP(), 
							this.cadastroOSDetalheProdutoFO.getNR_DEBT(), 
							this.cadastroOSDetalheProdutoFO.getNM_DEBT(), 
							this.cadastroOSDetalheProdutoFO.getCD_DEBT(), 
							this.cadastroOSDetalheProdutoFO.getNR_CDC_LEAS_GRVM(), 
							this.cadastroOSDetalheProdutoFO.getNM_OPED_CDC_LEAS_GRVM(), 
							this.formataValorParaEnvio(this.cadastroOSDetalheProdutoFO.getVL_CNTA_CDC_LEAS_GRVM()), 
							this.cadastroOSDetalheProdutoFO.getNR_PARC_CDC_LEAS_GRVM(), 
							this.formataValorParaEnvio(this.cadastroOSDetalheProdutoFO.getVL_ABER_CDC_LEAS_GRVM()), 
							this.cadastroOSDetalheProdutoFO.getTP_CNAL_ORIG_CDC_LEAS_GRVM(), 
							this.formataValorParaEnvio(this.cadastroOSDetalheProdutoFO.getVL_PAGO_CDC_LEAS_GRVM()), 
							this.cadastroOSDetalheProdutoFO.getTP_OPER_FRAU(), 
							this.cadastroOSDetalheProdutoFO.getTP_FRAU_AUTO_ATEN(), 
							this.cadastroOSDetalheProdutoFO.getTP_OUTR_FRAU()
							));
			
			
							
			RequestContext.getCurrentInstance().execute("alert('Dados Salvos com Sucesso!');");
		} catch (Exception e) {
			e.printStackTrace();
			RequestContext.getCurrentInstance().execute("alert('Erro: Não foi possível salvar os dados!');");
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	public String formataValorParaEnvio(String valorRecebido) {
		if (valorRecebido.isEmpty()) {
			return "";
		} else {
			System.out.println("valorRecebido "+valorRecebido);
			System.out.println("return "+valorRecebido.replace(".", "").replace(",", "."));
			return valorRecebido.replace(".", "").replace(",", ".");			
		}
	}
	
	public String formataValorRecebido(String valorRecebido) {
		return  valorRecebido.isEmpty() ? 
				"" :
				NumberFormat.getCurrencyInstance(ptBR).format(Double.parseDouble(valorRecebido)).replace("R$", "").trim();
	}
	
	public String getStrNrSeqTranFrau() {
		return strNrSeqTranFrau;
	}

	public void setStrNrSeqTranFrau(String strNrSeqTranFrau) {
		this.strNrSeqTranFrau = strNrSeqTranFrau;
	}

	public CadastroOSDetalheProdutoFO getCadastroOSDetalheProdutoFO() {
		return cadastroOSDetalheProdutoFO;
	}

	public void setCadastroOSDetalheProdutoFO(CadastroOSDetalheProdutoFO cadastroOSDetalheProdutoFO) {
		this.cadastroOSDetalheProdutoFO = cadastroOSDetalheProdutoFO;
	}
	
	


}