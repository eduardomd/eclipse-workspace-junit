package com.altec.bsbr.app.hyb.dto;

public class Hyb_relosfraudeiniciadaencerrada_model {
	
	private String EVENTO;
	private String CD_CNAL;
	private String BANCO;
	private int QUANTIDADE;
	private int VALOR;
	private String CANAL;
	
	public Hyb_relosfraudeiniciadaencerrada_model() {
		
	}

	public Hyb_relosfraudeiniciadaencerrada_model(String eVENTO, String cD_CNAL, String bANCO, int qUANTIDADE,
			int vALOR, String cANAL) {
		EVENTO = eVENTO;
		CD_CNAL = cD_CNAL;
		BANCO = bANCO;
		QUANTIDADE = qUANTIDADE;
		VALOR = vALOR;
		CANAL = cANAL;
	}
	
	public String getEVENTO() {
		return EVENTO;
	}
	public void setEVENTO(String eVENTO) {
		EVENTO = eVENTO;
	}
	public String getCD_CNAL() {
		return CD_CNAL;
	}
	public void setCD_CNAL(String cD_CNAL) {
		CD_CNAL = cD_CNAL;
	}
	public String getBANCO() {
		return BANCO;
	}
	public void setBANCO(String bANCO) {
		BANCO = bANCO;
	}
	public int getQUANTIDADE() {
		return QUANTIDADE;
	}
	public void setQUANTIDADE(int qUANTIDADE) {
		QUANTIDADE = qUANTIDADE;
	}
	public int getVALOR() {
		return VALOR;
	}
	public void setVALOR(int vALOR) {
		VALOR = vALOR;
	}
	public String getCANAL() {
		return CANAL;
	}
	public void setCANAL(String cANAL) {
		CANAL = cANAL;
	}
	
	

}
