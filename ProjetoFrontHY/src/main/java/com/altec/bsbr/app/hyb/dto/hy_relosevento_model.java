package com.altec.bsbr.app.hyb.dto;

public class hy_relosevento_model {

	//N OS                               
	//Canal de Origem OS             
	//Status OS                           
	//Pessoas Envolvidas<br>no Processo
	//Status de Participacao          
	//Evento                             
	//Canal                              
	//Proposicao                         
	//Data Encerramento OS               
	
	private String strNOs;
	private String strCnalOrig;
	private String strStatusOs;
	private String strPessoasEnvolvidas;
	private String strStatusPart;
	private String strEvnto;
	private String strCnal;
	private String strProps;
	private String strDtEncOs;
	
	public hy_relosevento_model(String strNOs, String strCnalOrig, String strStatusOs, String strPessoasEnvolvidas,
			String strStatusPart, String strEvnto, String strCnal, String strProps, String strDtEncOs) {
		this.strNOs = strNOs;
		this.strCnalOrig = strCnalOrig;
		this.strStatusOs = strStatusOs;
		this.strPessoasEnvolvidas = strPessoasEnvolvidas;
		this.strStatusPart = strStatusPart;
		this.strEvnto = strEvnto;
		this.strCnal = strCnal;
		this.strProps = strProps;
		this.strDtEncOs = strDtEncOs;
	}
	public String getStrNOs() {
		return strNOs;
	}
	public void setStrNOs(String strNOs) {
		this.strNOs = strNOs;
	}
	public String getStrCnalOrig() {
		return strCnalOrig;
	}
	public void setStrCnalOrig(String strCnalOrig) {
		this.strCnalOrig = strCnalOrig;
	}
	public String getStrStatusOs() {
		return strStatusOs;
	}
	public void setStrStatusOs(String strStatusOs) {
		this.strStatusOs = strStatusOs;
	}
	public String getStrPessoasEnvolvidas() {
		return strPessoasEnvolvidas;
	}
	public void setStrPessoasEnvolvidas(String strPessoasEnvolvidas) {
		this.strPessoasEnvolvidas = strPessoasEnvolvidas;
	}
	public String getStrStatusPart() {
		return strStatusPart;
	}
	public void setStrStatusPart(String strStatusPart) {
		this.strStatusPart = strStatusPart;
	}
	public String getStrEvnto() {
		return strEvnto;
	}
	public void setStrEvnto(String strEvnto) {
		this.strEvnto = strEvnto;
	}
	public String getStrCnal() {
		return strCnal;
	}
	public void setStrCnal(String strCnal) {
		this.strCnal = strCnal;
	}
	public String getStrProps() {
		return strProps;
	}
	public void setStrProps(String strProps) {
		this.strProps = strProps;
	}
	public String getStrDtEncOs() {
		return strDtEncOs;
	}
	public void setStrDtEncOs(String strDtEncOs) {
		this.strDtEncOs = strDtEncOs;
	}
	
	
}
