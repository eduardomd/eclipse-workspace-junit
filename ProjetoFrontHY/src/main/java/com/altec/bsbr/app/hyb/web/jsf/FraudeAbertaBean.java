package com.altec.bsbr.app.hyb.web.jsf;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.RelFraudeAberto;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
 
@Component("fraudeAbertaMB")
@Scope("session")
public class FraudeAbertaBean extends BasicBBean {

	private List<RelFraudeAberto> fraudesAbertas = new ArrayList<RelFraudeAberto>();
	
//	private String pArea;
//	private String pTpRel;
//	private String pNmRel;
//	private String pDtIni;
//	private String pDtFim;

	
	public FraudeAbertaBean() {
        for (int i = 0; i < 20; i++) {
               fraudesAbertas.add(generateRandomFraude());
        }
	}
	
	public String [] bancos = {"Santander", "Itaú", "Bradesco", "Banco do Brasil", "Safra"};
	
	/*public List<RelFraudeAberto> getFraudesAbertas(){
		HttpServletRequest request = (HttpServletRequest) FacesContext
			.getCurrentInstance().getExternalContext().getRequest();
		request.getParameter("pArea");
		request.getParameter("pTpRel");
		request.getParameter("pNmRel");
		request.getParameter("pDtIni");
		request.getParameter("pDtFim");
	}*/

	public List<RelFraudeAberto> getFraudesAbertas() {
		return fraudesAbertas;
	}

	public RelFraudeAberto generateRandomFraude() {
        int indice = (int) Math.floor(Math.random()*5);
        RelFraudeAberto fraude = new RelFraudeAberto();
        fraude.setBanco(bancos[indice]);
        fraude.setQtdOS(indice);
        fraude.setValor((indice + 1) * 3);
        fraude.setCanal("Teste");
         
        return fraude;  
  }
	
	public float getValorTotal () {
		float valorTotal = 0;
		for (int i = 0; i < 20; i++) {
            valorTotal += fraudesAbertas.get(i).getValor();
		}
		return valorTotal;
	}
}