package com.altec.bsbr.app.hyb.dto;

public class PE74_PEM2520AeaResponse {

	public PE74_PEM2520AeaResponse() {}
	
	public PE74_PEM2520AeaResponse(String dC_FORMATO, String pECLATE, String pETIPTE, String pECARTE, String pENUMTE,
			String pEPRETE) {
		super();
		DC_FORMATO = dC_FORMATO;
		PECLATE = pECLATE;
		PETIPTE = pETIPTE;
		PECARTE = pECARTE;
		PENUMTE = pENUMTE;
		PEPRETE = pEPRETE;
	}

	private String DC_FORMATO;

	private String PECLATE;
	
	private String PETIPTE;
	
	private String PECARTE;
	
	private String PENUMTE;
	
	private String PEPRETE;
	
	public String getDC_FORMATO() {
		return DC_FORMATO;
	}
	
	public void setDC_FORMATO(String dC_FORMATO) {
		DC_FORMATO = dC_FORMATO;
	}

	public String getPECLATE() {
		return PECLATE;
	}

	public void setPECLATE(String pECLATE) {
		PECLATE = pECLATE;
	}

	public String getPETIPTE() {
		return PETIPTE;
	}

	public void setPETIPTE(String pETIPTE) {
		PETIPTE = pETIPTE;
	}

	public String getPECARTE() {
		return PECARTE;
	}

	public void setPECARTE(String pECARTE) {
		PECARTE = pECARTE;
	}

	public String getPENUMTE() {
		return PENUMTE;
	}

	public void setPENUMTE(String pENUMTE) {
		PENUMTE = pENUMTE;
	}

	public String getPEPRETE() {
		return PEPRETE;
	}

	public void setPEPRETE(String pEPRETE) {
		PEPRETE = pEPRETE;
	}

}
