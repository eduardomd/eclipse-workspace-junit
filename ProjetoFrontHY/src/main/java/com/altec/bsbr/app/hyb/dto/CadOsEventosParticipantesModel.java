package com.altec.bsbr.app.hyb.dto;

public class CadOsEventosParticipantesModel {

	private String nome; 
	private String tpParp;
	private String sqParp;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getTpParp() {
		return tpParp;
	}
	public void setTpParp(String tpParp) {
		this.tpParp = tpParp;
	}
	public String getSqParp() {
		return sqParp;
	}
	public void setSqParp(String sqParp) {
		this.sqParp = sqParp;
	}
}
