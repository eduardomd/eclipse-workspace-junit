package com.altec.bsbr.app.hyb.web.jsf;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.security.auth.message.callback.PrivateKeyCallback.Request;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.RsNotiManual;
import com.altec.bsbr.app.hyb.dto.UploadNotificacaoModel;
import com.altec.bsbr.app.hyb.dto.UsuarioIncModel;
import com.altec.bsbr.app.hyb.web.util.LogIncUtil;
import com.altec.bsbr.app.hyb.web.util.XHYUsuarioIncService;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAcoesOs.XHYAcoesOsEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmLog.XHYAdmLogEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmNotificacao.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmNotificacao.XHYAdmNotificacaoEndPoint;
import com.altec.bsbr.fw.security.authorization.Authorization;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component("AdmWorkflowNotificacaoManualBean")
@Scope("request")
public class AdmWorkflowNotificacaoManualBean extends BasicBBean {

	private static final long serialVersionUID = 1L;

	private List<UploadNotificacaoModel> objUpload = new ArrayList<UploadNotificacaoModel>();

	@Autowired
	private XHYUsuarioIncService usuarioService;

	private UsuarioIncModel usuario;

	@Autowired
	private XHYAdmNotificacaoEndPoint admworkflownotificacao;

	@Autowired
	private XHYAcoesOsEndPoint acoes;

	@Autowired
	private XHYAdmLogEndPoint admLog;

	@Autowired
	private Authorization authorization;

	private List<RsNotiManual> objRsNotiManual;

	private List<RsNotiManual> selectedEmail;

	private String txtOcorEspec;
	private String strNrOS;
	private String strNrNotiAnt;
	private String strDestinatario;
	private String strNrSeqOs;
	private String txtMesg;
	private String txtHdAction;
	private String strNumOS;

	private String cboTpNotificacao = "5020";
	private String txtPrazRet = "01/01/1900";
	private String txtHdAuxAnexo;
	private String txtHdOS;
	private String txtHdDest;
	private String txtHdRetEmail;

	// hyb_adm_workflow_notificacao_pesq_email
	private String txtNmFunc;
	private String txtNmEmail;

	private String txtArrAnexo = "10|20|30";

	/* TODO Ver o path correto para salvar arquivos */
	// private final String strCaminhoServidor = "C:\\backlevel\\anexos\\";
	private final String strCaminhoServidor = System.getProperty("user.dir");

	private UploadedFile arquivoUpload;

	private String strNmArqAnex;

	@PostConstruct
	public void init() {
		instanciarUsuario();
	}

	public void validaOS() {

		String retorno = "";
		try {

			retorno = admworkflownotificacao.consultarNotificacaoOs(this.getTxtOcorEspec(),
					Long.parseLong(usuario.getCdAreaUsuario()));

			JSONObject validaOS = new JSONObject(retorno);
			JSONArray pcursor = validaOS.getJSONArray("PCURSOR");

			if (pcursor.length() == 0) {
				RequestContext.getCurrentInstance().execute(
						"document.getElementById('frm:txtMesg').disabled = true;document.getElementById('frm:txtMesg').style.backgroundColor = \"#eee\";");
				RequestContext.getCurrentInstance()
						.execute("alert('Ordem de Serviço năo Encontrada. Favor digitar uma OS valida !!');");
			} else {
				for (int i = 0; i < pcursor.length(); i++) {
					JSONObject f = pcursor.getJSONObject(i);

					this.setStrNrOS(f.isNull("NR_SEQU_ORDE_SERV") == true ? "" : f.get("NR_SEQU_ORDE_SERV").toString());
					this.setStrNrNotiAnt(f.isNull("CD_AREA") == true ? "" : f.get("CD_AREA").toString());

				}

				RequestContext.getCurrentInstance().execute(
						"document.getElementById('frm:txtMesg').disabled = false;document.getElementById('frm:txtMesg').style.backgroundColor = \"white\";");
				RequestContext.getCurrentInstance().execute("document.getElementById('frm:txtOcorEspec').readOnly = true;");
				RequestContext.getCurrentInstance().update("frm:txthdPesqNrNoti");

			}

		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}

	}

	public AdmWorkflowNotificacaoManualBean() {
		this.objRsNotiManual = new ArrayList<RsNotiManual>();
	}

	public void openDialog() {

		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('hyb_adm_workflow_notificacao_pesq_email').show();");

		pesqEmail();

	}

	public void openDialogAnexo() {

		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('hyb_uploadnotificacao').show();");
	}

	public void pesqEmail() {

		// String txthdNmFunc =
		// FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("txthdNmFunc");

		if (this.getTxtNmFunc() == null) {
			this.setTxtNmFunc("");
		}

		String retorno = "";

		try {
			retorno = admworkflownotificacao.consultarEmailFunc(this.getTxtNmFunc());
		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		JSONObject jsonRetorno = new JSONObject(retorno);
		JSONArray pcursor = jsonRetorno.getJSONArray("PCURSOR");

		for (int i = 0; i < pcursor.length(); i++) {
			JSONObject curr = pcursor.getJSONObject(i);
			RsNotiManual notimanual = new RsNotiManual();
			notimanual.setNM_EMAIL_RECU(curr.getString("NM_EMAIL_RECU"));
			notimanual.setNM_RECU_OCOR_ESPC(curr.getString("NM_RECU_OCOR_ESPC"));
			objRsNotiManual.add(notimanual);
		}

	}

	public void deletar() {
		objUpload.clear();
		RequestContext.getCurrentInstance().update("frm:anexoUpload");
	}

	public List<RsNotiManual> getObjRsNotiManual() {
		return objRsNotiManual;
	}

	public void instanciarUsuario() {
		try {
			usuario = usuarioService.getDadosUsuario();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void formSubmit(FileUploadEvent event) {
		// objUpload = new ArrayList<UploadNotificacaoModel>();

		/* Salvando arquivo no path */
		try {

			arquivoUpload = event.getFile();

			if (arquivoUpload == null)
				throw new Exception("Arquivo nulo");

			if (arquivoUpload.getSize() > 1024000) {
				RequestContext.getCurrentInstance().execute(
						"alert('O arquivo enviado ultrapassa o limite de 1MB permitido para envio de arquivos do Portal Corporativo, favor verificar!')");
				throw new Exception(
						"O arquivo enviado ultrapassa o limite de 1MB permitido para envio de arquivos do Portal Corporativo, favor verificar!");
			}

			this.strNmArqAnex = this.arquivoUpload.getFileName();

			File folder = new File(strCaminhoServidor, "anexos");
			File file = new File(folder, arquivoUpload.getFileName());

			if (!folder.exists()) {
				folder.mkdir();
			}

			OutputStream out = new FileOutputStream(file);
			out.write(arquivoUpload.getContents());
			out.close();

			objUpload.add(new UploadNotificacaoModel(arquivoUpload.getFileName(), strCaminhoServidor));

//			System.out.println("O arquivo '" + arquivoUpload.getFileName() + "' foi salvo com sucesso no path '"
//					+ this.strCaminhoServidor + "'"); // apenas para debug

			/* TODO Chamada ao backend, obter novo path, strCaminhoMudado */

			RequestContext.getCurrentInstance().update("frm:anexoUpload");
			RequestContext.getCurrentInstance().execute(
					"document.getElementById('frm:txtHdAuxAnexo').value = '" + this.arquivoUpload.getFileName() + "'");
			RequestContext.getCurrentInstance().execute("alert('O arquivo foi salvo com sucesso!')");
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('hyb_uploadnotificacao').hide();");

		} catch (Exception e) {
			/* TODO Tratamento de erro */
			//System.out.println("Aconteceu o seguinte erro no Upload de arquivos: " + e.getMessage());
		}
	}

	public void Incluir() throws ParseException, DatatypeConfigurationException, InterruptedException, IOException {

		DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date date = format.parse("1900-01-01 00:00:00");

		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);

		XMLGregorianCalendar xmlGregCal = null;

		try {
			xmlGregCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
//			System.out.println("data: " + xmlGregCal);
		} catch (DatatypeConfigurationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		String strRetNrSeqNot = "0";
		// System.out.println(txtMesg + " : " + xmlGregCal + " : 5020 : " + strNrOS + "
		// : " + txtNmEmail + " : " + txtArrAnexo + " : 0 : email@email.com :
		// 26021551877" );

		try {
			strRetNrSeqNot = admworkflownotificacao.incluirNotificacaoManual(txtMesg, xmlGregCal, 5020, txtHdOS,
					txtNmEmail, txtArrAnexo, 0, usuario.getNmEmailusuario(), usuario.getCpfUsuario());

			JSONObject objResult = new JSONObject(strRetNrSeqNot);

			strRetNrSeqNot = objResult.get("PCURSOR").toString();


			// JSONObject temp = pcursorResult.getJSONObject();

			// System.out.println(temp);

		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// System.out.println("result 001: " + strRetNrSeqNot);

		String result = "";

		// System.out.println("result 002: " + strNrOS + "," + "E" + "," + "5020" + ","
		// + "" + "," + "" + "," + false);

		try {
			result = acoes.incluirProxWorkFlow(txtHdOS, "E", "5020", "", "", false);
		} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYAcoesOs.WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		Thread.sleep(1000);

		try {
			result = acoes.incluirProxWorkFlow(txtHdOS, "E", "5020", "", "", false);
		} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYAcoesOs.WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			String enviouEmail = admworkflownotificacao.enviarEmail(Long.parseLong(strRetNrSeqNot),
					usuario.getNmEmailusuario(), txtNmEmail, "", "5020", txtMesg);
			admLog.incluirAcaoLog(txtHdOS, Integer.toString(LogIncUtil.ENVIAR_NOTIFICACAO_MANUAL),
					usuario.getCpfUsuario(), "Notificação Manual Cadastrada - E-mail enviado");
			RequestContext.getCurrentInstance().execute("alert('E-mail enviado com sucesso!')");

			FacesContext context = FacesContext.getCurrentInstance();
			context.getExternalContext().getSessionMap().remove("AdmWorkflowNotificacaoManualBean");
			FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_adm_workflow_notificacao_manual.xhtml");

		} catch (WebServiceException | com.altec.bsbr.app.jab.hyb.webclient.XHYAdmLog.WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public String getTxtOcorEspec() {
		return txtOcorEspec;
	}

	public void setTxtOcorEspec(String txtOcorEspec) {
		this.txtOcorEspec = txtOcorEspec;
	}

	public String getStrNrOS() {
		return strNrOS;
	}

	public void setStrNrOS(String strNrOS) {
		this.strNrOS = strNrOS;
	}

	public String getStrNrNotiAnt() {
		return strNrNotiAnt;
	}

	public void setStrNrNotiAnt(String strNrNotiAnt) {
		this.strNrNotiAnt = strNrNotiAnt;
	}

	public String getTxtNmFunc() {
		return txtNmFunc;
	}

	public void setTxtNmFunc(String txtNmFunc) {
		this.txtNmFunc = txtNmFunc;
	}

	public String getTxtNmEmail() {
		return txtNmEmail;
	}

	public void setTxtNmEmail(String txtNmEmail) {
		this.txtNmEmail = txtNmEmail;
	}

	public String getStrDestinatario() {
		return strDestinatario;
	}

	public void setStrDestinatario(String strDestinatario) {
		this.strDestinatario = strDestinatario;
	}

	public String getStrNrSeqOs() {
		return strNrSeqOs;
	}

	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}

	public String getCboTpNotificacao() {
		return cboTpNotificacao;
	}

	public void setCboTpNotificacao(String cboTpNotificacao) {
		this.cboTpNotificacao = cboTpNotificacao;
	}

	public List<RsNotiManual> getSelectedEmail() {
		return selectedEmail;
	}

	public void setSelectedEmail(List<RsNotiManual> selectedEmails) {
		this.selectedEmail = selectedEmails;
	}

	public String getTxtMesg() {
		return txtMesg;
	}

	public void setTxtMesg(String txtMesg) {
		this.txtMesg = txtMesg;
	}

	public String getTxtHdAction() {
		return txtHdAction;
	}

	public void setTxtHdAction(String txtHdAction) {
		this.txtHdAction = txtHdAction;
	}

	public String getTxtHdAuxAnexo() {
		return txtHdAuxAnexo;
	}

	public void setTxtHdAuxAnexo(String txtHdAuxAnexo) {
		this.txtHdAuxAnexo = txtHdAuxAnexo;
	}

	public String getTxtHdOS() {
		return txtHdOS;
	}

	public void setTxtHdOS(String txtHdOS) {
		this.txtHdOS = txtHdOS;
	}

	public String getTxtHdDest() {
		return txtHdDest;
	}

	public void setTxtHdDest(String txtHdDest) {
		this.txtHdDest = txtHdDest;
	}

	public String getTxtHdRetEmail() {
		return txtHdRetEmail;
	}

	public void setTxtHdRetEmail(String txtHdRetEmail) {
		this.txtHdRetEmail = txtHdRetEmail;
	}

	public String getStrNmArqAnex() {
		return strNmArqAnex;
	}

	public void setStrNmArqAnex(String strNmArqAnex) {
		this.strNmArqAnex = strNmArqAnex;
	}

	public List<UploadNotificacaoModel> getObjUpload() {
		return objUpload;
	}

	public void setObjUpload(List<UploadNotificacaoModel> objUpload) {
		this.objUpload = objUpload;
	}

	public String getStrNumOS() {
		return strNumOS;
	}

	public void setStrNumOS(String strNumOS) {
		this.strNumOS = strNumOS;
	}

	public String getTxtPrazRet() {
		return txtPrazRet;
	}

	public void setTxtPrazRet(String txtPrazRet) {
		this.txtPrazRet = txtPrazRet;
	}

	public String getTxtArrAnexo() {
		return txtArrAnexo;
	}

	public void setTxtArrAnexo(String txtArrAnexo) {
		this.txtArrAnexo = txtArrAnexo;
	}

}
