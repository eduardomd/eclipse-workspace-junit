package com.altec.bsbr.app.hyb.web.jsf;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;

import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.EncerrarOsModel;
import com.altec.bsbr.app.hyb.dto.EncerrarOsMotiPenaModel;
import com.altec.bsbr.app.hyb.dto.EncerrarOsParticipantesModel;
import com.altec.bsbr.app.hyb.dto.EncerrarOsPenaModel;
import com.altec.bsbr.app.hyb.dto.ObjRsEquipe;
import com.altec.bsbr.app.hyb.dto.UsuarioIncModel;
import com.altec.bsbr.app.hyb.web.util.XHYUsuarioIncService;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAcoesOs.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAcoesOs.XHYAcoesOsEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmLog.XHYAdmLogEndPoint;

@Component("encerrarOSBean")
@Scope("session")
public class EncerrarOSBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String strNrSeqOs;
	private String retorno;
	private String cpfUsuario = "";
	private String strReadOnly;
	private String strTitulo;

	@Autowired 
	private XHYUsuarioIncService userServ;

	public void verificarPermissao(ComponentSystemEvent event) {
		String[] perfis = {"SUPERINT","GER","COORD","ANALISTA","PREST_SERV","ESTAG","SECRET","ADM"};
		
		userServ.verificarPermissao(perfis);
	}

	public EncerrarOsMotiPenaModel getObjRsOsMotiPenaModel() {
		return objRsOsMotiPenaModel;
	}

	public void setObjRsOsMotiPenaModel(EncerrarOsMotiPenaModel objRsOsMotiPenaModel) {
		this.objRsOsMotiPenaModel = objRsOsMotiPenaModel;
	}

	public List<EncerrarOsMotiPenaModel> getObjRsOsMotiPena() {
		return objRsOsMotiPena;
	}

	public void setObjRsOsMotiPena(List<EncerrarOsMotiPenaModel> objRsOsMotiPena) {
		this.objRsOsMotiPena = objRsOsMotiPena;
	}

	private EncerrarOsModel objRsOsModel;

	private EncerrarOsParticipantesModel objRsOsParticipantesModel;
	private List<EncerrarOsParticipantesModel> objRsOsParticipantes;

	private EncerrarOsPenaModel objRsOsPenaModel;
	private List<EncerrarOsPenaModel> objRsOsPena;
	
	private EncerrarOsMotiPenaModel objRsOsMotiPenaModel;
	private List<EncerrarOsMotiPenaModel> objRsOsMotiPena;
	

	@Autowired
    private XHYAcoesOsEndPoint acoesOS;
	@Autowired
    private  XHYAdmLogEndPoint admLog;
	@Autowired
	private XHYUsuarioIncService user;

	@PostConstruct
	public void init() {
		//TODO Checar permissão
		carregarPagina();
	}

	public void carregarPagina() {
		strNrSeqOs = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs");
		System.out.println("INIT!!");
		objRsOsModel = new EncerrarOsModel();
		
		getObjRsOs();
		listarParticipantes();
		listarPenalidades();
		listarMotiPena();
		
		if (user != null) {			
			try {
				UsuarioIncModel dadosUser = user.getDadosUsuario();
				setCpfUsuario(dadosUser.getCpfUsuario());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}	
	}

	public void getObjRsOs() {
		try {
	        retorno = acoesOS.consultarOS(strNrSeqOs);
	        JSONObject rstemp = new JSONObject(retorno);
	        JSONArray pcursor = rstemp.getJSONArray("PCURSOR");
	        JSONObject f = pcursor.getJSONObject(0);

	        objRsOsModel.setCD_NOTI(f.isNull("CD_NOTI") ? 0 : f.getInt("CD_NOTI"));
	        objRsOsModel.setCD_SITU(f.isNull("CD_SITU") ? "" : f.getString("CD_SITU"));
			objRsOsModel.setNM_NOTI(f.isNull("NM_NOTI") ? "" : f.getString("NM_NOTI"));
			objRsOsModel.setSITUACAO(f.isNull("SITUACAO") ? "" : f.getString("SITUACAO"));
			objRsOsModel.setTX_ABERTURA(f.isNull("TX_ABERTURA") ? "" : f.getString("TX_ABERTURA"));
			objRsOsModel.setTX_ENCERRAMENTO(f.isNull("TX_ENCERRAMENTO") ? "" : f.getString("TX_ENCERRAMENTO"));
			objRsOsModel.setTX_FALHAS(f.isNull("TX_FALHAS") ? "" : f.getString("TX_FALHAS"));
			objRsOsModel.setTX_OUTRAS_PROP(f.isNull("TX_OUTRAS_PROP") ? "" : f.getString("TX_OUTRAS_PROP"));
			objRsOsModel.setPARECER_JURI(f.isNull("PARECER_JURI") ? "" : f.getString("PARECER_JURI"));
			objRsOsModel.setPARECER_GEST(f.isNull("PARECER_GEST") ? "" : f.getString("PARECER_GEST"));
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (Exception ex) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
	}

    public void listarParticipantes() {
    	objRsOsParticipantes = new ArrayList<EncerrarOsParticipantesModel>();
    	
    	try {
    		retorno = acoesOS.consultarParticipanteOs(strNrSeqOs);
	        
	        JSONObject consultarParticipantes = new JSONObject(retorno);
	        JSONArray pcursor = consultarParticipantes.getJSONArray("PCURSOR");
	        System.out.println(retorno);
	        for (int i = 0; i < pcursor.length(); i++) {
	        	JSONObject f = pcursor.getJSONObject(i);
	        	
	        	String cpfCnpj = "";
	        	String newCpfCnpj = "";
			   
	        	if (!f.isNull("CPF_CNPJ")) {
	        		cpfCnpj = f.get("CPF_CNPJ").toString();
	        		if (f.get("TIPO_PARTICIP").toString().equals("3"))
	        			newCpfCnpj = cpfCnpj.substring(0, 2)+"."+cpfCnpj.substring(2, 5)+"."+cpfCnpj.substring(5, 8)+"/"+cpfCnpj.substring(8, 12)+"-"+cpfCnpj.substring(12, 14);
	        		else {
	        			if (f.get("TIPO_PARTICIP").toString().equals("1") || f.get("TIPO_PARTICIP").toString().equals("2"))
	        				newCpfCnpj = cpfCnpj.substring(0, 3)+"."+cpfCnpj.substring(3, 6)+"."+cpfCnpj.substring(6, 9)+"-"+cpfCnpj.substring(9);
	        		}
	        	}	        	
	        	
	        	objRsOsParticipantesModel = new EncerrarOsParticipantesModel();
	        	objRsOsParticipantesModel.setCODIGO(f.isNull("CODIGO") ? 0 : f.getInt("CODIGO"));
	        	objRsOsParticipantesModel.setPOSICAO(f.isNull("POSICAO") ? 0 : f.getInt("POSICAO"));
	        	objRsOsParticipantesModel.setNOME(f.isNull("NOME") ? "" : f.getString("NOME"));
	        	objRsOsParticipantesModel.setTIPO_PARTICIP(f.isNull("TIPO_PARTICIP") ? 0 : f.getInt("TIPO_PARTICIP"));
	        	objRsOsParticipantesModel.setCPF_CNPJ(newCpfCnpj);
	        	objRsOsParticipantesModel.setDESC_TIPO(f.isNull("DESC_TIPO") ? "" : f.getString("DESC_TIPO"));
	        	objRsOsParticipantesModel.setCD_PENALIDADE(f.isNull("CD_PENALIDADE") ? "" : f.get("CD_PENALIDADE").toString());
	        	objRsOsParticipantesModel.setCD_MOTIVO(f.isNull("CD_MOTIVO") ? "" : f.get("CD_MOTIVO").toString());
	            
	        	objRsOsParticipantes.add(objRsOsParticipantesModel); 
	        }
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (Exception ex) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
    }
    
    public void listarPenalidades() {
    	objRsOsPena = new ArrayList<EncerrarOsPenaModel>();
    	
    	try {
    		retorno = acoesOS.consultarPenalidade();
	        
	        JSONObject consultarParticipantes = new JSONObject(retorno);
	        JSONArray pcursor = consultarParticipantes.getJSONArray("PCURSOR");
	        for (int i = 0; i < pcursor.length(); i++) {
	        	JSONObject f = pcursor.getJSONObject(i);	        	
	        	
	        	objRsOsPenaModel = new EncerrarOsPenaModel();
	        	objRsOsPenaModel.setCODIGO(f.isNull("CODIGO") ? 0 : f.getInt("CODIGO"));
	        	objRsOsPenaModel.setNOME(f.isNull("NOME") ? "" : f.getString("NOME"));
	        	
	        	objRsOsPena.add(objRsOsPenaModel); 
	        }
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (Exception ex) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
    }
    
    public void listarMotiPena() {
    	objRsOsMotiPena = new ArrayList<EncerrarOsMotiPenaModel>();
    	
    	try {
    		retorno = acoesOS.consultarMotiPenalidade();
	        
	        JSONObject consultarParticipantes = new JSONObject(retorno);
	        JSONArray pcursor = consultarParticipantes.getJSONArray("PCURSOR");
	        for (int i = 0; i < pcursor.length(); i++) {
	        	JSONObject f = pcursor.getJSONObject(i);	        	
	        	
	        	objRsOsMotiPenaModel = new EncerrarOsMotiPenaModel();
	        	objRsOsMotiPenaModel.setCODIGO(f.isNull("CODIGO") ? 0 : f.getInt("CODIGO"));
	        	objRsOsMotiPenaModel.setNOME(f.isNull("NOME") ? "" : f.getString("NOME"));
	        	
	        	objRsOsMotiPena.add(objRsOsMotiPenaModel); 
	        }
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (Exception ex) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
    }

	public void salvar() {		
		try {
			String strFalhas = objRsOsModel.getTX_FALHAS();
			String strOutrasProp = objRsOsModel.getTX_OUTRAS_PROP();
			String strParecerGestor = objRsOsModel.getPARECER_GEST();
			String strTxAberOS = objRsOsModel.getTX_ABERTURA();
			String strTxEnceOS = objRsOsModel.getTX_ENCERRAMENTO();
			String strParecer = objRsOsModel.getPARECER_JURI();
			String strNrArquivo = "";
			String strNmPastaLocal = "";
			String strDtArquivLocal = "";
			String strNmRespArquiv = "";
			String strDtPrazoPrescLocal = "";
			String txtNrCaixa = "";
			String strNmPastaGeral = "";
			String strDtArquivGeral = "";
			String strEmprResp = "";
			String strPrazoPrescGeral = "";
			String strParecerJuridico = "";
			String strDtRetJuridico = "";
			String strDtEfetProc = "";
			String strCdMatrDeAcordo = "";
			String strNmRespDeAcordo = "";
			
			acoesOS.salvarEncerramentoOs(strNrSeqOs, strFalhas, strOutrasProp, strParecerGestor, strTxAberOS, strTxEnceOS, strParecer, strNrArquivo, strNmPastaLocal, strDtArquivLocal, strNmRespArquiv, strDtPrazoPrescLocal, txtNrCaixa, strNmPastaGeral, strDtArquivGeral, strEmprResp, strPrazoPrescGeral, strParecerJuridico, strDtRetJuridico, strDtEfetProc, strCdMatrDeAcordo, strNmRespDeAcordo);

			for( int i = 0; i < objRsOsParticipantes.size(); i++ ) {				
				String strCdPart = String.valueOf(objRsOsParticipantes.get(i).getCODIGO());
				String strPena = String.valueOf(objRsOsParticipantes.get(i).getCD_PENALIDADE());
				String strMoti = String.valueOf(objRsOsParticipantes.get(i).getCD_MOTIVO());
				
				acoesOS.salvarPenaEnvolvidosOs(strCdPart, strPena, strMoti);
			} 
			
			RequestContext.getCurrentInstance().execute("alert('Dados salvos com sucesso!')");

		} catch (WebServiceException e) {
			e.printStackTrace();
		}
	}

	public void encerrar() {
		try {
			acoesOS.incluirProxWorkFlow(strNrSeqOs, "E", "3020", "TE", "", false);

			acoesOS.incluirProxWorkFlow(strNrSeqOs, "E", "5010", "", "", false);

			RequestContext.getCurrentInstance().execute("alert('A Ordem de Serviço foi encerrada com sucesso!\\nA notificação foi enviada aos responsáveis!')");

		} catch (WebServiceException e) {
			e.printStackTrace();
		}
	}

	public boolean verificaTipoDesignacao(ObjRsEquipe equipe) {
		if (equipe.getTP_DESI() == "AD" || equipe.getTP_DESI() == "AA" || equipe.getTP_DESI() == "PSD"
				|| equipe.getTP_DESI() == "PSA" || equipe.getTP_DESI() == "NA") {
			return true;
		}
		return false;
	}

	public String getStrNrSeqOs() {
		return strNrSeqOs;
	}
	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}
	
	public String getCpfUsuario() {
		return cpfUsuario;
	}
	public void setCpfUsuario(String cpfUsuario) {
		this.cpfUsuario = cpfUsuario;
	}

	public String getStrReadOnly() {
		return strReadOnly;
	}

	public void setStrReadOnly(String strReadOnly) {
		this.strReadOnly = strReadOnly;
	}

	public String getStrTitulo() {
		return strTitulo;
	}

	public void setStrTitulo(String strTitulo) {
		this.strTitulo = strTitulo;
	}

	public EncerrarOsModel getObjRsOsModel() {
		return objRsOsModel;
	}
	public void setObjRsOsModel(EncerrarOsModel objRsOsModel) {
		this.objRsOsModel = objRsOsModel;
	}

	public EncerrarOsParticipantesModel getObjRsOsParticipantesModel() {
		return objRsOsParticipantesModel;
	}
	public void setObjRsOsParticipantesModel(EncerrarOsParticipantesModel objRsOsParticipantesModel) {
		this.objRsOsParticipantesModel = objRsOsParticipantesModel;
	}
	
	public List<EncerrarOsParticipantesModel> getObjRsOsParticipantes() {
		return objRsOsParticipantes;
	}
	public void setObjRsOsParticipantes(List<EncerrarOsParticipantesModel> objRsOsParticipantes) {
		this.objRsOsParticipantes = objRsOsParticipantes;
	}

	public EncerrarOsPenaModel getObjRsOsPenaModel() {
		return objRsOsPenaModel;
	}

	public void setObjRsOsPenaModel(EncerrarOsPenaModel objRsOsPenaModel) {
		this.objRsOsPenaModel = objRsOsPenaModel;
	}

	public List<EncerrarOsPenaModel> getObjRsOsPena() {
		return objRsOsPena;
	}

	public void setObjRsOsPena(List<EncerrarOsPenaModel> objRsOsPena) {
		this.objRsOsPena = objRsOsPena;
	}

}
