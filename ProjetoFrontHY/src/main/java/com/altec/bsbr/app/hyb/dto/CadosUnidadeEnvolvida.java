package com.altec.bsbr.app.hyb.dto;


public class CadosUnidadeEnvolvida {
	
	private String strNrSeqOs;
	private String strErro;
	private int intTpUor;
	//ClsCadOsUnidEnvolv objCadOsUnidEnvolv;
	//dim objRs
	
	public CadosUnidadeEnvolvida(){}
	
	public CadosUnidadeEnvolvida(String strNrSeqOs,String strErro,int intTpOur){
		this.strNrSeqOs = strNrSeqOs;
		this.strErro = strErro;
		this.intTpUor = intTpOur;
		
	}


	public String getStrNrSeqOs() {
		return strNrSeqOs;
	}


	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}


	public String getStrErro() {
		return strErro;
	}


	public void setStrErro(String strErro) {
		this.strErro = strErro;
	}


	public int getIntTpUor() {
		return intTpUor;
	}


	public void setIntTpUor(int intTpUor) {
		this.intTpUor = intTpUor;
	}
	
	
	

}
