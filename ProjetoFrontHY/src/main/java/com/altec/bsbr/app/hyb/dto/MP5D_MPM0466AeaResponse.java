package com.altec.bsbr.app.hyb.dto;

public class MP5D_MPM0466AeaResponse {

	public MP5D_MPM0466AeaResponse() {
		
	}

	public MP5D_MPM0466AeaResponse(String dC_FORMATO, String fECALTA, String fECBAJA) {
		super();
		DC_FORMATO = dC_FORMATO;
		FECALTA = fECALTA;
		FECBAJA = fECBAJA;
	}

	private String DC_FORMATO;

	private String FECALTA;
	
	private String FECBAJA;
	
	public String getDC_FORMATO() {
		return DC_FORMATO;
	}
	
	public void setDC_FORMATO(String dC_FORMATO) {
		DC_FORMATO = dC_FORMATO;
	}

	public String getFECALTA() {
		return FECALTA;
	}

	public void setFECALTA(String fECALTA) {
		FECALTA = fECALTA;
	}

	public String getFECBAJA() {
		return FECBAJA;
	}

	public void setFECBAJA(String fECBAJA) {
		FECBAJA = fECBAJA;
	}
}
