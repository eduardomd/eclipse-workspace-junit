package com.altec.bsbr.app.hyb.dto;

public class Hyb_cados_dados_contabeis_exe_model_objRst {
	private String CD_MANI;
	
	public Hyb_cados_dados_contabeis_exe_model_objRst() {
		
	}

	public String getCD_MANI() {
		return CD_MANI;
	}

	public void setCD_MANI(String cD_MANI) {
		CD_MANI = cD_MANI;
	}
	
}
