package com.altec.bsbr.app.hyb.dto;

public class CadastroosDetalheRiscopot {
	private String strOs;
	private String strNomeProd;
	
	public String getStrOs() {
		return strOs;
	}

	public void setStrOs(String strOs) {
		this.strOs = strOs;
	}

	public String getStrNomeProd() {
		return strNomeProd;
	}

	public void setStrNomeProd(String strNomeProd) {
		this.strNomeProd = strNomeProd;
	}
	
}

