package com.altec.bsbr.app.hyb.dto;

public class AdmWorkflowNotificacaoManualModel {

	private String TP_NOTI;
	private String NR_NOTI;
	private String NM_NOTI;
	
	public AdmWorkflowNotificacaoManualModel() {
		
	}
	
	public AdmWorkflowNotificacaoManualModel(String tP_NOTI, String nR_NOTI, String nM_NOTI) {
		super();
		TP_NOTI = tP_NOTI;
		NR_NOTI = nR_NOTI;
		NM_NOTI = nM_NOTI;
	}
	
	public String getTP_NOTI() {
		return TP_NOTI;
	}
	
	public void setTP_NOTI(String tP_NOTI) {
		TP_NOTI = tP_NOTI;
	}
	
	public String getNR_NOTI() {
		return NR_NOTI;
	}
	
	public void setNR_NOTI(String nR_NOTI) {
		NR_NOTI = nR_NOTI;
	}
	
	public String getNM_NOTI() {
		return NM_NOTI;
	}
	
	public void setNM_NOTI(String nM_NOTI) {
		NM_NOTI = nM_NOTI;
	}
	
	
	
}
