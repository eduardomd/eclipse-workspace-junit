package com.altec.bsbr.app.hyb.dto;

public class ObjRsCausaRaizOs {
	private String CODIGO;
	
	public ObjRsCausaRaizOs() {}

	public ObjRsCausaRaizOs(String cODIGO) {
		super();
		CODIGO = cODIGO;
	}

	public String getCODIGO() {
		return CODIGO;
	}

	public void setCODIGO(String cODIGO) {
		CODIGO = cODIGO;
	}
	
}
