package com.altec.bsbr.app.hyb.web.jsf;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;
import org.primefaces.model.UploadedFile;


@ManagedBean(name = "AnexarArquivoIframeBean")
@ViewScoped
public class AnexarArquivoIframeBean implements Serializable { 
	
	private static final long serialVersionUID = 1L;

	/* TODO Ver o path correto para salvar arquivos */
	private final String strCaminhoServidor = "C:\\backlevel\\anexos\\";

	private UploadedFile arquivoUpload;
	
	private String strNmArqAnex;
	private String strNmArquivo;
	private String strCaminho;
	private String strFlg;
	
	
	public void formSubmit() {
		/* Salvando arquivo no path */
		try {
			if (arquivoUpload == null)
				throw new Exception("Arquivo nulo");
			
			this.strNmArqAnex = this.arquivoUpload.getFileName();
			
			File file = new File(strCaminhoServidor, arquivoUpload.getFileName());

			OutputStream out = new FileOutputStream(file);
			out.write(arquivoUpload.getContents());
			out.close();

			System.out.println("O arquivo '"+arquivoUpload.getFileName()+"' foi salvo com sucesso no path '"+this.strCaminhoServidor+"'"); //apenas para debug
		} catch (Exception e) {
			/* TODO Tratamento de erro */
			e.printStackTrace();
		}
		
		/* TODO Chamada ao backend */
		
		strNmArqAnex = strNmArqAnex + strCaminho + "|";
		
	    strNmArquivo = strNmArquivo + arquivoUpload.getFileName() + "|";
		
		RequestContext.getCurrentInstance().execute("alert('Arquivo anexado com sucesso!')");
	}		
	

	public String getStrCaminho() {
		return strCaminho;
	}

	public void setStrCaminho(String strCaminho) {
		this.strCaminho = strCaminho;
	}

	public String getStrFlg() {
		return strFlg;
	}

	public void setStrFlg(String strFlg) {
		this.strFlg = strFlg;
	}

	public UploadedFile getArquivoUpload() {
		return arquivoUpload;
	}

	public void setArquivoUpload(UploadedFile arquivoUpload) {
		this.arquivoUpload = arquivoUpload;
	}

	public String getStrNmArqAnex() {
		return strNmArqAnex;
	}

	public void setStrNmArqAnex(String strNmArqAnex) {
		this.strNmArqAnex = strNmArqAnex;
	}
	
}