package com.altec.bsbr.app.hyb.dto;

import java.util.Date;

public class AdmLogModel {
	
	private String[] acaoLog; 
	private String[] acaoUsua; 
	
	public String[] ConsultarAcaoLog(Date dteInicio, Date dteFim, String strCodUsua, int intCodAcao, String strErro) {
		this.acaoLog = new String[2];
		
		this.acaoLog[0] = "Ação Log 01";
		this.acaoLog[1] = "Ação Log 02";
        
		return this.acaoLog;
	}
		
	public String[] ConsultarAcaoUsua(String strErro) {
		this.acaoUsua = new String[4];
		
		this.acaoUsua[0] = "Açao Usuario 01";
		this.acaoUsua[1] = "Açao Usuario 02";
		this.acaoUsua[2] = "Açao Usuario 03";
		this.acaoUsua[3] = "Açao Usuario 04";
        
		return this.acaoUsua;
	}
}
