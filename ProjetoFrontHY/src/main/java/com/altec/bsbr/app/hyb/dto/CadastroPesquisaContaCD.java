package com.altec.bsbr.app.hyb.dto;

public class CadastroPesquisaContaCD {
	private String NRCARTAO;
	private String TITULAR;
	private String CPFCNPJ;
	private String NRCONTA;
	private String TIPO;
	
	public String getNRCARTAO() {
		return NRCARTAO;
	}
	public void setNRCARTAO(String nRCARTAO) {
		NRCARTAO = nRCARTAO;
	}
	public String getTITULAR() {
		return TITULAR;
	}
	public void setTITULAR(String tITULAR) {
		TITULAR = tITULAR;
	}
	public String getCPFCNPJ() {
		return CPFCNPJ;
	}
	public void setCPFCNPJ(String cPFCNPJ) {
		CPFCNPJ = cPFCNPJ;
	}
	public String getNRCONTA() {
		return NRCONTA;
	}
	public void setNRCONTA(String nRCONTA) {
		NRCONTA = nRCONTA;
	}
	public String getTIPO() {
		return TIPO;
	}
	public void setTIPO(String tIPO) {
		TIPO = tIPO;
	}
	
}
