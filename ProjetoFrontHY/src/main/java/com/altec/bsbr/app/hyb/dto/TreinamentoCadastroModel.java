package com.altec.bsbr.app.hyb.dto;

public class TreinamentoCadastroModel{ 
 
private String cboLocal;
private String txtHdAction;
private String txtHdParamNmArquivo;
public TreinamentoCadastroModel(){ 
 } 
 
public String getCboLoCal() { 
   return cboLocal; 
} 
 
public void setCboLoCal(String cboLocal) { 
   this.cboLocal = cboLocal; 
}
 
public String getTxTHdAcTion() { 
   return txtHdAction; 
} 
 
public void setTxTHdAcTion(String txtHdAction) { 
   this.txtHdAction = txtHdAction; 
}
 
public String getTxTHdParamNmArquivo() { 
   return txtHdParamNmArquivo; 
} 
 
public void setTxTHdParamNmArquivo(String txtHdParamNmArquivo) { 
   this.txtHdParamNmArquivo = txtHdParamNmArquivo; 
}
 
} 
 
