package com.altec.bsbr.app.hyb.dto;

public class PE04_PEM2422AeaResponse {
	
	public PE04_PEM2422AeaResponse() {}

	public PE04_PEM2422AeaResponse(String dC_FORMATO, String tIPDOM, String pEOBSE1, String pEOBSE2, String pEHSTAM,
			String pENOMBL, String pEDESCO, String pECODPO, String pEDESLO, String cODPRV, String pETIPNU) {
		super();
		DC_FORMATO = dC_FORMATO;
		TIPDOM = tIPDOM;
		PEOBSE1 = pEOBSE1;
		PEOBSE2 = pEOBSE2;
		PEHSTAM = pEHSTAM;
		PENOMBL = pENOMBL;
		PEDESCO = pEDESCO;
		PECODPO = pECODPO;
		PEDESLO = pEDESLO;
		CODPRV = cODPRV;
		PETIPNU = pETIPNU;
	}

	private String DC_FORMATO;

	private String TIPDOM;
	
	private String PEOBSE1;
	
	private String PEOBSE2;
	
	private String PEHSTAM;
	
	private String PENOMBL;
	
	private String PEDESCO;
	
	private String PECODPO;
	
	private String PEDESLO;
	
	private String CODPRV;
	
	private String PETIPNU;
	
	public String getDC_FORMATO() {
		return DC_FORMATO;
	}
	
	public void setDC_FORMATO(String dC_FORMATO) {
		DC_FORMATO = dC_FORMATO;
	}

	public String getTIPDOM() {
		return TIPDOM;
	}

	public void setTIPDOM(String tIPDOM) {
		TIPDOM = tIPDOM;
	}

	public String getPEOBSE1() {
		return PEOBSE1;
	}

	public void setPEOBSE1(String pEOBSE1) {
		PEOBSE1 = pEOBSE1;
	}

	public String getPEOBSE2() {
		return PEOBSE2;
	}

	public void setPEOBSE2(String pEOBSE2) {
		PEOBSE2 = pEOBSE2;
	}

	public String getPEHSTAM() {
		return PEHSTAM;
	}

	public void setPEHSTAM(String pEHSTAM) {
		PEHSTAM = pEHSTAM;
	}

	public String getPENOMBL() {
		return PENOMBL;
	}

	public void setPENOMBL(String pENOMBL) {
		PENOMBL = pENOMBL;
	}

	public String getPEDESCO() {
		return PEDESCO;
	}

	public void setPEDESCO(String pEDESCO) {
		PEDESCO = pEDESCO;
	}

	public String getPECODPO() {
		return PECODPO;
	}

	public void setPECODPO(String pECODPO) {
		PECODPO = pECODPO;
	}

	public String getPEDESLO() {
		return PEDESLO;
	}

	public void setPEDESLO(String pEDESLO) {
		PEDESLO = pEDESLO;
	}

	public String getCODPRV() {
		return CODPRV;
	}

	public void setCODPRV(String cODPRV) {
		CODPRV = cODPRV;
	}
	
	public String getPETIPNU() {
		return PETIPNU;
	}

	public void setPETIPNU(String pETIPNU) {
		PETIPNU = pETIPNU;
	}

}
