package com.altec.bsbr.app.hyb.dto;

public class TipoCapturaModel {

	private String nomeTipoCap;

	public TipoCapturaModel(String nomeTipoCap) {
		this.nomeTipoCap = nomeTipoCap;
	}

	public String getNomeTipoCap() {
		return nomeTipoCap;
	}

	public void setNomeTipoCap(String nomeTipoCap) {
		this.nomeTipoCap = nomeTipoCap;
	}

}
