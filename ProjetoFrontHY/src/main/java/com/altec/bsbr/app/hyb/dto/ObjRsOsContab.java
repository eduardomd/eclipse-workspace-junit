package com.altec.bsbr.app.hyb.dto;

public class ObjRsOsContab {
	private String SITUACAO;
	
	public ObjRsOsContab() {}

	public ObjRsOsContab(String sITUACAO) {
		super();
		SITUACAO = sITUACAO;
	}

	public String getSITUACAO() {
		return SITUACAO;
	}

	public void setSITUACAO(String sITUACAO) {
		SITUACAO = sITUACAO;
	}
	
}
