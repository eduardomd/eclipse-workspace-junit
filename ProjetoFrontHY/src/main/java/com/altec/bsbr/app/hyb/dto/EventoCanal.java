package com.altec.bsbr.app.hyb.dto;

import java.util.List;

public class EventoCanal {
	private int codigoEvento;
	private int codigoArea;
	private boolean existRel;
	
	
	public EventoCanal(int codigoEvento, int codigoArea, boolean existRel ) {
		this.setCodigoEvento(codigoEvento);
		this.setCodigoArea(codigoArea);
		this.setExistRel(existRel);
		
	}

	public int getCodigoEvento() {
		return codigoEvento;
	}

	public void setCodigoEvento(int codigoEvento) {
		this.codigoEvento = codigoEvento;
	}

	public int getCodigoArea() {
		return codigoArea;
	}

	public void setCodigoArea(int codigoArea) {
		this.codigoArea = codigoArea;
	}

	public boolean isExistRel() {
		return existRel;
	}

	public void setExistRel(boolean existRel) {
		this.existRel = existRel;
	}


}
