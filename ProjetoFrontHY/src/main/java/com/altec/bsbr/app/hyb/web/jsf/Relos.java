package com.altec.bsbr.app.hyb.web.jsf;

import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.Relos_ModelPag12;
import com.altec.bsbr.app.hyb.dto.hy_relosModel;
import com.altec.bsbr.fw.web.jsf.BasicBBean;

@Component("Relos")
@Scope("session")
public class Relos extends BasicBBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Object strArea = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
			.get("pArea");
	private Object strTpRel = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
			.get("pTpRel");
	private Object strNmRelat = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
			.get("pNmRel");
	private Object strDtIni = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
			.get("pDtIni");
	private Object strDtFim = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
			.get("pDtFim");
 
	// Simulando banco
	private banco objRelat = new banco();
	private List<hy_relosModel> objRsRelat = objRelat.getObjRsRelat();
	private List<List<hy_relosModel>> listaObjRsRelat = objRelat.getListaObjRsRelat();
	private List<List<hy_relosModel>> listaObjRsRelat11 = objRelat.getListaObjRsRelat11();  
	private List<Relos_ModelPag12> listaObjRsRelat12Model = objRelat.listaObjRsRelat12;

	// Getters e Setters

	public Object getStrArea() {
		return strArea;
	}

	public void setStrArea(Object strArea) {
		this.strArea = strArea;
	}

	public Object getStrTpRel() {
		return strTpRel;
	}

	public void setStrTpRel(Object strTpRel) {
		this.strTpRel = strTpRel;
	}

	public Object getStrNmRelat() {
		return strNmRelat;
	}

	public void setStrNmRelat(Object strNmRelat) {
		this.strNmRelat = strNmRelat;
	}

	public Object getStrDtIni() {
		return strDtIni;
	}

	public void setStrDtIni(Object strDtIni) {
		this.strDtIni = strDtIni;
	}

	public Object getStrDtFim() {
		return strDtFim;
	}

	public void setStrDtFim(Object strDtFim) {
		this.strDtFim = strDtFim;
	}

	public banco getObjRelat() {
		return objRelat;
	}

	public void setObjRelat(banco objRelat) {
		this.objRelat = objRelat;
	}

	public List<hy_relosModel> getObjRsRelat() {
		return objRsRelat;
	}

	public void setObjRsRelat(List<hy_relosModel> objRsRelat) {
		this.objRsRelat = objRsRelat;
	}

	public List<List<hy_relosModel>> getListaObjRsRelat() {
		return listaObjRsRelat;
	}

	public void setListaObjRsRelat(List<List<hy_relosModel>> listaObjRsRelat) {
		this.listaObjRsRelat = listaObjRsRelat;
	}

	public List<List<hy_relosModel>> getListaObjRsRelat11() {
		return listaObjRsRelat11;
	}

	public void setListaObjRsRelat11(List<List<hy_relosModel>> listaObjRsRelat11) {
		this.listaObjRsRelat11 = listaObjRsRelat11;
	}

	public List<Relos_ModelPag12> getListaObjRsRelat12Model() {
		return listaObjRsRelat12Model;
	}

	public void setListaObjRsRelat12Model(List<Relos_ModelPag12> listaObjRsRelat12Model) {
		this.listaObjRsRelat12Model = listaObjRsRelat12Model;
	}

	// Classe simula��o banco
	class banco {

		private List<hy_relosModel> objRsRelat = new ArrayList<>();
		private List<List<hy_relosModel>> listaObjRsRelat = new ArrayList<>();//Primeira lista a ser organizada
		private List<List<hy_relosModel>> listaObjRsRelat11 = new ArrayList<>();//Segunda lista a ser organizada
		private List<Relos_ModelPag12> listaObjRsRelat12 = new ArrayList<>();//Terceira lista a ser organizada

		public banco() {
			// Incrementa lista
			hy_relosModel a = new hy_relosModel("A", 1, 6, "01/01");// Pessoa Fisica
			objRsRelat.add(a);
			hy_relosModel b = new hy_relosModel("A", 1, 6, "01/01");// Pessoa Fisica
			objRsRelat.add(b);
			hy_relosModel c = new hy_relosModel("A", 2, 36, "01/02");// Pessoa Juridica
			objRsRelat.add(c);
			hy_relosModel d = new hy_relosModel("B", 1, 36, "01/02");// Pessoa Fisica
			objRsRelat.add(d);
			hy_relosModel e = new hy_relosModel("B", 2, 36, "01/02");// Pessoa Juridica
			objRsRelat.add(e);
			hy_relosModel f = new hy_relosModel("B", 2, 20, "01/03");// Pessoa Juridica
			objRsRelat.add(f);
			hy_relosModel g = new hy_relosModel("C", 1, 20, "01/03");// Pessoa Fisica
			objRsRelat.add(g);

			organizaListadeListas();
		}

		// -------------------Poss�veis retornos do banco-------------------//

		public void organizaListadeListas() {

			List<hy_relosModel> listAux = new ArrayList<>();

			for (int i = 0; i < this.objRsRelat.size(); i++) {

				if (i == 0) {

					listAux.add(this.objRsRelat.get(i));

				} else if (i == this.objRsRelat.size() - 1) {

					if (this.objRsRelat.get(i).getStrDT().equals(this.objRsRelat.get(i - 1).getStrDT())) {

						listAux.add(this.objRsRelat.get(i));

						this.listaObjRsRelat.add(listAux);

					} else {

						this.listaObjRsRelat.add(listAux);

						listAux = new ArrayList<>();

						listAux.clear();
						listAux.add(this.objRsRelat.get(i));

						this.listaObjRsRelat.add(listAux);

					}

				} else {

					if (this.objRsRelat.get(i).getStrDT().equals(this.objRsRelat.get(i - 1).getStrDT())) {

						listAux.add(this.objRsRelat.get(i));

					} else {

						this.listaObjRsRelat.add(listAux);

						listAux = new ArrayList<>();

						listAux.add(this.objRsRelat.get(i));
					}

				}
 
			}
			
			listAux = new ArrayList<>();

			for (int i = 0; i < this.objRsRelat.size(); i++) {

				if (i == 0) {

					listAux.add(this.objRsRelat.get(i));

				} else if (i == this.objRsRelat.size() - 1) {

					if (this.objRsRelat.get(i).getCD_CNAL() == this.objRsRelat.get(i - 1).getCD_CNAL()) {

						listAux.add(this.objRsRelat.get(i));

						this.listaObjRsRelat11.add(listAux); 

					} else {

						this.listaObjRsRelat11.add(listAux);

						listAux = new ArrayList<>();           

						listAux.clear();
						listAux.add(this.objRsRelat.get(i));

						this.listaObjRsRelat11.add(listAux);

					}

				} else {

					if (this.objRsRelat.get(i).getCD_CNAL() == this.objRsRelat.get(i - 1).getCD_CNAL()) {

						listAux.add(this.objRsRelat.get(i));

					} else {

						this.listaObjRsRelat11.add(listAux);

						listAux = new ArrayList<>();

						listAux.add(this.objRsRelat.get(i));
					}

				}

			}
			
			listAux = new ArrayList<>();
			List<List<hy_relosModel>> listaObjRsRelat12 = new ArrayList<>();

			for (int i = 0; i < this.objRsRelat.size(); i++) {

				if (i == 0) {

					listAux.add(this.objRsRelat.get(i));

				} else if (i == this.objRsRelat.size() - 1) {

					if (this.objRsRelat.get(i).getNR_MES() == this.objRsRelat.get(i - 1).getNR_MES()) {

						listAux.add(this.objRsRelat.get(i));

						listaObjRsRelat12.add(listAux); 

					} else {

						listaObjRsRelat12.add(listAux);

						listAux = new ArrayList<>();           

						listAux.clear();
						listAux.add(this.objRsRelat.get(i));

						listaObjRsRelat12.add(listAux);

					}

				} else {

					if (this.objRsRelat.get(i).getNR_MES() == this.objRsRelat.get(i - 1).getNR_MES()) {

						listAux.add(this.objRsRelat.get(i));

					} else {

						listaObjRsRelat12.add(listAux);

						listAux = new ArrayList<>();

						listAux.add(this.objRsRelat.get(i));
					}

				}

			}
			
			this.listaObjRsRelat12 = criaListaPag12(listaObjRsRelat12);

		}   
		
		public List<Relos_ModelPag12> criaListaPag12(List<List<hy_relosModel>> listaObjRsRelat12){
			
			//Lista de listas para retorno
			List<Relos_ModelPag12> listaRetorno = new ArrayList<>();
			
			for (List<hy_relosModel> listObj : listaObjRsRelat12) {
				
				//Objeto do model para adicionar na lista que vai para a lista de listas
				Relos_ModelPag12 objModel = new Relos_ModelPag12(listObj.get(0).getNR_MES(), 0, 0, 0, 0);
				
				for(int i = 0; i < listObj.size(); i++) {
					
					
					switch (listObj.get(i).getCD_CNAL()) {
					case 6:
						
						objModel.setInternet(listObj.get(i).getStrInternet());
						
						break;
						
					case 37:
							
						objModel.setSuperLinha(listObj.get(i).getStrSuperLinha());
						
						break;
					
					case 36:
						
						objModel.setCartaoCredito(listObj.get(i).getStrCartaoCredit());
	
						break;
					
					case 20:
						
						objModel.setCartaoDebito(listObj.get(i).getStrCartaoDebit());
	
						break;
						
					default:
						//---//
						break;
					}
					
					
					if(i == listObj.size() - 1) {
						
						listaRetorno.add(objModel);
						
					}
										
				}
				
				
				
			}
			
			
			return listaRetorno;
			
		}
		
		public List<hy_relosModel> fnRelPenalidade(String strDtIni, String strDtFim) {

			return objRsRelat;

		}

		public List<hy_relosModel> fnRelOSFraudeSegmento(String strDtIni, String strDtFim, int cod) {

			return objRsRelat;

		}

		public List<hy_relosModel> fnRelRiscoOper(String strDtIni, String strDtFim) {

			return objRsRelat;

		}

		public List<hy_relosModel> fnRelFraudeAberto(String strDtIni, String strDtFim) {

			return objRsRelat;

		}

		public List<hy_relosModel> fnRelGOERecupMes(String strDtIni, String strDtFim) {

			return objRsRelat;

		}

		// ---------------------------------------------------------------------------//

		// Construtores

		public List<hy_relosModel> getObjRsRelat() {
			return objRsRelat;
		}

		public void setObjRsRelat(List<hy_relosModel> objRsRelat) {
			this.objRsRelat = objRsRelat;
		}

		public List<List<hy_relosModel>> getListaObjRsRelat() {
			return listaObjRsRelat;
		}

		public void setListaObjRsRelat(List<List<hy_relosModel>> listaObjRsRelat) {
			this.listaObjRsRelat = listaObjRsRelat;
		}

		public List<List<hy_relosModel>> getListaObjRsRelat11() {
			return listaObjRsRelat11;
		}

		public void setListaObjRsRelat11(List<List<hy_relosModel>> listaObjRsRelat11) {
			this.listaObjRsRelat11 = listaObjRsRelat11;
		}

		public List<Relos_ModelPag12> getListaObjRsRelat12() {
			return listaObjRsRelat12;
		}

		public void setListaObjRsRelat12(List<Relos_ModelPag12> listaObjRsRelat12) {
			this.listaObjRsRelat12 = listaObjRsRelat12;
		}

	}

}
