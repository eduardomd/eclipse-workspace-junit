package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

//import com.altec.bsbr.app.hyb.web.util.HyUsuarioIncUtil;
import com.altec.bsbr.app.jab.hyb.webclient.XHYRelatorios.XHYRelatoriosEndPoint;
import com.altec.bsbr.app.hyb.dto.RelosEncerradaCanalCdModel;
import com.altec.bsbr.app.hyb.dto.RelosEstatisticoSegmento;
import com.altec.bsbr.app.hyb.dto.RelosEstatisticoSegmentoData;
import com.altec.bsbr.app.hyb.dto.RelosEstatisticoSegmentoPessoa;
import com.altec.bsbr.app.hyb.web.util.XHYUsuarioIncService;
import com.altec.bsbr.app.jab.hyb.webclient.XHYRelatorios.WebServiceException;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.ibm.icu.text.NumberFormat;

@Component("RelosEstatiticoSegmentoBean")
@Scope("session")
public class RelosEstatiticoSegmentoBean extends BasicBBean {

	private static final long serialVersionUID = 1L;

	private Date data = new Date(System.currentTimeMillis());

	private String strArea;
	private String pCodArea;
	private String strTpRel;
	private String strNmRelat;
	private String strDtIni;
	private String strDtFim;
	private String intCanal;

	private int linhaAtual;

	private String NOME_AREA_USUARIO = "";

	@Autowired
	private XHYRelatoriosEndPoint objRelat;

	private List<RelosEstatisticoSegmento> objRsRelat;
	private List<RelosEstatisticoSegmentoPessoa> objRsRelatHeaderPF;
	private List<RelosEstatisticoSegmentoPessoa> objRsRelatHeaderPJ;
	private List<RelosEstatisticoSegmentoData> objRsRelatData;

	private double totalPrejuMesPF;
	private double totalPrejuMesPJ;

	private double totalTpCliPj = 0;
	private double totalTpCliPf = 0;

	public RelosEstatiticoSegmentoBean() {
	}

	@PostConstruct
	public void init() {
		FacesContext fc = FacesContext.getCurrentInstance();
		objRsRelat = new ArrayList<RelosEstatisticoSegmento>();
		objRsRelatHeaderPF = new ArrayList<RelosEstatisticoSegmentoPessoa>();
		objRsRelatHeaderPJ = new ArrayList<RelosEstatisticoSegmentoPessoa>();
		objRsRelatData = new ArrayList<RelosEstatisticoSegmentoData>();
		try {
			Map<String, String> params = fc.getExternalContext().getRequestParameterMap();

			strArea = params.get("pArea");
			pCodArea = params.get("pCodArea");
			strTpRel = params.get("pTpRel");
			strNmRelat = params.get("pNmRel");
			strDtIni = params.get("pDtIni");
			strDtFim = params.get("pDtFim");
			intCanal = params.get("pCdCanal");
			NOME_AREA_USUARIO = strArea;

		} catch (NullPointerException e) {
			e.printStackTrace();
		}

		obterRelosEstatisticoSeg();
		obterRelosEstatisticoSegHeaderPF();
		obterRelosEstatisticoSegHeaderPJ();
		obterRelosEstatisticoSegData();
		
		cleanSession();
	}
	public void cleanSession() {
		FacesContext context = FacesContext.getCurrentInstance();
		if (context.getExternalContext().getSessionMap().get("relatorioosBean") != null) {
			context.getExternalContext().getSessionMap().remove("relatorioosBean");
		}
	}

	public void obterRelosEstatisticoSeg() {

		try {

			String retorno = "";
			retorno = this.objRelat.fnRelOSFraudeSegmento(this.strDtIni, this.strDtFim, this.pCodArea);

			JSONObject relatorios = new JSONObject(retorno);
			JSONArray pcursor = relatorios.getJSONArray("PCURSOR");
			for (int i = 0; i < pcursor.length(); i++) {
				JSONObject item = pcursor.getJSONObject(i);
				RelosEstatisticoSegmento relatorio = new RelosEstatisticoSegmento();
				relatorio.setDT_MM_YYYY(item.isNull("DT_MM_YYYY") ? "" : item.get("DT_MM_YYYY").toString());
				relatorio.setDT_MONTH_YYYY(item.isNull("DT_MONTH_YYYY") ? "" : item.get("DT_MONTH_YYYY").toString());
				relatorio.setNM_SEGM(item.isNull("NM_SEGM") ? "" : item.get("NM_SEGM").toString());
				relatorio.setPREJUIZO(item.isNull("PREJUIZO") ? "" : item.get("PREJUIZO").toString());
				relatorio.setQNTD(item.isNull("QNTD") ? "" : item.get("QNTD").toString());
				relatorio.setTP_PESS_CLIE(item.isNull("TP_PESS_CLIE") ? "" : item.get("TP_PESS_CLIE").toString());

				this.objRsRelat.add(relatorio);
			}
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	public void obterRelosEstatisticoSegHeaderPF() {

		try {

			String retorno = "";
			retorno = this.objRelat.fnRelOSFraudeSegmentoHeader(this.strDtIni, this.strDtFim, this.pCodArea, "1");
			JSONObject relatorios = new JSONObject(retorno);
			JSONArray pcursor = relatorios.getJSONArray("PCURSOR");
			for (int i = 0; i < pcursor.length(); i++) {
				JSONObject item = pcursor.getJSONObject(i);
				RelosEstatisticoSegmentoPessoa relatorio = new RelosEstatisticoSegmentoPessoa();
				relatorio.setNM_SEGM(item.isNull("NM_SEGM") ? "" : item.get("NM_SEGM").toString());
				relatorio.setTP_PESS_CLIE(item.isNull("TP_PESS_CLIE") ? "" : item.get("TP_PESS_CLIE").toString());
				this.objRsRelatHeaderPF.add(relatorio);
			}
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	public void obterRelosEstatisticoSegHeaderPJ() {
		try {

			String retorno = "";
			retorno = this.objRelat.fnRelOSFraudeSegmentoHeader(this.strDtIni, this.strDtFim, this.pCodArea, "2");
			JSONObject relatorios = new JSONObject(retorno);
			JSONArray pcursor = relatorios.getJSONArray("PCURSOR");
			for (int i = 0; i < pcursor.length(); i++) {
				JSONObject item = pcursor.getJSONObject(i);
				RelosEstatisticoSegmentoPessoa relatorio = new RelosEstatisticoSegmentoPessoa();
				relatorio.setNM_SEGM(item.isNull("NM_SEGM") ? "" : item.get("NM_SEGM").toString());
				relatorio.setTP_PESS_CLIE(item.isNull("TP_PESS_CLIE") ? "" : item.get("TP_PESS_CLIE").toString());
				this.objRsRelatHeaderPJ.add(relatorio);
			}
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	public void obterRelosEstatisticoSegData() {

		FacesContext fc = FacesContext.getCurrentInstance();
		@SuppressWarnings("unchecked")
		Map<String, String> params = fc.getExternalContext().getRequestParameterMap();

		this.strDtIni = params.get("pDtIni");
		this.strDtFim = params.get("pDtFim");
		this.pCodArea = params.get("pCodArea");

		try {

			String retorno = "";

			retorno = this.objRelat.fnRelOSFraudeSegmentoData(this.strDtIni, this.strDtFim, this.pCodArea);
			JSONObject relatorios = new JSONObject(retorno);
			JSONArray pcursor = relatorios.getJSONArray("PCURSOR");
			for (int i = 0; i < pcursor.length(); i++) {
				JSONObject item = pcursor.getJSONObject(i);

				RelosEstatisticoSegmentoData relatorio = new RelosEstatisticoSegmentoData();
				relatorio.setDT_MM_YYYY(item.isNull("DT_MM_YYYY") ? "" : item.get("DT_MM_YYYY").toString());
				relatorio.setDT_MONTH_YYYY(item.isNull("DT_MONTH_YYYY") ? "" : item.get("DT_MONTH_YYYY").toString());
				this.objRsRelatData.add(relatorio);
			}
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	public void postProcessExcel(Object doc) {

		try {
			XSSFWorkbook wb = (XSSFWorkbook) doc;
			XSSFSheet sheet = wb.getSheetAt(0);
			sheet.setDisplayGridlines(false);

			formatarCabecalho(wb, sheet);
			linhaAtual = 8;

			if (!objRsRelat.isEmpty() && objRsRelat != null) {
				construirTabelaValoresEmReais(linhaAtual, sheet, wb);
				linhaAtual += 7;

				construirTabelaQuantidade(linhaAtual, sheet, wb);
				linhaAtual += 7;

				construirResumoFraudesClassifSegmento(linhaAtual, sheet, wb);
				linhaAtual += 2;
				construirResumoGeralFraudes(linhaAtual + 2, sheet, wb);

				criarRodape(wb, sheet);
				redefinirColunas(wb, sheet.getRow(9).getLastCellNum(), false);

			}

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public void redefinirColunas(XSSFWorkbook wb, int n, Boolean merged) {
		XSSFSheet sheet = wb.getSheetAt(0);
		for (int i = 0; i <= n; i++) {
			sheet.autoSizeColumn(i, merged);
		}

	}

	public Boolean possuiTipoDeTabela(int tipoTabela, List<RelosEncerradaCanalCdModel> objRs) {
		for (int i = 0; i < objRs.size(); i++) {
			if (objRs.get(i).getTipoTabela() == tipoTabela) {
				return true;
			}
		}
		return false;
	}

	private void criarRodape(XSSFWorkbook wb, XSSFSheet sheet) {
		Font fontCenter;
		/* Criação, inclusão e formatação dos dados do footer do excel */
		XSSFRow ultimaLinha = sheet.createRow(sheet.getLastRowNum() + 2);

		XSSFCellStyle footerStyle = wb.createCellStyle();
		footerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		footerStyle.setWrapText(true);
		fontCenter = footerStyle.getFont();
		fontCenter.setFontHeight((short) 150);
		fontCenter.setFontName("Arial");
		fontCenter.setColor(IndexedColors.BLACK.getIndex());
		fontCenter.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		footerStyle.setFont(fontCenter);

		XSSFCell cell1 = ultimaLinha.createCell(0);
		cell1.setCellValue(now());
		cell1.setCellStyle(footerStyle);

		XSSFCell cell2 = ultimaLinha.createCell(1);
		cell2.setCellValue("Superintêndencia de Ocorrências Especiais");
		cell2.setCellStyle(footerStyle);

		CellRangeAddress region2 = new CellRangeAddress(ultimaLinha.getRowNum(), ultimaLinha.getRowNum(), 1, 12);
		sheet.addMergedRegion(region2);
		if (objRsRelat.isEmpty() || objRsRelat == null) {
			sheet.shiftRows(13, 14, -5);
		}

	}

	private void formatarCabecalho(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyle1 = wb.createCellStyle();
		headerStyle1.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle1.setWrapText(true);
		headerStyle1.setBorderBottom(CellStyle.BORDER_NONE);
		headerStyle1.setBorderLeft(CellStyle.BORDER_NONE);
		headerStyle1.setBorderRight(CellStyle.BORDER_NONE);
		headerStyle1.setBorderTop(CellStyle.BORDER_NONE);
		Font fontCenter = wb.createFont();
		fontCenter.setFontHeightInPoints((short) 14);
		fontCenter.setFontName("Calibri");
		fontCenter.setColor(IndexedColors.BLACK.getIndex());
		fontCenter.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		headerStyle1.setFont(fontCenter);

		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setWrapText(true);
		headerStyle.setBorderBottom(CellStyle.BORDER_NONE);
		headerStyle.setBorderLeft(CellStyle.BORDER_NONE);
		headerStyle.setBorderRight(CellStyle.BORDER_NONE);
		headerStyle.setBorderTop(CellStyle.BORDER_NONE);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 14);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(font);

		XSSFRow primeiraLinha = sheet.getRow(1);
		if (primeiraLinha != null) {
			XSSFCell primeiraLinhaCell = primeiraLinha.getCell(0);
			primeiraLinhaCell.setCellStyle(headerStyle);
			primeiraLinha.setHeightInPoints((short) 19);
		}
		XSSFRow segundaLinha = sheet.getRow(3);
		if (segundaLinha != null) {
			XSSFCell segundaLinhaCell = segundaLinha.getCell(0);
			segundaLinhaCell.setCellStyle(headerStyle1);
			segundaLinha.setHeightInPoints((short) 19);
		}

	}

	public void construirTabelaValoresEmReais(int linha, XSSFSheet sheet, XSSFWorkbook wb) {
		String titulo = "Valores em Reais";
		montarCabecalhoTabelaPFePJ(objRsRelatHeaderPF, objRsRelatHeaderPJ, titulo, linha, sheet, wb);
		montarCorpoTabelaPFePJ(linha + 2, sheet, wb);
		montarTotalTabelaPFePJ(linha + 3, sheet, wb);
		montarPorcentagemTabelaPFePJ(linha + 4, sheet, wb);
	}

	public void construirTabelaQuantidade(int linha, XSSFSheet sheet, XSSFWorkbook wb) {
		String titulo = "Quantidade";
		montarCabecalhoTabelaQuantidade(objRsRelatHeaderPF, objRsRelatHeaderPJ, titulo, linha, sheet, wb);
		montarCorpoTabelaQuantidade(linha + 2, sheet, wb);
		montarTotalTabelaQuantidade(linha + 3, sheet, wb);
		montarPorcentagemTabelaQuantidade(linha + 4, sheet, wb);
	}

	public void construirResumoFraudesClassifSegmento(int linha, XSSFSheet sheet, XSSFWorkbook wb) {
		String titulo = "Resumo de Fraudes Classificadas por Segmento";

		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyle.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyle.setBorderRight(CellStyle.BORDER_THIN);
		headerStyle.setBorderTop(CellStyle.BORDER_THIN);
		headerStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font fontCenter = wb.createFont();
		fontCenter.setFontHeightInPoints((short) 11);
		fontCenter.setFontName("Calibri");
		fontCenter.setColor(IndexedColors.WHITE.getIndex());
		fontCenter.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(fontCenter);

		XSSFCellStyle tituloStyle = wb.createCellStyle();
		tituloStyle.setAlignment(CellStyle.ALIGN_LEFT);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		tituloStyle.setFont(font);

		headerStyle.setFont(fontCenter);
		XSSFRow tituloLinha = sheet.getRow(linhaAtual);
		if (tituloLinha == null) {
			tituloLinha = sheet.createRow(linhaAtual);
		}
		XSSFCell cell1 = tituloLinha.createCell(0);
		cell1.setCellValue(titulo);
		cell1.setCellStyle(tituloStyle);

		CellRangeAddress regionTitulo = new CellRangeAddress(tituloLinha.getRowNum(), tituloLinha.getRowNum(), 0, 3);
		sheet.addMergedRegion(regionTitulo);
		linhaAtual += 1;

		for (int i = 0; i < objRsRelatData.size(); i++) {
			XSSFRow linhaTabela = sheet.getRow(linhaAtual);
			if (linhaTabela == null) {
				linhaTabela = sheet.createRow(linhaAtual);
			}
			XSSFCell cell = linhaTabela.createCell(0);
			cell.setCellValue(objRsRelatData.get(i).getDT_MONTH_YYYY());

			CellRangeAddress region = new CellRangeAddress(linhaTabela.getRowNum(), linhaTabela.getRowNum(), 0, 3);
			sheet.addMergedRegion(region);

			RegionUtil.setBorderTop(CellStyle.BORDER_THIN, region, sheet, wb);
			RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, region, sheet, wb);
			RegionUtil.setBorderRight(CellStyle.BORDER_THIN, region, sheet, wb);
			RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, region, sheet, wb);

			cell.setCellStyle(headerStyle);

			linhaAtual++;
			montarCabecalhoFraudesCSegmento(sheet, wb);
			linhaAtual++;
			montarCorpoFraudesCSegmento(sheet, wb, objRsRelatData.get(i).getDT_MM_YYYY());
			linhaAtual += 3;

		}
	}

	public void construirResumoGeralFraudes(int linha, XSSFSheet sheet, XSSFWorkbook wb) {
		String titulo = "Resumo Geral de Fraudes Classificadas por Segmento";
		montarCabecalhoFraudeGeral(titulo, linha, sheet, wb);
		montarCorpoFraudeGeral(linha + 2, sheet, wb);
	}

	public void montarCabecalhoTabelaPFePJ(List<RelosEstatisticoSegmentoPessoa> cabecalhosPF,
			List<RelosEstatisticoSegmentoPessoa> cabecalhosPJ, String titulo, int linha, XSSFSheet sheet,
			XSSFWorkbook wb) {
		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setWrapText(true);
		headerStyle.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyle.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyle.setBorderRight(CellStyle.BORDER_THIN);
		headerStyle.setBorderTop(CellStyle.BORDER_THIN);
		headerStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font fontCenter = wb.createFont();
		fontCenter.setFontHeightInPoints((short) 11);
		fontCenter.setFontName("Calibri");
		fontCenter.setColor(IndexedColors.WHITE.getIndex());
		fontCenter.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(fontCenter);

		XSSFCellStyle tituloStyle = wb.createCellStyle();
		tituloStyle.setAlignment(CellStyle.ALIGN_LEFT);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		tituloStyle.setFont(font);

		XSSFRow tituloLinha = sheet.getRow(linha);
		if (tituloLinha == null) {
			tituloLinha = sheet.createRow(linha);
		}
		XSSFCell cell1 = tituloLinha.createCell(0);
		cell1.setCellValue(titulo);
		cell1.setCellStyle(tituloStyle);

		CellRangeAddress region2 = new CellRangeAddress(tituloLinha.getRowNum(), tituloLinha.getRowNum(), 0, 2);
		sheet.addMergedRegion(region2);

		XSSFRow primeiraLinha = sheet.getRow(linha + 1);
		if (primeiraLinha == null) {
			primeiraLinha = sheet.createRow(linha + 1);
		}
		primeiraLinha.setHeightInPoints((short) 18);

		XSSFCell primeiraLinhaCell = primeiraLinha.getCell(0);
		if (primeiraLinhaCell == null) {
			primeiraLinhaCell = primeiraLinha.createCell(0);
		}
		primeiraLinhaCell.setCellStyle(headerStyle);
		primeiraLinhaCell.setCellValue(" ");

		for (int i = 0; i < cabecalhosPF.size(); i++) {
			XSSFCell primeiraLinhaCellPF = primeiraLinha.getCell(i + 1);
			if (primeiraLinhaCellPF == null) {
				primeiraLinhaCellPF = primeiraLinha.createCell(i + 1);
			}
			primeiraLinhaCellPF.setCellStyle(headerStyle);
			primeiraLinhaCellPF.setCellValue(cabecalhosPF.get(i).getNM_SEGM());
		}
		if (!objRsRelatHeaderPF.isEmpty() && objRsRelatHeaderPF != null) {
			XSSFCell primeiraLinhaCellTotalPF = primeiraLinha.getCell(primeiraLinha.getLastCellNum());
			if (primeiraLinhaCellTotalPF == null) {
				primeiraLinhaCellTotalPF = primeiraLinha.createCell(primeiraLinha.getLastCellNum());
			}
			primeiraLinhaCellTotalPF.setCellStyle(headerStyle);
			primeiraLinhaCellTotalPF.setCellValue("Total PF");
		}
		for (int i = 0; i < cabecalhosPJ.size(); i++) {
			XSSFCell primeiraLinhaCellPJ = primeiraLinha.getCell(primeiraLinha.getLastCellNum());
			if (primeiraLinhaCellPJ == null) {
				primeiraLinhaCellPJ = primeiraLinha.createCell(primeiraLinha.getLastCellNum());
			}
			primeiraLinhaCellPJ.setCellStyle(headerStyle);
			primeiraLinhaCellPJ.setCellValue(cabecalhosPJ.get(i).getNM_SEGM());
		}
		if (!objRsRelatHeaderPJ.isEmpty() && objRsRelatHeaderPJ != null) {
			XSSFCell primeiraLinhaCellTotalPJ = primeiraLinha.getCell(primeiraLinha.getLastCellNum());
			if (primeiraLinhaCellTotalPJ == null) {
				primeiraLinhaCellTotalPJ = primeiraLinha.createCell(primeiraLinha.getLastCellNum());
			}
			primeiraLinhaCellTotalPJ.setCellStyle(headerStyle);
			primeiraLinhaCellTotalPJ.setCellValue("Total PJ");
		}
		XSSFCell TotalGeral = primeiraLinha.getCell(primeiraLinha.getLastCellNum());
		if (TotalGeral == null) {
			TotalGeral = primeiraLinha.createCell(primeiraLinha.getLastCellNum());
		}
		TotalGeral.setCellStyle(headerStyle);
		TotalGeral.setCellValue("Total Geral");

	}

	public void montarCorpoTabelaPFePJ(int linha, XSSFSheet sheet, XSSFWorkbook wb) {

		XSSFCellStyle headerStyleLeft = wb.createCellStyle();
		headerStyleLeft.setAlignment(CellStyle.ALIGN_LEFT);
		headerStyleLeft.setWrapText(true);
		headerStyleLeft.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderTop(CellStyle.BORDER_THIN);
		headerStyleLeft.setFillForegroundColor(IndexedColors.RED.getIndex());
		headerStyleLeft.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font fontLeft = wb.createFont();
		fontLeft.setFontHeightInPoints((short) 11);
		fontLeft.setFontName("Calibri");
		fontLeft.setColor(IndexedColors.WHITE.getIndex());
		fontLeft.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyleLeft.setFont(fontLeft);

		XSSFCellStyle headerStyleRight = wb.createCellStyle();
		headerStyleRight.setAlignment(CellStyle.ALIGN_RIGHT);
		headerStyleRight.setWrapText(true);
		headerStyleRight.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderTop(CellStyle.BORDER_THIN);
		headerStyleRight.setFillForegroundColor(IndexedColors.RED.getIndex());
		headerStyleRight.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font fontRight = wb.createFont();
		fontRight.setFontHeightInPoints((short) 11);
		fontRight.setFontName("Calibri");
		fontRight.setColor(IndexedColors.WHITE.getIndex());
		fontRight.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyleRight.setFont(fontLeft);

		XSSFRow primeiraLinha = sheet.getRow(linha);
		if (primeiraLinha == null) {
			primeiraLinha = sheet.createRow(linha);
		}
		primeiraLinha.setHeightInPoints((short) 18);

		int cellNum = 0;
		NumberFormat formatacao = NumberFormat.getCurrencyInstance(Locale.getDefault());
		for (int i = 0; i < objRsRelatData.size(); i++) {
			XSSFCell primeiraLinhaCellMes = primeiraLinha.getCell(cellNum);
			if (primeiraLinhaCellMes == null) {
				primeiraLinhaCellMes = primeiraLinha.createCell(cellNum);
			}
			primeiraLinhaCellMes.setCellStyle(headerStyleLeft);
			primeiraLinhaCellMes.setCellValue(objRsRelatData.get(i).getDT_MONTH_YYYY());

			cellNum++;

			for (int k = 0; k < objRsRelatHeaderPF.size(); k++) {
				for (int d = 0; d < objRsRelat.size(); d++) {

					if (objRsRelat.get(d).getDT_MM_YYYY().equals(objRsRelatData.get(i).getDT_MM_YYYY())
							&& objRsRelat.get(d).getNM_SEGM().equals(objRsRelatHeaderPF.get(k).getNM_SEGM())
							&& objRsRelat.get(d).getTP_PESS_CLIE().equals("1")) {

						XSSFCell primeiraLinhaCell = primeiraLinha.getCell(cellNum);
						if (primeiraLinhaCell == null) {
							primeiraLinhaCell = primeiraLinha.createCell(cellNum);
						}
						primeiraLinhaCell.setCellStyle(headerStyleRight);
						if (Double.valueOf(objRsRelat.get(d).getPREJUIZO()) == 0) {
							primeiraLinhaCell.setCellValue(0);
						} else {
							primeiraLinhaCell.setCellValue(NumberFormat.getCurrencyInstance()
									.format(Double.valueOf(objRsRelat.get(d).getPREJUIZO()))
									.replace(formatacao.getCurrency().getSymbol(), ""));
						}
						cellNum++;
						totalPrejuMesPF = totalPrejuMesPF + Double.valueOf(objRsRelat.get(d).getPREJUIZO());
					}
				}
			}
			if (!objRsRelatHeaderPF.isEmpty() && objRsRelatHeaderPF != null) {
				XSSFCell primeiraLinhaCell = primeiraLinha.getCell(cellNum);
				if (primeiraLinhaCell == null) {
					primeiraLinhaCell = primeiraLinha.createCell(cellNum);
				}
				primeiraLinhaCell.setCellStyle(headerStyleRight);
				if (totalPrejuMesPF == 0) {
					primeiraLinhaCell.setCellValue(0);
				} else {
					primeiraLinhaCell.setCellValue(NumberFormat.getCurrencyInstance().format(totalPrejuMesPF)
							.replace(formatacao.getCurrency().getSymbol(), ""));
				}

				cellNum++;
			}
			for (int k = 0; k < objRsRelatHeaderPJ.size(); k++) {
				for (int d = 0; d < objRsRelat.size(); d++) {

					if (objRsRelat.get(d).getDT_MM_YYYY().equals(objRsRelatData.get(i).getDT_MM_YYYY())
							&& objRsRelat.get(d).getNM_SEGM().equals(objRsRelatHeaderPJ.get(k).getNM_SEGM())
							&& objRsRelat.get(d).getTP_PESS_CLIE().equals("2")) {

						XSSFCell primeiraLinhaCell = primeiraLinha.getCell(cellNum);
						if (primeiraLinhaCell == null) {
							primeiraLinhaCell = primeiraLinha.createCell(cellNum);
						}
						primeiraLinhaCell.setCellStyle(headerStyleRight);
						if (Double.valueOf(objRsRelat.get(d).getPREJUIZO()) == 0) {
							primeiraLinhaCell.setCellValue(0);
						} else {
							primeiraLinhaCell.setCellValue(NumberFormat.getCurrencyInstance()
									.format(Double.valueOf(objRsRelat.get(d).getPREJUIZO()))
									.replace(formatacao.getCurrency().getSymbol(), ""));
						}
						cellNum++;
						totalPrejuMesPJ = totalPrejuMesPJ + Double.valueOf(objRsRelat.get(d).getPREJUIZO());
					}
				}
			}

			XSSFCell primeiraLinhaCell = primeiraLinha.getCell(cellNum);
			if (primeiraLinhaCell == null) {
				primeiraLinhaCell = primeiraLinha.createCell(cellNum);
			}
			primeiraLinhaCell.setCellStyle(headerStyleRight);
			if (totalPrejuMesPJ == 0) {
				primeiraLinhaCell.setCellValue(0);
			} else {
				primeiraLinhaCell.setCellValue(NumberFormat.getCurrencyInstance().format(totalPrejuMesPJ)
						.replace(formatacao.getCurrency().getSymbol(), ""));
			}
			cellNum++;

		}
		if (!objRsRelatHeaderPJ.isEmpty() && objRsRelatHeaderPJ != null) {
			XSSFCell primeiraLinhaCell = primeiraLinha.getCell(cellNum);
			if (primeiraLinhaCell == null) {
				primeiraLinhaCell = primeiraLinha.createCell(cellNum);
			}
			primeiraLinhaCell.setCellStyle(headerStyleRight);
			if (totalPrejuMesPJ + totalPrejuMesPF == 0) {
				primeiraLinhaCell.setCellValue(0);
			} else {
				primeiraLinhaCell.setCellValue(NumberFormat.getCurrencyInstance()
						.format(totalPrejuMesPJ + totalPrejuMesPF).replace(formatacao.getCurrency().getSymbol(), ""));
			}
		}
	}

	public void montarTotalTabelaPFePJ(int linha, XSSFSheet sheet, XSSFWorkbook wb) {
		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_RIGHT);
		headerStyle.setWrapText(true);
		headerStyle.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyle.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyle.setBorderRight(CellStyle.BORDER_THIN);
		headerStyle.setBorderTop(CellStyle.BORDER_THIN);
		headerStyle.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font fontCenter = wb.createFont();
		fontCenter.setFontHeightInPoints((short) 11);
		fontCenter.setFontName("Calibri");
		fontCenter.setColor(IndexedColors.WHITE.getIndex());
		fontCenter.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(fontCenter);

		XSSFCellStyle headerStyleLeft = wb.createCellStyle();
		headerStyleLeft.setAlignment(CellStyle.ALIGN_LEFT);
		headerStyleLeft.setWrapText(true);
		headerStyleLeft.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderTop(CellStyle.BORDER_THIN);
		headerStyleLeft.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		headerStyleLeft.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.WHITE.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyleLeft.setFont(font);

		XSSFRow primeiraLinha = sheet.getRow(linha);
		if (primeiraLinha == null) {
			primeiraLinha = sheet.createRow(linha);
		}
		primeiraLinha.setHeightInPoints((short) 18);

		int cellNum = 0;
		XSSFCell linhaCell = primeiraLinha.getCell(cellNum);
		if (linhaCell == null) {
			linhaCell = primeiraLinha.createCell(cellNum);
		}
		linhaCell.setCellStyle(headerStyleLeft);
		linhaCell.setCellValue("Total:");
		cellNum++;

		NumberFormat formatacao = NumberFormat.getCurrencyInstance(Locale.getDefault());

		for (int i = 0; i < objRsRelatHeaderPF.size(); i++) {
			double total = 0;
			for (int j = 0; j < objRsRelat.size(); j++) {
				if (objRsRelat.get(j).getNM_SEGM().equals(objRsRelatHeaderPF.get(i).getNM_SEGM())) {
					total = total + Double.valueOf(objRsRelat.get(i).getPREJUIZO());
				}
			}
			totalTpCliPf = totalTpCliPf + total;
			XSSFCell primeiraLinhaCell = primeiraLinha.getCell(cellNum);
			if (primeiraLinhaCell == null) {
				primeiraLinhaCell = primeiraLinha.createCell(cellNum);
			}
			primeiraLinhaCell.setCellStyle(headerStyle);
			if (total == 0) {
				primeiraLinhaCell.setCellValue(0);
			} else {
				primeiraLinhaCell.setCellValue(NumberFormat.getCurrencyInstance().format(total)
						.replace(formatacao.getCurrency().getSymbol(), ""));
			}
			cellNum++;
		}
		if (!objRsRelatHeaderPF.isEmpty() && objRsRelatHeaderPF != null) {
			XSSFCell primeiraLinhaCell = primeiraLinha.getCell(cellNum);
			if (primeiraLinhaCell == null) {
				primeiraLinhaCell = primeiraLinha.createCell(cellNum);
			}
			primeiraLinhaCell.setCellStyle(headerStyle);
			if (totalTpCliPf == 0) {
				primeiraLinhaCell.setCellValue(0);
			} else {
				primeiraLinhaCell.setCellValue(NumberFormat.getCurrencyInstance().format(totalTpCliPf)
						.replace(formatacao.getCurrency().getSymbol(), ""));
			}
			cellNum++;
		}

		for (int i = 0; i < objRsRelatHeaderPJ.size(); i++) {
			double total = 0;
			for (int j = 0; j < objRsRelat.size(); j++) {
				if (objRsRelat.get(j).getNM_SEGM().equals(objRsRelatHeaderPJ.get(i).getNM_SEGM())) {
					total = total + Double.valueOf(objRsRelat.get(i).getPREJUIZO());
				}
			}
			totalTpCliPj = totalTpCliPj + total;
			XSSFCell primeiraLinhaCell = primeiraLinha.getCell(cellNum);
			if (primeiraLinhaCell == null) {
				primeiraLinhaCell = primeiraLinha.createCell(cellNum);
			}
			primeiraLinhaCell.setCellStyle(headerStyle);
			if (total == 0) {
				primeiraLinhaCell.setCellValue(0);
			} else {
				primeiraLinhaCell.setCellValue(NumberFormat.getCurrencyInstance().format(total)
						.replace(formatacao.getCurrency().getSymbol(), ""));
			}
			cellNum++;
		}
		if (!objRsRelatHeaderPJ.isEmpty() && objRsRelatHeaderPJ != null) {
			XSSFCell primeiraLinhaCell = primeiraLinha.getCell(cellNum);
			if (primeiraLinhaCell == null) {
				primeiraLinhaCell = primeiraLinha.createCell(cellNum);
			}
			primeiraLinhaCell.setCellStyle(headerStyle);
			if (totalTpCliPj == 0) {
				primeiraLinhaCell.setCellValue(0);
			} else {
				primeiraLinhaCell.setCellValue(NumberFormat.getCurrencyInstance().format(totalTpCliPj)
						.replace(formatacao.getCurrency().getSymbol(), ""));
			}
			cellNum++;
		}

		XSSFCell primeiraLinhaCell = primeiraLinha.getCell(cellNum);
		if (primeiraLinhaCell == null) {
			primeiraLinhaCell = primeiraLinha.createCell(cellNum);
		}
		primeiraLinhaCell.setCellStyle(headerStyle);
		if (totalTpCliPj + totalTpCliPf == 0) {
			primeiraLinhaCell.setCellValue(0);
		} else {
			primeiraLinhaCell.setCellValue(NumberFormat.getCurrencyInstance().format(totalTpCliPj + totalTpCliPf)
					.replace(formatacao.getCurrency().getSymbol(), ""));
		}
		cellNum++;

	}

	public void montarPorcentagemTabelaPFePJ(int linha, XSSFSheet sheet, XSSFWorkbook wb) {

		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_RIGHT);
		headerStyle.setWrapText(true);
		headerStyle.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyle.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyle.setBorderRight(CellStyle.BORDER_THIN);
		headerStyle.setBorderTop(CellStyle.BORDER_THIN);
		headerStyle.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font fontCenter = wb.createFont();
		fontCenter.setFontHeightInPoints((short) 11);
		fontCenter.setFontName("Calibri");
		fontCenter.setColor(IndexedColors.WHITE.getIndex());
		fontCenter.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(fontCenter);

		XSSFCellStyle headerStyleLeft = wb.createCellStyle();
		headerStyleLeft.setAlignment(CellStyle.ALIGN_LEFT);
		headerStyleLeft.setWrapText(true);
		headerStyleLeft.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderTop(CellStyle.BORDER_THIN);
		headerStyleLeft.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		headerStyleLeft.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.WHITE.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyleLeft.setFont(font);

		NumberFormat formatacao = NumberFormat.getCurrencyInstance(Locale.getDefault());

		XSSFRow primeiraLinha = sheet.getRow(linha);
		if (primeiraLinha == null) {
			primeiraLinha = sheet.createRow(linha);
		}
		primeiraLinha.setHeightInPoints((short) 18);

		int cellNum = 0;
		XSSFCell linhaCell = primeiraLinha.getCell(cellNum);
		if (linhaCell == null) {
			linhaCell = primeiraLinha.createCell(cellNum);
		}
		linhaCell.setCellStyle(headerStyleLeft);
		linhaCell.setCellValue("Porcentagem:");
		cellNum++;
		double conta = 0;
		for (int i = 0; i < objRsRelatHeaderPF.size(); i++) {
			double total = 0;
			for (int j = 0; j < objRsRelat.size(); j++) {
				if (objRsRelat.get(j).getNM_SEGM().equals(objRsRelatHeaderPF.get(i).getNM_SEGM())) {
					total = total + Double.valueOf(objRsRelat.get(j).getPREJUIZO());
				}
			}
			XSSFCell primeiraLinhaCell = primeiraLinha.getCell(cellNum);
			if (primeiraLinhaCell == null) {
				primeiraLinhaCell = primeiraLinha.createCell(cellNum);
			}

			if (total == 0 && totalTpCliPf == 0) {
				conta = 0;
				primeiraLinhaCell.setCellValue("0%");
			} else {
				conta = (total / totalTpCliPf) * 100;
				primeiraLinhaCell.setCellValue(NumberFormat.getCurrencyInstance().format(conta)
						.replace(formatacao.getCurrency().getSymbol(), "") + "%");
			}
			primeiraLinhaCell.setCellStyle(headerStyle);

			cellNum++;
		}
		if (!objRsRelatHeaderPF.isEmpty() && objRsRelatHeaderPF != null) {
			XSSFCell primeiraLinhaCell = primeiraLinha.getCell(cellNum);
			if (primeiraLinhaCell == null) {
				primeiraLinhaCell = primeiraLinha.createCell(cellNum);
			}
			if (totalTpCliPf == 0) {
				conta = 0;
				primeiraLinhaCell.setCellValue("0%");
			} else {
				conta = (totalTpCliPf / totalTpCliPf) * 100;
				primeiraLinhaCell.setCellValue(NumberFormat.getCurrencyInstance().format(conta)
						.replace(formatacao.getCurrency().getSymbol(), "") + "%");
			}
			primeiraLinhaCell.setCellStyle(headerStyle);

			cellNum++;
		}

		for (int i = 0; i < objRsRelatHeaderPJ.size(); i++) {
			double total = 0;
			for (int j = 0; j < objRsRelat.size(); j++) {
				if (objRsRelat.get(j).getNM_SEGM().equals(objRsRelatHeaderPJ.get(i).getNM_SEGM())) {
					total = total + Double.valueOf(objRsRelat.get(j).getPREJUIZO());
				}
			}
			XSSFCell primeiraLinhaCell = primeiraLinha.getCell(cellNum);
			if (primeiraLinhaCell == null) {
				primeiraLinhaCell = primeiraLinha.createCell(cellNum);
			}
			if (total == 0 && totalTpCliPj == 0) {
				conta = 0;
				primeiraLinhaCell.setCellValue("0%");
			} else {
				conta = (total / totalTpCliPj) * 100;
				primeiraLinhaCell.setCellValue(NumberFormat.getCurrencyInstance().format(conta)
						.replace(formatacao.getCurrency().getSymbol(), "") + "%");
			}
			primeiraLinhaCell.setCellStyle(headerStyle);

			cellNum++;
		}
		if (!objRsRelatHeaderPJ.isEmpty() && objRsRelatHeaderPJ != null) {

			XSSFCell primeiraLinhaCell = primeiraLinha.getCell(cellNum);
			if (primeiraLinhaCell == null) {
				primeiraLinhaCell = primeiraLinha.createCell(cellNum);
			}
			if (totalTpCliPj == 0) {
				conta = 0;
				primeiraLinhaCell.setCellValue("0%");
			} else {
				conta = (totalTpCliPj / totalTpCliPj) * 100;
				primeiraLinhaCell.setCellValue(NumberFormat.getCurrencyInstance().format(conta)
						.replace(formatacao.getCurrency().getSymbol(), "") + "%");
			}
			primeiraLinhaCell.setCellStyle(headerStyle);

			cellNum++;
		}
		XSSFCell primeiraLinhaCell = primeiraLinha.getCell(cellNum);
		if (primeiraLinhaCell == null) {
			primeiraLinhaCell = primeiraLinha.createCell(cellNum);
		}
		primeiraLinhaCell.setCellStyle(headerStyle);

		if (totalTpCliPj == 0 && totalTpCliPf == 0) {
			primeiraLinhaCell.setCellValue("0%");
			conta = 0;
		} else {
			conta = ((totalTpCliPj + totalTpCliPf) / (totalTpCliPj + totalTpCliPf)) * 100;
			primeiraLinhaCell.setCellValue(
					NumberFormat.getCurrencyInstance().format(conta).replace(formatacao.getCurrency().getSymbol(), "")
							+ "%");
		}

	}

	public void montarCabecalhoTabelaQuantidade(List<RelosEstatisticoSegmentoPessoa> cabecalhosPF,
			List<RelosEstatisticoSegmentoPessoa> cabecalhosPJ, String titulo, int linha, XSSFSheet sheet,
			XSSFWorkbook wb) {
		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setWrapText(true);
		headerStyle.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyle.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyle.setBorderRight(CellStyle.BORDER_THIN);
		headerStyle.setBorderTop(CellStyle.BORDER_THIN);
		headerStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font fontCenter = wb.createFont();
		fontCenter.setFontHeightInPoints((short) 11);
		fontCenter.setFontName("Calibri");
		fontCenter.setColor(IndexedColors.WHITE.getIndex());
		fontCenter.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(fontCenter);

		XSSFCellStyle tituloStyle = wb.createCellStyle();
		tituloStyle.setAlignment(CellStyle.ALIGN_LEFT);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		tituloStyle.setFont(font);

		XSSFRow tituloLinha = sheet.getRow(linha);
		if (tituloLinha == null) {
			tituloLinha = sheet.createRow(linha);
		}
		XSSFCell cell1 = tituloLinha.createCell(0);
		cell1.setCellValue(titulo);
		cell1.setCellStyle(tituloStyle);

		CellRangeAddress region2 = new CellRangeAddress(tituloLinha.getRowNum(), tituloLinha.getRowNum(), 0, 2);
		sheet.addMergedRegion(region2);

		XSSFRow primeiraLinha = sheet.getRow(linha + 1);
		if (primeiraLinha == null) {
			primeiraLinha = sheet.createRow(linha + 1);
		}
		primeiraLinha.setHeightInPoints((short) 18);

		XSSFCell primeiraLinhaCell = primeiraLinha.getCell(0);
		if (primeiraLinhaCell == null) {
			primeiraLinhaCell = primeiraLinha.createCell(0);
		}
		primeiraLinhaCell.setCellStyle(headerStyle);
		primeiraLinhaCell.setCellValue(" ");

		for (int i = 0; i < cabecalhosPF.size(); i++) {
			XSSFCell primeiraLinhaCellPF = primeiraLinha.getCell(i + 1);
			if (primeiraLinhaCellPF == null) {
				primeiraLinhaCellPF = primeiraLinha.createCell(i + 1);
			}
			primeiraLinhaCellPF.setCellStyle(headerStyle);
			primeiraLinhaCellPF.setCellValue(cabecalhosPF.get(i).getNM_SEGM());
		}
		if (!objRsRelatHeaderPF.isEmpty() && objRsRelatHeaderPF != null) {

			XSSFCell primeiraLinhaCellTotalPF = primeiraLinha.getCell(primeiraLinha.getLastCellNum());
			if (primeiraLinhaCellTotalPF == null) {
				primeiraLinhaCellTotalPF = primeiraLinha.createCell(primeiraLinha.getLastCellNum());
			}
			primeiraLinhaCellTotalPF.setCellStyle(headerStyle);
			primeiraLinhaCellTotalPF.setCellValue("Total PF");
		}
		for (int i = 0; i < cabecalhosPJ.size(); i++) {
			XSSFCell primeiraLinhaCellPJ = primeiraLinha.getCell(primeiraLinha.getLastCellNum());
			if (primeiraLinhaCellPJ == null) {
				primeiraLinhaCellPJ = primeiraLinha.createCell(primeiraLinha.getLastCellNum());
			}
			primeiraLinhaCellPJ.setCellStyle(headerStyle);
			primeiraLinhaCellPJ.setCellValue(cabecalhosPJ.get(i).getNM_SEGM());
		}
		if (!objRsRelatHeaderPJ.isEmpty() && objRsRelatHeaderPJ != null) {
			XSSFCell primeiraLinhaCellTotalPJ = primeiraLinha.getCell(primeiraLinha.getLastCellNum());
			if (primeiraLinhaCellTotalPJ == null) {
				primeiraLinhaCellTotalPJ = primeiraLinha.createCell(primeiraLinha.getLastCellNum());
			}
			primeiraLinhaCellTotalPJ.setCellStyle(headerStyle);
			primeiraLinhaCellTotalPJ.setCellValue("Total PJ");
		}
		XSSFCell TotalGeral = primeiraLinha.getCell(primeiraLinha.getLastCellNum());
		if (TotalGeral == null) {
			TotalGeral = primeiraLinha.createCell(primeiraLinha.getLastCellNum());
		}
		TotalGeral.setCellStyle(headerStyle);
		TotalGeral.setCellValue("Total Geral");

	}

	public void montarCorpoTabelaQuantidade(int linha, XSSFSheet sheet, XSSFWorkbook wb) {

		XSSFCellStyle headerStyleLeft = wb.createCellStyle();
		headerStyleLeft.setAlignment(CellStyle.ALIGN_LEFT);
		headerStyleLeft.setWrapText(true);
		headerStyleLeft.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderTop(CellStyle.BORDER_THIN);
		headerStyleLeft.setFillForegroundColor(IndexedColors.RED.getIndex());
		headerStyleLeft.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font fontLeft = wb.createFont();
		fontLeft.setFontHeightInPoints((short) 11);
		fontLeft.setFontName("Calibri");
		fontLeft.setColor(IndexedColors.WHITE.getIndex());
		fontLeft.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyleLeft.setFont(fontLeft);

		XSSFCellStyle headerStyleRight = wb.createCellStyle();
		headerStyleRight.setAlignment(CellStyle.ALIGN_RIGHT);
		headerStyleRight.setWrapText(true);
		headerStyleRight.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderTop(CellStyle.BORDER_THIN);
		headerStyleRight.setFillForegroundColor(IndexedColors.RED.getIndex());
		headerStyleRight.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font fontRight = wb.createFont();
		fontRight.setFontHeightInPoints((short) 11);
		fontRight.setFontName("Calibri");
		fontRight.setColor(IndexedColors.WHITE.getIndex());
		fontRight.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyleRight.setFont(fontLeft);

		XSSFRow primeiraLinha = sheet.getRow(linha);
		if (primeiraLinha == null) {
			primeiraLinha = sheet.createRow(linha);
		}
		primeiraLinha.setHeightInPoints((short) 18);

		int cellNum = 0;
		NumberFormat formatacao = NumberFormat.getCurrencyInstance(Locale.getDefault());
		for (int i = 0; i < objRsRelatData.size(); i++) {
			XSSFCell primeiraLinhaCellMes = primeiraLinha.getCell(cellNum);
			if (primeiraLinhaCellMes == null) {
				primeiraLinhaCellMes = primeiraLinha.createCell(cellNum);
			}
			primeiraLinhaCellMes.setCellStyle(headerStyleLeft);
			primeiraLinhaCellMes.setCellValue(objRsRelatData.get(i).getDT_MONTH_YYYY());

			cellNum++;

			for (int k = 0; k < objRsRelatHeaderPF.size(); k++) {
				for (int d = 0; d < objRsRelat.size(); d++) {
					if (objRsRelat.get(d).getDT_MM_YYYY().equals(objRsRelatData.get(i).getDT_MM_YYYY())
							&& objRsRelat.get(d).getNM_SEGM().equals(objRsRelatHeaderPF.get(k).getNM_SEGM())) {
						XSSFCell primeiraLinhaCell = primeiraLinha.getCell(cellNum);
						if (primeiraLinhaCell == null) {
							primeiraLinhaCell = primeiraLinha.createCell(cellNum);
						}
						primeiraLinhaCell.setCellStyle(headerStyleRight);
						primeiraLinhaCell.setCellValue(Integer.valueOf(objRsRelat.get(d).getQNTD()));
						cellNum++;
						totalPrejuMesPF = totalPrejuMesPF + Double.valueOf(objRsRelat.get(d).getQNTD());
					}
				}
			}
			if (!objRsRelatHeaderPF.isEmpty() && objRsRelatHeaderPF != null) {
				XSSFCell primeiraLinhaCell = primeiraLinha.getCell(cellNum);
				if (primeiraLinhaCell == null) {
					primeiraLinhaCell = primeiraLinha.createCell(cellNum);
				}
				primeiraLinhaCell.setCellStyle(headerStyleRight);
				primeiraLinhaCell.setCellValue((int) totalPrejuMesPF);

				cellNum++;
			}
			for (int k = 0; k < objRsRelatHeaderPJ.size(); k++) {
				for (int d = 0; d < objRsRelat.size(); d++) {

					if (objRsRelat.get(d).getDT_MM_YYYY().equals(objRsRelatData.get(i).getDT_MM_YYYY())
							&& objRsRelat.get(d).getNM_SEGM().equals(objRsRelatHeaderPJ.get(k).getNM_SEGM())) {

						XSSFCell primeiraLinhaCell = primeiraLinha.getCell(cellNum);
						if (primeiraLinhaCell == null) {
							primeiraLinhaCell = primeiraLinha.createCell(cellNum);
						}
						primeiraLinhaCell.setCellStyle(headerStyleRight);
						primeiraLinhaCell.setCellValue(Integer.valueOf(objRsRelat.get(d).getQNTD()));
						cellNum++;
						totalPrejuMesPJ = totalPrejuMesPJ + Double.valueOf(objRsRelat.get(d).getQNTD());
					}
				}
			}
			if (!objRsRelatHeaderPJ.isEmpty() && objRsRelatHeaderPJ != null) {
				XSSFCell primeiraLinhaCell = primeiraLinha.getCell(cellNum);
				if (primeiraLinhaCell == null) {
					primeiraLinhaCell = primeiraLinha.createCell(cellNum);
				}
				primeiraLinhaCell.setCellStyle(headerStyleRight);
				primeiraLinhaCell.setCellValue((int) totalPrejuMesPJ);
				cellNum++;
			}

		}

		XSSFCell primeiraLinhaCell = primeiraLinha.getCell(cellNum);
		if (primeiraLinhaCell == null) {
			primeiraLinhaCell = primeiraLinha.createCell(cellNum);
		}
		primeiraLinhaCell.setCellStyle(headerStyleRight);
		primeiraLinhaCell.setCellValue((int) (totalPrejuMesPJ + totalPrejuMesPF));
	}

	public void montarTotalTabelaQuantidade(int linha, XSSFSheet sheet, XSSFWorkbook wb) {
		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_RIGHT);
		headerStyle.setWrapText(true);
		headerStyle.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyle.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyle.setBorderRight(CellStyle.BORDER_THIN);
		headerStyle.setBorderTop(CellStyle.BORDER_THIN);
		headerStyle.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font fontCenter = wb.createFont();
		fontCenter.setFontHeightInPoints((short) 11);
		fontCenter.setFontName("Calibri");
		fontCenter.setColor(IndexedColors.WHITE.getIndex());
		fontCenter.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(fontCenter);

		XSSFCellStyle headerStyleLeft = wb.createCellStyle();
		headerStyleLeft.setAlignment(CellStyle.ALIGN_LEFT);
		headerStyleLeft.setWrapText(true);
		headerStyleLeft.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderTop(CellStyle.BORDER_THIN);
		headerStyleLeft.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		headerStyleLeft.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.WHITE.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyleLeft.setFont(font);

		XSSFRow primeiraLinha = sheet.getRow(linha);
		if (primeiraLinha == null) {
			primeiraLinha = sheet.createRow(linha);
		}
		primeiraLinha.setHeightInPoints((short) 18);

		int cellNum = 0;
		XSSFCell linhaCell = primeiraLinha.getCell(cellNum);
		if (linhaCell == null) {
			linhaCell = primeiraLinha.createCell(cellNum);
		}
		linhaCell.setCellStyle(headerStyleLeft);
		linhaCell.setCellValue("Total:");
		cellNum++;

		NumberFormat formatacao = NumberFormat.getCurrencyInstance(Locale.getDefault());

		for (int i = 0; i < objRsRelatHeaderPF.size(); i++) {
			int total = 0;
			for (int j = 0; j < objRsRelat.size(); j++) {
				if (objRsRelat.get(j).getNM_SEGM().equals(objRsRelatHeaderPF.get(i).getNM_SEGM())) {
					total = total + Integer.valueOf(objRsRelat.get(i).getQNTD());
				}
			}
			totalTpCliPf = totalTpCliPf + total;
			XSSFCell primeiraLinhaCell = primeiraLinha.getCell(cellNum);
			if (primeiraLinhaCell == null) {
				primeiraLinhaCell = primeiraLinha.createCell(cellNum);
			}
			primeiraLinhaCell.setCellStyle(headerStyle);
			primeiraLinhaCell.setCellValue(total);

			cellNum++;
		}
		if (!objRsRelatHeaderPF.isEmpty() && objRsRelatHeaderPF != null) {
			XSSFCell primeiraLinhaCell = primeiraLinha.getCell(cellNum);
			if (primeiraLinhaCell == null) {
				primeiraLinhaCell = primeiraLinha.createCell(cellNum);
			}
			primeiraLinhaCell.setCellStyle(headerStyle);
			primeiraLinhaCell.setCellValue((int) totalTpCliPf);

			cellNum++;
		}

		for (int i = 0; i < objRsRelatHeaderPJ.size(); i++) {
			int total = 0;
			for (int j = 0; j < objRsRelat.size(); j++) {
				if (objRsRelat.get(j).getNM_SEGM().equals(objRsRelatHeaderPJ.get(i).getNM_SEGM())) {
					total = total + Integer.valueOf(objRsRelat.get(i).getQNTD());
				}
			}
			totalTpCliPj = totalTpCliPj + total;
			XSSFCell primeiraLinhaCell = primeiraLinha.getCell(cellNum);
			if (primeiraLinhaCell == null) {
				primeiraLinhaCell = primeiraLinha.createCell(cellNum);
			}
			primeiraLinhaCell.setCellStyle(headerStyle);
			primeiraLinhaCell.setCellValue(total);
			cellNum++;
		}

		if (!objRsRelatHeaderPJ.isEmpty() && objRsRelatHeaderPJ != null) {
			XSSFCell primeiraLinhaCell = primeiraLinha.getCell(cellNum);
			if (primeiraLinhaCell == null) {
				primeiraLinhaCell = primeiraLinha.createCell(cellNum);
			}
			primeiraLinhaCell.setCellStyle(headerStyle);
			primeiraLinhaCell.setCellValue((int) totalTpCliPj);
			cellNum++;
		}
		XSSFCell primeiraLinhaCell = primeiraLinha.getCell(cellNum);
		if (primeiraLinhaCell == null) {
			primeiraLinhaCell = primeiraLinha.createCell(cellNum);
		}
		primeiraLinhaCell.setCellStyle(headerStyle);
		primeiraLinhaCell.setCellValue((int) (totalTpCliPj + totalTpCliPf));

		cellNum++;

	}

	public void montarPorcentagemTabelaQuantidade(int linha, XSSFSheet sheet, XSSFWorkbook wb) {

		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_RIGHT);
		headerStyle.setWrapText(true);
		headerStyle.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyle.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyle.setBorderRight(CellStyle.BORDER_THIN);
		headerStyle.setBorderTop(CellStyle.BORDER_THIN);
		headerStyle.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font fontCenter = wb.createFont();
		fontCenter.setFontHeightInPoints((short) 11);
		fontCenter.setFontName("Calibri");
		fontCenter.setColor(IndexedColors.WHITE.getIndex());
		fontCenter.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(fontCenter);

		XSSFCellStyle headerStyleLeft = wb.createCellStyle();
		headerStyleLeft.setAlignment(CellStyle.ALIGN_LEFT);
		headerStyleLeft.setWrapText(true);
		headerStyleLeft.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderTop(CellStyle.BORDER_THIN);
		headerStyleLeft.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		headerStyleLeft.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.WHITE.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyleLeft.setFont(font);

		NumberFormat formatacao = NumberFormat.getCurrencyInstance(Locale.getDefault());

		XSSFRow primeiraLinha = sheet.getRow(linha);
		if (primeiraLinha == null) {
			primeiraLinha = sheet.createRow(linha);
		}
		primeiraLinha.setHeightInPoints((short) 18);

		int cellNum = 0;
		XSSFCell linhaCell = primeiraLinha.getCell(cellNum);
		if (linhaCell == null) {
			linhaCell = primeiraLinha.createCell(cellNum);
		}
		linhaCell.setCellStyle(headerStyleLeft);
		linhaCell.setCellValue("Porcentagem:");
		cellNum++;
		double conta = 0;
		for (int i = 0; i < objRsRelatHeaderPF.size(); i++) {
			double total = 0;
			for (int j = 0; j < objRsRelat.size(); j++) {
				if (objRsRelat.get(j).getNM_SEGM().equals(objRsRelatHeaderPF.get(i).getNM_SEGM())) {
					total = total + Double.valueOf(objRsRelat.get(j).getQNTD());
				}
			}
			XSSFCell primeiraLinhaCell = primeiraLinha.getCell(cellNum);
			if (primeiraLinhaCell == null) {
				primeiraLinhaCell = primeiraLinha.createCell(cellNum);
			}

			if (total == 0 && totalTpCliPf == 0) {
				conta = 0;
				primeiraLinhaCell.setCellValue("0%");
			} else {
				conta = (total / totalTpCliPf) * 100;
				primeiraLinhaCell.setCellValue(NumberFormat.getCurrencyInstance().format(conta)
						.replace(formatacao.getCurrency().getSymbol(), "") + "%");
			}
			primeiraLinhaCell.setCellStyle(headerStyle);

			cellNum++;
		}
		if (!objRsRelatHeaderPF.isEmpty() && objRsRelatHeaderPF != null) {
			XSSFCell primeiraLinhaCell = primeiraLinha.getCell(cellNum);
			if (primeiraLinhaCell == null) {
				primeiraLinhaCell = primeiraLinha.createCell(cellNum);
			}
			if (totalTpCliPf == 0) {
				conta = 0;
				primeiraLinhaCell.setCellValue("0%");
			} else {
				conta = (totalTpCliPf / totalTpCliPf) * 100;
				primeiraLinhaCell.setCellValue(NumberFormat.getCurrencyInstance().format(conta)
						.replace(formatacao.getCurrency().getSymbol(), "") + "%");
			}
			primeiraLinhaCell.setCellStyle(headerStyle);

			cellNum++;
		}

		for (int i = 0; i < objRsRelatHeaderPJ.size(); i++) {
			double total = 0;
			for (int j = 0; j < objRsRelat.size(); j++) {
				if (objRsRelat.get(j).getNM_SEGM().equals(objRsRelatHeaderPJ.get(i).getNM_SEGM())) {
					total = total + Double.valueOf(objRsRelat.get(j).getQNTD());
				}
			}
			XSSFCell primeiraLinhaCell = primeiraLinha.getCell(cellNum);
			if (primeiraLinhaCell == null) {
				primeiraLinhaCell = primeiraLinha.createCell(cellNum);
			}
			if (total == 0 && totalTpCliPj == 0) {
				conta = 0;
				primeiraLinhaCell.setCellValue("0%");
			} else {
				conta = (total / totalTpCliPj) * 100;
				primeiraLinhaCell.setCellValue(NumberFormat.getCurrencyInstance().format(conta)
						.replace(formatacao.getCurrency().getSymbol(), "") + "%");
			}
			primeiraLinhaCell.setCellStyle(headerStyle);

			cellNum++;
		}

		if (!objRsRelatHeaderPJ.isEmpty() && objRsRelatHeaderPJ != null) {
			XSSFCell primeiraLinhaCell = primeiraLinha.getCell(cellNum);
			if (primeiraLinhaCell == null) {
				primeiraLinhaCell = primeiraLinha.createCell(cellNum);
			}
			if (totalTpCliPj == 0) {
				conta = 0;
				primeiraLinhaCell.setCellValue("0%");
			} else {
				conta = (totalTpCliPj / totalTpCliPj) * 100;
				primeiraLinhaCell.setCellValue(NumberFormat.getCurrencyInstance().format(conta)
						.replace(formatacao.getCurrency().getSymbol(), "") + "%");
			}
			primeiraLinhaCell.setCellStyle(headerStyle);

			cellNum++;
		}

		XSSFCell primeiraLinhaCell = primeiraLinha.getCell(cellNum);
		if (primeiraLinhaCell == null) {
			primeiraLinhaCell = primeiraLinha.createCell(cellNum);
		}
		primeiraLinhaCell.setCellStyle(headerStyle);

		if (totalTpCliPj == 0 && totalTpCliPf == 0) {
			conta = 0;
			primeiraLinhaCell.setCellValue("0%");
		} else {
			conta = ((totalTpCliPj + totalTpCliPf) / (totalTpCliPj + totalTpCliPf)) * 100;

			primeiraLinhaCell.setCellValue(
					NumberFormat.getCurrencyInstance().format(conta).replace(formatacao.getCurrency().getSymbol(), "")
							+ "%");
		}

	}

	public void montarCabecalhoFraudesCSegmento(XSSFSheet sheet, XSSFWorkbook wb) {

		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setWrapText(true);
		headerStyle.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyle.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyle.setBorderRight(CellStyle.BORDER_THIN);
		headerStyle.setBorderTop(CellStyle.BORDER_THIN);
		headerStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font fontCenter = wb.createFont();
		fontCenter.setFontHeightInPoints((short) 11);
		fontCenter.setFontName("Calibri");
		fontCenter.setColor(IndexedColors.WHITE.getIndex());
		fontCenter.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(fontCenter);

		XSSFRow tituloLinha = sheet.getRow(linhaAtual);
		if (tituloLinha == null) {
			tituloLinha = sheet.createRow(linhaAtual);
		}
		XSSFCell cell1 = tituloLinha.createCell(0);
		cell1.setCellValue("Segmento");
		cell1.setCellStyle(headerStyle);

		XSSFCell cell2 = tituloLinha.createCell(1);
		cell2.setCellValue("Quantidade");
		cell2.setCellStyle(headerStyle);

		XSSFCell cell3 = tituloLinha.createCell(2);
		cell3.setCellValue("Prejuízo");
		cell3.setCellStyle(headerStyle);

		XSSFCell cell4 = tituloLinha.createCell(3);
		cell4.setCellValue("%");
		cell4.setCellStyle(headerStyle);
	}

	public void montarCorpoFraudesCSegmento(XSSFSheet sheet, XSSFWorkbook wb, String data) {

		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_RIGHT);
		headerStyle.setWrapText(true);
		headerStyle.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyle.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyle.setBorderRight(CellStyle.BORDER_THIN);
		headerStyle.setBorderTop(CellStyle.BORDER_THIN);
		Font fontCenter = wb.createFont();
		fontCenter.setFontHeightInPoints((short) 11);
		fontCenter.setFontName("Calibri");
		headerStyle.setFont(fontCenter);

		XSSFCellStyle headerStyleLeft = wb.createCellStyle();
		headerStyleLeft.setAlignment(CellStyle.ALIGN_LEFT);
		headerStyleLeft.setWrapText(true);
		headerStyleLeft.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderTop(CellStyle.BORDER_THIN);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		headerStyleLeft.setFont(font);

		XSSFCellStyle headerStyleGrey = wb.createCellStyle();
		headerStyleGrey.setAlignment(CellStyle.ALIGN_RIGHT);
		headerStyleGrey.setWrapText(true);
		headerStyleGrey.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleGrey.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleGrey.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleGrey.setBorderTop(CellStyle.BORDER_THIN);
		headerStyleGrey.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		headerStyleGrey.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font fontCenterGrey = wb.createFont();
		fontCenterGrey.setFontHeightInPoints((short) 11);
		fontCenterGrey.setFontName("Calibri");
		fontCenterGrey.setColor(IndexedColors.WHITE.getIndex());
		fontCenterGrey.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyleGrey.setFont(fontCenterGrey);

		XSSFCellStyle headerStyleGreyLeft = wb.createCellStyle();
		headerStyleGreyLeft.setAlignment(CellStyle.ALIGN_LEFT);
		headerStyleGreyLeft.setWrapText(true);
		headerStyleGreyLeft.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleGreyLeft.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleGreyLeft.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleGreyLeft.setBorderTop(CellStyle.BORDER_THIN);
		headerStyleGreyLeft.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		headerStyleGreyLeft.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font fontGrey = wb.createFont();
		fontGrey.setFontHeightInPoints((short) 11);
		fontGrey.setFontName("Calibri");
		fontGrey.setColor(IndexedColors.WHITE.getIndex());
		fontGrey.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyleGreyLeft.setFont(fontGrey);

		int totalQtd = 0;
		double totalPrej = 0;

		NumberFormat formatacao = NumberFormat.getCurrencyInstance(Locale.getDefault());

		for (int i = 0; i < objRsRelatHeaderPF.size(); i++) {
			for (int j = 0; j < objRsRelat.size(); j++) {
				if (objRsRelat.get(j).getTP_PESS_CLIE().equals("1") && objRsRelat.get(j).getDT_MM_YYYY().equals(data)
						&& objRsRelatHeaderPF.get(i).getNM_SEGM().equals(objRsRelat.get(j).getNM_SEGM())) {
					XSSFRow linhaTabela = sheet.getRow(linhaAtual);
					if (linhaTabela == null) {
						linhaTabela = sheet.createRow(linhaAtual);
					}
					XSSFCell cell = linhaTabela.createCell(0);
					cell.setCellValue(objRsRelat.get(j).getNM_SEGM());
					cell.setCellStyle(headerStyleLeft);

					XSSFCell cell2 = linhaTabela.createCell(1);
					cell2.setCellValue(objRsRelat.get(j).getQNTD());
					cell2.setCellStyle(headerStyle);

					XSSFCell cell3 = linhaTabela.createCell(2);
					if (Double.valueOf(objRsRelat.get(j).getPREJUIZO()) == 0) {
						cell3.setCellValue(0);
					} else {
						cell3.setCellValue(NumberFormat.getCurrencyInstance()
								.format(Double.valueOf(objRsRelat.get(j).getPREJUIZO()))
								.replace(formatacao.getCurrency().getSymbol(), ""));
					}
					totalQtd = totalQtd + Integer.valueOf(objRsRelat.get(j).getQNTD());
					totalPrej = totalPrej + Double.valueOf(objRsRelat.get(j).getPREJUIZO());

					cell3.setCellStyle(headerStyle);

					XSSFCell cell4PF = linhaTabela.createCell(3);
					cell4PF.setCellValue(" ");
					cell4PF.setCellStyle(headerStyleLeft);
					linhaAtual++;
				}
			}
		}
		XSSFRow linhaAtualBranca = sheet.getRow(linhaAtual);
		if (linhaAtualBranca == null) {
			linhaAtualBranca = sheet.createRow(linhaAtual);
		}

		XSSFCell linhaAtualBrancaCell = linhaAtualBranca.getCell(0);
		if (linhaAtualBrancaCell == null) {
			linhaAtualBrancaCell = linhaAtualBranca.createCell(0);
		}
		linhaAtualBrancaCell.setCellStyle(headerStyleLeft);


		XSSFCell linhaAtualBrancaCell2 = linhaAtualBranca.getCell(1);
		if (linhaAtualBrancaCell2 == null) {
			linhaAtualBrancaCell2 = linhaAtualBranca.createCell(1);
		}
		linhaAtualBrancaCell2.setCellStyle(headerStyle);
	
		XSSFCell linhaAtualBrancaCell3 = linhaAtualBranca.getCell(2);
		if (linhaAtualBrancaCell3 == null) {
			linhaAtualBrancaCell3 = linhaAtualBranca.createCell(2);
		}
		linhaAtualBrancaCell3.setCellStyle(headerStyle);

		XSSFCell linhaAtualBrancaCell4 = linhaAtualBranca.getCell(3);
		if (linhaAtualBrancaCell4 == null) {
			linhaAtualBrancaCell4 = linhaAtualBranca.createCell(3);
		}
		linhaAtualBrancaCell4.setCellStyle(headerStyle);
		linhaAtual += 1;
		
		if (!objRsRelatHeaderPF.isEmpty() && objRsRelatHeaderPF != null) {

			XSSFRow linhaTabelaTotalPF = sheet.getRow(linhaAtual);
			if (linhaTabelaTotalPF == null) {
				linhaTabelaTotalPF = sheet.createRow(linhaAtual);
			}
			XSSFCell cellPF = linhaTabelaTotalPF.createCell(0);
			cellPF.setCellValue("Total PF");
			cellPF.setCellStyle(headerStyleGreyLeft);

			XSSFCell cell2PF = linhaTabelaTotalPF.createCell(1);
			cell2PF.setCellValue(totalQtd);
			cell2PF.setCellStyle(headerStyleGrey);

			XSSFCell cell3PF = linhaTabelaTotalPF.createCell(2);
			if (totalPrej == 0) {
				cell3PF.setCellValue(0);
			} else {
				cell3PF.setCellValue(NumberFormat.getCurrencyInstance().format(totalPrej)
						.replace(formatacao.getCurrency().getSymbol(), ""));
			}
			cell3PF.setCellStyle(headerStyleGrey);

			XSSFCell cell4PF = linhaTabelaTotalPF.createCell(3);
			cell4PF.setCellValue(" ");
			cell4PF.setCellStyle(headerStyleGrey);

			linhaAtual++;
		}

		totalQtd = 0;
		totalPrej = 0;

		for (int i = 0; i < objRsRelatHeaderPJ.size(); i++) {
			for (int j = 0; j < objRsRelat.size(); j++) {
				if (objRsRelat.get(j).getTP_PESS_CLIE().equals("2") && objRsRelat.get(j).getDT_MM_YYYY().equals(data)
						&& objRsRelatHeaderPJ.get(i).getNM_SEGM().equals(objRsRelat.get(j).getNM_SEGM())) {

					XSSFRow linhaTabela = sheet.getRow(linhaAtual);
					if (linhaTabela == null) {
						linhaTabela = sheet.createRow(linhaAtual);
					}
					XSSFCell cell = linhaTabela.createCell(0);
					cell.setCellValue(objRsRelat.get(j).getNM_SEGM());
					cell.setCellStyle(headerStyleLeft);

					XSSFCell cell2 = linhaTabela.createCell(1);
					cell2.setCellValue(objRsRelat.get(j).getQNTD());
					cell2.setCellStyle(headerStyle);

					XSSFCell cell3 = linhaTabela.createCell(2);
					if (Double.valueOf(objRsRelat.get(j).getPREJUIZO()) == 0) {
						cell3.setCellValue(0);
					} else {
						cell3.setCellValue(NumberFormat.getCurrencyInstance()
								.format(Double.valueOf(objRsRelat.get(j).getPREJUIZO()))
								.replace(formatacao.getCurrency().getSymbol(), ""));
					}
					totalQtd = totalQtd + Integer.valueOf(objRsRelat.get(j).getQNTD());
					totalPrej = totalPrej + Double.valueOf(objRsRelat.get(j).getPREJUIZO());

					cell3.setCellStyle(headerStyle);

					XSSFCell cell4 = linhaTabela.createCell(3);
					cell4.setCellValue(" ");
					cell4.setCellStyle(headerStyleLeft);
					linhaAtual++;
				}
			}
		}
		if (!objRsRelatHeaderPJ.isEmpty() && objRsRelatHeaderPJ != null) {
			XSSFRow linhaTabelaTotalPJ = sheet.getRow(linhaAtual);
			if (linhaTabelaTotalPJ == null) {
				linhaTabelaTotalPJ = sheet.createRow(linhaAtual);
			}
			XSSFCell cellPJ = linhaTabelaTotalPJ.createCell(0);
			cellPJ.setCellValue("Total PJ");
			cellPJ.setCellStyle(headerStyleGreyLeft);

			XSSFCell cell2PJ = linhaTabelaTotalPJ.createCell(1);
			cell2PJ.setCellValue(totalQtd);
			cell2PJ.setCellStyle(headerStyleGrey);

			XSSFCell cell3PJ = linhaTabelaTotalPJ.createCell(2);
			if (totalPrej == 0) {
				cell3PJ.setCellValue(0);
			} else {
				cell3PJ.setCellValue(NumberFormat.getCurrencyInstance().format(totalPrej)
						.replace(formatacao.getCurrency().getSymbol(), ""));
			}
			cell3PJ.setCellStyle(headerStyleGrey);

			XSSFCell cell4PJ = linhaTabelaTotalPJ.createCell(3);
			cell4PJ.setCellValue(" ");
			cell4PJ.setCellStyle(headerStyleGrey);
			linhaAtual++;
		}
	}

	public void montarCorpoFraudeGeral(int linha, XSSFSheet sheet, XSSFWorkbook wb) {

		NumberFormat formatacao = NumberFormat.getCurrencyInstance(Locale.getDefault());
		XSSFCellStyle headerStyleLeft = wb.createCellStyle();
		headerStyleLeft.setAlignment(CellStyle.ALIGN_LEFT);
		headerStyleLeft.setWrapText(true);
		headerStyleLeft.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderTop(CellStyle.BORDER_THIN);
		Font fontLeft = wb.createFont();
		fontLeft.setFontHeightInPoints((short) 11);
		fontLeft.setFontName("Calibri");
		headerStyleLeft.setFont(fontLeft);

		XSSFCellStyle headerStyleRight = wb.createCellStyle();
		headerStyleRight.setAlignment(CellStyle.ALIGN_RIGHT);
		headerStyleRight.setWrapText(true);
		headerStyleRight.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderTop(CellStyle.BORDER_THIN);
		Font fontRight = wb.createFont();
		fontRight.setFontHeightInPoints((short) 11);
		fontRight.setFontName("Calibri");
		headerStyleRight.setFont(fontLeft);

		XSSFCellStyle headerStyleGrey = wb.createCellStyle();
		headerStyleGrey.setAlignment(CellStyle.ALIGN_RIGHT);
		headerStyleGrey.setWrapText(true);
		headerStyleGrey.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleGrey.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleGrey.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleGrey.setBorderTop(CellStyle.BORDER_THIN);
		headerStyleGrey.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		headerStyleGrey.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font fontCenter = wb.createFont();
		fontCenter.setFontHeightInPoints((short) 11);
		fontCenter.setFontName("Calibri");
		fontCenter.setColor(IndexedColors.WHITE.getIndex());
		fontCenter.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyleGrey.setFont(fontCenter);

		XSSFCellStyle headerStyleGreyLeft = wb.createCellStyle();
		headerStyleGreyLeft.setAlignment(CellStyle.ALIGN_LEFT);
		headerStyleGreyLeft.setWrapText(true);
		headerStyleGreyLeft.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleGreyLeft.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleGreyLeft.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleGreyLeft.setBorderTop(CellStyle.BORDER_THIN);
		headerStyleGreyLeft.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		headerStyleGreyLeft.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.WHITE.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyleGreyLeft.setFont(font);

		int cellNum = 0;

		int totalQtdPF = 0;
		double totalPrejPF = 0;

		int totalQtdPJ = 0;
		double totalPrejPJ = 0;

		for (int i = 0; i < objRsRelatHeaderPF.size(); i++) {
			cellNum = 0;
			for (int j = 0; j < objRsRelat.size(); j++) {
				cellNum = 0;
				if (objRsRelat.get(j).getTP_PESS_CLIE().equals("1")
						&& objRsRelat.get(j).getNM_SEGM().equals(objRsRelatHeaderPF.get(i).getNM_SEGM())) {
					XSSFRow primeiraLinha = sheet.getRow(linha);
					if (primeiraLinha == null) {
						primeiraLinha = sheet.createRow(linha);
					}
					linha += 1;
					primeiraLinha.setHeightInPoints((short) 18);

					XSSFCell primeiraLinhaCell = primeiraLinha.getCell(cellNum);
					if (primeiraLinhaCell == null) {
						primeiraLinhaCell = primeiraLinha.createCell(cellNum);
					}
					primeiraLinhaCell.setCellStyle(headerStyleLeft);
					primeiraLinhaCell.setCellValue(objRsRelat.get(j).getNM_SEGM());
					cellNum++;

					XSSFCell primeiraLinhaCell2 = primeiraLinha.getCell(cellNum);
					if (primeiraLinhaCell2 == null) {
						primeiraLinhaCell2 = primeiraLinha.createCell(cellNum);
					}
					primeiraLinhaCell2.setCellStyle(headerStyleRight);
					primeiraLinhaCell2.setCellValue(objRsRelat.get(j).getQNTD());
					cellNum++;

					XSSFCell primeiraLinhaCell3 = primeiraLinha.getCell(cellNum);
					if (primeiraLinhaCell3 == null) {
						primeiraLinhaCell3 = primeiraLinha.createCell(cellNum);
					}
					primeiraLinhaCell3.setCellStyle(headerStyleRight);

					if (Double.valueOf(objRsRelat.get(j).getPREJUIZO()) == 0) {
						primeiraLinhaCell3.setCellValue(0);
					} else {
						primeiraLinhaCell3.setCellValue(NumberFormat.getCurrencyInstance()
								.format(Double.valueOf(objRsRelat.get(j).getPREJUIZO()))
								.replace(formatacao.getCurrency().getSymbol(), ""));
					}
					totalQtdPF = totalQtdPF + Integer.valueOf(objRsRelat.get(j).getQNTD());
					totalPrejPF = totalPrejPF + Double.valueOf(objRsRelat.get(j).getPREJUIZO());
					cellNum++;

					XSSFCell primeiraLinhaCell4 = primeiraLinha.getCell(cellNum);
					if (primeiraLinhaCell4 == null) {
						primeiraLinhaCell4 = primeiraLinha.createCell(cellNum);
					}
					primeiraLinhaCell4.setCellStyle(headerStyleRight);
					primeiraLinhaCell4.setCellValue("");
					cellNum++;
				}
			}
		}


		if (!objRsRelatHeaderPF.isEmpty() && objRsRelatHeaderPF != null) {

			XSSFRow primeiraLinha = sheet.getRow(linha);
			if (primeiraLinha == null) {
				primeiraLinha = sheet.createRow(linha);
			}
			linha += 1;
			cellNum = 0;
			XSSFCell primeiraLinhaCellPF = primeiraLinha.getCell(cellNum);
			if (primeiraLinhaCellPF == null) {
				primeiraLinhaCellPF = primeiraLinha.createCell(cellNum);
			}
			primeiraLinhaCellPF.setCellStyle(headerStyleGreyLeft);
			primeiraLinhaCellPF.setCellValue("Total PF");

			cellNum++;

			XSSFCell primeiraLinhaCellPF2 = primeiraLinha.getCell(cellNum);
			if (primeiraLinhaCellPF2 == null) {
				primeiraLinhaCellPF2 = primeiraLinha.createCell(cellNum);
			}
			primeiraLinhaCellPF2.setCellStyle(headerStyleGrey);
			primeiraLinhaCellPF2.setCellValue(totalQtdPF);

			cellNum++;
			XSSFCell primeiraLinhaCellPF3 = primeiraLinha.getCell(cellNum);
			if (primeiraLinhaCellPF3 == null) {
				primeiraLinhaCellPF3 = primeiraLinha.createCell(cellNum);
			}
			if (totalPrejPF == 0) {
				primeiraLinhaCellPF3.setCellValue(0);
			} else {
				primeiraLinhaCellPF3.setCellValue(NumberFormat.getCurrencyInstance().format(totalPrejPF)
						.replace(formatacao.getCurrency().getSymbol(), ""));
			}
			primeiraLinhaCellPF3.setCellStyle(headerStyleGrey);

			cellNum++;
			XSSFCell primeiraLinhaCellPF4 = primeiraLinha.getCell(cellNum);
			if (primeiraLinhaCellPF4 == null) {
				primeiraLinhaCellPF4 = primeiraLinha.createCell(cellNum);
			}
			primeiraLinhaCellPF4.setCellStyle(headerStyleGrey);
			primeiraLinhaCellPF4.setCellValue(" ");
		}
		cellNum = 0;

		for (int i = 0; i < objRsRelatHeaderPJ.size(); i++) {
			cellNum = 0;
			for (int j = 0; j < objRsRelat.size(); j++) {
				if (objRsRelat.get(j).getTP_PESS_CLIE().equals("2")
						&& objRsRelat.get(j).getNM_SEGM().equals(objRsRelatHeaderPJ.get(i).getNM_SEGM())) {
					XSSFRow primeiraLinhaPJ = sheet.getRow(linha);
					if (primeiraLinhaPJ == null) {
						primeiraLinhaPJ = sheet.createRow(linha);
					}
					linha += 1;
					primeiraLinhaPJ.setHeightInPoints((short) 18);

					XSSFCell primeiraLinhaPJCell = primeiraLinhaPJ.getCell(cellNum);
					if (primeiraLinhaPJCell == null) {
						primeiraLinhaPJCell = primeiraLinhaPJ.createCell(cellNum);
					}
					primeiraLinhaPJCell.setCellStyle(headerStyleLeft);
					primeiraLinhaPJCell.setCellValue(objRsRelat.get(j).getNM_SEGM());
					cellNum++;
					XSSFCell primeiraLinhaPJCell2 = primeiraLinhaPJ.getCell(cellNum);
					if (primeiraLinhaPJCell2 == null) {
						primeiraLinhaPJCell2 = primeiraLinhaPJ.createCell(cellNum);
					}
					primeiraLinhaPJCell2.setCellStyle(headerStyleRight);
					primeiraLinhaPJCell2.setCellValue(objRsRelat.get(j).getQNTD());
					cellNum++;
					XSSFCell primeiraLinhaPJCell3 = primeiraLinhaPJ.getCell(cellNum);
					if (primeiraLinhaPJCell3 == null) {
						primeiraLinhaPJCell3 = primeiraLinhaPJ.createCell(cellNum);
					}
					primeiraLinhaPJCell3.setCellStyle(headerStyleRight);

					if (Double.valueOf(objRsRelat.get(j).getPREJUIZO()) == 0) {
						primeiraLinhaPJCell3.setCellValue(0);
					} else {
						primeiraLinhaPJCell3.setCellValue(NumberFormat.getCurrencyInstance()
								.format(Double.valueOf(objRsRelat.get(j).getPREJUIZO()))
								.replace(formatacao.getCurrency().getSymbol(), ""));
					}
					totalQtdPJ = totalQtdPJ + Integer.valueOf(objRsRelat.get(j).getQNTD());
					totalPrejPJ = totalPrejPJ + Double.valueOf(objRsRelat.get(j).getPREJUIZO());
					cellNum++;

					XSSFCell primeiraLinhaPJCell4 = primeiraLinhaPJ.getCell(cellNum);
					if (primeiraLinhaPJCell4 == null) {
						primeiraLinhaPJCell4 = primeiraLinhaPJ.createCell(cellNum);
					}
					primeiraLinhaPJCell4.setCellStyle(headerStyleRight);
					primeiraLinhaPJCell4.setCellValue(" ");
					cellNum++;
				}
			}
		}
		cellNum = 0;
		if (!objRsRelatHeaderPJ.isEmpty() && objRsRelatHeaderPJ != null) {
			XSSFRow primeiraLinhaPJ = sheet.getRow(linha);
			if (primeiraLinhaPJ == null) {
				primeiraLinhaPJ = sheet.createRow(linha);
			}
			linha += 1;

			XSSFCell primeiraLinhaPJCellPJ = primeiraLinhaPJ.getCell(cellNum);
			if (primeiraLinhaPJCellPJ == null) {
				primeiraLinhaPJCellPJ = primeiraLinhaPJ.createCell(cellNum);
			}
			primeiraLinhaPJCellPJ.setCellStyle(headerStyleGreyLeft);
			primeiraLinhaPJCellPJ.setCellValue("Total PJ");

			cellNum++;

			XSSFCell primeiraLinhaPJCellPJ2 = primeiraLinhaPJ.getCell(cellNum);
			if (primeiraLinhaPJCellPJ2 == null) {
				primeiraLinhaPJCellPJ2 = primeiraLinhaPJ.createCell(cellNum);
			}
			primeiraLinhaPJCellPJ2.setCellStyle(headerStyleGrey);
			primeiraLinhaPJCellPJ2.setCellValue(totalQtdPJ);

			cellNum++;
			XSSFCell primeiraLinhaPJCellPJ3 = primeiraLinhaPJ.getCell(cellNum);
			if (primeiraLinhaPJCellPJ3 == null) {
				primeiraLinhaPJCellPJ3 = primeiraLinhaPJ.createCell(cellNum);
			}
			if (totalPrejPJ == 0) {
				primeiraLinhaPJCellPJ3.setCellValue(0);
			} else {
				primeiraLinhaPJCellPJ3.setCellValue(NumberFormat.getCurrencyInstance().format(totalPrejPJ)
						.replace(formatacao.getCurrency().getSymbol(), ""));
			}
			primeiraLinhaPJCellPJ3.setCellStyle(headerStyleGrey);
			cellNum++;
			XSSFCell primeiraLinhaPJCellPJ4 = primeiraLinhaPJ.getCell(cellNum);
			if (primeiraLinhaPJCellPJ4 == null) {
				primeiraLinhaPJCellPJ4 = primeiraLinhaPJ.createCell(cellNum);
			}
			primeiraLinhaPJCellPJ4.setCellStyle(headerStyleGrey);
			primeiraLinhaPJCellPJ4.setCellValue("");
			cellNum++;
			
			
			
			//total geral
			
			cellNum = 0;
			XSSFRow linhaTotalGeral = sheet.getRow(linha);
			if (linhaTotalGeral == null) {
				linhaTotalGeral = sheet.createRow(linha);
			}
			linha += 1;

			XSSFCell linhaCell1 = linhaTotalGeral.getCell(cellNum);
			if (linhaCell1 == null) {
				linhaCell1 = linhaTotalGeral.createCell(cellNum);
			}
			linhaCell1.setCellStyle(headerStyleGreyLeft);
			linhaCell1.setCellValue("Total Fraudes");

			cellNum++;

			XSSFCell linhaCell2 = linhaTotalGeral.getCell(cellNum);
			if (linhaCell2 == null) {
				linhaCell2 = linhaTotalGeral.createCell(cellNum);
			}
			linhaCell2.setCellStyle(headerStyleGrey);
			linhaCell2.setCellValue(totalQtdPF + totalQtdPJ);

			cellNum++;
			XSSFCell linhaCell3 = linhaTotalGeral.getCell(cellNum);
			if (linhaCell3 == null) {
				linhaCell3 = linhaTotalGeral.createCell(cellNum);
			}
			if (totalPrejPF + totalPrejPJ == 0) {
				linhaCell3.setCellValue(0);
			} else {
				linhaCell3.setCellValue(NumberFormat.getCurrencyInstance().format(totalPrejPF + totalPrejPJ)
						.replace(formatacao.getCurrency().getSymbol(), ""));
			}
			linhaCell3.setCellStyle(headerStyleGrey);
			cellNum++;
			XSSFCell linhaCell4 = linhaTotalGeral.getCell(cellNum);
			if (linhaCell4 == null) {
				linhaCell4 = linhaTotalGeral.createCell(cellNum);
			}
			linhaCell4.setCellStyle(headerStyleGrey);
			linhaCell4.setCellValue("");
			cellNum++;
		}
		
	}

	public void montarCabecalhoFraudeGeral(String titulo, int linha, XSSFSheet sheet, XSSFWorkbook wb) {

		XSSFCellStyle tituloStyle = wb.createCellStyle();
		tituloStyle.setAlignment(CellStyle.ALIGN_LEFT);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		tituloStyle.setFont(font);

		XSSFRow tituloTabela = sheet.getRow(linha);
		if (tituloTabela == null) {
			tituloTabela = sheet.createRow(linha);
		}
		XSSFCell cell = tituloTabela.createCell(0);
		cell.setCellValue(titulo);
		cell.setCellStyle(tituloStyle);

		CellRangeAddress regionTitulo = new CellRangeAddress(tituloTabela.getRowNum(), tituloTabela.getRowNum(), 0, 3);
		sheet.addMergedRegion(regionTitulo);
		linha += 1;

		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setWrapText(true);
		headerStyle.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyle.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyle.setBorderRight(CellStyle.BORDER_THIN);
		headerStyle.setBorderTop(CellStyle.BORDER_THIN);
		headerStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font fontCenter = wb.createFont();
		fontCenter.setFontHeightInPoints((short) 11);
		fontCenter.setFontName("Calibri");
		fontCenter.setColor(IndexedColors.WHITE.getIndex());
		fontCenter.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(fontCenter);

		XSSFRow tituloLinha = sheet.getRow(linha);
		if (tituloLinha == null) {
			tituloLinha = sheet.createRow(linha);
		}
		XSSFCell cell1 = tituloLinha.createCell(0);
		cell1.setCellValue("Segmento");
		cell1.setCellStyle(headerStyle);

		XSSFCell cell2 = tituloLinha.createCell(1);
		cell2.setCellValue("Quantidade");
		cell2.setCellStyle(headerStyle);

		XSSFCell cell3 = tituloLinha.createCell(2);
		cell3.setCellValue("Prejuízo");
		cell3.setCellStyle(headerStyle);

		XSSFCell cell4 = tituloLinha.createCell(3);
		cell4.setCellValue("%");
		cell4.setCellStyle(headerStyle);
	}

	public String now() {
		DateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		Date date = new Date();
		return dateformat.format(date);
	}

	public String getStrArea() {
		return strArea;
	}

	public void setStrArea(String strArea) {
		this.strArea = strArea;
	}

	public String getpCodArea() {
		return pCodArea;
	}

	public void setpCodArea(String pCodArea) {
		this.pCodArea = pCodArea;
	}

	public String getStrTpRel() {
		return strTpRel;
	}

	public void setStrTpRel(String strTpRel) {
		this.strTpRel = strTpRel;
	}

	public String getStrNmRelat() {
		return strNmRelat;
	}

	public void setStrNmRelat(String strNmRelat) {
		this.strNmRelat = strNmRelat;
	}

	public String getStrDtIni() {
		return strDtIni;
	}

	public void setStrDtIni(String strDtIni) {
		this.strDtIni = strDtIni;
	}

	public String getStrDtFim() {
		return strDtFim;
	}

	public void setStrDtFim(String strDtFim) {
		this.strDtFim = strDtFim;
	}

	public String getIntCanal() {
		return intCanal;
	}

	public void setIntCanal(String intCanal) {
		this.intCanal = intCanal;
	}

	public XHYRelatoriosEndPoint getObjRelat() {
		return objRelat;
	}

	public void setObjRelat(XHYRelatoriosEndPoint objRelat) {
		this.objRelat = objRelat;
	}

	public List<RelosEstatisticoSegmento> getObjRsRelat() {
		return objRsRelat;
	}

	public void setObjRsRelat(List<RelosEstatisticoSegmento> objRsRelat) {
		this.objRsRelat = objRsRelat;
	}

	public List<RelosEstatisticoSegmentoPessoa> getObjRsRelatHeaderPF() {
		return objRsRelatHeaderPF;
	}

	public void setObjRsRelatHeaderPF(List<RelosEstatisticoSegmentoPessoa> objRsRelatHeaderPF) {
		this.objRsRelatHeaderPF = objRsRelatHeaderPF;
	}

	public List<RelosEstatisticoSegmentoPessoa> getObjRsRelatHeaderPJ() {
		return objRsRelatHeaderPJ;
	}

	public void setObjRsRelatHeaderPJ(List<RelosEstatisticoSegmentoPessoa> objRsRelatHeaderPJ) {
		this.objRsRelatHeaderPJ = objRsRelatHeaderPJ;
	}

	public List<RelosEstatisticoSegmentoData> getObjRsRelatData() {
		return objRsRelatData;
	}

	public void setObjRsRelatData(List<RelosEstatisticoSegmentoData> objRsRelatData) {
		this.objRsRelatData = objRsRelatData;
	}

	public String getNOME_AREA_USUARIO() {
		return NOME_AREA_USUARIO;
	}

	public void setNOME_AREA_USUARIO(String nOME_AREA_USUARIO) {
		NOME_AREA_USUARIO = nOME_AREA_USUARIO;
	}

}
