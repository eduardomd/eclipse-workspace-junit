package com.altec.bsbr.app.hyb.web.util;

public class Restricao {
	
	private String SQ_RESTRICAO;
	private String INSTITUICAO;
	private String ESPECIFICACAO;
	private String DATA;
	private String VALOR;
	
	public Restricao() {
		
	}

	public String getSQ_RESTRICAO() {
		return SQ_RESTRICAO;
	}

	public void setSQ_RESTRICAO(String sQ_RESTRICAO) {
		SQ_RESTRICAO = sQ_RESTRICAO;
	}

	public String getINSTITUICAO() {
		return INSTITUICAO;
	}

	public void setINSTITUICAO(String iNSTITUICAO) {
		INSTITUICAO = iNSTITUICAO;
	}

	public String getESPECIFICACAO() {
		return ESPECIFICACAO;
	}

	public void setESPECIFICACAO(String eSPECIFICACAO) {
		ESPECIFICACAO = eSPECIFICACAO;
	}

	public String getDATA() {
		return DATA;
	}

	public void setDATA(String dATA) {
		DATA = dATA;
	}

	public String getVALOR() {
		return VALOR;
	}

	public void setVALOR(String vALOR) {
		VALOR = vALOR;
	}
}
