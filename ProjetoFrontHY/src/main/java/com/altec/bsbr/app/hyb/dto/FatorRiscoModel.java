package com.altec.bsbr.app.hyb.dto;

public class FatorRiscoModel {

	private String nomeFatorRisco;
	private String codigoFatorRisco;

	public FatorRiscoModel(String codigoFatorRisco, String nomeFatorRisco) {

		this.nomeFatorRisco = nomeFatorRisco;
		this.codigoFatorRisco = codigoFatorRisco;

	}

	public String getNomeFatorRisco() {
		return nomeFatorRisco;
	}

	public void setNomeFatorRisco(String nomeFatorRisco) {
		this.nomeFatorRisco = nomeFatorRisco;
	}

	public String getCodigoFatorRisco() {
		return codigoFatorRisco;
	}

	public void setCodigoFatorRisco(String codigoFatorRisco) {
		this.codigoFatorRisco = codigoFatorRisco;
	}
}