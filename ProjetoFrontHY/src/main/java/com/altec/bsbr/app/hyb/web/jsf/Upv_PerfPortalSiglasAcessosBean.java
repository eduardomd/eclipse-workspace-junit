package com.altec.bsbr.app.hyb.web.jsf;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import com.altec.bsbr.app.hyb.dto.Upv_PerfPortalSiglasAcessos;

@Component("upvBean")
@Scope("session")
public class Upv_PerfPortalSiglasAcessosBean extends BasicBBean {

	private Upv_PerfPortalSiglasAcessos upvMock;

	private static final long serialVersionUID = 1L;

	public Upv_PerfPortalSiglasAcessosBean() {
		Upv_PerfPortalSiglasAcessos upvMock = new Upv_PerfPortalSiglasAcessos();

		upvMock.setStrSiglaWindows("Sigla");
		upvMock.setStrStatus("Status");
		upvMock.setStrMatricula("Matricula");
		upvMock.setStrNome("Nome");
	}

	public Upv_PerfPortalSiglasAcessos getUpvMock() {
		return upvMock;
	}

	public void setUpvMock(Upv_PerfPortalSiglasAcessos upvMock) {
		this.upvMock = upvMock;
	}

}
