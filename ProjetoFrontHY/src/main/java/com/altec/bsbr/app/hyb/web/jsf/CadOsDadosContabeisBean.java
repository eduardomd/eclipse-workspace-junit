package com.altec.bsbr.app.hyb.web.jsf;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import com.altec.bsbr.app.hyb.dto.ObjCatgNv3;
import com.altec.bsbr.app.hyb.dto.ObjCntaContab;
import com.altec.bsbr.app.hyb.dto.RsOut;



@ManagedBean(name="CadOsDadosContabeisBean")
@ViewScoped
public class CadOsDadosContabeisBean implements Serializable {
	private static final long serialVersionUID = 1L;
    
	//Drop down menu
	private String tipoManiSelecionado;
	private Map<String, String> tipoMani;
	private String cboEmpresaSelecionado;
	private Map<String, String> cboEmpresa;
	private String cboAreaCompSelecionado;
	private Map<String, String> cboAreaComp;
	private String cboCtaContabilSelecionado;
	private Map<String, ObjCntaContab> cboCtaContabil;
	private String cboCatgNv1Selecionado;
	private Map<String, String> cboCatgNv1;
	private String cboCatgNv2Selecionado;
	private Map<String, String> cboCatgNv2;
	private String cboCatgNv3Selecionado;
	private Map<String, ObjCatgNv3> cboCatgNv3;
	private String cboLinhaNego1Selecionado;
	private Map<String, String> cboLinhaNego1;
	private String cboLinhaNego2Selecionado;
	private Map<String, String> cboLinhaNego2;
	private String cboFatorRiscoSelecionado;
	private Map<String, String> cboFatorRisco;
	private String cboTpCapturaSelecionado;
	private List<String> cboTpCaptura = Arrays.asList("Autom�tica", "Manual");
	
	//Inputs
	private String strNrSequOSRela;
	private String strDtLancContab;
	private Date txtDtLancContab;
	private String txtBuscaN3;
	private String strNmCanalOrig; //readonly
	private String strCausaEvento; //readonly
	private String txtCtrCntbOUR; //readonly
	private String txtCtrOrigOUR; //readonly
	private String txtPTVD; //readonly
	
	//Inputs hidden
	private String contOnLoad;
	private String strAction;
	private String strTipoLanc;
	
	//Query String
	private Object strTitulo = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strTitulo");
	private Object strNrSequOS = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSequOS");
	
	//Class usada no body da p�gina
	private String strRefrCdMani;
		
	private Object objDadosContabeisOut;
	private RsOut objRsOut;
	private String strCntaContab;
	private String strCodEvent, strEmpr, strAreaComp, strCatgContab1, strCatgContab2;
	private String strCatgContab3, strLinhNego1, strLinhNego2, strFatRisc, strTpCaptura;
	private String strCtrContbOUR, strCtrOrigOUR, strPTVD, strTpMani;
	private String strErro;
	
	//Pra desabilitar dinamicamente os selects igual era feito no JS
	private Boolean cboCatgNv1Disabled;
	private Boolean cboCatgNv2Disabled;

	@PostConstruct
	public void init() {
		tipoMani = fnGetTipoMani();
		cboEmpresa = fnGetEmpresa();
		cboCtaContabil = fnGetContaContabil();
		cboCatgNv3 = fnGetCatgNv();
		cboLinhaNego1 = fnGetLinhaNego1();
	
		cboAreaCompSelecionado = "-1";
		cboLinhaNego1Selecionado = "-1";
		
		cboCatgNv1Disabled = true;
		cboCatgNv2Disabled = true;
		
//		Set objDadosContabeisOut = server.CreateObject ("xHY_CadOsDadosContab.clsCadOsDadosCntb")
//		Set objRsOut = objDadosContabeisOut.fnSelDadosContabeis (strNrSequOS, strErro)
//		VerificaErro(strErro)
		
		//Mock
		RsOut objRsOut = new RsOut("dT_CNTB ", 
						"cD_CNAL_ORIG ", 
						"nM_CNAL_ORIG ", 
						"cD_CNTA_CNTB ", 
						"cD_EMPR_GRUP ",			
						"cD_AREA_COMP ", 
						"cD_CATG_CNTB_NIVE_1 ", 
						"cD_CATG_CNTB_NIVE_2 ", 
						"cD_CATG_CNTB_NIVE_3 ",
						"cD_LINH_NEGO_NIVE_1 ", 
						"cD_LINH_NEGO_NIVE_2 ", 
						"cD_FATR_RISC ", 
						"cD_CPTU ",
						"nM_CENT_CNTB_UOR ", 
						"nM_CENT_ORIG_UOR ", 
						"nM_PV ", 
						"nM_CAUS_EVEN ", 
						"cD_MANI ",	
						"nR_SEQU_ORDE_SERV_RELA ");
		
		if (objRsOut != null) { //voltou do backend
			strDtLancContab = objRsOut.getDT_CNTB();
			
			if (Arrays.asList("8", "12", "1", "9").contains(objRsOut.getCD_CNAL_ORIG())) {
				strNmCanalOrig = objRsOut.getCD_CNAL_ORIG() + " - " + objRsOut.getNM_CNAL_ORIG();				
			}
			else {
				strNmCanalOrig = "";
			}
			
			strCntaContab  = objRsOut.getCD_CNTA_CNTB();
			strEmpr        = objRsOut.getCD_EMPR_GRUP();
			strAreaComp    = objRsOut.getCD_AREA_COMP();
			strCatgContab1 = objRsOut.getCD_CATG_CNTB_NIVE_1();
			strCatgContab2 = objRsOut.getCD_CATG_CNTB_NIVE_2();
			strCatgContab3 = objRsOut.getCD_CATG_CNTB_NIVE_3();
			strLinhNego1   = objRsOut.getCD_LINH_NEGO_NIVE_1();
			strLinhNego2   = objRsOut.getCD_LINH_NEGO_NIVE_2();
			strFatRisc     = objRsOut.getCD_FATR_RISC();
			strTpCaptura   = objRsOut.getCD_CPTU();
			strCtrContbOUR = objRsOut.getNM_CENT_CNTB_UOR();
			strCtrOrigOUR  = objRsOut.getNM_CENT_ORIG_UOR();
			strPTVD        = objRsOut.getNM_PV();
			strCausaEvento = objRsOut.getNM_CAUS_EVEN();
			strTpMani      = objRsOut.getCD_MANI();
			strNrSequOSRela= objRsOut.getNR_SEQU_ORDE_SERV_RELA();
		}
		
		if (!objRsOut.getCD_MANI().isEmpty() && contOnLoad != "S") {
			strRefrCdMani = "onload='BuscaNV3CC(" + objRsOut.getCD_MANI() + ");fnCarregaPagina();'";
		}
		
		fnCarregaPagina();
		
	}
	
	//Fun��o anteriormente no JS
	public void fnCarregaPagina() {
		//TODO carregar os combos com o que vem do backend
		if (strCntaContab != "") {
			/* cboCtaContabil = fnCarregaCombo("cboCtaContabil", false); */
			cboCtaContabil = fnGetContaContabil();
			cboCtaContabilSelecionado = strCntaContab;
		}
				
		if (strEmpr != "") {
			cboEmpresa = fnCarregaCombo("cboEmpresa", false);
			cboAreaComp = fnCarregaCombo("cboAreaComp", false);
			cboEmpresaSelecionado = strEmpr;
			cboAreaCompSelecionado = strAreaComp;
		}
		
		if (strCatgContab3 != "") {
			/* cboCatgNv3 = fnCarregaCombo("cboCatgNv3", false); */
			cboCatgNv3 = fnGetCatgNv();
			cboCatgNv3Selecionado = strCatgContab3;
		}

		if (strLinhNego1 != "") {
			cboLinhaNego1 = fnCarregaCombo("cboLinhaNego1", false);
			cboLinhaNego2 = fnCarregaCombo("cboLinhaNego2", false);
			cboLinhaNego1Selecionado = strLinhNego1;
			cboLinhaNego2Selecionado = strLinhNego2;
		}
		
		if (strFatRisc != "") {
			cboFatorRisco = fnCarregaCombo("cboFatorRisco", false);
			cboFatorRiscoSelecionado = strFatRisc;
		}
		
		if (strTpCaptura != "") {
			cboTpCapturaSelecionado = strTpCaptura;
		}
		
		if (strTpMani != "") {
			tipoMani = fnCarregaCombo("tipoMani", false);
			tipoManiSelecionado = strTpMani;
		}
		
		txtCtrCntbOUR = strCtrContbOUR;
		txtCtrOrigOUR = strCtrOrigOUR;
		txtPTVD = strPTVD;
		
	}
	
	//FnBuscaN3CBO
	public void rcBuscaN3BO() {
		Boolean resp = false;
		String key;
		ObjCatgNv3 catNv3Busca = new ObjCatgNv3();
			
		for (Map.Entry<String, ObjCatgNv3> obj : cboCatgNv3.entrySet()) {
			key = obj.getKey();
			catNv3Busca = obj.getValue();
			if (catNv3Busca.getCdN3().equals(txtBuscaN3)) {
				cboCatgNv3 = fnGetCatgNv();
				cboCatgNv3Selecionado = catNv3Busca.getValue();
				resp = true;
				break;
			}	
		}
	}
	
	//FnBuscaN3 = param N3
	//FnBuscaN3CBO
	public void rcBuscaN3() {
		Boolean resp = false;
		String key;
		ObjCatgNv3 catNv3Busca = new ObjCatgNv3();
			
		for (Map.Entry<String, ObjCatgNv3> obj : cboCatgNv3.entrySet()) {
			key = obj.getKey();
			catNv3Busca = obj.getValue();
			if (catNv3Busca.getCdN3().equals(txtBuscaN3)) {
				cboCatgNv3 = fnGetCatgNv();
				cboCatgNv3Selecionado = catNv3Busca.getValue();
				resp = true;
				break;
			}	
		}
		
		if (!resp) {
			cboCatgNv3Selecionado = "";
			cboCtaContabilSelecionado = "";
			cboCatgNv1Selecionado = "";
			cboCatgNv2Selecionado = "";
			cboCatgNv1Disabled = true;
			cboCatgNv2Disabled = true;
			tipoManiSelecionado = "";
			RequestContext.getCurrentInstance().execute("alert('Nivel 3 N�o Encontrado !')");
			return;
		}
		
		Boolean respCC = false;

		for (ObjCntaContab ctCont : cboCtaContabil.values()) {
			String NUMCC = catNv3Busca.getNuCntC();
			System.out.println("NUMCC "+NUMCC);
			if (ctCont.getNuCntC() == NUMCC) {
				cboCtaContabilSelecionado = ctCont.getValue();
				respCC = true;
			}
			
		}
		
		if (!respCC) {
			cboCtaContabilSelecionado = "";
			RequestContext.getCurrentInstance().execute("alert('Conta Cont�bil N�o Encontrada !')");
			return;
		}
		
		txtBuscaN3 = "";
	}
	
	//Overload - pra usar no remoteCommand e internamente
	public Map<String, String> fnCarregaCombo(String aStrIdObj, Boolean IsFunction) {
		Map<String, String> ret = new HashMap<String, String>();

		//mock
		switch(aStrIdObj) {
			case "tipoMani":
				ret.put("1", "2");
				ret.put("3", "4");
				break;
			
			//random pra todos os combos de mock
			default:
				for(int i = 0; i < 5; i++) {
					ret.put("mock select "+i, "mock selected "+i);
				}
				break;
		}
		return ret;		
	}
	
	//Overload - pra usar no remoteCommand e internamente
	public Map<String, String> fnCarregaCombo() {
		String aStrIdObj = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("aStrIdObj").toString();
		Boolean IsFunction = Boolean.valueOf(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("IsFunction").toString());
		return fnCarregaCombo(aStrIdObj, IsFunction);		
	}
	
	public Map<String, String> fnGetTipoMani() {
    	//Set objDadosContabeis = server.CreateObject ("xHY_CadOsDadosContab.clsCadOsDadosCntb")
    	//Set objRs = objDadosContabeis.fnSelTipoMani(MsgErro)
    	//VerificaErro(MsgErro)
		Map<String, String> ret = new HashMap<String, String>();
		ret.put("DE_TIPO_MANI_1", "SQ_TIPO_MANI_1");
		ret.put("DE_TIPO_MANI_2", "SQ_TIPO_MANI_2");
		return ret;
	}
	
	public Map<String, String> fnGetEmpresa() {
		//Set objDadosContabeis = server.CreateObject ("xHY_CadOsDadosContab.clsCadOsDadosCntb")
		//Set objRs = objDadosContabeis.fnSelValEmpresa(MsgErro)
		//VerificaErro(MsgErro)
		Map<String, String> ret = new HashMap<String, String>();
		ret.put("NO_EMPR_1", "SQ_EMPR_1");
		ret.put("NO_EMPR_2", "SQ_EMPR_2");
		ret.put("NO_EMPR_3", "SQ_EMPR_3");
		ret.put("NO_EMPR_4", "SQ_EMPR_4");
		return ret;		
	}
	
	public Map<String, ObjCntaContab> fnGetContaContabil() {
		//Set objDadosContabeis = server.CreateObject ("xHY_CadOsDadosContab.clsCadOsDadosCntb")
		//Set objRs = objDadosContabeis.fnSelValCboContaContabil (MsgErro)
		//VerificaErro(MsgErro)
		Map<String, ObjCntaContab> ret = new HashMap<String, ObjCntaContab>();
		ret.put("no_cnta_inte_1", new ObjCntaContab("sq_cnta_cont_1", "1234"));
		ret.put("no_cnta_inte_2", new ObjCntaContab("sq_cnta_cont_2", "5678"));
		return ret;		
	}
	
	public Map<String, ObjCatgNv3> fnGetCatgNv() {
		//Set objDadosContabeis = server.CreateObject ("xHY_CadOsDadosContab.clsCadOsDadosCntb")
		//Set objRs = objDadosContabeis.fnSelValCatgNvCC("", "", "3", "1" , MsgErro)
		//VerificaErro(MsgErro)
		/* arrCdN3.add(objRs.getMN_TIPO_EVEN_PERD().replace('_', '')); */
		Map<String, ObjCatgNv3> ret = new HashMap<String, ObjCatgNv3>();
		ret.put("MN_TIPO_EVEN_PERD_1", new ObjCatgNv3("SQ_TIPO_EVEN_PERD_1", "1234", "1234"));
		ret.put("MN_TIPO_EVEN_PERD_2", new ObjCatgNv3("SQ_TIPO_EVEN_PERD_2", "5678", "0000"));
		return ret;		
	}
	
	public Map<String, String> fnGetLinhaNego1() {
		//Set objDadosContabeis = server.CreateObject ("xHY_CadOsDadosContab.clsCadOsDadosCntb")
		//Set objRs = objDadosContabeis.fnSelValLinhaNego1(MsgErro)
		//VerificaErro(MsgErro)
		Map<String, String> ret = new HashMap<String, String>();
		ret.put("DE_PILA_1", "SQ_PILA_1");
		ret.put("DE_PILA_2", "SQ_PILA_2");
		ret.put("DE_PILA_3", "SQ_PILA_3");
		ret.put("DE_PILA_4", "SQ_PILA_4");
		return ret;				
	}
	
	public Map<String, String> fnGetFatorRisco() {
		//Set objDadosContabeis = server.CreateObject ("xHY_CadOsDadosContab.clsCadOsDadosCntb")
    	//Set objRs = objDadosContabeis.fnSelValFatorRisco(MsgErro)
    	//VerificaErro(MsgErro)
		Map<String, String> ret = new HashMap<String, String>();
		ret.put("NO_SUB_FATR_RISC_1", "SQ_SUB_FATR_RISC_1");
		ret.put("NO_SUB_FATR_RISC_2", "SQ_SUB_FATR_RISC_2");
		ret.put("NO_SUB_FATR_RISC_3", "SQ_SUB_FATR_RISC_3");
		return ret;
	}	
	
	public String getTxtBuscaN3() {
		return txtBuscaN3;
	}

	public void setTxtBuscaN3(String txtBuscaN3) {
		this.txtBuscaN3 = txtBuscaN3;
	}

	public String getTxtCtrCntbOUR() {
		return txtCtrCntbOUR;
	}

	public String getTxtCtrOrigOUR() {
		return txtCtrOrigOUR;
	}

	public String getTxtPTVD() {
		return txtPTVD;
	}

	public Boolean getCboCatgNv1Disabled() {
		return cboCatgNv1Disabled;
	}

	public void setCboCatgNv1Disabled(Boolean cboCatgNv1Disabled) {
		this.cboCatgNv1Disabled = cboCatgNv1Disabled;
	}

	public Boolean getCboCatgNv2Disabled() {
		return cboCatgNv2Disabled;
	}

	public void setCboCatgNv2Disabled(Boolean cboCatgNv2Disabled) {
		this.cboCatgNv2Disabled = cboCatgNv2Disabled;
	}

	public String getStrRefrCdMani() {
		return strRefrCdMani;
	}

	public void setStrRefrCdMani(String strRefrCdMani) {
		this.strRefrCdMani = strRefrCdMani;
	}

	public String getContOnLoad() {
		return contOnLoad;
	}

	public void setContOnLoad(String contOnLoad) {
		this.contOnLoad = contOnLoad;
	}
	
	public String getStrAction() {
		return strAction;
	}

	public void setStrAction(String strAction) {
		this.strAction = strAction;
	}

	public String getStrTipoLanc() {
		return strTipoLanc;
	}

	public void setStrTipoLanc(String strTipoLanc) {
		this.strTipoLanc = strTipoLanc;
	}

	public Date getTxtDtLancContab() {
		return txtDtLancContab;
	}

	public void setTxtDtLancContab(Date txtDtLancContab) {
		this.txtDtLancContab = txtDtLancContab;
	}

	public String getStrCausaEvento() {
		return strCausaEvento;
	}

	public String getCboTpCapturaSelecionado() {
		return cboTpCapturaSelecionado;
	}

	public void setCboTpCapturaSelecionado(String cboTpCapturaSelecionado) {
		this.cboTpCapturaSelecionado = cboTpCapturaSelecionado;
	}

	public List<String> getCboTpCaptura() {
		return cboTpCaptura;
	}

	public void setCboTpCaptura(List<String> cboTpCaptura) {
		this.cboTpCaptura = cboTpCaptura;
	}

	public String getCboFatorRiscoSelecionado() {
		return cboFatorRiscoSelecionado;
	}

	public void setCboFatorRiscoSelecionado(String cboFatorRiscoSelecionado) {
		this.cboFatorRiscoSelecionado = cboFatorRiscoSelecionado;
	}

	public Map<String, String> getCboFatorRisco() {
		return cboFatorRisco;
	}

	public void setCboFatorRisco(Map<String, String> cboFatorRisco) {
		this.cboFatorRisco = cboFatorRisco;
	}

	public String getCboLinhaNego2Selecionado() {
		return cboLinhaNego2Selecionado;
	}

	public void setCboLinhaNego2Selecionado(String cboLinhaNego2Selecionado) {
		this.cboLinhaNego2Selecionado = cboLinhaNego2Selecionado;
	}

	public Map<String, String> getCboLinhaNego2() {
		return cboLinhaNego2;
	}

	public void setCboLinhaNego2(Map<String, String> cboLinhaNego2) {
		this.cboLinhaNego2 = cboLinhaNego2;
	}

	public String getCboLinhaNego1Selecionado() {
		return cboLinhaNego1Selecionado;
	}

	public void setCboLinhaNego1Selecionado(String cboLinhaNego1Selecionado) {
		this.cboLinhaNego1Selecionado = cboLinhaNego1Selecionado;
	}

	public Map<String, String> getCboLinhaNego1() {
		return cboLinhaNego1;
	}

	public void setCboLinhaNego1(Map<String, String> cboLinhaNego1) {
		this.cboLinhaNego1 = cboLinhaNego1;
	}

	public String getCboCatgNv3Selecionado() {
		return cboCatgNv3Selecionado;
	}

	public void setCboCatgNv3Selecionado(String cboCatgNv3Selecionado) {
		this.cboCatgNv3Selecionado = cboCatgNv3Selecionado;
	}

	public Map<String, ObjCatgNv3> getCboCatgNv3() {
		return cboCatgNv3;
	}

	public void setCboCatgNv3(Map<String, ObjCatgNv3> cboCatgNv3) {
		this.cboCatgNv3 = cboCatgNv3;
	}

	public String getCboCatgNv2Selecionado() {
		return cboCatgNv2Selecionado;
	}

	public void setCboCatgNv2Selecionado(String cboCatgNv2Selecionado) {
		this.cboCatgNv2Selecionado = cboCatgNv2Selecionado;
	}

	public Map<String, String> getCboCatgNv2() {
		return cboCatgNv2;
	}

	public void setCboCatgNv2(Map<String, String> cboCatgNv2) {
		this.cboCatgNv2 = cboCatgNv2;
	}
	
	public String getCboCatgNv1Selecionado() {
		return cboCatgNv1Selecionado;
	}

	public void setCboCatgNv1Selecionado(String cboCatgNv1Selecionado) {
		this.cboCatgNv1Selecionado = cboCatgNv1Selecionado;
	}

	public Map<String, String> getCboCatgNv1() {
		return cboCatgNv1;
	}

	public void setCboCatgNv1(Map<String, String> cboCatgNv1) {
		this.cboCatgNv1 = cboCatgNv1;
	}

	public String getCboCtaContabilSelecionado() {
		return cboCtaContabilSelecionado;
	}

	public void setCboCtaContabilSelecionado(String cboCtaContabilSelecionado) {
		this.cboCtaContabilSelecionado = cboCtaContabilSelecionado;
	}

	public Map<String, ObjCntaContab> getCboCtaContabil() {
		return cboCtaContabil;
	}

	public void setCboCtaContabil(Map<String, ObjCntaContab> cboCtaContabil) {
		this.cboCtaContabil = cboCtaContabil;
	}

	public String getCboAreaCompSelecionado() {
		return cboAreaCompSelecionado;
	}

	public void setCboAreaCompSelecionado(String cboAreaCompSelecionado) {
		this.cboAreaCompSelecionado = cboAreaCompSelecionado;
	}

	public Map<String, String> getCboAreaComp() {
		return cboAreaComp;
	}

	public void setCboAreaComp(Map<String, String> cboAreaComp) {
		this.cboAreaComp = cboAreaComp;
	}

	public String getStrNmCanalOrig() {
		return strNmCanalOrig;
	}
	
	public String getCboEmpresaSelecionado() {
		return cboEmpresaSelecionado;
	}

	public void setCboEmpresaSelecionado(String cboEmpresaSelecionado) {
		this.cboEmpresaSelecionado = cboEmpresaSelecionado;
	}

	public Map<String, String> getCboEmpresa() {
		return cboEmpresa;
	}

	public void setCboEmpresa(Map<String, String> cboEmpresa) {
		this.cboEmpresa = cboEmpresa;
	}

	public String getStrDtLancContab() {
		return strDtLancContab;
	}

	public void setStrDtLancContab(String strDtLancContab) {
		this.strDtLancContab = strDtLancContab;
	}

	public String getStrNrSequOSRela() {
		return strNrSequOSRela;
	}

	public void setStrNrSequOSRela(String strNrSequOSRela) {
		this.strNrSequOSRela = strNrSequOSRela;
	}
		
	public String getTipoManiSelecionado() {
		return tipoManiSelecionado;
	}

	public void setTipoManiSelecionado(String tipoManiSelecionado) {
		this.tipoManiSelecionado = tipoManiSelecionado;
	}


	public Map<String, String> getTipoMani() {
		return tipoMani;
	}

	public void setTipoMani(Map<String, String> tipoMani) {
		this.tipoMani = tipoMani;
	}
}