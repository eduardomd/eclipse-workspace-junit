package com.altec.bsbr.app.hyb.dto;

public class StatusOS {
	private String CODIGO;
	private String SITUACAO;
	private String CD_NOTI;
	private String NM_NOTI;
	
	public StatusOS() {
		
	}
	
	public StatusOS(String CODIGO, String SITUACAO, String CD_NOTI, String NM_NOTI) {
		this.setCODIGO(CODIGO);
		this.setSITUACAO(SITUACAO);
		this.CD_NOTI = CD_NOTI;
		this.NM_NOTI = NM_NOTI;
	}

	public String getCD_NOTI() {
		return CD_NOTI;
	}
	public void setCD_NOTI(String cD_NOTI) {
		CD_NOTI = cD_NOTI;
	}
	public String getNM_NOTI() {
		return NM_NOTI;
	}
	public void setNM_NOTI(String nM_NOTI) {
		NM_NOTI = nM_NOTI;
	}

	public String getCODIGO() {
		return CODIGO;
	}

	public void setCODIGO(String cODIGO) {
		CODIGO = cODIGO;
	}

	public String getSITUACAO() {
		return SITUACAO;
	}

	public void setSITUACAO(String sITUACAO) {
		SITUACAO = sITUACAO;
	}
}
