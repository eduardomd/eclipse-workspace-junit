package com.altec.bsbr.app.hyb.web.jsf;

import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


@Component("CadastroOsPesquisaEnvolvIfrmBean")
@Scope("session")
public class CadastroOsPesquisaEnvolvIfrmBean extends BasicBBean {

	private String srcIframe;

	public CadastroOsPesquisaEnvolvIfrmBean() {

		String strEntidade;
		String strNrSeqOs;

		// block to get params from url
		FacesContext fc = FacesContext.getCurrentInstance();
		@SuppressWarnings("unchecked")
		Map<String, String> params = fc.getExternalContext().getRequestParameterMap();

		strEntidade = params.get("entidade");
		strNrSeqOs = params.get("NrSeqOs");

		switch (strNrSeqOs) {
		case "1":
			srcIframe = "hyb_cadastroos_pesquisaequipe.xhtml?NrSeqOs=" + strNrSeqOs;
			break;
		case "2":
			srcIframe = "hyb_cadastroos_pesquisacliente.xhtml?NrSeqOs=" + strNrSeqOs;
			break;
		case "3":
			srcIframe = "hyb_cadastroos_pesquisacolaborador.xhtml?NrSeqOs=" + strNrSeqOs;
			break;
		case "4":
			srcIframe = "hyb_cadastroos_pesquisanaocliente.xhtml?NrSeqOs=" + strNrSeqOs;
			break;
		}
		
		setSrcIframe(srcIframe);
		
	}

	public String getSrcIframe() {
		return srcIframe;
	}

	public void setSrcIframe(String srcIframe) {
		this.srcIframe = srcIframe;
	}
}
