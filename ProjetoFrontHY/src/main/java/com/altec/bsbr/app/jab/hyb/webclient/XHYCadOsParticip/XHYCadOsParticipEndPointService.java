//
// Generated By:JAX-WS RI 2.2.9-b130926.1035 (JAXB RI IBM 2.2.8-b130911.1802)
//


package com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsParticip;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;

@WebServiceClient(name = "xHY_CadOsParticipEndPointService", targetNamespace = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/", wsdlLocation = "http://d2240769.bs.br.bsch:80/JAB-HYBSRVOESP/xHY_CadOsParticipEndPointService/XHYCadOsParticipEndPointService.wsdl")
public class XHYCadOsParticipEndPointService
    extends Service
{

    private final static URL XHYCADOSPARTICIPENDPOINTSERVICE_WSDL_LOCATION;
    private final static WebServiceException XHYCADOSPARTICIPENDPOINTSERVICE_EXCEPTION;
    private final static QName XHYCADOSPARTICIPENDPOINTSERVICE_QNAME = new QName("http://webservice.srv001.hyb.jab.app.bsbr.altec.com/", "xHY_CadOsParticipEndPointService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://d2240769.bs.br.bsch:80/JAB-HYBSRVOESP/xHY_CadOsParticipEndPointService/XHYCadOsParticipEndPointService.wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        XHYCADOSPARTICIPENDPOINTSERVICE_WSDL_LOCATION = url;
        XHYCADOSPARTICIPENDPOINTSERVICE_EXCEPTION = e;
    }

    public XHYCadOsParticipEndPointService() {
        super(__getWsdlLocation(), XHYCADOSPARTICIPENDPOINTSERVICE_QNAME);
    }

    public XHYCadOsParticipEndPointService(WebServiceFeature... features) {
        super(__getWsdlLocation(), XHYCADOSPARTICIPENDPOINTSERVICE_QNAME, features);
    }

    public XHYCadOsParticipEndPointService(URL wsdlLocation) {
        super(wsdlLocation, XHYCADOSPARTICIPENDPOINTSERVICE_QNAME);
    }

    public XHYCadOsParticipEndPointService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, XHYCADOSPARTICIPENDPOINTSERVICE_QNAME, features);
    }

    public XHYCadOsParticipEndPointService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public XHYCadOsParticipEndPointService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns XHYCadOsParticipEndPoint
     */
    @WebEndpoint(name = "xHY_CadOsParticipEndPointPort")
    public XHYCadOsParticipEndPoint getXHYCadOsParticipEndPointPort() {
        return super.getPort(new QName("http://webservice.srv001.hyb.jab.app.bsbr.altec.com/", "xHY_CadOsParticipEndPointPort"), XHYCadOsParticipEndPoint.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns XHYCadOsParticipEndPoint
     */
    @WebEndpoint(name = "xHY_CadOsParticipEndPointPort")
    public XHYCadOsParticipEndPoint getXHYCadOsParticipEndPointPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://webservice.srv001.hyb.jab.app.bsbr.altec.com/", "xHY_CadOsParticipEndPointPort"), XHYCadOsParticipEndPoint.class, features);
    }

    private static URL __getWsdlLocation() {
        if (XHYCADOSPARTICIPENDPOINTSERVICE_EXCEPTION!= null) {
            throw XHYCADOSPARTICIPENDPOINTSERVICE_EXCEPTION;
        }
        return XHYCADOSPARTICIPENDPOINTSERVICE_WSDL_LOCATION;
    }

}
