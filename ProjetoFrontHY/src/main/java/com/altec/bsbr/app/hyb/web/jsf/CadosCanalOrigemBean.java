package com.altec.bsbr.app.hyb.web.jsf;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import javax.annotation.PostConstruct;
/*import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;*/
/*import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
*/

import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.validation.constraints.Null;

import org.dom4j.Document;
import org.dom4j.io.SAXReader;
import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.web.util.ClsCadOSCanalOrig;
import com.altec.bsbr.app.hyb.web.util.LogIncUtil;
import com.altec.bsbr.app.hyb.web.util.XHYUsuarioIncService;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmLog.XHYAdmLogEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsCanalOrig.XHYCadOsCanalOrigEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYPesquisa.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYUsuario.XHYUsuarioEndPoint;
import com.altec.bsbr.fw.security.SecurityInfo;
import com.altec.bsbr.fw.security.authorization.Authorization;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.altec.bsbr.app.hyb.dto.CanalOrigEvento;
import com.altec.bsbr.app.hyb.dto.CnalOrigModel;
import com.altec.bsbr.app.hyb.dto.OSCanalOrigem;
import com.altec.bsbr.app.hyb.dto.UsuarioIncModel;

//import com.altec.bsbr.app.blank.web.util.ClsCadOSCanalOrig;
//import com.altec.bsbr.app.web.util.ClsCadOSCanalOrig;
//import com.altec.bsbr.app.blank.web.util.LogIncUtil;
//import com.altec.bsbr.app.blank.web.util.Util;

@Component("CadosCanalOrigemBean")
@Scope("session")
public class CadosCanalOrigemBean extends BasicBBean {

	private static final long serialVersionUID = 1L;

	private String cboCanalOrigemSelecionada;
	private LinkedHashMap<String, String> objRsAdmCnalOrig;
	private String strMsg = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
			.get("strMsg");
	private String strNrSeqOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
			.get("strNrSeqOs");
	private String strTitulo = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
			.get("strTitulo");
	private String TP_OFCI;
	private String TP_MONI_LYNX;
	private String IN_MINSTR;
	private String IN_OMINTE;
	private String IN_OMINTR;
	private String IN_OMINSTR;
	private String IN_OMTELE;
	private String IN_OMMALT;
	private String IN_OMDENC;
	private String IN_OMDEPT;
	private String IN_OMAGEN;
	private String cboOrigemOS;
	private boolean trLynx = false;
	private boolean trOficio = false;
	private boolean trMonitInst = false;
	private boolean trOutrosMeios = false;
	private String cpfUsuario;
	private boolean desabilitar = false;
	
	private String strReadOnly = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strReadOnly");
	
	@Autowired
	private XHYUsuarioIncService usuario;

	@Autowired
	private XHYCadOsCanalOrigEndPoint canalOrigem;

	private UsuarioIncModel usuarioIncModel;

	@Autowired
	private XHYAdmLogEndPoint admLog;

	@PostConstruct
	public void init() {
		exibirAlerta();
		carregarUsuario();
		consultarCanalOrigem();
		carregarCombo();
		carregarUsuario();
		
		if (strReadOnly != null) {
			if (strReadOnly.equals("1")) {
				desabilitar = true;
			}
		}

	}

	private void carregarUsuario() {
		try {
			usuarioIncModel = usuario.getDadosUsuario();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void exibirAlerta() {
		if (strMsg != null) {
			RequestContext.getCurrentInstance().execute("alert('" + strMsg + "')");
		}
	}

	public void exibirCampos() {
		String[] result = cboCanalOrigemSelecionada.split("=");

		switch (result[0]) {
		case "9":
			trOficio = true;
			trLynx = false;
			trMonitInst = false;
			trOutrosMeios = false;
			break;
		case "6":
			trOficio = false;
			trLynx = true;
			trMonitInst = false;
			trOutrosMeios = false;
			break;
		case "7":
			trOficio = false;
			trLynx = false;
			trMonitInst = true;
			trOutrosMeios = false;
			break;
		case "10":
			trOficio = false;
			trLynx = false;
			trMonitInst = false;
			trOutrosMeios = true;
			break;
		default:
			trOficio = false;
			trLynx = false;
			trMonitInst = false;
			trOutrosMeios = false;
			break;
		}
		
	}

	private void consultarCanalOrigem() {

		JSONArray pcursorCanalOrg = new JSONArray();
		String objRsAreaTemp;

		try {
			
			objRsAreaTemp = canalOrigem.consultarOSCanalOrigem(strNrSeqOs);

			JSONObject objRsAreaTemp2 = new JSONObject(objRsAreaTemp);

			pcursorCanalOrg = objRsAreaTemp2.getJSONArray("PCURSOR");
			JSONObject temp2 = pcursorCanalOrg.getJSONObject(0);

			cboOrigemOS = temp2.get("CD_CNAL_ORIG").toString();

			TP_OFCI = temp2.get("TP_OFCI").toString() == "null" ? "": temp2.get("TP_OFCI").toString();
			TP_MONI_LYNX = temp2.get("TP_MONI_LYNX").toString() == "null" ? "": temp2.get("TP_MONI_LYNX").toString();
			IN_MINSTR = temp2.get("IN_MINSTR").toString() == "null" ? "": temp2.get("IN_MINSTR").toString();
			IN_OMINTE = temp2.get("IN_OMINTE").toString();
			IN_OMINTR = temp2.get("IN_OMINTR").toString();
			IN_OMINSTR = temp2.get("IN_OMINSTR").toString();
			IN_OMTELE = temp2.get("IN_OMTELE").toString();
			IN_OMMALT = temp2.get("IN_OMMALT").toString();
			IN_OMDENC = temp2.get("IN_OMDENC").toString();
			IN_OMDEPT = temp2.get("IN_OMDEPT").toString();
			IN_OMAGEN = temp2.get("IN_OMAGEN").toString();

			
		} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsCanalOrig.WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void carregarCombo() {

		objRsAdmCnalOrig = new LinkedHashMap<>();

		JSONArray pcursorObjRsArea = new JSONArray();

		String objRsAreaTemp;

		try {
			objRsAreaTemp = canalOrigem.consultarCnAlOrigEvento(usuarioIncModel.getCdAreaUsuario());

			JSONObject objRsAreaTemp2 = new JSONObject(objRsAreaTemp);
			pcursorObjRsArea = objRsAreaTemp2.getJSONArray("PCURSOR");

			for (int i = 0; i < pcursorObjRsArea.length(); i++) {
				JSONObject temp = pcursorObjRsArea.getJSONObject(i);

				if (cboOrigemOS.equals(temp.get("CODIGO").toString())) {
					objRsAdmCnalOrig.put(temp.get("CODIGO").toString(), temp.get("NOME").toString()); // Selecionar Item
					cboCanalOrigemSelecionada = temp.get("CODIGO").toString() + "=" + temp.get("NOME").toString();
					exibirCampos();
				} else {
					objRsAdmCnalOrig.put(temp.get("CODIGO").toString(), temp.get("NOME").toString());
				}

			}

		} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsCanalOrig.WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void salvar() {

		String OFCI = TP_OFCI == null ? "" : TP_OFCI;
		String MONI_LYNX = TP_MONI_LYNX == null ? "" : TP_MONI_LYNX;
		String MINSTR = IN_MINSTR == null ? "" : IN_MINSTR;
		long OMINTE = IN_OMINTE == null ? 0 : 1;
		long OMINTR = IN_OMINTR == null ? 0 : 1;
		long OMINSTR = IN_OMINSTR == null ? 0 : 1;
		long OMTELE = IN_OMTELE == null ? 0 : 1;
		long OMMALT = IN_OMMALT == null ? 0 : 1;
		long OMDENC = IN_OMDENC == null ? 0 : 1;
		long OMDEPT = IN_OMDEPT == null ? 0 : 1;
		long OMAGEN = IN_OMAGEN == null ? 0 : 1;
		long canalOrigemSelecionado = Long.parseLong(cboCanalOrigemSelecionada.split("=")[0]);

		
		try {
			canalOrigem.alterarOSCanalOrigem(strNrSeqOs, canalOrigemSelecionado, OFCI, MONI_LYNX, MINSTR, OMINTE,
					OMINTR, OMINSTR, OMTELE, OMMALT, OMDENC, OMDEPT, OMAGEN);

			try {
				admLog.incluirAcaoLog(strNrSeqOs, Integer.toString(LogIncUtil.SALVAR_CANAL_ORIGEM_OS),
						usuarioIncModel.getCpfUsuario(), "");
			} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYAdmLog.WebServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			RequestContext.getCurrentInstance().execute("alert('Canal de Origem alterado com sucesso!')");

		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsCanalOrig.WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public String getCboCanalOrigemSelecionada() {
		return cboCanalOrigemSelecionada;
	}

	public void setCboCanalOrigemSelecionada(String cboCanalOrigemSelecionada) {
		this.cboCanalOrigemSelecionada = cboCanalOrigemSelecionada;
	}

	public String getTP_OFCI() {
		return TP_OFCI;
	}

	public void setTP_OFCI(String tP_OFCI) {
		TP_OFCI = tP_OFCI;
	}

	public String getTP_MONI_LYNX() {
		return TP_MONI_LYNX;
	}

	public void setTP_MONI_LYNX(String tP_MONI_LYNX) {
		TP_MONI_LYNX = tP_MONI_LYNX;
	}

	public String getIN_MINSTR() {
		return IN_MINSTR;
	}

	public void setIN_MINSTR(String iN_MINSTR) {
		IN_MINSTR = iN_MINSTR;
	}

	public String getIN_OMINTE() {
		return IN_OMINTE;
	}

	public void setIN_OMINTE(String iN_OMINTE) {
		IN_OMINTE = iN_OMINTE;
	}

	public String getIN_OMINTR() {
		return IN_OMINTR;
	}

	public void setIN_OMINTR(String iN_OMINTR) {
		IN_OMINTR = iN_OMINTR;
	}

	public String getIN_OMINSTR() {
		return IN_OMINSTR;
	}

	public void setIN_OMINSTR(String iN_OMINSTR) {
		IN_OMINSTR = iN_OMINSTR;
	}

	public String getIN_OMTELE() {
		return IN_OMTELE;
	}

	public void setIN_OMTELE(String iN_OMTELE) {
		IN_OMTELE = iN_OMTELE;
	}

	public String getIN_OMMALT() {
		return IN_OMMALT;
	}

	public void setIN_OMMALT(String iN_OMMALT) {
		IN_OMMALT = iN_OMMALT;
	}

	public String getIN_OMDENC() {
		return IN_OMDENC;
	}

	public void setIN_OMDENC(String iN_OMDENC) {
		IN_OMDENC = iN_OMDENC;
	}

	public String getIN_OMDEPT() {
		return IN_OMDEPT;
	}

	public void setIN_OMDEPT(String iN_OMDEPT) {
		IN_OMDEPT = iN_OMDEPT;
	}

	public String getStrNrSeqOs() {
		return strNrSeqOs;
	}

	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getStrTitulo() {
		return strTitulo;
	}

	public void setStrTitulo(String strTitulo) {
		this.strTitulo = strTitulo;
	}

	public String getStrMsg() {
		return strMsg;
	}

	public void setStrMsg(String strMsg) {
		this.strMsg = strMsg;
	}

	public LinkedHashMap<String, String> getObjRsAdmCnalOrig() {
		return objRsAdmCnalOrig;
	}

	public void setObjRsAdmCnalOrig(LinkedHashMap<String, String> objRsAdmCnalOrig) {
		this.objRsAdmCnalOrig = objRsAdmCnalOrig;
	}

	public boolean isTrLynx() {
		return trLynx;
	}

	public void setTrLynx(boolean trLynx) {
		this.trLynx = trLynx;
	}

	public boolean isTrOficio() {
		return trOficio;
	}

	public void setTrOficio(boolean trOficio) {
		this.trOficio = trOficio;
	}

	public boolean isTrMonitInst() {
		return trMonitInst;
	}

	public void setTrMonitInst(boolean trMonitInst) {
		this.trMonitInst = trMonitInst;
	}

	public boolean isTrOutrosMeios() {
		return trOutrosMeios;
	}

	public void setTrOutrosMeios(boolean trOutrosMeios) {
		this.trOutrosMeios = trOutrosMeios;
	}

	public String getIN_OMAGEN() {
		return IN_OMAGEN;
	}

	public void setIN_OMAGEN(String iN_OMAGEN) {
		IN_OMAGEN = iN_OMAGEN;
	}

	public boolean isDesabilitar() {
		return desabilitar;
	}

	public void setDesabilitar(boolean desabilitar) {
		this.desabilitar = desabilitar;
	}

}
