package com.altec.bsbr.app.hyb.dto;

public class EquipAtrib {
	
	private String strNome;
	private String strCargo;
	private String strMatricula;
	private String strDesignacao;
	private String strCpf;
	private String strNmDesignacao;
	private String strNmEmailRecu;
	
	public EquipAtrib(){
		
	}

	public String getStrNome() {
		return strNome;
	}

	public void setStrNome(String strNome) {
		this.strNome = strNome;
	}

	public String getStrCargo() {
		return strCargo;
	}

	public void setStrCargo(String strCargo) {
		this.strCargo = strCargo;
	}

	public String getStrMatricula() {
		return strMatricula;
	}

	public void setStrMatricula(String strMatricula) {
		this.strMatricula = strMatricula;
	}

	public String getStrDesignacao() {
		return strDesignacao;
	}

	public void setStrDesignacao(String strDesignacao) {
		this.strDesignacao = strDesignacao;
	}

	public String getStrCpf() {
		return strCpf;
	}

	public void setStrCpf(String strCpf) {
		this.strCpf = strCpf;
	}

	public String getStrNmDesignacao() {
		return strNmDesignacao;
	}

	public void setStrNmDesignacao(String strNmDesignacao) {
		this.strNmDesignacao = strNmDesignacao;
	}

	public String getStrNmEmailRecu() {
		return strNmEmailRecu;
	}

	public void setStrNmEmailRecu(String strNmEmailRecu) {
		this.strNmEmailRecu = strNmEmailRecu;
	}
	
	

}
