package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.altec.bsbr.fw.web.jsf.BasicBBean;

import com.altec.bsbr.app.hyb.dto.AreaModel;
import com.altec.bsbr.app.hyb.dto.EventosModel;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmGer.XHYAdmGerEndPoint;

@Component("AdmGerManutEventosBean")
@Scope("session")
public class AdmGerManutEventosBean extends BasicBBean {

	private static final long serialVersionUID = 1L;

	private static final String TABLE_CARREGANDO = "Carregando...";
	private static final String TABLE_VAZIO = "Sem registro"; 
	
    @Autowired
	private XHYAdmGerEndPoint admGer;
    
	private Object strMsg = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strMsg");
    private String strErro;
    
    private List<EventosModel> objAdmGer = new ArrayList<EventosModel>();
    private LazyDataModel<EventosModel> objRsEvento;
    private List<AreaModel> objRsAreaEvento = new ArrayList<AreaModel>();
    
    private String txtAddEvento;	
    private String txtHdEvento;
    private String txtHdParamAltEvento;
	private String txtHdParamAltRelac;

	private String emptyMessage;
	
	private Map<String, Integer> manutCanalChecked;	
	
	@PostConstruct
    public void init() {
		txtAddEvento = "";
		emptyMessage = TABLE_CARREGANDO;
		carregarArea();		
    }

	private void carregarArea() {
		//this.objRsAreaEvento = new String[3];
		
		this.objRsAreaEvento.clear();
		
		String retorno = "";
		try {
			retorno = admGer.consultarArea();
			
			JSONObject consultaParticipanteOS = new JSONObject(retorno);
			JSONArray pcursor = consultaParticipanteOS.getJSONArray("PCURSOR");
			for (int i = 0; i < pcursor.length(); i++) {
				JSONObject f = pcursor.getJSONObject(i);
				
				int codigoArea = Integer.parseInt(f.get("CODIGO").toString());
				String nome = f.isNull("NOME") == true? "" : f.get("NOME").toString();
				
				this.objRsAreaEvento.add(new AreaModel(codigoArea, nome));

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

	}
	
	public void montarListaEventos_Lazy() {
    	
		emptyMessage = TABLE_VAZIO;   
    	
		objRsEvento = new LazyDataModel<EventosModel>() {

			private static final long serialVersionUID = 1L;
			
			@Override
			public List<EventosModel> load(int first, int pageSize, String sortField,
                                                    SortOrder sortOrder, Map<String, Object> filters) {
												
				List<EventosModel> objRsEventosModelTemp = carregarEventos();
				
				setRowCount(objRsEventosModelTemp.size());
				setPageSize(pageSize);
				
				List<EventosModel> result = new ArrayList<EventosModel>();
				
				if(manutCanalChecked == null)
					manutCanalChecked = new HashMap<String, Integer>();
				
				manutCanalChecked.clear();
				int criarConexao = 0;
				
				for (int i = first; i < (first + pageSize); i++) {
					
					result.add(objRsEventosModelTemp.get(i));
					
					for (int j = 0; j < objRsAreaEvento.size(); j++) {
						
						String chave = objRsAreaEvento.get(j).getId() + "_" + objRsEventosModelTemp.get(i).getId();
												
						manutCanalChecked.put(chave, VerificaAreaEvento(criarConexao, Integer.toString(objRsAreaEvento.get(j).getId()), Integer.toString(objRsEventosModelTemp.get(i).getId())));	
					
						criarConexao++;
					}
				}
				
				System.out.println("Size MAP:: " + manutCanalChecked.size());
				
				return result;
			}
		};
    }
	
	private List<EventosModel> carregarEventos() {
		
		List<EventosModel> objRsEventoTemp = new ArrayList<EventosModel>();
		
		String retorno = "";
		try {
			retorno = admGer.consultarEvento();
			
			JSONObject consultaEventos = new JSONObject(retorno);
			JSONArray pcursor = consultaEventos.getJSONArray("PCURSOR");
			for (int i = 0; i < pcursor.length(); i++) {
				JSONObject f = pcursor.getJSONObject(i);
				
				String codigoEvento = f.get("CODIGO").toString();
				String nome = f.isNull("NOME") == true? "" : f.get("NOME").toString();				
				
				objRsEventoTemp.add(new EventosModel(Integer.parseInt(codigoEvento), nome, true, false, true));
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		return objRsEventoTemp;
	}
	
	private Integer VerificaAreaEvento(int index, String intCodArea, String intCodEvento) {

        String objRsVerificaAreaCanalOrigem = "";
        Integer retorno = 0;

        try {
			objRsVerificaAreaCanalOrigem = admGer.verificaAreaEvento(index, intCodArea, intCodEvento);
			
	        JSONObject objRsVerificaAreaCanalOrigemTemp = new JSONObject(objRsVerificaAreaCanalOrigem);
	        JSONArray pcursor = objRsVerificaAreaCanalOrigemTemp.getJSONArray("PCURSOR");
	        JSONObject f = pcursor.getJSONObject(0);
	        
	        if (f.get("RELAC").toString().equals("1")) {
	        	//System.out.println("intCodArea: " + intCodArea + " intCodEvento: " + intCodEvento  );
	             retorno = 1;
	        }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
        
        return retorno;
}
        
    public Integer isAreaEventoChecked(int intCodEvento, int intCodCanal) {
		String chave = intCodEvento + "_" + intCodCanal;
		return manutCanalChecked.get(chave);
    }
	
	public void adicionar() {
			
		boolean contains = false;
		
		for (EventosModel evento : getObjRsEvento()) {
			if (evento.getEvento().toUpperCase().equals(txtAddEvento.toUpperCase())) {
				contains = true;
			}
		}
       
		if (contains) {
			RequestContext.getCurrentInstance().execute("alert('Nome jÃ¡ existente! Escolha outro Nome!')");  
			txtAddEvento = "";
		} else {
			try {
				admGer.inserirEvento(txtAddEvento);
				txtAddEvento="";
				RequestContext.getCurrentInstance().execute("alert('Evento incluÃ­do com sucesso!')");  
				montarListaEventos_Lazy();
				//FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "hy_adm_ger_manut_eventos.xhtml");
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				try {
					FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
	}
	
	public void deletar() {
		Map<String, String> requestParamMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		String idRegistro = requestParamMap.get("frm:txtHdEvento");

		try {
			admGer.apagarEvento(idRegistro);
			RequestContext.getCurrentInstance().execute("alert('Evento excluÃ­do com sucesso!')");
			montarListaEventos_Lazy();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}	
	}
	
	public void salvar() {
		Map<String, String> requestParamMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		String result = requestParamMap.get("frm:txtHdParamAltEvento");
		
		String [] x = result.split("@");
		
		for(int i = 0; i < x.length;  i++) {

			String [] y = x[i].split("/");
			
			try {
				admGer.alterarEvento(y[0], y[1]);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				try {
					FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
		}
		
		result = requestParamMap.get("frm:txtHdParamAltRelac");
		//System.out.println("Salvar2:" + result);
		String [] p = result.split("@");
		
		for(int i = 0; i < p.length;  i++) {

			String [] y = p[i].split("-");
			
			if (y[2].equals("true")) {
				try {
					admGer.inserirAreaEvento(y[0], y[1]);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					try {
						FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			} else {
				try {
					admGer.apagarAreaEvento(y[0], y[1]);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					try {
						FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}	
		}
		
		RequestContext.getCurrentInstance().execute("alert('AlteraÃ§Ãµes efetuadas com sucesso!')"); 
		
		carregarArea();
		montarListaEventos_Lazy();
	}
	
	public int gerarId() {
		return (int) (Math.random() * 1000);
	}

	public String getStrMsg() {
		if(strMsg == null)
			return "";
		
		return strMsg.toString();
	}

	public void setStrMsg(String strMsg) {
		this.strMsg = strMsg;
	}

	public String getStrErro() {
		return strErro;
	}

	public void setStrErro(String strErro) {
		this.strErro = strErro;
	}

	public List<EventosModel> getObjAdmGer() {
		return objAdmGer;
	}

	public void setObjAdmGer(List<EventosModel> objAdmGer) {
		this.objAdmGer = objAdmGer;
	}

	public List<AreaModel> getObjRsAreaEvento() {
		return objRsAreaEvento;
	}

	public int getObjRsAreaEventoLength() {
		return this.objRsAreaEvento.size();
	}
	
	public void setObjRsAreaEvento(List<AreaModel> objRsArea) {
		this.objRsAreaEvento = objRsArea;
	}

	public String getTxtAddEvento() {
		return txtAddEvento;
	}

	public void setTxtAddEvento(String txtAddEvento) {
		this.txtAddEvento = txtAddEvento;
	}

	public String getTxtHdEvento() {
		return txtHdEvento;
	}

	public void setTxtHdEvento(String txtHdEvento) {
		this.txtHdEvento = txtHdEvento;
	}

	public String getTxtHdParamAltEvento() {
		return txtHdParamAltEvento;
	}

	public void setTxtHdParamAltEvento(String txtHdParamAltEvento) {
		this.txtHdParamAltEvento = txtHdParamAltEvento;
	}

	public String getTxtHdParamAltRelac() {
		return txtHdParamAltRelac;
	}

	public void setTxtHdParamAltRelac(String txtHdParamAltRelac) {
		this.txtHdParamAltRelac = txtHdParamAltRelac;
	}

	public String getEmptyMessage() {
		return emptyMessage;
	}

	public void setEmptyMessage(String emptyMessage) {
		this.emptyMessage = emptyMessage;
	}

	public LazyDataModel<EventosModel> getObjRsEvento() {
		return objRsEvento;
	}
}
