//
// Generated By:JAX-WS RI 2.2.9-b130926.1035 (JAXB RI IBM 2.2.8-b130911.1802)
//


package com.altec.bsbr.app.jab.hyb.webclient.XHYTramite;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;

@WebService(name = "xHY_TramiteEndPoint", targetNamespace = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/")
@SOAPBinding(style = SOAPBinding.Style.RPC)
@XmlSeeAlso({
    ObjectFactory.class
})
public interface XHYTramiteEndPoint {


    /**
     * 
     * @param arg2
     * @param arg1
     * @param arg0
     * @return
     *     returns java.lang.String
     * @throws WebServiceException
     */
    @WebMethod(operationName = "InserirTramiteHistoricoEnvio")
    @WebResult(partName = "return")
    @Action(input = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_TramiteEndPoint/InserirTramiteHistoricoEnvioRequest", output = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_TramiteEndPoint/InserirTramiteHistoricoEnvioResponse", fault = {
        @FaultAction(className = WebServiceException.class, value = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_TramiteEndPoint/InserirTramiteHistoricoEnvio/Fault/WebServiceException")
    })
    public String inserirTramiteHistoricoEnvio(
        @WebParam(name = "arg0", partName = "arg0")
        String arg0,
        @WebParam(name = "arg1", partName = "arg1")
        String arg1,
        @WebParam(name = "arg2", partName = "arg2")
        String arg2)
        throws WebServiceException
    ;

    /**
     * 
     * @param arg0
     * @return
     *     returns java.lang.String
     * @throws WebServiceException
     */
    @WebMethod(operationName = "ConsultarProximoTramite")
    @WebResult(partName = "return")
    @Action(input = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_TramiteEndPoint/ConsultarProximoTramiteRequest", output = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_TramiteEndPoint/ConsultarProximoTramiteResponse", fault = {
        @FaultAction(className = WebServiceException.class, value = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_TramiteEndPoint/ConsultarProximoTramite/Fault/WebServiceException")
    })
    public String consultarProximoTramite(
        @WebParam(name = "arg0", partName = "arg0")
        String arg0)
        throws WebServiceException
    ;

    /**
     * 
     * @param arg3
     * @param arg2
     * @param arg5
     * @param arg4
     * @param arg1
     * @param arg0
     * @return
     *     returns java.lang.String
     * @throws WebServiceException
     */
    @WebMethod(operationName = "InserirTramite")
    @WebResult(partName = "return")
    @Action(input = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_TramiteEndPoint/InserirTramiteRequest", output = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_TramiteEndPoint/InserirTramiteResponse", fault = {
        @FaultAction(className = WebServiceException.class, value = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_TramiteEndPoint/InserirTramite/Fault/WebServiceException")
    })
    public String inserirTramite(
        @WebParam(name = "arg0", partName = "arg0")
        String arg0,
        @WebParam(name = "arg1", partName = "arg1")
        String arg1,
        @WebParam(name = "arg2", partName = "arg2")
        String arg2,
        @WebParam(name = "arg3", partName = "arg3")
        String arg3,
        @WebParam(name = "arg4", partName = "arg4")
        String arg4,
        @WebParam(name = "arg5", partName = "arg5")
        String arg5)
        throws WebServiceException
    ;

    /**
     * 
     * @param arg1
     * @param arg0
     * @return
     *     returns java.lang.String
     * @throws WebServiceException
     */
    @WebMethod(operationName = "ConsultarOS_Tramite")
    @WebResult(partName = "return")
    @Action(input = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_TramiteEndPoint/ConsultarOS_TramiteRequest", output = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_TramiteEndPoint/ConsultarOS_TramiteResponse", fault = {
        @FaultAction(className = WebServiceException.class, value = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_TramiteEndPoint/ConsultarOS_Tramite/Fault/WebServiceException")
    })
    public String consultarOSTramite(
        @WebParam(name = "arg0", partName = "arg0")
        String arg0,
        @WebParam(name = "arg1", partName = "arg1")
        String arg1)
        throws WebServiceException
    ;

    /**
     * 
     * @param arg0
     * @return
     *     returns java.lang.String
     * @throws WebServiceException
     */
    @WebMethod(operationName = "Consultar_OS_DataRetorno")
    @WebResult(partName = "return")
    @Action(input = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_TramiteEndPoint/Consultar_OS_DataRetornoRequest", output = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_TramiteEndPoint/Consultar_OS_DataRetornoResponse", fault = {
        @FaultAction(className = WebServiceException.class, value = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_TramiteEndPoint/Consultar_OS_DataRetorno/Fault/WebServiceException")
    })
    public String consultarOSDataRetorno(
        @WebParam(name = "arg0", partName = "arg0")
        String arg0)
        throws WebServiceException
    ;

    /**
     * 
     * @param arg2
     * @param arg1
     * @param arg0
     * @return
     *     returns java.lang.String
     * @throws WebServiceException
     */
    @WebMethod(operationName = "ConsultarOS_Periodo")
    @WebResult(partName = "return")
    @Action(input = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_TramiteEndPoint/ConsultarOS_PeriodoRequest", output = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_TramiteEndPoint/ConsultarOS_PeriodoResponse", fault = {
        @FaultAction(className = WebServiceException.class, value = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_TramiteEndPoint/ConsultarOS_Periodo/Fault/WebServiceException")
    })
    public String consultarOSPeriodo(
        @WebParam(name = "arg0", partName = "arg0")
        String arg0,
        @WebParam(name = "arg1", partName = "arg1")
        String arg1,
        @WebParam(name = "arg2", partName = "arg2")
        String arg2)
        throws WebServiceException
    ;

    /**
     * 
     * @param arg2
     * @param arg1
     * @param arg0
     * @return
     *     returns java.lang.String
     * @throws WebServiceException
     */
    @WebMethod(operationName = "InserirTramiteHistoricoRetorno")
    @WebResult(partName = "return")
    @Action(input = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_TramiteEndPoint/InserirTramiteHistoricoRetornoRequest", output = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_TramiteEndPoint/InserirTramiteHistoricoRetornoResponse", fault = {
        @FaultAction(className = WebServiceException.class, value = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_TramiteEndPoint/InserirTramiteHistoricoRetorno/Fault/WebServiceException")
    })
    public String inserirTramiteHistoricoRetorno(
        @WebParam(name = "arg0", partName = "arg0")
        String arg0,
        @WebParam(name = "arg1", partName = "arg1")
        String arg1,
        @WebParam(name = "arg2", partName = "arg2")
        String arg2)
        throws WebServiceException
    ;

    /**
     * 
     * @param arg0
     * @return
     *     returns java.lang.String
     * @throws WebServiceException
     */
    @WebMethod(operationName = "Consultar_Tramites")
    @WebResult(partName = "return")
    @Action(input = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_TramiteEndPoint/Consultar_TramitesRequest", output = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_TramiteEndPoint/Consultar_TramitesResponse", fault = {
        @FaultAction(className = WebServiceException.class, value = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_TramiteEndPoint/Consultar_Tramites/Fault/WebServiceException")
    })
    public String consultarTramites(
        @WebParam(name = "arg0", partName = "arg0")
        String arg0)
        throws WebServiceException
    ;

    /**
     * 
     * @param arg3
     * @param arg2
     * @param arg5
     * @param arg4
     * @param arg1
     * @param strOS
     * @return
     *     returns java.lang.String
     * @throws WebServiceException
     */
    @WebMethod(operationName = "AlterarTramite")
    @WebResult(partName = "return")
    @Action(input = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_TramiteEndPoint/AlterarTramiteRequest", output = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_TramiteEndPoint/AlterarTramiteResponse", fault = {
        @FaultAction(className = WebServiceException.class, value = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_TramiteEndPoint/AlterarTramite/Fault/WebServiceException")
    })
    public String alterarTramite(
        @WebParam(name = "arg0", partName = "arg0")
        String strOS,
        @WebParam(name = "arg1", partName = "arg1")
        String arg1,
        @WebParam(name = "arg2", partName = "arg2")
        String arg2,
        @WebParam(name = "arg3", partName = "arg3")
        String arg3,
        @WebParam(name = "arg4", partName = "arg4")
        String arg4,
        @WebParam(name = "arg5", partName = "arg5")
        String arg5)
        throws WebServiceException
    ;

    /**
     * 
     * @param arg0
     * @return
     *     returns java.lang.String
     * @throws WebServiceException
     */
    @WebMethod(operationName = "ConsultarOSEncerradas")
    @WebResult(partName = "return")
    @Action(input = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_TramiteEndPoint/ConsultarOSEncerradasRequest", output = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_TramiteEndPoint/ConsultarOSEncerradasResponse", fault = {
        @FaultAction(className = WebServiceException.class, value = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_TramiteEndPoint/ConsultarOSEncerradas/Fault/WebServiceException")
    })
    public String consultarOSEncerradas(
        @WebParam(name = "arg0", partName = "arg0")
        String arg0)
        throws WebServiceException
    ;

}
