package com.altec.bsbr.app.hyb.dto;

public class ObjRsNotiEnv {
	private String NR_SEQU_ORDE_SERV;
	private String NM_NOTI;
	private String DATA_RET;
	private String TX_MESG;

	public ObjRsNotiEnv() {}

	public ObjRsNotiEnv(String nR_SEQU_ORDE_SERV, String nM_NOTI, String dATA_RET, String tX_MESG) {
		super();
		NR_SEQU_ORDE_SERV = nR_SEQU_ORDE_SERV;
		NM_NOTI = nM_NOTI;
		DATA_RET = dATA_RET;
		TX_MESG = tX_MESG;
	}

	public String getNR_SEQU_ORDE_SERV() {
		return NR_SEQU_ORDE_SERV;
	}

	public void setNR_SEQU_ORDE_SERV(String nR_SEQU_ORDE_SERV) {
		NR_SEQU_ORDE_SERV = nR_SEQU_ORDE_SERV;
	}

	public String getNM_NOTI() {
		return NM_NOTI;
	}

	public void setNM_NOTI(String nM_NOTI) {
		NM_NOTI = nM_NOTI;
	}

	public String getDATA_RET() {
		return DATA_RET;
	}

	public void setDATA_RET(String dATA_RET) {
		DATA_RET = dATA_RET;
	}

	public String getTX_MESG() {
		return TX_MESG;
	}

	public void setTX_MESG(String tX_MESG) {
		TX_MESG = tX_MESG;
	}
}
