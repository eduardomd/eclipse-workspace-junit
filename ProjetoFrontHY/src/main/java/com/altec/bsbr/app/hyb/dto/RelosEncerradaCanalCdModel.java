package com.altec.bsbr.app.hyb.dto;

public class RelosEncerradaCanalCdModel {
	
	private int tipoTabela;
	private String nOS;
	private String dataAbertura;
	private String dataEvento;
	private String dataEncerramento;
	private String agencia;
	private String cc;
	private String cliente;
	private String pfPj;
	private double valorEnvolvido;
	private double valorPrejuizo;
	private String analista;
	
	private String solicitacao;
	private String ativacao;
	private String cancelamento;
	private String indeferidas;
	private String clienteInformou;
	private String ressarcido;
	
	private String tipoProduto;
	private String operacoes;
	private int quantidade;
	
	private double valorTotal;
	private double percentual;
	private double valorExcluido;
	private double valorRecuperado;
	
	private String clienteAjudadoPor3;
	private String nomeAgencia;
	private String cartaoRecebido;
	private String cartaoRetido;
	private String interesse;
	
	private String fragilizacao;
	private String horario;
	private int codigoAgencia;
	private String descricaoAgencia;
	private String dataOcorrenciaPoc;
	private int nTerminal;
	private String possuiSensor;
	private String outros;
	private String nomeEstabelecimento;
	private int row;
	
	public RelosEncerradaCanalCdModel() {
		
	}

	public int getTipoTabela() {
		return tipoTabela;
	}

	public void setTipoTabela(int tipoTabela) {
		this.tipoTabela = tipoTabela;
	}

	public String getnOS() {
		return nOS;
	}

	public void setnOS(String nOS) {
		this.nOS = nOS;
	}

	public String getDataAbertura() {
		return dataAbertura;
	}

	public void setDataAbertura(String dataAbertura) {
		this.dataAbertura = dataAbertura;
	}

	public String getDataEvento() {
		return dataEvento;
	}

	public void setDataEvento(String dataEvento) {
		this.dataEvento = dataEvento;
	}

	public String getDataEncerramento() {
		return dataEncerramento;
	}

	public void setDataEncerramento(String dataEncerramento) {
		this.dataEncerramento = dataEncerramento;
	}

	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public String getPfPj() {
		return pfPj;
	}

	public void setPfPj(String pfPj) {
		this.pfPj = pfPj;
	}

	public double getValorEnvolvido() {
		return valorEnvolvido;
	}

	public void setValorEnvolvido(double valorEnvolvido) {
		this.valorEnvolvido = valorEnvolvido;
	}

	public double getValorPrejuizo() {
		return valorPrejuizo;
	}

	public void setValorPrejuizo(double valorPrejuizo) {
		this.valorPrejuizo = valorPrejuizo;
	}

	public String getAnalista() {
		return analista;
	}

	public void setAnalista(String analista) {
		this.analista = analista;
	}

	public String getSolicitacao() {
		return solicitacao;
	}

	public void setSolicitacao(String solicitacao) {
		this.solicitacao = solicitacao;
	}

	public String getAtivacao() {
		return ativacao;
	}

	public void setAtivacao(String ativacao) {
		this.ativacao = ativacao;
	}

	public String getCancelamento() {
		return cancelamento;
	}

	public void setCancelamento(String cancelamento) {
		this.cancelamento = cancelamento;
	}

	public String getIndeferidas() {
		return indeferidas;
	}

	public void setIndeferidas(String indeferidas) {
		this.indeferidas = indeferidas;
	}

	public String getClienteInformou() {
		return clienteInformou;
	}

	public void setClienteInformou(String clienteInformou) {
		this.clienteInformou = clienteInformou;
	}

	public String getRessarcido() {
		return ressarcido;
	}

	public void setRessarcido(String ressarcido) {
		this.ressarcido = ressarcido;
	}

	public String getTipoProduto() {
		return tipoProduto;
	}

	public void setTipoProduto(String tipoProduto) {
		this.tipoProduto = tipoProduto;
	}

	public String getOperacoes() {
		return operacoes;
	}

	public void setOperacoes(String operacoes) {
		this.operacoes = operacoes;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(double valorTotal) {
		this.valorTotal = valorTotal;
	}

	public double getPercentual() {
		return percentual;
	}

	public void setPercentual(double percentual) {
		this.percentual = percentual;
	}

	public double getValorExcluido() {
		return valorExcluido;
	}

	public void setValorExcluido(double valorExcluido) {
		this.valorExcluido = valorExcluido;
	}

	public double getValorRecuperado() {
		return valorRecuperado;
	}

	public void setValorRecuperado(double valorRecuperado) {
		this.valorRecuperado = valorRecuperado;
	}

	public String getClienteAjudadoPor3() {
		return clienteAjudadoPor3;
	}

	public void setClienteAjudadoPor3(String clienteAjudadoPor3) {
		this.clienteAjudadoPor3 = clienteAjudadoPor3;
	}

	public String getNomeAgencia() {
		return nomeAgencia;
	}

	public void setNomeAgencia(String nomeAgencia) {
		this.nomeAgencia = nomeAgencia;
	}

	public String getCartaoRecebido() {
		return cartaoRecebido;
	}

	public void setCartaoRecebido(String cartaoRecebido) {
		this.cartaoRecebido = cartaoRecebido;
	}

	public String getCartaoRetido() {
		return cartaoRetido;
	}

	public void setCartaoRetido(String cartaoRetido) {
		this.cartaoRetido = cartaoRetido;
	}

	public String getInteresse() {
		return interesse;
	}

	public void setInteresse(String interesse) {
		this.interesse = interesse;
	}

	public String getFragilizacao() {
		return fragilizacao;
	}

	public void setFragilizacao(String fragilizacao) {
		this.fragilizacao = fragilizacao;
	}

	public String getHorario() {
		return horario;
	}

	public void setHorario(String horario) {
		this.horario = horario;
	}

	public int getCodigoAgencia() {
		return codigoAgencia;
	}

	public void setCodigoAgencia(int codigoAgencia) {
		this.codigoAgencia = codigoAgencia;
	}

	public String getDescricaoAgencia() {
		return descricaoAgencia;
	}

	public void setDescricaoAgencia(String descricaoAgencia) {
		this.descricaoAgencia = descricaoAgencia;
	}

	public String getDataOcorrenciaPoc() {
		return dataOcorrenciaPoc;
	}

	public void setDataOcorrenciaPoc(String dataOcorrenciaPoc) {
		this.dataOcorrenciaPoc = dataOcorrenciaPoc;
	}

	public int getnTerminal() {
		return nTerminal;
	}

	public void setnTerminal(int nTerminal) {
		this.nTerminal = nTerminal;
	}

	public String getPossuiSensor() {
		return possuiSensor;
	}

	public void setPossuiSensor(String possuiSensor) {
		this.possuiSensor = possuiSensor;
	}

	public String getOutros() {
		return outros;
	}

	public void setOutros(String outros) {
		this.outros = outros;
	}

	public String getNomeEstabelecimento() {
		return nomeEstabelecimento;
	}

	public void setNomeEstabelecimento(String nomeEstabelecimento) {
		this.nomeEstabelecimento = nomeEstabelecimento;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}
	
	
	
}
