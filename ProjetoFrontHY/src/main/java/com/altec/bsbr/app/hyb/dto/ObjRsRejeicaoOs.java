package com.altec.bsbr.app.hyb.dto;

public class ObjRsRejeicaoOs {
	private String TXTREJEICAO;
	private String DATA;
	private String USUARIO;
	
	public ObjRsRejeicaoOs() {}

	public ObjRsRejeicaoOs(String tXTREJEICAO, String dATA, String uSUARIO) {
		super();
		TXTREJEICAO = tXTREJEICAO;
		DATA = dATA;
		USUARIO = uSUARIO;
	}

	public String getTXTREJEICAO() {
		return TXTREJEICAO;
	}

	public void setTXTREJEICAO(String tXTREJEICAO) {
		TXTREJEICAO = tXTREJEICAO;
	}

	public String getDATA() {
		return DATA;
	}

	public void setDATA(String dATA) {
		DATA = dATA;
	}

	public String getUSUARIO() {
		return USUARIO;
	}

	public void setUSUARIO(String uSUARIO) {
		USUARIO = uSUARIO;
	}	
	
}
