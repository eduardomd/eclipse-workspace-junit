package com.altec.bsbr.app.hyb.dto;

public class CadosParticipacaoProcesso {
	
	private String strNrSeqOs;
	private String strDelNrSeqPart;

	public CadosParticipacaoProcesso() {}
	
	public CadosParticipacaoProcesso(String strNrSeqOs, String strDelNrSeqPart) {
		this.strNrSeqOs = strNrSeqOs;
		this.strDelNrSeqPart = strDelNrSeqPart;
	}
	
	public String getStrNrSeqOs() {
		return strNrSeqOs;
	}
	
	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}
	
	public String getStrDelNrSeqPart() {
		return strDelNrSeqPart;
	}
	
	public void setStrDelNrSeqPart(String strDelNrSeqPart) {
		this.strDelNrSeqPart = strDelNrSeqPart;
	}
	
	
}
