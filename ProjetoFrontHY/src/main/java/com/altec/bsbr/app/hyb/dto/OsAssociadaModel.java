package com.altec.bsbr.app.hyb.dto;

public class OsAssociadaModel {
	private String ordem;
	private String area;
	private String status;
	
	public OsAssociadaModel(String ordem, String area, String status) {
		super();
		this.ordem = ordem;
		this.area = area;
		this.status = status;
	}
	
	public OsAssociadaModel() {
		
	}

	public String getOrdem() {
		return ordem;
	}

	public void setOrdem(String ordem) {
		this.ordem = ordem;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}	
}
