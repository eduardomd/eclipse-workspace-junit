package com.altec.bsbr.app.hyb.dto;


public class ObjRsCombo {
	private String CODIGO;
	private String NOME;
	private String NR_NOTI;
	private String NM_NOTI;
	
	public ObjRsCombo() {}
	
	public ObjRsCombo(String cODIGO, String nOME, String nR_NOTI, String nM_NOTI) {
		CODIGO = cODIGO;
		NOME = nOME;
		NR_NOTI = nR_NOTI;
		NM_NOTI = nM_NOTI;
	}

	public String getCODIGO() {
		return CODIGO;
	}

	public void setCODIGO(String cODIGO) {
		CODIGO = cODIGO;
	}

	public String getNOME() {
		return NOME;
	}

	public void setNOME(String nOME) {
		NOME = nOME;
	}

	public String getNR_NOTI() {
		return NR_NOTI;
	}

	public void setNR_NOTI(String nR_NOTI) {
		NR_NOTI = nR_NOTI;
	}

	public String getNM_NOTI() {
		return NM_NOTI;
	}

	public void setNM_NOTI(String nM_NOTI) {
		NM_NOTI = nM_NOTI;
	}

}

