package com.altec.bsbr.app.hyb.dto;

public class CadastroOsDefaultModel {

	private String cdCanal;
	private String strEvento;
	//private String objEvento; // objeto xHY_CadOsEventos.clsCadOsEventos
	//private String objRsEvento; // RecordSet de retorno dos detalhes da Fraude Canal
	private String strErro; // String de Erro
	private String strNrSeqId; // String que armazena o número sequencial da Fraude Canal
	private boolean blnSalvar; // Indicador de gravação
	private String strAgencia; // Número da Agência
	private String strConta; // Numero da Conta
	private String strCartao; // Número Cartão

	/* variables used in objRsEvento */
	private String strNrSeqOs; // String número da OS
	private String strTitular; // Nome do Titular
	private String strSeqConta; // Número Sequencial do Conta
	private String strSeqCartao; // Número Sequencial do Cartão
	private String strCdCanal; // Código do Canal
	private String strSeqEvento; // Número Sequencial da Fraude Evento
	private String strCpfCnpj; // Número do CPF CNPJ
	private String strNrQntdEven; // Quantidade de eventos
	private String strCdCentroOrigem; // Centro de origem pCD_CTRO_ORIG
	private String strCdCentroDestino; // Centro de destino pCD_CTRO_DEST
	private String strCdCentroOperante; // Centro operante pCD_CTRO_OPER
	private String strContabilizado; // valida contablização

	public CadastroOsDefaultModel() {

	}

	public CadastroOsDefaultModel(String strNrSeqOs, String strTitular, String strSeqConta, String strSeqCartao,
			String strCdCanal, String strSeqEvento, String strCpfCnpj, String strNrQntdEven, String strCdCentroOrigem,
			String strCdCentroDestino, String strCdCentroOperante, String strContabilizado, String strErro) {

		this.strNrSeqOs = strNrSeqOs;
		this.strTitular = strTitular;
		this.strSeqConta = strSeqConta;
		this.strSeqCartao = strSeqCartao;
		this.strCdCanal = strCdCanal;
		this.strSeqEvento = strSeqEvento;
		this.strCpfCnpj = strCpfCnpj;
		this.strNrQntdEven = strNrQntdEven;
		this.strCdCentroOrigem = strCdCentroOrigem;
		this.strCdCentroDestino = strCdCentroDestino;
		this.strCdCentroOperante = strCdCentroOperante;
		this.strContabilizado = strContabilizado;
		this.strErro = strErro;

	}

	public String getCdCanal() {
		return cdCanal;
	}

	public void setCdCanal(String cdCanal) {
		this.cdCanal = cdCanal;
	}

	public String getStrEvento() {
		return strEvento;
	}

	public void setStrEvento(String strEvento) {
		this.strEvento = strEvento;
	}

/*	public String getObjEvento() {
		return objEvento;
	}

	public void setObjEvento(String objEvento) {
		this.objEvento = objEvento;
	}*/

/*	public String getObjRsEvento() {
		return objRsEvento;
	}

	public void setObjRsEvento(String objRsEvento) {
		this.objRsEvento = objRsEvento;
	}*/

	public String getStrErro() {
		return strErro;
	}

	public void setStrErro(String strErro) {
		this.strErro = strErro;
	}

	public String getStrNrSeqOs() {
		return strNrSeqOs;
	}

	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}

	public String getStrNrSeqId() {
		return strNrSeqId;
	}

	public void setStrNrSeqId(String strNrSeqId) {
		this.strNrSeqId = strNrSeqId;
	}

	public boolean isBlnSalvar() {
		return blnSalvar;
	}

	public void setBlnSalvar(boolean blnSalvar) {
		this.blnSalvar = blnSalvar;
	}

	public String getStrTitular() {
		return strTitular;
	}

	public void setStrTitular(String strTitular) {
		this.strTitular = strTitular;
	}

	public String getStrSeqConta() {
		return strSeqConta;
	}

	public void setStrSeqConta(String strSeqConta) {
		this.strSeqConta = strSeqConta;
	}

	public String getStrSeqCartao() {
		return strSeqCartao;
	}

	public void setStrSeqCartao(String strSeqCartao) {
		this.strSeqCartao = strSeqCartao;
	}

	public String getStrAgencia() {
		return strAgencia;
	}

	public void setStrAgencia(String strAgencia) {
		this.strAgencia = strAgencia;
	}

	public String getStrConta() {
		return strConta;
	}

	public void setStrConta(String strConta) {
		this.strConta = strConta;
	}

	public String getStrCartao() {
		return strCartao;
	}

	public void setStrCartao(String strCartao) {
		this.strCartao = strCartao;
	}

	public String getStrCdCanal() {
		return strCdCanal;
	}

	public void setStrCdCanal(String strCdCanal) {
		this.strCdCanal = strCdCanal;
	}

	public String getStrSeqEvento() {
		return strSeqEvento;
	}

	public void setStrSeqEvento(String strSeqEvento) {
		this.strSeqEvento = strSeqEvento;
	}

	public String getStrCpfCnpj() {
		return strCpfCnpj;
	}

	public void setStrCpfCnpj(String strCpfCnpj) {
		this.strCpfCnpj = strCpfCnpj;
	}

	public String getStrNrQntdEven() {
		return strNrQntdEven;
	}

	public void setStrNrQntdEven(String strNrQntdEven) {
		this.strNrQntdEven = strNrQntdEven;
	}

	public String getStrCdCentroOrigem() {
		return strCdCentroOrigem;
	}

	public void setStrCdCentroOrigem(String strCdCentroOrigem) {
		this.strCdCentroOrigem = strCdCentroOrigem;
	}

	public String getStrCdCentroDestino() {
		return strCdCentroDestino;
	}

	public void setStrCdCentroDestino(String strCdCentroDestino) {
		this.strCdCentroDestino = strCdCentroDestino;
	}

	public String getStrCdCentroOperante() {
		return strCdCentroOperante;
	}

	public void setStrCdCentroOperante(String strCdCentroOperante) {
		this.strCdCentroOperante = strCdCentroOperante;
	}

	public String getStrContabilizado() {
		return strContabilizado;
	}

	public void setStrContabilizado(String strContabilizado) {
		this.strContabilizado = strContabilizado;
	}

}


