package com.altec.bsbr.app.hyb.dto;

public class CanalOrigEvento {
	private String stErro;
	private String CODIGO;
	
	public CanalOrigEvento() {}
	
	public String getStErro() {
		return stErro;
	}
	public void setStErro(String stErro) {
		this.stErro = stErro;
	}

	public String getCODIGO() {
		return CODIGO;
	}

	public void setCODIGO(String cODIGO) {
		CODIGO = cODIGO;
	}
	
	
}
