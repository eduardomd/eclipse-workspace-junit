package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.MotPenaModel;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmPenalidade.XHYAdmPenalidadeEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;

@Component("AdmGerManutMotPenaBean")
@Scope("session")
public class AdmGerManutMotPenaBean extends BasicBBean {

	private static final long serialVersionUID = 1L;

	private Object strMsg = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
			.get("strMsg");
	private String strErro;

	private List<MotPenaModel> objAdmMotiPenl;
	private String[] objRsAdmPenl;

	private String txtAddMotiPenalidade;
	private String txtHdCodMotiPenalidade;
	private String txtHdMotiPenalidade;
	private String txtHdAuxMotiPenalidade;
	private String txtHdUnload;

	@Autowired
	private XHYAdmPenalidadeEndPoint admpena;

	public void setupObjAdmMotiPenl() {
		String retorno = "";
		this.objAdmMotiPenl = new ArrayList<MotPenaModel>();

		try {
			retorno = admpena.consultarMotiPenalidade();
			System.out.println("retorno consultarMotiPenalidade "+retorno);
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}

		JSONObject jsonRetorno = new JSONObject(retorno);
		JSONArray pcursor = jsonRetorno.getJSONArray("PCURSOR");

		for (int i = 0; i < pcursor.length(); i++) {
			JSONObject curr = pcursor.getJSONObject(i);
			this.objAdmMotiPenl.add(new MotPenaModel(String.valueOf(curr.getInt("CODIGO")),
					curr.isNull("NOME") ? "" : curr.getString("NOME"),
					curr.isNull("EXCLUSAO") ? "" : String.valueOf(curr.getInt("EXCLUSAO"))));
		}
	}

	@PostConstruct
	public void init() {

		// ---- mock
//		for(int c = 1; c <= 9; c++)	{
//			this.objAdmMotiPenl.add(new MotPenaModel(c, "Motivo " + c));
//		}		
		// ----

		setupObjAdmMotiPenl();
	}

	@SuppressWarnings("unchecked")
	public void adicionar() {
		Map<String, String> requestParamMap = FacesContext.getCurrentInstance().getExternalContext()
				.getRequestParameterMap();
		String retorno = "";
		String motiAdd = requestParamMap.get("frm:txtHdMotiPenalidade");
		System.out.println("motiAdd "+motiAdd);
			
		try {
			retorno = admpena.incluirMotiPenalidade(motiAdd);
			setupObjAdmMotiPenl();
			setTxtAddMotiPenalidade(null);
			RequestContext.getCurrentInstance().execute("alert('Motivo da penalidade incluído com sucesso!')");
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void deletar() {
		Map<String, String> requestParamMap = FacesContext.getCurrentInstance().getExternalContext()
				.getRequestParameterMap();
		String retorno = "";
		String txtHdCodMotiPenalidade = requestParamMap.get("frm:txtHdCodMotiPenalidade");
		System.out.println("txtHdCodMotiPenalidade "+txtHdCodMotiPenalidade);

		try {
			retorno = admpena.apagarMotiPenalidade(Long.valueOf(requestParamMap.get("frm:txtHdCodMotiPenalidade")));
			setTxtHdCodMotiPenalidade(null);			
			setupObjAdmMotiPenl();
			RequestContext.getCurrentInstance().execute("alert('Motivo da penalidade excluído com sucesso!')");
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void salvar() {
		Map<String, String> requestParamMap = FacesContext.getCurrentInstance().getExternalContext()
				.getRequestParameterMap();
		String motivos = requestParamMap.get("frm:txtHdAuxMotiPenalidade");

		try {
			admpena.arrayAlterarMotiPenalidade(motivos);
			setupObjAdmMotiPenl();			
			RequestContext.getCurrentInstance().execute("alert('Alterações efetuadas com sucesso!')");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getMotivosAsJson() {
		JSONArray jsonArray = new JSONArray(objAdmMotiPenl);
		return jsonArray.toString();
	}

	public String getStrMsg() {
		if (strMsg == null) {
			return "";
		}
		return strMsg.toString();
	}

	public void setStrMsg(String strMsg) {
		this.strMsg = strMsg;
	}

	public String getStrErro() {
		return strErro;
	}

	public void setStrErro(String strErro) {
		this.strErro = strErro;
	}

	public List<MotPenaModel> getObjAdmMotiPenl() {
		return objAdmMotiPenl;
	}

	public void setObjAdmMotiPenl(List<MotPenaModel> objAdmMotiPenl) {
		this.objAdmMotiPenl = objAdmMotiPenl;
	}

	public String[] getObjRsAdmPenl() {
		return objRsAdmPenl;
	}

	public void setObjRsAdmPenl(String[] objRsAdmPenl) {
		this.objRsAdmPenl = objRsAdmPenl;
	}

	public String getTxtAddMotiPenalidade() {
		return txtAddMotiPenalidade;
	}

	public void setTxtAddMotiPenalidade(String txtAddMotiPenalidade) {
		this.txtAddMotiPenalidade = txtAddMotiPenalidade;
	}

	public String getTxtHdCodMotiPenalidade() {
		return txtHdCodMotiPenalidade;
	}

	public void setTxtHdCodMotiPenalidade(String txtHdCodMotiPenalidade) {
		this.txtHdCodMotiPenalidade = txtHdCodMotiPenalidade;
	}

	public String getTxtHdMotiPenalidade() {
		return txtHdMotiPenalidade;
	}

	public void setTxtHdMotiPenalidade(String txtHdMotiPenalidade) {
		this.txtHdMotiPenalidade = txtHdMotiPenalidade;
	}

	public String getTxtHdAuxMotiPenalidade() {
		return txtHdAuxMotiPenalidade;
	}

	public void setTxtHdAuxMotiPenalidade(String txtHdAuxMotiPenalidade) {
		this.txtHdAuxMotiPenalidade = txtHdAuxMotiPenalidade;
	}

	public String getTxtHdUnload() {
		return txtHdUnload;
	}

	public void setTxtHdUnload(String txtHdUnload) {
		this.txtHdUnload = txtHdUnload;
	}

}
