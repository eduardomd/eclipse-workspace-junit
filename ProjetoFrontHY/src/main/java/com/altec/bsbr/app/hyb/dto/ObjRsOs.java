package com.altec.bsbr.app.hyb.dto;

public class ObjRsOs {
	
	private String NOME;
	private String SITUACAO;
	private String CD_NOTI;
	private String NM_NOTI;
	private String CODIGO;
	private String DT_ABERTURA;
	private String DT_RELATORIO;
	private String VL_ENVOLVIDO;
	private String VL_RECUPERADO;
	private String VL_PREJUIZO;
	private String AREA;
	private String TX_ABERTURA;
	private String TX_FALHAS;
	private String TX_ENCERRAMENTO;
	private String TX_OUTRAS_PROP;
	private String PARECER_JURI;
	private String MOT_REC_APROV;
	private String CD_SITU;
	private String CD_SITUACAO;
	private String IN_ENVI_RISC_OPER;
	private String CRITICIDADE;
	private String FASE_ANALISE;
	
	public ObjRsOs() {};
	
	public String getMOT_REC_APROV() {
		return MOT_REC_APROV;
	}

	public void setMOT_REC_APROV(String mOT_REC_APROV) {
		MOT_REC_APROV = mOT_REC_APROV;
	}
	
	public String NOME() {
		return NOME;
	}

	public void setNOME(String nOME) {
		NOME = nOME;
	}

	public String getSITUACAO() {
		return SITUACAO;
	}

	public void setSITUACAO(String sITUACAO) {
		SITUACAO = sITUACAO;
	}

	public String getCD_NOTI() {
		return CD_NOTI;
	}

	public void setCD_NOTI(String cD_NOTI) {
		CD_NOTI = cD_NOTI;
	}

	public String getNM_NOTI() {
		return NM_NOTI;
	}

	public void setNM_NOTI(String nM_NOTI) {
		NM_NOTI = nM_NOTI;
	}

	public String getCODIGO() {
		return CODIGO;
	}

	public void setCODIGO(String cODIGO) {
		CODIGO = cODIGO;
	}

	public String getDT_ABERTURA() {
		return DT_ABERTURA;
	}

	public void setDT_ABERTURA(String dT_ABERTURA) {
		DT_ABERTURA = dT_ABERTURA;
	}

	public String getDT_RELATORIO() {
		return DT_RELATORIO;
	}

	public void setDT_RELATORIO(String dT_RELATORIO) {
		DT_RELATORIO = dT_RELATORIO;
	}

	public String getVL_ENVOLVIDO() {
		return VL_ENVOLVIDO;
	}

	public void setVL_ENVOLVIDO(String vL_ENVOLVIDO) {
		VL_ENVOLVIDO = vL_ENVOLVIDO;
	}

	public String getVL_RECUPERADO() {
		return VL_RECUPERADO;
	}

	public void setVL_RECUPERADO(String vL_RECUPERADO) {
		VL_RECUPERADO = vL_RECUPERADO;
	}

	public String getVL_PREJUIZO() {
		return VL_PREJUIZO;
	}

	public void setVL_PREJUIZO(String vL_PREJUIZO) {
		VL_PREJUIZO = vL_PREJUIZO;
	}

	public String getAREA() {
		return AREA;
	}

	public void setAREA(String aREA) {
		AREA = aREA;
	}

	public String getTX_ABERTURA() {
		return TX_ABERTURA;
	}

	public void setTX_ABERTURA(String tX_ABERTURA) {
		TX_ABERTURA = tX_ABERTURA;
	}

	public String getTX_FALHAS() {
		return TX_FALHAS;
	}

	public void setTX_FALHAS(String tX_FALHAS) {
		TX_FALHAS = tX_FALHAS;
	}

	public String getTX_ENCERRAMENTO() {
		return TX_ENCERRAMENTO;
	}

	public void setTX_ENCERRAMENTO(String tX_ENCERRAMENTO) {
		TX_ENCERRAMENTO = tX_ENCERRAMENTO;
	}

	public String getTX_OUTRAS_PROP() {
		return TX_OUTRAS_PROP;
	}

	public void setTX_OUTRAS_PROP(String tX_OUTRAS_PROP) {
		TX_OUTRAS_PROP = tX_OUTRAS_PROP;
	}

	public String getPARECER_JURI() {
		return PARECER_JURI;
	}

	public void setPARECER_JURI(String pARECER_JURI) {
		PARECER_JURI = pARECER_JURI;
	}
	
	public String getCD_SITU() {
		return CD_SITU;
	}

	public void setCD_SITU(String cD_SITU) {
		CD_SITU = cD_SITU;
	}

	public String getCD_SITUACAO() {
		return CD_SITUACAO;
	}

	public void setCD_SITUACAO(String cD_SITUACAO) {
		CD_SITUACAO = cD_SITUACAO;
	}

	public String getIN_ENVI_RISC_OPER() {
		return IN_ENVI_RISC_OPER;
	}

	public void setIN_ENVI_RISC_OPER(String iN_ENVI_RISC_OPER) {
		IN_ENVI_RISC_OPER = iN_ENVI_RISC_OPER;
	}

	public String getCRITICIDADE() {
		return CRITICIDADE;
	}

	public void setCRITICIDADE(String cRITICIDADE) {
		CRITICIDADE = cRITICIDADE;
	}

	public String getFASE_ANALISE() {
		return FASE_ANALISE;
	}

	public void setFASE_ANALISE(String fASE_ANALISE) {
		FASE_ANALISE = fASE_ANALISE;
	}


}
