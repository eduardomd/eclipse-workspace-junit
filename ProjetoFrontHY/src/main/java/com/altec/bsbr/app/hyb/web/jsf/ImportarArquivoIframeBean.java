package com.altec.bsbr.app.hyb.web.jsf;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.model.UploadedFile;

@ManagedBean(name="ImportarArquivoIframeBean")
@ViewScoped
public class ImportarArquivoIframeBean implements Serializable {
	private static final long serialVersionUID = 1L;
    
	private String strNmArqAnex;
	private String strNmArquivo;
	
	/* TODO Ver o path correto para salvar arquivos */
	/* strCaminhoServidor = objGeral.GetConfig("anexo", "treinamento", strErro) */
	private final String strCaminhoServidor = "C:\\backlevel\\anexos\\";

	private UploadedFile arquivoUpload;

	/* Query String */
	private Object strFlg = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strFlg");
	private Object strCaminho = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strCaminho");
	
	
	public void formSubmit() {
		try {
			if (arquivoUpload == null)
				throw new Exception("Arquivo nulo");
			
			this.strNmArqAnex = this.arquivoUpload.getFileName();
			
			File file = new File(strCaminhoServidor, arquivoUpload.getFileName());
	
			OutputStream out = new FileOutputStream(file);
			out.write(arquivoUpload.getContents());
			out.close();
	
			System.out.println("O arquivo '"+arquivoUpload.getFileName()+"' foi salvo com sucesso no path '"+this.strCaminhoServidor+"'"); //apenas para debug
		} catch (Exception e) {
			/* TODO Tratamento de erro */
			e.printStackTrace();
		}
		
		/* TODO Chamada ao backend, instanciar componente e chamar metodo para obter strCaminho */
		
		strNmArqAnex = strNmArqAnex + strCaminho + "|";
		
	    strNmArquivo = strNmArquivo + arquivoUpload.getFileName() + "|";
		
		RequestContext.getCurrentInstance().execute("alert('Arquivo anexado com sucesso!')");

	}
	
	public UploadedFile getArquivoUpload() {
		return arquivoUpload;
	}

	public void setArquivoUpload(UploadedFile arquivoUpload) {
		this.arquivoUpload = arquivoUpload;
	}
	
	public String getStrNmArqAnex() {
		return strNmArqAnex;
	}
	public void setStrNmArqAnex(String strNmArqAnex) {
		this.strNmArqAnex = strNmArqAnex;
	}	
	
	public Object getStrFlg() {
		return strFlg;
	}
	public void setStrFlg(Object strFlg) {
		this.strFlg = strFlg;
	}
	public Object getStrCaminho() {
		return strCaminho;
	}
	public void setStrCaminho(Object strCaminho) {
		this.strCaminho = strCaminho;
	}
	
	
}