package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.ObjRs;
import com.altec.bsbr.app.hyb.dto.ObjRsArea;
import com.altec.bsbr.app.hyb.dto.ObjRsAreaOs;
import com.altec.bsbr.app.hyb.dto.ObjRsCausaRaiz;
import com.altec.bsbr.app.hyb.dto.ObjRsCausaRaizOs;
import com.altec.bsbr.app.hyb.dto.ObjRsCriticidadeOs;
import com.altec.bsbr.app.hyb.dto.ObjRsFaseAnaliseOs;
import com.altec.bsbr.app.hyb.dto.ObjRsOs;
import com.altec.bsbr.app.hyb.dto.ObjRsOsContab;
import com.altec.bsbr.app.hyb.dto.ObjRsOsProvisao;
import com.altec.bsbr.app.hyb.dto.ObjRsOut;
import com.altec.bsbr.app.hyb.dto.ObjRsRejeicaoOs;
import com.altec.bsbr.app.hyb.dto.ObjRsSoliCancOs;
import com.altec.bsbr.app.hyb.dto.UsuarioIncModel;
import com.altec.bsbr.app.hyb.web.util.LogIncUtil;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAcoesOs.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAcoesOs.XHYAcoesOsEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmLog.XHYAdmLogEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsDadosContab.XHYCadOsDadosContabEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsDadosIniciais.XHYCadOsDadosIniciaisEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYUsuario.XHYUsuarioEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;

import com.altec.bsbr.fw.security.SecurityInfo;
import com.altec.bsbr.fw.security.authorization.Authorization;
import com.altec.bsbr.fw.security.authorization.menu.MBSMenuItem;


@Component("cadosDadosIniciaisBean")
@Scope("request")
public class CadosDadosIniciaisBean extends BasicBBean{
	
	private static final long serialVersionUID = 1L;

	@Autowired
	private XHYAcoesOsEndPoint acoesOsEndPoint;
	
	@Autowired
	private XHYCadOsDadosIniciaisEndPoint cadOsDadosIniciaisEndPoint;  
	
	@Autowired
	private XHYCadOsDadosContabEndPoint cadOsDadosContabEndPoint;
		
    @Autowired
    private XHYUsuarioEndPoint usuario;
    
    @Autowired
	private XHYAdmLogEndPoint log;

    @Autowired 
     private Authorization authorization;

    private UsuarioIncModel objRsUsuario;	
	
	private ObjRsOs objRsOs = new ObjRsOs();
	private ObjRsSoliCancOs objRsSoliCancOs = new ObjRsSoliCancOs();
	private ObjRsOsProvisao objRsOsProvisao = new ObjRsOsProvisao();
	private ObjRsCausaRaizOs objRsCausaRaizOs = new ObjRsCausaRaizOs();
	private ObjRsRejeicaoOs objRsRejeicaoOs = new ObjRsRejeicaoOs("", "", "");
	private ObjRsOsContab objRsOsContab = new ObjRsOsContab();
	private ObjRsAreaOs objRsAreaOs = new ObjRsAreaOs();
	private List<ObjRsCriticidadeOs> objRsCriticidadeOs;
	private List<ObjRsFaseAnaliseOs> objRsFaseAnaliseOs;
	private List<ObjRsCausaRaiz> objRsCausaRaiz;
	private List<ObjRsArea> objRsArea;
	private List<ObjRs> objRs;
	private ObjRsOut objRsOut = new ObjRsOut();
	
	private String strTitulo;
	private Boolean flgEnvRO = false;
	private int qtOsProvisao;
	private int qtOsRejeicao;
	private String txtHdFlgEnvRO = "0";
	private String strNrSeqOs;
	private String strReadOnly;
	private boolean alteracao;
	private boolean desabilitarCampos = false;
	
	@PostConstruct
	public void init() {
		this.strTitulo = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strTitulo");
		this.strNrSeqOs = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs");
		this.strReadOnly = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strReadOnly");
		buscarCriticidade();
		buscarFaseAnalise();
		buscarCausaRaiz();
		buscarArea();
		fnSelTipoMani();
		verificaQueryString();
		
		
		//Recupera o usuario corrente da rede
	       SecurityInfo secInfo = SecurityInfo.getCurrent(); 
	       String userName = secInfo.getUserName().replaceAll("[^0-9]", "");         

	             objRsUsuario = new UsuarioIncModel();
	             
	             if (userName.isEmpty()) {         
	              FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(
	                           FacesContext.getCurrentInstance(), null, "hyb_erro.xhtml?&strErro=Sessão Expirada!");
	       } else {
	        String retorno = "";
	             try {
	                    
	               retorno = usuario.recuperarDadosUsuario(userName);

	               JSONObject dadosUsuario = new JSONObject(retorno);
	               JSONArray pcursor = dadosUsuario.getJSONArray("PCURSOR");

	               if(pcursor.length() > 0) {
	   				JSONObject f = pcursor.getJSONObject(0);
	   				objRsUsuario = new UsuarioIncModel();
	   				objRsUsuario.setMatriculaUsuario(f.isNull("MATRICULA") == true ? "" : f.get("MATRICULA").toString());
	   				objRsUsuario.setNomeUsuario(f.isNull("NOME_USUARIO") == true ? "" : f.get("NOME_USUARIO").toString());
	   				objRsUsuario.setCpfUsuario(f.isNull("CPF") == true ? "" : f.get("CPF").toString());
	   				objRsUsuario.setLoginUsuario(f.isNull("LOGIN") == true ? "" : f.get("LOGIN").toString());
	   				objRsUsuario.setCdAreaUsuario(f.isNull("CODIGO_AREA") == true ? "" : f.get("CODIGO_AREA").toString());
	   				objRsUsuario.setNomeAreaUsuario(f.isNull("NOME_AREA") == true ? "" : f.get("NOME_AREA").toString());
	   				objRsUsuario.setNmEmailusuario(f.isNull("EMAIL") == true ? "" : f.get("EMAIL").toString());
	   				objRsUsuario.setCdCargoUsuario(f.isNull("CODIGO_CARGO") == true ? "" : f.get("CODIGO_CARGO").toString());
	               }
	               
	             } catch (Exception e) {
	     			try {
	    				FacesContext.getCurrentInstance().getExternalContext()
	    						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
	    			} catch (IOException e1) {
	    				// TODO Auto-generated catch block
	    				e1.printStackTrace();
	    			}
	    		}
	       }

	}
	
	public void verificaQueryString() {
		if (!(this.strReadOnly == null) && !this.strReadOnly.isEmpty()) {
			System.out.println(strReadOnly + " : strReadOnly ---");
			System.out.println(strReadOnly.equals("1"));
			if (this.strReadOnly.equals("1") ) {
				setDesabilitarCampos(true);
			} 
		}
		
		
		if (this.strNrSeqOs == null || strNrSeqOs.isEmpty()) {
			this.setAlteracao(false);
		}else {
			this.setAlteracao(true);
			this.buscarOs(strNrSeqOs);
			this.verificaValorCheckbox();
			this.fnSelDadosContabeis(this.strNrSeqOs);
			this.consultarAreaOs(this.strNrSeqOs);
			this.consultarSoliCancOsAtiva(this.strNrSeqOs);
			this.consultarOsContab(this.strNrSeqOs);
			this.consultarOsProvisao(this.strNrSeqOs);
			this.consultarCausaRaizOs(this.strNrSeqOs);
			this.consultarRejeicaoOs(this.strNrSeqOs);
		}		
	}
	
	public void verificaValorCheckbox() {
		if(this.getFlgEnvRO()) {
			if(objRsOs.getIN_ENVI_RISC_OPER().equals("0")) {
				setTxtHdFlgEnvRO("0");
			}
			if(objRsOs.getIN_ENVI_RISC_OPER().equals("1")) {
				setTxtHdFlgEnvRO("1");
			}
		}else {
			if(objRsOs.getIN_ENVI_RISC_OPER().equals("0")) {
				setTxtHdFlgEnvRO("0");
			}
			if(objRsOs.getIN_ENVI_RISC_OPER().equals("1")) {
				setTxtHdFlgEnvRO("1");
			}
		}
	
	}
	
	public void consultarRejeicaoOs(String strNrSeqOs) {
		try {
			JSONObject json = new JSONObject(cadOsDadosIniciaisEndPoint.consultarRejeicaoOs(this.getStrNrSeqOs()));
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			
			if(pcursor.length() > 0) {
				setQtOsRejeicao(1);
				JSONObject item = pcursor.getJSONObject(0);
				objRsRejeicaoOs.setDATA(item.get("DATA").toString());
				objRsRejeicaoOs.setTXTREJEICAO(item.get("TXTREJEICAO").toString());
				objRsRejeicaoOs.setUSUARIO(item.get("USUARIO").toString());
			}else {
				setQtOsRejeicao(0);
			}
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	public void consultarCausaRaizOs(String strNrSeqOs) {
		try {
			JSONObject json = new JSONObject(cadOsDadosIniciaisEndPoint.consultarCausaRaizOs(this.getStrNrSeqOs()));
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			
			if(pcursor.length() > 0) {
				JSONObject item = pcursor.getJSONObject(0);
				objRsCausaRaizOs.setCODIGO(item.get("CODIGO").toString());
			}
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	public void consultarOsProvisao(String strNrSeqOs) {
		try {
			JSONObject json = new JSONObject(cadOsDadosIniciaisEndPoint.consultarOsProvisao(this.getStrNrSeqOs()));
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			
			if(pcursor.length() > 0) {
				setQtOsProvisao(1);
				JSONObject item = pcursor.getJSONObject(0);
				objRsOsProvisao.setSITUACAO(item.getString("SITUACAO"));
				objRsOsProvisao.setVALOR(item.get("VALOR").toString());
			}else {
				setQtOsProvisao(0);
			}
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}		
	}
	
	public void consultarOsContab(String strNrSeqOs) {
		try {
			JSONObject json = new JSONObject(cadOsDadosIniciaisEndPoint.consultarOsContab(this.getStrNrSeqOs()));
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			if(pcursor.length() > 0) {
				JSONObject item = pcursor.getJSONObject(0);
				objRsOsContab.setSITUACAO(item.get("SITUACAO").toString());
			}
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	public void consultarSoliCancOsAtiva(String strNrSeqOs) {
		try {
			JSONObject json = new JSONObject(cadOsDadosIniciaisEndPoint.consultarSoliCancOsAtiva(this.getStrNrSeqOs()));
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			
			if(pcursor.length() > 0) {
				JSONObject item = pcursor.getJSONObject(0);
				objRsSoliCancOs.setVALOR(item.getDouble("VALOR"));
			}
			
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	public void consultarAreaOs(String strNrSeqOs) {
		try {
			JSONObject json = new JSONObject(cadOsDadosIniciaisEndPoint.consultarAreaOs(this.getStrNrSeqOs()));
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			
			if(pcursor.length() > 0) {
				JSONObject item = pcursor.getJSONObject(0);
				objRsAreaOs.setCODIGO(item.getString("CODIGO"));
			}
			
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}	
	}
	
	public void fnSelDadosContabeis(String strNrSeqOs) {
		try {
			JSONObject json = new JSONObject(cadOsDadosContabEndPoint.fnSelDadosContabeis(this.getStrNrSeqOs()));
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			
			if(pcursor.length() > 0) {
				JSONObject item = pcursor.getJSONObject(0);
				objRsOut.setCD_MANI(item.get("CD_MANI").toString());
			}
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}		
	}
	
	public void fnSelTipoMani(){
		try {
			objRs = new ArrayList<ObjRs>();
			JSONObject json = new JSONObject(cadOsDadosContabEndPoint.fnSelTipoMani());
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			
			for (int i = 0; i < pcursor.length(); i++) {
				JSONObject item = pcursor.getJSONObject(i);
				this.objRs.add(new ObjRs(item.getString("SQ_TIPO_MANI"), item.getString("DE_TIPO_MANI")));
			}
			
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
	}
	
	public void buscarOs(String strNrSeqOs) {
		try {
			String retorno = cadOsDadosIniciaisEndPoint.consultarOs(this.getStrNrSeqOs());
			JSONObject json = new JSONObject(retorno);
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			if(pcursor.length() > 0) {
				JSONObject item = pcursor.getJSONObject(0);
				
				//objRsOs.setAREA("AREA");
				objRsOs.setCODIGO(item.getString("CODIGO"));
				/*objRsOs.setDT_ABERTURA("14/12/2018");
				objRsOs.setDT_RELATORIO("15/12/2018");
				objRsOs.setPARECER_JURI("PARECER_JURI");*/
				objRsOs.setSITUACAO(item.getString("SITUACAO"));
				/*objRsOs.setTX_ABERTURA("TX_ABERTURA");
				objRsOs.setTX_ENCERRAMENTO("TX_ENCERRAMENTO");
				objRsOs.setTX_FALHAS("TX_FALHAS");
				objRsOs.setTX_OUTRAS_PROP("TX_OUTRAS_PROP");
				objRsOs.setVL_ENVOLVIDO("1000.00");
				objRsOs.setVL_PREJUIZO("400.00");
				objRsOs.setVL_RECUPERADO("600.00");*/
				
				objRsOs.setIN_ENVI_RISC_OPER(item.get("IN_ENVI_RISC_OPER").toString());
				
				if(!item.isNull("FASE_ANALISE")) {
					objRsOs.setFASE_ANALISE(item.get("FASE_ANALISE").toString());
				}
				
				if(!item.isNull("CRITICIDADE")) {
					objRsOs.setCRITICIDADE(item.get("CRITICIDADE").toString());
				}
				if(!item.isNull("CD_NOTI")) {
					objRsOs.setCD_NOTI(item.get("CD_NOTI").toString());
				}
				if(!item.isNull("NM_NOTI")) {
					objRsOs.setNM_NOTI(item.get("NM_NOTI").toString());
				}
				
				
				objRsOs.setCD_SITUACAO(item.getString("CD_SITUACAO"));
				System.out.println(objRsOs.getFASE_ANALISE() + " Antes");
				if(objRsOs.getCD_SITUACAO().equals("E")) {
					objRsOs.setFASE_ANALISE("5");
					System.out.println(objRsOs.getFASE_ANALISE() + " Depos");
				}
			}
			
		}catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	public void buscarArea() {
		try {
			objRsArea = new ArrayList<ObjRsArea>();
			JSONObject json = new JSONObject(cadOsDadosIniciaisEndPoint.consultarArea());
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			
			for (int i = 0; i < pcursor.length(); i++) {
				JSONObject item = pcursor.getJSONObject(i);
				
				this.objRsArea.add(new ObjRsArea(item.getString("CODIGO"), item.getString("NOME")));
			}
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		
	}
	
	public void buscarFaseAnalise() {
		try {
			objRsFaseAnaliseOs = new ArrayList<ObjRsFaseAnaliseOs>();
			String retorno = cadOsDadosIniciaisEndPoint.consultarFaseAnaliseOs();
			JSONObject json = new JSONObject(retorno);
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			
			for (int i = 0; i < pcursor.length(); i++) {
				JSONObject item = pcursor.getJSONObject(i);
				
				this.objRsFaseAnaliseOs.add(new ObjRsFaseAnaliseOs(String.valueOf(item.getInt("CODIGO")), item.getString("NOME")));
			}
		}catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	public void buscarCriticidade() {
		try {
			objRsCriticidadeOs = new ArrayList<ObjRsCriticidadeOs>();
			String retorno = cadOsDadosIniciaisEndPoint.consultarCriticidadeOs();
			JSONObject json = new JSONObject(retorno);
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			
			for (int i = 0; i < pcursor.length(); i++) {
				JSONObject item = pcursor.getJSONObject(i);
				this.objRsCriticidadeOs.add(new ObjRsCriticidadeOs(item.getString("CODIGO"), item.getString("NOME")));
			}
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	public void buscarCausaRaiz() {
		try {
			objRsCausaRaiz = new ArrayList<ObjRsCausaRaiz>();
			String retorno = cadOsDadosIniciaisEndPoint.consultarCausaRaiz();
			JSONObject json = new JSONObject(retorno);
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			
			for (int i = 0; i < pcursor.length(); i++) {
				JSONObject item = pcursor.getJSONObject(i);
				if(item.getString("INCLUSAO").equals("S")) {
					this.objRsCausaRaiz.add(new ObjRsCausaRaiz(item.get("CODIGO").toString(), item.getString("DESCRICAO"), item.getString("INCLUSAO")));					
				}
			}
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	public void setObjRsOs(ObjRsOs objRsOs) {
		this.objRsOs = objRsOs;
	}
	
	public ObjRsOs getObjRsOs() {	
		return objRsOs;
	}
	
	public ObjRsCausaRaizOs getObjRsCausaRaizOs() {
		return objRsCausaRaizOs;
	}

	public ObjRsSoliCancOs getObjRsSoliCancOs() {
		return objRsSoliCancOs;
	}

	public void setObjRsSoliCancOs(ObjRsSoliCancOs objRsSoliCancOs) {
		this.objRsSoliCancOs = objRsSoliCancOs;
	}

	public String getStrTitulo() {
		return strTitulo;
	}

	public void setStrTitulo(String strTitulo) {
		this.strTitulo = strTitulo;
	}

	public Boolean getFlgEnvRO() {
		return flgEnvRO;
	}

	public void setFlgEnvRO(Boolean flgEnvRO) {
		this.flgEnvRO = flgEnvRO;
	}

	public int getQtOsProvisao() {
		return qtOsProvisao;
	}

	public void setQtOsProvisao(int qtOsProvisao) {
		this.qtOsProvisao = qtOsProvisao;
	}

	public ObjRsOsProvisao getObjRsOsProvisao() {
		return objRsOsProvisao;
	}

	public List<ObjRsCriticidadeOs> getObjRsCriticidadeOs() {
		return objRsCriticidadeOs;
	}
	
	public List<ObjRsFaseAnaliseOs> getObjRsFaseAnaliseOs() {
		return objRsFaseAnaliseOs;
	}
	
	public List<ObjRsCausaRaiz> getObjRsCausaRaiz() {
		return objRsCausaRaiz;
	}

	public int getQtOsRejeicao() {
		return qtOsRejeicao;
	}

	public void setQtOsRejeicao(int qtOsRejeicao) {
		this.qtOsRejeicao = qtOsRejeicao;
	}
	
	public ObjRsRejeicaoOs getObjRsRejeicaoOs() {
		return objRsRejeicaoOs;
	}

	public ObjRsOsContab getObjRsOsContab() {
		return objRsOsContab;
	}

	public ObjRsAreaOs getObjRsAreaOs() {
		return objRsAreaOs;
	}

	public List<ObjRsArea> getObjRsArea() {
		return objRsArea;
	}
	
	public List<ObjRs> getObjRs() {
		return objRs;
	}

	public String getTxtHdFlgEnvRO() {
		return txtHdFlgEnvRO;
	}

	public void setTxtHdFlgEnvRO(String txtHdFlgEnvRO) {
		this.txtHdFlgEnvRO = txtHdFlgEnvRO;
	}
	
	public void salvar() {	
		if(this.objRsCausaRaizOs.getCODIGO() == "") {
			RequestContext.getCurrentInstance().execute("alert('Informe a causa raiz')");
		}else {
			//chamada backend: 	bjCadOsDadosIniciais.Alterar()
			//InsereLog SALVAR_DADOS_INICIAIS_OS, CPF_USUARIO, strNrSeqOs, ""
			try {
				System.out.println(this.objRsOs.getCODIGO() + "," + this.objRsOs.getCRITICIDADE()+ "," +  this.objRsOs.getFASE_ANALISE()+ "," +  this.getTxtHdFlgEnvRO()+ "," +  this.objRsAreaOs.getCODIGO()+ "," +  this.objRsCausaRaizOs.getCODIGO());
				cadOsDadosIniciaisEndPoint.alterarOs(this.objRsOs.getCODIGO(), this.objRsOs.getCRITICIDADE(), this.objRsOs.getFASE_ANALISE(), this.getTxtHdFlgEnvRO(), this.objRsAreaOs.getCODIGO(), this.objRsCausaRaizOs.getCODIGO());
				RequestContext.getCurrentInstance().execute("alert('Dados salvos com sucesso!')");

				log.incluirAcaoLog(strNrSeqOs, Integer.toString(LogIncUtil.SALVAR_DADOS_INICIAIS_OS),
						objRsUsuario.getCpfUsuario(), "");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}    	
	}
	
	public void aprovar() {
		System.out.println("aprovar");
		String retorno = "";
		String strNrSeqOs = objRsOs.getCODIGO();
		//chamada backend objAcoesOs.IncluirProxWorkFlow(strNrSeqOs, "P", "2010", "OS", "", strErro)
		
		try {
			retorno = acoesOsEndPoint.incluirProxWorkFlow(strNrSeqOs, "P", "2010", "OS", "", false);
			RequestContext.getCurrentInstance().execute("alert('A Ordem de Serviço nº " + strNrSeqOs + " foi enviada para Aprovação com sucesso!\\nNotificação enviada aos responsáveis.')");
			RequestContext.getCurrentInstance().execute("location.reload();");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
         } catch (IOException e1) {
               // TODO Auto-generated catch block
               e1.printStackTrace();
         }

		}
		
	}
	
	public void aprovarOs() {
		System.out.println("aprovarOs");
	}
	
	public void encerrarOs() {
		System.out.println("encerrarOs");
	}
	
	public ObjRsOut getObjRsOut() {
		return objRsOut;
	}

	public void setObjRsOut(ObjRsOut objRsOut) {
		this.objRsOut = objRsOut;
	}

	public void cancelarOs() {
		System.out.println("cancelarOs");
		String strNrSeqOs = "1234"; 

		//chamada backend objCadOsDadosIniciais.CancelarOs
		/*if(objCadOsDadosIniciais.CancelarOs() ){
		 * 
		 * 
		 * }*/
		RequestContext.getCurrentInstance().execute("alert('A Ordem de Servi�o n� " + strNrSeqOs + " foi enviada para Aprova��o do Cancelamento, com sucesso!')");
	}

	public boolean isAlteracao() {
		return alteracao;
	}

	public void setAlteracao(boolean alteracao) {
		this.alteracao = alteracao;
	}

	public String getStrNrSeqOs() {
		return strNrSeqOs;
	}

	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}

	public UsuarioIncModel getObjRsUsuario() {
		return objRsUsuario;
	}

	public void setObjRsUsuario(UsuarioIncModel objRsUsuario) {
		this.objRsUsuario = objRsUsuario;
	}

	public String getStrReadOnly() {
		return strReadOnly;
	}

	public void setStrReadOnly(String strReadOnly) {
		this.strReadOnly = strReadOnly;
	}

	public boolean isDesabilitarCampos() {
		return desabilitarCampos;
	}

	public void setDesabilitarCampos(boolean desabilitarCampos) {
		this.desabilitarCampos = desabilitarCampos;
	}

	public Authorization getAuthorization() {
		return authorization;
	}

	public void setAuthorization(Authorization authorization) {
		this.authorization = authorization;
	}

	public XHYAcoesOsEndPoint getAcoesOsEndPoint() {
		return acoesOsEndPoint;
	}

	public void setAcoesOsEndPoint(XHYAcoesOsEndPoint acoesOsEndPoint) {
		this.acoesOsEndPoint = acoesOsEndPoint;
	}

}
