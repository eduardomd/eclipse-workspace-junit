package com.altec.bsbr.app.hyb.web.jsf;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.altec.bsbr.app.hyb.dto.AdmWorkflowNotificacaoParaModel;


@ManagedBean(name="hyb_notificacao_para")
@ViewScoped
public class HybAdmWorkflowNotificacaoParaBean implements Serializable {

	private Object strMsg = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strMsg");
	private String txtHdAction;
	private String hdnNotificacao;
	
	private List<AdmWorkflowNotificacaoParaModel> objRsAdmNoti;
	private int objRsAdmNotiLength;
	
	public HybAdmWorkflowNotificacaoParaBean() {
		this.objRsAdmNoti = new ArrayList<AdmWorkflowNotificacaoParaModel>();
		
		for(int i = 0; i <= 3; i++) {
			AdmWorkflowNotificacaoParaModel notificParaModel = new AdmWorkflowNotificacaoParaModel();
			notificParaModel.setNR_NOTI("Nr_notificacao"+i);
			notificParaModel.setNM_NOTI("6010");
			notificParaModel.setQT_HORA_PRAZ_RELZ("Teste_relz"+i);
			notificParaModel.setQT_HORA_PERI_REEV("Teste_reev"+i);
			notificParaModel.setTP_NOTI("B"); // M -> Disabled
			notificParaModel.setIN_REEV("1");
			this.objRsAdmNoti.add(notificParaModel);
		}
		this.objRsAdmNotiLength = this.objRsAdmNoti.size();
	}
	
	public HybAdmWorkflowNotificacaoParaBean(Object strMsg, String txtHdAction, String hdnNotificacao, AdmWorkflowNotificacaoParaModel notificParaModel) {
		this.strMsg = strMsg;
		this.txtHdAction = txtHdAction;
		this.hdnNotificacao = hdnNotificacao;
	}

	public int getObjRsAdmNotiLength() {
		return objRsAdmNotiLength;
	}

	public void setObjRsAdmNotiLength(int objRsAdmNotiLength) {
		this.objRsAdmNotiLength = objRsAdmNotiLength;
	}

	public List<AdmWorkflowNotificacaoParaModel> getObjRsAdmNoti() {
		return objRsAdmNoti;
	}

	public void setObjRsAdmNoti(List<AdmWorkflowNotificacaoParaModel> objRsAdmNoti) {
		this.objRsAdmNoti = objRsAdmNoti;
	}

	public Object getstrMsg() {
		return strMsg;
	}
	
	public void setstrMsg(String strMsg) {
		this.strMsg = strMsg;
	}

	public String getTxtHdAction() {
		return txtHdAction;
	}

	public void setTxtHdAction(String txtHdAction) {
		this.txtHdAction = txtHdAction;
	}
	
	public String getHdnNotificacao() {
		return hdnNotificacao;
	}

	public void setHdnNotificacao(String hdnNotificacao) {
		this.hdnNotificacao = hdnNotificacao;
	}
	
	public void setActionNoti() {
		this.setTxtHdAction("A");
		
		String hdnNotificacao = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("hdnNotificacaoValue").toString();
		
		this.setHdnNotificacao(hdnNotificacao);
		System.out.println("hdnNoti bean");
		System.out.println(this.hdnNotificacao);
		System.out.println(hdnNotificacao);
		
	}
	
}
