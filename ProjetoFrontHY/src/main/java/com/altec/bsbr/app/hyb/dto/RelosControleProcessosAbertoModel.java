package com.altec.bsbr.app.hyb.dto;

public class RelosControleProcessosAbertoModel {

	private String ROWNUM;
	private String NR_SEQU_ORDE_SERV;
	private String NM_EVEN;
	private String TP_UOR;
	private String CD_UOR;
	private String NM_UOR;
	private String NM_REDE;
	private String NM_REGI;
	private String TX_ABER_ORDE_SERV;
	private String VL_ENVO;
	private String VL_PERD_EFET;
	private String NM_RECU_OCOR_ESPC;
	private String DT_APROV;
	private String NM_CRIT;
	private String NM_SITU_ORDE_SERV;
	private String VL_ENVO_FORMATADO;
	private String VL_PERD_EFET_FORMATADO;
	private String DT_ABER_ORDE_SERV;

	public RelosControleProcessosAbertoModel(
			String ROWNUM,
			String NR_SEQU_ORDE_SERV,
			String NM_EVEN,
			String TP_UOR,
			String CD_UOR,
			String NM_UOR,
			String NM_REDE,
			String NM_REGI,
			String TX_ABER_ORDE_SERV,
			String VL_ENVO,
			String VL_PERD_EFET,
			String NM_RECU_OCOR_ESPC,
			String DT_APROV,
			String NM_CRIT,
			String NM_SITU_ORDE_SERV,
			String VL_ENVO_FORMATADO,
			String VL_PERD_EFET_FORMATADO,
			String DT_ABER_ORDE_SERV) {

		this.ROWNUM = ROWNUM;
		this.NR_SEQU_ORDE_SERV = NR_SEQU_ORDE_SERV;
		this.NM_EVEN = NM_EVEN;
		this.TP_UOR = TP_UOR;
		this.CD_UOR = CD_UOR;
		this.NM_UOR = NM_UOR;
		this.NM_REDE = NM_REDE;
		this.NM_REGI = NM_REGI;
		this.TX_ABER_ORDE_SERV = TX_ABER_ORDE_SERV;
		this.VL_ENVO = VL_ENVO;
		this.VL_PERD_EFET = VL_PERD_EFET;
		this.NM_RECU_OCOR_ESPC = NM_RECU_OCOR_ESPC;
		this.DT_APROV = DT_APROV;
		this.NM_CRIT = NM_CRIT;
		this.NM_SITU_ORDE_SERV = NM_SITU_ORDE_SERV;
		this.VL_ENVO_FORMATADO = VL_ENVO_FORMATADO;
		this.VL_PERD_EFET_FORMATADO = VL_PERD_EFET_FORMATADO;
		this.DT_ABER_ORDE_SERV = DT_ABER_ORDE_SERV;
	}

	public String getROWNUM() {
		return ROWNUM;
	}

	public void setROWNUM(String rOWNUM) {
		ROWNUM = rOWNUM;
	}

	public String getNR_SEQU_ORDE_SERV() {
		return NR_SEQU_ORDE_SERV;
	}

	public void setNR_SEQU_ORDE_SERV(String nR_SEQU_ORDE_SERV) {
		NR_SEQU_ORDE_SERV = nR_SEQU_ORDE_SERV;
	}

	public String getNM_EVEN() {
		return NM_EVEN;
	}

	public void setNM_EVEN(String nM_EVEN) {
		NM_EVEN = nM_EVEN;
	}

	public String getTP_UOR() {
		return TP_UOR;
	}

	public void setTP_UOR(String tP_UOR) {
		TP_UOR = tP_UOR;
	}

	public String getCD_UOR() {
		return CD_UOR;
	}

	public void setCD_UOR(String cD_UOR) {
		CD_UOR = cD_UOR;
	}

	public String getNM_UOR() {
		return NM_UOR;
	}

	public void setNM_UOR(String nM_UOR) {
		NM_UOR = nM_UOR;
	}

	public String getNM_REDE() {
		return NM_REDE;
	}

	public void setNM_REDE(String nM_REDE) {
		NM_REDE = nM_REDE;
	}

	public String getNM_REGI() {
		return NM_REGI;
	}

	public void setNM_REGI(String nM_REGI) {
		NM_REGI = nM_REGI;
	}

	public String getTX_ABER_ORDE_SERV() {
		return TX_ABER_ORDE_SERV;
	}

	public void setTX_ABER_ORDE_SERV(String tX_ABER_ORDE_SERV) {
		TX_ABER_ORDE_SERV = tX_ABER_ORDE_SERV;
	}

	public String getVL_ENVO() {
		return VL_ENVO;
	}

	public void setVL_ENVO(String vL_ENVO) {
		VL_ENVO = vL_ENVO;
	}

	public String getVL_PERD_EFET() {
		return VL_PERD_EFET;
	}

	public void setVL_PERD_EFET(String vL_PERD_EFET) {
		VL_PERD_EFET = vL_PERD_EFET;
	}

	public String getNM_RECU_OCOR_ESPC() {
		return NM_RECU_OCOR_ESPC;
	}

	public void setNM_RECU_OCOR_ESPC(String nM_RECU_OCOR_ESPC) {
		NM_RECU_OCOR_ESPC = nM_RECU_OCOR_ESPC;
	}

	public String getDT_APROV() {
		return DT_APROV;
	}

	public void setDT_APROV(String dT_APROV) {
		DT_APROV = dT_APROV;
	}

	public String getNM_CRIT() {
		return NM_CRIT;
	}

	public void setNM_CRIT(String nM_CRIT) {
		NM_CRIT = nM_CRIT;
	}

	public String getNM_SITU_ORDE_SERV() {
		return NM_SITU_ORDE_SERV;
	}

	public void setNM_SITU_ORDE_SERV(String nM_SITU_ORDE_SERV) {
		NM_SITU_ORDE_SERV = nM_SITU_ORDE_SERV;
	}

	public String getVL_ENVO_FORMATADO() {
		return VL_ENVO_FORMATADO;
	}

	public void setVL_ENVO_FORMATADO(String vL_ENVO_FORMATADO) {
		VL_ENVO_FORMATADO = vL_ENVO_FORMATADO;
	}

	public String getVL_PERD_EFET_FORMATADO() {
		return VL_PERD_EFET_FORMATADO;
	}

	public void setVL_PERD_EFET_FORMATADO(String vL_PERD_EFET_FORMATADO) {
		VL_PERD_EFET_FORMATADO = vL_PERD_EFET_FORMATADO;
	}

	public String getDT_ABER_ORDE_SERV() {
		return DT_ABER_ORDE_SERV;
	}

	public void setDT_ABER_ORDE_SERV(String dT_ABER_ORDE_SERV) {
		DT_ABER_ORDE_SERV = dT_ABER_ORDE_SERV;
	}

}
