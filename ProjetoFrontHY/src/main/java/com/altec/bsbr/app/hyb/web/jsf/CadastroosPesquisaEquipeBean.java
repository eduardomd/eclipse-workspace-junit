package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.JCGG_TCMGEN1AeaResponse;
import com.altec.bsbr.app.hyb.dto.ObjRsCombo;
import com.altec.bsbr.app.hyb.dto.ObjRsParticip;
import com.altec.bsbr.app.hyb.dto.PE95_PEM9310B_PEM931CAeaResponse;
import com.altec.bsbr.app.hyb.dto.PEL1_PEM0004AeaResponse;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsParticip.XHYCadOsParticipEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYTransacao.XHYTransacaoEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;

@Component("cadastroosPesquisaEquipeBean")
@Scope("session")
public class CadastroosPesquisaEquipeBean extends BasicBBean {
	
	private static final long serialVersionUID = 1L;
	private String strNrSeqOs;
	private String strMatricula;
	private String strNome;
	private String strDocto;
	private String cboTipo = "";
	private String hdnAcao;
	private List<ObjRsParticip> objRsParticipColaboradores = new ArrayList<ObjRsParticip>();
	private List<ObjRsCombo> objRsItemsCombo = new ArrayList<ObjRsCombo>();
	int iCountPartLesado = 1;
	private boolean emptyMsg;
	
	@Autowired
	private XHYCadOsParticipEndPoint cadOsParticipEndPoint;
		
	@Autowired
	private XHYTransacaoEndPoint transacaoEndPoint;
	
	@PostConstruct
	public void init() {
		System.out.println("----");
		buscarQueryString();
		this.emptyMsg = false;
	}
	
	
	public void buscarQueryString() {
		this.strNrSeqOs = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs");
		System.out.println(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs"));
		System.out.println(this.strNrSeqOs + "-->>>");	
	}
		
	
	public void refreshFuncionarioNrOS(String seqOs) {
		System.out.println("refreshFuncionarioNrOS");
		
		RequestContext.getCurrentInstance().execute("parent.parent.PF('statusDialog').hide();");
		this.strNrSeqOs = seqOs;
		System.out.println(seqOs + " seqOs");
	}
	
	public void gravarParticipante() {
		System.out.println("Chama Gravar --------->");
		int index = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("indice"));
		System.out.println(index + " index");
		String dblGravar = "0";
		String strError = "";
		System.out.println("-");
		System.out.println(objRsParticipColaboradores.get(index).getCPF_CNPJ());
			
		//RequestContext.getCurrentInstance().execute("alert('Falta MQ.');");
		
		try {
			
			String strlCdSegm;
			if(objRsParticipColaboradores.get(index).getStrCDSEG().isEmpty()) {
				strlCdSegm = "0";
			}else {
				strlCdSegm = objRsParticipColaboradores.get(index).getStrCDSEG();
			}
			
			String strSegm;
			if(objRsParticipColaboradores.get(index).getSEGMENTO().length() >= 30) {
				strSegm = objRsParticipColaboradores.get(index).getSEGMENTO().substring(0, 30);
			}else
				strSegm = objRsParticipColaboradores.get(index).getSEGMENTO();
						
			System.out.println("" +","+ objRsParticipColaboradores.get(index).getCPF_CNPJ()+","+ this.strNrSeqOs+","+ 1+","+ objRsParticipColaboradores.get(index).getNOME()+","+ objRsParticipColaboradores.get(index).getCBO_DESIG()+","+ objRsParticipColaboradores.get(index).getMATRICULA()+","+ strSegm+","+ Integer.valueOf(strlCdSegm));
			JSONObject json = new JSONObject(cadOsParticipEndPoint.inserirParticipante("", objRsParticipColaboradores.get(index).getCPF_CNPJ(), this.strNrSeqOs, 1, objRsParticipColaboradores.get(index).getNOME(), objRsParticipColaboradores.get(index).getCBO_DESIG(), objRsParticipColaboradores.get(index).getMATRICULA(), strSegm, Integer.valueOf(strlCdSegm)));
			dblGravar = json.get("PNR_SEQU_PARP").toString();
			
			if (Integer.valueOf(dblGravar) > 0 ) {
				RequestContext.getCurrentInstance().execute("alert('Participante incluído com sucesso.');");
			}else {
				RequestContext.getCurrentInstance().execute("alert('Não foi possível gravar o registro.');");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			RequestContext.getCurrentInstance().execute("alert('Não foi possível gravar o registro. Erro:'" + e.getMessage() + "');");
			
		}finally {
            try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_cadastroos_pesquisaequipe.xhtml?&strNrSeqOs=" + this.getStrNrSeqOs());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void limparCampos() {
		System.out.println(strNrSeqOs+ " -- strNrSeqOs");
		System.out.println("Limpando campos EQUIPE");
		this.objRsParticipColaboradores.clear();
		this.setCboTipo("");
		this.setStrMatricula("");
		this.setStrNome("");
		this.setStrDocto("");
		System.out.println("cboTipo "+ cboTipo);
		System.out.println("strDocto "+ strDocto );
		System.out.println("strMatricula "+ strMatricula );
		System.out.println("strNome "+ strNome);
        System.out.println("update view participante");

		
	}
	
	public void adicionaCombo() {
		objRsItemsCombo.add(new ObjRsCombo( "1", "Envolvido", "", ""));
		objRsItemsCombo.add(new ObjRsCombo("2", "Informante", "", ""));
		objRsItemsCombo.add(new ObjRsCombo("3", "Reclamante", "", ""));
	}
	
	public void FillTblPart() {
		try {
			
			objRsItemsCombo.clear();
			objRsParticipColaboradores = new ArrayList<ObjRsParticip>();
			String strSeg;
			
			System.out.println(strNrSeqOs);
			System.out.println(this.getStrNrSeqOs() + " ----->>>>");
			adicionaCombo();
			iCountPartLesado = cadOsParticipEndPoint.fnExistePartLesado(this.getStrNrSeqOs());
			
			if(iCountPartLesado == 0) {
				objRsItemsCombo.add(new ObjRsCombo("4", "Reclamado/Lesado", "", ""));
			}
			
			this.strDocto = this.strDocto.replaceAll("[^0-9]", ""); 
			JSONObject json = new JSONObject(cadOsParticipEndPoint.consultarColaborador(this.getCboTipo(), this.getStrDocto(), this.getStrNome(), this.getStrMatricula()));
			
			
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			/*for (int i = 0; i < pcursor.length(); i++) {
				JSONObject item = pcursor.getJSONObject(i);
				
				ObjRsParticip particip = new ObjRsParticip();
				particip.setMATRICULA(item.isNull("MATRICULA")? "" :item.get("MATRICULA").toString());
				particip.setNOME(item.isNull("NOME")? "" : item.get("NOME").toString());
				particip.setCPF_CNPJ(item.isNull("CPF")? "" : item.get("CPF").toString());
				particip.setCARGO(item.isNull("CARGO")? "" : item.get("CARGO").toString());
				
				particip.setSEGMENTO(item.isNull("SEGMENTO")? "" : item.get("SEGMENTO").toString());
				particip.setStrCDSEG(item.isNull("CDSEG")? "" : item.get("CDSEG").toString());
				
				particip.setCBO_DESIG(1);
				
				objRsParticipColaboradores.add(particip);
			}*/
			
			
			for (int i = 0; i < pcursor.length(); i++) {
				JSONObject item = pcursor.getJSONObject(i);
				
				ObjRsParticip particip = new ObjRsParticip();
				particip.setMATRICULA(item.isNull("MATRICULA")? "" :item.get("MATRICULA").toString());
				particip.setNOME(item.isNull("NOME")? "" : item.get("NOME").toString());
				particip.setCPF_CNPJ(item.isNull("CPF")? "" : item.get("CPF").toString());
				particip.setCARGO(item.isNull("CARGO")? "" : item.get("CARGO").toString());
				particip.setCBO_DESIG(1);
				
				//Chamada de Transacoes MQ
				String strPeNumPe = getPENUMPE(particip.getCPF_CNPJ(), "F", particip.getNOME());
				
				if(!strPeNumPe.isEmpty()) {
					String strCdSeg = getCdSegmentacao(strPeNumPe);					
				System.out.println("strCdSeg :" + strCdSeg);
					if(strCdSeg.trim().length() > 0) {
						strSeg = GetNameSegmento(strCdSeg);
					}else {
						strSeg = "";
					}
					particip.setSEGMENTO(strSeg);
					particip.setStrCDSEG(strCdSeg);
				}else {
					particip.setSEGMENTO("");
					particip.setStrCDSEG("");
				}
				
				
				objRsParticipColaboradores.add(particip);				
			}

			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.emptyMsg = this.objRsParticipColaboradores.isEmpty();
			RequestContext.getCurrentInstance().execute("parent.parent.PF('statusDialog').hide()");
		}
		
	}
	
	public String getPENUMPE(String  strCpfCnpj, String strTipo, String strNomePart) {
		String xmlNome;
		String xmlPeNumPe = "";
		try {
			
			JSONArray jsonArrayPEL1 = new JSONArray(transacaoEndPoint.pel1("", strCpfCnpj, strTipo, strNomePart));
						
			for (int j = 0; j < jsonArrayPEL1.length(); j++) {
				JSONObject itemPEL1 = jsonArrayPEL1.getJSONObject(j);
				
				PEL1_PEM0004AeaResponse responsePEL1 = new PEL1_PEM0004AeaResponse();
				
				responsePEL1.setPENOMPE(itemPEL1.isNull("PENOMPE")? "" : itemPEL1.get("PENOMPE").toString());
				responsePEL1.setPEPRIAP(itemPEL1.isNull("PEPRIAP")? "" : itemPEL1.get("PEPRIAP").toString());
				responsePEL1.setPESEGAP(itemPEL1.isNull("PESEGAP")? "" : itemPEL1.get("PESEGAP").toString());
				
				responsePEL1.setPENUMPE(itemPEL1.isNull("PENUMPE")? "" : itemPEL1.get("PENUMPE").toString());
				
				xmlNome = responsePEL1.getPENOMPE() + responsePEL1.getPEPRIAP() + responsePEL1.getPESEGAP(); 
				xmlPeNumPe = responsePEL1.getPENUMPE();
				
				if(strNomePart.equals(xmlPeNumPe)) {
					return xmlPeNumPe;
				}
				
				return xmlPeNumPe;

			}
			
		}catch (Exception e) {
			e.printStackTrace();
			System.err.println("Ocorreu um erro na leitura da transação PEL1");
		}
		
		return xmlPeNumPe;
		
	}
	
	public String getCdSegmentacao(String strPeNumPe) {
		String getCdSegmentacao = "";
		try {
			JSONArray jsonArrayPE95 = new JSONArray(transacaoEndPoint.pe95(strPeNumPe));
			
			if(jsonArrayPE95.length() == 0) {
				return getCdSegmentacao;
			}
			
			System.out.println("AQUIii ---!!!!");
			for (int i = 0; i < jsonArrayPE95.length(); i++) {
				JSONObject itemPE95 = jsonArrayPE95.getJSONObject(i);
				
				if(itemPE95.get("CLASEG").toString().equals("PRI") ) {
					PE95_PEM9310B_PEM931CAeaResponse responsePE95 = new PE95_PEM9310B_PEM931CAeaResponse();
					
					responsePE95.setSEGCLA(itemPE95.isNull("SEGCLA")? "" : itemPE95.get("SEGCLA").toString());
					getCdSegmentacao = responsePE95.getSEGCLA(); 
					
				}
				
			}
			
			System.out.println("getCdSegmentacao: --> " + getCdSegmentacao);
			
			return getCdSegmentacao.trim();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	public String GetNameSegmento(String strNrSegmentacao) {
		String retorno = "";
		
		try {
			JSONArray jsonArrayJCGG = new JSONArray(transacaoEndPoint.jcgg(strNrSegmentacao));
			
			for (int i = 0; i < jsonArrayJCGG.length(); i++) {
				JSONObject itemJCGG = jsonArrayJCGG.getJSONObject(i);
				
				JCGG_TCMGEN1AeaResponse responseJCGG = new JCGG_TCMGEN1AeaResponse();
				responseJCGG.setDATOS1(itemJCGG.isNull("DATOS1")? "" : itemJCGG.get("DATOS1").toString());
				retorno = responseJCGG.getDATOS1().substring(0, responseJCGG.getDATOS1().length() - 34);
				
			}
			
			return retorno;
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return retorno;
		
	}
	
	public String getStrNrSeqOs() {
		return strNrSeqOs;
	}
	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}

	public String getStrMatricula() {
		return strMatricula;
	}

	public void setStrMatricula(String strMatricula) {
		this.strMatricula = strMatricula;
	}

	public String getStrNome() {
		return strNome;
	}

	public void setStrNome(String strNome) {
		this.strNome = strNome;
	}

	public String getStrDocto() {
		return strDocto;
	}

	public void setStrDocto(String strDocto) {
		this.strDocto = strDocto;
	}

	public String getCboTipo() {
		return cboTipo;
	}

	public void setCboTipo(String cboTipo) {
		this.cboTipo = cboTipo;
	}

	public String getHdnAcao() {
		return hdnAcao;
	}

	public void setHdnAcao(String hdnAcao) {
		this.hdnAcao = hdnAcao;
	}

	public List<ObjRsParticip> getObjRsParticipColaboradores() {
		return objRsParticipColaboradores;
	}

	public void setObjRsParticipColaboradores(List<ObjRsParticip> objRsParticipColaboradores) {
		this.objRsParticipColaboradores = objRsParticipColaboradores;
	}

	public int getiCountPartLesado() {
		return iCountPartLesado;
	}

	public void setiCountPartLesado(int iCountPartLesado) {
		this.iCountPartLesado = iCountPartLesado;
	}

	public List<ObjRsCombo> getObjRsItemsCombo() {
		return objRsItemsCombo;
	}

	public void setObjRsItemsCombo(List<ObjRsCombo> objRsItemsCombo) {
		this.objRsItemsCombo = objRsItemsCombo;
	}
	
	public boolean isEmptyMsg() {
		return emptyMsg;
	}

	public void setEmptyMsg(boolean emptyMsg) {
		this.emptyMsg = emptyMsg;
	}
		
}
