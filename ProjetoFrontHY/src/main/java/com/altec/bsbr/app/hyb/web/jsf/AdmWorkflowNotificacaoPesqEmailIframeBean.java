package com.altec.bsbr.app.hyb.web.jsf;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.ObjRsNotiEnv;
import com.altec.bsbr.app.hyb.dto.RsNotiManual;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmNotificacao.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmNotificacao.XHYAdmNotificacaoEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;


@Component("AdmWorkflowNotificacaoPesqEmailIframeBean")
@Scope("request")
public class AdmWorkflowNotificacaoPesqEmailIframeBean extends BasicBBean {
     
	private static final long serialVersionUID = 1L;
//	
//	private Boolean MOCK = false;
	
	private List<RsNotiManual> objRsNotiManual;
	
	@Autowired
	private XHYAdmNotificacaoEndPoint admnoti;
	
//	private void mockBackend() {
//		this.MOCK = true;
//		for(int i = 0; i <= 10; i++ ) {
//			RsNotiManual obj = new RsNotiManual();
//			obj.setNM_EMAIL_RECU("teste"+i+"@teste.com");
//			obj.setNM_RECU_OCOR_ESPC("teste "+i);
//			this.objRsNotiManual.add(obj);
//		}		
//	}
	
	public AdmWorkflowNotificacaoPesqEmailIframeBean() {
		this.objRsNotiManual = new ArrayList<RsNotiManual>();
	}
	
		
	public void formSubmit() {
		String txthdNmFunc = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("txthdNmFunc");
		String retorno = "";
		
		try {
			retorno = admnoti.consultarEmailFunc(txthdNmFunc);
	    } catch (WebServiceException e) {
	    	// TODO Auto-generated catch block
	    	e.printStackTrace();
        }		
		
		JSONObject jsonRetorno = new JSONObject(retorno);
		JSONArray pcursor = jsonRetorno.getJSONArray("PCURSOR");
                    
        for(int i = 0; i < pcursor.length(); i++) {
        	JSONObject curr = pcursor.getJSONObject(i);
        	RsNotiManual notimanual = new RsNotiManual();
        	notimanual.setNM_EMAIL_RECU(curr.getString("NM_EMAIL_RECU"));
        	notimanual.setNM_RECU_OCOR_ESPC(curr.getString("NM_RECU_OCOR_ESPC"));
        	objRsNotiManual.add(notimanual);        	
        }
	}
	
	public List<RsNotiManual> getObjRsNotiManual() {
		return objRsNotiManual;
	}

    
}