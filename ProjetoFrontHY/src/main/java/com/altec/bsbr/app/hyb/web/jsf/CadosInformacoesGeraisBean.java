package com.altec.bsbr.app.hyb.web.jsf;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.ObjRsCadOsInfGerais;
import com.altec.bsbr.app.hyb.dto.ObjRsMotiPenaInformacoesGerais;
import com.altec.bsbr.app.hyb.dto.ObjRsOsInformacoesGerais;
import com.altec.bsbr.app.hyb.dto.ObjRsParticipanteInformacoesGerais;
import com.altec.bsbr.app.hyb.dto.ObjRsPenaInformacoesGerais;
import com.altec.bsbr.app.hyb.dto.PrintObjRsCadOSArqAnex;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAcoesOs.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAcoesOs.XHYAcoesOsEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsInfGerais.XHYCadOsInfGeraisEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;

@Component("cadosInformacoesGeraisBean")
@Scope("session")
public class CadosInformacoesGeraisBean extends BasicBBean {
	
	private static final long serialVersionUID = 1L;

    private String strTitulo;
    private String strNrSeqOs;
    private String strMsg;
    private String strNmArqAnex;
    private String OutInf;
    private String strAction;
	
	private String cboPenalidade;
	private String cboMotivo;
	
	private String txtEntrada;
	private String txtEncerramento;
	private String txtFalhas;
	private String txtOutrasProp;
	
	private String srcIfrmImpArq;
	
	private StreamedContent file;
	
	private ObjRsOsInformacoesGerais objRsOs = new ObjRsOsInformacoesGerais();
	private List<ObjRsPenaInformacoesGerais> objRsPena;
	private List<ObjRsMotiPenaInformacoesGerais> objRsMotiPena;
	private List<ObjRsParticipanteInformacoesGerais> objRsParticipante;
	private ObjRsCadOsInfGerais objRsCadOsInfGerais;
	private List<PrintObjRsCadOSArqAnex> objRsCadOSArqAnex;
	
	@Autowired
	private XHYCadOsInfGeraisEndPoint cadOsInfGerais;
	
	@Autowired
	private XHYAcoesOsEndPoint acoesOs;
	
	@PostConstruct
	public void init() {
		
		
		FacesContext contextParam = FacesContext.getCurrentInstance();
	    Map<String, String> paramMap = contextParam.getExternalContext().getRequestParameterMap();
	    
	    strTitulo = paramMap.get("strTitulo") == null ? "" : paramMap.get("strTitulo");
	    strNrSeqOs = paramMap.get("strNrSeqOs") == null ? "" : paramMap.get("strNrSeqOs");
	    strMsg = paramMap.get("strMsg") == null ? "" : paramMap.get("strMsg");
	    strNmArqAnex = paramMap.get("strNmArqAnex") == null ? "" : paramMap.get("strNmArqAnex");
	    OutInf = paramMap.get("OutInf") == null ? "" : paramMap.get("OutInf");
	    strAction = paramMap.get("txtHdAction") == null ? "" : paramMap.get("txtHdAction");
		
		System.out.println("INIT  s     ");
		System.out.println("strTitulo" + strTitulo);
		this.popularObjRsOs();
		this.popularObjRsPena();
		this.popularObjRsMotiPena();
		this.popularParticipante();
		this.popularObjRsInfGerais();
		
		objRsCadOSArqAnex = new ArrayList<PrintObjRsCadOSArqAnex>();
		this.setupObjRsCadOsArqAnex();
		
		System.out.println("INIT OBJRSOS FALHAS : " + objRsOs.getTxtFalhas());

		srcIfrmImpArq = "hyb_uploadprocessa.xhtml?qstrTpAnexo=I&qstrSeqOs="+this.strNrSeqOs;
		
	}
	
	public boolean VerificaCombo(String strCod, String strCod2) {
		if( strCod.toString().equals(strCod2.toString()) ) {
			return true;
		}
		
		return false;
	}
	
	public void salvar() {
		FacesContext contextParam = FacesContext.getCurrentInstance();
	    Map<String, String> paramMap = contextParam.getExternalContext().getRequestParameterMap();
			
		String strParecerGestor = paramMap.get("txtParecerGestor") == null ? "" : paramMap.get("txtParecerGestor");
		String strParecer = paramMap.get("txtParecer") == null ? "" : paramMap.get("txtParecer");

		String strNrSeqOs = this.strNrSeqOs;
		
		// 4 Textareas
		String strFalhas = objRsOs.getTxtFalhas();
		String strOutrasProp = objRsOs.getTxtOutrasProp();
		String strTxAberOS = objRsCadOsInfGerais.getTxAberOrdeServ(); //txAberOrdeServ
		String strTxEnceOS = objRsCadOsInfGerais.getTxEnceOrdeServ(); // txAberOrdeServ
		
		try {
			acoesOs.salvarEncerramentoOs(strNrSeqOs, strFalhas, strOutrasProp, strParecerGestor, strTxAberOS, strTxEnceOS, strParecer, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
			
			for( int i = 0; i < objRsParticipante.size(); i++ ) {
				
				String strCdPart = objRsParticipante.get(i).getCodigo();
				String strPena = objRsParticipante.get(i).getCdPenalidade();
				String strMoti = objRsParticipante.get(i).getCdMotivo();
						
				System.out.println("ARRAYs s : " + strCdPart + " - " + strPena + " - " + strMoti);
				acoesOs.salvarPenaEnvolvidosOs(strCdPart, strPena, strMoti);
			} 

		} catch (WebServiceException e) {
			e.printStackTrace();
		}
	}
	
	// Download File
	public void saveFile() throws FileNotFoundException {
		String idArquivo = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("fileId").toString();
		String filePath = this.objRsCadOSArqAnex.get(Integer.parseInt(idArquivo)).getCaminhoArquivo();
		String fileName = this.objRsCadOSArqAnex.get(Integer.parseInt(idArquivo)).getNomeArquivo();
		String contentType = FacesContext.getCurrentInstance().getExternalContext().getMimeType(filePath);
		
		InputStream stream = new FileInputStream(filePath);
		file = new DefaultStreamedContent(stream, contentType, fileName);
		
	}
	
	public void deletarArquivo() {
		String idArquivo = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("idArquivo").toString();
		String currArq = this.objRsCadOSArqAnex.get(Integer.parseInt(idArquivo)).getNM_CAMI_ARQU_ANEX();
		System.out.println("ARQ: a" + currArq);
		try {
			this.cadOsInfGerais.apagarArqAnex(this.strNrSeqOs, currArq, "I");
			//objRsCadOSArqAnex.remove(Integer.parseInt(idArquivo));
			objRsCadOSArqAnex = new ArrayList<PrintObjRsCadOSArqAnex>();
			this.setupObjRsCadOsArqAnex();
		} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsInfGerais.WebServiceException e) {
			e.printStackTrace();
		}
	}
	
	public void enviarAprovacao() {
		try {
			this.acoesOs.incluirProxWorkFlow(this.strNrSeqOs, "I", "3010", "TE", "", false);
		} catch (WebServiceException e) {
			e.printStackTrace();
		}
	}
	
	public void setupObjRsCadOsArqAnex() {
		String retorno = "";		
		try {
			retorno = cadOsInfGerais.consultarOSArqAnex(this.strNrSeqOs);
			System.out.println("retorno consultarOSArqAnexs: "+retorno);
		} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsInfGerais.WebServiceException e) {
			e.printStackTrace();
		}
	
        JSONObject jsonRetorno = new JSONObject(retorno);
        JSONArray pcursor = jsonRetorno.getJSONArray("PCURSOR");

        for(int i = 0; i < pcursor.length(); i++) {
        	JSONObject curr = pcursor.getJSONObject(i);
        	PrintObjRsCadOSArqAnex obj = new PrintObjRsCadOSArqAnex();
        	obj.setNM_CAMI_ARQU_ANEX(curr.isNull("NM_CAMI_ARQU_ANEX") ? "" : curr.getString("NM_CAMI_ARQU_ANEX"));
        	this.objRsCadOSArqAnex.add(obj);
        }
	}
	
	public void resetObjRsArq() {
		objRsCadOSArqAnex = new ArrayList<PrintObjRsCadOSArqAnex>();
		this.setupObjRsCadOsArqAnex();
	}
	
	public void popularObjRsInfGerais() {
		try {
			String infGeralRetorno = cadOsInfGerais.consultarOSInfGerais(this.strNrSeqOs);
			
			JSONObject consultar = new JSONObject(infGeralRetorno);
            JSONArray pcursor = consultar.getJSONArray("PCURSOR");
            System.out.println("CURS OR INF : " + pcursor);
            if (pcursor.length() > 0) {
	           JSONObject item = pcursor.getJSONObject(0);
	           this.objRsCadOsInfGerais = new ObjRsCadOsInfGerais(
	        		   item.isNull("TX_ABER_ORDE_SERV") ? "" : item.get("TX_ABER_ORDE_SERV").toString(),
					   item.isNull("TX_ENCE_ORDE_SERV") ? "" : item.get("TX_ENCE_ORDE_SERV").toString(),
					   item.isNull("CD_NOTI") ? "" : item.get("CD_NOTI").toString());
			
            }
		} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsInfGerais.WebServiceException e) {
	        try {
	            FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
	        } catch (IOException e1) {
	            e1.printStackTrace();
	        }
       }
	}
	
	public void popularParticipante() {
		try {
			String participanteRetorno = acoesOs.consultarParticipanteOs(this.strNrSeqOs);
			
			JSONObject consultar = new JSONObject(participanteRetorno);
            JSONArray pcursor = consultar.getJSONArray("PCURSOR");
            this.objRsParticipante = new ArrayList<ObjRsParticipanteInformacoesGerais>();
            
            if (pcursor.length() > 0) {
	            for (int i = 0; i < pcursor.length(); i++) {
	               JSONObject item = pcursor.getJSONObject(i);
	               
	               String cpf = "";
	               String newCpf = "";
	               
	               System.out.println("PARTICIP1: " + item.get("TIPO_PARTICIP").toString());
	               
	               if (!item.isNull("CPF_CNPJ")) {
	            	   cpf = item.get("CPF_CNPJ").toString();
	            	   if (item.get("TIPO_PARTICIP").toString().equals("3")) {
	            		   String maskCnpj = cpf.substring(0, 2)+"."+cpf.substring(2, 5)+"."+cpf.substring(5, 8)+"/"+cpf.substring(8, 12)+"-"+cpf.substring(12, 14);
	            		   newCpf = maskCnpj;
	            	   } else {
	            		   String maskCpf = cpf.substring(0, 3)+"."+cpf.substring(3, 6)+"."+cpf.substring(6, 9)+"-"+cpf.substring(9);
	            		   newCpf = maskCpf;
	            	   }
	               }
	               
	               
	               this.objRsParticipante.add(new ObjRsParticipanteInformacoesGerais(
		    		   item.isNull("CODIGO") ? "" : item.get("CODIGO").toString(),
					   item.isNull("POSICAO") ? "" : item.get("POSICAO").toString(),
					   item.isNull("NOME") ? "" : item.get("NOME").toString(),
					   item.isNull("TIPO_PARTICIP") ? "" : item.get("TIPO_PARTICIP").toString(),
					   newCpf,
					   item.isNull("DESC_TIPO") ? "" : item.get("DESC_TIPO").toString(),
					   item.isNull("CD_PENALIDADE") ? "" : item.get("CD_PENALIDADE").toString(),
					   item.isNull("CD_MOTIVO") ? "" : item.get("CD_MOTIVO").toString()));
	            }
            }
			
		} catch (WebServiceException e) {
	        try {
	            FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
            } catch (IOException e1) {
              e1.printStackTrace();
            }
       }
	}
	
	public void popularObjRsOs() {
		try {
			String objRsRetorno = acoesOs.consultarOS(strNrSeqOs);
			
		    JSONObject consultar = new JSONObject(objRsRetorno);
            JSONArray pcursor = consultar.getJSONArray("PCURSOR");
            if (pcursor.length() > 0) {
	            JSONObject item = pcursor.getJSONObject(0);
	            
	            this.objRsOs.setTxtFalhas(item.isNull("TX_FALHAS") ? "" : item.get("TX_FALHAS").toString());
	            this.objRsOs.setTxtOutrasProp(item.isNull("TX_OUTRAS_PROP") ? "" : item.get("TX_OUTRAS_PROP").toString());
        		this.objRsOs.setCdNoti(item.isNull("CD_NOTI") ? "" : item.get("CD_NOTI").toString());
				this.objRsOs.setCdSitu(item.isNull("CD_SITU") ? "" : item.get("CD_SITU").toString());
				
				System.out.println("TEST : " + this.objRsOs.getTxtFalhas());
            }
            
            
			
		} catch (WebServiceException e) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
	        } catch (IOException e1) {
	            e1.printStackTrace();
	        }
	    }
	}
	
	public void popularObjRsPena() {
		try {
			String objRsPenaRetorno = acoesOs.consultarPenalidade();
			
			JSONObject consultar = new JSONObject(objRsPenaRetorno);
            JSONArray pcursor = consultar.getJSONArray("PCURSOR");
            
            this.objRsPena = new ArrayList<ObjRsPenaInformacoesGerais>();
            if (pcursor.length() > 0) {
	            for (int i = 0; i < pcursor.length(); i++) {
	               JSONObject item = pcursor.getJSONObject(i);
	               this.objRsPena.add(new ObjRsPenaInformacoesGerais(
	            		   item.isNull("CODIGO") ? "" : item.get("CODIGO").toString(),
	    				   item.isNull("NOME") ? "" : item.get("NOME").toString()));
	            }
            }
			
		} catch (WebServiceException e) {
            try {
               FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
	        } catch (IOException e1) {
	           e1.printStackTrace();
	        }
	      }
	}
	
	public void popularObjRsMotiPena() {
		try {
			String objRsMotiPenaRetorno = acoesOs.consultarMotiPenalidade();
			
			JSONObject consultar = new JSONObject(objRsMotiPenaRetorno);
            JSONArray pcursor = consultar.getJSONArray("PCURSOR");
            this.objRsMotiPena = new ArrayList<ObjRsMotiPenaInformacoesGerais>();
            
            for (int i = 0; i < pcursor.length(); i++) {
               JSONObject item = pcursor.getJSONObject(i);
               this.objRsMotiPena.add(new ObjRsMotiPenaInformacoesGerais(
            		   item.isNull("CODIGO") ? "" : item.get("CODIGO").toString(),
    				   item.isNull("NOME") ? "" : item.get("NOME").toString()));
            }
			
		} catch (WebServiceException e) {
            try {
              FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
	        } catch (IOException e1) {
	          e1.printStackTrace();
	        }
	      }
	}
	
	public XHYCadOsInfGeraisEndPoint getCadOsInfGerais() {
		return cadOsInfGerais;
	}

	public void setCadOsInfGerais(XHYCadOsInfGeraisEndPoint cadOsInfGerais) {
		this.cadOsInfGerais = cadOsInfGerais;
	}

	public XHYAcoesOsEndPoint getAcoesOs() {
		return acoesOs;
	}

	public void setAcoesOs(XHYAcoesOsEndPoint acoesOs) {
		this.acoesOs = acoesOs;
	}

	public ObjRsCadOsInfGerais getObjRsCadOsInfGerais() {
		return objRsCadOsInfGerais;
	}

	public void setObjRsCadOsInfGerais(ObjRsCadOsInfGerais objRsCadOsInfGerais) {
		this.objRsCadOsInfGerais = objRsCadOsInfGerais;
	}

	public String getTxtEntrada() {
		return txtEntrada;
	}

	public void setTxtEntrada(String txtEntrada) {
		this.txtEntrada = txtEntrada;
	}

	public String getTxtEncerramento() {
		return txtEncerramento;
	}

	public void setTxtEncerramento(String txtEncerramento) {
		this.txtEncerramento = txtEncerramento;
	}

	public String getCboPenalidade() {
		return cboPenalidade;
	}

	public void setCboPenalidade(String cboPenalidade) {
		this.cboPenalidade = cboPenalidade;
	}

	public String getCboMotivo() {
		return cboMotivo;
	}

	public void setCboMotivo(String cboMotivo) {
		this.cboMotivo = cboMotivo;
	}

	public String getStrTitulo() {
		return strTitulo;
	}

	public void setStrTitulo(String strTitulo) {
		this.strTitulo = strTitulo;
	}

	public String getStrNrSeqOs() {
		return strNrSeqOs;
	}

	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}

	public String getStrMsg() {
		return strMsg;
	}

	public void setStrMsg(String strMsg) {
		this.strMsg = strMsg;
	}

	public String getStrNmArqAnex() {
		return strNmArqAnex;
	}

	public void setStrNmArqAnex(String strNmArqAnex) {
		this.strNmArqAnex = strNmArqAnex;
	}

	public String getStrAction() {
		return strAction;
	}

	public void setStrAction(String strAction) {
		this.strAction = strAction;
	}

	public ObjRsOsInformacoesGerais getObjRsOs() {
		return objRsOs;
	}

	public void setObjRsOs(ObjRsOsInformacoesGerais objRsOs) {
		this.objRsOs = objRsOs;
	}

	public List<ObjRsPenaInformacoesGerais> getObjRsPena() {
		return objRsPena;
	}

	public void setObjRsPena(List<ObjRsPenaInformacoesGerais> objRsPena) {
		this.objRsPena = objRsPena;
	}

	public List<ObjRsMotiPenaInformacoesGerais> getObjRsMotiPena() {
		return objRsMotiPena;
	}

	public void setObjRsMotiPena(List<ObjRsMotiPenaInformacoesGerais> objRsMotiPena) {
		this.objRsMotiPena = objRsMotiPena;
	}

	public List<ObjRsParticipanteInformacoesGerais> getObjRsParticipante() {
		return objRsParticipante;
	}

	public void setObjRsParticipante(List<ObjRsParticipanteInformacoesGerais> objRsParticipante) {
		this.objRsParticipante = objRsParticipante;
	}

	public String getTxtFalhas() {
		return txtFalhas;
	}

	public void setTxtFalhas(String txtFalhas) {
		this.txtFalhas = txtFalhas;
	}

	public String getTxtOutrasProp() {
		return txtOutrasProp;
	}

	public void setTxtOutrasProp(String txtOutrasProp) {
		this.txtOutrasProp = txtOutrasProp;
	}

	public List<PrintObjRsCadOSArqAnex> getObjRsCadOSArqAnex() {
		return objRsCadOSArqAnex;
	}

	public void setObjRsCadOSArqAnex(List<PrintObjRsCadOSArqAnex> objRsCadOSArqAnex) {
		this.objRsCadOSArqAnex = objRsCadOSArqAnex;
	}

	public String getOutInf() {
		return OutInf;
	}
	
	public String getSrcIfrmImpArq() {
		return srcIfrmImpArq;
	}

	public void setSrcIfrmImpArq(String srcIfrmImpArq) {
		this.srcIfrmImpArq = srcIfrmImpArq;
	}

	public void setOutInf(String outInf) {
		OutInf = outInf;
	}

	public StreamedContent getFile() {
		return file;
	}

	public void setFile(StreamedContent file) {
		this.file = file;
	}
	
}
