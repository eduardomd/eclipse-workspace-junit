package com.altec.bsbr.app.hyb.dto;

public class ObjCntaContab {

	private String value;
	private String NuCntC;	
	
	public ObjCntaContab() {
		
	}
	
	public ObjCntaContab(String value,String nuCntC) {
		super();
		this.value = value;
		NuCntC = nuCntC;
	}
	
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	public String getNuCntC() {
		return NuCntC;
	}
	public void setNuCntC(String nuCntC) {
		NuCntC = nuCntC;
	}    
}