package com.altec.bsbr.app.hyb.dto;

public class objRsAdmLog {
	
	private String usuaCod;
	private String usuaNome;
	
	private String DT_ACAO;
	private String DT_HORA;
	private String COD_USUARIO;
	private String NM_ACAO;
	private String NR_OS;
	
	public objRsAdmLog() {
		
	}
	
	public objRsAdmLog(String dT_ACAO, String dT_HORA, String cOD_USUARIO, String nM_ACAO, String nR_OS) {
		DT_ACAO = dT_ACAO;
		DT_HORA = dT_HORA;
		COD_USUARIO = cOD_USUARIO;
		NM_ACAO = nM_ACAO;
		NR_OS = nR_OS;
	}
	
	public String getDT_ACAO() {
		return DT_ACAO;
	}

	public void setDT_ACAO(String dT_ACAO) {
		DT_ACAO = dT_ACAO;
	}

	public objRsAdmLog(String usuaCod, String usuaNome) {
		this.usuaCod = usuaCod;
		this.usuaNome = usuaNome;
	}

	public String getDT_HORA() {
		return DT_HORA;
	}

	public void setDT_HORA(String dT_HORA) {
		DT_HORA = dT_HORA;
	}

	public String getCOD_USUARIO() {
		return COD_USUARIO;
	}

	public void setCOD_USUARIO(String cOD_USUARIO) {
		COD_USUARIO = cOD_USUARIO;
	}

	public String getNM_ACAO() {
		return NM_ACAO;
	}

	public void setNM_ACAO(String nM_ACAO) {
		NM_ACAO = nM_ACAO;
	}

	public String getNR_OS() {
		return NR_OS;
	}

	public void setNR_OS(String nR_OS) {
		NR_OS = nR_OS;
	}

	public String getUsuaCod() {
		return usuaCod;
	}
	public void setUsuaCod(String usuaCod) {
		this.usuaCod = usuaCod;
	}
	public String getUsuaNome() {
		return usuaNome;
	}
	public void setUsuaNome(String usuaNome) {
		this.usuaNome = usuaNome;
	}
	
}
