package com.altec.bsbr.app.hyb.dto;

public class RelCartaApresentacaoEquipeModel {
	private String nome;
	private String matricula;
	private String designacao;
	
	public RelCartaApresentacaoEquipeModel(String nome, String matricula, String designacao) {
		this.nome = nome;
		this.matricula = matricula;
		this.designacao = designacao;
	}
	
	public RelCartaApresentacaoEquipeModel() {
	}

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public String getDesignacao() {
		return designacao;
	}
	public void setDesignacao(String designacao) {
		this.designacao = designacao;
	}

}
