package com.altec.bsbr.app.hyb.dto;

import javax.faces.bean.ManagedBean;
@ManagedBean(name = "objRsEventosOs")
public class PrintObjRsEventosOs {
	
	private String NM_EVENTO;
	private String NM_CANAL;
	private String NM_OPERACAO;
	private String VALOR_OPERACAO;
	
	public PrintObjRsEventosOs() {}

	public String getNM_EVENTO() {
		return NM_EVENTO;
	}

	public void setNM_EVENTO(String nM_EVENTO) {
		NM_EVENTO = nM_EVENTO;
	}

	public String getNM_CANAL() {
		return NM_CANAL;
	}

	public void setNM_CANAL(String nM_CANAL) {
		NM_CANAL = nM_CANAL;
	}

	public String getNM_OPERACAO() {
		return NM_OPERACAO;
	}

	public void setNM_OPERACAO(String nM_OPERACAO) {
		NM_OPERACAO = nM_OPERACAO;
	}

	public String getVALOR_OPERACAO() {
		return VALOR_OPERACAO;
	}

	public void setVALOR_OPERACAO(String vALOR_OPERACAO) {
		VALOR_OPERACAO = vALOR_OPERACAO;
	}
}