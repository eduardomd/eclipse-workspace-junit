package com.altec.bsbr.app.hyb.dto;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name="tituloBean")
@ViewScoped
public class TituloBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String strTitulo;
	
	@PostConstruct
    public void PostConstruct() {
		this.strTitulo = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strTitulo");
		System.out.println(this.strTitulo);		
    }

	public String getStrTitulo() {
		return strTitulo;
	}

	public void setStrTitulo(String strTitulo) {
		this.strTitulo = strTitulo;
	}
	
	

}
