package com.altec.bsbr.app.hyb.web.jsf;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

@ManagedBean(name="VerificaTramitesBean")
@ViewScoped
public class VerificaTramitesBean implements Serializable {
     
	private static final long serialVersionUID = 1L;

	private String vrstTramite;
	
	//Query String
	private Object wid_OS = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id_OS");
		
	public VerificaTramitesBean() {
		//Componentes antes instanciados no backend
		//objGeral = server.CreateObject("xHY_Geral.clsGeral")
		//vobjTramite = server.createObject("xHy_tramite.clsTramite")
		//vrstTramite = Server.CreateObject ("ADODB.Recordset")
		
		if (wid_OS.toString() != "0") {
			
			/* Consulta backend */
			
			//vrstNrTramite		= trim(vrstTramite("NR_TRA").value)
			vrstTramite = "1111111";
			
			RequestContext.getCurrentInstance().execute("parent.document.forms[0].txtHDExisteTramite.value="+vrstTramite+";");
			//RequestContext.getCurrentInstance().execute("console.log("+vrstTramite+");");
		}

	}

	public String getVrstTramite() {
		return vrstTramite;
	}

	public void setVrstTramite(String vrstTramite) {
		this.vrstTramite = vrstTramite;
	}
    

}