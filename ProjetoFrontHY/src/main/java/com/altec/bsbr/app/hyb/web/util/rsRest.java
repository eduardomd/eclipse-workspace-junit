package com.altec.bsbr.app.hyb.web.util;

public class rsRest {
	private String SQ_RESTRICAO;
	private String INSTITUICAO;
	private String ESPECIFICACAO;
	private String VALOR;
	private String DATA;
	
	public rsRest() {}
	
	public rsRest(String SQ_RESTRICAO, String INSTITUICAO, String ESPECIFICACAO, String VALOR, String DATA) {
		this.SQ_RESTRICAO = SQ_RESTRICAO;
		this.INSTITUICAO = INSTITUICAO;
		this.ESPECIFICACAO = ESPECIFICACAO;
		this.VALOR = VALOR;
		this.DATA = DATA;
	}
	public String getSQ_RESTRICAO() {
		return SQ_RESTRICAO;
	}
	public void setSQ_RESTRICAO(String sQ_RESTRICAO) {
		SQ_RESTRICAO = sQ_RESTRICAO;
	}
	public String getINSTITUICAO() {
		return INSTITUICAO;
	}
	public void setINSTITUICAO(String iNSTITUICAO) {
		INSTITUICAO = iNSTITUICAO;
	}
	public String getESPECIFICACAO() {
		return ESPECIFICACAO;
	}
	public void setESPECIFICACAO(String eSPECIFICACAO) {
		ESPECIFICACAO = eSPECIFICACAO;
	}
	public String getVALOR() {
		return VALOR;
	}
	public void setVALOR(String vALOR) {
		VALOR = vALOR;
	}
	public String getDATA() {
		return DATA;
	}
	public void setDATA(String dATA) {
		DATA = dATA;
	}
}
