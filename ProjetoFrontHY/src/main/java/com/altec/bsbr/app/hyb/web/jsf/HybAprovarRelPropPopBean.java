package com.altec.bsbr.app.hyb.web.jsf;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("hybAprovarRelPropPopBean")
@Scope("session")
public class HybAprovarRelPropPopBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String strNrSeqOs;
	
	@PostConstruct
	public void init() {
		this.setStrNrSeqOs("2345");
	}

	public String getStrNrSeqOs() {
		return strNrSeqOs;
	}

	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}
}
