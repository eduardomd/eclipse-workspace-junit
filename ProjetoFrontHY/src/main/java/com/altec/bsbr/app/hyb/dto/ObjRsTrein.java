package com.altec.bsbr.app.hyb.dto;

import java.util.List;

public class ObjRsTrein {
	 
	private String codigo;
	private String nome;
	private String data;
	private String qtde;
	private String local;
	
	private List<ObjRsTreiAnexo> anexos;
	
	public ObjRsTrein() {}
	
	public ObjRsTrein(String codigo, String nome, String data, String qtde, String local, List<ObjRsTreiAnexo> anexos) {
		this.codigo = codigo;
		this.nome = nome;
		this.data = data;
		this.qtde = qtde;
		this.local = local;
		this.anexos = anexos;
	}
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getQtde() {
		return qtde;
	}
	public void setQtde(String qtde) {
		this.qtde = qtde;
	}
	public String getLocal() {
		return local;
	}
	public void setLocal(String local) {
		this.local = local;
	}
	public List<ObjRsTreiAnexo> getAnexos() {
		return anexos;
	}

	public void setAnexos(List<ObjRsTreiAnexo> anexos) {
		this.anexos = anexos;
	}
	
} 
 
