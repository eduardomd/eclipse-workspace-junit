package com.altec.bsbr.app.hyb.dto;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;

import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.altec.bsbr.app.jab.hyb.webclient.XHYPesquisa.XHYPesquisaEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYPesquisa.WebServiceException;


@ManagedBean(name="pesquisaOsModel")
public class PesquisaOsModel {
	@Autowired
    private XHYPesquisaEndPoint pesquisaOS;

	
	public List<SelectItem> situacaoOS;
	public List<SelectItem> faseOS;
	public List<SelectItem> criticidadeOs;
	public List<SelectItem> penalidadeOs;
	public List<SelectItem> motivoPenalidade;
	public List<SelectItem> situacaoRecurso;
	public List<SelectItem> canalOrigem;
	public List<SelectItem> evento;
	public List<SelectItem> canal;
	public List<SelectItem> pCorp;
	public List<SelectItem> pAux;
	
	public List<SelectItem> consultarSituacaoOS() {
		situacaoOS = new ArrayList<SelectItem>();
        try {
			String retorno = pesquisaOS.consultarSituacaoOs();
		
			JSONObject consultaArea = new JSONObject(retorno);
			JSONArray pCursorArea = consultaArea.getJSONArray("PCURSOR");

			for (int i = 0; i < pCursorArea.length(); i++) {
				JSONObject f = pCursorArea.getJSONObject(i);

				situacaoOS.add(new SelectItem(f.get("NOME").toString(), f.get("CODIGO").toString()));

			} // close for
			
			
			} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
        

//		situacaoOS.add(new SelectItem("Situa��o OS 01","Situa��o OS 01"));
//		situacaoOS.add(new SelectItem("Situa��o OS 02","Situa��o OS 02"));
//		situacaoOS.add(new SelectItem("Situa��o OS 03","Situa��o OS 03"));
        
		return situacaoOS;
	}
	
	public List<SelectItem> consultarFaseAnaliseOS() {
		
		faseOS = new ArrayList<SelectItem>();
		
        try {
			String retorno = pesquisaOS.consultarFaseAnaliseOs();
				
			JSONObject consultaArea = new JSONObject(retorno);
			JSONArray pCursorArea = consultaArea.getJSONArray("PCURSOR");

			for (int i = 0; i < pCursorArea.length(); i++) {
				JSONObject f = pCursorArea.getJSONObject(i);

				faseOS.add(new SelectItem(f.get("NOME").toString(), f.get("CODIGO").toString()));

			} // close for
			
			
			} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
		
		
//		faseOS.add(new SelectItem("Fase OS 01","Fase OS 01"));
//		faseOS.add(new SelectItem("Fase OS 02","Fase OS 02"));
        
		return faseOS;
	}
	
	public List<SelectItem> consultarCriticidadeOs() {
		
		criticidadeOs = new ArrayList<SelectItem>();
		
        try {
			String retorno = pesquisaOS.consultarCriticidadeOs();
				
			JSONObject consultaArea = new JSONObject(retorno);
			JSONArray pCursorArea = consultaArea.getJSONArray("PCURSOR");

			for (int i = 0; i < pCursorArea.length(); i++) {
				JSONObject f = pCursorArea.getJSONObject(i);

				criticidadeOs.add(new SelectItem(f.get("NOME").toString(), f.get("CODIGO").toString()));

			} // close for
			
			
			} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
//		criticidadeOs.add(new SelectItem("Criticidade OS 01","Criticidade OS 01"));
//		criticidadeOs.add(new SelectItem("Criticidade OS 02","Criticidade OS 02"));
//		criticidadeOs.add(new SelectItem("Criticidade OS 03","Criticidade OS 03"));
//		criticidadeOs.add(new SelectItem("Criticidade OS 04","Criticidade OS 04"));
//		criticidadeOs.add(new SelectItem("Criticidade OS 05","Criticidade OS 05"));
        
		return criticidadeOs;
	}

	public List<SelectItem> consultarPenalidade() {
			
		penalidadeOs = new ArrayList<SelectItem>();
		
		
        try {
			String retorno = pesquisaOS.consultarPenalidade();
				
			JSONObject consultaArea = new JSONObject(retorno);
			JSONArray pCursorArea = consultaArea.getJSONArray("PCURSOR");

			for (int i = 0; i < pCursorArea.length(); i++) {
				JSONObject f = pCursorArea.getJSONObject(i);

				penalidadeOs.add(new SelectItem(f.get("NOME").toString(), f.get("CODIGO").toString()));

			} // close for
			
			
			} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
		
//		penalidadeOs.add(new SelectItem("Penalidade OS 01","Penalidade OS 01"));
//		penalidadeOs.add(new SelectItem("Penalidade OS 02","Penalidade OS 02"));
//		penalidadeOs.add(new SelectItem("Penalidade OS 03","Penalidade OS 03"));
//		penalidadeOs.add(new SelectItem("Penalidade OS 04","Penalidade OS 04"));
//		penalidadeOs.add(new SelectItem("Penalidade OS 05","Penalidade OS 05"));
        
		return penalidadeOs;
	}
	
	public List<SelectItem> consultarMotivoPenalidade() {
		
		motivoPenalidade = new ArrayList<SelectItem>();
		
        try {
			String retorno = pesquisaOS.consultarMotivoPenalidade();
				
			JSONObject consultaArea = new JSONObject(retorno);
			JSONArray pCursorArea = consultaArea.getJSONArray("PCURSOR");

			for (int i = 0; i < pCursorArea.length(); i++) {
				JSONObject f = pCursorArea.getJSONObject(i);

				motivoPenalidade.add(new SelectItem(f.get("NOME").toString(), f.get("CODIGO").toString()));

			} // close for
			
			
			} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
        
//		motivoPenalidade.add(new SelectItem("Motivo Penalidade 01","Motivo Penalidade 01"));
//		motivoPenalidade.add(new SelectItem("Motivo Penalidade 02","Motivo Penalidade 02"));
//		motivoPenalidade.add(new SelectItem("Motivo Penalidade 03","Motivo Penalidade 03"));
//		motivoPenalidade.add(new SelectItem("Motivo Penalidade 04","Motivo Penalidade 04"));
//		motivoPenalidade.add(new SelectItem("Motivo Penalidade 05","Motivo Penalidade 05"));
        
		return motivoPenalidade;
	}
	
	public List<SelectItem> consultarSituacaoRecurso() {
		
		situacaoRecurso = new ArrayList<SelectItem>();
		
        try {
			String retorno = pesquisaOS.consultarSituacaoRecurso();
				
			JSONObject consultaArea = new JSONObject(retorno);
			JSONArray pCursorArea = consultaArea.getJSONArray("PCURSOR");

			for (int i = 0; i < pCursorArea.length(); i++) {
				JSONObject f = pCursorArea.getJSONObject(i);

				situacaoRecurso.add(new SelectItem(f.get("NOME").toString(), f.get("CODIGO").toString()));

			} // close for
			
			
			} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
        
        
        
//		situacaoRecurso.add(new SelectItem("Situac�o Recurso 01","Situac�o Recurso 01"));
//		situacaoRecurso.add(new SelectItem("Situac�o Recurso 02","Situac�o Recurso 02"));
//		situacaoRecurso.add(new SelectItem("Situac�o Recurso 03","Situac�o Recurso 03"));
//		situacaoRecurso.add(new SelectItem("Situac�o Recurso 04","Situac�o Recurso 04"));
        
		return situacaoRecurso;
	}
	
	public List<SelectItem> consultarCanalOrigem() {
		
		canalOrigem = new ArrayList<SelectItem>();
		
        try {
			String retorno = pesquisaOS.consultarCanalOrigem();
				
			JSONObject consultaArea = new JSONObject(retorno);
			JSONArray pCursorArea = consultaArea.getJSONArray("PCURSOR");

			for (int i = 0; i < pCursorArea.length(); i++) {
				JSONObject f = pCursorArea.getJSONObject(i);

				canalOrigem.add(new SelectItem(f.get("NOME").toString(), f.get("CODIGO").toString()));

			} // close for
			
			
			} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
        
        
        
//		canalOrigem.add(new SelectItem("canal de Origem 01","canal de Origem 01"));
//		canalOrigem.add(new SelectItem("canal de Origem 02","canal de Origem 02"));
//		canalOrigem.add(new SelectItem("canal de Origem 03","canal de Origem 03"));
//		canalOrigem.add(new SelectItem("canal de Origem 04","canal de Origem 04"));
        
		return canalOrigem;
	}
	
	public List<SelectItem> consultarEvento() {
		
		evento = new ArrayList<SelectItem>();
		
        try {
			String retorno = pesquisaOS.consultarEvento();
				
			JSONObject consultaArea = new JSONObject(retorno);
			JSONArray pCursorArea = consultaArea.getJSONArray("PCURSOR");

			for (int i = 0; i < pCursorArea.length(); i++) {
				JSONObject f = pCursorArea.getJSONObject(i);

				evento.add(new SelectItem(f.get("NOME").toString(), f.get("CODIGO").toString()));

			} // close for
			
			
			} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
        
        
//		evento.add(new SelectItem("evento 01","evento 01"));
//		evento.add(new SelectItem("evento 02","evento 02"));
//		evento.add(new SelectItem("evento 03","evento 03"));
//		evento.add(new SelectItem("evento 04","evento 04"));
//        
		return evento;
	}

	public List<SelectItem> consultarCanal() {
		
		canal = new ArrayList<SelectItem>();
		
        try {
			String retorno = pesquisaOS.consultarCanal();
				
			JSONObject consultaArea = new JSONObject(retorno);
			JSONArray pCursorArea = consultaArea.getJSONArray("PCURSOR");

			for (int i = 0; i < pCursorArea.length(); i++) {
				JSONObject f = pCursorArea.getJSONObject(i);

				canal.add(new SelectItem(f.get("NOME").toString(), f.get("CODIGO").toString()));

			} // close for
			
			
			} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
        
        
//		canal.add(new SelectItem("canal 01","canal 01"));
//		canal.add(new SelectItem("canal 02","canal 02"));
//		canal.add(new SelectItem("canal 03","canal 03"));
//		canal.add(new SelectItem("canal 04","canal 04"));
//	    
		return canal;
	}
	
	public List<SelectItem> consultarPCorp() {
		
		pCorp = new ArrayList<SelectItem>();
		
        try {
			String retorno = pesquisaOS.consultarPCorp();
				
			JSONObject consultaArea = new JSONObject(retorno);
			JSONArray pCursorArea = consultaArea.getJSONArray("PCURSOR");

			for (int i = 0; i < pCursorArea.length(); i++) {
				JSONObject f = pCursorArea.getJSONObject(i);

				pCorp.add(new SelectItem(f.get("NOME").toString(), f.get("CODIGO").toString()));

			} // close for
			
			
			} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
        
        
//		pCorp.add(new SelectItem("produto corporativo 01","produto corporativo 01"));
//		pCorp.add(new SelectItem("produto corporativo 02","produto corporativo 02"));
//		pCorp.add(new SelectItem("produto corporativo 03","produto corporativo 03"));
	    
		return pCorp;
	}
	
	
	public List<SelectItem> consultarPAux() {
		
		pAux = new ArrayList<SelectItem>();
		
        try {
			String retorno = pesquisaOS.consultarPAux();
				
			JSONObject consultaArea = new JSONObject(retorno);
			JSONArray pCursorArea = consultaArea.getJSONArray("PCURSOR");

			for (int i = 0; i < pCursorArea.length(); i++) {
				JSONObject f = pCursorArea.getJSONObject(i);

				pAux.add(new SelectItem(f.get("NOME").toString(), f.get("CODIGO").toString()));

			} // close for
			
			
			} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
        
        
//		pAux.add(new SelectItem("produto auxiliar 01","produto auxiliar 01"));
//		pAux.add(new SelectItem("produto auxiliar 02","produto auxiliar 02"));
//		pAux.add(new SelectItem("produto auxiliar 03","produto auxiliar 03"));
//		pAux.add(new SelectItem("produto auxiliar 04","produto auxiliar 04"));
	    
		return pAux;
	}	
	
	
	
}
