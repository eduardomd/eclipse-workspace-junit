package com.altec.bsbr.app.hyb.web.jsf;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import com.altec.bsbr.app.hyb.dto.ObjRsOSExc;

@ManagedBean(name="excluirOSIframeBean")
@ViewScoped
public class ExcluirOSIframeBean implements Serializable {
	private static final long serialVersionUID = 1L;
    
	private String strNrSeqOs;
	
	@PostConstruct
	public void init() {
		this.strNrSeqOs = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs");
	}

	private ObjRsOSExc objRs;

	public ObjRsOSExc getObjRs() {
		objRs = new ObjRsOSExc();
		objRs.setCODIGO("1234");
		objRs.setSITUACAO("EX");
		objRs.setCD_NOTI("2010");
		objRs.setMOT_REC_EXCL("");
		return objRs;
	}
	
	public void excluirOS() {
		//set objRsNrSeqNoti = server.CreateObject("ADODB.RECORDSET")
		//set objRsNrSeqNoti = objAcoesOs.IncluirProxWorkFlow(strNrSeqOs, "Ex", "0", objRs.getMOT_REC_APROV(), objRs.getMOT_REC_EXCL(), strErro)
		//VerificaErro(strErro)  
		//'Log
		//InsereLog EXCLUIR_OS, CPF_USUARIO, strNrSeqOs, ""
		
		RequestContext.getCurrentInstance().execute("alert('Exclus�o l�gica da Ordem de Servi�o n� "+ strNrSeqOs + " foi efetuada com sucesso!')");	
	}
    
    public String getStrNrSeqOs() {
		return this.strNrSeqOs;
	}

	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}

}