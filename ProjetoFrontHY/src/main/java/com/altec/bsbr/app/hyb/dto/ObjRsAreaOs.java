package com.altec.bsbr.app.hyb.dto;

public class ObjRsAreaOs {
	private String CODIGO;
	
	public ObjRsAreaOs() {}
	
	public ObjRsAreaOs(String cODIGO) {
		super();
		CODIGO = cODIGO;
	}

	public String getCODIGO() {
		return CODIGO;
	}

	public void setCODIGO(String cODIGO) {
		CODIGO = cODIGO;
	}
	
}
