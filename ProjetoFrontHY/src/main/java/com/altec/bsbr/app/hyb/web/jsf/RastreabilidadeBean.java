package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.application.FacesMessage;

import java.util.ArrayList;

import javax.annotation.PostConstruct;

import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.fw.security.SecurityInfo;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.ObjRs;
import com.altec.bsbr.app.hyb.dto.UsuarioIncModel;
import com.altec.bsbr.app.hyb.web.util.XHYUsuarioIncService;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAcoesOs.XHYAcoesOsEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYUsuario.XHYUsuarioEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAcoesOs.WebServiceException;

import com.altec.bsbr.fw.security.SecurityInfo;
import com.altec.bsbr.fw.security.authorization.Authorization;
import com.altec.bsbr.fw.security.authorization.menu.MBSMenuItem;



@Component("RastreabilidadeBean")
@Scope("session")
public class RastreabilidadeBean extends BasicBBean {

	@Autowired
	private XHYAcoesOsEndPoint acoesos;
	
	@Autowired
	private XHYUsuarioIncService usuarioService;


	private UsuarioIncModel usuario;
	
	@Autowired 
	 private Authorization authorization;

	
	private static final long serialVersionUID = 1L;
	private List<ObjRs> objRs;
	private String strNrSeqOs;
	private String LOGIN_USUARIO;
	private List<UsuarioIncModel> objRsUsuario;

	
	@PostConstruct
	public void init(){
		cleanSession();
		this.setStrNrSeqOs((String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
				.get("strNrSeqOs"));
		
		instanciarUsuario();
	}	
	
	public void instanciarUsuario() {
		try {
			usuario = usuarioService.getDadosUsuario();
			this.setLOGIN_USUARIO(usuario.getLoginUsuario());
			
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	public List<ObjRs> getObjRs() {

		objRs = new ArrayList<ObjRs>();

		String retorno = "";
		try {
			retorno = acoesos.consultarRastreabilidadeOs(this.getStrNrSeqOs());

			JSONObject consultaRecurso = new JSONObject(retorno);
			JSONArray pcursor = consultaRecurso.getJSONArray("PCURSOR");

			for (int i = 0; i < pcursor.length(); i++) {

				JSONObject f = pcursor.getJSONObject(i);

				objRs.add(new ObjRs(f.get("DATA_ATUAL").toString(), f.get("HORA_ATUAL").toString(),
						f.get("DATA").toString(), f.get("CD_SITUACAO").toString(), f.get("NM_SITUACAO").toString(),
						f.get("NOTIFICACAO").toString().replace((char) 0xff, (char) 0x2d)));
				System.out.println(f.get("NOTIFICACAO").toString().replace((char) 0xff, (char) 0x2d));

			} // close for
			


		} catch (WebServiceException e) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
         } catch (IOException e1) {
               // TODO Auto-generated catch block
               e1.printStackTrace();
         }
		}
		
	
		return objRs;

	}
	
    public void cleanSession() {
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getSessionMap().remove("RastreabilidadeBean");
  }

	public String getStrNrSeqOs() {
		return strNrSeqOs;
	}

	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}

	public String getLOGIN_USUARIO() {
		return LOGIN_USUARIO;
	}

	public void setLOGIN_USUARIO(String lOGIN_USUARIO) {
		LOGIN_USUARIO = lOGIN_USUARIO;
	}

	public List<UsuarioIncModel> getObjRsUsuario() {
		return objRsUsuario;
	}

	public void setObjRsUsuario(List<UsuarioIncModel> objRsUsuario) {
		this.objRsUsuario = objRsUsuario;
	}


}
