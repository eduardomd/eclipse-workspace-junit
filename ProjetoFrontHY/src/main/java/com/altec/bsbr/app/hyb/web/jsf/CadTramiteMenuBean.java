package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.jab.hyb.webclient.XHYTramite.XHYTramiteEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;


@Component("tramiteMenuBean")
@Scope("request")
public class CadTramiteMenuBean extends BasicBBean {
	private static final long serialVersionUID = 1L;
	
	private String activateBean;

	private String strOsData;
	private String tramiteData;
	private String dataRegData;
	private String dataPlanData;
	private String dataRetData;
	private String destData;
	private String histEnvData;
	private String histRetData;
	
	private String checkOSStatus;
	
	private String strOsQuery;
	private String nrTramiteQuery;
	@Autowired
	private XHYTramiteEndPoint admtramite;
	
	@PostConstruct
	public void init() {
		
		try {
			
			this.strOsQuery = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrOs") == null ? "" : FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrOs").toString();
			this.nrTramiteQuery = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("NrTramite") == null ? "" : (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("NrTramite").toString();
			
			System.out.println("OS QUERY: " + strOsQuery);
			System.out.println("TRAMITE QUERY  : " + nrTramiteQuery);
			
			if ( !strOsQuery.isEmpty() && !nrTramiteQuery.isEmpty() ) {
				
				this.setCheckOSStatus("A");
				
				String usuaRetorno = admtramite.consultarOSTramite(strOsQuery, nrTramiteQuery);
				
				JSONObject pesqQueryTramite = new JSONObject(usuaRetorno);
	            JSONArray pcursor = pesqQueryTramite.getJSONArray("PCURSOR");
	            System.out.println("CONSULTA:  " + pcursor);
	            
	            if (pcursor.length() > 0) {
			        JSONObject item = pcursor.getJSONObject(0);
			           	
			        String jsonOS = item.isNull("OS") ? "" : item.getString("OS");
			        String jsonTramite = item.isNull("TRAMITE") ? "" : String.valueOf(item.getInt("TRAMITE"));
			        String jsonRegistro = item.isNull("REGISTRO") ? null : item.getString("REGISTRO");
			        String jsonRetPlan = item.isNull("RET_PLAN") ? null : item.getString("RET_PLAN");
			        String jsonRetorno = item.isNull("RETORNO") ? null : item.getString("RETORNO");
			        String jsonDestino = item.isNull("DESTINO") ? "" : item.get("DESTINO").toString();
			        String jsonHistEnvio = item.isNull("HIST_ENVIO") ? "" : item.get("HIST_ENVIO").toString();
			        String jsonHistRet = item.isNull("HIST_RET") ? "" : item.get("HIST_RET").toString();
			           
			        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
//			        SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			
			        System.out.println("JSON: \n" +
			        		"OS : " + jsonOS + 
			        		"TRAMITE :" + jsonTramite +
			        		"DT REGISTR O: " + jsonRegistro +
			        		"DT RETOR NO PLAN: " + jsonRetPlan + 
			        		"DT RETORNO: " + jsonRetorno +
			        		"DEST: " + jsonDestino +
			        		"HIST 1:" + jsonHistEnvio +
			        		"HIST 2: " + jsonHistRet);
			        
			        Date dateRegistro = format.parse(jsonRegistro);
			        String dateRegistroFinal = format.format(dateRegistro);
		        
			        Date dateRetPlan = format.parse(jsonRetPlan);
			        String dateRetPlanFinal = format.format(dateRetPlan);
			        
			        if (jsonRetorno != null) {
				       	Date dateRetorno = format.parse(jsonRetorno);
			       		String dateRetornoFinal = format.format(dateRetorno);
			       		this.setDataRetData(dateRetornoFinal);
			        } else {
			        	this.setDataRetData("");
			        }
			           
			        this.setDataRegData(dateRegistroFinal);
			        this.setDataPlanData(dateRetPlanFinal);
			        this.setStrOsData(jsonOS);
			        this.setTramiteData(jsonTramite);
			        this.setDestData(jsonDestino);
			        this.setHistEnvData(jsonHistEnvio);
			        this.setHistRetData(jsonHistRet);
			        
	            }
			}
		} catch (Exception e) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
             } catch (IOException e1) {
                   e1.printStackTrace();
             }
	      }

	}

	public void handleDateReg(SelectEvent event) {
		Date date = (Date) event.getObject();
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

        String dateRegistro = format.format(date);
        this.setDataRegData(dateRegistro);
	}
	
	public void handleDatePlan(SelectEvent event) {
		Date date = (Date) event.getObject();
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

        String dateRegistro = format.format(date);
        this.setDataPlanData(dateRegistro);
	}
	
	public void handleDateRet(SelectEvent event) {
		Date date = (Date) event.getObject();
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

        String dateRegistro = format.format(date);
        this.setDataRetData(dateRegistro);
	}
	
	public void cleanSession() {
		FacesContext context = FacesContext.getCurrentInstance(); 
		context.getExternalContext().getSessionMap().remove("cadTramite");

		try {
			reload();
		} catch (Exception e) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
             } catch (IOException e1) {
	                   e1.printStackTrace();
             }
	      }
		
	}
	
	public void cleanSession2() {
		FacesContext context = FacesContext.getCurrentInstance(); 
		context.getExternalContext().getSessionMap().remove("cadTramite");
		
	}
	
	public void reload() throws IOException {
	    ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
	    ec.redirect("hyb_cad_tramite_menu.xhtml");
	}

	public String getActivateBean() {
		return activateBean;
	}

	public void setActivateBean(String activateBean) {
		this.activateBean = activateBean;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getStrOsQuery() {
		return strOsQuery;
	}

	public void setStrOsQuery(String strOsQuery) {
		this.strOsQuery = strOsQuery;
	}

	public String getNrTramiteQuery() {
		return nrTramiteQuery;
	}

	public void setNrTramiteQuery(String nrTramiteQuery) {
		this.nrTramiteQuery = nrTramiteQuery;
	}

	public XHYTramiteEndPoint getAdmtramite() {
		return admtramite;
	}

	public void setAdmtramite(XHYTramiteEndPoint admtramite) {
		this.admtramite = admtramite;
	}

	public String getStrOsData() {
		return strOsData;
	}

	public void setStrOsData(String strOsData) {
		this.strOsData = strOsData;
	}

	public String getTramiteData() {
		return tramiteData;
	}

	public void setTramiteData(String tramiteData) {
		this.tramiteData = tramiteData;
	}

	public String getDataRegData() {
		return dataRegData;
	}

	public void setDataRegData(String dataRegData) {
		this.dataRegData = dataRegData;
	}

	public String getDataPlanData() {
		return dataPlanData;
	}

	public void setDataPlanData(String dataPlanData) {
		this.dataPlanData = dataPlanData;
	}

	public String getDataRetData() {
		return dataRetData;
	}

	public void setDataRetData(String dataRetData) {
		this.dataRetData = dataRetData;
	}

	public String getDestData() {
		return destData;
	}

	public void setDestData(String destData) {
		this.destData = destData;
	}

	public String getHistEnvData() {
		return histEnvData;
	}

	public void setHistEnvData(String histEnvData) {
		this.histEnvData = histEnvData;
	}

	public String getHistRetData() {
		return histRetData;
	}

	public void setHistRetData(String histRetData) {
		this.histRetData = histRetData;
	}

	public String getCheckOSStatus() {
		return checkOSStatus;
	}

	public void setCheckOSStatus(String checkOSStatus) {
		this.checkOSStatus = checkOSStatus;
	}
	
}