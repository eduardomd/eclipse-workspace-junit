package com.altec.bsbr.app.hyb.web.jsf;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

@Component("contabilizacaoPopBean")
@Scope("session")
public class ContabilizacaoPopBean extends BasicBBean {

	private Object strNrSeqOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs");
	
	@PostConstruct
	public void init() {
	}
	
	
	public Object getStrNrSeqOs() {
		return strNrSeqOs;
	}
	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}	
	
}
