package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.altec.bsbr.fw.web.jsf.BasicBBean;

import com.altec.bsbr.app.hyb.dto.CnalOrigModel;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmGer.XHYAdmGerEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmCanalOrigem.XHYAdmCanalOrigemEndPoint;
import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;

@Component("AdmGerManutCnalOrigBean")
@Scope("session")
public class AdmGerManutCnalOrigBean extends BasicBBean {

	private static final long serialVersionUID = 1L;

	private static final String TABLE_CARREGANDO = "Carregando...";
	private static final String TABLE_VAZIO = "Sem registro"; 
	
	@Autowired
	private XHYAdmGerEndPoint objAdmGer;

	@Autowired
	private XHYAdmCanalOrigemEndPoint objAdmCanalOrigem;

	private Object strMsg = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strMsg");
	
	private LazyDataModel<CnalOrigModel> objRsAdmCnalOrig;
	private List<CnalOrigModel> objRsArea;

	private String txtAddCanalOrigem;
	private String txtHdCodCnalOrig;
	private String txtHdCnalOrig;
	private String txtHdAuxCnalOrig;
	private String txtHdAuxAreaCnalOrig;
	private int areaEvento;

	private String emptyMessage;
	
	private Map<String, Integer> manutCanalChecked;
	
	@PostConstruct
	public void init() throws IOException {
		emptyMessage = TABLE_CARREGANDO;
		consultarArea();
	}

	private List<CnalOrigModel> consultarArea() {

		this.objRsArea = new ArrayList<CnalOrigModel>();
		
		JSONArray pcursorObjRsArea = new JSONArray();
		String objRsAreaTemp;

		try {
			objRsAreaTemp = objAdmGer.consultarArea();
			JSONObject objRsAreaTemp2 = new JSONObject(objRsAreaTemp);
			pcursorObjRsArea = objRsAreaTemp2.getJSONArray("PCURSOR");
			
			for (int i = 0; i < pcursorObjRsArea.length(); i++) {
				JSONObject temp = pcursorObjRsArea.getJSONObject(i);

				objRsArea.add(new CnalOrigModel(temp.get("CODIGO").toString(), temp.get("NOME").toString(),
						temp.get("EXCLUSAO").toString()));
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		return objRsArea;

	}

	public void consultarCanal_Lazy() {
    	
		emptyMessage = TABLE_VAZIO;   
    	
		setObjRsAdmCnalOrig(new LazyDataModel<CnalOrigModel>() {

			private static final long serialVersionUID = 1L;
			
			@Override
			public List<CnalOrigModel> load(int first, int pageSize, String sortField,
                                                    SortOrder sortOrder, Map<String, Object> filters) {
	
				List<CnalOrigModel> objRsCnalOrigModelTemp = consultarCanal();
				
				setRowCount(objRsCnalOrigModelTemp.size());
				setPageSize(pageSize);
				
				List<CnalOrigModel> result = new ArrayList<CnalOrigModel>();
				
				if(manutCanalChecked == null)
					manutCanalChecked = new HashMap<String, Integer>();
				
				manutCanalChecked.clear();
				int criarConexao = 0;
				
				for (int i = first; i < (first + pageSize); i++) {
					
					result.add(objRsCnalOrigModelTemp.get(i));
					
					for (int j = 0; j < objRsArea.size(); j++) {
						
						String chave = objRsArea.get(j).getCodigo() + "_" + objRsCnalOrigModelTemp.get(i).getCodigo();
												
						manutCanalChecked.put(chave, verificaAreaCanalOrigem(criarConexao, objRsArea.get(j).getCodigo(), objRsCnalOrigModelTemp.get(i).getCodigo()));					
					
						criarConexao++;
					}
				}
				
				System.out.println("Size MAP::: " + manutCanalChecked.size());
				
				return result;
			}
		});
    }
	
	private List<CnalOrigModel> consultarCanal()  {

		List<CnalOrigModel> objRsAdmCnalOrigTmp = new ArrayList<CnalOrigModel>();

		JSONArray pcursorObjRsAdmCnalOrigTemp = new JSONArray();
		String objRsAdmCnalOrigTemp;

		try {
			objRsAdmCnalOrigTemp = objAdmCanalOrigem.consultarCanalOrigem();
			JSONObject objRsAdmCnalOrigTemp2 = new JSONObject(objRsAdmCnalOrigTemp);
			pcursorObjRsAdmCnalOrigTemp = objRsAdmCnalOrigTemp2.getJSONArray("PCURSOR");
			
			for (int i = 0; i < pcursorObjRsAdmCnalOrigTemp.length(); i++) {
				JSONObject temp = pcursorObjRsAdmCnalOrigTemp.getJSONObject(i);

				objRsAdmCnalOrigTmp.add(new CnalOrigModel(temp.get("CODIGO").toString(), temp.get("NOME").toString(),
						temp.get("EXCLUSAO").toString()));
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		return objRsAdmCnalOrigTmp;
	}

	private Integer verificaAreaCanalOrigem(int index, String intCodArea, String intCodCanalOrigem) {

		String objRsVerificaAreaCanalOrigem = "";
		Integer retorno = 0;
		
		//System.out.println(intCodArea + " Teste " + intCodCanalOrigem);

		try {
			objRsVerificaAreaCanalOrigem = objAdmCanalOrigem.verificarAreaCanalOrigem(index, Integer.parseInt(intCodArea),
			Integer.parseInt(intCodCanalOrigem));
			
			JSONObject objRsVerificaAreaCanalOrigemTemp = new JSONObject(objRsVerificaAreaCanalOrigem);
			JSONArray pcursor = objRsVerificaAreaCanalOrigemTemp.getJSONArray("PCURSOR");
			JSONObject f = pcursor.getJSONObject(0);

			if (f.get("RELAC").toString().equals("1")) {
				retorno = 1;
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		return retorno;
	}

    public Integer isAreaCanalChecked(int intCodEvento, int intCodCanal) {
		String chave = intCodEvento + "_" + intCodCanal;
		return manutCanalChecked.get(chave);
    }
	
	public void adicionar() {

		Map<String, String> requestParamMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		String registro = requestParamMap.get("frm:txtAddCanalOrigem");
		txtAddCanalOrigem = "";
		
		boolean contains = false;
		
		for (CnalOrigModel canal : objRsAdmCnalOrig) {
			if (canal.getNome().toUpperCase().equals(registro.toUpperCase())) {
				contains = true;
			}
		}
       
		if (contains) {
			RequestContext.getCurrentInstance().execute("alert('Canal de Origem existente! Digite nova descriÃ§Ã£o!!')");
		} else {
			
				try {
					objAdmCanalOrigem.incluirCanalOrigem(registro);
					RequestContext.getCurrentInstance().execute("alert('Canal de Origem incluÃ­do com sucesso!')"); 
					consultarCanal();
				} catch (Exception e) {
					try {
						FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
		}
	}

	public void deletar() {
		
		Map<String, String> requestParamMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		String idRegistro = requestParamMap.get("frm:txtHdCodCnalOrig");
		
		try {
			objAdmCanalOrigem.apagarCanalOrigem(Long.parseLong(idRegistro));
			RequestContext.getCurrentInstance().execute("alert('Evento excluÃ­do com sucesso!')");
			consultarCanal_Lazy();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	public void salvar() {
		Map<String, String> requestParamMap = FacesContext.getCurrentInstance().getExternalContext()
				.getRequestParameterMap();
/*		System.out.println("Salvar: " + requestParamMap.get("frm:txtHdAuxCnalOrig"));
		System.out.println("Salvar: " + requestParamMap.get("frm:txtHdAuxAreaCnalOrig"));*/
		
		String result = requestParamMap.get("frm:txtHdAuxCnalOrig");
		//System.out.println(result);
		
		try {
			objAdmCanalOrigem.arrayAlterarCanalOrigem(result);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		result = requestParamMap.get("frm:txtHdAuxAreaCnalOrig");
		System.out.println(result);
		
		try {
			objAdmCanalOrigem.arrayAreaCanalOrigem(result);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		RequestContext.getCurrentInstance().execute("alert('AlteraÃ§Ãµes efetuadas com sucesso!')"); 
			
		consultarArea();
		consultarCanal_Lazy();
	}

	public int getObjRsAreaLength() {
		return this.objRsArea.size();
	}

	public int gerarId() { 
		return (int) (Math.random() * 1000);
	}

	public String getStrMsg() {
		if (strMsg == null) {
			return "";
		}
		return strMsg.toString();
	}

	public void setStrMsg(String strMsg) {
		this.strMsg = strMsg;
	}

	public List<CnalOrigModel> getObjRsArea() {
		return objRsArea;
	}

	public void setObjRsArea(List<CnalOrigModel> objRsArea) {
		this.objRsArea = objRsArea;
	}

	public String getTxtAddCanalOrigem() {
		return txtAddCanalOrigem;
	}

	public void setTxtAddCanalOrigem(String txtAddCanalOrigem) {
		this.txtAddCanalOrigem = txtAddCanalOrigem;
	}

	public String getTxtHdCodCnalOrig() {
		return txtHdCodCnalOrig;
	}

	public void setTxtHdCodCnalOrig(String txtHdCodCnalOrig) {
		this.txtHdCodCnalOrig = txtHdCodCnalOrig;
	}

	public String getTxtHdCnalOrig() {
		return txtHdCnalOrig;
	}

	public void setTxtHdCnalOrig(String txtHdCnalOrig) {
		this.txtHdCnalOrig = txtHdCnalOrig;
	}

	public String getTxtHdAuxCnalOrig() {
		return txtHdAuxCnalOrig;
	}

	public void setTxtHdAuxCnalOrig(String txtHdAuxCnalOrig) {
		this.txtHdAuxCnalOrig = txtHdAuxCnalOrig;
	}

	public String getTxtHdAuxAreaCnalOrig() {
		return txtHdAuxAreaCnalOrig;
	}

	public void setTxtHdAuxAreaCnalOrig(String txtHdAuxAreaCnalOrig) {
		this.txtHdAuxAreaCnalOrig = txtHdAuxAreaCnalOrig;
	}

	public int getAreaEvento() {
		return areaEvento;
	}

	public void setAreaEvento(int areaEvento) {
		this.areaEvento = areaEvento;
	}

	public String getEmptyMessage() {
		return emptyMessage;
	}

	public void setEmptyMessage(String emptyMessage) {
		this.emptyMessage = emptyMessage;
	}

	public LazyDataModel<CnalOrigModel> getObjRsAdmCnalOrig() {
		return objRsAdmCnalOrig;
	}

	public void setObjRsAdmCnalOrig(LazyDataModel<CnalOrigModel> objRsAdmCnalOrig) {
		this.objRsAdmCnalOrig = objRsAdmCnalOrig;
	}

}