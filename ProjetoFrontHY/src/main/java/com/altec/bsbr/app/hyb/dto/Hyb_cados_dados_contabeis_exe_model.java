package com.altec.bsbr.app.hyb.dto;

public class Hyb_cados_dados_contabeis_exe_model {
	
	private String SQ_DEPE;
	private String NO_DEPE;
	private String SQ_TIPO_EVPD_PAI;
	private String SQ_TIPO_EVEN_PERD;
	private String NO_TIPO_EVEN_PERD;
	private String SQ_LINH_NEGO;
	private String DE_LINH_NEGO;
	
	public Hyb_cados_dados_contabeis_exe_model() {
		
	}
	
	public Hyb_cados_dados_contabeis_exe_model(String sQ_DEPE, String nO_DEPE, String sQ_TIPO_EVPD_PAI,
			String sQ_TIPO_EVEN_PERD, String nO_TIPO_EVEN_PERD, String sQ_LINH_NEGO, String dE_LINH_NEGO) {
		SQ_DEPE = sQ_DEPE;
		NO_DEPE = nO_DEPE;
		SQ_TIPO_EVPD_PAI = sQ_TIPO_EVPD_PAI;
		SQ_TIPO_EVEN_PERD = sQ_TIPO_EVEN_PERD;
		NO_TIPO_EVEN_PERD = nO_TIPO_EVEN_PERD;
		SQ_LINH_NEGO = sQ_LINH_NEGO;
		DE_LINH_NEGO = dE_LINH_NEGO;
	}
	
	public String getSQ_DEPE() {
		return SQ_DEPE;
	}
	public void setSQ_DEPE(String sQ_DEPE) {
		SQ_DEPE = sQ_DEPE;
	}
	public String getNO_DEPE() {
		return NO_DEPE;
	}
	public void setNO_DEPE(String nO_DEPE) {
		NO_DEPE = nO_DEPE;
	}
	public String getSQ_TIPO_EVPD_PAI() {
		return SQ_TIPO_EVPD_PAI;
	}
	public void setSQ_TIPO_EVPD_PAI(String sQ_TIPO_EVPD_PAI) {
		SQ_TIPO_EVPD_PAI = sQ_TIPO_EVPD_PAI;
	}
	public String getSQ_TIPO_EVEN_PERD() {
		return SQ_TIPO_EVEN_PERD;
	}
	public void setSQ_TIPO_EVEN_PERD(String sQ_TIPO_EVEN_PERD) {
		SQ_TIPO_EVEN_PERD = sQ_TIPO_EVEN_PERD;
	}
	public String getNO_TIPO_EVEN_PERD() {
		return NO_TIPO_EVEN_PERD;
	}
	public void setNO_TIPO_EVEN_PERD(String nO_TIPO_EVEN_PERD) {
		NO_TIPO_EVEN_PERD = nO_TIPO_EVEN_PERD;
	}
	public String getSQ_LINH_NEGO() {
		return SQ_LINH_NEGO;
	}
	public void setSQ_LINH_NEGO(String sQ_LINH_NEGO) {
		SQ_LINH_NEGO = sQ_LINH_NEGO;
	}
	public String getDE_LINH_NEGO() {
		return DE_LINH_NEGO;
	}
	public void setDE_LINH_NEGO(String dE_LINH_NEGO) {
		DE_LINH_NEGO = dE_LINH_NEGO;
	}
	
}
