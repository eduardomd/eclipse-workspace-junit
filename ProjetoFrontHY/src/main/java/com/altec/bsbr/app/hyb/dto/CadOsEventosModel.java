package com.altec.bsbr.app.hyb.dto;

public class CadOsEventosModel {

	private String cdEmprGrup; 
	private String cdAreaComp;
	private String cdLinhNegonive_1;		
	private String cdLinhNegonive_2;
	private String cdFatrRisc;
	private String cdCptu;
	private boolean intApurResp;
	private boolean intDivuIndiInfoClie;
	private boolean intDivuIndiInfoBanc;
	private boolean intTrmsMesgCntdIndq;
	private boolean intVendCasd;
	private boolean intVendNaoForz;
	private boolean intFals;
	private String strNmOutrPratIndq;
	private boolean intRstrFincCadr;
	private boolean intCdtaProfIndq;
	private boolean intDscmNort;
	private boolean intViolCodiEtic;
	private boolean intEndiExce;
	private boolean intApprValoClie;
	private boolean intApprValoBanc;
	private boolean intMoviFincIrre;
	private boolean intOperCred;
	private boolean intCntrProd;
	private boolean intAberIrreCnta;
	private boolean intEmailQubrSigi;
	private boolean intEmailVazaInfo;
	private boolean intEmailCrrt;
	private boolean intEmailExpcRisc;
	private boolean intEmailPorn;
	private boolean intEmailBtpp;
	private boolean intEmailOutr;
	private boolean intRstrInci;
	private boolean intRstrReic;
	private boolean intMoviQntd;
	private boolean intMoviValo;
	private boolean intDefrAntiNort;
	private boolean intIrreCnstGara;
	private boolean intUtizTituDuplFria;
	private boolean intFalsificacao;
	private boolean intIrreForz;
	private String strOperCredOutr;
	private String strOutros;
	private int intCdOutrTipoFrau;
	
	public String getCdEmprGrup() {
		return cdEmprGrup;
	}
	public void setCdEmprGrup(String cdEmprGrup) {
		this.cdEmprGrup = cdEmprGrup;
	}
	public String getCdAreaComp() {
		return cdAreaComp;
	}
	public void setCdAreaComp(String cdAreaComp) {
		this.cdAreaComp = cdAreaComp;
	}
	public String getCdLinhNegonive_1() {
		return cdLinhNegonive_1;
	}
	public void setCdLinhNegonive_1(String cdLinhNegonive_1) {
		this.cdLinhNegonive_1 = cdLinhNegonive_1;
	}
	public String getCdLinhNegonive_2() {
		return cdLinhNegonive_2;
	}
	public void setCdLinhNegonive_2(String cdLinhNegonive_2) {
		this.cdLinhNegonive_2 = cdLinhNegonive_2;
	}
	public String getCdFatrRisc() {
		return cdFatrRisc;
	}
	public void setCdFatrRisc(String cdFatrRisc) {
		this.cdFatrRisc = cdFatrRisc;
	}
	public String getCdCptu() {
		return cdCptu;
	}
	public void setCdCptu(String cdCptu) {
		this.cdCptu = cdCptu;
	}
	public boolean getIntApurResp() {
		return intApurResp;
	}
	public void setIntApurResp(boolean intApurResp) {
		this.intApurResp = intApurResp;
	}
	public boolean getIntDivuIndiInfoClie() {
		return intDivuIndiInfoClie;
	}
	public void setIntDivuIndiInfoClie(boolean intDivuIndiInfoClie) {
		this.intDivuIndiInfoClie = intDivuIndiInfoClie;
	}
	public boolean getIntDivuIndiInfoBanc() {
		return intDivuIndiInfoBanc;
	}
	public void setIntDivuIndiInfoBanc(boolean intDivuIndiInfoBanc) {
		this.intDivuIndiInfoBanc = intDivuIndiInfoBanc;
	}
	public boolean getIntTrmsMesgCntdIndq() {
		return intTrmsMesgCntdIndq;
	}
	public void setIntTrmsMesgCntdIndq(boolean intTrmsMesgCntdIndq) {
		this.intTrmsMesgCntdIndq = intTrmsMesgCntdIndq;
	}
	public boolean getIntVendCasd() {
		return intVendCasd;
	}
	public void setIntVendCasd(boolean intVendCasd) {
		this.intVendCasd = intVendCasd;
	}
	public boolean getIntVendNaoForz() {
		return intVendNaoForz;
	}
	public void setIntVendNaoForz(boolean intVendNaoForz) {
		this.intVendNaoForz = intVendNaoForz;
	}
	public boolean getIntFals() {
		return intFals;
	}
	public void setIntFals(boolean intFals) {
		this.intFals = intFals;
	}
	public String getStrNmOutrPratIndq() {
		return strNmOutrPratIndq;
	}
	public void setStrNmOutrPratIndq(String strNmOutrPratIndq) {
		this.strNmOutrPratIndq = strNmOutrPratIndq;
	}
	public boolean getIntRstrFincCadr() {
		return intRstrFincCadr;
	}
	public void setIntRstrFincCadr(boolean intRstrFincCadr) {
		this.intRstrFincCadr = intRstrFincCadr;
	}
	public boolean getIntCdtaProfIndq() {
		return intCdtaProfIndq;
	}
	public void setIntCdtaProfIndq(boolean intCdtaProfIndq) {
		this.intCdtaProfIndq = intCdtaProfIndq;
	}
	public boolean getIntDscmNort() {
		return intDscmNort;
	}
	public void setIntDscmNort(boolean intDscmNort) {
		this.intDscmNort = intDscmNort;
	}
	public boolean getIntViolCodiEtic() {
		return intViolCodiEtic;
	}
	public void setIntViolCodiEtic(boolean intViolCodiEtic) {
		this.intViolCodiEtic = intViolCodiEtic;
	}
	public boolean getIntEndiExce() {
		return intEndiExce;
	}
	public void setIntEndiExce(boolean intEndiExce) {
		this.intEndiExce = intEndiExce;
	}
	public boolean getIntApprValoClie() {
		return intApprValoClie;
	}
	public void setIntApprValoClie(boolean intApprValoClie) {
		this.intApprValoClie = intApprValoClie;
	}
	public boolean getIntApprValoBanc() {
		return intApprValoBanc;
	}
	public void setIntApprValoBanc(boolean intApprValoBanc) {
		this.intApprValoBanc = intApprValoBanc;
	}
	public boolean getIntMoviFincIrre() {
		return intMoviFincIrre;
	}
	public void setIntMoviFincIrre(boolean intMoviFincIrre) {
		this.intMoviFincIrre = intMoviFincIrre;
	}
	public boolean getIntOperCred() {
		return intOperCred;
	}
	public void setIntOperCred(boolean intOperCred) {
		this.intOperCred = intOperCred;
	}
	public boolean getIntCntrProd() {
		return intCntrProd;
	}
	public void setIntCntrProd(boolean intCntrProd) {
		this.intCntrProd = intCntrProd;
	}
	public boolean getIntAberIrreCnta() {
		return intAberIrreCnta;
	}
	public void setIntAberIrreCnta(boolean intAberIrreCnta) {
		this.intAberIrreCnta = intAberIrreCnta;
	}
	public boolean getIntEmailQubrSigi() {
		return intEmailQubrSigi;
	}
	public void setIntEmailQubrSigi(boolean intEmailQubrSigi) {
		this.intEmailQubrSigi = intEmailQubrSigi;
	}
	public boolean getIntEmailVazaInfo() {
		return intEmailVazaInfo;
	}
	public void setIntEmailVazaInfo(boolean intEmailVazaInfo) {
		this.intEmailVazaInfo = intEmailVazaInfo;
	}
	public boolean getIntEmailCrrt() {
		return intEmailCrrt;
	}
	public void setIntEmailCrrt(boolean intEmailCrrt) {
		this.intEmailCrrt = intEmailCrrt;
	}
	public boolean getIntEmailExpcRisc() {
		return intEmailExpcRisc;
	}
	public void setIntEmailExpcRisc(boolean intEmailExpcRisc) {
		this.intEmailExpcRisc = intEmailExpcRisc;
	}
	public boolean getIntEmailPorn() {
		return intEmailPorn;
	}
	public void setIntEmailPorn(boolean intEmailPorn) {
		this.intEmailPorn = intEmailPorn;
	}
	public boolean getIntEmailBtpp() {
		return intEmailBtpp;
	}
	public void setIntEmailBtpp(boolean intEmailBtpp) {
		this.intEmailBtpp = intEmailBtpp;
	}
	public boolean getIntEmailOutr() {
		return intEmailOutr;
	}
	public void setIntEmailOutr(boolean intEmailOutr) {
		this.intEmailOutr = intEmailOutr;
	}
	public boolean getIntRstrInci() {
		return intRstrInci;
	}
	public void setIntRstrInci(boolean intRstrInci) {
		this.intRstrInci = intRstrInci;
	}
	public boolean getIntRstrReic() {
		return intRstrReic;
	}
	public void setIntRstrReic(boolean intRstrReic) {
		this.intRstrReic = intRstrReic;
	}
	public boolean getIntMoviQntd() {
		return intMoviQntd;
	}
	public void setIntMoviQntd(boolean intMoviQntd) {
		this.intMoviQntd = intMoviQntd;
	}
	public boolean getIntMoviValo() {
		return intMoviValo;
	}
	public void setIntMoviValo(boolean intMoviValo) {
		this.intMoviValo = intMoviValo;
	}
	public boolean getIntDefrAntiNort() {
		return intDefrAntiNort;
	}
	public void setIntDefrAntiNort(boolean intDefrAntiNort) {
		this.intDefrAntiNort = intDefrAntiNort;
	}
	public boolean getIntIrreCnstGara() {
		return intIrreCnstGara;
	}
	public void setIntIrreCnstGara(boolean intIrreCnstGara) {
		this.intIrreCnstGara = intIrreCnstGara;
	}
	public boolean getIntUtizTituDuplFria() {
		return intUtizTituDuplFria;
	}
	public void setIntUtizTituDuplFria(boolean intUtizTituDuplFria) {
		this.intUtizTituDuplFria = intUtizTituDuplFria;
	}
	public boolean getIntFalsificacao() {
		return intFalsificacao;
	}
	public void setIntFalsificacao(boolean intFalsificacao) {
		this.intFalsificacao = intFalsificacao;
	}
	public boolean getIntIrreForz() {
		return intIrreForz;
	}
	public void setIntIrreForz(boolean intIrreForz) {
		this.intIrreForz = intIrreForz;
	}
	public String getStrOperCredOutr() {
		return strOperCredOutr;
	}
	public void setStrOperCredOutr(String strOperCredOutr) {
		this.strOperCredOutr = strOperCredOutr;
	}
	public String getStrOutros() {
		return strOutros;
	}
	public void setStrOutros(String strOutros) {
		this.strOutros = strOutros;
	}
	public int getIntCdOutrTipoFrau() {
		return intCdOutrTipoFrau;
	}
	public void setIntCdOutrTipoFrau(int intCdOutrTipoFrau) {
		this.intCdOutrTipoFrau = intCdOutrTipoFrau;
	}

}
