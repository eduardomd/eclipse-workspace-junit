package com.altec.bsbr.app.hyb.dto;

import javax.faces.bean.ManagedBean;

@ManagedBean(name = "objRsParticipante")
public class ObjRsParticipante {
	private String POSICAO;
	private String NOME;
	private String TIPO_PARTICIP;
	private String CPF_CNPJ;
	private String DESC_TIPO;
	private String PENALIDADE;
	private String MOTIVO;

	public ObjRsParticipante() {
	}
	
	
	public ObjRsParticipante(String pOSICAO, String nOME, String tIPO_PARTICIP, String cPF_CNPJ, String dESC_TIPO,
			String pENALIDADE, String mOTIVO) {
		super();
		POSICAO = pOSICAO;
		NOME = nOME;
		TIPO_PARTICIP = tIPO_PARTICIP;
		CPF_CNPJ = cPF_CNPJ;
		DESC_TIPO = dESC_TIPO;
		PENALIDADE = pENALIDADE;
		MOTIVO = mOTIVO;
	}
	
	public ObjRsParticipante(String pOSICAO, String nOME, String cPF_CNPJ, String dESC_TIPO,
			String pENALIDADE, String mOTIVO) {
		super();
		POSICAO = pOSICAO;
		NOME = nOME;
		CPF_CNPJ = cPF_CNPJ;
		DESC_TIPO = dESC_TIPO;
		PENALIDADE = pENALIDADE;
		MOTIVO = mOTIVO;
	}



	public ObjRsParticipante(String posicao, String nome, String cpf_cnpj) {
		this.POSICAO = posicao;
		this.NOME = nome;
		this.CPF_CNPJ = cpf_cnpj;
	}

	public String getPOSICAO() {
		return POSICAO;
	}

	public void setPOSICAO(String pOSICAO) {
		POSICAO = pOSICAO;
	}

	public String getNOME() {
		return NOME;
	}

	public void setNOME(String nOME) {
		NOME = nOME;
	}

	public String getTIPO_PARTICIP() {
		return TIPO_PARTICIP;
	}

	public void setTIPO_PARTICIP(String tIPO_PARTICIP) {
		TIPO_PARTICIP = tIPO_PARTICIP;
	}

	public String getCPF_CNPJ() {
		return CPF_CNPJ;
	}

	public void setCPF_CNPJ(String cPF_CNPJ) {
		CPF_CNPJ = cPF_CNPJ;
	}

	public String getDESC_TIPO() {
		return DESC_TIPO;
	}

	public void setDESC_TIPO(String dESC_TIPO) {
		DESC_TIPO = dESC_TIPO;
	}

	public String getPENALIDADE() {
		return PENALIDADE;
	}

	public void setPENALIDADE(String pENALIDADE) {
		PENALIDADE = pENALIDADE;
	}

	public String getMOTIVO() {
		return MOTIVO;
	}

	public void setMOTIVO(String mOTIVO) {
		MOTIVO = mOTIVO;
	}

}
