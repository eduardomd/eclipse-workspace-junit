package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;

import com.altec.bsbr.app.hyb.dto.CadOsAcoesComplModel;
import com.altec.bsbr.app.hyb.dto.UsuarioIncModel;
import com.altec.bsbr.app.hyb.web.util.LogIncUtil;
import com.altec.bsbr.app.hyb.web.util.XHYUsuarioIncService;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmLog.XHYAdmLogEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsAcoesCompl.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsAcoesCompl.XHYCadOsAcoesComplEndPoint;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.altec.bsbr.app.jab.hyb.webclient.XHYUsuario.XHYUsuarioEndPoint;

import com.altec.bsbr.fw.security.SecurityInfo;
import com.altec.bsbr.fw.security.authorization.Authorization;
import com.altec.bsbr.fw.security.authorization.menu.MBSMenuItem;

@Component("cadOsAcoesComplBean")
@Scope("session")
public class CadOsAcoesComplBean extends BasicBBean {

	private static final long serialVersionUID = 1L;
	private List<CadOsAcoesComplModel> objRsCadOsAcoesCompl;

	private List<UsuarioIncModel> objRsUsuario;
	private String strTitulo;
	private String strNrSeqOs;
	private String strReadOnly;
	private boolean desabilitarCampos = false;

	CadOsAcoesComplModel cadOsAcoesComplModel;

	@Autowired
	private XHYUsuarioIncService usuarioService;

	private UsuarioIncModel usuario;

	@Autowired
	private XHYAdmLogEndPoint admLog;

	@Autowired
	private Authorization authorization;

	@Autowired
	private XHYCadOsAcoesComplEndPoint cadOsAcoesCompl;

	@PostConstruct
	public void init() {

		this.strTitulo = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
				.get("strTitulo");
		this.strNrSeqOs = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
				.get("strNrSeqOs");
		this.strReadOnly = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
				.get("strReadOnly");
		instanciarUsuario();
		CadOsAcoesCompl();
		verificaQueryString();
	}

	public void CadOsAcoesCompl() {

		objRsCadOsAcoesCompl = new ArrayList<CadOsAcoesComplModel>();
		cadOsAcoesComplModel = new CadOsAcoesComplModel();

		cadOsAcoesComplModel.setStrTitulo(this.strTitulo);
		cadOsAcoesComplModel.setStrNrSeqOs(this.strNrSeqOs);

		String retorno = "";
		try {

			retorno = cadOsAcoesCompl.consultarOSAcoesCompl(this.cadOsAcoesComplModel.getStrNrSeqOs());
			JSONObject consultaOsAcoesCompl = new JSONObject(retorno);
			JSONArray pcursor = consultaOsAcoesCompl.getJSONArray("PCURSOR");

			for (int i = 0; i < pcursor.length(); i++) {

				JSONObject f = pcursor.getJSONObject(i);

				cadOsAcoesComplModel = new CadOsAcoesComplModel();

				cadOsAcoesComplModel.setStrNrSeqOs(
						f.isNull("NR_SEQU_ORDE_SERV") == true ? "" : f.get("NR_SEQU_ORDE_SERV").toString());
				cadOsAcoesComplModel
						.setIntNrBo(f.isNull("NR_BOLE_OCOR") == true ? "" : f.get("NR_BOLE_OCOR").toString());
				cadOsAcoesComplModel.setStrNmDelegacia(f.isNull("NM_DELC") == true ? "" : f.get("NM_DELC").toString());
				cadOsAcoesComplModel.setDteRegistro(
						f.isNull("DT_REGT_BOLE_OCOR") == true ? "" : f.get("DT_REGT_BOLE_OCOR").toString());
				cadOsAcoesComplModel
						.setIntInProcJuri(f.isNull("IN_PROC_JURI") == true ? "" : f.get("IN_PROC_JURI").toString());
				cadOsAcoesComplModel.setStrNmForum(
						f.isNull("NM_FORU_PROC_JURI") == true ? "" : f.get("NM_FORU_PROC_JURI").toString());

				objRsCadOsAcoesCompl.add(cadOsAcoesComplModel);

			}

		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}

	}

	public void instanciarUsuario() {
		try {
			usuario = usuarioService.getDadosUsuario();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void verificaQueryString() {
		if (!(this.strReadOnly == null) && !this.strReadOnly.isEmpty()) {
			if (this.strReadOnly.equals("1")) {
				setDesabilitarCampos(true);
			}
		}
	}

	public void CadOSAcoesComplAction() throws IOException {

		if (cadOsAcoesComplModel.getDteRegistro().isEmpty()) {
			this.cadOsAcoesComplModel.setDteRegistro("");
		} else {
			this.cadOsAcoesComplModel.setDteRegistro(convertData(cadOsAcoesComplModel.getDteRegistro()));
		}

		// Change Ações Complementares
		String retorno = "";
		try {
			cadOsAcoesCompl.alterarOSAcoesCompl(cadOsAcoesComplModel.getStrNrSeqOs(), cadOsAcoesComplModel.getIntNrBo(),
					cadOsAcoesComplModel.getStrNmDelegacia(), cadOsAcoesComplModel.getDteRegistro(),
					cadOsAcoesComplModel.getIntInProcJuri(), cadOsAcoesComplModel.getStrNmForum());

			this.objRsCadOsAcoesCompl.clear();
			CadOsAcoesCompl();
			admLog.incluirAcaoLog(cadOsAcoesComplModel.getStrNrSeqOs(),
					Integer.toString(LogIncUtil.SALVAR_ACOES_COMPLEMENTARES_OS), usuario.getCpfUsuario(), "");

			RequestContext.getCurrentInstance().execute("alert('Ações Complementares alterada com sucesso!');");
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

	}

	public static String convertData(String dataInput) {
		String dataInput2 = "";

		if (dataInput.matches("([0-9]{2}/[0-9]{2}/[0-9]{4})")) {
			Date initDate = null;
			try {
				initDate = new SimpleDateFormat("dd/MM/yyyy").parse(dataInput);
			} catch (java.text.ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			dataInput2 = formatter.format(initDate);

		} else {

			try {

				dataInput2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
						.format(new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyy", Locale.US).parse(dataInput));

			} catch (ParseException e) {
				try {
					FacesContext.getCurrentInstance().getExternalContext()
							.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
		return dataInput2;
	}

	public List<CadOsAcoesComplModel> getObjRsCadOsAcoesCompl() {
		return objRsCadOsAcoesCompl;
	}

	public void setObjRsCadOsAcoesCompl(List<CadOsAcoesComplModel> objRsCadOsAcoesCompl) {
		this.objRsCadOsAcoesCompl = objRsCadOsAcoesCompl;
	}

	public String getTxtHdSeqOs() {
		return cadOsAcoesComplModel.getStrNrSeqOs();
	}

	public void setTxtHdSeqOs(String txtHdSeqOs) {
		cadOsAcoesComplModel.setStrNrSeqOs(txtHdSeqOs);
	}

	public List<UsuarioIncModel> getObjRsUsuario() {
		return objRsUsuario;
	}

	public void setObjRsUsuario(List<UsuarioIncModel> objRsUsuario) {
		this.objRsUsuario = objRsUsuario;
	}

	public String getStrTitulo() {
		return strTitulo;
	}

	public void setStrTitulo(String strTitulo) {
		this.strTitulo = strTitulo;
	}

	public String getStrNrSeqOs() {
		return strNrSeqOs;
	}

	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}

	public String getStrReadOnly() {
		return strReadOnly;
	}

	public void setStrReadOnly(String strReadOnly) {
		this.strReadOnly = strReadOnly;
	}

	public boolean isDesabilitarCampos() {
		return desabilitarCampos;
	}

	public void setDesabilitarCampos(boolean desabilitarCampos) {
		this.desabilitarCampos = desabilitarCampos;
	}

}
