package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import java.util.ArrayList;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.altec.bsbr.app.hyb.web.util.ProdutosAuxiliares;
import com.altec.bsbr.app.hyb.web.util.ProdutosCorporativos;
import com.altec.bsbr.app.hyb.web.util.Util;
import com.altec.bsbr.app.hyb.web.util.clsCadOsRiscPoten;

@Component("cadosDetalheRiscopot")
@Scope("session")
public class CadosDetalheRiscopotBean extends BasicBBean {
	
	private static final long serialVersionUID = 1L;
	private String strTitulo = "";
	private String strNrOs = "";
	private String strErro = "";
	private String strIN_RESS = "1";
	private String strBgColor = "#FFFFFF";
	private String rdoRessarc;
	private List<ProdutosCorporativos> objRsProdCorp;
	private List<ProdutosAuxiliares> objRsProdAux;
	
	
	public CadosDetalheRiscopotBean() throws IOException {
		FacesContext fc = FacesContext.getCurrentInstance();
        @SuppressWarnings("unchecked")
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        strTitulo = params.get("strTitulo");
        setStrNrOs(params.get("strNrSeqOs"));
        
        clsCadOsRiscPoten objCadOsRiscPoten = new clsCadOsRiscPoten();
        
	    setObjRsProdAux(objCadOsRiscPoten.fnSelProdAuxiRiscPotenParcial(strNrOs,strErro));
	    Util.verificaErro(strErro);
	    
	    objRsProdCorp = objCadOsRiscPoten.fnSelProdCorpRiscPotenParcial(strNrOs,strErro);
	    Util.verificaErro(strErro);
        
/*	    ProdutosCorporativos temp = new ProdutosCorporativos();
	    temp.setIN_RESS("as");
	    temp.setNM_OPER("wsad");
	    
	    objRsProdCorp.add(temp);*/
	    
	}
	
	public String CadosDetalheRiscopotExeBean() {
	    String objxmlDoc;
	    String strReturn;
	    String strNR_SEQU_ORDE_SERV;
	    String strErro = "";
	    String strCpfUsuario;
	    
	    strCpfUsuario = "1234512"; // carrega da chamada CPF_USUARIO
	    
/*		Set objxmlDoc = Server.CreateObject("Microsoft.XMLDOM")
		
		objxmlDoc.async = false
		
	    'L� as informa��es XML que foram passadas para o ASP
		objxmlDoc.load(Request)*/
		
	    
	    List<ProdutosCorporativos> objRsProdCorp = new ArrayList<ProdutosCorporativos>();
	    objRsProdCorp.add(new ProdutosCorporativos("12312", "12312"));
	    
		strNR_SEQU_ORDE_SERV = ""; // trim(objxmlDoc.documentelement.childnodes(0).text)
		strIN_RESS = ""; // trim(objxmlDoc.documentelement.childnodes(1).text)

		clsCadOsRiscPoten objCadOsRiscPoten = new clsCadOsRiscPoten();
	    strReturn = objCadOsRiscPoten.fnUpdValRiscPotenOS(strNR_SEQU_ORDE_SERV, strIN_RESS, strErro);        
	        
	    return strReturn;
	}

	public void validardoRessarc() {
		
		if(strIN_RESS.isEmpty()) {
			rdoRessarc = "";
		}
		
		if(strIN_RESS.equals("0")) {
			rdoRessarc = "2";
		} else {
			rdoRessarc = "1";
		}
	}
	
 	public String getStrTitulo() {
		return strTitulo;
	}

	public void setStrTitulo(String strTitulo) {
		this.strTitulo = strTitulo;
	}

	public List<ProdutosCorporativos> getObjRsProdCorp() {
		return objRsProdCorp;
	}

	public void setObjRsProdCorp(List<ProdutosCorporativos> objRsProdCorp) {
		this.objRsProdCorp = objRsProdCorp;
	}


	public String getStrIN_RESS() {
		return strIN_RESS;
	}


	public void setStrIN_RESS(String strIN_RESS) {
		this.strIN_RESS = strIN_RESS;
	}


	public String getStrNrOs() {
		return strNrOs;
	}


	public void setStrNrOs(String strNrOs) {
		this.strNrOs = strNrOs;
	}


	public String getStrBgColor() {
		return strBgColor;
	}


	public void setStrBgColor(String strBgColor) {
		this.strBgColor = strBgColor;
	}


	public String getRdoRessarc() {
		return rdoRessarc;
	}


	public void setRdoRessarc(String rdoRessarc) {
		this.rdoRessarc = rdoRessarc;
	}


	public List<ProdutosAuxiliares> getObjRsProdAux() {
		return objRsProdAux;
	}


	public void setObjRsProdAux(List<ProdutosAuxiliares> objRsProdAux) {
		this.objRsProdAux = objRsProdAux;
	}


}
