package com.altec.bsbr.app.hyb.dto;

public class RelosEstatisticoSegmento {
	private String DT_MM_YYYY;
	private String DT_MONTH_YYYY;	
	private String NM_SEGM;
	private String PREJUIZO;
	private String QNTD;
	private String TP_PESS_CLIE;
	
	public RelosEstatisticoSegmento() {}

	public String getDT_MM_YYYY() {
		return DT_MM_YYYY;
	}

	public void setDT_MM_YYYY(String dT_MM_YYYY) {
		DT_MM_YYYY = dT_MM_YYYY;
	}

	public String getDT_MONTH_YYYY() {
		return DT_MONTH_YYYY;
	}

	public void setDT_MONTH_YYYY(String dT_MONTH_YYYY) {
		DT_MONTH_YYYY = dT_MONTH_YYYY;
	}

	public String getNM_SEGM() {
		return NM_SEGM;
	}

	public void setNM_SEGM(String nM_SEGM) {
		NM_SEGM = nM_SEGM;
	}

	public String getPREJUIZO() {
		return PREJUIZO;
	}

	public void setPREJUIZO(String pREJUIZO) {
		PREJUIZO = pREJUIZO;
	}

	public String getQNTD() {
		return QNTD;
	}

	public void setQNTD(String qNTD) {
		QNTD = qNTD;
	}

	public String getTP_PESS_CLIE() {
		return TP_PESS_CLIE;
	}

	public void setTP_PESS_CLIE(String tP_PESS_CLIE) {
		TP_PESS_CLIE = tP_PESS_CLIE;
	}
	
	
	
}
