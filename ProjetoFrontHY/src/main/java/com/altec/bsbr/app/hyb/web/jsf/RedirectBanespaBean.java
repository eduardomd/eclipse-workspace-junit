package com.altec.bsbr.app.hyb.web.jsf;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("redirectBanespaBean")
@Scope("session")
public class RedirectBanespaBean {
	
	private String strAmbiente;
	private String strServer;
	private String strLink;
	
	public RedirectBanespaBean() {
		
	}

	public RedirectBanespaBean(String strAmbiente, String strServer, String strLink) {
		this.strAmbiente = strAmbiente;
		this.strServer = strServer;
		this.strLink = strLink;
	}
	
	@PostConstruct
	public void init() {
		// MOCK
		this.setStrAmbiente("producao");
		//END MOCK
		
		int ambienteLength = (8 - this.getStrAmbiente().length()) + this.getStrAmbiente().length();
		
		// Make the strAmbiente always 8 length (with whitespace), to use the substring method with endIndex = 8,
		// in Strings with length < 8
		strAmbiente = String.format("%-"+ambienteLength+"s", this.getStrAmbiente());
		strAmbiente = this.getStrAmbiente().substring(0, 8).trim().toUpperCase();
		System.out.println(this.getStrAmbiente());
		
		switch (strAmbiente) {
		case "DESENVOL":
			this.setStrServer("http://pjavawebdes.isbanbr.corp/hy-web-admin/");
			break;
		case "PI":
		case "PROVA IN":
			this.setStrServer("http://pjavawebpi.isbanbr.corp/hy-web-admin/");
			break;
		case "HOMOLOGA":
			this.setStrServer("http://pjavawebhml.santanderbr.pre.corp/hy-web-admin/");
			break;
		case "PRODUCAO":
			this.setStrServer("http://pjavawebfuser.santanderbr.corp/hy-web-admin/");
			break;
		}
		
		//RequestContext.getCurrentInstance().execute("window.open('"+this.getStrServer()+"','AUCWindow', ' toolbar=no, status=yes, menubar=yes, resizable=yes, scrollbars=yes, top=0, width=1000, height=670') ;");
	}
	
	public String getStrAmbiente() {
		return strAmbiente;
	}
	
	public void setStrAmbiente(String strAmbiente) {
		this.strAmbiente = strAmbiente;
	}
	
	public String getStrServer() {
		return strServer;
	}
	
	public void setStrServer(String strServer) {
		this.strServer = strServer;
	}
	
	public String getStrLink() {
		return strLink;
	}
	
	public void setStrLink(String strLink) {
		this.strLink = strLink;
	}

}
