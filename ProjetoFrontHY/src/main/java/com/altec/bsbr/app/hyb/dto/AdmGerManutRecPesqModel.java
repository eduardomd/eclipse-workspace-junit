package com.altec.bsbr.app.hyb.dto;

public class AdmGerManutRecPesqModel {

	private String txtCpf;
	private String txtNome;
	private String cboAreaSelecionada;
	private String cdbCpf; // cdb -> column database
	private String cdbNome;
	private String cdbMatricula;
	private String cdbCargo;
	private String cdbTipoRecurso;
	private String cdbArea;
	private String cdbExclusao;

	public AdmGerManutRecPesqModel() {
	}

	public AdmGerManutRecPesqModel(String cdbCpf, String cdbNome, String cdbMatricula, String cdbCargo,
			String cdbTipoRecurso, String cdbArea, String cdbExclusao) {
		this.cdbCpf = cdbCpf;
		this.cdbNome = cdbNome;
		this.cdbMatricula = cdbMatricula;
		this.cdbCargo = cdbCargo;
		this.cdbTipoRecurso = cdbTipoRecurso;
		this.cdbArea = cdbArea;
		this.cdbExclusao = cdbExclusao;
	}

	public String getTxtCpf() {
		return txtCpf;
	}

	public void setTxtCpf(String txtCpf) {
		this.txtCpf = txtCpf.replaceAll("[^0-9]", "");
	}

	public String getTxtNome() {
		return txtNome;
	}

	public void setTxtNome(String txtNome) {
		this.txtNome = txtNome;
	}

	public String getCboAreaSelecionada() {
		return cboAreaSelecionada;
	}

	public void setCboAreaSelecionada(String cboAreaSelecionada) {
		this.cboAreaSelecionada = cboAreaSelecionada;
	}

	public String getCdbCpf() {
		return cdbCpf.substring(0, 3)+"."+cdbCpf.substring(3, 6)+"."+cdbCpf.substring(6, 9)+"-"+cdbCpf.substring(9);
	}

	public void setCdbCpf(String cdbCpf) {
		this.cdbCpf = cdbCpf;
	}

	public String getCdbNome() {
		return cdbNome;
	}

	public void setCdbNome(String cdbNome) {
		this.cdbNome = cdbNome;
	}

	public String getCdbMatricula() {
		return cdbMatricula;
	}

	public void setCdbMatricula(String cdbMatricula) {
		this.cdbMatricula = cdbMatricula;
	}

	public String getCdbCargo() {
		return cdbCargo;
	}

	public void setCdbCargo(String cdbCargo) {
		this.cdbCargo = cdbCargo;
	}

	public String getCdbTipoRecurso() {
		return cdbTipoRecurso;
	}

	public void setCdbTipoRecurso(String cdbTipoRecurso) {
		this.cdbTipoRecurso = cdbTipoRecurso;
	}

	public String getCdbArea() {
		return cdbArea;
	}

	public void setCdbArea(String cdbArea) {
		this.cdbArea = cdbArea;
	}

	public String getCdbExclusao() {
		return cdbExclusao;
	}

	public void setCdbExclusao(String cdbExclusao) {
		this.cdbExclusao = cdbExclusao;
	}
}
