package com.altec.bsbr.app.hyb.dto;

public class JCGG_TCMGEN1AeaResponse {
	
	public JCGG_TCMGEN1AeaResponse() {}

	public JCGG_TCMGEN1AeaResponse(String dC_FORMATO, String tABLA, String eNTIDAD, String iDIOMA, String cLAVE,
			String dATOS1, String dATOS2, String dATOS3) {
		super();
		DC_FORMATO = dC_FORMATO;
		TABLA = tABLA;
		ENTIDAD = eNTIDAD;
		IDIOMA = iDIOMA;
		CLAVE = cLAVE;
		DATOS1 = dATOS1;
		DATOS2 = dATOS2;
		DATOS3 = dATOS3;
	}

	private String DC_FORMATO;
	
	private String TABLA;
	
	private String ENTIDAD;
	
	private String IDIOMA;
	
	private String CLAVE;
	
	private String DATOS1;
	
	private String DATOS2;
	
	private String DATOS3;

	public String getDC_FORMATO() {
		return DC_FORMATO;
	}

	public void setDC_FORMATO(String dC_FORMATO) {
		DC_FORMATO = dC_FORMATO;
	}

	public String getTABLA() {
		return TABLA;
	}

	public void setTABLA(String tABLA) {
		TABLA = tABLA;
	}

	public String getENTIDAD() {
		return ENTIDAD;
	}

	public void setENTIDAD(String eNTIDAD) {
		ENTIDAD = eNTIDAD;
	}

	public String getIDIOMA() {
		return IDIOMA;
	}

	public void setIDIOMA(String iDIOMA) {
		IDIOMA = iDIOMA;
	}

	public String getCLAVE() {
		return CLAVE;
	}

	public void setCLAVE(String cLAVE) {
		CLAVE = cLAVE;
	}

	public String getDATOS1() {
		return DATOS1;
	}

	public void setDATOS1(String dATOS1) {
		DATOS1 = dATOS1;
	}

	public String getDATOS2() {
		return DATOS2;
	}

	public void setDATOS2(String dATOS2) {
		DATOS2 = dATOS2;
	}

	public String getDATOS3() {
		return DATOS3;
	}

	public void setDATOS3(String dATOS3) {
		DATOS3 = dATOS3;
	}

}
