package com.altec.bsbr.app.hyb.dto;

public class ObjRsOut {
	private String CD_MANI;
	
	public ObjRsOut() {}

	public ObjRsOut(String cD_MANI) {
		super();
		CD_MANI = cD_MANI;
	}

	public String getCD_MANI() {
		return CD_MANI;
	}

	public void setCD_MANI(String cD_MANI) {
		CD_MANI = cD_MANI;
	}
	
}
