package com.altec.bsbr.app.hyb.dto;

public class ObjRsOSExc {
	private String CODIGO;		
	private String SITUACAO;
	private String MOT_REC_EXCL;
	private String CD_NOTI;
	
	public ObjRsOSExc() {}
	
	public String getCODIGO() {
		return CODIGO;
	}
	public void setCODIGO(String CODIGO) {
		this.CODIGO = CODIGO;
	}
	public String getSITUACAO() {
		return SITUACAO;
	}
	public void setSITUACAO(String SITUACAO) {
		this.SITUACAO = SITUACAO;
	}
	public String getMOT_REC_EXCL() {
		return MOT_REC_EXCL;
	}
	public void setMOT_REC_EXCL(String MOT_REC_EXCL) {
		this.MOT_REC_EXCL = MOT_REC_EXCL;
	}
	public String getCD_NOTI() {
		return CD_NOTI;
	}
	public void setCD_NOTI(String CD_NOTI) {
		this.CD_NOTI = CD_NOTI;
	}
	
}
