package com.altec.bsbr.app.hyb.dto;

import java.text.NumberFormat;
import java.util.Locale;

public class CadastroosDetalheprodutofe {
	
	private String strNrSeqTranFrau;
	private String strErro;
	private String NR_SEQU_ORDE_SERV;
	private String DESC_TRAN;
	private String NM_LOCA_TRAN;
	private String CD_BARR;
	private String TX_DADO_COMP;
	private String NM_CLIE_FAVD;
	private String NR_CPF_CNPJ_CLIE_FAVD;
	private String NR_CNTA_CLIE_FAVD;
	private String NR_RENAVAM;
	private String CD_VEIC;
	private String NM_COR_VEIC;
	private String NR_ANO_VEIC;
	private String NR_PLAC_VEIC;
	private String NR_CHAS_VEIC;
	private String NR_TITU;
	private String NM_BANC_CRED;
	private String NM_CEDE_TITU;
	private String NM_DEVE;
	private String NR_CPF_CNPJ_DEVE;
	private String NR_CELU;
	private String VL_TRAN;
	private String TP_CNTA_CLIE_FAVD;
	
	private Locale ptBR = new Locale("pt", "BR");
	
	public CadastroosDetalheprodutofe() {
	}
	
	

	public CadastroosDetalheprodutofe(String nR_SEQU_ORDE_SERV,
			String dESC_TRAN, String nM_LOCA_TRAN, String cD_BARR, String tX_DADO_COMP, String nM_CLIE_FAVD,
			String nR_CPF_CNPJ_CLIE_FAVD, String nR_CNTA_CLIE_FAVD, String nR_RENAVAM, String cD_VEIC,
			String nM_COR_VEIC, String nR_ANO_VEIC, String nR_PLAC_VEIC, String nR_CHAS_VEIC, String nR_TITU,
			String nM_BANC_CRED, String nM_CEDE_TITU, String nM_DEVE, String nR_CPF_CNPJ_DEVE, String nR_CELU,
			String vL_TRAN, String tP_CNTA_CLIE_FAVD) {
		super();
		NR_SEQU_ORDE_SERV = nR_SEQU_ORDE_SERV;
		DESC_TRAN = dESC_TRAN;
		NM_LOCA_TRAN = nM_LOCA_TRAN;
		CD_BARR = cD_BARR;
		TX_DADO_COMP = tX_DADO_COMP;
		NM_CLIE_FAVD = nM_CLIE_FAVD;
		NR_CPF_CNPJ_CLIE_FAVD = nR_CPF_CNPJ_CLIE_FAVD;
		NR_CNTA_CLIE_FAVD = nR_CNTA_CLIE_FAVD;
		NR_RENAVAM = nR_RENAVAM;
		CD_VEIC = cD_VEIC;
		NM_COR_VEIC = nM_COR_VEIC;
		NR_ANO_VEIC = nR_ANO_VEIC;
		NR_PLAC_VEIC = nR_PLAC_VEIC;
		NR_CHAS_VEIC = nR_CHAS_VEIC;
		NR_TITU = nR_TITU;
		NM_BANC_CRED = nM_BANC_CRED;
		NM_CEDE_TITU = nM_CEDE_TITU;
		NM_DEVE = nM_DEVE;
		NR_CPF_CNPJ_DEVE = nR_CPF_CNPJ_DEVE;
		NR_CELU = nR_CELU;
		VL_TRAN = vL_TRAN;
		TP_CNTA_CLIE_FAVD = tP_CNTA_CLIE_FAVD;
	}

	public String getVlTranTransformado() {
		return this.VL_TRAN.isEmpty() ? 
				"" :
				NumberFormat.getCurrencyInstance(ptBR).format(Double.parseDouble(this.VL_TRAN.replace(",", "."))).replace("R$", "").trim();				 
	}

	public String getStrNrSeqTranFrau() {
		return strNrSeqTranFrau;
	}

	public void setStrNrSeqTranFrau(String strNrSeqTranFrau) {
		this.strNrSeqTranFrau = strNrSeqTranFrau;
	}

	public String getStrErro() {
		return strErro;
	}

	public void setStrErro(String strErro) {
		this.strErro = strErro;
	}

	public String getNR_SEQU_ORDE_SERV() {
		return NR_SEQU_ORDE_SERV;
	}

	public void setNR_SEQU_ORDE_SERV(String nR_SEQU_ORDE_SERV) {
		NR_SEQU_ORDE_SERV = nR_SEQU_ORDE_SERV;
	}

	public String getDESC_TRAN() {
		return DESC_TRAN;
	}

	public void setDESC_TRAN(String dESC_TRAN) {
		DESC_TRAN = dESC_TRAN;
	}

	public String getNM_LOCA_TRAN() {
		return NM_LOCA_TRAN;
	}

	public void setNM_LOCA_TRAN(String nM_LOCA_TRAN) {
		NM_LOCA_TRAN = nM_LOCA_TRAN;
	}

	public String getCD_BARR() {
		return CD_BARR;
	}

	public void setCD_BARR(String cD_BARR) {
		CD_BARR = cD_BARR;
	}

	public String getTX_DADO_COMP() {
		return TX_DADO_COMP;
	}

	public void setTX_DADO_COMP(String tX_DADO_COMP) {
		TX_DADO_COMP = tX_DADO_COMP;
	}

	public String getNM_CLIE_FAVD() {
		return NM_CLIE_FAVD;
	}

	public void setNM_CLIE_FAVD(String nM_CLIE_FAVD) {
		NM_CLIE_FAVD = nM_CLIE_FAVD;
	}

	public String getNR_CPF_CNPJ_CLIE_FAVD() {
		return NR_CPF_CNPJ_CLIE_FAVD;
	}

	public void setNR_CPF_CNPJ_CLIE_FAVD(String nR_CPF_CNPJ_CLIE_FAVD) {
		NR_CPF_CNPJ_CLIE_FAVD = nR_CPF_CNPJ_CLIE_FAVD;
	}

	public String getNR_CNTA_CLIE_FAVD() {
		return NR_CNTA_CLIE_FAVD;
	}

	public void setNR_CNTA_CLIE_FAVD(String nR_CNTA_CLIE_FAVD) {
		NR_CNTA_CLIE_FAVD = nR_CNTA_CLIE_FAVD;
	}

	public String getNR_RENAVAM() {
		return NR_RENAVAM;
	}

	public void setNR_RENAVAM(String nR_RENAVAM) {
		NR_RENAVAM = nR_RENAVAM;
	}

	public String getCD_VEIC() {
		return CD_VEIC;
	}

	public void setCD_VEIC(String cD_VEIC) {
		CD_VEIC = cD_VEIC;
	}

	public String getNM_COR_VEIC() {
		return NM_COR_VEIC;
	}

	public void setNM_COR_VEIC(String nM_COR_VEIC) {
		NM_COR_VEIC = nM_COR_VEIC;
	}

	public String getNR_ANO_VEIC() {
		return NR_ANO_VEIC;
	}

	public void setNR_ANO_VEIC(String nR_ANO_VEIC) {
		NR_ANO_VEIC = nR_ANO_VEIC;
	}

	public String getNR_PLAC_VEIC() {
		return NR_PLAC_VEIC;
	}

	public void setNR_PLAC_VEIC(String nR_PLAC_VEIC) {
		NR_PLAC_VEIC = nR_PLAC_VEIC;
	}

	public String getNR_CHAS_VEIC() {
		return NR_CHAS_VEIC;
	}

	public void setNR_CHAS_VEIC(String nR_CHAS_VEIC) {
		NR_CHAS_VEIC = nR_CHAS_VEIC;
	}

	public String getNR_TITU() {
		return NR_TITU;
	}

	public void setNR_TITU(String nR_TITU) {
		NR_TITU = nR_TITU;
	}

	public String getNM_BANC_CRED() {
		return NM_BANC_CRED;
	}

	public void setNM_BANC_CRED(String nM_BANC_CRED) {
		NM_BANC_CRED = nM_BANC_CRED;
	}

	public String getNM_CEDE_TITU() {
		return NM_CEDE_TITU;
	}

	public void setNM_CEDE_TITU(String nM_CEDE_TITU) {
		NM_CEDE_TITU = nM_CEDE_TITU;
	}

	public String getNM_DEVE() {
		return NM_DEVE;
	}

	public void setNM_DEVE(String nM_DEVE) {
		NM_DEVE = nM_DEVE;
	}

	public String getNR_CPF_CNPJ_DEVE() {
		return NR_CPF_CNPJ_DEVE;
	}

	public void setNR_CPF_CNPJ_DEVE(String nR_CPF_CNPJ_DEVE) {
		NR_CPF_CNPJ_DEVE = nR_CPF_CNPJ_DEVE;
	}

	public String getNR_CELU() {
		return NR_CELU;
	}

	public void setNR_CELU(String nR_CELU) {
		NR_CELU = nR_CELU;
	}

	public String getVL_TRAN() {
		return VL_TRAN;
	}

	public void setVL_TRAN(String vL_TRAN) {
		VL_TRAN = vL_TRAN;
	}

	public String getTP_CNTA_CLIE_FAVD() {
		return TP_CNTA_CLIE_FAVD;
	}

	public void setTP_CNTA_CLIE_FAVD(String tP_CNTA_CLIE_FAVD) {
		TP_CNTA_CLIE_FAVD = tP_CNTA_CLIE_FAVD;
	}
	
	
	
	
	
	
}
