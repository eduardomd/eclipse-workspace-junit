package com.altec.bsbr.app.hyb.dto;

public class LinhaNego2Model {
	
	private String codigoLinhaNego2;
	private String nomeLinhaNego2;
	
	public LinhaNego2Model(String codigoLinhaNego2, String nomeLinhaNego2) {
		this.codigoLinhaNego2 = codigoLinhaNego2;
		this.nomeLinhaNego2 = nomeLinhaNego2; 
	}

	public String getCodigoLinhaNego2() {
		return codigoLinhaNego2;
	}

	public void setCodigoLinhaNego2(String codigoLinhaNego2) {
		this.codigoLinhaNego2 = codigoLinhaNego2;
	}

	public String getNomeLinhaNego2() {
		return nomeLinhaNego2;
	}

	public void setNomeLinhaNego2(String nomeLinhaNego2) {
		this.nomeLinhaNego2 = nomeLinhaNego2;
	}		
}
 