package com.altec.bsbr.app.hyb.dto;

public class ObjRsOsAssociada {
	private String ORDEM;
	private String AREA;
	private String CD_STATUS;
	private String STATUS;
	
	public ObjRsOsAssociada() {}
	
	public ObjRsOsAssociada(String oRDEM, String aREA, String gOFE, String cD_STATUS, String sTATUS) {
		super();
		ORDEM = oRDEM;
		AREA = aREA;
		CD_STATUS = cD_STATUS;
		STATUS = sTATUS;
	}
	
	public String getORDEM() {
		return ORDEM;
	}
	
	public void setORDEM(String oRDEM) {
		ORDEM = oRDEM;
	}
	
	public String getAREA() {
		return AREA;
	}
	
	public void setAREA(String aREA) {
		AREA = aREA;
	}

	public String getCD_STATUS() {
		return CD_STATUS;
	}
	
	public void setCD_STATUS(String cD_STATUS) {
		CD_STATUS = cD_STATUS;
	}
	
	public String getSTATUS() {
		return STATUS;
	}
	
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	
}
