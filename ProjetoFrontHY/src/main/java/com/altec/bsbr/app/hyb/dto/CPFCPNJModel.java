package com.altec.bsbr.app.hyb.dto;

public class CPFCPNJModel {

	private double numeroCPFCPNJ;
	
	
	public CPFCPNJModel(double numeroCPFCPNJ) {
		
		this.numeroCPFCPNJ = numeroCPFCPNJ;	
		
	}


	public double getNumeroCPFCPNJ() {
		return numeroCPFCPNJ;
	}


	public void setNumeroCPFCPNJ(double numeroCPFCPNJ) {
		this.numeroCPFCPNJ = numeroCPFCPNJ;
	}
	
	
	
}
