package com.altec.bsbr.app.hyb.web.jsf;

import java.io.Serializable;
import java.io.StringReader;
import java.util.Iterator;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.collections.set.SynchronizedSet;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.altec.bsbr.app.hyb.dto.CadastroOsDetalheEnvolvFunc;
import com.altec.bsbr.app.hyb.dto.CadastroPesquisaContaCD;

@Component("PesquisaContaCD")
@Scope("session")
public class CadastroosPesquisaContaCDBean extends BasicBBean {

	private static final long serialVersionUID = 1L;
	
	private CadastroPesquisaContaCD cadastroPesquisaContaCD;
	private String srcIFrame;
	
	public CadastroosPesquisaContaCDBean() {
		
		this.cadastroPesquisaContaCD = new CadastroPesquisaContaCD();
		
		FacesContext fc = FacesContext.getCurrentInstance();
        @SuppressWarnings("unchecked")
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
 
        String produto = params.get("produto");
		srcIFrame = "hyb_cadastroos_pesquisacontacd.xhtml?produto=" + produto;
        
/*		FacesContext fc = FacesContext.getCurrentInstance();
        @SuppressWarnings("unchecked")
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
 
        int intProduto = Integer.parseInt(params.get("produto"));
		
		if (intProduto > -1) { 
			GravaRegistro(intProduto);
		}*/
		
		
	}
	
	private void GravaRegistro(int intProduto) {
		// Response.Write(Request.Form("hdnTitular" + iRegistro))
	}
	
	public String FillTblCartPart() {
		
		String strRetorno = "";
		String strXml;
		strXml ="<?xml version=\"1.0\" encoding=\"UTF-8\"?> \r\n" +
				"<DADOS>\r\n" + 
				"<REGISTRO>\r\n" + 
				"<CPFCNPJ>12312312312</CPFCNPJ>\r\n" + 
				"<NRCARTAO>111222</NRCARTAO>\r\n" + 
				"<TITULAR>Titular 001</TITULAR>\r\n" + 
				"</REGISTRO>\r\n" + 
				"<REGISTRO>\r\n" + 
				"<CPFCNPJ>99999999999</CPFCNPJ>\r\n" + 
				"<NRCARTAO>000000</NRCARTAO>\r\n" + 
				"<TITULAR>Titular 002</TITULAR>\r\n" + 
				"</REGISTRO>\r\n" + 
				"</DADOS>";
		
		
		Document objDom = null;
		
		
		try {
			SAXReader reader = new SAXReader();
			objDom = reader.read(new StringReader(strXml));
		}
		catch ( Exception e) {
			e.printStackTrace();
		}	
	
		//System.out.println(objDom.asXML());
		
		
		for (Element e : getIterable(objDom,"REGISTRO")) {  
			System.out.println(e.getName() + ": " + e.getText());  
		}
		

		
/*		objDom.selectNodes("//DADOS/REGISTRO");
		
		System.out.println(objDom.asXML());

		for (int iCont=0; iCont < objDom.nodeCount(); iCont++) {
	        strRetorno = strRetorno + "<tr>";
            
            strRetorno = strRetorno + "<td>" + objDom.getRootElement().node(iCont).selectNodes("NRCARTAO")  + "</td>";
            strRetorno = strRetorno + "<input type='hidden' id='hdnCartao'" + iCont + "' name='hdnCartao'" + iCont + "' value='" + objDom.getRootElement().node(iCont).selectNodes("NRCARTAO")  + "' />";
            
            strRetorno = strRetorno + "<td>" + objDom.getRootElement().node(iCont).selectNodes("TITULAR") + "</td>";
            strRetorno = strRetorno + "<input type='hidden' id='hdnTitular" + iCont + " name='hdnTitular" + iCont + " value='" + objDom.getRootElement().node(iCont).selectNodes("TITULAR") + " />";
            
            strRetorno = strRetorno + "<td>" + objDom.getRootElement().node(iCont).selectNodes("CPFCNPJ") + "</td>";
            strRetorno = strRetorno + "<input type='hidden' id='hdnCfpCnpj" + iCont + " name='hdnCfpCnpj" + iCont + " value='" + objDom.getRootElement().node(iCont).selectNodes("CPFCNPJ") + " />";
            
            strRetorno = strRetorno + "<td align='right'><input type='button' onclick='Adicionar(" + iCont + ")' value='+' style='width:20px; text-align:center'/></td>";
            strRetorno = strRetorno + "</tr>";
		};*/
		

		return strRetorno;
	}
	
	private static Iterable<Element> getIterable(final Document document, final String element) {
		return new Iterable<Element>() {  
			@Override @SuppressWarnings("unchecked")  
			public Iterator<Element> iterator() {  
				return document.getRootElement().element(element).elementIterator();
			}  
		};
	}
	
	public String FillTblContaPart() {
		
		String strRetorno = "";
		String strXml;
		strXml ="<?xml version=\"1.0\" encoding=\"UTF-8\"?> \r\n" +
				"<DADOS>\r\n" + 
				"<REGISTRO>\r\n" + 
				"<CPFCNPJ>1112222</CPFCNPJ>\r\n" + 
				"<NRCONTA>111222</NRCONTA>\r\n" + 
				"<TIPO>F�sica</TIPO>\r\n" + 
				"<TITULAR>Ana Paula</TITULAR>\r\n" + 
				"</REGISTRO>\r\n" + 
				"</DADOS>";
		
		Document objDom = null;
			
		try {
			SAXReader reader = new SAXReader();
			objDom = reader.read(new StringReader(strXml));
		}
		catch ( Exception e) {
			e.printStackTrace();
		}	
		
/*		objDom.selectNodes("//DADOS/REGISTRO");
		
		for (int iCont=0; iCont < objDom.nodeCount(); iCont++) {
			strRetorno = strRetorno + "<tr>";
	        strRetorno = strRetorno + "<td>" + objDom.getRootElement().node(iCont).selectNodes("NRCONTA") +"</td>";
            strRetorno = strRetorno + "<td>" + objDom.getRootElement().node(iCont).selectNodes("TIPO") +"</td>";
            strRetorno = strRetorno + "<td>" + objDom.getRootElement().node(iCont).selectNodes("TITULAR") +"</td>";
            strRetorno = strRetorno + "<td>" + objDom.getRootElement().node(iCont).selectNodes("CPFCNPJ") +"</td>";
            strRetorno = strRetorno + "<td align='right'><input type='button' value='+' style='width:20px; text-align:center'/></td>";
            strRetorno = strRetorno + "</tr>";
		}*/
		
		return strRetorno;
	}

	public CadastroPesquisaContaCD getCadastroPesquisaContaCD() {
		return cadastroPesquisaContaCD;
	}

	public void setCadastroPesquisaContaCD(CadastroPesquisaContaCD cadastroPesquisaContaCD) {
		this.cadastroPesquisaContaCD = cadastroPesquisaContaCD;
	}

	public String getSrcIFrame() {
		return srcIFrame;
	}

	public void setSrcIFrame(String srcIFrame) {
		this.srcIFrame = srcIFrame;
	}
	
}
