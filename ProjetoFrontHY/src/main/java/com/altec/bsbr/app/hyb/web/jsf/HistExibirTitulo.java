package com.altec.bsbr.app.hyb.web.jsf;

import javax.faces.context.FacesContext;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.fw.web.jsf.BasicBBean;

@Component("HistExibirTitulo")
@Scope("request")
public class HistExibirTitulo extends BasicBBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Object dsTitu = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("DsTitu");

	public Object getDsTitu() {
		return dsTitu;
	}

	public void setDsTitu(Object dsTitu) {
		this.dsTitu = dsTitu;
	}

}
