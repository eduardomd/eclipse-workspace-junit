package com.altec.bsbr.app.hyb.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;

public class Hyb_uploadprocessa_model {
	
	private String strCaminhoBasico;
	private String strPath;
	private String strCaminhoMudado;
	private String strNomeArquivo;
	private Object strTpAnexo = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("qstrTpAnexo");
	private Object strNrSeqOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("qstrSeqOs");
	private Object strSeqTrei = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("qstrSeqTrei");
	
	// MOCK
	private int intArquivos;
	private List<String> files;
	
	public Hyb_uploadprocessa_model(String strTpAnexo, String strCaminhoBasico, String strPath, String strCaminhoMudado,
			String strNomeArquivo, int intArquivos, Map<String, Map<String, String>> objUpload) {
		this.strTpAnexo = strTpAnexo;
		this.strCaminhoBasico = strCaminhoBasico;
		this.strPath = strPath;
		this.strCaminhoMudado = strCaminhoMudado;
		this.strNomeArquivo = strNomeArquivo;
		this.intArquivos = intArquivos;
	}
	
	public Hyb_uploadprocessa_model() {
		this.files = new ArrayList<String>();
//		this.setStrTpAnexo("I");
		this.setStrNrSeqOs("test");
		this.setStrSeqTrei("test2");
	}
	
	public Object getStrNrSeqOs() {
		return strNrSeqOs;
	}

	public void setStrNrSeqOs(Object strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}

	public Object getStrSeqTrei() {
		return strSeqTrei;
	}

	public void setStrSeqTrei(Object strSeqTrei) {
		this.strSeqTrei = strSeqTrei;
	}

	public Object getStrTpAnexo() {
		return strTpAnexo;
	}
	public void setStrTpAnexo(Object strTpAnexo) {
		this.strTpAnexo = strTpAnexo;
	}
	public String getStrCaminhoBasico() {
		return strCaminhoBasico;
	}
	public void setStrCaminhoBasico(String strCaminhoBasico) {
		this.strCaminhoBasico = strCaminhoBasico;
	}
	public String getStrPath() {
		return strPath;
	}
	public void setStrPath(String strPath) {
		this.strPath = strPath;
	}
	public String getStrCaminhoMudado() {
		return strCaminhoMudado;
	}
	public void setStrCaminhoMudado(String strCaminhoMudado) {
		this.strCaminhoMudado = strCaminhoMudado;
	}
	public String getStrNomeArquivo() {
		return strNomeArquivo;
	}
	public void setStrNomeArquivo(String strNomeArquivo) {
		this.strNomeArquivo = strNomeArquivo;
	}
	public int getIntArquivos() {
		return intArquivos;
	}
	public void setIntArquivos(int intArquivos) {
		this.intArquivos = intArquivos;
	}

	public List<String> getFiles() {
		return files;
	}

	public void setFiles(List<String> files) {
		this.files = files;
	}
	
}
