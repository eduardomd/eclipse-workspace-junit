package com.altec.bsbr.app.hyb.dto;


public class RsAdmLog {
	
	private String DT_ACAO;
	private String DT_HORA;
	private String COD_USUARIO;
	private String NM_ACAO;
	private String NR_OS;
	
	public String getDT_ACAO() {
		return DT_ACAO;
	}
	public void setDT_ACAO(String dT_ACAO) {
		DT_ACAO = dT_ACAO;
	}
	public String getDT_HORA() {
		return DT_HORA;
	}
	public void setDT_HORA(String dT_HORA) {
		DT_HORA = dT_HORA;
	}
	public String getCOD_USUARIO() { 
		return COD_USUARIO;
	}
	public void setCOD_USUARIO(String cOD_USUARIO) {
		COD_USUARIO = cOD_USUARIO;
	}
	public String getNM_ACAO() {
		return NM_ACAO;
	}
	public void setNM_ACAO(String nM_ACAO) {
		NM_ACAO = nM_ACAO;
	}
	public String getNR_OS() {
		return NR_OS;
	}
	public void setNR_OS(String nR_OS) {
		NR_OS = nR_OS;
	}
	
	
	
}
