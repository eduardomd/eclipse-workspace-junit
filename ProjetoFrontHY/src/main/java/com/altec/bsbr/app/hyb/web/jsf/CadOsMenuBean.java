package com.altec.bsbr.app.hyb.web.jsf;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("cadOsMenuBean")
@Scope("request")
public class CadOsMenuBean {

	private String strNrSeqOs;
	private String strReadOnly;
	private String strTitulo;
	private String strPagina;
	
    @PostConstruct
    private void init() {
    	System.out.println("init cados menu aqui!");
    	if(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs") != null)
    		strNrSeqOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs").toString();
    	else if(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("frmParticipante:modalEquipe:nros") != null)
    		strNrSeqOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("frmParticipante:modalEquipe:nros").toString();
    		
    		
    	if(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strReadOnly") != null)
    		strReadOnly = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strReadOnly").toString();
    		
    	if(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strTitulo") != null)
    		strTitulo = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strTitulo").toString();
    		
    	if(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strPagina") != null)
    		strPagina = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strPagina").toString();
    	
    	System.out.println(strNrSeqOs +"--"+ strReadOnly +"--"+ strTitulo +"--"+ strPagina);
    	cleanSession();
    }
    
    
    public void cleanSession() {
          System.out.println("\n --CLEAN SESSION! --");
          FacesContext context = FacesContext.getCurrentInstance();
          context.getExternalContext().getSessionMap().remove("cadosUnidadeEnvolvidaBean");
          context.getExternalContext().getSessionMap().remove("cadOsDatasBean");
          context.getExternalContext().getSessionMap().remove("cadOsAcoesComplBean");
  		  context.getExternalContext().getSessionMap().remove("cadosEquipatribLanchr");
  		context.getExternalContext().getSessionMap().remove("cadOsRiscoPotencialBean");
	  		System.out.println("\n --LIMPA SESSAO //z\\ -----");
	  		context.getExternalContext().getSessionMap().remove("cadosParticipacaoProcessoBeanBean");
	  		context.getExternalContext().getSessionMap().remove("cadosInformacoesGeraisBean");

	  		/*context.getExternalContext().getSessionMap().remove("CadastroosDetalheEnvolvFuncBean");
	  		context.getExternalContext().getSessionMap().remove("cadastroOsDetalheEnvolvClieBean");
	  		context.getExternalContext().getSessionMap().remove("CadastroOsDetalheEnvolvNaoClienteBean");
	  		context.getExternalContext().getSessionMap().remove("cadastroosPesquisaEquipeBean");
	  		context.getExternalContext().getSessionMap().remove("cadastroosPesquisaClienteBean");
	  		context.getExternalContext().getSessionMap().remove("cadastroosPesquisaNaoClienteBean");*/
	  		
    }

	
	public String getStrNrSeqOs() {
		return strNrSeqOs;
	}
	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}
	public String getStrReadOnly() {
		return strReadOnly;
	}
	public void setStrReadOnly(String strReadOnly) {
		this.strReadOnly = strReadOnly;
	}
	public String getStrTitulo() {
		return strTitulo;
	}
	public void setStrTitulo(String strTitulo) {
		this.strTitulo = strTitulo;
	}
	public String getStrPagina() {
		return strPagina;
	}
	public void setStrPagina(String strPagina) {
		this.strPagina = strPagina;
	}
	
	public void atualizaParams() {
		
	}
	
}
