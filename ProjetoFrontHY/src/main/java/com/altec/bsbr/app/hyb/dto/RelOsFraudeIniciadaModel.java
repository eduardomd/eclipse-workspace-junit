package com.altec.bsbr.app.hyb.dto;

public class RelOsFraudeIniciadaModel {
	private String EVENTO;
	private String CD_CNAL;
	private String BANCO;
	private String QUANTIDADE;
	private String VL_PREJ;
	private String VALOR;
	private String CANAL;
	private String FORMATADO_VALOR;
	private String FORMATADO_VL_PREJ;
	
	public RelOsFraudeIniciadaModel(
			String EVENTO,
			String CD_CNAL,
			String BANCO,
			String QUANTIDADE,
			String VL_PREJ,
			String VALOR,
			String CANAL,
			String FORMATADO_VALOR,
			String FORMATADO_VL_PREJ) {
		this.EVENTO = EVENTO;
		this.CD_CNAL = CD_CNAL;
		this.BANCO = BANCO;
		this.QUANTIDADE = QUANTIDADE;
		this.VL_PREJ = VL_PREJ;
		this.VALOR = VALOR;
		this.CANAL = CANAL;
		this.FORMATADO_VALOR = FORMATADO_VALOR;
		this.FORMATADO_VL_PREJ = FORMATADO_VL_PREJ;
	}
	
	public String getEVENTO() {
		return EVENTO;
	}
	public void setEVENTO(String eVENTO) {
		EVENTO = eVENTO;
	}
	public String getCD_CNAL() {
		return CD_CNAL;
	}
	public void setCD_CNAL(String cD_CNAL) {
		CD_CNAL = cD_CNAL;
	}
	public String getBANCO() {
		return BANCO;
	}
	public void setBANCO(String bANCO) {
		BANCO = bANCO;
	}
	public String getQUANTIDADE() {
		return QUANTIDADE;
	}
	public void setQUANTIDADE(String qUANTIDADE) {
		QUANTIDADE = qUANTIDADE;
	}
	public String getVL_PREJ() {
		return VL_PREJ;
	}
	public void setVL_PREJ(String vL_PREJ) {
		VL_PREJ = vL_PREJ;
	}
	public String getVALOR() {
		return VALOR;
	}
	public void setVALOR(String vALOR) {
		VALOR = vALOR;
	}
	public String getCANAL() {
		return CANAL;
	}
	public void setCANAL(String cANAL) {
		CANAL = cANAL;
	}

	public String getFORMATADO_VALOR() {
		return FORMATADO_VALOR;
	}

	public void setFORMATADO_VALOR(String fORMATADO_VALOR) {
		FORMATADO_VALOR = fORMATADO_VALOR;
	}

	public String getFORMATADO_VL_PREJ() {
		return FORMATADO_VL_PREJ;
	}

	public void setFORMATADO_VL_PREJ(String fORMATADO_VL_PREJ) {
		FORMATADO_VL_PREJ = fORMATADO_VL_PREJ;
	}
}
