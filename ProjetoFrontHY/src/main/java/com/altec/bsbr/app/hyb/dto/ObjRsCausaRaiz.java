package com.altec.bsbr.app.hyb.dto;

public class ObjRsCausaRaiz {
	private String CODIGO;
	private String DESCRICAO;
	private String INCLUSAO;
	
	public ObjRsCausaRaiz() {}
	
	public ObjRsCausaRaiz(String cODIGO, String dESCRICAO, String iNCLUSAO) {
		super();
		CODIGO = cODIGO;
		DESCRICAO = dESCRICAO;
		INCLUSAO = iNCLUSAO;
	}

	public String getCODIGO() {
		return CODIGO;
	}
	public void setCODIGO(String cODIGO) {
		CODIGO = cODIGO;
	}
	public String getDESCRICAO() {
		return DESCRICAO;
	}
	public void setDESCRICAO(String dESCRICAO) {
		DESCRICAO = dESCRICAO;
	}

	public String getINCLUSAO() {
		return INCLUSAO;
	}

	public void setINCLUSAO(String iNCLUSAO) {
		INCLUSAO = iNCLUSAO;
	}
	
	
}
