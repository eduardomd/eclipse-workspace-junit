package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.CadosEquipatribLanchr;
import com.altec.bsbr.app.hyb.dto.EquipAtrib;
import com.altec.bsbr.app.hyb.dto.UsuarioIncModel;
import com.altec.bsbr.app.hyb.web.util.LogIncUtil;
import com.altec.bsbr.app.hyb.web.util.XHYUsuarioIncService;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmLog.XHYAdmLogEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsEquipAtrib.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsEquipAtrib.XHYCadOsEquipAtribEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYUsuario.XHYUsuarioEndPoint;
import com.altec.bsbr.fw.security.SecurityInfo;
import com.altec.bsbr.fw.security.authorization.Authorization;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component("CadOsEquipatribBean")
@Scope("session")
public class CadOsEquipatribBean extends BasicBBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	private XHYCadOsEquipAtribEndPoint objCadOs;

	@Autowired
	private XHYAdmLogEndPoint log;
	
	@Autowired
	private XHYUsuarioIncService usuarioService;

	private UsuarioIncModel usuario;
	
	private String strMatricula;
	private String strNome;
	private String strCargo;
	private String strDesignacao;
	private String strCpf;
	private String strCdArea;
	private String txtHdAction;
	private List<EquipAtrib> objRs;
	private List<EquipAtrib> objRsPesquisados;
	private List<EquipAtrib> objRsAdicionados;
	private EquipAtrib objEscolhido;

	private CadosEquipatribLanchr cadosEquipatribLanchr;
	private String strHoras;
	private String strVlGasto;
	

	// Requisições não aceitas no init
	private String strNrSeqOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
			.get("strNrSeqOs").toString();

	private String strDelCpf;

	@PostConstruct
	public void init() {
		System.out.println("r35432532 oioioioioitchau !!!!@@");
		objRs = new ArrayList<EquipAtrib>();
		objRsPesquisados = new ArrayList<EquipAtrib>();
		objRsAdicionados = new ArrayList<EquipAtrib>();
		strCpf = "";
		strNome = "";
		strCdArea = "";
		cadosEquipatribLanchr = new CadosEquipatribLanchr();
		instanciarUsuario();
		consultar();

	}
	public void instanciarUsuario() {
		try {
			usuario = usuarioService.getDadosUsuario();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

	public void deletar() {
		String retorno = "";
		String cpf = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("Cpf").toString();
		try {
			retorno = objCadOs.deletarRecursoEquipe(strNrSeqOs, cpf);
			consultar();
			log.incluirAcaoLog(strNrSeqOs, Integer.toString(LogIncUtil.DELETAR_EQUIPE_ATRIBUIDA_OS), usuario.getCpfUsuario(),  "CPF membro excluído: " + cpf);
			//log.in
			// InsereLog DELETAR_EQUIPE_ATRIBUIDA_OS, CPF_USUARIO, strNrSeqOs, "CPF membro
			// excluído: " & strDelCpf
		 } catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	public void consultar() {
		String retorno = "";
		objRs.clear();
		try {
			retorno = objCadOs.consultarEquipeAtribuidaOs(strNrSeqOs);
			retorno = retorno.replaceAll("null", "\"\"");
			JSONObject jsonRetorno = new JSONObject(retorno);
			JSONArray pcursor = jsonRetorno.getJSONArray("PCURSOR");

			for (int i = 0; i < pcursor.length(); i++) {
				JSONObject curr = pcursor.getJSONObject(i);
				EquipAtrib unidade = new EquipAtrib();

				unidade.setStrCargo(curr.getString("CARGO"));
				unidade.setStrNome(curr.getString("NOME"));
				unidade.setStrMatricula(curr.getString("MATRICULA"));
				unidade.setStrDesignacao(curr.getString("DESIGNACAO"));
				unidade.setStrCpf(curr.getString("CPF"));
				unidade.setStrNmEmailRecu(curr.getString("NM_EMAIL_RECU"));
				unidade.setStrNmDesignacao(curr.getString("NM_DESIGNACAO"));

				this.objRs.add(unidade);
			}

		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

	}

	public void salvar() {
		
		
		for (int i = 0; i < objRs.size(); i++) {
			String retorno = "";
			try {
				retorno = objCadOs.alterarDesignacao(strNrSeqOs, objRs.get(i).getStrCpf(),
						objRs.get(i).getStrDesignacao());

				

			} catch (Exception e) {
				try {
					FacesContext.getCurrentInstance().getExternalContext()
							.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
		try {
			log.incluirAcaoLog(strNrSeqOs, Integer.toString(LogIncUtil.SALVAR_EQUIPE_ATRIBUIDA_OS), usuario.getCpfUsuario(),  "");
		}catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		RequestContext.getCurrentInstance().execute("alert('Dados alterados com sucesso!');");
	}

	public void adicionar() {
		if (objRsAdicionados.size() > 0) {
			try {

				for (int i = 0; i < objRsAdicionados.size(); i++) {
					String retorno = "";
					retorno = objCadOs.adicionarRecurso(strNrSeqOs, objRsAdicionados.get(i).getStrCpf());

				}
				RequestContext.getCurrentInstance().execute("alert('Recurso(s) atribuído(s) com sucesso!');");
				this.limparPesquisados();
				consultarRecurso();
				consultar();
			} catch (Exception e) {
				try {
					FacesContext.getCurrentInstance().getExternalContext()
							.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		} else {
			RequestContext.getCurrentInstance().execute("alert('Selecione um recurso!');");
		}
		
	}

	public void consultarRecurso() {
		String retorno = "";
		this.objRsPesquisados.clear();

		try {
			retorno = objCadOs.consultarRecurso(strNrSeqOs, strCpf, strMatricula, strNome, usuario.getCdAreaUsuario());
			retorno = retorno.replaceAll("null", "\"\"");
			JSONObject jsonRetorno = new JSONObject(retorno);
			JSONArray pcursor = jsonRetorno.getJSONArray("PCURSOR");

			for (int i = 0; i < pcursor.length(); i++) {
				JSONObject curr = pcursor.getJSONObject(i);
				EquipAtrib unidade = new EquipAtrib();

				unidade.setStrCargo(curr.getString("CARGO"));
				unidade.setStrNome(curr.getString("NOME"));
				unidade.setStrMatricula(curr.getString("MATRICULA"));
				unidade.setStrCpf(curr.getString("CPF"));

				this.objRsPesquisados.add(unidade);
			}
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

	}

	public void salvarHoras() {
		String retorno = "";
		//Se os dois forem vazios return
	
		if((strHoras.isEmpty() || strHoras == null) && (strVlGasto.isEmpty() || strVlGasto == null) ) {
			RequestContext.getCurrentInstance().execute("alert('Preencha o valor de pelo menos um campo!');");
			return;
		}
		if((strHoras.isEmpty() || strHoras == null) && Integer.valueOf(strVlGasto) == 0) {
			RequestContext.getCurrentInstance().execute("alert('Preencha o valor de pelo menos um campo!');");
			return;
		}	
		if((strVlGasto.isEmpty() || strVlGasto == null) && Integer.valueOf(strHoras) == 0) {
			RequestContext.getCurrentInstance().execute("alert('Preencha o valor de pelo menos um campo!');");
			return;
		}
		
		if((strHoras.isEmpty() || strHoras == null) && !(strVlGasto.isEmpty() || strVlGasto == null)) 
	    {
	        strHoras = "0";
	    }
	    
		if(!(strHoras.isEmpty() || strHoras == null) && (strVlGasto.isEmpty() || strVlGasto == null)) 
	    {
			strVlGasto = "0";
	    }
		if(Integer.valueOf(strHoras) == 0 && Integer.valueOf(strVlGasto) == 0) {
			RequestContext.getCurrentInstance().execute("alert('Preencha o valor de pelo menos um campo!');");
			return;
		}
		if (Double.valueOf(strHoras) < 0.0 ) {
	        if(Double.valueOf(cadosEquipatribLanchr.getHORAS()) < Math.abs(Double.valueOf(strHoras)))
	        {
	        	RequestContext.getCurrentInstance().execute("alert('Valor de Horas a ser lançado é inválido. Valor maior que o total!');");
	        	return;
	        }
		}
	   
	    if (Double.valueOf(strVlGasto) < 0.0 )
	        if (Double.valueOf(cadosEquipatribLanchr.getGASTOS_ADICIONAIS()) < Math.abs(Double.valueOf(strVlGasto)))
	        {
	        	RequestContext.getCurrentInstance().execute("alert('Valor de Gastos Adicionais a ser lançado é inválido. Valor maior que o total!');");
	        	return;
	        }
		
		

		try {
			retorno = objCadOs.lancarHoras(strNrSeqOs,  this.strCpf, Double.valueOf(strHoras), Double.valueOf(strVlGasto));
			RequestContext.getCurrentInstance().execute("alert('Horas e valores lançados com sucesso!');");
			cadosEquipatribLanchr.setHORAS("");
			cadosEquipatribLanchr.setGASTOS_ADICIONAIS("");
			FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_cados_equipatrib_lanchr_iframe.xhtml?strNrSeqOs="+strNrSeqOs+"&strCpf="+strCpf+"&strNome="+strNome);
			log.incluirAcaoLog(strNrSeqOs, Integer.toString(LogIncUtil.LANCAR_HORAS_TRABALHADAS_OS), usuario.getCpfUsuario(),  "");

		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		limparCampo();
		consultarHoras(strCpf);

	}

	public void consultarHoras(String cpf) {
		String retorno ="";
		if(cpf == null || cpf.isEmpty()) {
			this.strCpf = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("Cpf").toString();
			this.strNome = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("Nome").toString();
			

		} else {
			strCpf = cpf;
		}
		
		try {
			retorno = objCadOs.consultarLancamentoHoras(this.strNrSeqOs, this.strCpf);
			retorno = retorno.replaceAll("null", "\"\"");
			JSONObject jsonRetorno = new JSONObject(retorno);
			JSONArray pcursor = jsonRetorno.getJSONArray("PCURSOR");

			cadosEquipatribLanchr.setHORAS(pcursor.getJSONObject(0).get("HORAS").toString());
			cadosEquipatribLanchr.setGASTOS_ADICIONAIS(pcursor.getJSONObject(0).get("GASTOS_ADICIONAIS").toString());

		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	public void limparPesquisados() {
		objRsPesquisados.clear();
		objRsAdicionados.clear();
	}
	public void limparPessoa() {
		strCpf = "";
		strNome = "";
		strMatricula = "";
	}
	public void limparCampo() {
		strHoras = "";
		strVlGasto = "";
	}
	

	public String getStrMatricula() {
		return strMatricula;
	}

	public String getStrNome() {
		return strNome;
	}

	public String getStrCargo() {
		return strCargo;
	}

	public String getStrDesignacao() {
		return strDesignacao;
	}

	public void setStrMatricula(String strMatricula) {
		this.strMatricula = strMatricula;
	}

	public void setStrNome(String strNome) {
		this.strNome = strNome;
	}

	public void setStrCargo(String strCargo) {
		this.strCargo = strCargo;
	}

	public void setStrDesignacao(String strDesignacao) {
		this.strDesignacao = strDesignacao;
	}

	public String getStrNrSeqOs() {
		return strNrSeqOs;
	}

	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}


	public String getStrDelCpf() {
		return strDelCpf;
	}

	public void setStrDelCpf(String strDelCpf) {
		this.strDelCpf = strDelCpf;
	}

	public List<EquipAtrib> getObjRs() {
		return objRs;
	}

	public void setObjRs(List<EquipAtrib> objRs) {
		this.objRs = objRs;
	}


	public String getStrCpf() {
		return strCpf;
	}

	public void setStrCpf(String strCpf) {
		this.strCpf = strCpf;
	}

	public String getStrCdArea() {
		return strCdArea;
	}

	public void setStrCdArea(String strCdArea) {
		this.strCdArea = strCdArea;
	}

	public List<EquipAtrib> getObjRsAdicionados() {
		return objRsAdicionados;
	}

	public void setObjRsAdicionados(List<EquipAtrib> objRsAdicionados) {
		this.objRsAdicionados = objRsAdicionados;
	}

	public List<EquipAtrib> getObjRsPesquisados() {
		return objRsPesquisados;
	}

	public void setObjRsPesquisados(List<EquipAtrib> objRsPesquisados) {
		this.objRsPesquisados = objRsPesquisados;
	}

	public String getTxtHdAction() {
		return txtHdAction;
	}

	public void setTxtHdAction(String txtHdAction) {
		this.txtHdAction = txtHdAction;
	}

	public String getStrHoras() {
		return strHoras;
	}

	public void setStrHoras(String strHoras) {
		this.strHoras = strHoras;
	}

	public String getStrVlGasto() {
		return strVlGasto;
	}

	public void setStrVlGasto(String strVlGasto) {
		this.strVlGasto = strVlGasto;
	}

	public CadosEquipatribLanchr getCadosEquipatribLanchr() {
		return cadosEquipatribLanchr;
	}

	public void setCadosEquipatribLanchr(CadosEquipatribLanchr cadosEquipatribLanchr) {
		this.cadosEquipatribLanchr = cadosEquipatribLanchr;
	}
	
	public EquipAtrib getObjEscolhido() {
		return objEscolhido;
	}
/*	
    public List<UsuarioIncModel> getObjRsUsuario() {
        return objRsUsuario;
  }

  public void setObjRsUsuario(List<UsuarioIncModel> objRsUsuario) {
        this.objRsUsuario = objRsUsuario;
  }
*/

}
