package com.altec.bsbr.app.hyb.web.jsf;

import java.util.Map;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.apache.tools.ant.types.resources.selectors.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.altec.bsbr.app.hyb.dto.UsuarioIncModel;
import com.altec.bsbr.app.hyb.web.util.XHYUsuarioIncService;

@Component("relatorioosBean")
@Scope("session")
public class RelatoriosOsBean extends BasicBBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//Session
	@Autowired
	private XHYUsuarioIncService user;
	
	private UsuarioIncModel userData;
	
	private String strArea;
	private int intCodArea;

	private String dtIni;
	private String dtFim;
	
	private Map<String, String> params;

 
	@PostConstruct
	public void init() {
		FacesContext fc = FacesContext.getCurrentInstance();
		this.params = fc.getExternalContext().getRequestParameterMap();
		dtIni = params.getOrDefault("pDtIni", "");
		dtFim = params.getOrDefault("pDtFim", "");
			
		
		if (user != null) {			
			try {
				userData = user.getDadosUsuario();
				
				if (userData.getNomeAreaUsuario().isEmpty() || userData.getNomeAreaUsuario().equals("")) {
					this.strArea = "GOE";
				} else {
					this.strArea = userData.getNomeAreaUsuario();
				}
				
				this.intCodArea = Integer.parseInt(userData.getCdAreaUsuario());
			
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		/*
		 * 'CD_AREA_USUARIO = "1520232"
		 * '1520232 GOE
		 * '1390404 GOI
		 * '1390405 PREV
		 * '1390405
		 * GOFE
		 */
	}

	public void cleanSession() {
		FacesContext context = FacesContext.getCurrentInstance();

		if (context.getExternalContext().getSessionMap().get("RelosEstatiticoSegmentoBean") != null) {
			context.getExternalContext().getSessionMap().remove("RelosEstatiticoSegmentoBean");
		}

		if (context.getExternalContext().getSessionMap().get("relosFraudeIniciadaBean") != null) {
			context.getExternalContext().getSessionMap().remove("relosFraudeIniciadaBean");
		}

		if (context.getExternalContext().getSessionMap().get("relosFraudeEncerradaBean") != null) {
			context.getExternalContext().getSessionMap().remove("relosFraudeEncerradaBean");
		}

		if (context.getExternalContext().getSessionMap().get("relosEncerradacanalCd") != null) {
			context.getExternalContext().getSessionMap().remove("relosEncerradacanalCd");
		}

		if (context.getExternalContext().getSessionMap().get("relosEncerradaCanalIBBean") != null) {
			context.getExternalContext().getSessionMap().remove("relosEncerradaCanalIBBean");
		}

		if (context.getExternalContext().getSessionMap().get("relosEncerradacanalSl") != null) {
			context.getExternalContext().getSessionMap().remove("relosEncerradacanalSl");
		}

		if (context.getExternalContext().getSessionMap().get("relOsRecuperacaoFraudeBean") != null) {
			context.getExternalContext().getSessionMap().remove("relOsRecuperacaoFraudeBean");
		}

		if (context.getExternalContext().getSessionMap().get("relOsControlePenalidadeBean") != null) {
			context.getExternalContext().getSessionMap().remove("relOsControlePenalidadeBean");
		}

		if (context.getExternalContext().getSessionMap().get("RelosFraudeInternaPeriodoBean") != null) {
			context.getExternalContext().getSessionMap().remove("RelosFraudeInternaPeriodoBean");
		}

		if (context.getExternalContext().getSessionMap().get("relosControleProcessosAbertoBean") != null) {
			context.getExternalContext().getSessionMap().remove("relosControleProcessosAbertoBean");
		}

		if (context.getExternalContext().getSessionMap().get("RelOsDadosContabeisROBean") != null) {
			context.getExternalContext().getSessionMap().remove("RelOsDadosContabeisROBean");
		}

		if (context.getExternalContext().getSessionMap().get("relosEstatisticoBean") != null) {
			context.getExternalContext().getSessionMap().remove("relosEstatisticoBean");
		}

		if (context.getExternalContext().getSessionMap().get("RelOsDadosContabeisROBean") != null) {
			context.getExternalContext().getSessionMap().remove("RelOsDadosContabeisROBean");
		}

		if (context.getExternalContext().getSessionMap().get("relOSCustoTotalOSBean") != null) {
			context.getExternalContext().getSessionMap().remove("relOSCustoTotalOSBean");
		}

		if (context.getExternalContext().getSessionMap().get("relosEventoBean") != null) {
			context.getExternalContext().getSessionMap().remove("relosEventoBean");
		}
	}
	

	
	public String getDtIni() {
		return dtIni;
	}

	public void setDtIni(String dtIni) {
		
		this.dtIni = dtIni;
	}

	public String getDtFim() {

		return dtFim;
	}

	public void setDtFim(String dtFim) {
		this.dtFim = dtFim;
	}

	public int getIntCodArea() {
		return intCodArea;
	}

	public void setIntCodArea(int intCodArea) {
		this.intCodArea = intCodArea;
	}

	public String getStrArea() {
		return strArea;
	}

	public void setStrArea(String strArea) {
		this.strArea = strArea;
	}

	

}
