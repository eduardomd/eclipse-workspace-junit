package com.altec.bsbr.app.hyb.dto;

public class ObjRsOsProvisao {
	private String SITUACAO;
	private String VALOR;
	
	public ObjRsOsProvisao() {}

	public ObjRsOsProvisao(String sITUACAO, String vALOR) {
		super();
		SITUACAO = sITUACAO;
		VALOR = vALOR;
	}

	public String getSITUACAO() {
		return SITUACAO;
	}

	public void setSITUACAO(String sITUACAO) {
		SITUACAO = sITUACAO;
	}

	public String getVALOR() {
		return VALOR;
	}

	public void setVALOR(String vALOR) {
		VALOR = vALOR;
	}
	
}
