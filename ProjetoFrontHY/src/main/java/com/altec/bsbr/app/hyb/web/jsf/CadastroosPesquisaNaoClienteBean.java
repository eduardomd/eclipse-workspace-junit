package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.ObjRsCombo;
import com.altec.bsbr.app.hyb.dto.ObjRsParticip;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsParticip.XHYCadOsParticipEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;

import java.util.*;

@Component("cadastroosPesquisaNaoClienteBean")
@Scope("session")
public class CadastroosPesquisaNaoClienteBean extends BasicBBean {
	
	private static final long serialVersionUID = 1L;
	
	private String strNrSeqOs;
	private String strDocto;
	private String strNome;
	private String strMatricula;
	private String strTipo = "F";
	private String strAcao;
    private List<ObjRsParticip> objRsParticipNaoClientes = new ArrayList<ObjRsParticip>();
	private List<ObjRsCombo> objRsItemsCombo = new ArrayList<ObjRsCombo>();
	int iCountPartLesado = 1;
	
	private boolean emptyMsg;
	
	@Autowired
	private XHYCadOsParticipEndPoint cadOsParticipEndPoint;
	
	@PostConstruct
	public void init() {
		System.out.println("init hide text -->>");
		buscarQueryString();
		this.emptyMsg = false;
	}
	
	public void buscarQueryString() {
		/*this.strNrSeqOs = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs");
		System.out.println(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs"));*/
		
		
		if(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs") != null) {
			System.out.println(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs") + "sequence os");
			strNrSeqOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs").toString();
		}
		else if(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("frmParticipante:modalEquipe:nros") != null) {
			System.out.println(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("frmParticipante:modalEquipe:nros") + "sequence os equipe");
			strNrSeqOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("frmParticipante:modalEquipe:nros").toString();
		}
	}
	
	public void refreshNaoClienteNrOS(String seqOs) {
		System.out.println("refreshNaoClienteNrOS ---");
		RequestContext.getCurrentInstance().execute("parent.parent.PF('statusDialog').hide();");
		this.strNrSeqOs = seqOs;
		System.out.println(seqOs + " seqOs");
	}
	
	public void adicionaCombo() {
		objRsItemsCombo.add(new ObjRsCombo( "1", "Envolvido", "", ""));
		objRsItemsCombo.add(new ObjRsCombo("2", "Informante", "", ""));
		objRsItemsCombo.add(new ObjRsCombo("3", "Reclamante", "", ""));
	}
	
	public void FillTblPart() {
		
		String shdnTipo = "4";
		if(this.getStrTipo().equals("J")) {
			shdnTipo = "5";
		}
		try {
			objRsItemsCombo.clear();
			objRsParticipNaoClientes = new ArrayList<ObjRsParticip>();
			adicionaCombo();
			iCountPartLesado = cadOsParticipEndPoint.fnExistePartLesado(this.getStrNrSeqOs());			
			if(iCountPartLesado == 0) {
				objRsItemsCombo.add(new ObjRsCombo("4", "Reclamado/Lesado", "", ""));
			}
			this.strDocto = this.strDocto.replaceAll("[^0-9]", "");
			System.out.println(this.getStrDocto()+" ,"+ shdnTipo+" ,"+ this.getStrNome());
			JSONObject json = new JSONObject(cadOsParticipEndPoint.consultarPartNaoCliente(this.getStrDocto(), shdnTipo, this.getStrNome()));
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			
			for (int i = 0; i < pcursor.length(); i++) {
				JSONObject item = pcursor.getJSONObject(i);
				
				ObjRsParticip particip = new ObjRsParticip();
				particip.setCPF_CNPJ(item.isNull("NR_CPF_CNPJ")? "" :item.get("NR_CPF_CNPJ").toString());
				particip.setTP_PARP(item.isNull("TP_PARP")? null :item.getInt("TP_PARP"));
				particip.setNM_PARP(item.isNull("NM_PARP")? "" :item.get("NM_PARP").toString());
				particip.setCBO_DESIG(1);
				objRsParticipNaoClientes.add(particip);
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			RequestContext.getCurrentInstance().execute("parent.parent.PF('statusDialog').hide()");
			this.emptyMsg = this.objRsParticipNaoClientes.isEmpty();
		}
		
	}
	
	public void limparCampos() {
		System.out.println("limpar campos ***");
		this.objRsParticipNaoClientes.clear();
		this.setStrMatricula("");
		this.setStrDocto("");
		this.setStrNome("");
		this.setStrTipo("F");
	}
	    
	
	public void GravarParticipante() {
		System.out.println("GravarParticipant");
		int index = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("indice"));
				objRsParticipNaoClientes.get(index).getCPF_CNPJ(); //cpf 
				objRsParticipNaoClientes.get(index).getNM_PARP(); //nome
				objRsParticipNaoClientes.get(index).getCBO_DESIG(); //tipo
		
		String shdnTipo = "4";
		if(this.getStrTipo().equals("J")) {
			shdnTipo = "5";
		}
		
		try {
			System.out.println("" +" ,"+ this.objRsParticipNaoClientes.get(index).getCPF_CNPJ()+" ,"+ this.getStrNrSeqOs()+" ,"+ Integer.valueOf(shdnTipo)+" ,"+ this.objRsParticipNaoClientes.get(index).getNM_PARP()+" ,"+ objRsParticipNaoClientes.get(index).getCBO_DESIG()+" ,"+ ""+" ,"+ ""+" ,"+ 0);
			JSONObject json = new JSONObject(cadOsParticipEndPoint.inserirParticipante("", this.objRsParticipNaoClientes.get(index).getCPF_CNPJ(), this.getStrNrSeqOs(), Integer.valueOf(shdnTipo), this.objRsParticipNaoClientes.get(index).getNM_PARP(), objRsParticipNaoClientes.get(index).getCBO_DESIG(), "", "", 0));
			int dblGravar = json.getInt("PNR_SEQU_PARP");
			
			if(dblGravar > 0) {
				RequestContext.getCurrentInstance().execute("alert('Participante incluido com sucesso.');");
			}else {
				RequestContext.getCurrentInstance().execute("alert('Não foi possível gravar o registro.');");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			RequestContext.getCurrentInstance().execute("alert('Não foi possível gravar o registro');");
		} finally {
			limparCampos();
		}
	   
	}
	
	public String getStrAcao() {
		return strAcao;
	}
	public void setStrAcao(String strAcao) {
		this.strAcao = strAcao;
	}
	public String getStrNrSeqOs() {
		return strNrSeqOs;
	}
	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}
	public String getStrDocto() {
		return strDocto;
	}
	public void setStrDocto(String strDocto) {
		this.strDocto = strDocto;
	}
	public String getStrNome() {
		return strNome;
	}
	public void setStrNome(String strNome) {
		this.strNome = strNome;
	}
	public String getStrMatricula() {
		return strMatricula;
	}
	public void setStrMatricula(String strMatricula) {
		this.strMatricula = strMatricula;
	}
	public String getStrTipo() {
		return strTipo;
	}
	public void setStrTipo(String strTipo) {
		this.strTipo = strTipo;
	}

	public List<ObjRsParticip> getObjRsParticipNaoClientes() {
		return objRsParticipNaoClientes;
	}

	public void setObjRsParticipNaoClientes(List<ObjRsParticip> objRsParticipNaoClientes) {
		this.objRsParticipNaoClientes = objRsParticipNaoClientes;
	}

	public List<ObjRsCombo> getObjRsItemsCombo() {
		return objRsItemsCombo;
	}

	public void setObjRsItemsCombo(List<ObjRsCombo> objRsItemsCombo) {
		this.objRsItemsCombo = objRsItemsCombo;
	}

	public int getiCountPartLesado() {
		return iCountPartLesado;
	}

	public void setiCountPartLesado(int iCountPartLesado) {
		this.iCountPartLesado = iCountPartLesado;
	}
	
	public boolean isEmptyMsg() {
		return emptyMsg;
	}

	public void setEmptyMsg(boolean emptyMsg) {
		this.emptyMsg = emptyMsg;
	}
		
}
