package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.UsuarioIncModel;
import com.altec.bsbr.app.hyb.web.util.LogIncUtil;
import com.altec.bsbr.app.hyb.web.util.XHYUsuarioIncService;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmLog.XHYAdmLogEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsDatas.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsDatas.XHYCadOsDatasEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;

import antlr.debug.Event;

@Component("cadOsDatasBean")
@Scope("session")
public class CadOsDatasBean extends BasicBBean {

	private static final long serialVersionUID = 1L;
	private String strAbertura;
	private String strPeriodo;
	private String strDeteccao;
	private String strEncerramento;
	private String strTipoDtEvento;
	private String strCodigo;
	private String strPeriodoFim;
	private String strPeriodoIni;
	private String strFixa;
	private String strNrSeqOs;
	private String strReadOnly;
	private boolean desabilitarCampos = false;
	
	@Autowired
	private XHYCadOsDatasEndPoint admCadDatas;

	@Autowired
	private XHYAdmLogEndPoint log;

	@Autowired
	private XHYUsuarioIncService usuarioService;


	private UsuarioIncModel usuario;

	@PostConstruct
	public void init() {
		this.strNrSeqOs = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs");
		this.strReadOnly = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strReadOnly");
		instanciarUsuario();
		consultaDataOs();	
		verificaQueryString();

	}

	public void consultaDataOs() {
		String retorno;

		try {

			retorno = admCadDatas.consultarOs(strNrSeqOs);

			JSONObject pesqDatas = new JSONObject(retorno);
			JSONArray pcursor = pesqDatas.getJSONArray("PCURSOR");

			JSONObject item = pcursor.getJSONObject(0);

			this.setStrCodigo(item.isNull("CODIGO") == true ? "" : item.get("CODIGO").toString());
			this.setStrAbertura(item.isNull("ABERTURA") == true ? "" : item.get("ABERTURA").toString());
			this.setStrEncerramento(item.isNull("ENCERRAMENTO") == true ? "" : item.get("ENCERRAMENTO").toString());
			this.setStrDeteccao(item.isNull("DETECCAO") == true ? "" : item.get("DETECCAO").toString());
			this.setStrTipoDtEvento(item.isNull("TIPO_DT_EVENTO") == true ? "" : item.get("TIPO_DT_EVENTO").toString());
			this.setStrFixa(item.isNull("FIXA") == true ? "" : item.get("FIXA").toString());
			this.setStrPeriodoIni(item.isNull("PERIODO_INI") == true ? "" : item.get("PERIODO_INI").toString());
			this.setStrPeriodoFim(item.isNull("PERIODO_FIN") == true ? "" : item.get("PERIODO_FIN").toString());
			RequestContext.getCurrentInstance().execute("DesabilitaData("+this.strTipoDtEvento+")");
				

		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	


	public void instanciarUsuario() {
		try {
			usuario = usuarioService.getDadosUsuario();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	public void verificaQueryString() {
		if (!(this.strReadOnly == null) && !this.strReadOnly.isEmpty()) {
			if (this.strReadOnly.equals("1") ) {
				setDesabilitarCampos(true);
			} 
		}
	}
		
		
	public void salvarData() {

		if (this.getStrFixa().isEmpty() && this.getStrPeriodoIni().isEmpty() && this.getStrPeriodoFim().isEmpty()) {
			this.setStrTipoDtEvento("");
		}

		if (this.getStrDeteccao().isEmpty()) {
			this.setStrDeteccao("");
		} else {
			this.setStrDeteccao(convertData(this.getStrDeteccao()));
		}

		if (this.getStrFixa().isEmpty()) {
			this.setStrFixa("");
		} else {
			this.setStrFixa(convertData(this.getStrFixa()));
		}

		if (this.getStrPeriodoIni().isEmpty()) {
			this.setStrPeriodoIni("");
		} else {
			this.setStrPeriodoIni(convertData(this.getStrPeriodoIni()));
		}

		if (this.getStrPeriodoFim().isEmpty()) {
			this.setStrPeriodoFim("");
		} else {
			this.setStrPeriodoFim(convertData(this.getStrPeriodoFim()));
		}

		try {

			admCadDatas.alterarOs(strNrSeqOs, this.getStrDeteccao(), this.getStrFixa(), this.getStrTipoDtEvento(),
					this.getStrPeriodoIni(), this.getStrPeriodoFim());

			try {
				log.incluirAcaoLog(strNrSeqOs, Integer.toString(LogIncUtil.SALVAR_DATAS_OS), usuario.getCpfUsuario(),
						"");
			} catch (Exception e) {
				try {
					FacesContext.getCurrentInstance().getExternalContext()
							.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			consultaDataOs();
			RequestContext.getCurrentInstance().execute("alert('Dados salvos com sucesso!');");
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

	}
	
	
	public void limpaDataBean() {
		String tipo = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("tipo").toString();
		if(tipo.equals("1")) {
			strPeriodoIni = "";
			strPeriodoFim = "";
		}
		if(tipo.equals("2")) {
			strFixa = "";
		}
	}

	public void validaDataOcor(SelectEvent event) {
		

		String idInput = "ValidaDataOcorrencia('form:" + event.getComponent().getId() + "_input',1)";

		RequestContext.getCurrentInstance().execute(idInput);

	}

	public static String convertData(String dataInput) {
		String dataInput2 = "";

		if (dataInput.matches("([0-9]{2}/[0-9]{2}/[0-9]{4})")) {
			Date initDate = null;
			try {
				initDate = new SimpleDateFormat("dd/MM/yyyy").parse(dataInput);
			} catch (java.text.ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			dataInput2 = formatter.format(initDate);

		} else {

			try {

				dataInput2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
						.format(new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyy", Locale.US).parse(dataInput));

			} catch (ParseException e) {
				try {
					FacesContext.getCurrentInstance().getExternalContext()
							.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
		return dataInput2;
	}

	public String getStrAbertura() {
		return strAbertura;
	}

	public void setStrAbertura(String strAbertura) {
		this.strAbertura = strAbertura;
	}

	public String getStrPeriodo() {
		return strPeriodo;
	}

	public void setStrPeriodo(String strPeriodo) {
		this.strPeriodo = strPeriodo;
	}

	public String getStrDeteccao() {
		return strDeteccao;
	}

	public void setStrDeteccao(String strDeteccao) {
		this.strDeteccao = strDeteccao;
	}

	public String getStrEncerramento() {
		return strEncerramento;
	}

	public void setStrEncerramento(String strEncerramento) {
		this.strEncerramento = strEncerramento;
	}

	public String getStrTipoDtEvento() {
		return strTipoDtEvento;
	}

	public void setStrTipoDtEvento(String strTipoDtEvento) {
		this.strTipoDtEvento = strTipoDtEvento;
	}

	public String getStrCodigo() {
		return strCodigo;
	}

	public void setStrCodigo(String strCodigo) {
		this.strCodigo = strCodigo;
	}

	public String getStrPeriodoFim() {
		return strPeriodoFim;
	}

	public void setStrPeriodoFim(String strPeriodoFim) {
		this.strPeriodoFim = strPeriodoFim;
	}

	public String getStrPeriodoIni() {
		return strPeriodoIni;
	}

	public void setStrPeriodoIni(String strPeriodoIni) {
		this.strPeriodoIni = strPeriodoIni;
	}

	public String getStrFixa() {
		return strFixa;
	}

	public void setStrFixa(String strFixa) {
		this.strFixa = strFixa;
	}

	public String getStrReadOnly() {
		return strReadOnly;
	}

	public void setStrReadOnly(String strReadOnly) {
		this.strReadOnly = strReadOnly;
	}

	public boolean isDesabilitarCampos() {
		return desabilitarCampos;
	}

	public void setDesabilitarCampos(boolean desabilitarCampos) {
		this.desabilitarCampos = desabilitarCampos;
	}

}
