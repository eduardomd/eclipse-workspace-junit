package com.altec.bsbr.app.hyb.web.jsf;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;

import com.altec.bsbr.app.hyb.dto.CadosEquipatribLanchr;
import com.altec.bsbr.app.hyb.web.util.clsCadOsEquipAtrib;

@Component("EquipatribLanchrBean")
@Scope("session")
public class CadosEquipatribLanchrIframeBean extends BasicBBean{

	private static final long serialVersionUID = 1L;
	private CadosEquipatribLanchr cadosEquipatribLanchr;
	
	public CadosEquipatribLanchrIframeBean() {
		this.cadosEquipatribLanchr = new CadosEquipatribLanchr();
		
		String strNrSeqOs;
		String strCpf;
		String lngQtHoras;
		String lngVlGasto;
		String strErro = "";
		String strNomeRecurso;
		
		strNrSeqOs = cadosEquipatribLanchr.getStrNrSeqOs();
		strCpf = cadosEquipatribLanchr.getStrCpf();
		strNomeRecurso = cadosEquipatribLanchr.getStrNomeRecurso();
		clsCadOsEquipAtrib objCadOs = new clsCadOsEquipAtrib();
		
		if (cadosEquipatribLanchr.getTxtHdAction() == "S") {
		    strNrSeqOs  = cadosEquipatribLanchr.getStrNrSeqOs();
    	    strCpf = cadosEquipatribLanchr.getStrCpf();
    	    lngQtHoras = cadosEquipatribLanchr.getHORAS();
    	    lngVlGasto = cadosEquipatribLanchr.getGASTOS_ADICIONAIS();
    	    
    	    objCadOs.LancarHoras(strNrSeqOs, strCpf, lngQtHoras, lngVlGasto, strErro);
    	    VerificaErro(strErro);
    	    RequestContext.getCurrentInstance().execute("alert('Horas e valores lan�ados com sucesso!');");
    	    //InsereLog LANCAR_HORAS_TRABALHADAS_OS, strCpfUsu, strNrSeqOs, ""
		}
		
		objCadOs.ConsultarLancamentoHoras(strNrSeqOs, strCpf, strErro);
		VerificaErro(strErro);
	}

	public CadosEquipatribLanchr getCadosEquipatribLanchr() {
		return cadosEquipatribLanchr;
	}

	public void setCadosEquipatribLanchr(CadosEquipatribLanchr cadosEquipatribLanchr) {
		this.cadosEquipatribLanchr = cadosEquipatribLanchr;
	}
	
	public void VerificaErro(String strErro) {
		if (strErro.isEmpty() == false) {
			//Response.Redirect("HY_Erro.asp?strErro=" & strErro)
		}
	}


	
}
