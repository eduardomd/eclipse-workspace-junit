package com.altec.bsbr.app.hyb.dto;

public class CadOsEventosDetalhesModel {

	private String cartaoNrSequCrto; 
	private String cartaoNrCrto;
	private String cartaoNrSequParpProc;		
	private String cartaoNrSequFrauCnal;
	private String cartaoNrSequCntaBncr;
	
	private String contaNrSequCntaBncr; 
	private String contaNrCnta;
	private String contaNrSequParpProc;		
	private String contaNrSequFrauCnal;
	private String contaCdBanc;
	private String contaTpOperParp;
	private String contaQtdLanc;
	
	private String contratoNrSequCntr; 
	private String contratoNrCntr;
	private String contratoNrSequParpProc;		
	private String contratoNrSequFrauCnal;
	private String contratoCdEntiCntr;
	private String contratoNrProdCorp;
	private String contratoNrSequOrdeServ;
	private String contratoCdProd;
	private String contratoTpOperParp;
	private String contratoQtdLanc;
	
	public CadOsEventosDetalhesModel() {
	}
	
	public CadOsEventosDetalhesModel(String cartaoNrSequCrto, String cartaoNrCrto, String cartaoNrSequParpProc, String cartaoNrSequFrauCnal, String cartaoNrSequCntaBncr, String contaNrSequCntaBncr, String contaNrCnta, String contaNrSequParpProc, String contaNrSequFrauCnal, String contaCdBanc, String contaTpOperParp, String contaQtdLanc) {
		this.cartaoNrSequCrto = cartaoNrSequCrto; 
		this.cartaoNrCrto = cartaoNrCrto; 
		this.cartaoNrSequParpProc = cartaoNrSequParpProc; 		
		this.cartaoNrSequFrauCnal = cartaoNrSequFrauCnal; 
		this.cartaoNrSequCntaBncr = cartaoNrSequCntaBncr; 
		
		this.contaNrSequCntaBncr = contaNrSequCntaBncr;  
		this.contaNrCnta = contaNrCnta; 
		this.contaNrSequParpProc = contaNrSequParpProc; 		
		this.contaNrSequFrauCnal = contaNrSequFrauCnal; 
		this.contaCdBanc = contaCdBanc; 
		this.contaTpOperParp = contaTpOperParp; 
		this.contaQtdLanc = contaQtdLanc;
	}
	
	public String getCartaoNrSequCrto() {
		return cartaoNrSequCrto;
	}
	public void setCartaoNrSequCrto(String cartaoNrSequCrto) {
		this.cartaoNrSequCrto = cartaoNrSequCrto;
	}
	public String getCartaoNrCrto() {
		return cartaoNrCrto;
	}
	public void setCartaoNrCrto(String cartaoNrCrto) {
		this.cartaoNrCrto = cartaoNrCrto;
	}
	public String getCartaoNrSequParpProc() {
		return cartaoNrSequParpProc;
	}
	public void setCartaoNrSequParpProc(String cartaoNrSequParpProc) {
		this.cartaoNrSequParpProc = cartaoNrSequParpProc;
	}
	public String getCartaoNrSequFrauCnal() {
		return cartaoNrSequFrauCnal;
	}
	public void setCartaoNrSequFrauCnal(String cartaoNrSequFrauCnal) {
		this.cartaoNrSequFrauCnal = cartaoNrSequFrauCnal;
	}
	public String getCartaoNrSequCntaBncr() {
		return cartaoNrSequCntaBncr;
	}
	public void setCartaoNrSequCntaBncr(String cartaoNrSequCntaBncr) {
		this.cartaoNrSequCntaBncr = cartaoNrSequCntaBncr;
	}
	public String getContaNrSequCntaBncr() {
		return contaNrSequCntaBncr;
	}
	public void setContaNrSequCntaBncr(String contaNrSequCntaBncr) {
		this.contaNrSequCntaBncr = contaNrSequCntaBncr;
	}
	public String getContaNrCnta() {
		return contaNrCnta;
	}
	public void setContaNrCnta(String contaNrCnta) {
		this.contaNrCnta = contaNrCnta;
	}
	public String getContaNrSequParpProc() {
		return contaNrSequParpProc;
	}
	public void setContaNrSequParpProc(String contaNrSequParpProc) {
		this.contaNrSequParpProc = contaNrSequParpProc;
	}
	public String getContaNrSequFrauCnal() {
		return contaNrSequFrauCnal;
	}
	public void setContaNrSequFrauCnal(String contaNrSequFrauCnal) {
		this.contaNrSequFrauCnal = contaNrSequFrauCnal;
	}
	public String getContaCdBanc() {
		return contaCdBanc;
	}
	public void setContaCdBanc(String contaCdBanc) {
		this.contaCdBanc = contaCdBanc;
	}
	public String getContaTpOperParp() {
		return contaTpOperParp;
	}
	public void setContaTpOperParp(String contaTpOperParp) {
		this.contaTpOperParp = contaTpOperParp;
	}
	public String getContaQtdLanc() {
		return contaQtdLanc;
	}
	public void setContaQtdLanc(String contaQtdLanc) {
		this.contaQtdLanc = contaQtdLanc;
	}
	public String getContratoNrSequCntr() {
		return contratoNrSequCntr;
	}
	public void setContratoNrSequCntr(String contratoNrSequCntr) {
		this.contratoNrSequCntr = contratoNrSequCntr;
	}
	public String getContratoNrCntr() {
		return contratoNrCntr;
	}
	public void setContratoNrCntr(String contratoNrCntr) {
		this.contratoNrCntr = contratoNrCntr;
	}
	public String getContratoNrSequParpProc() {
		return contratoNrSequParpProc;
	}
	public void setContratoNrSequParpProc(String contratoNrSequParpProc) {
		this.contratoNrSequParpProc = contratoNrSequParpProc;
	}
	public String getContratoNrSequFrauCnal() {
		return contratoNrSequFrauCnal;
	}
	public void setContratoNrSequFrauCnal(String contratoNrSequFrauCnal) {
		this.contratoNrSequFrauCnal = contratoNrSequFrauCnal;
	}
	public String getContratoCdEntiCntr() {
		return contratoCdEntiCntr;
	}
	public void setContratoCdEntiCntr(String contratoCdEntiCntr) {
		this.contratoCdEntiCntr = contratoCdEntiCntr;
	}
	public String getContratoNrProdCorp() {
		return contratoNrProdCorp;
	}
	public void setContratoNrProdCorp(String contratoNrProdCorp) {
		this.contratoNrProdCorp = contratoNrProdCorp;
	}
	public String getContratoNrSequOrdeServ() {
		return contratoNrSequOrdeServ;
	}
	public void setContratoNrSequOrdeServ(String contratoNrSequOrdeServ) {
		this.contratoNrSequOrdeServ = contratoNrSequOrdeServ;
	}
	public String getContratoCdProd() {
		return contratoCdProd;
	}
	public void setContratoCdProd(String contratoCdProd) {
		this.contratoCdProd = contratoCdProd;
	}
	public String getContratoTpOperParp() {
		return contratoTpOperParp;
	}
	public void setContratoTpOperParp(String contratoTpOperParp) {
		this.contratoTpOperParp = contratoTpOperParp;
	}
	public String getContratoQtdLanc() {
		return contratoQtdLanc;
	}
	public void setContratoQtdLanc(String contratoQtdLanc) {
		this.contratoQtdLanc = contratoQtdLanc;
	}
}
