package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.security.auth.message.callback.PrivateKeyCallback.Request;

import org.eclipse.core.resources.mapping.ModelStatus;
import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.CadastroOsSLinhaIBanking;
import com.altec.bsbr.app.hyb.dto.CadastroOsSLinhaObjCanais;
import com.altec.bsbr.app.hyb.dto.CadastroOsSLinhaObjRs;
import com.altec.bsbr.app.hyb.dto.CadastroOsSLinhaObjRsCanal;
import com.altec.bsbr.app.hyb.dto.CadastroOsSLinhaTelfAcess;
import com.altec.bsbr.app.hyb.dto.OperAuxModel;
import com.altec.bsbr.app.hyb.dto.OperCorpModel;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsCanais.XHYCadOsCanaisEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsEventos.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsEventos.XHYCadOsEventosEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsUnidEnvol.XHYCadOsUnidEnvolEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.ibm.icu.text.SimpleDateFormat;

@Component("cadastroOsSLinhaBean")
@Scope("session")
public class CadastroOsSLinhaBean extends BasicBBean {
	
	private static final long serialVersionUID = 1L;
	
	@Autowired
	private XHYCadOsEventosEndPoint cadOsEventos;
	
	@Autowired
	private XHYCadOsCanaisEndPoint cadOsCanais;
	
	@Autowired
	private XHYCadOsUnidEnvolEndPoint cadOsUnidEnvolv;
	
	private CadastroOsSLinhaObjRsCanal objRsCanal;
	private CadastroOsSLinhaObjRs objRs;
	
	private String strNrCanal;
	private String strNrEvento;
	private String strNrSeqDetalhe;
	private String strNrSeqId;
	
	private List<CadastroOsSLinhaIBanking> iBanking;
	private List<CadastroOsSLinhaTelfAcess> telfAcessCnta;
	private List<CadastroOsSLinhaObjCanais> objCanais;
	
	private Locale ptBR = new Locale("pt", "BR");
	
	//Tel acessaram a conta
	private String txtDDDTelfAcessCnta;
	private String txtTelfAcessCnta;
	
	private boolean chkItssComl;
	private boolean showDatas; 
	
	private int intQntLancDbto = 0;
	private int ibtQntLancCrdt = 0;
	private int intQntLancCrdt = 0;
	private double intValAux = 0;
	
	private List<OperCorpModel> objRsOperCorp;
	private List<OperAuxModel> objRsOperAux;
	private String selectedOperCorp;
	private String selectedOperAux;
	
	private String selectRdoContestacao;
	
	private double intQntValDbto;
	private double intQntValCrdt;
	
	private boolean isCredito;
	private boolean origExists;
	private boolean destExists;
	private boolean ret;
	
	private String strDtPeriFixa;
	private String strDtPeriIni;
	private String strDtPeriFim;

	
	@PostConstruct
	public void init() {
		
		System.out.println("init -");
		this.strNrCanal = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("qsCanal").toString();
		this.strNrEvento = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("qsEvento").toString();
		this.strNrSeqDetalhe = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("NrSeqDetalhe").toString();
		this.strNrSeqId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("NrSeqDetalhe").toString();
		this.isCredito = false;
		
		consultarFraudeEvento();
		consultarContasAcessadasTelefone();
		consultarTelefones();
		consultarOperacaoCanal();
		carregaComboOper(1);
		carregaComboOperAux(1);
		
		this.selectRdoContestacao = "1";
		this.showDatas = !this.objRsCanal.getIN_CRTO_SEGR().equals("0"); 

	}
	
	public String[] formataTelefone(String str) {
		String newStr = ("0000000000" + str).substring(str.length());
		
		String n2 = newStr.substring(2, newStr.length()-4);
		String ddd = newStr.substring(0,2);
		String n3 = newStr.substring(newStr.length()-4);
		String fullNumber = n2 + "-" + n3;
		String formattefTel = "(" + ddd + ") " + fullNumber;
		String[] numArr = {ddd, fullNumber, formattefTel};
		return numArr;
	}

	public void consultarFraudeEvento() {
		try {
			System.out.println("consulta fraude evento");
			JSONObject json = new JSONObject(cadOsEventos.consultarFraudeCanalDetalhe(Long.parseLong(this.strNrSeqDetalhe)));
			
			JSONArray pcursor = json.getJSONArray("PCURSOR");
						
			JSONObject item = pcursor.getJSONObject(0);
			
			String strTelfClieDDD = formataTelefone(item.isNull("NR_TELF_CLIE") ? "" : item.get("NR_TELF_CLIE").toString())[0];
			String strTelfClie = formataTelefone(item.isNull("NR_TELF_CLIE") ? "" : item.get("NR_TELF_CLIE").toString())[1];
			
			String strAssTelSolicDDD = formataTelefone(item.isNull("NR_TELF_SOLI_ASSN_ELET") ? "" : item.get("NR_TELF_SOLI_ASSN_ELET").toString())[0];
			String strAssTelSolic = formataTelefone(item.isNull("NR_TELF_SOLI_ASSN_ELET") ? "" : item.get("NR_TELF_SOLI_ASSN_ELET").toString())[1];
			
			String strAssTelCadDDD = formataTelefone(item.isNull("NR_TELF_CADR_ASSN_ELET") ? "" : item.get("NR_TELF_CADR_ASSN_ELET").toString())[0];
			String strAssTelCad = formataTelefone(item.isNull("NR_TELF_CADR_ASSN_ELET") ? "" : item.get("NR_TELF_CADR_ASSN_ELET").toString())[1];

			String cdBanc = item.isNull("CD_BANC") ? "" : item.get("CD_BANC").toString();
			String strAgencia = ("0000" + cdBanc).substring(cdBanc.length());
			
			
			this.chkItssComl = item.get("IN_ITSS_COME").toString().equals("0") ? false : true;
			
			/*Date dtPrmFrau;
			if (!item.isNull("DT_PRMR_FRAU")) 
				dtPrmFrau = new Date((long) item.getInt("DT_PRMR_FRAU")*1000);
			else
				dtPrmFrau = null;*/
			
			String dtPrmFrau = "";
			if(!item.isNull("DT_PRMR_FRAU")) {
				
				dtPrmFrau = item.get("DT_PRMR_FRAU").toString();
				Timestamp t = new Timestamp(Long.parseLong(dtPrmFrau));
				SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
				Date date = new Date(t.getTime());
				dtPrmFrau = sf.format(date).toString();
			} 
			
			
			objRsCanal = new CadastroOsSLinhaObjRsCanal(
					item.isNull("NR_SEQU_ORDE_SERV") ? "" : item.get("NR_SEQU_ORDE_SERV").toString(),
					item.isNull("NM_PARP") ? "" : item.get("NM_PARP").toString(),
					item.isNull("NR_SEQU_PARP_PROC") ? "" : item.get("NR_SEQU_PARP_PROC").toString(),
					item.isNull("NR_CNTA") ? "" : item.get("NR_CNTA").toString(),
					item.isNull("NR_CPF_CNPJ_TITL") ? "" : item.get("NR_CPF_CNPJ_TITL").toString(),
					item.isNull("DT_FRAU") ? "" : item.get("DT_FRAU").toString(),
					strTelfClie,
					strTelfClieDDD,
					item.isNull("IN_BLOQ_TELF") ? "0" : item.get("IN_BLOQ_TELF").toString(),
					item.isNull("DT_BLOQ_TELF") ? "" : item.get("DT_BLOQ_TELF").toString(),
					item.isNull("HR_BLOQ_TELF") ? "" : item.get("HR_BLOQ_TELF").toString(),
					item.isNull("DT_SOLI_ASSN_ELET") ? "" : item.get("DT_SOLI_ASSN_ELET").toString(),
					strAssTelSolic,
					strAssTelSolicDDD,
					item.isNull("HR_SOLI_ASSN_ELET") ? "" : item.get("HR_SOLI_ASSN_ELET").toString(),
					item.isNull("DT_CADR_ASSN_ELET") ? "" : item.get("DT_CADR_ASSN_ELET").toString(),
					strAssTelCad,
					strAssTelCadDDD,
					item.isNull("HR_CADR_ASSN_ELET") ? "" : item.get("HR_CADR_ASSN_ELET").toString(),
					item.isNull("TP_CNTA_BNCR") ? "" : item.get("TP_CNTA_BNCR").toString(),
					item.isNull("DT_ABER_CNTA_BNCR") ? "" : item.get("DT_ABER_CNTA_BNCR").toString(),
					item.isNull("NM_GERE_RESP_CNTA_BNCR") ? "" : item.get("NM_GERE_RESP_CNTA_BNCR").toString(),
					item.isNull("IN_CRTO_SEGR") ? "" : item.get("IN_CRTO_SEGR").toString(),
					item.isNull("DT_SOLI_CRTO_SEGR") ? "" : item.get("DT_SOLI_CRTO_SEGR").toString(),
					item.isNull("DT_CANC_CRTO_SEGR") ? "" : item.get("DT_CANC_CRTO_SEGR").toString(),
					item.isNull("DT_ATIV_CRTO_SEGR") ? "" : item.get("DT_ATIV_CRTO_SEGR").toString(),
					dtPrmFrau,
					//dtPrmFrau == null ? "" : new SimpleDateFormat("dd/MM/yyyy").format(dtPrmFrau),
					item.isNull("NR_SEQU_CNTA_BNCR") ? "" : item.get("NR_SEQU_CNTA_BNCR").toString(),
					item.isNull("NR_SEQU_CRTO") ? "" : item.get("NR_SEQU_CRTO").toString(),
					item.isNull("NR_SEQU_FRAU_EVEN") ? "" : item.get("NR_SEQU_FRAU_EVEN").toString(),
					item.isNull("NR_SEQU_FRAU_CNAL") ? "" : item.get("NR_SEQU_FRAU_CNAL").toString(),
					item.isNull("CD_CNAL") ? "" : item.get("CD_CNAL").toString(),
					item.isNull("IN_ITSS_COME") ? "" : item.get("IN_ITSS_COME").toString(),
					strAgencia,
					item.isNull("DT_EMIS") ? "" : item.get("DT_EMIS").toString(),
					item.isNull("DT_ATIV") ? "" : item.get("DT_ATIV").toString(),
					item.isNull("DT_CANC") ? "" : item.get("DT_CANC").toString(),
					item.isNull("NM_LOG_RECU_SOLI") ? "" : item.get("NM_LOG_RECU_SOLI").toString(),
					item.isNull("NM_SITU_CNTA_BNCR") ? "" : item.get("NM_SITU_CNTA_BNCR").toString(),
					item.isNull("NM_ENDE_ENVI") ? "" : item.get("NM_ENDE_ENVI").toString(),
					item.isNull("NR_QNTD_EVEN") ? "" : item.get("NR_QNTD_EVEN").toString(),
					item.isNull("CD_CENT_CUST_ORIG") ? "" : item.get("CD_CENT_CUST_ORIG").toString(),
					item.isNull("CD_CENT_CUST_DEST") ? "" : item.get("CD_CENT_CUST_DEST").toString(),
					"6494",//CD_CENT_CUST_OPNT
					item.isNull("CONTABILIZADO") ? "" : item.get("CONTABILIZADO").toString());
			
			if(objRsCanal.getHR_BLOQ_TELF().length() > 5) {
				objRsCanal.setHR_BLOQ_TELF( objRsCanal.getHR_BLOQ_TELF().substring(0, 5));
			}
			
			if(objRsCanal.getHR_CADR_ASSN_ELET().length() > 5) {
				objRsCanal.setHR_CADR_ASSN_ELET(objRsCanal.getHR_CADR_ASSN_ELET().substring(0, 5));
			}
			
			if(objRsCanal.getHR_SOLI_ASSN_ELET().length() > 5) {
				objRsCanal.setHR_SOLI_ASSN_ELET(objRsCanal.getHR_SOLI_ASSN_ELET().substring(0, 5));
			}
			
		} catch (WebServiceException e) {
			e.printStackTrace();
		}
	}
	
	public void consultarContasAcessadasTelefone() {
		try {
			
			JSONObject json = new JSONObject(cadOsEventos.consultarContaIBanking(Long.parseLong(this.strNrSeqId)));
			
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			
			
			iBanking = new ArrayList<CadastroOsSLinhaIBanking>();

			for (int i = 0; i < pcursor.length(); i++) {
				JSONObject item = pcursor.getJSONObject(i);
			
				String frauCnal = item.isNull("NR_SEQU_FRAU_CNAL") ? "" : item.get("NR_SEQU_FRAU_CNAL").toString();
				String cntaAces = item.isNull("NR_CNTA_ACES") ? "" : item.get("NR_CNTA_ACES").toString();
				
				CadastroOsSLinhaIBanking modelBank = new CadastroOsSLinhaIBanking(frauCnal, cntaAces);
				
				iBanking.add(modelBank);
			}
			
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	public void insereContasAcessadasTelefone() {
		CadastroOsSLinhaIBanking modelBank = new CadastroOsSLinhaIBanking("", "");
		iBanking.add(modelBank);
	}
	
	public void excluirContasAcessadasTelefone() {
		String conta = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("conta");
		String canal = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("canal");

		try {
			cadOsEventos.excluirContaIBanking(Long.parseLong(canal), Long.parseLong(conta));
			consultarContasAcessadasTelefone();
		} catch (WebServiceException e) {
			e.printStackTrace();
		}
		
	}

	public void consultarTelefones() {
		try {
			
			JSONObject json = new JSONObject(cadOsEventos.consultarTelfAcessCnta(Long.parseLong(this.strNrSeqId)));
			
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			
			System.out.println("CURSsOR: " + pcursor);
			
			telfAcessCnta = new ArrayList<CadastroOsSLinhaTelfAcess>();

			for (int i = 0; i < pcursor.length(); i++) {
				JSONObject item = pcursor.getJSONObject(i);
			
				String nrTelf = formataTelefone(item.isNull("NR_TELF") ? "" : item.get("NR_TELF").toString())[2];
				String nrSequCnta = item.isNull("NR_SEQU_TELF_ACES_CNTA") ? "" : item.get("NR_SEQU_TELF_ACES_CNTA").toString();
				
				CadastroOsSLinhaTelfAcess modelTel = new CadastroOsSLinhaTelfAcess(nrTelf, nrSequCnta);
				
				telfAcessCnta.add(modelTel);
			}
			
		} catch (WebServiceException e) {
			e.printStackTrace();
		}
	}
	
	public void excluirTel() {
		String telConta = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("telConta");

		try {
			cadOsEventos.excluirTelfAcessCnta(Long.parseLong(telConta));
			consultarTelefones();
		} catch (WebServiceException e) {
			e.printStackTrace();
		}
	}
	
	public void gravarTelfAcessCnts() {
		
		String tel = txtDDDTelfAcessCnta + txtTelfAcessCnta.replace("-", "");
		System.out.println("DDD : " + txtDDDTelfAcessCnta);
		System.out.println("TEL: " + tel);
		try {
			cadOsEventos.gravarTelfAcessCnta(Long.parseLong(this.strNrSeqDetalhe), Long.parseLong(tel));
			this.txtDDDTelfAcessCnta = "";
			this.txtTelfAcessCnta = "";
			consultarTelefones();
		} catch (WebServiceException e) {
			e.printStackTrace();
		}
	}
	
	public String formataValorRecebido(String valorRecebido) {
		String valor = NumberFormat.getCurrencyInstance(ptBR).format(Double.parseDouble(valorRecebido.replace(",", ".")));
		return valor.replace("R$", "").trim();
	}
	
	public void consultarOperacaoCanal() {
		try {
			this.intQntLancDbto = 0;
			this.intQntLancCrdt = 0;
			
			JSONObject json = new JSONObject(cadOsCanais.retornaOperacaoCanal(this.strNrSeqId));
			
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			
			System.out.println("CURS OR: " + pcursor);
			
			objCanais = new ArrayList<CadastroOsSLinhaObjCanais>();

			for (int i = 0; i < pcursor.length(); i++) {
				JSONObject item = pcursor.getJSONObject(i);
			
				String nR_SEQU_TIPO_OPER_TRAN = item.isNull("NR_SEQU_TIPO_OPER_TRAN") ? "" : item.get("NR_SEQU_TIPO_OPER_TRAN").toString();
				String cD_TRAN = item.isNull("CD_TRAN") ? "" : item.get("CD_TRAN").toString();
				String nM_OPER = item.isNull("NM_OPER") ? "" : item.get("NM_OPER").toString();
				String nM_SUB_OPER = item.isNull("NM_SUB_OPER") ? "" : item.get("NM_SUB_OPER").toString();
				String cD_PROD_AUXI = item.isNull("CD_PROD_AUXI") ? "" : item.get("CD_PROD_AUXI").toString();
				String nM_PROD_AUXI = item.isNull("NM_PROD_AUXI") ? "" : item.get("NM_PROD_AUXI").toString();
				String vL_TRAN = item.isNull("VL_TRAN") ? "0" : item.get("VL_TRAN").toString();
				String tP_CNTE = item.isNull("TP_CNTE") ? "" : item.get("TP_CNTE").toString();
				String dT_TRAN = item.isNull("DT_TRAN") ? "" : item.get("DT_TRAN").toString();
				String hR_TRAN = item.isNull("HR_TRAN") ? "" : item.get("HR_TRAN").toString();
				String dT_OCOR_FIXA = item.isNull("DT_OCOR_FIXA") ? "" : item.get("DT_OCOR_FIXA").toString();
				String dT_OCOR_PERI_INIC = item.isNull("DT_OCOR_PERI_INIC") ? "" : item.get("DT_OCOR_PERI_INIC").toString();
				String dT_OCOR_PERI_FINA = item.isNull("DT_OCOR_PERI_FINA") ? "" : item.get("DT_OCOR_PERI_FINA").toString();
				String cARD = item.isNull("CARD") ? "" : item.get("CARD").toString();
				String nR_SEQU_TRAN_FRAU_CNAL = item.isNull("NR_SEQU_TRAN_FRAU_CNAL") ? "" : item.get("NR_SEQU_TRAN_FRAU_CNAL").toString();
				String dESC_TRAN = item.isNull("DESC_TRAN") ? "" : item.get("DESC_TRAN").toString();
				String dESC_CNTE = item.isNull("DESC_CNTE") ? "" : item.get("DESC_CNTE").toString();
				
				String strDtOper;
				String strHrOper;
				String strVlOper;
				
				
				if (item.isNull("VL_TRAN")) {
					this.intValAux = 0;
					strVlOper = "";
				} else {
					this.intValAux = Double.parseDouble(item.get("VL_TRAN").toString().replace(",", "."));
					strVlOper = formataValorRecebido(item.get("VL_TRAN").toString());
				}
				
				if (tP_CNTE.equals("1")) {
					this.intQntLancDbto += 1;
				} else if (tP_CNTE.equals("2")) {
					this.intQntLancCrdt += 1;
				}
				
				if (dT_TRAN.equals("01/01/1900")) {
					strDtOper = "";
				} else {
					strDtOper = dT_TRAN;
				}
				
				if (hR_TRAN.equals("00:00")) {
					strHrOper = "";
				} else {
					strHrOper = hR_TRAN;
				}
				
				if( !item.isNull("DT_TRAN") && strDtOper.equals("") && !dT_OCOR_FIXA.equals("")) {
					strDtOper = dT_OCOR_FIXA;
				}
				
				CadastroOsSLinhaObjCanais modelCanais = new CadastroOsSLinhaObjCanais(nR_SEQU_TIPO_OPER_TRAN, cD_TRAN, nM_OPER, nM_SUB_OPER, cD_PROD_AUXI, nM_PROD_AUXI, vL_TRAN, tP_CNTE, dT_TRAN, hR_TRAN, dT_OCOR_FIXA, dT_OCOR_PERI_INIC, dT_OCOR_PERI_FINA, cARD, nR_SEQU_TRAN_FRAU_CNAL, dESC_TRAN, dESC_CNTE, strDtOper, strHrOper, strVlOper);
				objCanais.add(modelCanais);
				
				this.strDtPeriFixa = dT_OCOR_FIXA;
				this.strDtPeriIni = dT_OCOR_PERI_INIC;
				this.strDtPeriFim = dT_OCOR_PERI_FINA;
			}
			
			atualizarValores();
			
		} catch (JSONException | com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsCanais.WebServiceException e) {
			e.printStackTrace();
		}
	}
	
	public void carregaComboOper(Integer intContestacao) {
		this.objRsOperCorp = new ArrayList<OperCorpModel>();
		try {
			JSONObject json = new JSONObject();
			json = new JSONObject(cadOsCanais.consultarOperacao(this.strNrCanal, intContestacao));
			//NM_OPER
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			for (int i = 0; i < pcursor.length(); i++) 
				this.objRsOperCorp.add(
						new OperCorpModel(pcursor.getJSONObject(i).isNull("NR_SEQU_TIPO_OPER_TRAN") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("NR_SEQU_TIPO_OPER_TRAN")),
									  pcursor.getJSONObject(i).isNull("CD_TRAN") ? "" : pcursor.getJSONObject(i).getString("CD_TRAN"),
									  pcursor.getJSONObject(i).isNull("NM_SUB_OPER") ? "" : pcursor.getJSONObject(i).getString("NM_SUB_OPER"),
										pcursor.getJSONObject(i).isNull("NM_OPER") ? "" : pcursor.getJSONObject(i).getString("NM_OPER"))
						);

				
		} catch (Exception e) {
			e.printStackTrace();
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}			
	}
	
	public void carregaComboOperAux(Integer intContestacao) {
		this.objRsOperAux = new ArrayList<OperAuxModel>();
		try {
			JSONObject json = new JSONObject();
			json = new JSONObject(cadOsCanais.consultarOperacaoAux(this.strNrCanal, intContestacao));			
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			for (int i = 0; i < pcursor.length(); i++) 
				this.objRsOperAux.add(
						new OperAuxModel(pcursor.getJSONObject(i).isNull("CD_PROD_AUXI") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("CD_PROD_AUXI")),
									  pcursor.getJSONObject(i).isNull("NM_PROD_AUXI") ? "" : pcursor.getJSONObject(i).getString("NM_PROD_AUXI"))
						);

			System.out.println("objRsOperAux "+this.objRsOperAux.size());
				
		} catch (Exception e) {
			e.printStackTrace();
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}					
	}
	
	public void handleIsCredito(AjaxBehaviorEvent abe) {
		this.isCredito = !this.isCredito;
		if (this.isCredito) {
			carregaComboOper(2);
			carregaComboOperAux(2);
		} 
		else {
			carregaComboOper(1);
			carregaComboOperAux(1);
		}
	}
	
	public void atualizarValores() {
		double valDbt = 0;
		double valCrdt = 0;
		
		for (int i = 0; i < objCanais.size(); i++) {
			System.out.println("CNTE: " + objCanais.get(i).getTP_CNTE());
			if (objCanais.get(i).getTP_CNTE() != null) {
				if (objCanais.get(i).getTP_CNTE().equals("2")) {
					if (objCanais.get(i).getStrVlOper() == null || objCanais.get(i).getStrVlOper().equals("")) {
						valCrdt += 0;
					} else {
						valCrdt += Double.parseDouble(objCanais.get(i).getStrVlOper().toString().replace(".", "").replace(",", "."));
					}
					
				} else {
					if (objCanais.get(i).getStrVlOper() == null || objCanais.get(i).getStrVlOper().equals("")) {
						valDbt += 0;
					} else {
						valDbt += Double.parseDouble(objCanais.get(i).getStrVlOper().toString().replace(".", "").replace(",", "."));
					}
				}
			}
		}
		
		this.intQntValCrdt = valCrdt;
		this.intQntValDbto = valDbt;
	}
	
	public void adicionarProduto(String tipoProduto) {
		//1-Corp 	2-Aux
				
		if(tipoProduto.isEmpty()) {
			tipoProduto = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("tipoProduto").toString();
		}
		
		String intTpContestacao = "";
		String strTipoContestacao = "";
		
		String strDescOperacao;
		
		if(tipoProduto.equals("1") || tipoProduto.equals("2")) {
			if(tipoProduto.equals("1") && this.selectedOperCorp.isEmpty()){
				RequestContext.getCurrentInstance().execute("alert('Selecione uma Operação')");
			} else if (tipoProduto.equals("2") && this.selectedOperAux.isEmpty()) {
				//alert
				RequestContext.getCurrentInstance().execute("alert('Selecione uma Operação')");
			} else {
				
				try {
					
					if(tipoProduto.equals("1")) {					
						
						strDescOperacao = this.objRsOperCorp
												.stream()
												.filter(item -> item.getNR_SEQU_TIPO_OPER_TRAN().equals(this.selectedOperCorp))
												.collect(Collectors.toList())
												.get(0).getLabel().substring(7);
						
						System.out.println("DESCOPERACAO ->" + strDescOperacao);
					} else {					
						strDescOperacao = this.objRsOperAux
												.stream()
												.filter(item -> item.getCD_PROD_AUXI().equals(this.selectedOperAux))
												.collect(Collectors.toList())
												.get(0).getNM_PROD_AUXI();
					}
	
					// Checked
					if (this.isCredito) {
						intTpContestacao = "2"; // Credito
						strTipoContestacao = "Crédito";
					} else {
						strTipoContestacao = "Débito";
						intTpContestacao = "1"; // Debito
					}
					
					
					CadastroOsSLinhaObjCanais canaisModel = new CadastroOsSLinhaObjCanais();
					
					JSONObject json = new JSONObject();
					System.out.println(strNrSeqDetalhe +","+ this.selectedOperCorp+","+ this.selectedOperAux+","+ "1"+","+ intTpContestacao+","+ this.objRsCanal.getCD_CENT_CUST_ORIG()+","+ this.objRsCanal.getCD_CENT_CUST_DEST()+","+ this.objRsCanal.getCD_CENT_CUST_OPNT()+","+ this.objRsCanal.getNR_QNTD_EVEN());
					
					if(tipoProduto.equals("1")) {
						json = new JSONObject(cadOsCanais.inserirOperacaoRetSeqTranFrau(strNrSeqDetalhe, this.selectedOperCorp, "", "1", intTpContestacao, this.objRsCanal.getCD_CENT_CUST_ORIG(), this.objRsCanal.getCD_CENT_CUST_DEST() , this.objRsCanal.getCD_CENT_CUST_OPNT(), this.objRsCanal.getNR_QNTD_EVEN()));
					}else {
						json = new JSONObject(cadOsCanais.inserirOperacaoRetSeqTranFrau(strNrSeqDetalhe, "", this.selectedOperAux, "2", intTpContestacao, this.objRsCanal.getCD_CENT_CUST_ORIG(), this.objRsCanal.getCD_CENT_CUST_DEST() , this.objRsCanal.getCD_CENT_CUST_OPNT(), this.objRsCanal.getNR_QNTD_EVEN()));
					}
					
					JSONArray pcursor = json.getJSONArray("PCURSOR");
					JSONObject item = pcursor.getJSONObject(0);
					
					if (this.selectRdoContestacao.equals("1")) {
						this.intQntLancDbto += 1;
					} else if (this.selectRdoContestacao.equals("2")) {
						this.intQntLancCrdt += 1;
					}
					
					canaisModel.setNR_SEQU_TRAN_FRAU_CNAL(item.get("NR_SEQU_TRAN_FRAU_CNAL").toString());
					canaisModel.setDESC_TRAN(strDescOperacao);
					canaisModel.setDESC_CNTE(strTipoContestacao);
					canaisModel.setTP_CNTE(intTpContestacao);
					
					objCanais.add(canaisModel);
					this.atualizarValores();
					System.out.println("CLEAR RENDER IDS AQUI");
					FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("frmDetalheSuperLinha:tbInserirOp");
					
				} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsCanais.WebServiceException  e) {
					e.printStackTrace();
					try {
						FacesContext.getCurrentInstance().getExternalContext()
								.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
		}else {
			salvarOperacao();
		}
		
	}
	
	
	public void excluirOperacao() {
		String nrSeqFrauCanal = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqTranFrau");
		int index = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("index"));
		String descCnte = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("desc_cnte").toString();
				
		try {
			cadOsCanais.excluirOperacao(nrSeqFrauCanal);
			
			if (descCnte.equals("Débito") && this.intQntLancDbto >= 0) {
				this.intQntLancDbto -= 1;
			} else if (descCnte.equals("Crédito") && this.intQntLancCrdt >= 0) {
				this.intQntLancCrdt -= 1;
			}
			
			//objCanais.clear();
			objCanais.remove(index);
			this.atualizarValores();
		} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsCanais.WebServiceException e) {
			e.printStackTrace();
		}
	}
		
	public void verifUnidadeEnvolvCPV() {
		try {
			String tipoProduto = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("tipoProduto").toString();
			
			
			if (!objRsCanal.getCD_CENT_CUST_ORIG().equals("6494")) {
				JSONObject json = new JSONObject();
				json = new JSONObject(cadOsUnidEnvolv.fnSelPVNr("1", objRsCanal.getCD_CENT_CUST_ORIG(), ""));
				JSONArray pcursor = json.getJSONArray("PCURSOR");
				
				if(pcursor.length() <= 0) {
					RequestContext.getCurrentInstance().execute("confirmarCentroOrigem("+tipoProduto+");");
				}else {
					//ret = true;
					verifUnidadeEnvolvCPVDest(tipoProduto);
				}
			}else {
				verifUnidadeEnvolvCPVDest(tipoProduto);
			}
			
			
		} catch (JSONException | com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsUnidEnvol.WebServiceException e) {
			e.printStackTrace();
		}
	}
	
	public void verifUnidadeEnvolvCPVDest(String tipoProduto) {
		try {
			
			if(tipoProduto.isEmpty()) {
				tipoProduto = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("tipoProduto").toString();
			}
			
			if(!this.objRsCanal.getCD_CENT_CUST_DEST().equals("6494")) {
				
			
				JSONObject json = new JSONObject();
				json = new JSONObject(cadOsUnidEnvolv.fnSelPVNr("1", this.objRsCanal.getCD_CENT_CUST_DEST(), ""));
				JSONArray pcursor = json.getJSONArray("PCURSOR");
				if (pcursor.length() <= 0) {
					this.destExists = false;
					RequestContext.getCurrentInstance().execute("confirmarCentroDest("+tipoProduto+");");
				} else {
					adicionarProduto(tipoProduto);
				}
				
			
			}else {
				adicionarProduto(tipoProduto);
			}
			
		} catch (JSONException | com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsUnidEnvol.WebServiceException e) {
			e.printStackTrace();
		}
	}
	
	public void fnSalvaDados() {
		System.out.println("fnSalvaDados---");
		String resposta = "";
		
		String strDados;
		String strInCrtoSeg; //objRsCanal.IN_CRTO_SEGR
		String strInBloqTelf; //objRsCanal.IN_BLOQ_TELF
		String strItssComl; //chkItssComl
		String intTpConta = "0"; //objRsCanal.getTP_CNTA_BNCR()
		String strContaIP = "";

		try {
			FillSalvarContaIP();
		
			int intI = iBanking.size();
			
			if (this.isChkItssComl())
				strItssComl = "1";
			else
				strItssComl = "0";
			
			if(intI  > 0) {
				
				if(intI == 1) {
					strContaIP = iBanking.get(0).getNR_CNTA_ACES();
				}else {
					for (int i = 0; i < iBanking.size(); i++) {
						strContaIP = strContaIP +iBanking.get(0).getNR_CNTA_ACES()+ "|";
					}
				}
				
			}

			System.out.println(
					this.strNrSeqDetalhe +" ,"+
					objRsCanal.getDT_FRAU() + " ," +
					objRsCanal.getStrTelClieDDD() + " ," +
					objRsCanal.getIN_BLOQ_TELF() + " ," +
					objRsCanal.getDT_BLOQ_TELF() + " ," +
					objRsCanal.getHR_BLOQ_TELF() + " ," +
					
					objRsCanal.getDT_SOLI_ASSN_ELET() + " ," +
					objRsCanal.getStrAssTelSolicDDD() + " ," +
					objRsCanal.getHR_SOLI_ASSN_ELET() + " ," +
					objRsCanal.getDT_CADR_ASSN_ELET() + " ," +
					objRsCanal.getStrAssTelCadDDD() + " ," +
					objRsCanal.getHR_CADR_ASSN_ELET() + " ," +
					
					objRsCanal.getNR_CNTA() + " ," +
					objRsCanal.getTP_CNTA_BNCR() + " ," +
					objRsCanal.getDT_ABER_CNTA_BNCR() + " ," +
					objRsCanal.getNM_GERE_RESP_CNTA_BNCR() + " ," +
					objRsCanal.getIN_CRTO_SEGR()+ " ," +
					objRsCanal.getDT_SOLI_CRTO_SEGR()+ " ," +
					objRsCanal.getDT_CANC_CRTO_SEGR()+ " ," +
					objRsCanal.getDT_ATIV_CRTO_SEGR()+ " ," +
					objRsCanal.getDT_PRMR_FRAU()+ " ," +
					
					objRsCanal.getNR_SEQU_CNTA_BNCR()+ " ," +
					objRsCanal.getNR_SEQU_CRTO()+ " ," +
					objRsCanal.getNR_SEQU_FRAU_EVEN()+ " ," +
					objRsCanal.getNR_SEQU_FRAU_CNAL()+ " ," +
					objRsCanal.getCD_CNAL()+ " ," +
					strItssComl+ " ," +
					//3
					objRsCanal.getCD_BANC()+ " ," +
					objRsCanal.getNM_SITU_CNTA_BNCR()+ " ," +
					objRsCanal.getNM_LOG_RECU_SOLI()+ " ," +
					objRsCanal.getNM_ENDE_ENVI()+ " ," +
					objRsCanal.getDT_ATIV()+ " ," +
					objRsCanal.getDT_EMIS()+ " ," +
					objRsCanal.getDT_CANC()+ " ," +
					objRsCanal.getNR_SEQU_PARP_PROC()+ " ," +
					objRsCanal.getNR_CPF_CNPJ_TITL()+ " ," +
					strContaIP+ " ," +
					objRsCanal.getNR_QNTD_EVEN()
				);
			
			System.out.println("formatação data");
			
			String dt_FRAUStr = "";
			String dt_BLOQ_TELFStr = "";
			String dt_SOLI_ASSN_ELETStr = "";
			String dt_CADR_ASSN_ELETStr = "";
			
			if(!objRsCanal.getDT_FRAU().isEmpty()) {
				Date dt_FRAU = new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyy", Locale.US).parse(objRsCanal.getDT_FRAU());
				dt_FRAUStr = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(dt_FRAU);
			}
			
			if(!objRsCanal.getDT_BLOQ_TELF().isEmpty()) {
				Date dt_BLOQ_TELF = new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyy", Locale.US).parse(objRsCanal.getDT_BLOQ_TELF());
				dt_BLOQ_TELFStr = new SimpleDateFormat("yyyy-MM-dd").format(dt_BLOQ_TELF);
			}
			
			if(!objRsCanal.getDT_SOLI_ASSN_ELET().isEmpty()) {
				Date dt_SOLI_ASSN_ELET = new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyy", Locale.US).parse(objRsCanal.getDT_SOLI_ASSN_ELET());
				dt_SOLI_ASSN_ELETStr = new SimpleDateFormat("yyyy-MM-dd").format(dt_SOLI_ASSN_ELET);
			}
			
			if(!objRsCanal.getDT_CADR_ASSN_ELET().isEmpty()) {
				Date dt_CADR_ASSN_ELET = new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyy", Locale.US).parse(objRsCanal.getDT_CADR_ASSN_ELET());
				dt_CADR_ASSN_ELETStr = new SimpleDateFormat("yyyy-MM-dd").format(dt_CADR_ASSN_ELET);
			}
			
			System.out.println("fimm formatação data");
			
			System.out.println("parametros gravarFraudeCanalDetalhe abaixo::::: ----------");
			System.out.println(
					this.strNrSeqDetalhe+ " ," +
					(objRsCanal.getNR_SEQU_CNTA_BNCR().isEmpty()? "0" : objRsCanal.getNR_SEQU_CNTA_BNCR())+ " ," +
					(objRsCanal.getNR_SEQU_CRTO().isEmpty()? "0"	: objRsCanal.getNR_SEQU_CRTO())+ " ," +
					objRsCanal.getCD_CNAL()+ " ," +
					""+ " ," +//null
					""+ " ," +//null
					""+ " ," +//null
					(objRsCanal.getDT_FRAU().isEmpty()? "" : dt_FRAUStr)+ " ," +
					""+ " ," +//null
					""+ " ," +//null
					""+ " ," +//null
					""+ " ," +//null
					""+ " ," +//null
					""+ " ," +
					""+ " ," +
					""+ " ," +//null
					""+ " ," +//null
					""+ " ," +
					""+ " ," +//null
					""+ " ," +//null
					""+ " ," +//null
					""+ " ," +//null
					""+ " ," +//null
					""+ " ," +
					""+ " ," +//null
					""+ " ," +//null
					""+ " ," +//null
					objRsCanal.getStrTelClieDDD().concat(objRsCanal.getNR_TELF_CLIE()).replace("-", "")+ " ," +//verificar ddd e telefone
					objRsCanal.getIN_BLOQ_TELF()+ " ," +
					(objRsCanal.getDT_BLOQ_TELF().isEmpty() || objRsCanal.getHR_BLOQ_TELF().isEmpty()? "" : dt_BLOQ_TELFStr +" "+ objRsCanal.getHR_BLOQ_TELF())+ " ," +
					(objRsCanal.getDT_SOLI_ASSN_ELET().isEmpty() || objRsCanal.getHR_SOLI_ASSN_ELET().isEmpty()? "" : dt_SOLI_ASSN_ELETStr+" "+objRsCanal.getHR_SOLI_ASSN_ELET())+ " ," +
					objRsCanal.getStrAssTelSolicDDD().concat(objRsCanal.getNR_TELF_SOLI_ASSN_ELET()).replace("-", "")+ " ," +//verificar ddd e telefone
					(objRsCanal.getDT_CADR_ASSN_ELET().isEmpty() || objRsCanal.getHR_CADR_ASSN_ELET().isEmpty() ? "" : dt_CADR_ASSN_ELETStr+" "+objRsCanal.getHR_CADR_ASSN_ELET())+ " ," +
					objRsCanal.getStrAssTelCadDDD().replace("-", "")+ " ," +//verificar ddd e telefone
					objRsCanal.getNR_SEQU_FRAU_EVEN()+ " ," +
					strItssComl+ " ," +
					objRsCanal.getNR_QNTD_EVEN());
					
			resposta = cadOsEventos.gravarFraudeCanalDetalhe(
					this.strNrSeqDetalhe,
					objRsCanal.getNR_SEQU_CNTA_BNCR().isEmpty()? "0" : objRsCanal.getNR_SEQU_CNTA_BNCR(),
					objRsCanal.getNR_SEQU_CRTO().isEmpty()? "0"	: objRsCanal.getNR_SEQU_CRTO(),
					objRsCanal.getCD_CNAL(),
					"",
					"",
					"", 
					objRsCanal.getDT_FRAU().isEmpty()? "" : dt_FRAUStr, 
					"", 
					"", 
					"", 
					"", 
					"", 
					"",
					"",
					"",
					"",
					"",
					"",
					"",
					"",
					"",
					"",
					"",
					"",
					"",
					"",
					objRsCanal.getStrTelClieDDD().concat(objRsCanal.getNR_TELF_CLIE()).replace("-", ""),//verificar ddd e telefone
					objRsCanal.getIN_BLOQ_TELF(),
					objRsCanal.getDT_BLOQ_TELF().isEmpty() || objRsCanal.getHR_BLOQ_TELF().isEmpty()? "" : dt_BLOQ_TELFStr +" "+ objRsCanal.getHR_BLOQ_TELF()+":00",
					objRsCanal.getDT_SOLI_ASSN_ELET().isEmpty() || objRsCanal.getHR_SOLI_ASSN_ELET().isEmpty()? "" : dt_SOLI_ASSN_ELETStr+" "+objRsCanal.getHR_SOLI_ASSN_ELET()+":00",
					objRsCanal.getStrAssTelSolicDDD().concat(objRsCanal.getNR_TELF_SOLI_ASSN_ELET()).replace("-", ""),//verificar ddd e telefone,
					objRsCanal.getDT_CADR_ASSN_ELET().isEmpty() || objRsCanal.getHR_CADR_ASSN_ELET().isEmpty() ? "" : dt_CADR_ASSN_ELETStr+" "+objRsCanal.getHR_CADR_ASSN_ELET()+":00",
					objRsCanal.getStrAssTelCadDDD().replace("-", ""),//verificar ddd e telefone,
					objRsCanal.getNR_SEQU_FRAU_EVEN(),
					strItssComl,
					//"a", FAIL
					objRsCanal.getNR_QNTD_EVEN()); 
			
			
				if(!resposta.equals("True")) {
					System.out.println("executa o atualizar *************");
					
					String dt_PRMR_FRAUStr = "";
					String dt_ABER_CNTA_BNCRStr = "";
					String dt_SOLI_CRTO_SEGRStr = "";
					String dt_ATIV_CRTO_SEGRStr = "";
					String dt_CANC_CRTO_SEGRStr = "";
					String dt_EMISStr = "";
					String dt_ATIVStr = "";
					String dt_CANCStr = "";
					
					if(!objRsCanal.getDT_PRMR_FRAU().isEmpty()) {
						Date dt_PRMR_FRAU = new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyy", Locale.US).parse(objRsCanal.getDT_PRMR_FRAU());
						dt_PRMR_FRAUStr = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(dt_PRMR_FRAU);  
					}
					
					if(!objRsCanal.getDT_ABER_CNTA_BNCR().isEmpty()) {
						Date dt_ABER_CNTA_BNCR = new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyy", Locale.US).parse(objRsCanal.getDT_ABER_CNTA_BNCR());
						dt_ABER_CNTA_BNCRStr = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(dt_ABER_CNTA_BNCR);
					}
					
					if(!objRsCanal.getDT_SOLI_CRTO_SEGR().isEmpty()) {
						Date dt_SOLI_CRTO_SEGR = new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyy", Locale.US).parse(objRsCanal.getDT_SOLI_CRTO_SEGR());
						dt_SOLI_CRTO_SEGRStr = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(dt_SOLI_CRTO_SEGR);  
					}
					
					if(!objRsCanal.getDT_ATIV_CRTO_SEGR().isEmpty()) {
						Date dt_ATIV_CRTO_SEGR = new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyy", Locale.US).parse(objRsCanal.getDT_ATIV_CRTO_SEGR());
						dt_ATIV_CRTO_SEGRStr = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(dt_ATIV_CRTO_SEGR);  
					}
					
					if(!objRsCanal.getDT_CANC_CRTO_SEGR().isEmpty()) {
						Date dt_CANC_CRTO_SEGR = new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyy", Locale.US).parse(objRsCanal.getDT_CANC_CRTO_SEGR());
						dt_CANC_CRTO_SEGRStr = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(dt_CANC_CRTO_SEGR);
					}
					
					if(!objRsCanal.getDT_EMIS().isEmpty()) {
						Date dt_EMIS = new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyy", Locale.US).parse(objRsCanal.getDT_EMIS());
						dt_EMISStr = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(dt_EMIS);
					}
					
					if(!objRsCanal.getDT_ATIV().isEmpty()) {
						Date dt_ATIV = new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyy", Locale.US).parse(objRsCanal.getDT_ATIV());
						dt_ATIVStr = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(dt_ATIV);
					}
					
					if(!objRsCanal.getDT_CANC().isEmpty()) {
						Date dt_CANC = new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyy", Locale.US).parse(objRsCanal.getDT_CANC());
						dt_CANCStr = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(dt_CANC);
					}
					

					
					System.out.println(
							(objRsCanal.getNR_SEQU_PARP_PROC().isEmpty()? "" : objRsCanal.getNR_SEQU_PARP_PROC())+ " ," +
							objRsCanal.getCD_CNAL()+ " ," +
							(objRsCanal.getNR_SEQU_FRAU_EVEN().isEmpty()? "0": objRsCanal.getNR_SEQU_FRAU_EVEN())+ " ," +//double
							(objRsCanal.getNR_SEQU_FRAU_CNAL().isEmpty()? "0" : objRsCanal.getNR_SEQU_FRAU_CNAL())+ " ," +
							(objRsCanal.getNR_SEQU_CRTO().isEmpty()? "0"	: objRsCanal.getNR_SEQU_CRTO())+ " ," +
							(objRsCanal.getNR_SEQU_CNTA_BNCR().isEmpty()? "0" : objRsCanal.getNR_SEQU_CNTA_BNCR())+ " ," +
							"0"+ " ," +
							objRsCanal.getNR_CPF_CNPJ_TITL()+ " ," +
							(objRsCanal.getNR_CPF_CNPJ_TITL().length() > 11? "2": "1")+ " ," +
							(objRsCanal.getDT_EMIS().isEmpty()? "" : dt_EMISStr)+ " ," +
							(objRsCanal.getDT_ATIV().isEmpty()? "" : dt_ATIVStr)+ " ," +
							(objRsCanal.getDT_CANC().isEmpty()? "" : dt_CANCStr)+ " ," +
							objRsCanal.getNM_LOG_RECU_SOLI()+ " ," +
							objRsCanal.getNM_ENDE_ENVI()+ " ," +
							""+ " ," +
							(objRsCanal.getCD_BANC().isEmpty()? "0" : objRsCanal.getCD_BANC())+ " ," +
							(objRsCanal.getNR_CNTA().isEmpty()? "0" : objRsCanal.getNR_CNTA())+ " ," +
							(dt_PRMR_FRAUStr)+ " ," +
							(objRsCanal.getIN_CRTO_SEGR().isEmpty()? "0" : objRsCanal.getIN_CRTO_SEGR())+ " ," +
							(objRsCanal.getTP_CNTA_BNCR().isEmpty()? "0" : objRsCanal.getTP_CNTA_BNCR())+ " ," +
							(dt_ABER_CNTA_BNCRStr)+ " ," +
							objRsCanal.getNM_GERE_RESP_CNTA_BNCR()+ " ," +
							objRsCanal.getNM_SITU_CNTA_BNCR()+ " ," +
							(dt_SOLI_CRTO_SEGRStr)+ " ," +
							(dt_ATIV_CRTO_SEGRStr)+ " ," +
							(dt_CANC_CRTO_SEGRStr)	
					);
					
					 		
							
					resposta = cadOsEventos.atualizarCartaoParticipante(
						objRsCanal.getNR_SEQU_PARP_PROC().isEmpty()? "" : objRsCanal.getNR_SEQU_PARP_PROC(),
						objRsCanal.getCD_CNAL(),
						objRsCanal.getNR_SEQU_FRAU_EVEN().isEmpty()? "0": objRsCanal.getNR_SEQU_FRAU_EVEN(),//double
						objRsCanal.getNR_SEQU_FRAU_CNAL().isEmpty()? "0" : objRsCanal.getNR_SEQU_FRAU_CNAL(),
						objRsCanal.getNR_SEQU_CRTO().isEmpty()? "0"	: objRsCanal.getNR_SEQU_CRTO(),
						objRsCanal.getNR_SEQU_CNTA_BNCR().isEmpty()? "0" : objRsCanal.getNR_SEQU_CNTA_BNCR(),
						"0",
						objRsCanal.getNR_CPF_CNPJ_TITL(), 
						objRsCanal.getNR_CPF_CNPJ_TITL().length() > 11? "2": "1",
						objRsCanal.getDT_EMIS().isEmpty()? "" : dt_EMISStr,
						objRsCanal.getDT_ATIV().isEmpty()? "" : dt_ATIVStr,
						objRsCanal.getDT_CANC().isEmpty()? "" : dt_CANCStr,
						objRsCanal.getNM_LOG_RECU_SOLI(),
						objRsCanal.getNM_ENDE_ENVI(),
						"0",
						objRsCanal.getCD_BANC().isEmpty()? "0" : objRsCanal.getCD_BANC(),
						objRsCanal.getNR_CNTA().isEmpty()? "0" : objRsCanal.getNR_CNTA(),
						objRsCanal.getDT_PRMR_FRAU().isEmpty()? "" : dt_PRMR_FRAUStr,
						objRsCanal.getIN_CRTO_SEGR().isEmpty()? "0" : objRsCanal.getIN_CRTO_SEGR(),
						objRsCanal.getTP_CNTA_BNCR().isEmpty()? "0" : objRsCanal.getTP_CNTA_BNCR(),
						objRsCanal.getDT_ABER_CNTA_BNCR().isEmpty()? "" : dt_ABER_CNTA_BNCRStr,
						objRsCanal.getNM_GERE_RESP_CNTA_BNCR(),
						objRsCanal.getNM_SITU_CNTA_BNCR(),
						objRsCanal.getDT_SOLI_CRTO_SEGR().isEmpty()? "" : dt_SOLI_CRTO_SEGRStr,
						objRsCanal.getDT_ATIV_CRTO_SEGR().isEmpty()? "" : dt_ATIV_CRTO_SEGRStr,
						objRsCanal.getDT_CANC_CRTO_SEGR().isEmpty()? "" : dt_CANC_CRTO_SEGRStr
					);
			     }
			      
								
				if(!resposta.equals("True")) {
                    RequestContext.getCurrentInstance().execute("VerErro('"+resposta+"')");
                    return;
				}
				
				System.out.println("chegou aqui");
				RequestContext.getCurrentInstance().execute("alert('Detalhe Salvo com sucesso.')");
				
		
		} catch (Exception e) {
		     // perguntar se quer visualizar erro
		     if(resposta.isEmpty()) {
		            resposta = e.getMessage();
		     }
		     RequestContext.getCurrentInstance().execute("VerErro("+resposta+")");                   
		}

	}
	
    // INC 
    public void salvarOperacao() {
    	System.out.println("salvarOperacao -----||||");
            try {
                    for(CadastroOsSLinhaObjCanais canal : this.objCanais) {
                           cadOsCanais.salvarOperacao(canal.getNR_SEQU_TRAN_FRAU_CNAL(), 
                                                                             canal.getStrDtOper(), 
                                                                             canal.getStrHrOper(), 
                                                                              canal.getStrVlOper().replace(".", "").replace(",", "."), 
                                                                              objRsCanal.getCD_CENT_CUST_ORIG(),
                                                                              objRsCanal.getCD_CENT_CUST_DEST(),
                                                                              objRsCanal.getCD_CENT_CUST_OPNT(),
                                                                              objRsCanal.getNR_QNTD_EVEN()
                                                                             );
                    }
                    
                    //Consultar de novo
                    this.consultarOperacaoCanal();
                    atualizarValores();
                    FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("frmDetalheSuperLinha:tbInserirOp");
            } catch (Exception e) {
                    e.printStackTrace();
                    try {
                           FacesContext.getCurrentInstance().getExternalContext()
                                           .redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
                    } catch (IOException e1) {
                           // TODO Auto-generated catch block
                           e1.printStackTrace();
                    }
            }               
    }
	
	private void FillSalvarContaIP() {
		try {
			System.out.println("FillSalvarContaIP -----!!");

			for (int i = 0; i < iBanking.size(); i++) {
				System.out.println(Long.parseLong(this.strNrSeqDetalhe)+","+ Long.parseLong(iBanking.get(i).getNR_CNTA_ACES())+ " ---- " + i);
				cadOsEventos.gravarContaIBanking(Long.parseLong(this.strNrSeqDetalhe), Long.parseLong(iBanking.get(i).getNR_CNTA_ACES()));
			}
			System.out.println("passou aqui");
			consultarContasAcessadasTelefone();
			
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	
	
	public void handleChangeShowDatas(AjaxBehaviorEvent abe) {
		this.showDatas = !this.showDatas;
	}
	
	public void validaDatasPrint(String id) {
		String data = "";
		
		System.out.println("validaDatasPrint: " + id);
		switch(id) {
			
			case "frmDetalheSuperLinha:txtDtFrau_input":
				data = objRsCanal.getDT_FRAU();
				break;
			case "frmDetalheSuperLinha:txtTelDtBloq_input":
				data = objRsCanal.getDT_BLOQ_TELF();
				break;
			case "frmDetalheSuperLinha:txtAssDtSolic_input":
				data = objRsCanal.getDT_SOLI_ASSN_ELET();
				break;
			case "frmDetalheSuperLinha:txtAssDtCadastr_input":
				data = objRsCanal.getDT_CADR_ASSN_ELET();
				break;
			case "frmDetalheSuperLinha:txtDadIniDtAtiv_input":
				data = objRsCanal.getDT_ATIV();
				break;
			case "frmDetalheSuperLinha:txtDadIniDtEmis_input":
				data = objRsCanal.getDT_EMIS();
				break;
			case "frmDetalheSuperLinha:txtDadIniDtCanc_input":
				data = objRsCanal.getDT_CANC();
				break;
			case "frmDetalheSuperLinha:txtExtrDtAber_input":
				data = objRsCanal.getDT_ABER_CNTA_BNCR();
				break;
			case "frmDetalheSuperLinha:txtCartDtSolic_input":
				data = objRsCanal.getDT_SOLI_CRTO_SEGR();
				break;
			case "frmDetalheSuperLinha:txtCartDtCance_input":
				data = objRsCanal.getDT_CANC_CRTO_SEGR();
				break;
			case "frmDetalheSuperLinha:txtCartDtAtiv_input":
				data = objRsCanal.getDT_ATIV_CRTO_SEGR();
				break;
			case "frmDetalheSuperLinha:txtCartDtFrau_input":
				data = objRsCanal.getDT_PRMR_FRAU();
				break;			
		}
		if(!data.contains("/")) {
		
			data = converterData(data);
		}
		 
		validaDatas(id, data);
	}
	
	public String converterData(String dataAconverter) {
		Date dt = null;
		String data = "";
		try {
			dt = new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyy", Locale.US).parse(dataAconverter);
			 data = new SimpleDateFormat("dd/MM/yyyy").format(dt); 
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return data;
		
	}
	
	public void validaDatas(String objId, String objValue) {
		System.out.println("validaDatas : " + objValue);
		String[] arrDtSolicCrto;
		String[] arrDtCancCrto;
		String[] arrDtAtivCrto;
		String[] arrDtFrauCrto;
		

		String[] arrDadIniDtAtiv;
		String[] arrDadIniDtEmissao;
		String[] arrDadIniDtCanc;

		String[] arrDtFrau;
		String[] arrDtBloqueio;

		String[] arrAssDtSolic;
		String[] arrAssDtCadastr;
		
		try {
			
		
			//Data da fraude 
			if (objId.equals("frmDetalheSuperLinha:txtDtFrau_input")) {
				//objRsCanal.DT_FRAU
				arrDtFrau = objValue.split("/");
				
				if(objRsCanal.getDT_BLOQ_TELF().contains("/")) {
					arrDtBloqueio = objRsCanal.getDT_BLOQ_TELF().split("/");
				} else {
					objRsCanal.setDT_BLOQ_TELF(converterData(objRsCanal.getDT_BLOQ_TELF()));
					arrDtBloqueio = objRsCanal.getDT_BLOQ_TELF().split("/"); 
				}
				System.out.println((Integer.valueOf(arrDtFrau[2] + arrDtFrau[1]	+ arrDtFrau[0])) > (Integer.valueOf(arrDtBloqueio[2] + arrDtBloqueio[1] + arrDtBloqueio[0])));
				
				if(arrDtFrau.length > 2 && arrDtBloqueio.length > 2) {
					if((Integer.valueOf(arrDtFrau[2] + arrDtFrau[1]	+ arrDtFrau[0])) > (Integer.valueOf(arrDtBloqueio[2] + arrDtBloqueio[1] + arrDtBloqueio[0]))) {
						RequestContext.getCurrentInstance().execute("alert('Data inválida!\\nA data da fraude deve ser menor ou igual à data de bloqueio!')");
						objRsCanal.setDT_FRAU("");
						FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("frmDetalheSuperLinha:txtDtFrau");
						return;
		
					}
				}
			}
	
			//Data de Ativaçăo
			if (objId.equals("frmDetalheSuperLinha:txtTelDtBloq_input")) {
				//objRsCanal.DT_BLOQ_TELF
				arrDtBloqueio = objValue.split("/");
				
				
				if(objRsCanal.getDT_FRAU().contains("/")) {
					arrDtFrau = objRsCanal.getDT_FRAU().split("/");
				}else {
					objRsCanal.setDT_FRAU(converterData(objRsCanal.getDT_FRAU()));
					arrDtFrau = objRsCanal.getDT_FRAU().split("/");
				}
				
				if(arrDtBloqueio.length > 2 && arrDtFrau.length > 2) {
					if((Integer.valueOf(arrDtBloqueio[2] + arrDtBloqueio[1] + arrDtBloqueio[0])) < (Integer.valueOf(arrDtFrau[2]+ arrDtFrau[1] + arrDtFrau[0]))) {
						RequestContext.getCurrentInstance().execute("alert('Data inválida!\\nA data de bloqueio deve ser maior ou igual à data da fraude!')");
						objRsCanal.setDT_BLOQ_TELF("");
						FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("frmDetalheSuperLinha:txtTelDtBloq");
						return;
					}				
				}
				
			}	
			
			//Data de Ativaçăo	
			if(objId.equals("frmDetalheSuperLinha:txtAssDtSolic_input")) {
				//objRsCanal.DT_SOLI_ASSN_ELET
				arrAssDtSolic = objValue.split("/");
				
				if(objRsCanal.getDT_CADR_ASSN_ELET().contains("/")) {
					arrAssDtCadastr = objRsCanal.getDT_CADR_ASSN_ELET().split("/");
				}else {
					objRsCanal.setDT_CADR_ASSN_ELET(converterData(objRsCanal.getDT_CADR_ASSN_ELET()));
					arrAssDtCadastr = objRsCanal.getDT_CADR_ASSN_ELET().split("/");
				}
				
				if(arrAssDtSolic.length > 2 && arrAssDtCadastr.length > 2) {
					if((Integer.valueOf(arrAssDtSolic[2] + arrAssDtSolic[1]	+ arrAssDtSolic[0])) > (Integer.valueOf(arrAssDtCadastr[2]+ arrAssDtCadastr[1] + arrAssDtCadastr[0]))) {
						RequestContext.getCurrentInstance().execute("alert('Data inválida!\\nA data da solicitaçăo deve ser menor ou igual à data do cadastramento!')");
						objRsCanal.setDT_SOLI_ASSN_ELET("");
						FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("frmDetalheSuperLinha:txtAssDtSolic");
						return;
					}					
				}
			}
			//Data de Ativaçăo 3
			if(objId.equals("frmDetalheSuperLinha:txtAssDtCadastr_input")) {
				//objRsCanal.DT_CADR_ASSN_ELET
				arrAssDtCadastr = objValue.split("/");
				
				if(objRsCanal.getDT_SOLI_ASSN_ELET().contains("/")) {
					arrAssDtSolic = objRsCanal.getDT_SOLI_ASSN_ELET().split("/");
				}else {
					objRsCanal.setDT_SOLI_ASSN_ELET(converterData(objRsCanal.getDT_SOLI_ASSN_ELET()));
					arrAssDtSolic = objRsCanal.getDT_SOLI_ASSN_ELET().split("/");
				}
				
				if(arrAssDtCadastr.length > 2 && arrAssDtSolic.length > 2) {
					if((Integer.valueOf(arrAssDtCadastr[2] + arrAssDtCadastr[1]+ arrAssDtCadastr[0])) < (Integer.valueOf(arrAssDtSolic[2]+ arrAssDtSolic[1] + arrAssDtSolic[0]))) {
						RequestContext.getCurrentInstance().execute("alert('Data inválida!\\nA data do cadastramento deve ser maior ou igual à data da solicitaçăo!')");
						objRsCanal.setDT_CADR_ASSN_ELET("");
						FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("frmDetalheSuperLinha:txtAssDtCadastr");
						return;
					}				
				}
				
			}
			
			//Data de Ativaçăo 4
			if(objId.equals("frmDetalheSuperLinha:txtDadIniDtAtiv_input")) {
				//objRsCanal.DT_ATIV
				arrDadIniDtAtiv = objValue.split("/");
				
				//objRsCanal.DT_EMIS
				if(objRsCanal.getDT_EMIS().contains("/")) {
					arrDadIniDtEmissao = objRsCanal.getDT_EMIS().split("/");
				}else {
					objRsCanal.setDT_EMIS(converterData(objRsCanal.getDT_EMIS())); 
					arrDadIniDtEmissao = objRsCanal.getDT_EMIS().split("/");
				}
				
				//objRsCanal.DT_CANC
				if(objRsCanal.getDT_CANC().contains("/")) {
					arrDadIniDtCanc = objRsCanal.getDT_CANC().split("/");
				}else {
					objRsCanal.setDT_CANC(converterData(objRsCanal.getDT_CANC()));
					arrDadIniDtCanc = objRsCanal.getDT_CANC().split("/");
				}
				
				if(arrDadIniDtAtiv.length > 2 && arrDadIniDtEmissao.length > 2) {
					if((Integer.valueOf(arrDadIniDtAtiv[2] + arrDadIniDtAtiv[1]
							+ arrDadIniDtAtiv[0])) > (Integer.valueOf(arrDadIniDtEmissao[2]
									+ arrDadIniDtEmissao[1] + arrDadIniDtEmissao[0]))) {
						RequestContext.getCurrentInstance().execute("alert('Data inválida!\\nA data de ativaçăo deve ser menor ou igual à data de emissăo!');");
						objRsCanal.setDT_ATIV("");
						FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("frmDetalheSuperLinha:txtDadIniDtAtiv");
						return;
					}
				}
				
				if (arrDadIniDtAtiv.length > 2 && arrDadIniDtCanc.length > 2) {
					if((Integer.valueOf(arrDadIniDtAtiv[2]
							+ arrDadIniDtAtiv[1] + arrDadIniDtAtiv[0])) > (Integer.valueOf(arrDadIniDtCanc[2]
									+ arrDadIniDtCanc[1] + arrDadIniDtCanc[0]))) {
						RequestContext.getCurrentInstance().execute("alert('Data inválida!\\nA data de ativaçăo deve ser menor ou igual à data de cancelamento!')");
						objRsCanal.setDT_ATIV("");
						FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("frmDetalheSuperLinha:txtDadIniDtAtiv");
						return;
					}
				}
			}
			
			//Data de Emissăo
			if(objId.equals("frmDetalheSuperLinha:txtDadIniDtEmis_input")) {
				arrDadIniDtEmissao = objValue.split("/");
				
				//objRsCanal.DT_ATIV
				if(objRsCanal.getDT_ATIV().contains("/")) {
					arrDadIniDtAtiv = objRsCanal.getDT_ATIV().split("/");
				}else {
					objRsCanal.setDT_ATIV(converterData(objRsCanal.getDT_ATIV()));
					arrDadIniDtAtiv = objRsCanal.getDT_ATIV().split("/");
				}
				
				//objRsCanal.DT_CANC
				if(objRsCanal.getDT_CANC().contains("/")) {
					arrDadIniDtCanc = objRsCanal.getDT_CANC().split("/");
				}else {
					objRsCanal.setDT_CANC(converterData(objRsCanal.getDT_CANC()));
					arrDadIniDtCanc = objRsCanal.getDT_CANC().split("/");
				}
				
				if (arrDadIniDtEmissao.length > 2 && arrDadIniDtAtiv.length > 2) {
					if((Integer.valueOf(arrDadIniDtEmissao[2]
							+ arrDadIniDtEmissao[1] + arrDadIniDtEmissao[0])) > (Integer.valueOf(arrDadIniDtAtiv[2]
									+ arrDadIniDtAtiv[1] + arrDadIniDtAtiv[0]))) {
						RequestContext.getCurrentInstance().execute("alert('Data inválida!\\nA data da emissăo deve ser menor ou igual à data de ativaçăo!')");
						objRsCanal.setDT_ATIV("");
						FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("frmDetalheSuperLinha:txtDadIniDtEmis");
						return;
					}			
				}
				
				if (arrDadIniDtEmissao.length > 2 && arrDadIniDtCanc.length > 2) {
					if((Integer.valueOf(arrDadIniDtEmissao[2]
							+ arrDadIniDtEmissao[1]
							+ arrDadIniDtEmissao[0])) > (Integer.valueOf(arrDadIniDtCanc[2]
									+ arrDadIniDtCanc[1] + arrDadIniDtCanc[0]))) {
						RequestContext.getCurrentInstance().execute("alert('Data inválida!\\nA data de emissăo deve ser menor ou igual à data de cancelamento!')");
						objRsCanal.setDT_ATIV("");
						FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("frmDetalheSuperLinha:txtDadIniDtEmis");
						return;
					}
				}			
			}
			
			//Data de Cancelamento
			if(objId.equals("frmDetalheSuperLinha:txtDadIniDtCanc_input")) {
				//objRsCanal.DT_CANC
				arrDadIniDtCanc = objValue.split("/");
				
				//objRsCanal.DT_ATIV
				if(objRsCanal.getDT_ATIV().contains("/")) {
					arrDadIniDtAtiv = objRsCanal.getDT_ATIV().split("/");
				}else {
					objRsCanal.setDT_ATIV(converterData(objRsCanal.getDT_ATIV()));
					arrDadIniDtAtiv = objRsCanal.getDT_ATIV().split("/");
				}
				
				//objRsCanal.DT_EMIS
				if(objRsCanal.getDT_EMIS().contains("/")) {
					arrDadIniDtEmissao = objRsCanal.getDT_EMIS().split("/");
				}else {
					objRsCanal.setDT_EMIS(converterData(objRsCanal.getDT_EMIS())); 
					arrDadIniDtEmissao = objRsCanal.getDT_EMIS().split("/");
				}
				
				if(arrDadIniDtAtiv.length > 2 && arrDadIniDtCanc.length > 2 && arrDadIniDtEmissao.length > 2) {
					if((Integer.valueOf(arrDadIniDtCanc[2]
							+ arrDadIniDtCanc[1] + arrDadIniDtCanc[0])) < (Integer.valueOf(arrDadIniDtEmissao[2]
									+ arrDadIniDtEmissao[1]
									+ arrDadIniDtEmissao[0]))) {
						RequestContext.getCurrentInstance().execute("alert('Data inválida!\\nA data de cancelamento deve ser maior ou igual à data de emissăo!')");
						objRsCanal.setDT_CANC("");
						FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("frmDetalheSuperLinha:txtDadIniDtCanc");
						return;
					}
				}
				
				if(arrDadIniDtEmissao.length > 2 && arrDadIniDtCanc.length > 2 && arrDadIniDtAtiv.length > 2) {
					if((Integer.valueOf(arrDadIniDtCanc[2]
							+ arrDadIniDtCanc[1] + arrDadIniDtCanc[0])) < (Integer.valueOf(arrDadIniDtAtiv[2]
									+ arrDadIniDtAtiv[1] + arrDadIniDtAtiv[0]))) {
						RequestContext.getCurrentInstance().execute("alert('Data inválida!\\nA data de cancelamento deve ser maior ou igual à data de ativaçăo!')");
						objRsCanal.setDT_CANC("");
						FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("frmDetalheSuperLinha:txtDadIniDtCanc");
						return;
					}
				}
				
			}
			
		//-----------
			//Data Solicitação
			if (objId.equals("frmDetalheSuperLinha:txtCartDtSolic_input"))
		    {
					        
		        if(objRsCanal.getDT_ATIV_CRTO_SEGR().contains("/")) {
		        	arrDtAtivCrto = objRsCanal.getDT_ATIV_CRTO_SEGR().split("/");
		                            
		        } else { 
		        	objRsCanal.setDT_ATIV_CRTO_SEGR(converterData(objRsCanal.getDT_ATIV_CRTO_SEGR()));
		        	arrDtAtivCrto = objRsCanal.getDT_ATIV_CRTO_SEGR().split("/");
		        }
		        
		        arrDtSolicCrto = objValue.split("/"); 
		        
		        if((Integer.valueOf(arrDtSolicCrto[2] + arrDtSolicCrto[1] + arrDtSolicCrto[0])) > (Integer.valueOf(arrDtAtivCrto[2] + arrDtAtivCrto[1] + arrDtAtivCrto[0])))
		        {
					RequestContext.getCurrentInstance().execute("alert('Data inválida!\\nA data da solicitação deve ser menor ou igual à data de ativação!')");
					RequestContext.getCurrentInstance().execute("document.getElementById('frmDetalheSuperLinha:txtCartDtSolic_input').value = '' ");
					objRsCanal.setDT_SOLI_CRTO_SEGR("");
		           // obj.focus(); 
		            return;         
		        }
		    }
			
			//Data Ativação
			if (objId.equals("frmDetalheSuperLinha:txtCartDtAtiv_input")) {
				
				//
				System.out.println(objRsCanal.getDT_SOLI_CRTO_SEGR().contains("/") + " contem barra");
				if(objRsCanal.getDT_SOLI_CRTO_SEGR().contains("/")) {
					
					arrDtSolicCrto = objRsCanal.getDT_SOLI_CRTO_SEGR().split("/");
		        
				} else {
					objRsCanal.setDT_SOLI_CRTO_SEGR(converterData(objRsCanal.getDT_SOLI_CRTO_SEGR()));
		        	arrDtSolicCrto = objRsCanal.getDT_SOLI_CRTO_SEGR().split("/");
		        }
				
		        arrDtAtivCrto =  objValue.split("/"); 
		                            
		        
		        if((Integer.valueOf(arrDtAtivCrto[2] + arrDtAtivCrto[1] + arrDtAtivCrto[0])) < (Integer.valueOf(arrDtSolicCrto[2] + arrDtSolicCrto[1] + arrDtSolicCrto[0])))
		        {
					RequestContext.getCurrentInstance().execute("alert('Data inválida!\\nA data de ativação deve ser maior ou igual à data de solicitação!')");
					RequestContext.getCurrentInstance().execute("document.getElementById('frmDetalheSuperLinha:txtCartDtAtiv_input').value = '' ");
					objRsCanal.setDT_ATIV_CRTO_SEGR("");
		           // obj.focus(); 
		            return;         
		        }
		    }
			
			//Data Cancelamento
			
			if (objId.equals("frmDetalheSuperLinha:txtDadIniDtCanc_input"))
		    {
				
				System.out.println(objRsCanal.getDT_ATIV_CRTO_SEGR().contains("/") + " contem " + objRsCanal.getDT_ATIV_CRTO_SEGR());
				if(objRsCanal.getDT_ATIV_CRTO_SEGR().contains("/")) {
		        	arrDtAtivCrto = objRsCanal.getDT_ATIV_CRTO_SEGR().split("/");
		                            
		        } else { 
		        	
		        	objRsCanal.setDT_ATIV_CRTO_SEGR(converterData(objRsCanal.getDT_ATIV_CRTO_SEGR()));
		        	arrDtAtivCrto = objRsCanal.getDT_ATIV_CRTO_SEGR().split("/");
		        }
		        
		        arrDtCancCrto =  objValue.split("/"); 
		                         
		        if((Integer.valueOf(arrDtCancCrto[2] + arrDtCancCrto[1] + arrDtCancCrto[0])) < (Integer.valueOf(arrDtAtivCrto[2] + arrDtAtivCrto[1] + arrDtAtivCrto[0])))
		        {
					RequestContext.getCurrentInstance().execute("alert('Data inválida!\\nA data do cancelamento deve ser maior ou igual à data de ativação!')");
					RequestContext.getCurrentInstance().execute("document.getElementById('frmDetalheSuperLinha:txtCartDtCance_input').value = '' ");
					//objRsCanal.setDT_CANC_CRTO_SEGR("");
		           // obj.focus(); 
					objRsCanal.setDT_CANC("");
		            return;         
		        }
		    }
			
			//Data Fraude
			
			if (objId.equals("frmDetalheSuperLinha:txtCartDtFrau_input"))
		    {
				
				if(objRsCanal.getDT_ATIV_CRTO_SEGR().contains("/")) {
		        	arrDtAtivCrto = objRsCanal.getDT_ATIV_CRTO_SEGR().split("/");
		                            
		        } else { 
		        	objRsCanal.setDT_ATIV_CRTO_SEGR(converterData(objRsCanal.getDT_ATIV_CRTO_SEGR()));
		        	arrDtAtivCrto = objRsCanal.getDT_ATIV_CRTO_SEGR().split("/");
		        }
		        arrDtFrauCrto =  objValue.split("/"); 
		                            
		        if((Integer.valueOf(arrDtFrauCrto[2] + arrDtFrauCrto[1] + arrDtFrauCrto[0])) < (Integer.valueOf(arrDtAtivCrto[2] + arrDtAtivCrto[1] + arrDtAtivCrto[0])))
		        {
					RequestContext.getCurrentInstance().execute("alert('Data inválida!\\nA data da fraude deve ser maior ou igual à data de ativação!')");
					RequestContext.getCurrentInstance().execute("document.getElementById('frmDetalheSuperLinha:txtCartDtFrau_input').value = '' ");
					objRsCanal.setDT_PRMR_FRAU("");
		           // obj.focus(); 
		            return;         
		        }
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void clearBean() {
		String type = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("pageType").toString();
		String nrSeq = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("nrSeq").toString();
		switch(type) {
			case "FE":
				System.out.println("nrSeq - "+nrSeq+" - FE "+FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("CadastroOsDetalheProdutoFEBean"));
				if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("CadastroOsDetalheProdutoFEBean") != null)
					FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CadastroOsDetalheProdutoFEBean");
				RequestContext.getCurrentInstance().execute("document.getElementById('modal-detalhe-produto-fo-fe').src = 'hyb_cadastroos_detalheprodutofe.xhtml?strNrSeqTranFrau="+nrSeq+"'");
				RequestContext.getCurrentInstance().execute("PF('detalhe-produto-fo-fe').show();");
				break;
			
			case "FO":
				System.out.println("nrSeq - "+nrSeq+" - FO "+FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("CadastroOsDetalheProdutoFOBean"));
				if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("CadastroOsDetalheProdutoFOBean") != null)
					FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CadastroOsDetalheProdutoFOBean");
				RequestContext.getCurrentInstance().execute("document.getElementById('modal-detalhe-produto-fo-fe').src = 'hyb_cadastroos_detalheprodutofo.xhtml?strNrSeqTranFrau="+nrSeq+"'");
				RequestContext.getCurrentInstance().execute("PF('detalhe-produto-fo-fe').show();");
				break;
			
			default:
				System.out.println("clearBean - não encontrado");
				break;
		}
	}
	
	public XHYCadOsEventosEndPoint getCadOsEventos() {
		return cadOsEventos;
	}

	public void setCadOsEventos(XHYCadOsEventosEndPoint cadOsEventos) {
		this.cadOsEventos = cadOsEventos;
	}

	public CadastroOsSLinhaObjRsCanal getObjRsCanal() {
		return objRsCanal;
	}

	public void setObjRsCanal(CadastroOsSLinhaObjRsCanal objRsCanal) {
		this.objRsCanal = objRsCanal;
	}

	public CadastroOsSLinhaObjRs getObjRs() {
		return objRs;
	}

	public void setObjRs(CadastroOsSLinhaObjRs objRs) {
		this.objRs = objRs;
	}

	public String getStrNrCanal() {
		return strNrCanal;
	}

	public void setStrNrCanal(String strNrCanal) {
		this.strNrCanal = strNrCanal;
	}

	public String getStrNrEvento() {
		return strNrEvento;
	}

	public void setStrNrEvento(String strNrEvento) {
		this.strNrEvento = strNrEvento;
	}

	public String getStrNrSeqDetalhe() {
		return strNrSeqDetalhe;
	}

	public void setStrNrSeqDetalhe(String strNrSeqDetalhe) {
		this.strNrSeqDetalhe = strNrSeqDetalhe;
	}

	public String getStrNrSeqId() {
		return strNrSeqId;
	}

	public void setStrNrSeqId(String strNrSeqId) {
		this.strNrSeqId = strNrSeqId;
	}

	public String getTxtDDDTelfAcessCnta() {
		return txtDDDTelfAcessCnta;
	}

	public void setTxtDDDTelfAcessCnta(String txtDDDTelfAcessCnta) {
		this.txtDDDTelfAcessCnta = txtDDDTelfAcessCnta;
	}

	public String getTxtTelfAcessCnta() {
		return txtTelfAcessCnta;
	}

	public void setTxtTelfAcessCnta(String txtTelfAcessCnta) {
		this.txtTelfAcessCnta = txtTelfAcessCnta;
	}

	public boolean isChkItssComl() {
		return chkItssComl;
	}

	public void setChkItssComl(boolean chkItssComl) {
		this.chkItssComl = chkItssComl;
	}

	public List<CadastroOsSLinhaIBanking> getiBanking() {
		return iBanking;
	}

	public void setiBanking(List<CadastroOsSLinhaIBanking> iBanking) {
		this.iBanking = iBanking;
	}

	public List<CadastroOsSLinhaTelfAcess> getTelfAcessCnta() {
		return telfAcessCnta;
	}

	public void setTelfAcessCnta(List<CadastroOsSLinhaTelfAcess> telfAcessCnta) {
		this.telfAcessCnta = telfAcessCnta;
	}

	public boolean isShowDatas() {
		return showDatas;
	}

	public void setShowDatas(boolean showDatas) {
		this.showDatas = showDatas;
	}

	public XHYCadOsCanaisEndPoint getCadOsCanais() {
		return cadOsCanais;
	}

	public void setCadOsCanais(XHYCadOsCanaisEndPoint cadOsCanais) {
		this.cadOsCanais = cadOsCanais;
	}

	public List<CadastroOsSLinhaObjCanais> getObjCanais() {
		return objCanais;
	}

	public void setObjCanais(List<CadastroOsSLinhaObjCanais> objCanais) {
		this.objCanais = objCanais;
	}

	public Locale getPtBR() {
		return ptBR;
	}

	public void setPtBR(Locale ptBR) {
		this.ptBR = ptBR;
	}

	public int getIntQntLancDbto() {
		return intQntLancDbto;
	}

	public void setIntQntLancDbto(int intQntLancDbto) {
		this.intQntLancDbto = intQntLancDbto;
	}

	public int getIbtQntLancCrdt() {
		return ibtQntLancCrdt;
	}

	public void setIbtQntLancCrdt(int ibtQntLancCrdt) {
		this.ibtQntLancCrdt = ibtQntLancCrdt;
	}

	public double getIntValAux() {
		return intValAux;
	}

	public void setIntValAux(double intValAux) {
		this.intValAux = intValAux;
	}

	public boolean isCredito() {
		return isCredito;
	}

	public void setCredito(boolean isCredito) {
		this.isCredito = isCredito;
	}

	public List<OperCorpModel> getObjRsOperCorp() {
		return objRsOperCorp;
	}

	public void setObjRsOperCorp(List<OperCorpModel> objRsOperCorp) {
		this.objRsOperCorp = objRsOperCorp;
	}

	public List<OperAuxModel> getObjRsOperAux() {
		return objRsOperAux;
	}

	public void setObjRsOperAux(List<OperAuxModel> objRsOperAux) {
		this.objRsOperAux = objRsOperAux;
	}

	public String getSelectedOperCorp() {
		return selectedOperCorp;
	}

	public void setSelectedOperCorp(String selectedOperCorp) {
		this.selectedOperCorp = selectedOperCorp;
	}

	public String getSelectedOperAux() {
		return selectedOperAux;
	}

	public void setSelectedOperAux(String selectedOperAux) {
		this.selectedOperAux = selectedOperAux;
	}

	public int getIntQntLancCrdt() {
		return intQntLancCrdt;
	}

	public void setIntQntLancCrdt(int intQntLancCrdt) {
		this.intQntLancCrdt = intQntLancCrdt;
	}

	public String getSelectRdoContestacao() {
		return selectRdoContestacao;
	}

	public void setSelectRdoContestacao(String selectRdoContestacao) {
		this.selectRdoContestacao = selectRdoContestacao;
	}

	public double getIntQntValDbto() {
		return intQntValDbto;
	}

	public void setIntQntValDbto(double intQntValDbto) {
		this.intQntValDbto = intQntValDbto;
	}

	public double getIntQntValCrdt() {
		return intQntValCrdt;
	}

	public void setIntQntValCrdt(double intQntValCrdt) {
		this.intQntValCrdt = intQntValCrdt;
	}

	public boolean isOrigExists() {
		return origExists;
	}

	public void setOrigExists(boolean origExists) {
		this.origExists = origExists;
	}

	public boolean isDestExists() {
		return destExists;
	}

	public void setDestExists(boolean destExists) {
		this.destExists = destExists;
	}

	public boolean isRet() {
		return ret;
	}

	public void setRet(boolean ret) {
		this.ret = ret;
	}

	public String getStrDtPeriFixa() {
		return strDtPeriFixa;
	}

	public void setStrDtPeriFixa(String strDtPeriFixa) {
		this.strDtPeriFixa = strDtPeriFixa;
	}

	public String getStrDtPeriIni() {
		return strDtPeriIni;
	}

	public void setStrDtPeriIni(String strDtPeriIni) {
		this.strDtPeriIni = strDtPeriIni;
	}

	public String getStrDtPeriFim() {
		return strDtPeriFim;
	}

	public void setStrDtPeriFim(String strDtPeriFim) {
		this.strDtPeriFim = strDtPeriFim;
	}
}
