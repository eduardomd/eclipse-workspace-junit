package com.altec.bsbr.app.hyb.dto;

public class ObjRsOsInformacoesGerais {
	
	private String txtFalhas;
	private String txtOutrasProp;
	private String cdNoti;
	private String cdSitu;
	
	public ObjRsOsInformacoesGerais() {
		
	}
	
	public ObjRsOsInformacoesGerais(String txtFalhas, String txtOutrasProp, String cdNoti, String cdSitu) {
		this.txtFalhas = txtFalhas;
		this.txtOutrasProp = txtOutrasProp;
		this.cdNoti = cdNoti;
		this.cdSitu = cdSitu;
	}

	public String getTxtFalhas() {
		return txtFalhas;
	}
	public void setTxtFalhas(String txtFalhas) {
		this.txtFalhas = txtFalhas;
	}
	public String getTxtOutrasProp() {
		return txtOutrasProp;
	}
	public void setTxtOutrasProp(String txtOutrasProp) {
		this.txtOutrasProp = txtOutrasProp;
	}
	public String getCdNoti() {
		return cdNoti;
	}
	public void setCdNoti(String cdNoti) {
		this.cdNoti = cdNoti;
	}
	public String getCdSitu() {
		return cdSitu;
	}
	public void setCdSitu(String cdSitu) {
		this.cdSitu = cdSitu;
	}

}
