package com.altec.bsbr.app.hyb.web.jsf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmGer.XHYAdmGerEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmGer.WebServiceException;

import com.altec.bsbr.app.hyb.dto.AdmGerManutPAuxModel;
import com.altec.bsbr.app.hyb.dto.AdmGerManutPAuxCanaisModel;

import com.altec.bsbr.fw.web.jsf.BasicBBean;

@Component("AdmGerManutPAuxBean")
@Scope("request")
public class AdmGerManutPAuxBean extends BasicBBean{

	private static final long serialVersionUID = 1L;
	
	private static final String TABLE_CARREGANDO = "Carregando...";
	private static final String TABLE_VAZIO = "Sem registro"; 
	
	@Autowired
	private XHYAdmGerEndPoint admGetPAux;

    private AdmGerManutPAuxModel objRsAdmPAuxlModel;
    private AdmGerManutPAuxCanaisModel objRsAdmPAuxCanaislModel;

    private LazyDataModel<AdmGerManutPAuxModel> objRsAdmPAux;
    private List<AdmGerManutPAuxCanaisModel> objRsAdmCanaisPAux;
    
	private String txtAddPAux;
	private String retorno;
	private String emptyMessage;
	
	private Map<String, Boolean> manutPauxChecked;
    
    @PostConstruct
    public void init() {
    	emptyMessage = TABLE_CARREGANDO;   	
    	montarListaCanais();
    }
        
    public void montarListaCanais() {
    	objRsAdmCanaisPAux = new ArrayList<AdmGerManutPAuxCanaisModel>();
    	
		JSONObject objJSON;
		JSONArray pcursor;
		
		try {			
 			retorno = admGetPAux.consultarCanal();
 	        
 			objJSON = new JSONObject(retorno);
 	        pcursor = objJSON.getJSONArray("PCURSOR");

 	        for (int i = 0; i < pcursor.length(); i++) {
 	        	JSONObject f = pcursor.getJSONObject(i);
 	        	objRsAdmPAuxCanaislModel = new AdmGerManutPAuxCanaisModel();	            
 	        	objRsAdmPAuxCanaislModel.setCODIGO(f.isNull("CODIGO") ? 0 : f.getInt("CODIGO"));
 	        	objRsAdmPAuxCanaislModel.setNOME(f.isNull("NOME") ? "" : f.getString("NOME"));
 	        	objRsAdmPAuxCanaislModel.setEXCLUSAO(f.getInt("EXCLUSAO") == 1 ? true : false);
	        	
 	        	objRsAdmCanaisPAux.add( new AdmGerManutPAuxCanaisModel(
 	            		objRsAdmPAuxCanaislModel.getCODIGO(), 
 	            		objRsAdmPAuxCanaislModel.getNOME(), 
 	            		objRsAdmPAuxCanaislModel.getEXCLUSAO())
 	    		); 
 	            
 	        } 
 	        
 		} catch (WebServiceException e) {
 		    // TODO Auto-generated catch block
 		    e.printStackTrace();
 		    FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "hyb_erro.xhtml?&strErro=" + e.getMessage());
        }
    }

    public void montarListaPAux_Lazy() {
    	
    	emptyMessage = TABLE_VAZIO;
    	
    	objRsAdmPAux = new LazyDataModel<AdmGerManutPAuxModel>() {

			private static final long serialVersionUID = 1L;
			
			@Override
			public List<AdmGerManutPAuxModel> load(int first, int pageSize, String sortField,
                                                    SortOrder sortOrder, Map<String, Object> filters) {
												
				List<AdmGerManutPAuxModel> admGerManutPAuxModelTemp = montarListaPAux();
				
				setRowCount(admGerManutPAuxModelTemp.size());
				setPageSize(pageSize);
				
				List<AdmGerManutPAuxModel> result = new ArrayList<AdmGerManutPAuxModel>();
				
				if(manutPauxChecked == null)
					manutPauxChecked = new HashMap<String, Boolean>();
				
				manutPauxChecked.clear();
				int criarConexao = 0;
				
				for (int i = first; i < (first + pageSize); i++) {
					
					result.add(admGerManutPAuxModelTemp.get(i));
					
					for (int j = 0; j < objRsAdmCanaisPAux.size(); j++) {
						
						String chave = objRsAdmCanaisPAux.get(j).getCODIGO() + "_" + admGerManutPAuxModelTemp.get(i).getCODIGO();
												
						manutPauxChecked.put(chave, checarCanalPAux(criarConexao, objRsAdmCanaisPAux.get(j).getCODIGO(), admGerManutPAuxModelTemp.get(i).getCODIGO()));					
					
						criarConexao++;
					}
				}
				
				System.out.println("Size MAP: " + manutPauxChecked.size());
				
				return result;
			}
		};
    }
    
    private List<AdmGerManutPAuxModel> montarListaPAux() {
    	List<AdmGerManutPAuxModel> ObjRsAdmPAuxTemp = new ArrayList<AdmGerManutPAuxModel>();
		
		JSONObject objJSON;
		JSONArray pcursor;
		
		try {			
 			retorno = admGetPAux.consultarPAux();
 	        
 			objJSON = new JSONObject(retorno);
 	        pcursor = objJSON.getJSONArray("PCURSOR");
 	        
 	        for (int i = 0; i < pcursor.length(); i++) {
 	        	
 	        	JSONObject f = pcursor.getJSONObject(i);
 	        	objRsAdmPAuxlModel = new AdmGerManutPAuxModel();
 	        	objRsAdmPAuxlModel.setCODIGO(f.isNull("CODIGO") ? 0 : f.getInt("CODIGO"));
 	        	objRsAdmPAuxlModel.setNOME(f.isNull("NOME") ? "" : f.getString("NOME"));
 	        	objRsAdmPAuxlModel.setSLA(f.isNull("SLA") ? 0 : f.getInt("SLA"));
 	        	objRsAdmPAuxlModel.setOPERACAODEB(f.isNull("OPERACAO") ? false : (f.getInt("OPERACAO") == 1 || f.getInt("OPERACAO") == 3) ? true : false);
 	        	objRsAdmPAuxlModel.setOPERACAOCRED(f.isNull("OPERACAO") ? false : (f.getInt("OPERACAO") == 2 || f.getInt("OPERACAO") == 3) ? true : false);
 	        	objRsAdmPAuxlModel.setEXCLUSAO(f.getInt("EXCLUSAO") == 1 ? true : false); 	            

 	        	ObjRsAdmPAuxTemp.add( new AdmGerManutPAuxModel(
 	        			objRsAdmPAuxlModel.getCODIGO(),
 	        			objRsAdmPAuxlModel.getNOME(),
 	        			objRsAdmPAuxlModel.getSLA(),
 	        			objRsAdmPAuxlModel.getOPERACAODEB(),
 	        			objRsAdmPAuxlModel.getOPERACAOCRED(),
 	        			objRsAdmPAuxlModel.getEXCLUSAO())
 	    		); 
 	        	
 	        } 
 		} catch (WebServiceException e) {
 		    // TODO Auto-generated catch block
 		    e.printStackTrace();
 		    //FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "hyb_erro.xhtml?&strErro=" + e.getMessage());
        }
				
		return ObjRsAdmPAuxTemp;
    }
    
    public boolean checarCanalPAux(int index, int codigoCanal, int codigoPAux) {	
		boolean relac = false;
		
		try {
    		String txtcodigoPAux = String.valueOf(codigoPAux);
    		String txtcodigoCanal = String.valueOf(codigoCanal);
    		String retornoCanalPCorp = admGetPAux.verificaCanalPAux(index, txtcodigoCanal, txtcodigoPAux);
 	        		
    		JSONObject verificaCanalPCorp = new JSONObject(retornoCanalPCorp);
    		JSONArray arr = verificaCanalPCorp.getJSONArray("PCURSOR");
    		JSONObject c = arr.getJSONObject(0);
 	        			        		 	        		
    		relac = (c.getInt("RELAC") == 0 ? false : true );
    		
 	        
 		} catch (WebServiceException e) {
 		    // TODO Auto-generated catch block
 		    e.printStackTrace();
 		    //FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "hyb_erro.xhtml?&strErro=" + e.getMessage());
        }
		
		return relac;
    }
    
    public boolean isCanalPAuxChecked(int codigoCanal, int codigoPAux) {
		String chave = codigoCanal + "_" + codigoPAux;
		return manutPauxChecked.get(chave);
    }
    	
    public void adicionar() {
    	try {
    		admGetPAux.inserirPAux(txtAddPAux); 
			RequestContext.getCurrentInstance().execute("alert('Produto Auxiliar incluÃ­do com sucesso!')");		
			montarListaCanais();
			montarListaPAux_Lazy();
			
		} catch (Exception e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		    try {
	    		FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (Exception ex) {
			    // TODO Auto-generated catch block
			    e.printStackTrace();
			}
		}   
    }
    
    public void deletar() {
    	String codPAux = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("CodPAux").toString();
    	try {
    		admGetPAux.apagarPAux(codPAux);
			RequestContext.getCurrentInstance().execute("alert('Produto Auxiliar excluÃ­do com sucesso!')");			
			montarListaCanais();
			montarListaPAux_Lazy();
		} catch (Exception e) {
		    // TODO Auto-generated catch block
		    try {
	    		FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (Exception ex) {
			    // TODO Auto-generated catch block
			    e.printStackTrace();
			}
		}
    }

    public void salvar() {		
    	String[] PAux = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("arrPAux").toString().split("@");
    	String[] canaisPAux = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("arrCanaisPAux").toString().split("@");

		String[] pAuxItens;
		String[] canaisPAuxItens;
		
		boolean validarAlteracao = false;
		
		try {
			for (int i=0; i<PAux.length; i++) {
				pAuxItens = PAux[i].split("\\|");	
				
				int intTipoOp = 0;
			    
		    	if (Boolean.parseBoolean(pAuxItens[3]) == true && Boolean.parseBoolean(pAuxItens[4]) == false)
		    		intTipoOp = 1;
		    	
		    	if (Boolean.parseBoolean(pAuxItens[3]) == false && Boolean.parseBoolean(pAuxItens[4]) == true)
		    		intTipoOp = 2;
		    	
		    	if (Boolean.parseBoolean(pAuxItens[3]) == true && Boolean.parseBoolean(pAuxItens[4]) == true)
		    		intTipoOp = 3;
		    	
		    	validarAlteracao = admGetPAux.alterarPAux(String.valueOf(pAuxItens[0]), String.valueOf(pAuxItens[1]), String.valueOf(pAuxItens[2]), String.valueOf(intTipoOp));
		    	
//		    	if (!validarAlteracao){
//			        try {
//						FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=NÃ£o foi possÃ­vel atualizar " + pAuxItens[0] + pAuxItens[1]);
//					} catch (Exception ex) {
//					    // TODO Auto-generated catch block
//					    ex.printStackTrace();
//					}
//		    	}
			}
    		
    		for (int i=0; i<canaisPAux.length; i++) {
        		String[] canaisPAuxItens1;
    			canaisPAuxItens = canaisPAux[i].split("\\|");
    			canaisPAuxItens1 = canaisPAuxItens[0].split("_");
    			
    			if (Boolean.parseBoolean(canaisPAuxItens[1])) 
    				validarAlteracao = admGetPAux.inserirCanalPAux(String.valueOf(canaisPAuxItens1[0]),String.valueOf(canaisPAuxItens1[1]));
    			else
    				validarAlteracao = admGetPAux.apagarCanalPAux(String.valueOf(canaisPAuxItens1[0]),String.valueOf(canaisPAuxItens1[1]));
		    	
//		    	if (!validarAlteracao){
//				    try {
//			    		FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=NÃ£o foi possÃ­vel atualizar " + canaisPAuxItens1[0] + canaisPAuxItens1[1]);
//					} catch (Exception ex) {
//					    // TODO Auto-generated catch block
//					    ex.printStackTrace();
//					}
//		    	}
    		}    		

			RequestContext.getCurrentInstance().execute("alert('AlteraÃ§Ãµes efetuadas com sucesso!')"); 
			
			montarListaCanais();
			montarListaPAux_Lazy();
		} catch (Exception e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}  
    }

	public String getTxtAddPAux() {
		return txtAddPAux;
	}

	public void setTxtAddPAux(String txtAddPAux) {
		this.txtAddPAux = txtAddPAux;
	}
	
	public List<AdmGerManutPAuxCanaisModel> getObjRsAdmCanaisPAux() {
		return objRsAdmCanaisPAux;
	}

	public void setObjRsAdmCanaisPAux(List<AdmGerManutPAuxCanaisModel> objRsAdmCanaisPAux) {
		this.objRsAdmCanaisPAux = objRsAdmCanaisPAux;
	}

	public boolean getChecarCanalPAux() {
		return true;
	}
	public void setChecarCanalPAux(boolean canaisPAux) {
	}

	public LazyDataModel<AdmGerManutPAuxModel> getObjRsAdmPAux() {
		return objRsAdmPAux;
	}

	public String getEmptyMessage() {
		return emptyMessage;
	}

	public void setEmptyMessage(String emptyMessage) {
		this.emptyMessage = emptyMessage;
	}
	
}
