package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.helpers.XSSFRowShifter;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.RelosEstatisticoModel;
import com.altec.bsbr.app.jab.hyb.webclient.XHYRelatorios.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYRelatorios.XHYRelatoriosEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;

@Component("relosEstatisticoBean")
@Scope("session")
public class RelosEstatisticoBean extends BasicBBean {

	private static final long serialVersionUID = 1L;

	@Autowired
	private XHYRelatoriosEndPoint objRelat;

	private List<RelosEstatisticoModel> objRsRelat = new ArrayList<RelosEstatisticoModel>();
	private String strDtIni;
	private String strDtFim;

	private Double valorEnvolvidoTotal;
	private Double valorPerdaTotal;

	ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
	JSONArray pcursorObjRs = new JSONArray();

	@PostConstruct
	public void init() throws IOException {
		FacesContext fc = FacesContext.getCurrentInstance();
		@SuppressWarnings("unchecked")
		Map<String, String> params = fc.getExternalContext().getRequestParameterMap();

		this.strDtIni = params.get("pDtIni");
		this.strDtFim = params.get("pDtFim");

		try {
			String objRs = objRelat.fnRelEstatistico(this.strDtIni, this.strDtFim);
			JSONObject objResTemp = new JSONObject(objRs);
			pcursorObjRs = objResTemp.getJSONArray("PCURSOR");
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		for (int i = 0; i < pcursorObjRs.length(); i++) {
			JSONObject temp = pcursorObjRs.getJSONObject(i);

			objRsRelat.add(new RelosEstatisticoModel(
					temp.isNull("NR_SEQU_ORDE_SERV") ? "" : temp.get("NR_SEQU_ORDE_SERV").toString(),
					temp.isNull("DT_ABER_ORDE_SERV") ? "" : temp.get("DT_ABER_ORDE_SERV").toString(),
					temp.isNull("NM_EVEN") ? "" : temp.get("NM_EVEN").toString(),
					temp.isNull("NM_CRIT") ? "" : temp.get("NM_CRIT").toString(),
					temp.isNull("CD_UOR_FORMAT") ? "" : temp.get("CD_UOR_FORMAT").toString(),
					temp.isNull("CD_UOR") ? "" : temp.get("CD_UOR").toString(),
					temp.isNull("TP_UOR") ? "" : temp.get("TP_UOR").toString(),
					temp.isNull("NM_UOR") ? "" : temp.get("NM_UOR").toString(),
					temp.isNull("NM_REDE") ? "" : temp.get("NM_REDE").toString(),
					temp.isNull("NM_REGI") ? "" : temp.get("NM_REGI").toString(),
					temp.isNull("TX_ABER_ORDE_SERV") ? "" : temp.get("TX_ABER_ORDE_SERV").toString(),
					temp.isNull("VL_ENVO") ? "" : temp.get("VL_ENVO").toString(),
					temp.isNull("VL_PERD_EFET") ? "" : temp.get("VL_PERD_EFET").toString(),
					temp.isNull("NM_ANL_DESIG") ? "" : temp.get("NM_ANL_DESIG").toString(),
					temp.isNull("NM_SITU_ORDE_SERV") ? "" : temp.get("NM_SITU_ORDE_SERV").toString(),
					temp.isNull("DT_PREV_ENCE_ORDE_SERV") ? "" : temp.get("DT_PREV_ENCE_ORDE_SERV").toString(),
					temp.isNull("VL_ENVO") ? "" : formataNumero(temp.get("VL_ENVO").toString()),
					temp.isNull("VL_PERD_EFET") ? "" : formataNumero(temp.get("VL_PERD_EFET").toString())));
		}
		
		/*if(objRsRelat.size() == 0) {
			for (int i = 0; i < 10; i++) {
				objRsRelat.add(new RelosEstatisticoModel(
						"2008000001",
						"20/03/2019",
						"NM_EVEN",
						"NM_CRIT",
						"CD_UOR_FORMAT",
						"CD_UOR",
						"TP_UOR",
						"NM_UOR",
						"NM_REDE",
						"NM_REGI",
						"TX_ABER_ORDE_SERV",
						"20000",
						"15000",
						"NM_ANL_DESIG",
						"NM_SITU_ORDE_SERV",
						"20/03/2019",
						formataNumero("20000"),
						formataNumero("15000")));
			}
		}*/

		cleanSession();
	}

	public void cleanSession() {
		FacesContext context = FacesContext.getCurrentInstance();
		if (context.getExternalContext().getSessionMap().get("relatorioosBean") != null) {
			context.getExternalContext().getSessionMap().remove("relatorioosBean");
		}
	}

	public int listSize() {
		return objRsRelat.size();
	}

	public List<RelosEstatisticoModel> getObjRsRelat() {
		return objRsRelat;
	}

	public void setObjRsRelat(List<RelosEstatisticoModel> objRsRelat) {
		this.objRsRelat = objRsRelat;
	}

	public String getStrDtIni() {
		return strDtIni;
	}

	public void setStrDtIni(String strDtIni) {
		this.strDtIni = strDtIni;
	}

	public String getStrDtFim() {
		return strDtFim;
	}

	public void setStrDtFim(String strDtFim) {
		this.strDtFim = strDtFim;
	}

	public String now() {
		DateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		Date date = new Date();
		return dateformat.format(date);
	}

	public String formataNumero(String number) {
		String result = "";
		if (number != null && !number.isEmpty()) {
			DecimalFormat value = new DecimalFormat("###,##0.00");
			result = value.format(Float.parseFloat(number));
			String fp = result.substring(0, result.length() - 3);
			String sp = result.substring(result.length() - 3);
			result = fp.replace(",", ".") + sp.replace(".", ",");
		}
		
		if (result.equals("0,00")) {
			return "0";
		}

		return result;
	}

	public String valorEnvolvidoTotalGeral() {
		this.valorEnvolvidoTotal = 0.00;
		objRsRelat.forEach((item) -> {
			this.valorEnvolvidoTotal += Integer.parseInt(item.getVL_ENVO());
		});
		return formataNumero(Double.toString(this.valorEnvolvidoTotal));
	}

	public String valorPerdaGeral() {
		this.valorPerdaTotal = 0.00;
		objRsRelat.forEach((item) -> {
			this.valorPerdaTotal += Integer.parseInt(item.getVL_PERD_EFET());
		});
		return formataNumero(Double.toString(this.valorPerdaTotal));
	}

	public void postProcessExcel(Object doc) {
		try {
			XSSFWorkbook wb = (XSSFWorkbook) doc;
			XSSFSheet sheet = wb.getSheetAt(0);
			sheet.setDisplayGridlines(false);

			formatarCabecalho(wb, sheet);
			formatarCabecalhoTabela(wb, sheet);
			formatarRegistrosTabela(wb, sheet);
			criarRodape(wb, sheet);
			
			sheet.shiftRows(3, sheet.getLastRowNum() + 1, 2);
			sheet.shiftRows(6, sheet.getLastRowNum() + 1, 1);

		} catch (Exception e) {
			System.out.println(e);
		}

	}

	private void formatarCabecalho(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyle1 = wb.createCellStyle();
		headerStyle1.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle1.setWrapText(true);
		headerStyle1.setBorderBottom(CellStyle.BORDER_NONE);
		headerStyle1.setBorderLeft(CellStyle.BORDER_NONE);
		headerStyle1.setBorderRight(CellStyle.BORDER_NONE);
		headerStyle1.setBorderTop(CellStyle.BORDER_NONE);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 14);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		headerStyle1.setFont(font);

		XSSFRow primeiraLinha = sheet.getRow(1);
		if (primeiraLinha != null) {
			XSSFCell primeiraLinhaCell = primeiraLinha.getCell(0);
			primeiraLinhaCell.setCellStyle(headerStyle1);
			primeiraLinha.setHeightInPoints((short) 19);
		}
		XSSFRow segundaLinha = sheet.getRow(2);
		if (segundaLinha != null) {
			XSSFCell segundaLinhaCell = segundaLinha.getCell(0);
			segundaLinhaCell.setCellStyle(headerStyle1);
			segundaLinha.setHeightInPoints((short) 19);
		}
	}

	private void formatarCabecalhoTabela(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setWrapText(true);
		headerStyle.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyle.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyle.setBorderRight(CellStyle.BORDER_THIN);
		headerStyle.setBorderTop(CellStyle.BORDER_THIN);
		headerStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.WHITE.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(font);

		XSSFRow linhaCabecalhoTabela = sheet.getRow(4);
		if (linhaCabecalhoTabela != null) {
			for (int i = 0; i <= linhaCabecalhoTabela.getLastCellNum(); i++) {
				XSSFCell celulaHeader = linhaCabecalhoTabela.getCell(i);
				if (celulaHeader != null) {
					celulaHeader.setCellStyle(headerStyle);
				}
			}
		}
	}

	private void formatarRegistrosTabela(XSSFWorkbook wb, XSSFSheet sheet) {

		if (objRsRelat != null && !objRsRelat.isEmpty()) {
			formatarLinhasDetalhe(wb, sheet);
			formatarLinhaQntd(wb, sheet);
			formatarLinhaTotais(wb, sheet);
		} else {
			formatarDetalheVazio(wb, sheet);
		}

		for (int i = 0; i <= sheet.getRow(4).getLastCellNum(); i++) {
			sheet.autoSizeColumn(i);
		}
	}

	private void formatarDetalheVazio(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setWrapText(true);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(font);
		
		CellRangeAddress detalheVazio = new CellRangeAddress(5, 5, 0, 14);
		sheet.addMergedRegion(detalheVazio);

		XSSFRow linhaVazia = sheet.getRow(5);
		if (linhaVazia == null) {
			linhaVazia = sheet.createRow(5);
		}
		XSSFCell celulaVazia = linhaVazia.createCell(0);
		linhaVazia.setHeightInPoints((short) 20);
		celulaVazia.setCellValue("Não existem dados para o período selecionado.");
		celulaVazia.setCellStyle(headerStyle);

		RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
		RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
		RegionUtil.setBorderRight(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
		RegionUtil.setBorderTop(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
	}

	private void formatarLinhasDetalhe(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyleLeft = wb.createCellStyle();
		headerStyleLeft.setAlignment(CellStyle.ALIGN_LEFT);
		headerStyleLeft.setWrapText(true);
		headerStyleLeft.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderTop(CellStyle.BORDER_THIN);
		Font fontLeft = wb.createFont();
		fontLeft.setFontHeightInPoints((short) 11);
		fontLeft.setFontName("Calibri");
		fontLeft.setColor(IndexedColors.BLACK.getIndex());
		fontLeft.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		headerStyleLeft.setFont(fontLeft);
		
		XSSFCellStyle headerStyleRight = wb.createCellStyle();
		headerStyleRight.setAlignment(CellStyle.ALIGN_RIGHT);
		headerStyleRight.setWrapText(true);
		headerStyleRight.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderTop(CellStyle.BORDER_THIN);
		Font fontRight = wb.createFont();
		fontRight.setFontHeightInPoints((short) 11);
		fontRight.setFontName("Calibri");
		fontRight.setColor(IndexedColors.BLACK.getIndex());
		fontRight.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		headerStyleRight.setFont(fontRight);
		
		
		int linhaInicial = 5;
		int linhasDetalhe = objRsRelat.size();
		int linhaFinal = linhaInicial + linhasDetalhe;

		for (int linha = linhaInicial; linha <= linhaFinal; linha++) {
			XSSFRow detalheTabela = sheet.getRow(linha);
			if (detalheTabela != null) {
				for (int i = 0; i <= detalheTabela.getLastCellNum(); i++) {
					XSSFCell celulaDetalhe = detalheTabela.getCell(i);
					if (celulaDetalhe != null) {
						if (i == 1 || i == 3 || i == 5 || i == 10 || i == 11 || i == 14) {
							celulaDetalhe.setCellStyle(headerStyleRight);
						} else {
							celulaDetalhe.setCellStyle(headerStyleLeft);
						}
						
					}
				}
			}
		}
	}
	
	private void formatarLinhaQntd(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyleLeft = wb.createCellStyle();
		headerStyleLeft.setAlignment(CellStyle.ALIGN_LEFT);
		headerStyleLeft.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderTop(CellStyle.BORDER_THIN);
		
		XSSFCellStyle headerStyleSemBold = wb.createCellStyle();
		headerStyleSemBold.setAlignment(CellStyle.ALIGN_LEFT);
		headerStyleSemBold.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleSemBold.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleSemBold.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleSemBold.setBorderTop(CellStyle.BORDER_THIN);
		
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyleLeft.setFont(font);
		
		
		Font fontSemBold = wb.createFont();
		fontSemBold.setFontHeightInPoints((short) 11);
		fontSemBold.setFontName("Calibri");
		fontSemBold.setColor(IndexedColors.BLACK.getIndex());
		headerStyleSemBold.setFont(fontSemBold);
		
		CellRangeAddress detalheQntd = new CellRangeAddress(5 + objRsRelat.size(), 5 + objRsRelat.size(), 0, 1);
		sheet.addMergedRegion(detalheQntd);
		
		RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, detalheQntd, sheet, wb);
		RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, detalheQntd, sheet, wb);
		RegionUtil.setBorderRight(CellStyle.BORDER_THIN, detalheQntd, sheet, wb);
		RegionUtil.setBorderTop(CellStyle.BORDER_THIN, detalheQntd, sheet, wb);
		
		CellRangeAddress detalheQntd2 = new CellRangeAddress(5 + objRsRelat.size(), 5 + objRsRelat.size(), 2, 14);
		sheet.addMergedRegion(detalheQntd2);
		
		RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, detalheQntd2, sheet, wb);
		RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, detalheQntd2, sheet, wb);
		RegionUtil.setBorderRight(CellStyle.BORDER_THIN, detalheQntd2, sheet, wb);
		RegionUtil.setBorderTop(CellStyle.BORDER_THIN, detalheQntd2, sheet, wb);
		
		XSSFRow linhaQntd = sheet.getRow(5 + objRsRelat.size());
		if (linhaQntd == null) {
			linhaQntd = sheet.createRow(5 + objRsRelat.size());
		}
		
		XSSFCell celulaVazia = linhaQntd.createCell(0);
		linhaQntd.setHeightInPoints((short) 20);
		celulaVazia.setCellValue("Qntdade OS: " + objRsRelat.size());
		celulaVazia.setCellStyle(headerStyleLeft);
		
		XSSFCell celulaVazia2 = linhaQntd.createCell(2);
		linhaQntd.setHeightInPoints((short) 20);
		celulaVazia2.setCellValue(objRsRelat.size());
		celulaVazia2.setCellStyle(headerStyleSemBold);
		
		XSSFRow linhaEmBranco = sheet.getRow(6 + objRsRelat.size());
		if (linhaEmBranco == null) {
			linhaEmBranco = sheet.createRow(6 + objRsRelat.size());
		}
		
		CellRangeAddress detalheQntd3 = new CellRangeAddress(6 + objRsRelat.size(), 6 + objRsRelat.size(), 2, 14);
		sheet.addMergedRegion(detalheQntd3);
		
		RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, detalheQntd3, sheet, wb);
		RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, detalheQntd3, sheet, wb);
		RegionUtil.setBorderRight(CellStyle.BORDER_THIN, detalheQntd3, sheet, wb);
		RegionUtil.setBorderTop(CellStyle.BORDER_THIN, detalheQntd3, sheet, wb);
	}
	
	private void formatarLinhaTotais(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyleCenter = wb.createCellStyle();
		headerStyleCenter.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyleCenter.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleCenter.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleCenter.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleCenter.setBorderTop(CellStyle.BORDER_THIN);
		headerStyleCenter.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		headerStyleCenter.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		
		XSSFCellStyle headerStyleRight = wb.createCellStyle();
		headerStyleRight.setAlignment(CellStyle.ALIGN_RIGHT);
		headerStyleRight.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderTop(CellStyle.BORDER_THIN);
		headerStyleRight.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		headerStyleRight.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		
		
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.WHITE.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyleCenter.setFont(font);
		headerStyleRight.setFont(font);
		
		CellRangeAddress detalheQntd = new CellRangeAddress(7 + objRsRelat.size(), 7 + objRsRelat.size(), 0, 9);
		sheet.addMergedRegion(detalheQntd);
		
		RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, detalheQntd, sheet, wb);
		RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, detalheQntd, sheet, wb);
		RegionUtil.setBorderRight(CellStyle.BORDER_THIN, detalheQntd, sheet, wb);
		RegionUtil.setBorderTop(CellStyle.BORDER_THIN, detalheQntd, sheet, wb);
		
		CellRangeAddress detalheQntd2 = new CellRangeAddress(7 + objRsRelat.size(), 7 + objRsRelat.size(), 12, 14);
		sheet.addMergedRegion(detalheQntd2);
		
		RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, detalheQntd2, sheet, wb);
		RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, detalheQntd2, sheet, wb);
		RegionUtil.setBorderRight(CellStyle.BORDER_THIN, detalheQntd2, sheet, wb);
		RegionUtil.setBorderTop(CellStyle.BORDER_THIN, detalheQntd2, sheet, wb);
		
		XSSFRow linhaQntd = sheet.getRow(7 + objRsRelat.size());
		if (linhaQntd == null) {
			linhaQntd = sheet.createRow(7 + objRsRelat.size());
		}
		
		XSSFCell celulaVazia = linhaQntd.createCell(0);
		linhaQntd.setHeightInPoints((short) 20);
		celulaVazia.setCellValue("Total Geral:");
		celulaVazia.setCellStyle(headerStyleCenter);
		
		XSSFCell celulaVazia2 = linhaQntd.createCell(10);
		linhaQntd.setHeightInPoints((short) 20);
		celulaVazia2.setCellValue(this.valorEnvolvidoTotalGeral());
		celulaVazia2.setCellStyle(headerStyleRight);
		
		XSSFCell celulaVazia3 = linhaQntd.createCell(11);
		linhaQntd.setHeightInPoints((short) 20);
		celulaVazia3.setCellValue(this.valorPerdaGeral());
		celulaVazia3.setCellStyle(headerStyleRight);
		
		XSSFCell celulaVazia4 = linhaQntd.createCell(12);
		linhaQntd.setHeightInPoints((short) 20);
		celulaVazia4.setCellValue("");
		celulaVazia4.setCellStyle(headerStyleRight);
	}
	
	private void criarRodape(XSSFWorkbook wb, XSSFSheet sheet) {
		Font font;
		/* Criação, inclusão e formatação dos dados do footer do excel */
		XSSFRow ultimaLinha = sheet.createRow(sheet.getLastRowNum() + 1);

		XSSFCellStyle footerStyle = wb.createCellStyle();
		footerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		footerStyle.setWrapText(true);
		font = footerStyle.getFont();
		font.setFontHeight((short) 150);
		font.setFontName("Arial");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		footerStyle.setFont(font);

		XSSFCell cell1 = ultimaLinha.createCell(0);
		cell1.setCellValue(now());
		cell1.setCellStyle(footerStyle);

		XSSFCell cell2 = ultimaLinha.createCell(2);
		cell2.setCellValue("Superintêndencia de Ocorrências Especiais");
		cell2.setCellStyle(footerStyle);

		CellRangeAddress region1 = new CellRangeAddress(ultimaLinha.getRowNum(), ultimaLinha.getRowNum(), 0, 1);
		CellRangeAddress region2 = new CellRangeAddress(ultimaLinha.getRowNum(), ultimaLinha.getRowNum(), 2, 13);
		sheet.addMergedRegion(region1);
		sheet.addMergedRegion(region2);
	}
}
