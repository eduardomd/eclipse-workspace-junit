package com.altec.bsbr.app.hyb.web.jsf;

public class CadOsPartici {
	
	private String strEndereco;
	private String strNumero;
	private String strComplemento;
	private String strBairro;
	private String strCEP;
	private String strCidade;
	private String strEstado;
	private String strTelefone;
	private String strCelular;
	private int intTipoEnd;
	private String SQ_ENDE;
	
	//--------------------

	private String stErro;
	private int TP_PARTICIPACAO;
	private String OS;
	private String NAME;
	private String CPF_CNPJ;
	private String RG;
	private String ORGAO_EMISSOR;
	private String DT_EMISSAO_RG;
	private String PROFISSAO;
	private String RENDA;
	private String ENDERECO;
	private String NUMERO;
	private String COMPLEMENTO;
	private String BAIRRO;
	private String CEP;
	private String TELEFONE;
	private String CELULAR;
	private String DT_ABERTURA;
	private String OPER_VENCER;
	private String OPER_VENCIDA;
	private String OPER_CRELI;
	private String OPER_PREJU;
	private String OPER_PASSIVA;
	private String RECIPROCIDADE;

	public String getOS() {
		return this.OS;
	}

	public void setOS(String oS) {
		OS = oS;
	}

	public String getNAME() {
		return NAME;
	}

	public void setNAME(String nAME) {
		NAME = nAME;
	}

	public String getCPF_CNPJ() {
		return CPF_CNPJ;
	}

	public void setCPF_CNPJ(String cPF_CNPJ) {
		CPF_CNPJ = cPF_CNPJ;
	}

	public String getRG() {
		return RG;
	}

	public void setRG(String rG) {
		RG = rG;
	}

	public String getORGAO_EMISSOR() {
		return ORGAO_EMISSOR;
	}

	public void setORGAO_EMISSOR(String oRGAO_EMISSOR) {
		ORGAO_EMISSOR = oRGAO_EMISSOR;
	}

	public String getDT_EMISSAO_RG() {
		return DT_EMISSAO_RG;
	}

	public void setDT_EMISSAO_RG(String dT_EMISSAO_RG) {
		DT_EMISSAO_RG = dT_EMISSAO_RG;
	}

	public String getPROFISSAO() {
		return PROFISSAO;
	}

	public void setPROFISSAO(String pROFISSAO) {
		PROFISSAO = pROFISSAO;
	}

	public String getRENDA() {
		return RENDA;
	}

	public void setRENDA(String rENDA) {
		RENDA = rENDA;
	}

	public String getENDERECO() {
		return ENDERECO;
	}

	public void setENDERECO(String eNDERECO) {
		ENDERECO = eNDERECO;
	}

	public String getNUMERO() {
		return NUMERO;
	}

	public void setNUMERO(String nUMERO) {
		NUMERO = nUMERO;
	}

	public String getCOMPLEMENTO() {
		return COMPLEMENTO;
	}

	public void setCOMPLEMENTO(String cOMPLEMENTO) {
		COMPLEMENTO = cOMPLEMENTO;
	}

	public String getBAIRRO() {
		return BAIRRO;
	}

	public void setBAIRRO(String bAIRRO) {
		BAIRRO = bAIRRO;
	}

	public String getCEP() {
		return CEP;
	}

	public void setCEP(String cEP) {
		CEP = cEP;
	}

	public String getTELEFONE() {
		return TELEFONE;
	}

	public void setTELEFONE(String tELEFONE) {
		TELEFONE = tELEFONE;
	}

	public String getCELULAR() {
		return CELULAR;
	}

	public void setCELULAR(String cELULAR) {
		CELULAR = cELULAR;
	}

	public String getDT_ABERTURA() {
		return DT_ABERTURA;
	}

	public void setDT_ABERTURA(String dT_ABERTURA) {
		DT_ABERTURA = dT_ABERTURA;
	}

	public String getOPER_VENCER() {
		return OPER_VENCER;
	}

	public void setOPER_VENCER(String oPER_VENCER) {
		OPER_VENCER = oPER_VENCER;
	}

	public String getOPER_VENCIDA() {
		return OPER_VENCIDA;
	}

	public void setOPER_VENCIDA(String oPER_VENCIDA) {
		OPER_VENCIDA = oPER_VENCIDA;
	}

	public String getOPER_CRELI() {
		return OPER_CRELI;
	}

	public void setOPER_CRELI(String oPER_CRELI) {
		OPER_CRELI = oPER_CRELI;
	}

	public String getOPER_PREJU() {
		return OPER_PREJU;
	}

	public void setOPER_PREJU(String oPER_PREJU) {
		OPER_PREJU = oPER_PREJU;
	}

	public String getOPER_PASSIVA() {
		return OPER_PASSIVA;
	}

	public void setOPER_PASSIVA(String oPER_PASSIVA) {
		OPER_PASSIVA = oPER_PASSIVA;
	}

	public String getRECIPROCIDADE() {
		return RECIPROCIDADE;
	}

	public void setRECIPROCIDADE(String rECIPROCIDADE) {
		RECIPROCIDADE = rECIPROCIDADE;
	}

	public int getTP_PARTICIPACAO() {
		return TP_PARTICIPACAO;
	}

	public void setTP_PARTICIPACAO(int tP_PARTICIPACAO) {
		TP_PARTICIPACAO = tP_PARTICIPACAO;
	}

	public String getStErro() {
		return stErro;
	}

	public void setStErro(String stErro) {
		this.stErro = stErro;
	}
	
	public int getIntTipoEnd() {
		return intTipoEnd;
	}

	public void setIntTipoEnd(int intTipoEnd) {
		this.intTipoEnd = intTipoEnd;
	}

	public String getStrEndereco() {
		return strEndereco;
	}

	public void setStrEndereco(String strEndereco) {
		this.strEndereco = strEndereco;
	}

	public String getStrNumero() {
		return strNumero;
	}

	public void setStrNumero(String strNumero) {
		this.strNumero = strNumero;
	}

	public String getStrComplemento() {
		return strComplemento;
	}

	public void setStrComplemento(String strComplemento) {
		this.strComplemento = strComplemento;
	}

	public String getStrBairro() {
		return strBairro;
	}

	public void setStrBairro(String strBairro) {
		this.strBairro = strBairro;
	}

	public String getStrCEP() {
		return strCEP;
	}

	public void setStrCEP(String strCEP) {
		this.strCEP = strCEP;
	}

	public String getStrCidade() {
		return strCidade;
	}

	public void setStrCidade(String strCidade) {
		this.strCidade = strCidade;
	}

	public String getStrEstado() {
		return strEstado;
	}

	public void setStrEstado(String strEstado) {
		this.strEstado = strEstado;
	}

	public String getStrTelefone() {
		return strTelefone;
	}

	public void setStrTelefone(String strTelefone) {
		this.strTelefone = strTelefone;
	}

	public String getStrCelular() {
		return strCelular;
	}

	public void setStrCelular(String strCelular) {
		this.strCelular = strCelular;
	}

	public CadOsPartici ConsultarDetalhePart(String strgNrSeqPart, String strErro) {
		CadOsPartici objRs = new CadOsPartici();
		
		objRs.setOS("OS12345");
		objRs.setNAME("Fellipe do Prado Arruda");
		objRs.setCPF_CNPJ("040.200.241-20");
		objRs.setRG("5589823232232");
		objRs.setORGAO_EMISSOR("SSP");
		objRs.setDT_EMISSAO_RG("22/01/2018");
		objRs.setPROFISSAO("Programador");
		objRs.setRENDA("500000");
		objRs.setENDERECO("Rua dos Democratas");
		objRs.setNUMERO("543");
		objRs.setCOMPLEMENTO("");
		objRs.setBAIRRO("Vila Monte Alegre");
		objRs.setCEP("04305-000");
		objRs.setTELEFONE("1132740424");
		objRs.setCELULAR("11958371095");
		objRs.setDT_ABERTURA("25/01/2018");
		objRs.setOPER_VENCER("500000");
		objRs.setOPER_VENCIDA("500000");
		objRs.setOPER_CRELI("500000");
		objRs.setOPER_PREJU("500000");
		objRs.setOPER_PASSIVA("500000");
		objRs.setRECIPROCIDADE("500000");
		objRs.setTP_PARTICIPACAO(1);
		objRs.setStErro("");

		return objRs;
	}

	public CadOsPartici ConsultarEndeCliente(String strgNrSeqPart, int i, String strErro) {
		CadOsPartici objRs = new CadOsPartici();
		
		objRs.setStrEndereco("Rua dos Democratas");
		objRs.setStrNumero("543");
		objRs.setStrComplemento("no complemento");
		objRs.setStrBairro("Vila Monte Alegre");
		objRs.setStrCEP("04305000");
		objRs.setStrCidade("São Paulo");
		objRs.setStrEstado("São Paulo");
		objRs.setStrTelefone("1132730424");
		objRs.setStrCelular("11958371095");
		objRs.setIntTipoEnd(1);
		objRs.setSQ_ENDE("111111111");
		
        /*strEndereco = objRs("LOGRADOURO")
        strNumero   = objRs("NUMERO")
        strComplemento = objRs("COMPLEMENTO")
        strBairro   = objRs("BAIRRO")
		strCEP = FormataCEP(objRs("CEP"))
        strCidade   = objRs("CIDADE")
        strEstado   = objRs("UF")
        strTelefone = objRs("TELEFONE")
        strCelular  = objRs("CELULAR")*/
		
		return objRs;
	}

	public String getSQ_ENDE() {
		return SQ_ENDE;
	}

	public void setSQ_ENDE(String sQ_ENDE) {
		SQ_ENDE = sQ_ENDE;
	}

}
