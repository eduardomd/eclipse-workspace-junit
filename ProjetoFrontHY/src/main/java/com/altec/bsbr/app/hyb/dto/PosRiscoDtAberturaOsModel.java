package com.altec.bsbr.app.hyb.dto;

public class PosRiscoDtAberturaOsModel {

	private String dtAbertura;
	private String oper_vencer;
	private String oper_vencida;
	private String oper_creli;
	private String oper_preju;
	private String oper_passiva;
	private String reciprocidade;
	
	public PosRiscoDtAberturaOsModel() {
		
	}
	
	public PosRiscoDtAberturaOsModel(String dtAbertura, String oper_vencer, String oper_vencida, String oper_creli,
			String oper_preju, String oper_passiva, String reciprocidade) {
		super();
		this.dtAbertura = dtAbertura;
		this.oper_vencer = oper_vencer;
		this.oper_vencida = oper_vencida;
		this.oper_creli = oper_creli;
		this.oper_preju = oper_preju;
		this.oper_passiva = oper_passiva;
		this.reciprocidade = reciprocidade;
	}

	public String getDtAbertura() {
		return dtAbertura;
	}

	public void setDtAbertura(String dtAbertura) {
		this.dtAbertura = dtAbertura;
	}

	public String getOper_vencer() {
		return oper_vencer;
	}

	public void setOper_vencer(String oper_vencer) {
		this.oper_vencer = oper_vencer;
	}

	public String getOper_vencida() {
		return oper_vencida;
	}

	public void setOper_vencida(String oper_vencida) {
		this.oper_vencida = oper_vencida;
	}

	public String getOper_creli() {
		return oper_creli;
	}

	public void setOper_creli(String oper_creli) {
		this.oper_creli = oper_creli;
	}

	public String getOper_preju() {
		return oper_preju;
	}

	public void setOper_preju(String oper_preju) {
		this.oper_preju = oper_preju;
	}

	public String getOper_passiva() {
		return oper_passiva;
	}

	public void setOper_passiva(String oper_passiva) {
		this.oper_passiva = oper_passiva;
	}

	public String getReciprocidade() {
		return reciprocidade;
	}

	public void setReciprocidade(String reciprocidade) {
		this.reciprocidade = reciprocidade;
	}
	
}
