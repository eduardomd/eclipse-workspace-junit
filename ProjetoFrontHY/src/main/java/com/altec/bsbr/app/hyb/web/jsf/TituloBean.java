package com.altec.bsbr.app.hyb.web.jsf;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.fw.web.jsf.BasicBBean;


@Component("tituloBean")
@Scope("request")
public class TituloBean extends BasicBBean {
	
	private static final long serialVersionUID = 1L;
	private String strTitulo;
	
	@PostConstruct
    public void init() {
		this.strTitulo = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strTitulo");
    }

	public String getStrTitulo() {
		return strTitulo;
	}

	public void setStrTitulo(String strTitulo) {
		this.strTitulo = strTitulo;
	}
	
	

}
