package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.PesquisaOsResultadoModel;
import com.altec.bsbr.app.jab.hyb.webclient.XHYPesquisa.XHYPesquisaEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;


@Component("pesquisaOsResultadoBean")
@Scope("session")
public class PesquisaOsResultadoBean extends BasicBBean {

	@Autowired
	private XHYPesquisaEndPoint pesquisa;

	public String pageVersion = "0.95";
	private static final long serialVersionUID = 1L;

	private String strXml;
	private String strNrOs;
	private String strCdArea;
	private String strAction;

	private String msgErroPermissao;

	private String CD_CARGO_USUARIO = "B70555";
	private String CD_AREA_USUARIO;
	private String CPF_USUARIO = "343.263.830-20";

	private String CODIGO_AREA;
	private String CODIGO;
	private String CODIGO_SITUACAO;
	private String DATA_ABERTURA;
	private String AREA;

	private String txtNrSeqOs;
	private String cboSituacaoOsSelecionada;
	private String cboSituacaoOs;
	private String cboFaseOsSelecionada;
	private String cboCriticidadeOsSelecionada;
	private String criticidadeOs;
	private String txtCustoOsIni;
	private String cboPCorpSelecionada;
	private String cboPAuxSelecionada;
	private String chkInteresseCom;
	private String chkRessarcimento;
	private String txtPeriPesqIni;
	private String txtPeriPesqFin;
	private String txtDataEventoIni;
	private String txtDataEventoFin;
	private String txtDataPocIni;
	private String txtDataPocFin;
	private String txtCidade;
	private String txtEstado;
	private String txtCdDepto;
	private String txtTpPv;
	private String txtCdPv;
	private String txtNmPv;
	private String txtTextoAbertura;
	private String cboCanalSelecionada;

	List<PesquisaOsResultadoModel> ObjRsPesquisaOsResultado = new ArrayList<PesquisaOsResultadoModel>();

	public List<PesquisaOsResultadoModel> getObjRsPesquisaOsResultado() {
		return ObjRsPesquisaOsResultado;
	}
	
	@PostConstruct
	public void init() {
		System.out.println("PESQUISA_RESULTADO - INIT ");

	}	
	
	public void getParametersBusca(ActionEvent event) {
		txtNrSeqOs = event.getComponent().getAttributes().get("txtNrSeqOs") == null ? "" : event.getComponent().getAttributes().get("txtNrSeqOs").toString().trim();
		cboSituacaoOsSelecionada = event.getComponent().getAttributes().get("cboSituacaoOsSelecionada") == null ? "" : event.getComponent().getAttributes().get("cboSituacaoOsSelecionada").toString().trim();
		cboFaseOsSelecionada = event.getComponent().getAttributes().get("cboFaseOsSelecionada") == null ? "": event.getComponent().getAttributes().get("cboFaseOsSelecionada").toString().trim(); 
		cboCriticidadeOsSelecionada = event.getComponent().getAttributes().get("cboCriticidadeOsSelecionada") == null ? "" : event.getComponent().getAttributes().get("cboCriticidadeOsSelecionada").toString().trim();
		txtCustoOsIni = event.getComponent().getAttributes().get("txtCustoOsIni") == null ? "" : event.getComponent().getAttributes().get("txtCustoOsIni").toString().trim();
//		txtCpfEnvolvido = event.getComponent().getAttributes().get("txtCpfEnvolvido") == null ? "" : event.getComponent().getAttributes().get("txtCpfEnvolvido").toString().trim();
//		txtNomeEnvolvido = event.getComponent().getAttributes().get("txtNomeEnvolvido") == null ? "" : event.getComponent().getAttributes().get("txtNomeEnvolvido").toString().trim();
//		cboPenalidadeSelecionada = event.getComponent().getAttributes().get("cboPenalidadeSelecionada") == null ? "" : event.getComponent().getAttributes().get("cboPenalidadeSelecionada").toString().trim();
//		cboMotivoPenalidadeSelecionada = event.getComponent().getAttributes().get("cboMotivoPenalidadeSelecionada") == null ? "" : event.getComponent().getAttributes().get("cboMotivoPenalidadeSelecionada").toString().trim();
//		txtMatrAnDesig = event.getComponent().getAttributes().get("txtMatrAnDesig") == null ? "" : event.getComponent().getAttributes().get("txtMatrAnDesig").toString().trim();
//		txtNomeAnDesig = event.getComponent().getAttributes().get("txtNomeAnDesig") == null ? "" : event.getComponent().getAttributes().get("txtNomeAnDesig").toString().trim();
//		cboSituacaoRecSelecionada = event.getComponent().getAttributes().get("cboSituacaoRecSelecionada") == null ? "" : event.getComponent().getAttributes().get("cboSituacaoRecSelecionada").toString().trim();
//		cboCanalOrigemSelecionada = event.getComponent().getAttributes().get("cboCanalOrigemSelecionada") == null ? "" : event.getComponent().getAttributes().get("cboCanalOrigemSelecionada").toString().trim();
//		cboEventoSelecionada = event.getComponent().getAttributes().get("cboEventoSelecionada") == null ? "" : event.getComponent().getAttributes().get("cboEventoSelecionada").toString().trim();
		cboCanalSelecionada = event.getComponent().getAttributes().get("cboCanalSelecionada") == null ? "" : event.getComponent().getAttributes().get("cboCanalSelecionada").toString().trim();
		cboPCorpSelecionada = event.getComponent().getAttributes().get("cboPCorpSelecionada") == null ? "" : event.getComponent().getAttributes().get("cboPCorpSelecionada").toString().trim();
		cboPAuxSelecionada = event.getComponent().getAttributes().get("cboPAuxSelecionada") == null ? "" : event.getComponent().getAttributes().get("cboPAuxSelecionada").toString().trim();
		txtPeriPesqIni = event.getComponent().getAttributes().get("txtPeriPesqIni") == null ? "" : event.getComponent().getAttributes().get("txtPeriPesqIni").toString().trim();
		txtDataEventoIni = event.getComponent().getAttributes().get("txtDataEventoIni") == null ? "" : event.getComponent().getAttributes().get("txtDataEventoIni").toString().trim();
		txtDataEventoFin = event.getComponent().getAttributes().get("txtDataEventoFin") == null ? "" : event.getComponent().getAttributes().get("txtDataEventoFin").toString().trim();
		txtDataPocIni = event.getComponent().getAttributes().get("txtDataPocIni") == null ? "" : event.getComponent().getAttributes().get("txtDataPocIni").toString().trim();
		txtDataPocFin = event.getComponent().getAttributes().get("txtDataPocFin") == null ? "" : event.getComponent().getAttributes().get("txtDataPocFin").toString().trim();
		txtCidade = event.getComponent().getAttributes().get("txtCidade") == null ? "" : event.getComponent().getAttributes().get("txtCidade").toString().trim();
		txtCdDepto = event.getComponent().getAttributes().get("txtCdDepto") == null ? "" : event.getComponent().getAttributes().get("txtCdDepto").toString().trim();
//		txtNmDetpo = event.getComponent().getAttributes().get("txtNmDetpo") == null ? "" : event.getComponent().getAttributes().get("txtNmDetpo").toString().trim();
		txtTpPv = event.getComponent().getAttributes().get("txtTpPv") == null ? "" : event.getComponent().getAttributes().get("txtTpPv").toString().trim();
		txtNmPv = event.getComponent().getAttributes().get("txtNmPv") == null ? "" : event.getComponent().getAttributes().get("txtNmPv").toString().trim();
		txtTextoAbertura = event.getComponent().getAttributes().get("txtTextoAbertura") == null ? "" : event.getComponent().getAttributes().get("txtTextoAbertura").toString().trim();
		
	}
	
	public void setObjRsPesquisaOsResultado(List<PesquisaOsResultadoModel> objRsPesquisaOsResultado) {
		ObjRsPesquisaOsResultado = objRsPesquisaOsResultado;
	}

	public String getTxtNrSeqOs() {
		return txtNrSeqOs;
	}

	public void setTxtNrSeqOs(String txtNrSeqOs) {
		this.txtNrSeqOs = txtNrSeqOs;
	}

	public String getCboSituacaoOsSelecionada() {
		return cboSituacaoOsSelecionada;
	}

	public void setCboSituacaoOsSelecionada(String cboSituacaoOsSelecionada) {
		this.cboSituacaoOsSelecionada = cboSituacaoOsSelecionada;
	}

	public String getCboSituacaoOs() {
		return cboSituacaoOs;
	}

	public void setCboSituacaoOs(String cboSituacaoOs) {
		this.cboSituacaoOs = cboSituacaoOs;
	}

	public String getCboFaseOsSelecionada() {
		return cboFaseOsSelecionada;
	}

	public void setCboFaseOsSelecionada(String cboFaseOsSelecionada) {
		this.cboFaseOsSelecionada = cboFaseOsSelecionada;
	}

	public String getCboCriticidadeOsSelecionada() {
		return cboCriticidadeOsSelecionada;
	}

	public void setCboCriticidadeOsSelecionada(String cboCriticidadeOsSelecionada) {
		this.cboCriticidadeOsSelecionada = cboCriticidadeOsSelecionada;
	}

	public String getCriticidadeOs() {
		return criticidadeOs;
	}

	public void setCriticidadeOs(String criticidadeOs) {
		this.criticidadeOs = criticidadeOs;
	}

	public String getTxtCustoOsIni() {
		return txtCustoOsIni;
	}

	public void setTxtCustoOsIni(String txtCustoOsIni) {
		this.txtCustoOsIni = txtCustoOsIni;
	}

	public String getCboCanalSelecionada() {
		return cboCanalSelecionada;
	}

	public void setCboCanalSelecionada(String cboCanalSelecionada) {
		this.cboCanalSelecionada = cboCanalSelecionada;
	}

	public String getCboPCorpSelecionada() {
		return cboPCorpSelecionada;
	}

	public void setCboPCorpSelecionada(String cboPCorpSelecionada) {
		this.cboPCorpSelecionada = cboPCorpSelecionada;
	}

	public String getCboPAuxSelecionada() {
		return cboPAuxSelecionada;
	}

	public void setCboPAuxSelecionada(String cboPAuxSelecionada) {
		this.cboPAuxSelecionada = cboPAuxSelecionada;
	}

	public String getChkInteresseCom() {
		return chkInteresseCom;
	}

	public void setChkInteresseCom(String chkInteresseCom) {
		this.chkInteresseCom = chkInteresseCom;
	}

	public String getChkRessarcimento() {
		return chkRessarcimento;
	}

	public void setChkRessarcimento(String chkRessarcimento) {
		this.chkRessarcimento = chkRessarcimento;
	}

	public String getTxtPeriPesqIni() {
		return txtPeriPesqIni;
	}

	public void setTxtPeriPesqIni(String txtPeriPesqIni) {
		this.txtPeriPesqIni = txtPeriPesqIni;
	}

	public String getTxtPeriPesqFin() {
		return txtPeriPesqFin;
	}

	public void setTxtPeriPesqFin(String txtPeriPesqFin) {
		this.txtPeriPesqFin = txtPeriPesqFin;
	}

	public String getTxtDataEventoIni() {
		return txtDataEventoIni;
	}

	public void setTxtDataEventoIni(String txtDataEventoIni) {
		this.txtDataEventoIni = txtDataEventoIni;
	}

	public String getTxtDataEventoFin() {
		return txtDataEventoFin;
	}

	public void setTxtDataEventoFin(String txtDataEventoFin) {
		this.txtDataEventoFin = txtDataEventoFin;
	}

	public String getTxtDataPocIni() {
		return txtDataPocIni;
	}

	public void setTxtDataPocIni(String txtDataPocIni) {
		this.txtDataPocIni = txtDataPocIni;
	}

	public String getTxtDataPocFin() {
		return txtDataPocFin;
	}

	
	public String verificaExcel;
	
	public void setTxtDataPocFin(String txtDataPocFin) {
		this.txtDataPocFin = txtDataPocFin;
	}

	public String getTxtCidade() {
		return txtCidade;
	}

	public void setTxtCidade(String txtCidade) {
		this.txtCidade = txtCidade;
	}

	public String getTxtEstado() {
		return txtEstado;
	}

	public void setTxtEstado(String txtEstado) {
		this.txtEstado = txtEstado;
	}

	public String getTxtCdDepto() {
		return txtCdDepto;
	}

	public void setTxtCdDepto(String txtCdDepto) {
		this.txtCdDepto = txtCdDepto;
	}

	public String getTxtTpPv() {
		return txtTpPv;
	}

	public void setTxtTpPv(String txtTpPv) {
		this.txtTpPv = txtTpPv;
	}

	public String getTxtCdPv() {
		return txtCdPv;
	}

	public void setTxtCdPv(String txtCdPv) {
		this.txtCdPv = txtCdPv;
	}

	public String getTxtNmPv() {
		return txtNmPv;
	}

	public void setTxtNmPv(String txtNmPv) {
		this.txtNmPv = txtNmPv;
	}

	public String getTxtTextoAbertura() {
		return txtTextoAbertura;
	}

	public void setTxtTextoAbertura(String txtTextoAbertura) {
		this.txtTextoAbertura = txtTextoAbertura;
	}

	public String getStrCpfUsuario() {
		return strCpfUsuario;
	}

	public void setStrCpfUsuario(String strCpfUsuario) {
		this.strCpfUsuario = strCpfUsuario;
	}

	public String getStrCdArea() {
		return strCdArea;
	}

	public void setStrCdArea(String strCdArea) {
		this.strCdArea = strCdArea;
	}

	public String getStrNrOs() {
		return strNrOs;
	}

	public void setStrNrOs(String strNrOs) {
		this.strNrOs = strNrOs;
	}

	public String getStrAction() {
		return strAction;
	}

	public void setStrAction(String strAction) {
		this.strAction = strAction;
	}

	public String getStrXml() {
		return this.strXml;
	}

	public void setStrXml(String strXml) {
		this.strXml = strXml;
	}

	public String getMsgErroPermissao() {
		return msgErroPermissao;
	}

	public void setMsgErroPermissao(String msgErroPermissao) {
		this.msgErroPermissao = msgErroPermissao;
	}

	public void iniciaMsgErro() {
		this.msgErroPermissao = "Permiss�o de acesso � esta Ordem de Servi�o negada!";
	}

	public String getCODIGO() {
		return CODIGO;
	}

	public void setCODIGO(String cODIGO) {
		CODIGO = cODIGO;
	}

	public String getCODIGO_AREA() {
		return CODIGO_AREA;
	}

	public void setCODIGO_AREA(String cODIGO_AREA) {
		CODIGO_AREA = cODIGO_AREA;
	}

	public String getCODIGO_SITUACAO() {
		return CODIGO_SITUACAO;
	}

	public void setCODIGO_SITUACAO(String cODIGO_SITUACAO) {
		CODIGO_SITUACAO = cODIGO_SITUACAO;
	}

	public String getAREA() {
		return AREA;
	}

	public void setAREA(String aREA) {
		AREA = aREA;
	}

	public String getDATA_ABERTURA() {
		return DATA_ABERTURA;
	}

	public void setDATA_ABERTURA(String dATA_ABERTURA) {
		DATA_ABERTURA = dATA_ABERTURA;
	}

	private boolean blnPemissao = false;

	RequestContext context = RequestContext.getCurrentInstance();

	private String strCpfUsuario = "343.263.830-20";



	public boolean verificaPermissaoArea() {
		System.out.println(this.strCdArea);

		if (!CD_CARGO_USUARIO.equals("B70555")) {

			if (!strCdArea.equals(CD_AREA_USUARIO)) {

				this.msgErroPermissao = this.msgErroPermissao
						+ "\n- �rea do usu�rio diferente da �rea da Ordem de Servi�o.";

				return false;
			}
		}

		return true;

	}

	public boolean verificaPermissaoEquipe() {
		System.out.println(this.strCdArea);

		if (strAction.equals("L")) {

			System.out.println("Listar");
			return true;
			// InsereLog strAcao, strCpfUsuario, "", strTxtAdd

		} else if (strAction.equals("C")) {

			System.out.println("Consultar");

			// objAcaoes.ConsultarEquipeOs(strNrOs);

			String strRetorno = "XHY";
			System.out.println(strRetorno.indexOf("XHY", -1));

			// if( ) {
			// FacesContext.getCurrentInstance().getExternalContext().redirect("HY_Erro.asp?strErro="
			// + URLEncoder.encode(strRetorno, "UTF-8");
			// return false;
			// }

			if (!CD_CARGO_USUARIO.equals("B70555")) {

				if (CPF_USUARIO.equals(strCpfUsuario)) {
					this.blnPemissao = true;
				}
			} else {
				this.blnPemissao = true;
			}

			if (blnPemissao == false) {
				msgErroPermissao = msgErroPermissao + "\n- Usu�rio n�o faz parte da equipe atribu�da � OS.";
			}

			return blnPemissao;

		}

		return true;

	}

	public boolean verificaPermissao(String strNrOs) {

		if (strNrOs.equals("")) {
			context.execute("alert('Selecione uma Ordem de Servi�o!'); getOsSelecionada()");
			return false;
		}

		return true;
	}

	public void aprovarOs() {
		if (verificaPermissao(strNrOs)) {
			context.execute("window.open('hyb_AprovarOs.xhtml?strNrSeqOs=" + strNrOs + "' , '_self', '')");
		}
	}

	public void mostrarStatusOs() {
		if (verificaPermissao(strNrOs)) {
			context.execute("window.open('hyb_AlterarStatusOs.xhtml?strNrSeqOs=" + strNrOs + "' , '_self', '')");
		}
	}

	public void mostrarCustoOS() {
		if (verificaPermissao(strNrOs)) {
			context.execute("window.open('hyb_CustoOS.xhtml?strNrSeqOs=" + strNrOs + "' , '_self', '')");
		}
	}

	public void mostrarCapaOs() {
		if (verificaPermissao(strNrOs)) {
			context.execute("window.open('hyb_RelCapaOS.xhtml?strNrSeqOs=" + strNrOs + "' , '_self', '')");
		}
	}

	public void mostrarCartaAprOs() {
		if (verificaPermissao(strNrOs)) {
			context.execute("window.open('hyb_RelCartaApresentacao.xhtml?strNrSeqOs=" + strNrOs + "' , '_self', '')");
		}
	}

	public void mostrarMinutaOs() {
		if (verificaPermissao(strNrOs)) {
			context.execute("window.open('hyb_RelMinuta.xhtml?strNrSeqOs=" + strNrOs + "' , '_self', '')");
		}
	}

	public void mostrarRastreabilidade() {
		if (verificaPermissao(strNrOs)) {
			context.execute("window.open('hyb_Rastreabilidade.xhtml?strNrSeqOs=" + strNrOs + "' , '_self', '')");
		}
	}

	public void alterar(String strNrOs) {

		if (CD_CARGO_USUARIO.equals("B70555")) {
			if (verificaPermissao(strNrOs)) {
				context.execute("window.open('hyb_CadOsMenu.xhtml?strNrSeqOs=" + strNrOs + "' , '_self', '')");
			}
		}
	}

	public void aprovarOS(String strNrOs) {
		if (verificaPermissao(strNrOs)) {
			context.execute("window.open('hyb_AprovarOs.xhtml?strNrSeqOs=" + strNrOs + "' , '_self', '')");
		}
	}

	public void AprovarDadoCont(String strNrOs) {

		if (verificaPermissao(strNrOs)) {
			context.execute("window.open('hyb_Contabilizacao.xhtml?strNrSeqOs=" + strNrOs + "' , '_self', '')");
		}
	}

	public void encerrarOs(String strNrOs) {

		if (verificaPermissao(strNrOs)) {
			context.execute("window.open('hyb_EncerrarOsMenu.xhtml?strNrSeqOs=" + strNrOs + "' , '_self', '')");
		}

	}

	public void imprimirOs(String strNrOs) {

		if (verificaPermissao(strNrOs)) {
			context.execute("window.open('hyb_ImprimirOs.xhtml?strNrSeqOs=" + strNrOs + "' , '_self', '')");
		}
	}

	public void excluir(String strNrOs) {

		if (verificaPermissao(strNrOs)) {
			context.execute("window.open('hyb_ExcluirOs.xhtml?strNrSeqOs=" + strNrOs + "' , '_self', '')");
		}

	}

	public void mostrarDetalheOs(String strNrOs, String CODIGO_AREA) throws IOException {
		FacesContext.getCurrentInstance().getExternalContext()
				.redirect("hyb_CadOsMenu.xhtml?strNrSeqOs="+strNrOs+"&strReadOnly=1&strTitulo=Detalhes OS");
	}

	public void verificaExcel() throws IOException {

		/*
		 * 
		 * strXml = "<param>" + "  <NrOs>" + strNrSeqOs + "</NrOs>" + "  <CdSituacaoOs>"
		 * + strCdSituacaoOs + "</CdSituacaoOs>" + "  <CdFase>" + strCdFaseAnOs +
		 * "</CdFase>" + "  <CdCriticidade>" + strCdCriticidadeOS + "</CdCriticidade>" +
		 * "  <CustoIni>" + strCustoIniOs + "</CustoIni>" + "  <CustoFin>" +
		 * strCustoFinOs + "</CustoFin>" + "  <CpfCnpjEnvolv>" + strCpfCnpjEnvolvido +
		 * "</CpfCnpjEnvolv>" + "  <NomeEnvolv>" + strNomeEnvolvido + "</NomeEnvolv>" +
		 * "  <CdPenal>" + strCdPenalidade + "</CdPenal>" + "  <CdMotPenal>" +
		 * strCdMotivoPelalidade + "</CdMotPenal>" + "  <MatrAnDesig>" +
		 * strMatriculaAnDesig + "</MatrAnDesig>" + "  <NomeAnDesig>" + strNomeAnDesig +
		 * "</NomeAnDesig>" + "  <CdSituRec>" + strCdSituRec + "</CdSituRec>" +
		 * "  <CdCanalOrig>" + strCdCanalOrigem + "</CdCanalOrig>" + "  <CdEvento>" +
		 * strCdEvento + "</CdEvento>" + "  <CdCanal>" + strCdCanal + "</CdCanal>" +
		 * "  <CdPCorp>" + strCdPCorp + "</CdPCorp>" + "  <CdPAux>" + strCdPAux +
		 * "</CdPAux>" + "  <InterCom>" + strInteresseCom + "</InterCom>" + "  <Ress>" +
		 * strRessarcimento + "</Ress>" + "  <DtPeriIni>" + strDtPeriodoPesqIni +
		 * "</DtPeriIni>" + "  <DtPeriFin>" + strDtPeriodoPesqFin + "</DtPeriFin>" +
		 * "  <DtEventoIni>" + strDtEventoIni + "</DtEventoIni>" + "  <DtEventoFin>" +
		 * strDtEventoFin + "</DtEventoFin>" + "  <DtRegIni>" + strDtRegistroIni +
		 * "</DtRegIni>" + "  <DtRegFin>" + strDtRegistroFin + "</DtRegFin>" +
		 * "  <DtPocIni>" + strDtPocIni + "</DtPocIni>" + "  <DtPocFin>" + strDtPocFin +
		 * "</DtPocFin>" + "  <NmCidade>" + strNmCidade + "</NmCidade>" + "  <NmEstado>"
		 * + strNmEstado + "</NmEstado>" + "  <NmDepto>" + strNmDepto + "</NmDepto>" +
		 * "  <TpPv>" + strTpPv + "</TpPv>" + "  <CdPv>" + strCdPv + "</CdPv>" +
		 * "  <NmPv>" + strNmPv + "</NmPv>" + "  <TxtAbertura>" + strTextoAbertura +
		 * "</TxtAbertura>" + "</param>";
		 * 
		 */
		FacesContext.getCurrentInstance().getExternalContext()
				.redirect("hyb_Verifica_Excel_Imprime.xhtml?strXml=" + this.strXml);
	}

}
