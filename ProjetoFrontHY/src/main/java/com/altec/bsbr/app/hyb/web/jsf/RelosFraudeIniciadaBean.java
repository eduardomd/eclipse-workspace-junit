package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jfree.ui.NumberCellRenderer;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.RelOsFraudeIniciadaModel;
import com.altec.bsbr.app.jab.hyb.webclient.XHYRelatorios.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYRelatorios.XHYRelatoriosEndPoint;
import com.ibm.icu.text.NumberFormat;

@Component("relosFraudeIniciadaBean")
@Scope("session")
public class RelosFraudeIniciadaBean {

	@Autowired
	private XHYRelatoriosEndPoint objRelat;

	private String strArea;
	private String pCodArea;
	private String strTpRel;
	private String strNmRelat;
	private String strDtIni;
	private String strDtFim;
	private String intCanal;

	private int quantidadeTotal;
	private Double valorTotal;

	private int finalTabela;

	private List<RelOsFraudeIniciadaModel> objRsRelat;
	private List<String> tiposDeFraudes;

	@PostConstruct
	public void init() {
		objRsRelat = new ArrayList<RelOsFraudeIniciadaModel>();
		tiposDeFraudes = new ArrayList<String>();

		FacesContext fc = FacesContext.getCurrentInstance();
		try {
			Map<String, String> params = fc.getExternalContext().getRequestParameterMap();

			this.pCodArea = params.get("pCodArea");
			this.strDtIni = params.get("pDtIni");
			this.strDtFim = params.get("pDtFim");
			this.strNmRelat = params.get("pNmRel");
		} catch (NullPointerException ex) {
			ex.printStackTrace();
		}
		relatoriosFraudesAbertas();
	}

	public void relatoriosFraudesAbertas() {
		JSONArray pcursorObjRs = new JSONArray();
		try {
			String objRs = objRelat.fnRelFraudeAberto(this.strDtIni, this.strDtFim, this.pCodArea);
			JSONObject objResTemp = new JSONObject(objRs);
			pcursorObjRs = objResTemp.getJSONArray("PCURSOR");

		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		for (int i = 0; i < pcursorObjRs.length(); i++) {
			JSONObject temp = pcursorObjRs.getJSONObject(i);

			objRsRelat.add(new RelOsFraudeIniciadaModel(temp.isNull("EVENTO") ? "" : temp.get("EVENTO").toString(),
					temp.isNull("CD_CNAL") ? "" : temp.get("CD_CNAL").toString(),
					temp.isNull("BANCO") ? "" : temp.get("BANCO").toString(),
					temp.isNull("QUANTIDADE") ? "" : temp.get("QUANTIDADE").toString(),
					temp.isNull("VL_PREJ") ? "" : temp.get("VL_PREJ").toString(),
					temp.isNull("VALOR") ? "" : temp.get("VALOR").toString(),
					temp.isNull("CANAL") ? "" : temp.get("CANAL").toString(),
					temp.isNull("VALOR") ? "" : formataNumero(temp.get("VALOR").toString()),
					temp.isNull("VL_PREJ") ? "" : formataNumero(temp.get("VL_PREJ").toString())));
			if (!tiposDeFraudes.contains(objRsRelat.get(i).getEVENTO())) {
				tiposDeFraudes.add(objRsRelat.get(i).getEVENTO());
			}

		}

		cleanSession();
	}

	public void cleanSession() {
		FacesContext context = FacesContext.getCurrentInstance();
		if (context.getExternalContext().getSessionMap().get("relatorioosBean") != null) {
			context.getExternalContext().getSessionMap().remove("relatorioosBean");
		}
	}

	public void postProcessExcel(Object doc) {

		try {
			XSSFWorkbook wb = (XSSFWorkbook) doc;
			XSSFSheet sheet = wb.getSheetAt(0);
			sheet.setDisplayGridlines(false);

			formatarCabecalho(wb, sheet);

			if (!objRsRelat.isEmpty() && objRsRelat != null) {
				formatarCabecalhoTabela(wb, sheet);
				formatarLinhasDetalhe(wb, sheet);
				estiloLinhaTotal(wb, sheet);
				criarRodape(wb, sheet);
				redefinirColunas(wb, 7, false);
				objRsRelat.clear();
			} else {
				criarRodape(wb, sheet);
				redefinirColunas(wb, 8, true);
				formatarDetalheVazio(wb, sheet);
			}

		} catch (Exception e) {
			System.out.println(e);
		}

	}

	public void redefinirColunas(XSSFWorkbook wb, int n, Boolean merged) {
		XSSFSheet sheet = wb.getSheetAt(0);
		for (int i = 0; i <= sheet.getRow(n).getLastCellNum(); i++) {
			sheet.autoSizeColumn(i, merged);
		}

	}

	private void formatarCabecalho(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyle1 = wb.createCellStyle();
		headerStyle1.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle1.setWrapText(true);
		headerStyle1.setBorderBottom(CellStyle.BORDER_NONE);
		headerStyle1.setBorderLeft(CellStyle.BORDER_NONE);
		headerStyle1.setBorderRight(CellStyle.BORDER_NONE);
		headerStyle1.setBorderTop(CellStyle.BORDER_NONE);
		Font fontCenter = wb.createFont();
		fontCenter.setFontHeightInPoints((short) 14);
		fontCenter.setFontName("Calibri");
		fontCenter.setColor(IndexedColors.BLACK.getIndex());
		fontCenter.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		headerStyle1.setFont(fontCenter);

		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setWrapText(true);
		headerStyle.setBorderBottom(CellStyle.BORDER_NONE);
		headerStyle.setBorderLeft(CellStyle.BORDER_NONE);
		headerStyle.setBorderRight(CellStyle.BORDER_NONE);
		headerStyle.setBorderTop(CellStyle.BORDER_NONE);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 14);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(font);

		XSSFRow primeiraLinha = sheet.getRow(1);
		if (primeiraLinha != null) {
			XSSFCell primeiraLinhaCell = primeiraLinha.getCell(0);
			primeiraLinhaCell.setCellStyle(headerStyle);
			primeiraLinha.setHeightInPoints((short) 19);
		}
		XSSFRow segundaLinha = sheet.getRow(3);
		if (segundaLinha != null) {
			XSSFCell segundaLinhaCell = segundaLinha.getCell(0);
			segundaLinhaCell.setCellStyle(headerStyle1);
			segundaLinha.setHeightInPoints((short) 19);
		}

	}

	private void formatarCabecalhoTabela(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setWrapText(true);
		headerStyle.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyle.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyle.setBorderRight(CellStyle.BORDER_THIN);
		headerStyle.setBorderTop(CellStyle.BORDER_THIN);
		headerStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font fontCenter = wb.createFont();
		fontCenter.setFontHeightInPoints((short) 11);
		fontCenter.setFontName("Calibri");
		fontCenter.setColor(IndexedColors.WHITE.getIndex());
		fontCenter.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(fontCenter);

		XSSFRow linhaCabecalhoTabela = sheet.getRow(7);
		linhaCabecalhoTabela.setHeightInPoints(15);
		if (linhaCabecalhoTabela != null) {
			for (int i = 0; i <= linhaCabecalhoTabela.getLastCellNum(); i++) {
				XSSFCell celulaHeader = linhaCabecalhoTabela.getCell(i);
				if (celulaHeader != null) {
					celulaHeader.setCellStyle(headerStyle);
				}
			}
		}

	}

	private void formatarDetalheVazio(XSSFWorkbook wb, XSSFSheet sheet) {

		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setWrapText(true);
		Font fontCenter = wb.createFont();
		fontCenter.setFontHeightInPoints((short) 11);
		fontCenter.setFontName("Calibri");
		fontCenter.setColor(IndexedColors.RED.getIndex());
		fontCenter.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(fontCenter);
		CellRangeAddress detalheVazio = new CellRangeAddress(9, 9, 0, 3);
		sheet.addMergedRegion(detalheVazio);

		sheet.removeRow(sheet.getRow(7));
		sheet.removeRow(sheet.getRow(8));
		XSSFRow linhaVazia = sheet.getRow(9);
		if (linhaVazia == null) {
			linhaVazia = sheet.createRow(9);
		}
		XSSFCell celulaVazia = linhaVazia.createCell(0);
		linhaVazia.setHeightInPoints((short) 20);
		celulaVazia.setCellValue("Nenhum registro encontrado para o período selecionado.");
		celulaVazia.setCellStyle(headerStyle);
		sheet.shiftRows(9, 10, -1);

	}

	private void formatarLinhasDetalhe(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_LEFT);
		headerStyle.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyle.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyle.setBorderRight(CellStyle.BORDER_THIN);
		headerStyle.setBorderTop(CellStyle.BORDER_THIN);
		Font fontCenter = wb.createFont();
		fontCenter.setFontHeightInPoints((short) 11);
		fontCenter.setFontName("Calibri");
		fontCenter.setColor(IndexedColors.BLACK.getIndex());
		fontCenter.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		headerStyle.setFont(fontCenter);

		XSSFCellStyle headerStyleCenter = wb.createCellStyle();
		headerStyleCenter.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyleCenter.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleCenter.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleCenter.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleCenter.setBorderTop(CellStyle.BORDER_THIN);
		Font fontCenterCenter = wb.createFont();
		fontCenterCenter.setFontHeightInPoints((short) 11);
		fontCenterCenter.setFontName("Calibri");
		fontCenterCenter.setColor(IndexedColors.BLACK.getIndex());
		fontCenterCenter.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		headerStyleCenter.setFont(fontCenterCenter);

		XSSFCellStyle headerStyleCenterSubTitulo = wb.createCellStyle();
		headerStyleCenterSubTitulo.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyleCenterSubTitulo.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleCenterSubTitulo.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleCenterSubTitulo.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleCenterSubTitulo.setBorderTop(CellStyle.BORDER_THIN);
		Font fontCenterCenterSubTitulo = wb.createFont();
		fontCenterCenterSubTitulo.setFontHeightInPoints((short) 11);
		fontCenterCenterSubTitulo.setFontName("Calibri");
		fontCenterCenterSubTitulo.setColor(IndexedColors.BLACK.getIndex());
		fontCenterCenterSubTitulo.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyleCenterSubTitulo.setFont(fontCenterCenterSubTitulo);

		XSSFCellStyle headerStyleGrey = wb.createCellStyle();
		headerStyleGrey.setAlignment(CellStyle.ALIGN_LEFT);
		headerStyleGrey.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleGrey.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleGrey.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleGrey.setBorderTop(CellStyle.BORDER_THIN);
		headerStyleGrey.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		headerStyleGrey.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font fontCenterGrey = wb.createFont();
		fontCenterGrey.setFontHeightInPoints((short) 11);
		fontCenterGrey.setFontName("Calibri");
		fontCenterGrey.setColor(IndexedColors.WHITE.getIndex());
		fontCenterGrey.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyleGrey.setFont(fontCenterGrey);

		XSSFCellStyle headerStyleGreyCenter = wb.createCellStyle();
		headerStyleGreyCenter.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyleGreyCenter.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleGreyCenter.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleGreyCenter.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleGreyCenter.setBorderTop(CellStyle.BORDER_THIN);
		headerStyleGreyCenter.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		headerStyleGreyCenter.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font fontCenterGreyCenter = wb.createFont();
		fontCenterGreyCenter.setFontHeightInPoints((short) 11);
		fontCenterGreyCenter.setFontName("Calibri");
		fontCenterGreyCenter.setColor(IndexedColors.WHITE.getIndex());
		fontCenterGreyCenter.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyleGreyCenter.setFont(fontCenterGreyCenter);

		int linhaInicial = 8;
		int linhasDetalhe = objRsRelat.size();
		int linhaFinal = linhaInicial + linhasDetalhe;
		int linhaTpFraude = linhaInicial;
		for (int i = 0; i < tiposDeFraudes.size(); i++) {
			String fraude = tiposDeFraudes.get(i);

			CellRangeAddress detalheFraude = new CellRangeAddress(linhaTpFraude, linhaTpFraude, 0, 3);
			sheet.addMergedRegion(detalheFraude);

			XSSFRow linhaVazia = sheet.getRow(linhaTpFraude);
			if (linhaVazia == null) {
				linhaVazia = sheet.createRow(linhaTpFraude);
			}
			XSSFCell celulaInfo = linhaVazia.createCell(0);
			celulaInfo.setCellValue(linhaTpFraude);
			celulaInfo.setCellStyle(headerStyleCenterSubTitulo);
			celulaInfo.setCellValue(fraude);

			XSSFCell celulaVazia = linhaVazia.createCell(4);
			celulaVazia.setCellValue(linhaTpFraude);
			celulaVazia.setCellStyle(headerStyle);
			celulaVazia.setCellValue("");

			RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, detalheFraude, sheet, wb);
			RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, detalheFraude, sheet, wb);
			RegionUtil.setBorderRight(CellStyle.BORDER_THIN, detalheFraude, sheet, wb);
			RegionUtil.setBorderTop(CellStyle.BORDER_THIN, detalheFraude, sheet, wb);
			linhaTpFraude++;

			for (int j = 0; j < objRsRelat.size(); j++) {
				if (fraude.equals(objRsRelat.get(j).getEVENTO())) {

					// Coloca a informação
					sheet.shiftRows(linhaTpFraude, linhaTpFraude + 1, 1);

					XSSFRow linha = sheet.getRow(linhaTpFraude);
					if (linha == null) {
						linha = sheet.createRow(linhaTpFraude);
					}
					XSSFCell celula = linha.createCell(0);
					celula.setCellValue(linhaTpFraude);
					celula.setCellStyle(headerStyle);
					celula.setCellValue(objRsRelat.get(j).getBANCO());

					XSSFCell celula2 = linha.createCell(1);
					celula2.setCellValue(linhaTpFraude);
					celula2.setCellStyle(headerStyleCenter);
					celula2.setCellValue(objRsRelat.get(j).getQUANTIDADE());

					XSSFCell celula3 = linha.createCell(2);
					celula3.setCellValue(linhaTpFraude);
					celula3.setCellStyle(headerStyleCenter);
					celula3.setCellValue(objRsRelat.get(j).getVL_PREJ());

					NumberFormat formatacao = NumberFormat.getCurrencyInstance(Locale.getDefault());
					XSSFCell celula4 = linha.createCell(3);
					celula4.setCellValue(linhaTpFraude);
					celula4.setCellStyle(headerStyleCenter);
					celula4.setCellValue(
							NumberFormat.getCurrencyInstance().format(Double.valueOf(objRsRelat.get(j).getVALOR()))
									.replace(formatacao.getCurrency().getSymbol(), ""));

					XSSFCell celula5 = linha.createCell(4);
					celula5.setCellValue(linhaTpFraude);
					celula5.setCellStyle(headerStyle);
					celula5.setCellValue(objRsRelat.get(j).getCANAL());

					linhaTpFraude++;

					// Coloca o total da informação
					sheet.shiftRows(linhaTpFraude, linhaTpFraude + 1, 1);

					XSSFRow linhaTotal = sheet.getRow(linhaTpFraude);
					if (linhaTotal == null) {
						linhaTotal = sheet.createRow(linhaTpFraude);
					}
					XSSFCell celulaTotal = linhaTotal.createCell(0);
					celulaTotal.setCellValue(linhaTpFraude);
					celulaTotal.setCellStyle(headerStyleGrey);
					celulaTotal.setCellValue("Total");

					XSSFCell celulaTotal2 = linhaTotal.createCell(1);
					celulaTotal2.setCellValue(linhaTpFraude);
					celulaTotal2.setCellStyle(headerStyleGreyCenter);
					celulaTotal2.setCellValue(quantidadeTotal(objRsRelat.get(j).getCANAL()));

					XSSFCell celulaTotal3 = linhaTotal.createCell(2);
					celulaTotal3.setCellValue(linhaTpFraude);
					celulaTotal3.setCellStyle(headerStyleGreyCenter);
					celulaTotal3.setCellValue("");
					XSSFCell celulaTotal4 = linhaTotal.createCell(3);
					celulaTotal4.setCellValue(linhaTpFraude);
					celulaTotal4.setCellStyle(headerStyleGreyCenter);
					celulaTotal4.setCellValue(valorTotal(objRsRelat.get(j).getCANAL()));

					XSSFCell celulaTotal5 = linhaTotal.createCell(4);
					celulaTotal5.setCellValue(linhaTpFraude);
					celulaTotal5.setCellStyle(headerStyleGrey);
					celulaTotal5.setCellValue("");
					linhaTpFraude++;
				}
			}
		}
		finalTabela = linhaTpFraude;

	}

	private void criarRodape(XSSFWorkbook wb, XSSFSheet sheet) {
		Font fontCenter;
		/* Criação, inclusão e formatação dos dados do footer do excel */
		XSSFRow ultimaLinha = sheet.createRow(sheet.getLastRowNum() + 1);

		XSSFCellStyle footerStyle = wb.createCellStyle();
		footerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		footerStyle.setWrapText(true);
		fontCenter = footerStyle.getFont();
		fontCenter.setFontHeight((short) 150);
		fontCenter.setFontName("Arial");
		fontCenter.setColor(IndexedColors.BLACK.getIndex());
		fontCenter.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		footerStyle.setFont(fontCenter);

		XSSFCell cell1 = ultimaLinha.createCell(0);
		cell1.setCellValue(now());
		cell1.setCellStyle(footerStyle);

		XSSFCell cell2 = ultimaLinha.createCell(1);
		cell2.setCellValue("Superintêndencia de Ocorrências Especiais");
		cell2.setCellStyle(footerStyle);

		CellRangeAddress region2 = new CellRangeAddress(ultimaLinha.getRowNum(), ultimaLinha.getRowNum(), 1, 4);
		sheet.addMergedRegion(region2);
		if (objRsRelat.isEmpty() || objRsRelat == null) {
			sheet.shiftRows(14, 15, -3);
		}
	}

	private void estiloLinhaTotal(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle linhaStyle = wb.createCellStyle();
		linhaStyle.setAlignment(CellStyle.ALIGN_LEFT);
		linhaStyle.setBorderBottom(CellStyle.BORDER_THIN);
		linhaStyle.setBorderLeft(CellStyle.BORDER_THIN);
		linhaStyle.setBorderRight(CellStyle.BORDER_THIN);
		linhaStyle.setBorderTop(CellStyle.BORDER_THIN);
		linhaStyle.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		linhaStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.WHITE.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		linhaStyle.setFont(font);

		XSSFCellStyle linhaStyleCenter = wb.createCellStyle();
		linhaStyleCenter.setAlignment(CellStyle.ALIGN_CENTER);
		linhaStyleCenter.setBorderBottom(CellStyle.BORDER_THIN);
		linhaStyleCenter.setBorderLeft(CellStyle.BORDER_THIN);
		linhaStyleCenter.setBorderRight(CellStyle.BORDER_THIN);
		linhaStyleCenter.setBorderTop(CellStyle.BORDER_THIN);
		linhaStyleCenter.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		linhaStyleCenter.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font fontCenter = wb.createFont();
		fontCenter.setFontHeightInPoints((short) 11);
		fontCenter.setFontName("Calibri");
		fontCenter.setColor(IndexedColors.WHITE.getIndex());
		fontCenter.setBoldweight(Font.BOLDWEIGHT_BOLD);
		linhaStyleCenter.setFont(fontCenter);

		XSSFRow linhaTotalTabela = sheet.getRow(finalTabela + 1);
		if (linhaTotalTabela == null) {
			linhaTotalTabela = sheet.createRow(finalTabela + 1);
		}

		XSSFCell primeiraLinhaCell = linhaTotalTabela.getCell(0);
		if (primeiraLinhaCell == null) {
			primeiraLinhaCell = linhaTotalTabela.createCell(0);
		}
		primeiraLinhaCell.setCellStyle(linhaStyle);
		primeiraLinhaCell.setCellValue("Total Geral:");
		XSSFCell primeiraLinhaCell1 = linhaTotalTabela.getCell(1);
		if (primeiraLinhaCell1 == null) {
			primeiraLinhaCell1 = linhaTotalTabela.createCell(1);
		}
		primeiraLinhaCell1.setCellStyle(linhaStyleCenter);
		primeiraLinhaCell1.setCellValue(quantidadeTotalGeral());

		XSSFCell primeiraLinhaCell2 = linhaTotalTabela.getCell(2);
		if (primeiraLinhaCell2 == null) {
			primeiraLinhaCell2 = linhaTotalTabela.createCell(2);
		}
		primeiraLinhaCell2.setCellStyle(linhaStyleCenter);
		primeiraLinhaCell2.setCellValue("");

		XSSFCell primeiraLinhaCell3 = linhaTotalTabela.getCell(3);
		if (primeiraLinhaCell3 == null) {
			primeiraLinhaCell3 = linhaTotalTabela.createCell(3);
		}
		primeiraLinhaCell3.setCellStyle(linhaStyleCenter);
		primeiraLinhaCell3.setCellValue(valorTotalGeral());

		XSSFCell primeiraLinhaCell4 = linhaTotalTabela.getCell(4);
		if (primeiraLinhaCell4 == null) {
			primeiraLinhaCell4 = linhaTotalTabela.createCell(4);
		}
		primeiraLinhaCell4.setCellStyle(linhaStyle);
		primeiraLinhaCell4.setCellValue("");

		sheet.shiftRows(finalTabela + 1, finalTabela + 2, -1);

	}

	public Double calculateValorTotal() {
		objRsRelat.forEach((item) -> {
			this.valorTotal += Double.parseDouble(item.getVALOR());
		});

		return this.valorTotal;
	}

	public String formataNumero(String number) {
		String result = "";
		if (number != null && !number.isEmpty()) {
			DecimalFormat value = new DecimalFormat("###,##0.00");
			result = value.format(Float.parseFloat(number));
			String fp = result.substring(0, result.length() - 3);
			String sp = result.substring(result.length() - 3);
			result = fp.replace(",", ".") + sp.replace(".", ",");
		}

		if (result.equals("0,00")) {
			return "0";
		}

		return result;
	}

	public String quantidadeTotal(String canal) {
		this.quantidadeTotal = 0;
		objRsRelat.forEach((item) -> {
			if (canal.equals(item.getCANAL())) {

				this.quantidadeTotal += Integer.parseInt(item.getQUANTIDADE());
			}
		});
		return Integer.toString(this.quantidadeTotal);
	}

	public String valorTotal(String canal) {
		this.valorTotal = 0.00;
		objRsRelat.forEach((item) -> {
			if (canal.equals(item.getCANAL())) {

				this.valorTotal += Integer.parseInt(item.getVALOR());
			}
		});
		return formataNumero(Double.toString(this.valorTotal));
	}

	public String quantidadeTotalGeral() {
		this.quantidadeTotal = 0;
		objRsRelat.forEach((item) -> {
			this.quantidadeTotal += Integer.parseInt(item.getQUANTIDADE());
		});
		return Integer.toString(this.quantidadeTotal);
	}

	public String valorTotalGeral() {
		this.valorTotal = 0.00;
		objRsRelat.forEach((item) -> {
			this.valorTotal += Integer.parseInt(item.getVALOR());
		});
		return formataNumero(Double.toString(this.valorTotal));
	}

	public String now() {
		DateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		Date date = new Date();
		return dateformat.format(date);
	}

	public int size() {
		return objRsRelat.size();
	}

	/*
	 * private ExcelOptions excelOpt; public void xlsFormat(Object document) {
	 * 
	 * }
	 */

	public String getStrArea() {
		return strArea;
	}

	public void setStrArea(String strArea) {
		this.strArea = strArea;
	}

	public String getStrTpRel() {
		return strTpRel;
	}

	public void setStrTpRel(String strTpRel) {
		this.strTpRel = strTpRel;
	}

	public String getStrNmRelat() {
		return strNmRelat;
	}

	public void setStrNmRelat(String strNmRelat) {
		this.strNmRelat = strNmRelat;
	}

	public String getIntCanal() {
		return intCanal;
	}

	public void setIntCanal(String intCanal) {
		this.intCanal = intCanal;
	}

	public String getpCodArea() {
		return pCodArea;
	}

	public void setpCodArea(String pCodArea) {
		this.pCodArea = pCodArea;
	}

	public String getStrDtIni() {
		return strDtIni;
	}

	public void setStrDtIni(String strDtIni) {
		this.strDtIni = strDtIni;
	}

	public String getStrDtFim() {
		return strDtFim;
	}

	public void setStrDtFim(String strDtFim) {
		this.strDtFim = strDtFim;
	}

	public int getQuantidadeTotal() {
		return quantidadeTotal;
	}

	public void setQuantidadeTotal(int quantidadeTotal) {
		this.quantidadeTotal = quantidadeTotal;
	}

	public Double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}

	public List<RelOsFraudeIniciadaModel> getObjRsRelat() {
		return objRsRelat;
	}

	public void setObjRsRelat(List<RelOsFraudeIniciadaModel> objRsRelat) {
		this.objRsRelat = objRsRelat;
	}
}
