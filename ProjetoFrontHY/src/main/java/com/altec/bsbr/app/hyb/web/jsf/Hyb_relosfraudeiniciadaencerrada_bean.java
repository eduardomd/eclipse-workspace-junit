package com.altec.bsbr.app.hyb.web.jsf;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.altec.bsbr.app.hyb.dto.Hyb_relosfraudeiniciadaencerrada_model;

@ManagedBean(name="hyb_relosfraudeiniciadaencerrada_bean")
@ViewScoped
public class Hyb_relosfraudeiniciadaencerrada_bean {
	
	private String strTpCol1;
	private String strTpCol2;
	private String strTpCol3;
	private String strTpCol4;
	private String strTable;
	private String strResult;
	private int intMaxCols;
	private Date now;
	
	private Object pArea = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("pArea");
	private Object pCodArea = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("pCodArea");
	private Object strTpRel = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("pTpRel");
	private Object strNmRelat = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("pNmRel");
	private Object strDtIni = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("pDtIni");
	private Object strDtFim = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("pDtFim");
	private Object intCanal = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("pCdCanal");
	
	private String html;
	
	private Hyb_relosfraudeiniciadaencerrada_model objRsRelat;
	
	public Hyb_relosfraudeiniciadaencerrada_bean() {
		this.setNow(Calendar.getInstance().getTime());
		objRsRelat = new Hyb_relosfraudeiniciadaencerrada_model();
		this.setStrTpCol1("Banco");
		this.setStrTpCol2("Quantidade");
		this.setStrTpCol3("Valor Envolvido");
		this.setStrTpCol4("Canal");
		
		objRsRelat.setBANCO("TEST BANCO");
		objRsRelat.setCANAL("TEST CANAL");
		objRsRelat.setCD_CNAL("2");
		objRsRelat.setEVENTO("EVENTO TEST");
		objRsRelat.setQUANTIDADE(3);
		objRsRelat.setVALOR(100);
		
		List<String> arrCols = new ArrayList<String>();
		arrCols.add(this.getStrTpCol1());
		arrCols.add(this.getStrTpCol2());
		arrCols.add(this.getStrTpCol3());
		arrCols.add(this.getStrTpCol4());
		
		this.setIntMaxCols(arrCols.size());
		
		String x = fnGeraRelatorioFraudeAberto(arrCols, this.getIntMaxCols(), objRsRelat);
		this.setHtml(x);
		
		int mockEOF = 1;
		
		if(mockEOF > 0) {
			this.setStrResult(fnGeraRelatorioFraudeAberto (arrCols, intMaxCols, objRsRelat));
		} else {
			this.setStrResult("<table border=0><tr><td colspan='" + intMaxCols + "' align='center' ><font color='red'><b>Nenhum registro encontrado para o per�odo selecionado.</b></font></td></tr></table>");
		}
		
	}
	
	public Hyb_relosfraudeiniciadaencerrada_bean(String strTpCol1, String strTpCol2, String strTpCol3, String strTpCol4,
			String strTable, String html, Hyb_relosfraudeiniciadaencerrada_model objRsRelat) {
		this.strTpCol1 = strTpCol1;
		this.strTpCol2 = strTpCol2;
		this.strTpCol3 = strTpCol3;
		this.strTpCol4 = strTpCol4;
		this.strTable = strTable;
		this.html = html;
		this.objRsRelat = objRsRelat;
	}

	public int getIntMaxCols() {
		return intMaxCols;
	}
	public void setIntMaxCols(int intMaxCols) {
		this.intMaxCols = intMaxCols;
	}
	public Date getNow() {
		return now;
	}
	public void setNow(Date now) {
		this.now = now;
	}
	public Object getpArea() {
		return pArea;
	}
	public void setpArea(Object pArea) {
		this.pArea = pArea;
	}
	public Object getpCodArea() {
		return pCodArea;
	}
	public void setpCodArea(Object pCodArea) {
		this.pCodArea = pCodArea;
	}
	public Object getStrTpRel() {
		return strTpRel;
	}
	public void setStrTpRel(Object strTpRel) {
		this.strTpRel = strTpRel;
	}
	public Object getStrNmRelat() {
		return strNmRelat;
	}
	public void setStrNmRelat(Object strNmRelat) {
		this.strNmRelat = strNmRelat;
	}
	public Object getStrDtIni() {
		return strDtIni;
	}
	public void setStrDtIni(Object strDtIni) {
		this.strDtIni = strDtIni;
	}
	public Object getStrDtFim() {
		return strDtFim;
	}
	public void setStrDtFim(Object strDtFim) {
		this.strDtFim = strDtFim;
	}
	public Object getIntCanal() {
		return intCanal;
	}
	public void setIntCanal(Object intCanal) {
		this.intCanal = intCanal;
	}
	public Hyb_relosfraudeiniciadaencerrada_model getObjRsRelat() {
		return objRsRelat;
	}
	public void setObjRsRelat(Hyb_relosfraudeiniciadaencerrada_model objRsRelat) {
		this.objRsRelat = objRsRelat;
	}
	public String getStrResult() {
		return strResult;
	}
	public void setStrResult(String strResult) {
		this.strResult = strResult;
	}
	public String getHtml() {
		return html;
	}
	public void setHtml(String html) {
		this.html = html;
	}
	public String getStrTable() {
		return strTable;
	}
	public void setStrTable(String strTable) {
		this.strTable = strTable;
	}
	public String getStrTpCol1() {
		return strTpCol1;
	}
	public void setStrTpCol1(String strTpCol1) {
		this.strTpCol1 = strTpCol1;
	}
	public String getStrTpCol2() {
		return strTpCol2;
	}
	public void setStrTpCol2(String strTpCol2) {
		this.strTpCol2 = strTpCol2;
	}
	public String getStrTpCol3() {
		return strTpCol3;
	}
	public void setStrTpCol3(String strTpCol3) {
		this.strTpCol3 = strTpCol3;
	}
	public String getStrTpCol4() {
		return strTpCol4;
	}
	public void setStrTpCol4(String strTpCol4) {
		this.strTpCol4 = strTpCol4;
	}
	
	public String fnGeraRelatorioFraudeAberto(List<String> arrCols, int intMaxCols, Hyb_relosfraudeiniciadaencerrada_model objRsRelat) {
		System.out.println("------ FUNCTION ------");
		int intEvento = 0;
	    int intCountTbs = 0;
	    int intCountSeg = 0;
	    int dblSUMEnv = 0;
	    int intCanal = Integer.parseInt(objRsRelat.getCD_CNAL());
	    int intVeryTpCanal = intCanal;
	    int intCountSegTOTAL = 0;
	    int dblSUMEnvTOTAL = 0;
	    int intAuxTotal;
	    int dblAuxTotal;
	    String strEvento = objRsRelat.getEVENTO();

	    List<String> arrTables = new ArrayList<String>();
	    
	    this.strTable = "<table border=1 width='600px'>";
	    this.strTable += "<tr style='background-color:Red; color:White; font-weight:bold'>";
	    
	    for (int intCount = 0; intCount < intMaxCols; intCount++) {
	    	System.out.println("first for");
	    	this.strTable += "<td align='center' valign='middle' nowrap>" + arrCols.get(intCount) + "</td>";
	    }
	    
	    this.strTable += "</tr>";
	    
	    if(intEvento == 0) {
	    	System.out.println("intEvento == 0");
	    	strTable += "<tr>";
	    	strTable += "<td colspan=" + intMaxCols + " align=center><b>" + objRsRelat.getEVENTO() + "</b></td>";
	    	strTable += "<tr>";
	    }
	    
	    if(intCanal == Integer.parseInt(objRsRelat.getCD_CNAL())) {
	    	System.out.println("intCanal == getCD_CNAL"); 
	    	if(strEvento != objRsRelat.getEVENTO()) {
	    		System.out.println("strEvento != getEVENTO");
	    		strTable += "<tr>";
	    		strTable += "<td colspan=" + intMaxCols + " align=center><b>" + objRsRelat.getEVENTO() + "</b></td>";
	    		strTable += "<tr>";
	    	}
	    	
	    	strTable += "<tr>";
            strTable += "<td>" + objRsRelat.getBANCO() + "</td>";
            strTable += "<td align='center'>" + objRsRelat.getQUANTIDADE() + "</td>";
            strTable += "<td align='center'>" + String.format("%.2f", (double)objRsRelat.getVALOR()) + "</td>";
            strTable += "<td>" + objRsRelat.getCANAL() + "</TD>";
            strTable += "</tr>";
            
            intVeryTpCanal = intCanal;
            intCountSeg += objRsRelat.getQUANTIDADE();
            dblSUMEnv += objRsRelat.getVALOR();
	    } else {
	    	System.out.println("intCanal != getCD_CNAL"); 
	    	strTable += "<tr style='background-color:Gray; color:White; font-weight:bold'>";
            strTable += "<td>Total</td><td align='center'>" + intCountSeg + "</td><td align='center'>" + String.format("%.2f", (double)dblSUMEnv) + "</td><td>&nbsp;</TD>";
            strTable +=  "</tr>";
            
            intCountSegTOTAL += intCountSeg;
            dblSUMEnvTOTAL += dblSUMEnv;
            
            intCountSeg = 0;
            dblSUMEnv = 0;
            
            if(strEvento != objRsRelat.getEVENTO()) {
            	System.out.println("strEvento != getEVENTO"); 
            	 strTable += "<tr>";
                 strTable += "<td colspan=" + intMaxCols + " align=center><b>" + objRsRelat.getEVENTO() + "</b></td>";        
                 strTable += "</tr>";
            }
            
            strTable += "<tr>";
            strTable += "<td>" + objRsRelat.getBANCO() + "</td>";
            strTable += "<td align='center'>" + objRsRelat.getQUANTIDADE() + "</td>";
            strTable += "<td align='center'>" + String.format("%.2f", (double)objRsRelat.getVALOR()) + "</td>";
            strTable += "<td>" + objRsRelat.getCANAL() + "</TD>";
            strTable += "</tr>";
            
            intCountSeg = objRsRelat.getQUANTIDADE();
            dblSUMEnv = objRsRelat.getVALOR();
	    }

	    strEvento = objRsRelat.getEVENTO();
	    intCanal = Integer.parseInt(objRsRelat.getCD_CNAL());
	    intEvento = 1;
	    // End While
	    
	    strTable += "<tr style='background-color:Gray; color:White; font-weight:bold'>";
	    strTable += "<td>Total</td><td align='center'>" + intCountSeg + "</td><td align='center'>" + String.format("%.2f", (double)dblSUMEnv) + "</td><td>&nbsp;</td>";
	    strTable += "</tr>";
	    
	    intAuxTotal = intCountSeg + intCountSegTOTAL;
	    dblAuxTotal = dblSUMEnv + dblSUMEnvTOTAL;
	    
	    strTable += "<tr style='background-color:Gray; color:White; font-weight:bold'>";
	    strTable += "<td>Total Geral</td><td align='center'>" + intAuxTotal + "</td><td align='center'>" + String.format("%.2f", (double)dblAuxTotal) + "</td><td>&nbsp;</td>";
	    strTable += "</tr>";
	    strTable += "</table>";
	    
	    arrTables.add(strTable);
	    
	    for(int i = 0; i < intCountTbs; i++) {
	    	System.out.println("for 2");
	    	if (i < 3) {
	    		System.out.println("for 2 -> j < 3");
	    		strTable += "<td>" + arrTables.get(i) + "</td><td>&nbsp;</td>";
	    	} else {
	    		System.out.println("for 2 -> j >= 3");
	    		strTable += "</tr><tr valign='top'>";
	    		i = 0;
	    	}
	    }
	    
	    strTable += "</tr></table>";
	    System.out.println("------ END FUNCTION ------");
	    return strTable;
	    
	}

}
