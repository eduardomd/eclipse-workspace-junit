package com.altec.bsbr.app.hyb.dto;

public class HistPesqAnexoModel {

	private String NM_ARQ;
	private String TP_ANEXO; 
	
	public HistPesqAnexoModel()
	{
		
	}
	
	public HistPesqAnexoModel(String nm_ARQ, String tp_ANEXO)
	{
		super();
		this.NM_ARQ = nm_ARQ;
		this.TP_ANEXO = tp_ANEXO;
	}
	
	public String getNM_ARQ() {
		return NM_ARQ;
	}
	
	public void setNM_ARQ(String nM_ARQ) {
		NM_ARQ = nM_ARQ;
	}
	
	public String getTP_ANEXO() {
		return TP_ANEXO;
	}
	
	public void setTP_ANEXO(String tP_ANEXO) {
		TP_ANEXO = tP_ANEXO;
	}
	
	
}
