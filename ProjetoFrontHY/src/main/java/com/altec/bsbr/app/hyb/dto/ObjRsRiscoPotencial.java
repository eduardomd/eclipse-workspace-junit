package com.altec.bsbr.app.hyb.dto;

public class ObjRsRiscoPotencial {
		private String VL_TRAN;
		private String VL_RECR;
		private String VL_EXCL_EVIT;
		private String VL_NEGA;
		private String VL_TRST;
		private String VL_ENVOL;
		private String VL_PERD_EFET;
		private String VL_PREJ;
		
		public ObjRsRiscoPotencial() {}

		
		public ObjRsRiscoPotencial(String vL_TRAN, String vL_RECR, String vL_EXCL_EVIT, String vL_NEGA, String vL_TRST,
				String vL_ENVOL, String vL_PERD_EFET, String vL_PREJ) {
			VL_TRAN = vL_TRAN;
			VL_RECR = vL_RECR;
			VL_EXCL_EVIT = vL_EXCL_EVIT;
			VL_NEGA = vL_NEGA;
			VL_TRST = vL_TRST;
			VL_ENVOL = vL_ENVOL;
			VL_PERD_EFET = vL_PERD_EFET;
			VL_PREJ = vL_PREJ;
		}
		
		public String getVL_TRAN() {
			return VL_TRAN;
		}

		public void setVL_TRAN(String vL_TRAN) {
			VL_TRAN = vL_TRAN;
		}

		public String getVL_RECR() {
			return VL_RECR;
		}

		public void setVL_RECR(String vL_RECR) {
			VL_RECR = vL_RECR;
		}

		public String getVL_EXCL_EVIT() {
			return VL_EXCL_EVIT;
		}

		public void setVL_EXCL_EVIT(String vL_EXCL_EVIT) {
			VL_EXCL_EVIT = vL_EXCL_EVIT;
		}

		public String getVL_NEGA() {
			return VL_NEGA;
		}

		public void setVL_NEGA(String vL_NEGA) {
			VL_NEGA = vL_NEGA;
		}

		public String getVL_TRST() {
			return VL_TRST;
		}

		public void setVL_TRST(String vL_TRST) {
			VL_TRST = vL_TRST;
		}

		public String getVL_ENVOL() {
			return VL_ENVOL;
		}

		public void setVL_ENVOL(String vL_ENVOL) {
			VL_ENVOL = vL_ENVOL;
		}

		public String getVL_PERD_EFET() {
			return VL_PERD_EFET;
		}

		public void setVL_PERD_EFET(String vL_PERD_EFET) {
			VL_PERD_EFET = vL_PERD_EFET;
		}

		public String getVL_PREJ() {
			return VL_PREJ;
		}

		public void setVL_PREJ(String vL_PREJ) {
			VL_PREJ = vL_PREJ;
		}
}
