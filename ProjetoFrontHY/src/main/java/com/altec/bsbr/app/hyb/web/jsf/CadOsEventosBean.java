package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.AreaCompModel;
import com.altec.bsbr.app.hyb.dto.CPFCPNJModel;
import com.altec.bsbr.app.hyb.dto.CadOsCanalModel;
import com.altec.bsbr.app.hyb.dto.CadOsEventosAreaModel;
import com.altec.bsbr.app.hyb.dto.CadOsEventosContaCanalModel;
import com.altec.bsbr.app.hyb.dto.CadOsEventosContratoModel;
import com.altec.bsbr.app.hyb.dto.CadOsEventosModel;
import com.altec.bsbr.app.hyb.dto.CadOsEventosParticipantesModel;
import com.altec.bsbr.app.hyb.dto.ClienteModel;
import com.altec.bsbr.app.hyb.dto.EmpresaEventoModel;
import com.altec.bsbr.app.hyb.dto.EventoCanalOSModel;
import com.altec.bsbr.app.hyb.dto.FatorRiscoModel;
import com.altec.bsbr.app.hyb.dto.LinhaNego1Model;
import com.altec.bsbr.app.hyb.dto.LinhaNego2Model;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsDadosContab.XHYCadOsDadosContabEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsEventos.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsEventos.XHYCadOsEventosEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsParticip.XHYCadOsParticipEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYTransacao.XHYTransacaoEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;

@Component("cadosEventoBean")
@Scope("session")
public class CadOsEventosBean extends BasicBBean {
	
	private static final long serialVersionUID = 1L;	
	
	private String strNrSeqOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs").toString();
	private String cboEmpresaSelecionado;
	private String cboEventoSelecionado;
	private String cboCanalSelecionado;
	private String cboAreaCompSelecionado;
	private String cboLinhaNego1Selecionado;
	private String cboLinhaNego2Selecionado;
	private String cboFatorRiscoSelecionado; 
	private String cboTipoCapturaSelecionado; 
	private String cboClienteSelecionado; 
	private String retorno = "";
	private String objCpfCnpj;	
	
	private List<CadOsEventosModel> cadOSEventosList;
	private List<CadOsEventosAreaModel> eventosAreaModelList;
	private List<CadOsCanalModel> canalModelList;
	private List<EmpresaEventoModel> empresaModelList;
	private List<AreaCompModel> areaCompModelList;
	private List<LinhaNego1Model> linhaNego1ModelList;
	private List<LinhaNego2Model> linhaNego2ModelList;
	private List<FatorRiscoModel> fatorRiscoModelList; 
	private List<ClienteModel> clienteModelList;
	private List<CPFCPNJModel> CPFCNPJModelList;
	private List<EventoCanalOSModel> eventoCanalOSList;
	private List<CadOsEventosParticipantesModel> participantesCliList;
	private List<CadOsEventosParticipantesModel> participantesNCliList;
	private List<CadOsEventosParticipantesModel> participantesTodos;
	private List<CadOsEventosContaCanalModel> eventosContaList;
	private List<CadOsEventosContratoModel> eventosContratoList;
	private List<String[]> eventosDetalheList;
	
	@Autowired
	private XHYCadOsEventosEndPoint cadOsEvento;
		
 	@Autowired
	private XHYCadOsDadosContabEndPoint cadOsDados;
 	
 	@Autowired
 	private XHYCadOsParticipEndPoint cadOsParticip;	
 	
	@Autowired
	private XHYTransacaoEndPoint transacaoEndPoint;
 	
	CadOsEventosModel cadOSEventosModel = new CadOsEventosModel();
	EmpresaEventoModel cadOsDaosContabModel = new EmpresaEventoModel();

	@PostConstruct
	public void init() {		
		cadOsEvento();
		cadOsDadosContab();
		
		listaEventoCanalOS();
		
		populaEventoCanalOs();
		
		populaComboEmpresa();
		cboEmpresaSelecionado = cadOsDaosContabModel.getStrEmpr();
		cboCanalSelecionado = null;

		populaComboAreaComp();
		cboAreaCompSelecionado = cadOSEventosModel.getCdAreaComp();
		
		populaLinhaDeNegocio1();
		cboLinhaNego1Selecionado = cadOSEventosModel.getCdLinhNegonive_1();
		populaLinhaDeNegocio2();
		cboLinhaNego2Selecionado = cadOSEventosModel.getCdLinhNegonive_2();	
		
		populaFatorRisco();
		cboFatorRiscoSelecionado = cadOSEventosModel.getCdFatrRisc();
		cboTipoCapturaSelecionado = cadOSEventosModel.getCdCptu();	
		
		
//		strTipoPess = "F"
//				If Len(strNrCPF_CNPJ) > 11 Then
//					strTipoPess = "J"
//				End If
		
		/* CHAMADAS MQ */
//		String json = transacaoEndPoint.pel1(strNomepart, strDoctoPart, strTipo, strPeNumPe_Rechamada);
//		System.out.println("retorno pel1: "+json);	
		
		cleanSession();		
	}
	
	public void cleanSession() {
		System.out.println("\n --CLEAN SESSIO N ----");
		FacesContext context = FacesContext.getCurrentInstance();
		context.getExternalContext().getSessionMap().remove("cadosEventoBean");
	}

	public void cadOsEvento() {
		this.cadOSEventosList = new ArrayList<CadOsEventosModel>();
		try {
			retorno = cadOsEvento.consultarFraudeEvento(strNrSeqOs);
		} catch (WebServiceException e) {
			e.printStackTrace();
		}

		JSONObject jsonRetorno = new JSONObject(retorno);
		JSONArray pcursor = jsonRetorno.getJSONArray("PCURSOR");
		
		for (int i = 0; i < pcursor.length(); i++) {
			JSONObject curr = pcursor.getJSONObject(i);
			cadOSEventosModel.setCdEmprGrup(curr.isNull("CD_EMPR_GRUP") ? "" : curr.getString("CD_EMPR_GRUP")); 
			cadOSEventosModel.setCdAreaComp(curr.isNull("CD_AREA_COMP") ? "" : curr.getString("CD_AREA_COMP")); 
			cadOSEventosModel.setCdLinhNegonive_1(curr.isNull("CD_LINH_NEGO_NIVE_1") ? "" : curr.getString("CD_LINH_NEGO_NIVE_1"));		
			cadOSEventosModel.setCdLinhNegonive_2(curr.isNull("CD_LINH_NEGO_NIVE_2") ? "" : curr.getString("CD_LINH_NEGO_NIVE_2"));
			cadOSEventosModel.setCdFatrRisc(curr.isNull("CD_FATR_RISC") ? "" : curr.getString("CD_FATR_RISC"));
			cadOSEventosModel.setCdCptu(curr.isNull("CD_CPTU") ? "" : curr.getString("CD_CPTU"));
			switch(curr.get("CD_EVEN").toString()) {
				case "1": //Assalto
					cadOSEventosModel.setIntApurResp(curr.isNull("IN_APUR_RESP") ? false : true);
					break;		
				case "2": //'Complemento
					cadOSEventosModel.setIntDivuIndiInfoClie(curr.isNull("IN_DIVU_INVI_INFO_CLIE") ? false : true);
					cadOSEventosModel.setIntDivuIndiInfoBanc(curr.isNull("IN_DIVU_INVI_INFO_BANC") ? false : true);
					cadOSEventosModel.setIntTrmsMesgCntdIndq(curr.isNull("IN_TRMS_MESG_CNTD_INDQ") ? false : true);
					cadOSEventosModel.setIntVendCasd(curr.isNull("IN_VEND_CASD") ? false : true);
					cadOSEventosModel.setIntVendNaoForz(curr.isNull("IN_VEND_NAO_FORZ") ? false : true);
					cadOSEventosModel.setIntFals(curr.isNull("IN_FALS") ? false : true);
					cadOSEventosModel.setStrNmOutrPratIndq(curr.isNull("NM_OUTR_PRAT_INDQ") ? "" : curr.getString("NM_OUTR_PRAT_INDQ"));
					cadOSEventosModel.setIntRstrFincCadr(curr.isNull("IN_RSTR_FINC_CADR") ? false : true);
					cadOSEventosModel.setIntCdtaProfIndq(curr.isNull("IN_CDTA_PROF_INDQ") ? false : true);
					cadOSEventosModel.setIntDscmNort(curr.isNull("IN_DSCM_NORT") ? false : true);
					cadOSEventosModel.setIntViolCodiEtic(curr.isNull("IN_VIOL_CODI_ETIC") ? false : true);
					cadOSEventosModel.setIntEndiExce(curr.isNull("IN_ENDI_EXCE") ? false : true);
					break;		
				case "3": //Fraudes
					cadOSEventosModel.setIntApprValoClie(curr.isNull("IN_APPR_VALO_CLIE") ? false : true);
					cadOSEventosModel.setIntApprValoBanc(curr.isNull("IN_APPR_VALO_BANC") ? false : true);
					cadOSEventosModel.setIntMoviFincIrre(curr.isNull("IN_MOVI_FINC_IRRE") ? false : true);
					cadOSEventosModel.setIntOperCred(curr.isNull("IN_OPER_CRED") ? false : true);
					cadOSEventosModel.setIntCntrProd(curr.isNull("IN_CNTR_PROD") ? false : true);
					cadOSEventosModel.setIntAberIrreCnta(curr.isNull("IN_ABER_IRRE_CNTA") ? false : true);
					break;	
				case "4": //Fraudes Eletrônicas
					break;
				case "5": //Fraudes Operacionais
					break;
				case "6": //Monitoramento
					cadOSEventosModel.setIntEmailQubrSigi(curr.isNull("IN_EMAIL_QUBR_SIGI") ? false : true);
					cadOSEventosModel.setIntEmailVazaInfo(curr.isNull("IN_EMAIL_VAZA_INFO") ? false : true);
					cadOSEventosModel.setIntEmailCrrt(curr.isNull("IN_EMAIL_CRRT") ? false : true);
					cadOSEventosModel.setIntEmailExpcRisc(curr.isNull("IN_EMAIL_EXPC_RISC") ? false : true);
					cadOSEventosModel.setIntEmailPorn(curr.isNull("IN_EMAIL_PORN") ? false : true);
					cadOSEventosModel.setIntEmailBtpp(curr.isNull("IN_EMAIL_BTPP") ? false : true);
					cadOSEventosModel.setIntEmailOutr(curr.isNull("IN_EMAIL_OUTR") ? false : true);
					cadOSEventosModel.setIntRstrInci(curr.isNull("IN_RSTR_INCI") ? false : true);
					cadOSEventosModel.setIntRstrReic(curr.isNull("IN_RSTR_REIC") ? false : true);
					cadOSEventosModel.setIntMoviQntd(curr.isNull("IN_MOVI_QNTD") ? false : true);
					cadOSEventosModel.setIntMoviValo(curr.isNull("IN_MOVI_VALO") ? false : true);
					break;
				case "7": //Operações de Crédito
					cadOSEventosModel.setIntDefrAntiNort(curr.isNull("IN_DEFR_ANTI_NORT") ? false : true);
					cadOSEventosModel.setIntIrreCnstGara(curr.isNull("IN_IRRE_CNST_GARA") ? false : true);
					cadOSEventosModel.setIntUtizTituDuplFria(curr.isNull("IN_UTIZ_TITU_DUPL_FRIA") ? false : true);
					cadOSEventosModel.setIntFalsificacao(curr.isNull("IN_FALS") ? false : true);
					cadOSEventosModel.setIntIrreForz(curr.isNull("IN_IRRE_FORZ") ? false : true);
					cadOSEventosModel.setStrOperCredOutr(curr.isNull("TX_OPER_CRED_OUTR") ? "" : curr.getString("TX_OPER_CRED_OUTR"));
					break;
				case "8": //Outros
					cadOSEventosModel.setStrOutros(curr.isNull("TX_OPER_CRED_OUTR") ? "" : curr.getString("TX_OPER_CRED_OUTR"));
					cadOSEventosModel.setIntCdOutrTipoFrau(curr.isNull("CD_OUTR_TIPO_FRAU") ? 0 : curr.getInt("CD_OUTR_TIPO_FRAU"));
					break;
			}	
			
			this.cadOSEventosList.add(cadOSEventosModel);
		}
	}	

	public void cadOsDadosContab() {
		
		try {
			retorno = cadOsDados.fnSelDadosContabeis(strNrSeqOs);
		} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsDadosContab.WebServiceException e) {
			
			e.printStackTrace();
		}	
		
		JSONObject jsonRetorno = new JSONObject(retorno);
		JSONArray pcursor = jsonRetorno.getJSONArray("PCURSOR");
		JSONObject curr = pcursor.getJSONObject(0);
				
		cadOsDaosContabModel.setStrEmpr(curr.isNull("CD_EMPR_GRUP") ? "" : curr.get("CD_EMPR_GRUP").toString());
	}

	public void populaEventoCanalOs() {
		cboEventoSelecionado = "";
		this.eventosAreaModelList = new ArrayList<CadOsEventosAreaModel>();
		try {
			retorno = cadOsEvento.consultarEventoArea(strNrSeqOs);
		} catch (WebServiceException e) {
			e.printStackTrace();
		}

		JSONObject jsonRetorno = new JSONObject(retorno);
		JSONArray pcursor = jsonRetorno.getJSONArray("PCURSOR");
		
		for (int i = 0; i < pcursor.length(); i++) {
			JSONObject curr = pcursor.getJSONObject(i);
			CadOsEventosAreaModel cadModel = new CadOsEventosAreaModel();
			cadModel.setCodigoEvento(curr.isNull("CODIGO") ? "" : curr.get("CODIGO").toString());
			cadModel.setNomeEvento(curr.isNull("NOME") ? "" : curr.get("NOME").toString());		
			
			this.eventosAreaModelList.add(cadModel);
		}
	}

	public void populaCanalOS() {
		this.canalModelList = new ArrayList<CadOsCanalModel>();
		try {
			retorno = cadOsEvento.consultarCanalEvento(cboEventoSelecionado);
		} catch (WebServiceException e) {
			e.printStackTrace();
		}

		JSONObject jsonRetorno = new JSONObject(retorno);
		JSONArray pcursor = jsonRetorno.getJSONArray("PCURSOR");

		if (pcursor.length()>0) {
			RequestContext.getCurrentInstance().execute("habilitaCamposForm('')");  
		}else {
			RequestContext.getCurrentInstance().execute("habilitaCamposForm('hidden')");  
		}
		
		for (int i = 0; i < pcursor.length(); i++) {
			JSONObject curr = pcursor.getJSONObject(i);
			CadOsCanalModel cadModel = new CadOsCanalModel();
			cadModel.setCodigo(curr.isNull("CODIGO") ? "" : curr.get("CODIGO").toString());
			cadModel.setPgDestino(curr.isNull("PGDESTINO") ? "" : curr.get("PGDESTINO").toString().toLowerCase().replace("hy_", "hyb_").replace(".asp", ".xhtml"));
			cadModel.setNome(curr.isNull("NOME") ? "" : curr.get("NOME").toString());			
			this.canalModelList.add(cadModel);
		}
	}
	
	public String pegarCanalSelecionado(String campo) {
		String canalSelecionado="";
		if (cboCanalSelecionado != null) {
			String[] canalSele = cboCanalSelecionado.split("\\|");
			switch (campo) {
				case "codigo":
					canalSelecionado = canalSele[0];
					break;
				case "nome":
					canalSelecionado = canalSele[1];
					break;
				case "page":
					canalSelecionado = canalSele[2];
			}
		}
		return canalSelecionado;
	}
	
	public String pegarNmCanal() {
		return cboCanalSelecionado.split("\\|")[1];
	}

	public void populaComboEmpresa() {
		this.empresaModelList = new ArrayList<EmpresaEventoModel>();
		
		try {
			retorno = cadOsDados.fnSelValEmpresa();
		} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsDadosContab.WebServiceException e) {
			
			e.printStackTrace();
		}	
		
		JSONObject jsonRetorno = new JSONObject(retorno);
		JSONArray pcursor = jsonRetorno.getJSONArray("PCURSOR");

		this.empresaModelList.add(new EmpresaEventoModel("","Selecione"));
		for (int i = 0; i < pcursor.length(); i++) {
			JSONObject curr = pcursor.getJSONObject(i);
			this.empresaModelList.add(new EmpresaEventoModel(
					curr.get("SQ_EMPR").toString(), 
					curr.get("NO_EMPR").toString()
			));
		}		
	}

	
	public void populaComboAreaComp() {
		this.cboAreaCompSelecionado="";
		this.areaCompModelList = new ArrayList<AreaCompModel>();	
		try {
			retorno = cadOsDados.fnSelValAreaComp(cboEmpresaSelecionado);
		} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsDadosContab.WebServiceException e) {
			e.printStackTrace();
		}		
		JSONObject jsonRetorno = new JSONObject(retorno);
		JSONArray pcursor = jsonRetorno.getJSONArray("PCURSOR");
		
		this.areaCompModelList.add(new AreaCompModel("","Selecione"));
		for (int i = 0; i < pcursor.length(); i++) {
			JSONObject curr = pcursor.getJSONObject(i);
			this.areaCompModelList.add(new AreaCompModel(
					curr.get("SQ_DEPE").toString(), 
					curr.get("NO_DEPE").toString()
			));
		}		
	}	
	
	public void populaLinhaDeNegocio1() {
		this.linhaNego1ModelList = new ArrayList<LinhaNego1Model>();
		
		try {
			retorno = cadOsDados.fnSelValLinhaNego1();
		} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsDadosContab.WebServiceException e) {
			e.printStackTrace();
		}		
		
		JSONObject jsonRetorno = new JSONObject(retorno);
		JSONArray pcursor = jsonRetorno.getJSONArray("PCURSOR");

		this.linhaNego1ModelList.add(new LinhaNego1Model("","Selecione"));
		for (int i = 0; i < pcursor.length(); i++) {
			JSONObject curr = pcursor.getJSONObject(i);
			this.linhaNego1ModelList.add(new LinhaNego1Model(
					curr.get("SQ_PILA").toString(), 
					curr.get("DE_PILA").toString()
			));
		}		
	}	
	
	public void populaLinhaDeNegocio2() {
		this.linhaNego2ModelList = new ArrayList<LinhaNego2Model>();		
		try {
			retorno = cadOsDados.fnSelValLinhaNego2(cboLinhaNego1Selecionado);
		} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsDadosContab.WebServiceException e) {
			e.printStackTrace();
		}		
		
		JSONObject jsonRetorno = new JSONObject(retorno);
		JSONArray pcursor = jsonRetorno.getJSONArray("PCURSOR");

		this.linhaNego2ModelList.add(new LinhaNego2Model("","Selecione"));
		for (int i = 0; i < pcursor.length(); i++) {
			JSONObject curr = pcursor.getJSONObject(i);
			this.linhaNego2ModelList.add(new LinhaNego2Model(
					curr.get("SQ_LINH_NEGO").toString(), 
					curr.get("DE_LINH_NEGO").toString()
			));
		}		
	}
	
	public void populaFatorRisco() {
		this.fatorRiscoModelList = new ArrayList<FatorRiscoModel>();
		
		try {
			retorno = cadOsDados.fnSelValFatorRisco();
		} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsDadosContab.WebServiceException e) {
			
			e.printStackTrace();
		}	
				
		JSONObject jsonRetorno = new JSONObject(retorno);
		JSONArray pcursor = jsonRetorno.getJSONArray("PCURSOR");

		this.fatorRiscoModelList.add(new FatorRiscoModel("","Selecione"));
		for (int i = 0; i < pcursor.length(); i++) {
			JSONObject curr = pcursor.getJSONObject(i);
			this.fatorRiscoModelList.add(new FatorRiscoModel(
					curr.get("SQ_SUB_FATR_RISC").toString(),
					curr.get("NO_SUB_FATR_RISC").toString()
			));
		}		
	}	
	
	public void listaEventoCanalOS() {
		this.eventoCanalOSList = new ArrayList<EventoCanalOSModel>();
		
		try {
			retorno = cadOsEvento.consultarEventoCanalOS(strNrSeqOs);
		} catch (WebServiceException e) {			
			e.printStackTrace();
		}	
				
		JSONObject jsonRetorno = new JSONObject(retorno);
		JSONArray pcursor = jsonRetorno.getJSONArray("PCURSOR");

		for (int i = 0; i < pcursor.length(); i++) {
			JSONObject curr = pcursor.getJSONObject(i);
			this.eventoCanalOSList.add(new EventoCanalOSModel(
					curr.get("NM_EVEN").toString(),
					curr.get("NM_CNAL").toString()
			));
		}		
	}		
	
	public void carregaDetalhes(){
		String cdBanc;
		String tipo = "";
		this.eventosDetalheList = new ArrayList<String[]>();
		this.eventosContaList = new ArrayList<CadOsEventosContaCanalModel>();
		this.eventosContratoList = new ArrayList<CadOsEventosContratoModel>();

		try {
			retorno = cadOsEvento.consultarContaCanal(strNrSeqOs,Integer.parseInt(cboEventoSelecionado),pegarCanalSelecionado("codigo"));
		} catch (WebServiceException e) {			
			e.printStackTrace();
		}	
		JSONObject jsonRetornoConta = new JSONObject(retorno);	
		JSONArray pcursorConta = jsonRetornoConta.getJSONArray("PCURSOR");
		
		try {
			retorno = cadOsEvento.consultarContrato(strNrSeqOs,Integer.parseInt(cboEventoSelecionado),pegarCanalSelecionado("codigo"));
		} catch (WebServiceException e) {			
			e.printStackTrace();
		}	
		JSONObject jsonRetornoContrato = new JSONObject(retorno);
		JSONArray pcursorContrato = jsonRetornoContrato.getJSONArray("PCURSOR");	
		
		for (int i = 0; i < pcursorConta.length(); i++) {
			
			JSONObject jsonConta = pcursorConta.getJSONObject(i);
			CadOsEventosContaCanalModel cadModel = new CadOsEventosContaCanalModel();
			
			cadModel.setContaNrSequCntaBncr(jsonConta.isNull("NR_SEQU_CNTA_BNCR") ? "" : jsonConta.get("NR_SEQU_CNTA_BNCR").toString()); 
			cadModel.setContaNrCnta(jsonConta.isNull("NR_CNTA") ? "" : jsonConta.get("NR_CNTA").toString());
			cadModel.setContaNrSequParpProc(jsonConta.isNull("NR_SEQU_PARP_PROC") ? "" : jsonConta.get("NR_SEQU_PARP_PROC").toString());		
			cadModel.setContaNrSequFrauCnal(jsonConta.isNull("NR_SEQU_FRAU_CNAL") ? "" : jsonConta.get("NR_SEQU_FRAU_CNAL").toString());
			
			cdBanc = "0000" + (jsonConta.isNull("Cd_Banc") ? "" : jsonConta.get("Cd_Banc").toString());
			cdBanc = cdBanc.substring(cdBanc.length()-4);		
			cadModel.setContaCdBanc(cdBanc);
			
			cadModel.setContaTpOperParp(jsonConta.isNull("TP_OPER_PARP") ? "" : jsonConta.get("TP_OPER_PARP").toString());
			cadModel.setContaQtdLanc(jsonConta.isNull("QTD_LANC") ? "" : jsonConta.get("QTD_LANC").toString());
			System.out.println("AQUI");
			if (tipo == "")
				tipo=carregaDetalhesAdic(i,"0","");
			
				eventosDetalheList.add(new String[] {
					"conta",
					tipo,
					cadModel.getContaNrSequCntaBncr(),
					cadModel.getContaNrCnta(),
					cadModel.getContaNrSequParpProc(),
					cadModel.getContaNrSequFrauCnal(),
					cadModel.getContaNrSequCntaBncr(),
					cadModel.getContaCdBanc(),
					cadModel.getContaTpOperParp(),
					cadModel.getContaQtdLanc(),
					"",
					"",
				}
			);
		}
		
		for (int i = 0; i < pcursorContrato.length(); i++) {			
			JSONObject jsonContrato = pcursorContrato.getJSONObject(i);			

			CadOsEventosContratoModel cadModel = new CadOsEventosContratoModel();
			
			cadModel.setContratoNrSequCntr(jsonContrato.isNull("NR_SEQU_CNTR") ? "" : jsonContrato.get("NR_SEQU_CNTR").toString());
			cadModel.setContratoNrCntr(jsonContrato.isNull("NR_CNTR") ? "" : jsonContrato.get("NR_CNTR").toString());
			cadModel.setContratoNrSequParpProc(jsonContrato.isNull("NR_SEQU_PARP_PROC") ? "" : jsonContrato.get("NR_SEQU_PARP_PROC").toString());
			cadModel.setContratoNrSequFrauCnal(jsonContrato.isNull("NR_SEQU_FRAU_CNAL") ? "" : jsonContrato.get("NR_SEQU_FRAU_CNAL").toString());
			cadModel.setContratoCdEntiCntr(jsonContrato.isNull("CD_ENTI_CNTR") ? "" : jsonContrato.get("CD_ENTI_CNTR").toString());
			cadModel.setContratoNmProdCorp(jsonContrato.isNull("NM_PROD_CORP") ? "" : jsonContrato.get("NM_PROD_CORP").toString());
			cadModel.setContratoNrSequOrdeServ(jsonContrato.isNull("NR_SEQU_ORDE_SERV") ? "" : jsonContrato.get("NR_SEQU_ORDE_SERV").toString());
			cadModel.setContratoCdProd(jsonContrato.isNull("CD_PROD") ? "" : jsonContrato.get("CD_PROD").toString());
			cadModel.setContratoTpOperParp(jsonContrato.isNull("TP_OPER_PARP") ? "" : jsonContrato.get("TP_OPER_PARP").toString());
			cadModel.setContratoQtdLanc(jsonContrato.isNull("QTD_LANC") ? "" : jsonContrato.get("QTD_LANC").toString());
			
			if (tipo == "")
				tipo=carregaDetalhesAdic(i,cadModel.getContratoTpOperParp(),cadModel.getContratoNrSequCntr());
			
				eventosDetalheList.add(new String[] {
					"contrato",
					tipo,
					cadModel.getContratoNrSequCntr(),
					cadModel.getContratoNrCntr(),
					cadModel.getContratoNrSequParpProc(),
					cadModel.getContratoNrSequFrauCnal(),
					cadModel.getContratoCdEntiCntr(),
					cadModel.getContratoNmProdCorp(),
					cadModel.getContratoNrSequOrdeServ(),
					cadModel.getContratoCdProd(),
					cadModel.getContratoTpOperParp(),
					cadModel.getContratoQtdLanc()
				}
			);
		}		
		
		listaParticipantes();		
	}
	
	public String carregaDetalhesAdic(int linha, String tpOper, String nrSequCntr) {
		int tipo = 0;
		if (linha == 0){
			tipo = 1;
			// busca conta corrente nonono nonon ononononno nono 
			//seta co campo tipo como conta corrente
			// fecha o campo para edição
			//CarregaAgenciaConta(linhaAtual, arrColunas[2]);
			//var campo = document.getElementById("cbo"+ linhaAtual +"Tipo");;
			//campo.value = 1;
			//campo.disabled=true;	
			//var agencia = document.getElementById("cboContaAg"+linhaAtual+"Cntr"); 												
			//agencia.disabled=true;
			
//			for( var count = 0; count <= agencia.length - 1;  count++){
//				
//				var conta = '0033'+'-'+ arrColunas[5] +'-'+ zeroEsquerda(arrColunas[1]);
//				
//				if  (agencia.options[count].value ==  conta){														
//					agencia.value = conta
//					break;
//				}														
//			}												
		}
		
		if (linha == 1){
			tipo = 2;
			//var campo = document.getElementById("cbo"+ linhaAtual +"Tipo");													
			//campo.value = 2;
			//campo.disabled=true;													
			//CarregaContrato(linhaAtual,false);
		    //var contrato = document.getElementById("cbo"+linhaAtual+"Cntr");
			//contrato.disabled=true;
			//contrato.value = arrColunas[0];
		
			
			// carrega não cliente
			if (tpOper == "3"){													
				//var desc = contrato.options[contrato.selectedIndex].text;
				if (nrSequCntr != ""){
					//campo.value = 3;
					tipo = 3;														
					//CarregaContratoTXT(linhaAtual,arrColunas[0]);																											
				}
			}												
		}
		return String.valueOf(tipo);
	}
	
	public void cleanSession(String beanName) {
		try {
			//RequestContext.getCurrentInstance().execute("window.open('"+page+"', '"+pageId+"', 'dialogHeight:250px;dialogWidth:530px; center: Yes; resizable: no; status: No;');"); 
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove(beanName);
			//RequestContext.getCurrentInstance().execute("setDialogDispatch('"+page+"');");
			System.out.println("CLEAN: " + beanName);
		} 
		catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	public String abrePgDetalhe() {
		String strPage = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strPage").toString();
		String strQueryString = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strQueryString").toString();
		
//		String beanName = strPage.split(".")[0].replace("_", "").replace("hyb", "");

		System.out.println("STRPAGE: " + strPage + " STRQUERY: " + strQueryString);
		
		switch (strPage) {
		case "hyb_cadastroos_slinha.xhtml":
			cleanSession("cadastroOsSLinhaBean");
		case "hyb_cadastroos_cdebito.xhtml":
			cleanSession("CadastroOsCDebitoBean");
			break;
		case "hyb_cadastroos_ccredito.xhtml":
			cleanSession("CadastroOSCCreditoBean");
			break;
		case "hyb_cadastroos_ibanking.xhtml":
			cleanSession("cadastroOsIbankingBean");
			break;
		case "hyb_cadastroos_default.xhtml":
			cleanSession("CadastroOsDefaultBean");
			break;
		case "hyb_cadastroosfo.xhtml":
			cleanSession("cadastroOsFoBean");
			break;
		}
		
		return strPage + strQueryString + "&faces-redirect=true";
	}
	
	public void insereLinha() {
		System.out.println("Adicionando!!!!!");
		this.eventosDetalheList.add(new String[] {"", "", "", "", "", "", "", "", "", ""});
	}
	
	public void excluiLinha() {
		String strNrSeqDetalhe = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("seq").toString();
		String strNrSeqPartic = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("partic").toString();
		String strTipo = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("tipo").toString();
			
		System.out.println("Excluído!");
		
		switch (strTipo) {
			case "1":
				try {
					cadOsEvento.excluirCartaoCanal(Long.parseLong("0" + strNrSeqDetalhe));  
					RequestContext.getCurrentInstance().execute("alert('Cartão excluído com sucesso!')");
				} catch (Exception e) {
					try {
						FacesContext.getCurrentInstance().getExternalContext()
								.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
					} catch (Exception ex) {
						e.printStackTrace();
					}
				}
				break;
			case "2":
				try {
					cadOsEvento.excluirContaCanal(Long.parseLong("0" + strNrSeqDetalhe)); 
					RequestContext.getCurrentInstance().execute("alert('Conta excluída com sucesso!')");
				} catch (Exception e) {
					try {
						FacesContext.getCurrentInstance().getExternalContext()
								.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
					} catch (Exception ex) {
						e.printStackTrace();
					}
				}
				break;
			case "3":
			case "4":
				try {
					cadOsEvento.excluirContratoCanal(Long.parseLong("0" + strNrSeqDetalhe),Long.parseLong(strNrSeqPartic),Long.parseLong(strNrSeqOs)); 
					RequestContext.getCurrentInstance().execute("alert('Contrato excluído com sucesso!')");
				} catch (Exception e) {
					try {
						FacesContext.getCurrentInstance().getExternalContext()
								.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
					} catch (Exception ex) {
						e.printStackTrace();
					}
				}
				break;
		}
		carregaDetalhes();
	}
	
	public String zeroEsquerda(String strConta) {
		String ret = strConta;
		for (int i=strConta.length(); i < 12;i++){
			ret = "0" + ret;			
		}	
		return ret;
	}
	
	public void listaParticipantes() {
		int intLinha = 0;
		this.participantesCliList = new ArrayList<CadOsEventosParticipantesModel>();
		this.participantesNCliList = new ArrayList<CadOsEventosParticipantesModel>();
		
		try {
			retorno = cadOsParticip.consultarCliente(strNrSeqOs, 0);
		} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsParticip.WebServiceException e) {			
			e.printStackTrace();
		}		

				
		JSONObject jsonRetorno = new JSONObject(retorno);
		JSONArray pcursor = jsonRetorno.getJSONArray("PCURSOR");

		for (int i = 0; i < pcursor.length(); i++) {
			JSONObject curr = pcursor.getJSONObject(i);
			
			try {
				retorno = cadOsParticip.getCPFParticipante(curr.getInt("SQ_PARP"));
			} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsParticip.WebServiceException e) {			
				e.printStackTrace();
			}
			
			CadOsEventosParticipantesModel cadModel = new CadOsEventosParticipantesModel();
			cadModel.setNome(curr.isNull("NOME") ? "" : curr.get("NOME").toString());
			cadModel.setTpParp(curr.isNull("TP_PARP") ? "" : curr.get("TP_PARP").toString());
			cadModel.setSqParp(curr.isNull("SQ_PARP") ? "" : curr.get("SQ_PARP").toString());			
			this.participantesCliList.add(cadModel);
			
			if (curr.get("TP_PARP").toString().equals("4") || curr.get("TP_PARP").toString().equals("5")){
				System.out.println("AQUI");
				cadModel = new CadOsEventosParticipantesModel();
				cadModel.setNome(curr.isNull("NOME") ? "" : curr.get("NOME").toString());
				cadModel.setTpParp(curr.isNull("TP_PARP") ? "" : curr.get("TP_PARP").toString());
				cadModel.setSqParp(curr.isNull("SQ_PARP") ? "" : curr.get("SQ_PARP").toString());			
				this.participantesNCliList.add(cadModel);
			}
			
			intLinha += 1;
			objCpfCnpj += intLinha + "§" + cadModel.getNome() + "§" + retorno + "§" + cadModel.getSqParp() + "|";
					
		}
		System.out.println("--------------------------");
		System.out.println(objCpfCnpj);
	}

	public List<CadOsEventosParticipantesModel> listaTodosParticipantes() {		
		if (participantesCliList.size() == 0)
			participantesTodos=participantesNCliList;
		else
			participantesTodos=participantesCliList;

		//this.participantesTodos = new ArrayList<CadOsEventosParticipantesModel>();
		//participantesTodos.add(participantesCliList);
		
		return participantesTodos;
	}
	
	public void listaContratos(String dblNrPartProc) {
		this.eventosContratoList = new ArrayList<CadOsEventosContratoModel>();
		
		try {
			retorno = cadOsParticip.consultarContratoCliente(Double.parseDouble(dblNrPartProc));
		} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsParticip.WebServiceException e) {			
			e.printStackTrace();
		}
		
		JSONObject jsonRetorno = new JSONObject(retorno);
		JSONArray pcursor = jsonRetorno.getJSONArray("PCURSOR");

		for (int i = 0; i < pcursor.length(); i++) {
			JSONObject curr = pcursor.getJSONObject(i);

			this.eventosContratoList.add(new CadOsEventosContratoModel(
				curr.isNull("NR_SEQU_CNTR") ? "" : curr.get("NR_SEQU_CNTR").toString(),
				curr.isNull("NR_CNTR") ? "" : curr.get("NR_CNTR").toString(),
				curr.isNull("NM_PROD_CORP") ? "" : curr.get("NM_PROD_CORP").toString(),
				curr.isNull("CD_PROD") ? "" : curr.get("CD_PROD").toString())
			);
		}
	}

	
	public void salvarForm() {
		
		// ************Verificar os parâmetros que estão com o valor "0".*************
		String retorno = ""; 
		
//		try {
//			retorno = cadOsEvento.GravarFraudeEvento(
//					cboEventoSelecionado,
//					strNrSeqOs,
//					"0",
//					"0",
//					"0", 
//					"0",
//					"0",
//					"0",
//					"0",
//					"0",
//					"0",
//					"0",
//					"0",
//					"0",
//					"0",
//					"0",
//					"0",
//					"0",
//					"0",
//					"0",
//					"0",//strNmOutrPratIndq
//					"0",
//					"0",
//					"0",
//					"0",
//					"0",
//					"0",
//					"0",
//					"0",
//					"0",
//					"0",
//					"0",
//					"0",
//					"0",
//					"0",
//					"0",
//					"0", //strOperCredOutr 
//					cboEmpresaSelecionado, 
//					cboAreaCompSelecionado, 
//					cboLinhaNego1Selecionado, 
//					cboLinhaNego2Selecionado, 
//					cboFatorRiscoSelecionado, 
//					cboTipoCapturaSelecionado,
//					"0"
//					);
//		} catch (WebServiceException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
	
	public String getStrNrSeqOs() {
		return strNrSeqOs;
	}
	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}


	public String getCboEventoSelecionado() {
		return cboEventoSelecionado;
	}
	public void setCboEventoSelecionado(String cboEventoSelecionado) {
		this.cboEventoSelecionado = cboEventoSelecionado;
	}

	public String getCboCanalSelecionado() {
		return cboCanalSelecionado;
	}
	public void setCboCanalSelecionado(String cboCanalSelecionado) {
		this.cboCanalSelecionado = cboCanalSelecionado;
	}

	public String getCboEmpresaSelecionado() {
		return cboEmpresaSelecionado;
	}
	public void setCboEmpresaSelecionado(String cboEmpresaSelecionado) {
		this.cboEmpresaSelecionado = cboEmpresaSelecionado;
	}

	public List<EmpresaEventoModel> getEmpresaModelList() {
		return empresaModelList;
	}
	public void setEmpresaModelList(List<EmpresaEventoModel> empresaModelList) {
		this.empresaModelList = empresaModelList;
	}

	public String getCboAreaCompSelecionado() {
		return cboAreaCompSelecionado;
	}
	public void setCboAreaCompSelecionado(String cboAreaCompSelecionado) {
		this.cboAreaCompSelecionado = cboAreaCompSelecionado;
	}

	public List<CadOsCanalModel> getCanalModelList() {
		return canalModelList;
	}
	public void setCanalModelList(List<CadOsCanalModel> canalModelList) {
		this.canalModelList = canalModelList;
	}

	public List<CadOsEventosAreaModel> getEventosAreaModelList() {
		return eventosAreaModelList;
	}
	public void setEventosAreaModelList(List<CadOsEventosAreaModel> eventosAreaModelList) {
		this.eventosAreaModelList = eventosAreaModelList;
	}
	
	public List<AreaCompModel> getAreaCompModelList() {
		return areaCompModelList;
	}
	public void setAreaCompModelList(List<AreaCompModel> areaCompModelList) {
		this.areaCompModelList = areaCompModelList;
	}

	public String getCboLinhaNego1Selecionado() {
		return cboLinhaNego1Selecionado;
	}
	public void setCboLinhaNego1Selecionado(String cboLinhaNego1Selecionado) {
		this.cboLinhaNego1Selecionado = cboLinhaNego1Selecionado;
	}

	public String getCboLinhaNego2Selecionado() {
		return cboLinhaNego2Selecionado;
	}
	public void setCboLinhaNego2Selecionado(String cboLinhaNego2Selecionado) {
		this.cboLinhaNego2Selecionado = cboLinhaNego2Selecionado;
	}

	public String getRetorno() {
		return retorno;
	}

	public void setRetorno(String retorno) {
		this.retorno = retorno;
	}

	public List<LinhaNego1Model> getLinhaNego1ModelList() {
		return linhaNego1ModelList;
	}
	public void setLinhaNego1ModelList(List<LinhaNego1Model> linhaNego1ModelList) {
		this.linhaNego1ModelList = linhaNego1ModelList;
	}

	public List<LinhaNego2Model> getLinhaNego2ModelList() {
		return linhaNego2ModelList;
	}
	public void setLinhaNego2ModelList(List<LinhaNego2Model> linhaNego2ModelList) {
		this.linhaNego2ModelList = linhaNego2ModelList;
	}

	public String getCboFatorRiscoSelecionado() {
		return cboFatorRiscoSelecionado;
	}
	public void setCboFatorRiscoSelecionado(String cboFatorRiscoSelecionado) {
		this.cboFatorRiscoSelecionado = cboFatorRiscoSelecionado;
	}

	public String getCboTipoCapturaSelecionado() {
		return cboTipoCapturaSelecionado;
	}
	public void setCboTipoCapturaSelecionado(String cboTipoCapturaSelecionado) {
		this.cboTipoCapturaSelecionado = cboTipoCapturaSelecionado;
	}

	public List<FatorRiscoModel> getFatorRiscoModelList() {
		return fatorRiscoModelList;
	}
	public void setFatorRiscoModelList(List<FatorRiscoModel> fatorRiscoModelList) {
		this.fatorRiscoModelList = fatorRiscoModelList;
	}

	public String getCboClienteSelecionado() {
		return cboClienteSelecionado;
	}
	public void setCboClienteSelecionado(String cboClienteSelecionado) {
		this.cboClienteSelecionado = cboClienteSelecionado;
	}

	public List<ClienteModel> getClienteModelList() {
		return clienteModelList;
	}
	public void setClienteModelList(List<ClienteModel> clienteModelList) {
		this.clienteModelList = clienteModelList;
	}

	public List<CPFCPNJModel> getCPFCNPJModelList() {
		return CPFCNPJModelList;
	}
	public void setCPFCNPJModelList(List<CPFCPNJModel> CPFCNPJModelList) {
		this.CPFCNPJModelList = CPFCNPJModelList;
	}

	public List<EventoCanalOSModel> getEventoCanalOSList() {
		return eventoCanalOSList;
	}

	public void setEventoCanalOSList(List<EventoCanalOSModel> eventoCanalOSList) {
		this.eventoCanalOSList = eventoCanalOSList;
	}

	public List<CadOsEventosModel> getCadOSEventosList() {
		return cadOSEventosList;
	}

	public void setCadOSEventosList(List<CadOsEventosModel> cadOSEventosList) {
		this.cadOSEventosList = cadOSEventosList;
	}

	public List<CadOsEventosParticipantesModel> getParticipantesCliList() {
		return participantesCliList;
	}

	public void setParticipantesCliList(List<CadOsEventosParticipantesModel> participantesCliList) {
		this.participantesCliList = participantesCliList;
	}

	public List<CadOsEventosParticipantesModel> getParticipantesNCliList() {
		return participantesNCliList;
	}

	public void setParticipantesNCliList(List<CadOsEventosParticipantesModel> participantesNCliList) {
		this.participantesNCliList = participantesNCliList;
	}

	public List<CadOsEventosParticipantesModel> getParticipantesTodos() {
		return participantesTodos;
	}

	public void setParticipantesTodos(List<CadOsEventosParticipantesModel> participantesTodos) {
		this.participantesTodos = participantesTodos;
	}

	public List<String[]> getEventosDetalheList() {
		return eventosDetalheList;
	}

	public void setEventosDetalheList(List<String[]> eventosDetalheList) {
		this.eventosDetalheList = eventosDetalheList;
	}

	public List<CadOsEventosContaCanalModel> getEventosContaList() {
		return eventosContaList;
	}

	public void setEventosContaList(List<CadOsEventosContaCanalModel> eventosContaList) {
		this.eventosContaList = eventosContaList;
	}

	public List<CadOsEventosContratoModel> getEventosContratoList() {
		return eventosContratoList;
	}

	public void setEventosContratoList(List<CadOsEventosContratoModel> eventosContratoList) {
		this.eventosContratoList = eventosContratoList;
	}

	public CadOsEventosModel getCadOSEventosModel() {
		return cadOSEventosModel;
	}

	public void setCadOSEventosModel(CadOsEventosModel cadOSEventosModel) {
		this.cadOSEventosModel = cadOSEventosModel;
	}
	
}