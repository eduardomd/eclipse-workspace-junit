package com.altec.bsbr.app.hyb.web.jsf;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.fw.web.jsf.BasicBBean;


@Component("ImportarArquivoBean")
@Scope("request")
public class ImportarArquivoBean extends BasicBBean {
    private static final long serialVersionUID = 1L;
    
    /* Query String */
    private String strAnexo;
    private String strSeqOs;
    private String strSeqTrei;
    
    /* Source do iframe com a l�gica para qual mostrar */
    private String srcIfrmImpArq;
    
    @PostConstruct
    public void init() {
    	/* Source do iframe com a l�gica para qual mostrar */
    	this.strAnexo = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strTpAnexo") == null ? "" : FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strTpAnexo").toString();
    	this.strSeqOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strSeqOs") == null ? "" : FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strSeqOs").toString();
    	this.strSeqTrei = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strSeqTrei") == null ? "" : FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strSeqTrei").toString();
    	
    	if (strAnexo.equals("N")) {
    		srcIfrmImpArq = "hyb_uploadnotificacao.xhtml?qstrSeqOs="+strSeqOs+"&qstrTpAnexo="+strAnexo;
    	} 
    	else {
    		srcIfrmImpArq = "hyb_uploadprocessa.xhtml?qstrTpAnexo="+strAnexo+"&qstrSeqTrei="+strSeqTrei+"&qstrSeqOs="+strSeqOs;
    	}    	
    }
    
	public String getSrcIfrmImpArq() {
		return srcIfrmImpArq;
	}
	public void setSrcIfrmImpArq(String srcIfrmImpArq) {
		this.srcIfrmImpArq = srcIfrmImpArq;
	}
	public String getStrAnexo() {
		return strAnexo;
	}
	public void setStrAnexo(String strAnexo) {
		this.strAnexo = strAnexo;
	}
	public String getStrSeqOs() {
		return strSeqOs;
	}
	public void setStrSeqOs(String strSeqOs) {
		this.strSeqOs = strSeqOs;
	}
	public String getStrSeqTrei() {
		return strSeqTrei;
	}
	public void setStrSeqTrei(String strSeqTrei) {
		this.strSeqTrei = strSeqTrei;
	}
        

}