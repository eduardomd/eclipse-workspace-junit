package com.altec.bsbr.app.hyb.dto;

public class CadastroOsCDebitoPontoVendaModel {
	
	private String NM_REDE;
	private String NM_REGI;
	private String CD_UOR;
	private String NM_UOR;
	private String TP_UOR;
		
	public CadastroOsCDebitoPontoVendaModel(String nM_REDE, String nM_REGI, String cD_UOR, String nM_UOR,
			String tP_UOR) {
		super();
		NM_REDE = nM_REDE;
		NM_REGI = nM_REGI;
		CD_UOR = cD_UOR;
		NM_UOR = nM_UOR;
		TP_UOR = tP_UOR;
	}
	public String getNM_REDE() {
		return NM_REDE;
	}
	public void setNM_REDE(String nM_REDE) {
		NM_REDE = nM_REDE;
	}
	public String getNM_REGI() {
		return NM_REGI;
	}
	public void setNM_REGI(String nM_REGI) {
		NM_REGI = nM_REGI;
	}
	public String getCD_UOR() {
		return CD_UOR;
	}
	public void setCD_UOR(String cD_UOR) {
		CD_UOR = cD_UOR;
	}
	public String getNM_UOR() {
		return NM_UOR;
	}
	public void setNM_UOR(String nM_UOR) {
		NM_UOR = nM_UOR;
	}
	public String getTP_UOR() {
		return TP_UOR;
	}
	public void setTP_UOR(String tP_UOR) {
		TP_UOR = tP_UOR;
	}
	
	
}
