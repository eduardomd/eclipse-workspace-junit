package com.altec.bsbr.app.hyb.dto;

import java.util.ArrayList;
import java.util.List;

public class CadastroOSDetalheProdutoFO {

	private String VL_TRAN;
	private String TP_ESPE;
	private String NM_BANC_CRED;
	private String TP_OUTR_FRAU;
	private String NR_SEQU_ORDE_SERV;
	private String DESC_TRAN;
	private String NM_LOCA_TRAN;
	private String TX_DADO_COMP;
	private String NM_CLIE_FAVD;
	private String DT_PGTO_CHEQ;
	private String TP_FRAU_CHEQ;
	private String TP_CNAL_PGTO;
	private String NR_DEPO;
	private String TP_SUBP_DEPO;
	private String TP_ABER_CNTA;
	private String NR_CHEQ;
	private String TP_FORM_DEPO;
	private String TP_SUBP;
	private String NR_CDC_LEAS_GRVM;
	private String NM_OPED_CDC_LEAS_GRVM;
	private String NR_PARC_CDC_LEAS_GRVM;
	private String TP_CNAL_ORIG_CDC_LEAS_GRVM;
	private String VL_PAGO_CDC_LEAS_GRVM;
	private String NR_EQPT_DEPO;
	private String NR_DEBT;
	private String NM_DEBT;
	private String CD_DEBT;
	private String NR_TITU;
	private String NM_CEDE_TITU;
	private String IN_CONF_ENDE_REFE;
	private String IN_DOCT_PROC_COPT;
	private String NM_BANC_CRED_00;
	private String NM_BANC_CRED_01;
	private String VL_ABER_CDC_LEAS_GRVM;
	private String TP_FRAU_AUTO_ATEN;
	private String TP_OPER_FRAU;
	private String strErro;
	private String strNrSeqTranFrau;
	private String VL_CNTA_CDC_LEAS_GRVM;
	private String TP_FLOAT;
	
	
	public CadastroOSDetalheProdutoFO() {
		
	}
	
	

	public CadastroOSDetalheProdutoFO(String vL_TRAN, String tP_ESPE, String nM_BANC_CRED, String tP_OUTR_FRAU,
			String nR_SEQU_ORDE_SERV, String dESC_TRAN, String nM_LOCA_TRAN, String tX_DADO_COMP, String nM_CLIE_FAVD,
			String dT_PGTO_CHEQ, String tP_FRAU_CHEQ, String tP_CNAL_PGTO, String nR_DEPO, String tP_SUBP_DEPO,
			String tP_ABER_CNTA, String nR_CHEQ, String tP_FORM_DEPO, String tP_SUBP, String nR_CDC_LEAS_GRVM,
			String nM_OPED_CDC_LEAS_GRVM, String nR_PARC_CDC_LEAS_GRVM, String tP_CNAL_ORIG_CDC_LEAS_GRVM,
			String vL_PAGO_CDC_LEAS_GRVM, String nR_EQPT_DEPO, String nR_DEBT, String nM_DEBT, String cD_DEBT,
			String nR_TITU, String nM_CEDE_TITU, String iN_CONF_ENDE_REFE, String iN_DOCT_PROC_COPT,
			String nM_BANC_CRED_00, String nM_BANC_CRED_01, String vL_ABER_CDC_LEAS_GRVM, String tP_FRAU_AUTO_ATEN,
			String tP_OPER_FRAU, String vL_CNTA_CDC_LEAS_GRVM,
			String tP_FLOAT) {
		super();
		VL_TRAN = vL_TRAN;
		TP_ESPE = tP_ESPE;
		NM_BANC_CRED = nM_BANC_CRED;
		TP_OUTR_FRAU = tP_OUTR_FRAU;
		NR_SEQU_ORDE_SERV = nR_SEQU_ORDE_SERV;
		DESC_TRAN = dESC_TRAN;
		NM_LOCA_TRAN = nM_LOCA_TRAN;
		TX_DADO_COMP = tX_DADO_COMP;
		NM_CLIE_FAVD = nM_CLIE_FAVD;
		DT_PGTO_CHEQ = dT_PGTO_CHEQ;
		TP_FRAU_CHEQ = tP_FRAU_CHEQ;
		TP_CNAL_PGTO = tP_CNAL_PGTO;
		NR_DEPO = nR_DEPO;
		TP_SUBP_DEPO = tP_SUBP_DEPO;
		TP_ABER_CNTA = tP_ABER_CNTA;
		NR_CHEQ = nR_CHEQ;
		TP_FORM_DEPO = tP_FORM_DEPO;
		TP_SUBP = tP_SUBP;
		NR_CDC_LEAS_GRVM = nR_CDC_LEAS_GRVM;
		NM_OPED_CDC_LEAS_GRVM = nM_OPED_CDC_LEAS_GRVM;
		NR_PARC_CDC_LEAS_GRVM = nR_PARC_CDC_LEAS_GRVM;
		TP_CNAL_ORIG_CDC_LEAS_GRVM = tP_CNAL_ORIG_CDC_LEAS_GRVM;
		VL_PAGO_CDC_LEAS_GRVM = vL_PAGO_CDC_LEAS_GRVM;
		NR_EQPT_DEPO = nR_EQPT_DEPO;
		NR_DEBT = nR_DEBT;
		NM_DEBT = nM_DEBT;
		CD_DEBT = cD_DEBT;
		NR_TITU = nR_TITU;
		NM_CEDE_TITU = nM_CEDE_TITU;
		IN_CONF_ENDE_REFE = iN_CONF_ENDE_REFE;
		IN_DOCT_PROC_COPT = iN_DOCT_PROC_COPT;
		NM_BANC_CRED_00 = nM_BANC_CRED_00;
		NM_BANC_CRED_01 = nM_BANC_CRED_01;
		VL_ABER_CDC_LEAS_GRVM = vL_ABER_CDC_LEAS_GRVM;
		TP_FRAU_AUTO_ATEN = tP_FRAU_AUTO_ATEN;
		TP_OPER_FRAU = tP_OPER_FRAU;
		VL_CNTA_CDC_LEAS_GRVM = vL_CNTA_CDC_LEAS_GRVM;
		TP_FLOAT = tP_FLOAT;
	}
	
	public String getVL_TRAN() {
		return VL_TRAN;
	}

	public void setVL_TRAN(String vL_TRAN) {
		VL_TRAN = vL_TRAN;
	}

	public String getTP_ESPE() {
		return TP_ESPE;
	}

	public void setTP_ESPE(String tP_ESPE) {
		TP_ESPE = tP_ESPE;
	}

	public String getNM_BANC_CRED() {
		return NM_BANC_CRED;
	}

	public void setNM_BANC_CRED(String nM_BANC_CRED) {
		NM_BANC_CRED = nM_BANC_CRED;
	}

	public String getTP_OUTR_FRAU() {
		return TP_OUTR_FRAU;
	}

	public void setTP_OUTR_FRAU(String tP_OUTR_FRAU) {
		TP_OUTR_FRAU = tP_OUTR_FRAU;
	}

	public String getNR_SEQU_ORDE_SERV() {
		return NR_SEQU_ORDE_SERV;
	}

	public void setNR_SEQU_ORDE_SERV(String nR_SEQU_ORDE_SERV) {
		NR_SEQU_ORDE_SERV = nR_SEQU_ORDE_SERV;
	}

	public String getDESC_TRAN() {
		return DESC_TRAN;
	}

	public void setDESC_TRAN(String dESC_TRAN) {
		DESC_TRAN = dESC_TRAN;
	}

	public String getNM_LOCA_TRAN() {
		return NM_LOCA_TRAN;
	}

	public void setNM_LOCA_TRAN(String nM_LOCA_TRAN) {
		NM_LOCA_TRAN = nM_LOCA_TRAN;
	}

	public String getTX_DADO_COMP() {
		return TX_DADO_COMP;
	}

	public void setTX_DADO_COMP(String tX_DADO_COMP) {
		TX_DADO_COMP = tX_DADO_COMP;
	}

	public String getNM_CLIE_FAVD() {
		return NM_CLIE_FAVD;
	}

	public void setNM_CLIE_FAVD(String nM_CLIE_FAVD) {
		NM_CLIE_FAVD = nM_CLIE_FAVD;
	}

	public String getDT_PGTO_CHEQ() {
		return DT_PGTO_CHEQ;
	}

	public void setDT_PGTO_CHEQ(String dT_PGTO_CHEQ) {
		DT_PGTO_CHEQ = dT_PGTO_CHEQ;
	}

	public String getTP_FRAU_CHEQ() {
		return TP_FRAU_CHEQ;
	}

	public void setTP_FRAU_CHEQ(String tP_FRAU_CHEQ) {
		TP_FRAU_CHEQ = tP_FRAU_CHEQ;
	}

	public String getTP_CNAL_PGTO() {
		return TP_CNAL_PGTO;
	}

	public void setTP_CNAL_PGTO(String tP_CNAL_PGTO) {
		TP_CNAL_PGTO = tP_CNAL_PGTO;
	}

	public String getNR_DEPO() {
		return NR_DEPO;
	}

	public void setNR_DEPO(String nR_DEPO) {
		NR_DEPO = nR_DEPO;
	}

	public String getTP_SUBP_DEPO() {
		return TP_SUBP_DEPO;
	}

	public void setTP_SUBP_DEPO(String tP_SUBP_DEPO) {
		TP_SUBP_DEPO = tP_SUBP_DEPO;
	}

	public String getTP_ABER_CNTA() {
		return TP_ABER_CNTA;
	}

	public void setTP_ABER_CNTA(String tP_ABER_CNTA) {
		TP_ABER_CNTA = tP_ABER_CNTA;
	}

	public String getNR_CHEQ() {
		return NR_CHEQ;
	}

	public void setNR_CHEQ(String nR_CHEQ) {
		NR_CHEQ = nR_CHEQ;
	}

	public String getTP_FORM_DEPO() {
		return TP_FORM_DEPO;
	}

	public void setTP_FORM_DEPO(String tP_FORM_DEPO) {
		TP_FORM_DEPO = tP_FORM_DEPO;
	}

	public String getTP_SUBP() {
		return TP_SUBP;
	}

	public void setTP_SUBP(String tP_SUBP) {
		TP_SUBP = tP_SUBP;
	}

	public String getNR_CDC_LEAS_GRVM() {
		return NR_CDC_LEAS_GRVM;
	}

	public void setNR_CDC_LEAS_GRVM(String nR_CDC_LEAS_GRVM) {
		NR_CDC_LEAS_GRVM = nR_CDC_LEAS_GRVM;
	}

	public String getNM_OPED_CDC_LEAS_GRVM() {
		return NM_OPED_CDC_LEAS_GRVM;
	}

	public void setNM_OPED_CDC_LEAS_GRVM(String nM_OPED_CDC_LEAS_GRVM) {
		NM_OPED_CDC_LEAS_GRVM = nM_OPED_CDC_LEAS_GRVM;
	}

	public String getNR_PARC_CDC_LEAS_GRVM() {
		return NR_PARC_CDC_LEAS_GRVM;
	}

	public void setNR_PARC_CDC_LEAS_GRVM(String nR_PARC_CDC_LEAS_GRVM) {
		NR_PARC_CDC_LEAS_GRVM = nR_PARC_CDC_LEAS_GRVM;
	}

	public String getTP_CNAL_ORIG_CDC_LEAS_GRVM() {
		return TP_CNAL_ORIG_CDC_LEAS_GRVM;
	}

	public void setTP_CNAL_ORIG_CDC_LEAS_GRVM(String tP_CNAL_ORIG_CDC_LEAS_GRVM) {
		TP_CNAL_ORIG_CDC_LEAS_GRVM = tP_CNAL_ORIG_CDC_LEAS_GRVM;
	}

	public String getVL_PAGO_CDC_LEAS_GRVM() {
		return VL_PAGO_CDC_LEAS_GRVM;
	}

	public void setVL_PAGO_CDC_LEAS_GRVM(String vL_PAGO_CDC_LEAS_GRVM) {
		VL_PAGO_CDC_LEAS_GRVM = vL_PAGO_CDC_LEAS_GRVM;
	}

	public String getNR_EQPT_DEPO() {
		return NR_EQPT_DEPO;
	}

	public void setNR_EQPT_DEPO(String nR_EQPT_DEPO) {
		NR_EQPT_DEPO = nR_EQPT_DEPO;
	}

	public String getNR_DEBT() {
		return NR_DEBT;
	}

	public void setNR_DEBT(String nR_DEBT) {
		NR_DEBT = nR_DEBT;
	}

	public String getNM_DEBT() {
		return NM_DEBT;
	}

	public void setNM_DEBT(String nM_DEBT) {
		NM_DEBT = nM_DEBT;
	}

	public String getCD_DEBT() {
		return CD_DEBT;
	}

	public void setCD_DEBT(String cD_DEBT) {
		CD_DEBT = cD_DEBT;
	}

	public String getNR_TITU() {
		return NR_TITU;
	}

	public void setNR_TITU(String nR_TITU) {
		NR_TITU = nR_TITU;
	}

	public String getNM_CEDE_TITU() {
		return NM_CEDE_TITU;
	}

	public void setNM_CEDE_TITU(String nM_CEDE_TITU) {
		NM_CEDE_TITU = nM_CEDE_TITU;
	}

	public String getIN_CONF_ENDE_REFE() {
		return IN_CONF_ENDE_REFE;
	}

	public void setIN_CONF_ENDE_REFE(String iN_CONF_ENDE_REFE) {
		IN_CONF_ENDE_REFE = iN_CONF_ENDE_REFE;
	}

	public String getIN_DOCT_PROC_COPT() {
		return IN_DOCT_PROC_COPT;
	}

	public void setIN_DOCT_PROC_COPT(String iN_DOCT_PROC_COPT) {
		IN_DOCT_PROC_COPT = iN_DOCT_PROC_COPT;
	}

	//AGENCIA
	public String getNM_BANC_CRED_00() {
		/*return this.NM_BANC_CRED.isEmpty() ?
				"" :
				this.NM_BANC_CRED.substring(0, this.NM_BANC_CRED.indexOf("|"));*/
		//split no | e index [0]
		return NM_BANC_CRED_00;
	}

	public void setNM_BANC_CRED_00(String nM_BANC_CRED_00) {
		NM_BANC_CRED_00 = nM_BANC_CRED_00;
	}
	
	//FILIAL
	public String getNM_BANC_CRED_01() {
		/*return this.NM_BANC_CRED.isEmpty() ?
				"" :
				this.NM_BANC_CRED.substring(this.NM_BANC_CRED.length() - (this.NM_BANC_CRED.length() - (this.NM_BANC_CRED.indexOf("|") + 1)));*/
		//split no | e index [1]
		return NM_BANC_CRED_01;
	}

	public void setNM_BANC_CRED_01(String nM_BANC_CRED_01) {
		NM_BANC_CRED_01 = nM_BANC_CRED_01;
	}

	public String getVL_ABER_CDC_LEAS_GRVM() {
		return VL_ABER_CDC_LEAS_GRVM;
	}

	public void setVL_ABER_CDC_LEAS_GRVM(String vL_ABER_CDC_LEAS_GRVM) {
		VL_ABER_CDC_LEAS_GRVM = vL_ABER_CDC_LEAS_GRVM;
	}

	public String getStrErro() {
		return strErro;
	}

	public void setStrErro(String strErro) {
		this.strErro = strErro;
	}

	public String getStrNrSeqTranFrau() {
		return strNrSeqTranFrau;
	}

	public void setStrNrSeqTranFrau(String strNrSeqTranFrau) {
		this.strNrSeqTranFrau = strNrSeqTranFrau;
	}

	public String getTP_FRAU_AUTO_ATEN() {
		return TP_FRAU_AUTO_ATEN;
	}

	public void setTP_FRAU_AUTO_ATEN(String tP_FRAU_AUTO_ATEN) {
		TP_FRAU_AUTO_ATEN = tP_FRAU_AUTO_ATEN;
	}

	public String getTP_OPER_FRAU() {
		return TP_OPER_FRAU;
	}

	public void setTP_OPER_FRAU(String tP_OPER_FRAU) {
		TP_OPER_FRAU = tP_OPER_FRAU;
	}

	public String getVL_CNTA_CDC_LEAS_GRVM() {
		return VL_CNTA_CDC_LEAS_GRVM;
	}

	public void setVL_CNTA_CDC_LEAS_GRVM(String vL_CNTA_CDC_LEAS_GRVM) {
		VL_CNTA_CDC_LEAS_GRVM = vL_CNTA_CDC_LEAS_GRVM;
	}

	public String getTP_FLOAT() {
		return TP_FLOAT;
	}

	public void setTP_FLOAT(String tP_FLOAT) {
		TP_FLOAT = tP_FLOAT;
	}
	
	
	
	
	
	
}
