package com.altec.bsbr.app.hyb.dto;

public class RelOsDadosContabeisRO {

	private String nOs;
	private String statusEvento;
	private String evento;
	private String area;
	private String empresa;
	private String linha1;
	private String linha2;
	private String codigoPV;
	private String categoria1;
	private String categoria2;
	private String categoria3;
	private String codigoCategoria;
	private String fatorRisco;
	private String dataOcorrencia;
	private String dataDeteccao;
	private String dataEncerramento;
	private double valorEnvolvido;
	private double valorEvitado;
	private double valorRecuperado;
	private String valorSeguro;
	private double valorPerdaEfetiva;
	private String dataPagamento;
	private String contaContabil;
	private String dataContabilizacao;
	private String causaEvento;
	private double valorExcluido;



	public RelOsDadosContabeisRO(String nOs, String statusEvento, String evento, String area, String empresa,
			String linha1, String linha2, String codigoPV, String categoria1, String categoria2, String categoria3,
			String codigoCategoria, String fatorRisco, String dataOcorrencia, String dataDeteccao,
			String dataEncerramento, double valorEnvolvido, double valorEvitado, double valorRecuperado,
			String valorSeguro, double valorPerdaEfetiva, String dataPagamento, String contaContabil,
			String dataContabilizacao, String causaEvento, double valorExcluido) {
		super();
		this.nOs = nOs == null? "": nOs.trim().replace("null", "");
		this.statusEvento = statusEvento == null? "" : statusEvento.trim().replace("null", "");
		this.evento = evento == null ? "" : evento.trim().replace("null", "");
		this.area = area == null ? "" : area.trim().replace("null", "");
		this.empresa = empresa == null ? "" : empresa.trim().replace("null", "");
		this.linha1 = linha1 == null ? "" : linha1.trim().replace("null", "");
		this.linha2 = linha2 == null ? "" : linha2.trim().replace("null", "");
		this.codigoPV = codigoPV == null ? "" : codigoPV.trim().replace("null", "");
		this.categoria1 = categoria1 == null ? "" : categoria1.trim().replace("null", "");
		this.categoria2 = categoria2 == null ? "" : categoria2.trim().replace("null", "");
		this.categoria3 = categoria3 == null ? "" : categoria3.trim().replace("null", "");
		this.codigoCategoria = codigoCategoria == null ? "" : codigoCategoria.trim().replace("null", "");
		this.fatorRisco = fatorRisco == null ? "" : fatorRisco.trim().replace("null", "");
		this.dataOcorrencia = dataOcorrencia == null ? "" : dataOcorrencia.trim().replace("null", "");
		this.dataDeteccao = dataDeteccao == null ? "" : dataDeteccao.trim().replace("null", "");
		this.dataEncerramento = dataEncerramento == null ? "" : dataEncerramento.trim().replace("null", "");
		this.valorEnvolvido = valorEnvolvido;
		this.valorEvitado = valorEvitado;
		this.valorRecuperado = valorRecuperado;
		this.valorSeguro = valorSeguro;
		this.valorPerdaEfetiva = valorPerdaEfetiva;
		this.dataPagamento = dataPagamento == null ? "" : dataPagamento.trim().replace("null", "");
		this.contaContabil = contaContabil == null ? "" : contaContabil.trim().replace("null", "");
		this.dataContabilizacao = dataContabilizacao == null ? "" : dataContabilizacao.trim().replace("null", "");
		this.causaEvento = causaEvento == null ? "" : causaEvento.trim().replace("null", "");
		this.valorExcluido = valorExcluido;
	}

	public double getValorEvitado() {
		return valorEvitado;
	}

	public void setValorEvitado(double valorEvitado) {
		this.valorEvitado = valorEvitado;
	}

	public String getnOs() {
		return nOs;
	}

	public void setnOs(String nOs) {
		this.nOs = nOs;
	}

	public String getStatusEvento() {
		return statusEvento;
	}

	public void setStatusEvento(String statusEvento) {
		this.statusEvento = statusEvento;
	}

	public String getEvento() {
		return evento;
	}

	public void setEvento(String evento) {
		this.evento = evento;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public String getLinha1() {
		return linha1;
	}

	public void setLinha1(String linha1) {
		this.linha1 = linha1;
	}

	public String getLinha2() {
		return linha2;
	}

	public void setLinha2(String linha2) {
		this.linha2 = linha2;
	}

	public String getCodigoPV() {
		return codigoPV;
	}

	public void setCodigoPV(String codigoPV) {
		this.codigoPV = codigoPV;
	}

	public String getCategoria1() {
		return categoria1;
	}

	public void setCategoria1(String categoria1) {
		this.categoria1 = categoria1;
	}

	public String getCategoria2() {
		return categoria2;
	}

	public void setCategoria2(String categoria2) {
		this.categoria2 = categoria2;
	}

	public String getCategoria3() {
		return categoria3;
	}

	public void setCategoria3(String categoria3) {
		this.categoria3 = categoria3;
	}

	public String getCodigoCategoria() {
		return codigoCategoria;
	}

	public void setCodigoCategoria(String codigoCategoria) {
		this.codigoCategoria = codigoCategoria;
	}

	public String getFatorRisco() {
		return fatorRisco;
	}

	public void setFatorRisco(String fatorRisco) {
		this.fatorRisco = fatorRisco;
	}

	public String getDataOcorrencia() {
		return dataOcorrencia;
	}

	public void setDataOcorrencia(String dataOcorrencia) {
		this.dataOcorrencia = dataOcorrencia;
	}

	public String getDataDeteccao() {
		return dataDeteccao;
	}

	public void setDataDeteccao(String dataDeteccao) {
		this.dataDeteccao = dataDeteccao;
	}

	public String getDataEncerramento() {
		return dataEncerramento;
	}

	public void setDataEncerramento(String dataEncerramento) {
		this.dataEncerramento = dataEncerramento;
	}

	public double getValorEnvolvido() {
		return valorEnvolvido;
	}

	public void setValorEnvolvido(double valorEnvolvido) {
		this.valorEnvolvido = valorEnvolvido;
	}

	public double getValorRecuperado() {
		return valorRecuperado;
	}

	public void setValorRecuperado(double valorRecuperado) {
		this.valorRecuperado = valorRecuperado;
	}

	public String getValorSeguro() {
		return valorSeguro;
	}

	public void setValorSeguro(String valorSeguro) {
		this.valorSeguro = valorSeguro;
	}

	public double getValorPerdaEfetiva() {
		return valorPerdaEfetiva;
	}

	public void setValorPerdaEfetiva(double valorPerdaEfetiva) {
		this.valorPerdaEfetiva = valorPerdaEfetiva;
	}

	public String getDataPagamento() {
		return dataPagamento;
	}

	public void setDataPagamento(String dataPagamento) {
		this.dataPagamento = dataPagamento;
	}

	public String getContaContabil() {
		return contaContabil;
	}

	public void setContaContabil(String contaContabil) {
		this.contaContabil = contaContabil;
	}

	public String getDataContabilizacao() {
		return dataContabilizacao;
	}

	public void setDataContabilizacao(String dataContabilizacao) {
		this.dataContabilizacao = dataContabilizacao;
	}

	public String getCausaEvento() {
		return causaEvento;
	}

	public void setCausaEvento(String causaEvento) {
		this.causaEvento = causaEvento;
	}

	public double getValorExcluido() {
		return valorExcluido;
	}

	public void setValorExcluido(double valorExcluido) {
		this.valorExcluido = valorExcluido;
	}
	
}
