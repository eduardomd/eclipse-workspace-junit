package com.altec.bsbr.app.hyb.web.jsf;

import java.awt.Color;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;


import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jfree.ui.NumberCellRenderer;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.RelOsDadosContabeisRO;
import com.altec.bsbr.app.jab.hyb.webclient.XHYRelatorios.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYRelatorios.XHYRelatoriosEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.ibm.icu.text.NumberFormat;

import net.sf.jasperreports.components.table.Cell;

@Component("RelOsDadosContabeisROBean")
@Scope("session")
public class RelOsDadosContabeisROBean extends BasicBBean {

	private static final long serialVersionUID = 1L;

	@Autowired
	private XHYRelatoriosEndPoint relatorioEP;

	private String strDtIni;
	private String strDtFim;
	private String strCodArea;
	private String dataAtual;
	private int finalPrimeiraColuna;
	private int finalSegundaColuna;
	private double totalValorEnvolvido;
	private double totalValorEvitado;
	private double totalValorRecuperado;
	private double totalValorPerdaEfetiva;
	private double totalValorExcluido;

	private List<RelOsDadosContabeisRO> relatorio;

	

	@PostConstruct
	public void init() throws IOException {
		 totalValorEnvolvido = 0.0;
		 totalValorEvitado = 0.0;
		 totalValorRecuperado = 0.0;
		 totalValorPerdaEfetiva = 0.0;
		 totalValorExcluido = 0.0;
		 relatorio = new ArrayList<RelOsDadosContabeisRO>();
		
		FacesContext fc = FacesContext.getCurrentInstance();
		try {
			Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
	
			this.strDtIni = params.get("pDtIni");
			this.strDtFim = params.get("pDtFim");
			this.strCodArea = params.get("pCodArea");
			
		} catch (NullPointerException ex) {
			ex.printStackTrace();
		}
		
		buscarDadosRelatorio();
	}
	public void buscarDadosRelatorio() {
		JSONArray pcursorObjRs = new JSONArray();
		try {
			String objRs = relatorioEP.fnRelRiscoOper(this.strDtIni, this.strDtFim, this.strCodArea);
			JSONObject objResTemp = new JSONObject(objRs);
			pcursorObjRs = objResTemp.getJSONArray("PCURSOR");
		} catch (WebServiceException e) {
			e.printStackTrace();
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("hy_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		for (int i = 0; i < pcursorObjRs.length(); i++) {
			JSONObject temp = pcursorObjRs.getJSONObject(i);

			relatorio.add(new RelOsDadosContabeisRO(
					temp.isNull("NUMERO_OS") ? "" : temp.get("NUMERO_OS").toString(),
					temp.isNull("STATUS_REPORT") ? "" : temp.get("STATUS_REPORT").toString(),
					temp.isNull("NM_EVEN") ? "" : temp.get("NM_EVEN").toString(),
					temp.isNull("NM_AREA") ? "" : temp.get("NM_AREA").toString(),
					temp.isNull("EMPRESA") ? "" : temp.get("EMPRESA").toString(),
					temp.isNull("LINHA_NEGO_NIVEL1") ? "" : temp.get("LINHA_NEGO_NIVEL1").toString(),
					temp.isNull("LINHA_NEGO_NIVEL2") ? "" : temp.get("LINHA_NEGO_NIVEL2").toString(),
					temp.isNull("CD_PV") ? "" : temp.get("CD_PV").toString(),
					temp.isNull("CATG_NIVEL1") ? "" : temp.get("CATG_NIVEL1").toString(),
					temp.isNull("CATG_NIVEL2") ? "" : temp.get("CATG_NIVEL2").toString(),
					temp.isNull("CATG_NIVEL3") ? "" : temp.get("CATG_NIVEL3").toString(),
					temp.isNull("COD_CATG_NIVEL3") ? "" : temp.get("COD_CATG_NIVEL3").toString(),
					temp.isNull("FATOR_RISCO") ? "" : temp.get("FATOR_RISCO").toString(),
					temp.isNull("DATA_OCORRENCIA") ? "" : temp.get("DATA_OCORRENCIA").toString(),
					temp.isNull("DATE_DETECCAO") ? "" : temp.get("DATE_DETECCAO").toString(),
					temp.isNull("DATA_PREV_ENCERRAMENTO") ? "" : temp.get("DATA_PREV_ENCERRAMENTO").toString(),
					temp.isNull("VL_ENVO") ? 0 : temp.getDouble("VL_ENVO"),
					temp.isNull("VL_EXCL_EVIT") ? 0 : temp.getDouble("VL_EXCL_EVIT"),
					temp.isNull("VL_RECR") ? 0 : temp.getDouble("VL_RECR"),
					"Valor recuperado por Seguro",
					temp.isNull("VL_PERD_EFET") ? 0 : temp.getDouble("VL_PERD_EFET"),
					"Data Pagamento Seguro",
					temp.isNull("CONTA_CONTABIL") ? "" : temp.get("CONTA_CONTABIL").toString(),
					temp.isNull("DATA_CNTB") ? "" : temp.get("DATA_CNTB").toString(),
					"Causa do Evento",
					temp.isNull("VL_EXCL_EVIT") ? 0 : temp.getDouble("VL_EXCL_EVIT")));
		}
		
		cleanSession();
	}

	public void cleanSession() {
		FacesContext context = FacesContext.getCurrentInstance();
		if (context.getExternalContext().getSessionMap().get("relatorioosBean") != null) {
			context.getExternalContext().getSessionMap().remove("relatorioosBean");
		}
	}
	public String now() {
		DateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		Date date = new Date();
		return dateformat.format(date);
	}
	
	public void fazerContas() {
		for(int i = 0; i < relatorio.size(); i++) {
			totalValorEnvolvido = totalValorEnvolvido + relatorio.get(i).getValorEnvolvido();
			 totalValorEvitado = totalValorEvitado + relatorio.get(i).getValorEvitado();
			 totalValorRecuperado = totalValorRecuperado + relatorio.get(i).getValorRecuperado();
			 totalValorPerdaEfetiva = totalValorPerdaEfetiva + relatorio.get(i).getValorPerdaEfetiva();
			 totalValorExcluido = totalValorExcluido + relatorio.get(i).getValorExcluido();
		}
	}
	public void postProcessExcel(Object doc) {
		try {
			XSSFWorkbook wb = (XSSFWorkbook) doc;
			XSSFSheet sheet = wb.getSheetAt(0);
			sheet.setDisplayGridlines(false);
			
			formatarCabecalho(wb, sheet);

			formatarCabecalhoTabela(wb, sheet);

			formatarCabecalhoSubTabela(wb,sheet);

			formatarRegistrosTabela(wb, sheet);
			
			formatarCabecalhoTabelaTb2(wb, sheet);
			

			formatarCabecalhoSubTabelaTb2(wb,sheet);

			formatarRegistrosTabelaTb2(wb, sheet);
			estiloLinhaTotal(wb,sheet);
			
			
			colocarBorda(sheet,wb);

			criarRodape(wb, sheet);
			redefinirColunas(wb);
			sheet.shiftRows(3, sheet.getLastRowNum() + 1, 1);
			
		} catch (Exception e) {
			System.out.println(e);
		}
		
	}
	
	public void redefinirColunas(XSSFWorkbook wb) {
		XSSFSheet sheet = wb.getSheetAt(0);
		for (int i = 0; i <= sheet.getRow(7).getLastCellNum(); i++) {
			sheet.autoSizeColumn(i);
			
			if(sheet.getColumnWidth(i) < 3000) {
				sheet.setColumnWidth(i, 3000);
			}
		}
	
		
	
	}
	private void colocarBorda(XSSFSheet sheet,XSSFWorkbook wb) {
		int nbrMergedRegions = sheet.getNumMergedRegions();
		
		for(int i = 3; i < nbrMergedRegions;i++) {
			CellRangeAddress a = sheet.getMergedRegion(i);
			RegionUtil.setBorderTop(CellStyle.BORDER_THIN, a, sheet, wb);
			RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, a, sheet, wb);
			RegionUtil.setBorderRight(CellStyle.BORDER_THIN, a, sheet, wb);
			RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, a, sheet, wb);
			
		}
		
	}

	
	private void formatarCabecalho(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyle1 = wb.createCellStyle();
		headerStyle1.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle1.setWrapText(true);
		headerStyle1.setBorderBottom(CellStyle.BORDER_NONE);
		headerStyle1.setBorderLeft(CellStyle.BORDER_NONE);
		headerStyle1.setBorderRight(CellStyle.BORDER_NONE);
		headerStyle1.setBorderTop(CellStyle.BORDER_NONE);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 14);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		headerStyle1.setFont(font);
		
		XSSFRow primeiraLinha = sheet.getRow(1);
		if (primeiraLinha != null) {
			XSSFCell primeiraLinhaCell = primeiraLinha.getCell(0);
			primeiraLinhaCell.setCellStyle(headerStyle1);
			primeiraLinha.setHeightInPoints((short) 19);
		}
		XSSFRow segundaLinha = sheet.getRow(2);
		if (segundaLinha != null) {
			XSSFCell segundaLinhaCell = segundaLinha.getCell(0);
			segundaLinhaCell.setCellStyle(headerStyle1);
			segundaLinha.setHeightInPoints((short) 19);
		}

	}
	// PRIMEIRA TABELA
	
	private void formatarCabecalhoTabela(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setWrapText(true);
		headerStyle.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyle.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyle.setBorderRight(CellStyle.BORDER_THIN);
		headerStyle.setBorderTop(CellStyle.BORDER_THIN);
		headerStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.WHITE.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(font);
		
		XSSFRow linhaCabecalhoTabela = sheet.getRow(6);
		linhaCabecalhoTabela.setHeightInPoints(15);
		if (linhaCabecalhoTabela != null) {
			for (int i = 0; i <= linhaCabecalhoTabela.getLastCellNum(); i++) {
				XSSFCell celulaHeader = linhaCabecalhoTabela.getCell(i);
				if (celulaHeader != null) {
					celulaHeader.setCellStyle(headerStyle);
				}
			}
		}
		XSSFRow linhaCabecalhoTabelaDois = sheet.getRow(7);
		linhaCabecalhoTabelaDois.setHeightInPoints(15);
		if (linhaCabecalhoTabelaDois != null) {
			for (int i = 0; i <= linhaCabecalhoTabelaDois.getLastCellNum(); i++) {
				XSSFCell celulaHeader = linhaCabecalhoTabelaDois.getCell(i);
				if (celulaHeader != null) {
					celulaHeader.setCellStyle(headerStyle);
				}
			}
		}

	}
	
	private void formatarCabecalhoSubTabela(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setWrapText(true);
		headerStyle.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyle.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyle.setBorderRight(CellStyle.BORDER_THIN);
		headerStyle.setBorderTop(CellStyle.BORDER_THIN);
		headerStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.WHITE.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(font);

		XSSFRow linhaCabecalhoTabela = sheet.getRow(8);
		linhaCabecalhoTabela.setHeightInPoints(20);
		if (linhaCabecalhoTabela != null) {
			for (int i = 0; i <= linhaCabecalhoTabela.getLastCellNum(); i++) {
				XSSFCell celulaHeader = linhaCabecalhoTabela.getCell(i);
				if (celulaHeader != null) {
					celulaHeader.setCellStyle(headerStyle);
				}
			}
		}
	}

	private void formatarRegistrosTabela(XSSFWorkbook wb, XSSFSheet sheet) {
		
		if (relatorio!= null && !relatorio.isEmpty()) {
			formatarLinhasDetalhe(wb, sheet);
		} else {
			formatarDetalheVazio(wb, sheet);
		}
		
	}

	private void formatarDetalheVazio(XSSFWorkbook wb, XSSFSheet sheet) {

		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setWrapText(true);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(font);
		
		CellRangeAddress detalheVazio = new CellRangeAddress(9, 9, 0, 16);
		sheet.addMergedRegion(detalheVazio);
		
		XSSFRow linhaVazia = sheet.getRow(9);
		if (linhaVazia == null) {
			linhaVazia = sheet.createRow(9);
		}
		XSSFCell celulaVazia = linhaVazia.createCell(0);
		linhaVazia.setHeightInPoints((short) 20);
		celulaVazia.setCellValue("Não existem dados para o período selecionado.");
		celulaVazia.setCellStyle(headerStyle);
		
		RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
		RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
		RegionUtil.setBorderRight(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
		RegionUtil.setBorderTop(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
		finalPrimeiraColuna = 11;
	}

	private void formatarLinhasDetalhe(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyleLeft = wb.createCellStyle();
		headerStyleLeft.setAlignment(CellStyle.ALIGN_LEFT);
		headerStyleLeft.setWrapText(true);
		headerStyleLeft.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderTop(CellStyle.BORDER_THIN);
		
		XSSFCellStyle headerStyleRight = wb.createCellStyle();
		headerStyleRight.setAlignment(CellStyle.ALIGN_RIGHT);
		headerStyleRight.setWrapText(true);
		headerStyleRight.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderTop(CellStyle.BORDER_THIN);
		
		
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		headerStyleLeft.setFont(font);
		headerStyleRight.setFont(font);
		
		int linhaInicial = 9;
		int linhasDetalhe = relatorio.size();
		int linhaFinal = linhaInicial + linhasDetalhe;
		finalPrimeiraColuna = linhaFinal+2;

		for (int linha = linhaInicial; linha <= linhaFinal; linha++) {
			XSSFRow detalheTabela = sheet.getRow(linha);
			if (detalheTabela != null) {
				for (int i = 0; i <= detalheTabela.getLastCellNum(); i++) {
					XSSFCell celulaDetalhe = detalheTabela.getCell(i);
					if (celulaDetalhe != null) {
						if (i == 1 || i == 14 || i == 15 || i ==16) {
							celulaDetalhe.setCellStyle(headerStyleRight);
						} else {
							celulaDetalhe.setCellStyle(headerStyleLeft);
						}
					}
				}
			}
		}
	}

	private void criarRodape(XSSFWorkbook wb, XSSFSheet sheet) {
		Font font;
		/*Criação, inclusão e formatação dos dados do footer do excel*/
		XSSFRow ultimaLinha = sheet.createRow(sheet.getLastRowNum() + 1);
		
		XSSFCellStyle footerStyle = wb.createCellStyle();
		footerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		footerStyle.setWrapText(true);
		font = footerStyle.getFont();
		font.setFontHeight((short) 150);
		font.setFontName("Arial");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		footerStyle.setFont(font);
		
		XSSFCell cell1 = ultimaLinha.createCell(0);
		cell1.setCellValue(now());
		cell1.setCellStyle(footerStyle);
		
		XSSFCell cell2 = ultimaLinha.createCell(3);
		cell2.setCellValue("Superintêndencia de Ocorrências Especiais");
		cell2.setCellStyle(footerStyle);
		
		CellRangeAddress region1 = new CellRangeAddress(ultimaLinha.getRowNum(), ultimaLinha.getRowNum(), 0, 2);
		CellRangeAddress region2 = new CellRangeAddress(ultimaLinha.getRowNum(), ultimaLinha.getRowNum(), 3, 16);
		sheet.addMergedRegion(region1);
		sheet.addMergedRegion(region2);
	}
	
	private void estiloLinhaTotal(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyle.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyle.setBorderRight(CellStyle.BORDER_THIN);
		headerStyle.setBorderTop(CellStyle.BORDER_THIN);
		headerStyle.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.WHITE.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(font);
		
		XSSFRow linhaCabecalhoTabela = sheet.getRow(finalSegundaColuna);
		linhaCabecalhoTabela.setHeightInPoints(20);
		
		fazerContas();
		for(int k = 0 ; k < sheet.getRow(finalPrimeiraColuna + 2).getLastCellNum();k++) {
			NumberFormat formatacao = NumberFormat.getCurrencyInstance(Locale.getDefault());
			switch(k) {
 			case 2:
				if (totalValorEnvolvido == 0) {
					linhaCabecalhoTabela.getCell(k).setCellValue(0);
				} else {
					linhaCabecalhoTabela.getCell(k).setCellValue(NumberFormat.getCurrencyInstance().format(totalValorEnvolvido).replace(formatacao.getCurrency().getSymbol(), ""));
				}
				
				break;
			case 3:
				if (totalValorEvitado == 0) {
					linhaCabecalhoTabela.getCell(k).setCellValue(0);
				} else {
					linhaCabecalhoTabela.getCell(k).setCellValue(NumberFormat.getCurrencyInstance().format(totalValorEvitado).replace(formatacao.getCurrency().getSymbol(), ""));
				}
				
				break;
			case 4:
				if (totalValorRecuperado == 0) {
					linhaCabecalhoTabela.getCell(k).setCellValue(0);
				} else {
					linhaCabecalhoTabela.getCell(k).setCellValue(NumberFormat.getCurrencyInstance().format(totalValorRecuperado).replace(formatacao.getCurrency().getSymbol(), ""));
				}
				
				break;
			case 6:
				if (totalValorPerdaEfetiva == 0) {
					linhaCabecalhoTabela.getCell(k).setCellValue(0);
				} else {
					linhaCabecalhoTabela.getCell(k).setCellValue(NumberFormat.getCurrencyInstance().format(totalValorPerdaEfetiva).replace(formatacao.getCurrency().getSymbol(), ""));
				}

				break;
			case 11:
				if (totalValorExcluido == 0) {
					linhaCabecalhoTabela.getCell(k).setCellValue(0);
				} else {
					linhaCabecalhoTabela.getCell(k).setCellValue(NumberFormat.getCurrencyInstance().format(totalValorExcluido).replace(formatacao.getCurrency().getSymbol(), ""));
				}

				break;
						
			}
		}
		
		if (linhaCabecalhoTabela != null) {
			for (int i = 0; i <= linhaCabecalhoTabela.getLastCellNum(); i++) {
				XSSFCell celulaHeader = linhaCabecalhoTabela.getCell(i);
				if (celulaHeader != null) {
					celulaHeader.setCellStyle(headerStyle);
				}
			}
		}
	}
	
	
	
	//Segunda tabela
	
	
	private void formatarCabecalhoTabelaTb2(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setWrapText(true);
		headerStyle.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyle.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyle.setBorderRight(CellStyle.BORDER_THIN);
		headerStyle.setBorderTop(CellStyle.BORDER_THIN);
		headerStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.WHITE.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(font);

		XSSFRow linhaCabecalhoTabela = sheet.getRow(finalPrimeiraColuna+1);
		linhaCabecalhoTabela.setHeightInPoints(20);
		if (linhaCabecalhoTabela != null) {
			for (int i = 0; i <= linhaCabecalhoTabela.getLastCellNum(); i++) {
				XSSFCell celulaHeader = linhaCabecalhoTabela.getCell(i);
				if (celulaHeader != null) {
					celulaHeader.setCellStyle(headerStyle);
				}
			}
		}

	}
	
	private void formatarCabecalhoSubTabelaTb2(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setWrapText(true);
		headerStyle.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyle.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyle.setBorderRight(CellStyle.BORDER_THIN);
		headerStyle.setBorderTop(CellStyle.BORDER_THIN);
		headerStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.WHITE.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(font);
		
		XSSFRow linhaCabecalhoTabela = sheet.getRow(finalPrimeiraColuna+2);
		linhaCabecalhoTabela.setHeightInPoints(20);
		if (linhaCabecalhoTabela != null) {
			for (int i = 0; i <= linhaCabecalhoTabela.getLastCellNum(); i++) {
				XSSFCell celulaHeader = linhaCabecalhoTabela.getCell(i);
				if (celulaHeader != null) {
					celulaHeader.setCellStyle(headerStyle);
				}
			}
		}
	}

	private void formatarRegistrosTabelaTb2(XSSFWorkbook wb, XSSFSheet sheet) {
		
		if (relatorio!= null && !relatorio.isEmpty()) {
			formatarLinhasDetalhethTb2(wb, sheet);
		} else {
			formatarDetalheVazioTb2(wb, sheet);
		}/*
		for (int i = 0; i <= 16; i++) {
			sheet.autoSizeColumn(i);
			sheet.autoSizeColumn(i,true);
		}*/
	}

	private void formatarDetalheVazioTb2(XSSFWorkbook wb, XSSFSheet sheet) {

		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setWrapText(true);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(font);
		

		sheet.shiftRows(finalPrimeiraColuna+3,finalPrimeiraColuna+4, 1);
		
		CellRangeAddress detalheVazio = new CellRangeAddress(finalPrimeiraColuna+3,finalPrimeiraColuna+3, 0, 11);
		sheet.addMergedRegion(detalheVazio);
		
		XSSFRow linhaVazia = sheet.getRow(finalPrimeiraColuna+3);
		if (linhaVazia == null) {
			linhaVazia = sheet.createRow(finalPrimeiraColuna+3);
		}
		
		XSSFCell celulaVazia = linhaVazia.createCell(0);
		linhaVazia.setHeightInPoints((short) 20);
		celulaVazia.setCellValue("Não existem dados para o período selecionado.");
		celulaVazia.setCellStyle(headerStyle);
		
		RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
		RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
		RegionUtil.setBorderRight(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
		RegionUtil.setBorderTop(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);

		finalSegundaColuna = finalPrimeiraColuna+4;
		
	}

	private void formatarLinhasDetalhethTb2(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setWrapText(true);
		headerStyle.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyle.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyle.setBorderRight(CellStyle.BORDER_THIN);
		headerStyle.setBorderTop(CellStyle.BORDER_THIN);
		headerStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		
		XSSFCellStyle headerStyleLeft = wb.createCellStyle();
		headerStyleLeft.setAlignment(CellStyle.ALIGN_LEFT);
		headerStyleLeft.setWrapText(true);
		headerStyleLeft.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderTop(CellStyle.BORDER_THIN);
		
		XSSFCellStyle headerStyleRight = wb.createCellStyle();
		headerStyleRight.setAlignment(CellStyle.ALIGN_RIGHT);
		headerStyleRight.setWrapText(true);
		headerStyleRight.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderTop(CellStyle.BORDER_THIN);
		
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.WHITE.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(font);
		headerStyleLeft.setFont(font);
		headerStyleRight.setFont(font);
		
		int linhaInicial = finalPrimeiraColuna+3;
		int linhasDetalhe = relatorio.size();
		int linhaFinal = linhaInicial + linhasDetalhe;
		finalSegundaColuna = linhaFinal;

		for (int linha = linhaInicial; linha <= linhaFinal; linha++) {
			XSSFRow detalheTabela = sheet.getRow(linha);
			if (detalheTabela != null) {
				for (int i = 0; i <= detalheTabela.getLastCellNum(); i++) {
					XSSFCell celulaDetalhe = detalheTabela.getCell(i);
					if (celulaDetalhe != null) {
						if (i == 1 || i == 2 || i == 3 || i == 4 || i == 5 || i == 6 || i == 7 || i == 9 || i == 11) {
							celulaDetalhe.setCellStyle(headerStyleRight);
						} else {
							if(i == 0) {
								celulaDetalhe.setCellStyle(headerStyleLeft);
							} else {
								celulaDetalhe.setCellStyle(headerStyle);
							}
						}
					}
				}
			}
		}
	}
	

	public String getStrDtIni() {
		return strDtIni;
	}

	public void setStrDtIni(String strDtIni) {
		this.strDtIni = strDtIni;
	}

	public String getStrDtFim() {
		return strDtFim;
	}

	public void setStrDtFim(String strDtFim) {
		this.strDtFim = strDtFim;
	}

	public List<RelOsDadosContabeisRO> getRelatorio() {
		return relatorio;
	}

	public void setRelatorio(List<RelOsDadosContabeisRO> relatorio) {
		this.relatorio = relatorio;
	}

	public String getStrCodArea() {
		return strCodArea;
	}

	public void setStrCodArea(String strCodArea) {
		this.strCodArea = strCodArea;
	}

	public String getDataAtual() {
		return dataAtual;
	}

	public void setDataAtual(String dataAtual) {
		this.dataAtual = dataAtual;
	}

	public int getFinalPrimeiraColuna() {
		return finalPrimeiraColuna;
	}

	public void setFinalPrimeiraColuna(int finalPrimeiraColuna) {
		this.finalPrimeiraColuna = finalPrimeiraColuna;
	}

	public int getFinalSegundaColuna() {
		return finalSegundaColuna;
	}

	public void setFinalSegundaColuna(int finalSegundaColuna) {
		this.finalSegundaColuna = finalSegundaColuna;
	}

	public Double getTotalValorEnvolvida() {
		return totalValorEnvolvido;
	}

	public void setTotalValorEnvolvida(Double totalValorEnvolvida) {
		this.totalValorEnvolvido = totalValorEnvolvida;
	}

	public Double getTotalValorEvitado() {
		return totalValorEvitado;
	}

	public void setTotalValorEvitado(Double totalValorEvitado) {
		this.totalValorEvitado = totalValorEvitado;
	}

	public Double getTotalValorRecuperado() {
		return totalValorRecuperado;
	}

	public void setTotalValorRecuperado(Double totalValorRecuperado) {
		this.totalValorRecuperado = totalValorRecuperado;
	}

	public Double getTotalValorPerdeEfetiva() {
		return totalValorPerdaEfetiva;
	}

	public void setTotalValorPerdeEfetiva(Double totalValorPerdeEfetiva) {
		this.totalValorPerdaEfetiva = totalValorPerdeEfetiva;
	}

	public Double getTotalValorExcluido() {
		return totalValorExcluido;
	}

	public void setTotalValorExcluido(Double totalValorExcluido) {
		this.totalValorExcluido = totalValorExcluido;
	}

	
	
}
