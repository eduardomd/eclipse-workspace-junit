package com.altec.bsbr.app.hyb.dto;

public class ObjRsManutCanaisCanalModel {	
	private String CODIGO;
	private String NOME;
	private boolean EXCLUSAO;
	
	public ObjRsManutCanaisCanalModel() {}
	
	public ObjRsManutCanaisCanalModel(String cODIGO, String nOME, boolean eXCLUSAO) {
		super();
		CODIGO = cODIGO;
		NOME = nOME;
		EXCLUSAO = eXCLUSAO;
	}
	
	public String getCODIGO() {
		return CODIGO;
	}
	public void setCODIGO(String cODIGO) {
		CODIGO = cODIGO;
	}
	public String getNOME() {
		return NOME;
	}
	public void setNOME(String nOME) {
		NOME = nOME;
	}	
	
	public boolean isEXCLUSAO() {
		return EXCLUSAO;
	}
	public void setEXCLUSAO(boolean eXCLUSAO) {
		EXCLUSAO = eXCLUSAO;
	}

}
