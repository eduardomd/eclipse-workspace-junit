package com.altec.bsbr.app.hyb.dto;

public class CadastroOsSLinhaTelfAcess {
	
	private String NR_TELF;
	private String NR_SEQU_TELF_ACES_CNTA;
	
	public CadastroOsSLinhaTelfAcess() {
		
	}
	
	public CadastroOsSLinhaTelfAcess(String nR_TELF, String nR_SEQU_TELF_ACES_CNTA) {
		NR_TELF = nR_TELF;
		NR_SEQU_TELF_ACES_CNTA = nR_SEQU_TELF_ACES_CNTA;
	}
	
	public String getNR_TELF() {
		return NR_TELF;
	}
	
	public void setNR_TELF(String nR_TELF) {
		NR_TELF = nR_TELF;
	}
	
	public String getNR_SEQU_TELF_ACES_CNTA() {
		return NR_SEQU_TELF_ACES_CNTA;
	}
	
	public void setNR_SEQU_TELF_ACES_CNTA(String nR_SEQU_TELF_ACES_CNTA) {
		NR_SEQU_TELF_ACES_CNTA = nR_SEQU_TELF_ACES_CNTA;
	}

}
