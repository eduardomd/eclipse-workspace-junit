package com.altec.bsbr.app.hyb.dto;

public class CadastroOsSLinhaIBanking {
	
	private String NR_SEQU_FRAU_CNAL;
	private String NR_CNTA_ACES;
	
	public CadastroOsSLinhaIBanking() {
		
	}
	
	public CadastroOsSLinhaIBanking(String nR_SEQU_FRAU_CNAL, String nR_CNTA_ACES) {
		NR_SEQU_FRAU_CNAL = nR_SEQU_FRAU_CNAL;
		NR_CNTA_ACES = nR_CNTA_ACES;
	}
	public String getNR_SEQU_FRAU_CNAL() {
		return NR_SEQU_FRAU_CNAL;
	}
	public void setNR_SEQU_FRAU_CNAL(String nR_SEQU_FRAU_CNAL) {
		NR_SEQU_FRAU_CNAL = nR_SEQU_FRAU_CNAL;
	}
	public String getNR_CNTA_ACES() {
		return NR_CNTA_ACES;
	}
	public void setNR_CNTA_ACES(String nR_CNTA_ACES) {
		NR_CNTA_ACES = nR_CNTA_ACES;
	}
	

}
