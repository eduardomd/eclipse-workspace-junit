package com.altec.bsbr.app.hyb.web.jsf;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;


@ManagedBean
@ViewScoped
public class BtGenericoBean implements Serializable {
	
	private String strFunction;
	private String strButton;
	private String strWidthBtn;
	
	//strIFrame - vem junto com os bot�es
	//strNameButton="Salvar:50/Salvar@Voltar/Voltar"
	
	//intCont=0
	//strNameButton="Salvar"
	//intSize=50
	//strIFrame=""
	
	//Chamada quando n�o passar o bot�o
	/*If CInt(UBound(objAryFunction)) = 0 Then
			'- Monta fun��es que chamam as fun��es do frame principal.
			strFunction = strFunction & "function " & CStr(objArrWidth(0)) & "()" & VbCrLf
			strFunction = strFunction & "{" & VbCrLf
			strFunction = strFunction & "  parent.principal." & strIFrame & CStr(objArrWidth(0)) & "()" & VbCrLf
			strFunction = strFunction & "}"  & VbCrLf	 & VbCrLf
			'- Monta as fun��es que habilitam e desabilitam bot�es.
			strBtnHabilitaDesabilita		=	strBtnHabilitaDesabilita		&	"	document.frmBot.btn" & CStr(objArrWidth(0)) & ".disabled = status"&intCont&";"	&	VbCrLf
			'- Monta os bot�es
			'strButton = strButton & "<input type='button' class='btn_110' value='" & CStr(objArrWidth(0)) & "' name='btn" & CStr(objArrWidth(0)) &"' id='btn" & CStr(objArrWidth(0)) &"'	style='cursor:hand;width:" & strWidthBtn & "px;' onClick='"& CStr(objArrWidth(0)) &"();' >&nbsp;" & VbCrLf & "			"			
			strButton = strButton & "<input type='button' class='' value='" & CStr(objArrWidth(0)) & "' name='btn" & CStr(objArrWidth(0)) &"' id='btn" & CStr(objArrWidth(0)) &"'	onClick='"& CStr(objArrWidth(0)) &"();' >&nbsp;" & VbCrLf & "			"			
		Else	*/
		
	
	
	//ESTE MÉTODO DEVERIA RETORNAR UM VALOR MAS ESTA SEM RETORNO, COLOQUEI PARA RETORNAR VAZIO. REVISAR 
	public String gerarBt(int intCont, String strNameButton, String strWidthBtn, String strIFrame) {

		String objAryFunction = "";
		String objArrNameWidth = "";
		String strBtnHabilitaDesabilita = "function ML_Status_Botao_fn(status" + intCont + "){" + System.lineSeparator();
		String strFunction;
		
		if (strIFrame!="") {
			strIFrame += ".";
		}

		strFunction = "<script language='JavaScript'><!-- " + System.lineSeparator();

		if (strWidthBtn == "")
			strWidthBtn = "80";
			
		// Monta fun��es que chamam as fun��es do frame principal.
		strFunction += "function " + strNameButton + "(){\n";
		strFunction += "  parent.principal." + strIFrame + strNameButton + "()\n";
		strFunction += "}\n\n";
		// Monta as fun��es que habilitam e desabilitam bot�es.
		strBtnHabilitaDesabilita += "document.frmBot.btn" + strNameButton + ".disabled = status" + intCont + ";\n";
		// Monta os bot�es
		strButton += "<input type='button' class='' value='" + strNameButton + "' name='btn" + strNameButton + "' id='btn" + strNameButton + "' onClick='" + strNameButton + "();'>&nbsp;\n";
		
		strBtnHabilitaDesabilita += "}\n";

		strFunction += strBtnHabilitaDesabilita;
		strFunction += "//-->\n</script>" + System.lineSeparator();
		
		return "";
	} 

}