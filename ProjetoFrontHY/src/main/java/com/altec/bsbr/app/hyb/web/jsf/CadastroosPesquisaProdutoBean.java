package com.altec.bsbr.app.hyb.web.jsf;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

//@Component("CadPesqProduto")
//@Scope("session")

@ManagedBean(name = "CadPesqProduto")
@ViewScoped
public class CadastroosPesquisaProdutoBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String strOs = "310";
	private String strTitular = "João Marcelo"; 
	private String strId = "123.456.789-10";
	
	public CadastroosPesquisaProdutoBean() {
		
	}
	
	
	
	public String getStrOs() {
		return strOs;
	}
	public void setStrOs(String strOs) {
		this.strOs = strOs;
	}
	public String getStrTitular() {
		return strTitular;
	}
	public void setStrTitular(String strTitular) {
		this.strTitular = strTitular;
	}
	public String getStrId() {
		return strId;
	}
	public void setStrId(String strId) {
		this.strId = strId;
	}
	

	
}
