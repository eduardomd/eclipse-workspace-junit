package com.altec.bsbr.app.hyb.web.jsf;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;



import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.swing.JOptionPane;

import com.altec.bsbr.app.hyb.dto.HyAdmGerManutEventosModel;


@ManagedBean(name="hyAdmGerManutEventosBean")
@ViewScoped
public class HyAdmGerManutEventosBean implements Serializable {
     
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    private Object strMsg = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strMsg");
    private String strErro;
    
    // private String strLink;
    // private int intRegistro;
    // private int intPagina;
    // private int intPaginaLinkCont;
    // private int intRestoTotalReg;
    
    private HyAdmGerManutEventosModel objAdmGer = new HyAdmGerManutEventosModel();
    private String[] objRsArea;
    private String[] objRsEvento;
    
    private String txtAddEvento;	
    private String txtHdEvento;
    private String txtHdParamAltEvento;
	private String txtHdParamAltRelac;
	private Boolean blnRet;

	@PostConstruct
    public void init() {
		this.objRsArea = objAdmGer.ConsultarArea();
		this.objRsEvento = objAdmGer.ConsultarEvento();
    }
	
	public HyAdmGerManutEventosBean() {
	}
	
	public boolean VerificaAreaEvento(int intCodArea, int intCodEvento) {
		
	    boolean objRsVerificaAreaEvento = objAdmGer.VerificarAreaEvento(intCodArea, intCodEvento, strErro);
	    // verifcaErro(strErro);
	    
	    if(objRsVerificaAreaEvento == true) {
	    	 return true;
	    }
	    
	    return false;
	}
	
	public void adicionar() {
		Map<String, String> requestParamMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		System.out.println("Adicionar: " + requestParamMap.get("frm:txtAddEvento"));
		//blnRet = objAdmGer.InserirEvento(requestParamMap.get("frm:txtAddEvento"));
		
		
		//if(blnRet) {
		//	JOptionPane.showMessageDialog(null, "alert", "alert", JOptionPane.ERROR_MESSAGE);
		//}
		

	}
	
	public void deletar() {
		Map<String, String> requestParamMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		System.out.println("Deletar: " + requestParamMap.get("frm:txtHdEvento"));
		//blnRet = objAdmGer.ApagarEvento(requestParamMap.get("frm:txtAddEvento"));
	}
	
	public void salvar() {
		Map<String, String> requestParamMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		System.out.println("Salvar: " + requestParamMap.get("frm:txtHdParamAltEvento"));
		String[] arrEvento = requestParamMap.get("frm:txtHdParamAltEvento").split("@");
		String[] arrEventoDetalhe;	    
	    for (int i = 0; i<=arrEvento.length; i++) {
	    	arrEventoDetalhe = arrEvento[i].split("|");
	    	//blnRet = objAdmGer.AlterarEvento(arrEventoDetalhe[0], arrEventoDetalhe[1]);
	    } 
			    
		System.out.println("Salvar: " + requestParamMap.get("frm:txtHdParamAltRelac"));
		String[] arrAreaEvento = requestParamMap.get("frm:txtHdParamAltRelac").split("@");
		String[] arrAreaEventoDetalhe;	
		for (int i = 0; i<=arrAreaEvento.length; i++) {
			arrAreaEventoDetalhe = arrAreaEvento[i].split("|");
	    	if(arrAreaEventoDetalhe[2] == "true") {
	    		//blnRet = objAdmGer.InserirAreaEvento(arrAreaEventoDetalhe[0], arrAreaEventoDetalhe[1]);
	    	}
	    	else{                  
                //blnRet = objAdmGer.ApagarAreaEvento(arrAreaEventoDetalhe[0], arrAreaEventoDetalhe[1]);
	    	}
	    } 
	}
	
	public int gerarId() {
		return (int) (Math.random() * 1000);
	}
	
	// getters e setters
	public String getStrMsg() {
		if(strMsg == null) {
			return "";
		}
		return strMsg.toString();
	}

	public void setStrMsg(String strMsg) {
		this.strMsg = strMsg;
	}
	
	public String[] getObjRsArea() {
		return objRsArea;
	}

	public void setObjRsArea(String[] objRsArea) {
		this.objRsArea = objRsArea;
	}
	
	public String[] getObjRsEvento() {
		return objRsEvento;
	}

	public void setObjRsEvento(String[] objRsEvento) {
		this.objRsEvento = objRsEvento;
	}    
	
	public String getTxtAddEvento() {
		return txtAddEvento;
	}

	public void setTxtAddEvento(String txtAddEvento) {
		this.txtAddEvento = txtAddEvento;
	}

	public String getTxtHdEvento() {
		return txtHdEvento;
	}

	public void setTxtHdEvento(String txtHdEvento) {
		this.txtHdEvento = txtHdEvento;
	}
	
	public String getTxtHdParamAltEvento() {
		return txtHdParamAltEvento;
	}

	public void setTxtHdParamAltEvento(String txtHdParamAltEvento) {
		this.txtHdParamAltEvento = txtHdParamAltEvento;
	}

	public String getTxtHdParamAltRelac() {
		return txtHdParamAltRelac;
	}

	public void setTxtHdParamAltRelac(String txtHdParamAltRelac) {
		this.txtHdParamAltRelac = txtHdParamAltRelac;
	}

	public Boolean getBlnRet() {
		return blnRet;
	}

	public void setBlnRet(Boolean blnRet) {
		this.blnRet = blnRet;
	}
}