package com.altec.bsbr.app.hyb.web.jsf;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.altec.bsbr.app.hyb.dto.hy_relosevento_model;

@ManagedBean(name="hy_relosevento")
@ViewScoped
public class hy_relosevento implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Object strArea = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("pArea");
	private Object pCodArea = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("pCodArea");
	private Object strTpRel = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("pTpRel");
	private Object strNmRelat = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("pNmRel");
	private Object strDtIni = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("pDtIni");
	private Object strDtFim = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("pDtFim");
	private Object intCanal = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("pCdCanal");
	
	private Date data = new Date(System.currentTimeMillis());
	
	//Simalando banco
	private banco objRelat = new banco(strDtIni, strDtFim);
	private List<hy_relosevento_model> objRsRelat = objRelat.fnRelEventos();
	
	public Object getStrArea() {
		return strArea;
	}
	public void setStrArea(Object strArea) {
		this.strArea = strArea;
	}
	public Object getpCodArea() {
		return pCodArea;
	}
	public void setpCodArea(Object pCodArea) {
		this.pCodArea = pCodArea;
	}
	public Object getStrTpRel() {
		return strTpRel;
	}
	public void setStrTpRel(Object strTpRel) {
		this.strTpRel = strTpRel;
	}
	public Object getStrNmRelat() {
		return strNmRelat;
	}
	public void setStrNmRelat(Object strNmRelat) {
		this.strNmRelat = strNmRelat;
	}
	public Object getStrDtIni() {
		return strDtIni;
	}
	public void setStrDtIni(Object strDtIni) {
		this.strDtIni = strDtIni;
	}
	public Object getStrDtFim() {
		return strDtFim;
	}
	public void setStrDtFim(Object strDtFim) {
		this.strDtFim = strDtFim;
	}
	public Object getIntCanal() {
		return intCanal;
	}
	public void setIntCanal(Object intCanal) {
		this.intCanal = intCanal;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
public List<hy_relosevento_model> getObjRsRelat() {
		return objRsRelat;
	}
	public void setObjRsRelat(List<hy_relosevento_model> objRsRelat) {
		this.objRsRelat = objRsRelat;
	}

	
	
public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}



class banco{
	
		private List<hy_relosevento_model> objRsRelat = new ArrayList<hy_relosevento_model>();
		
		public banco(Object strDtIni,Object strDtFim) {
			hy_relosevento_model w = new hy_relosevento_model("123", "A", "Encerrada", "4", "Ok", "Evento x", "Canal x", "ABC", "12/08");
			this.objRsRelat.add(w);
			hy_relosevento_model x = new hy_relosevento_model("456", "B", "Andamento", "3", "N�o Ok", "Evento w", "Canal x", "DEF", "17/08");
			this.objRsRelat.add(x);
			hy_relosevento_model y = new hy_relosevento_model("789", "C", "Encerrada", "4", "Ok", "Evento x", "Canal y", "GHI", "02/10");
			this.objRsRelat.add(y);
			hy_relosevento_model z = new hy_relosevento_model("536", "D", "Parada", "2", "N�o Ok", "Evento x", "Canal z", "JKL", "22/03");
			this.objRsRelat.add(z);
		}
		
		public List<hy_relosevento_model> fnRelEventos() {
			
			return this.objRsRelat;
			
		}

		public List<hy_relosevento_model> getObjRsRelat() {
			return objRsRelat;
		}

		public void setObjRsRelat(List<hy_relosevento_model> objRsRelat) {
			this.objRsRelat = objRsRelat;
		}
		
	}
	
}

