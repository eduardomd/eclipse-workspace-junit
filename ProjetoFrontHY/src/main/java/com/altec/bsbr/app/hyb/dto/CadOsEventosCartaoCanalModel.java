package com.altec.bsbr.app.hyb.dto;

public class CadOsEventosCartaoCanalModel {

	private String nrSequCrto; 
	private String nrCrto;
	private String nrSequParpProc;		
	private String nrSequFrauCnal;
	private String nrSequCntaBncr;

	public String getNrSequCrto() {
		return nrSequCrto;
	}
	public void setNrSequCrto(String nrSequCrto) {
		this.nrSequCrto = nrSequCrto;
	}

	public String getNrCrto() {
		return nrCrto;
	}
	public void setNrCrto(String nrCrto) {
		this.nrCrto = nrCrto;
	}

	public String getNrSequParpProc() {
		return nrSequParpProc;
	}
	public void setNrSequParpProc(String nrSequParpProc) {
		this.nrSequParpProc = nrSequParpProc;
	}

	public String getNrSequFrauCnal() {
		return nrSequFrauCnal;
	}
	public void setNrSequFrauCnal(String nrSequFrauCnal) {
		this.nrSequFrauCnal = nrSequFrauCnal;
	}

	public String getNrSequCntaBncr() {
		return nrSequCntaBncr;
	}
	public void setNrSequCntaBncr(String nrSequCntaBncr) {
		this.nrSequCntaBncr = nrSequCntaBncr;
	}
}
