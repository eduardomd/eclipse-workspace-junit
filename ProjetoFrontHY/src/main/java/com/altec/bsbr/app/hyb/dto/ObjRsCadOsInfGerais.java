package com.altec.bsbr.app.hyb.dto;

public class ObjRsCadOsInfGerais {
	
	private String txAberOrdeServ;
	private String txEnceOrdeServ;
	private String cdNoti;
	
	public ObjRsCadOsInfGerais() {
		
	}
	
	public ObjRsCadOsInfGerais(String txAberOrdeServ, String txEnceOrdeServ, String cdNoti) {
		this.txAberOrdeServ = txAberOrdeServ;
		this.txEnceOrdeServ = txEnceOrdeServ;
		this.cdNoti = cdNoti;
	}
	
	public String getTxAberOrdeServ() {
		return txAberOrdeServ;
	}
	
	public void setTxAberOrdeServ(String txAberOrdeServ) {
		this.txAberOrdeServ = txAberOrdeServ;
	}
	
	public String getTxEnceOrdeServ() {
		return txEnceOrdeServ;
	}
	
	public void setTxEnceOrdeServ(String txEnceOrdeServ) {
		this.txEnceOrdeServ = txEnceOrdeServ;
	}
	
	public String getCdNoti() {
		return cdNoti;
	}
	
	public void setCdNoti(String cdNoti) {
		this.cdNoti = cdNoti;
	}

	
}
