
package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.AdmWorkflowNotificacaoParaModel;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmNotificacao.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmNotificacao.XHYAdmNotificacaoEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;


@Component("AdmWorkflowNotificacaoPara")
@Scope("request")
public class AdmWorkflowNotificacaoParaBean extends BasicBBean {

	private Object strMsg = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strMsg");
	private String txtHdAction;
	private String hdnNotificacao;
	
	private List<AdmWorkflowNotificacaoParaModel> objRsAdmNoti;
	
	@Autowired
	private XHYAdmNotificacaoEndPoint admnoti;
	
	@PostConstruct
	public void init() {		
		setupObjRsAdmNoti();		
	}
	
	public void setupObjRsAdmNoti() {
		this.objRsAdmNoti = new ArrayList<AdmWorkflowNotificacaoParaModel>();
		try {
			String retorno = admnoti.consultarNotificacao();
            System.out.println("consultarNotificacao retorno!!:  " + retorno);
            
            JSONObject jsonRetorno = new JSONObject(retorno);
            JSONArray pcursor = jsonRetorno.getJSONArray("PCURSOR");
            
            for(int i = 0; i < pcursor.length(); i++) {
            	JSONObject curr = pcursor.getJSONObject(i);
    			AdmWorkflowNotificacaoParaModel notificParaModel = new AdmWorkflowNotificacaoParaModel();
    			notificParaModel.setNR_NOTI(String.valueOf(curr.getInt("NR_NOTI")));
    			notificParaModel.setNM_NOTI(curr.getString("NM_NOTI"));
    			notificParaModel.setQT_HORA_PRAZ_RELZ(String.valueOf(curr.getInt("QT_HORA_PRAZ_RELZ")));
    			notificParaModel.setQT_HORA_PERI_REEV(String.valueOf(curr.getInt("QT_HORA_PERI_REEV")));
    			notificParaModel.setTP_NOTI(curr.getString("TP_NOTI")); // M -> Disabled
    			notificParaModel.setIN_REEV(String.valueOf(curr.getInt("IN_REEV")));
    			notificParaModel.setInReevChk(String.valueOf(curr.getInt("IN_REEV")).equals("1"));
    			this.objRsAdmNoti.add(notificParaModel);
            }

	    } catch (Exception e) {
	    	try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
        }
	}

	public void exeRemoteCommand() {	
		String hdnNotificacao = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("hdnNotificacaoValue").toString();
		setActionNoti(hdnNotificacao);
		System.out.println("this.hdnNotificacao "+this.hdnNotificacao);
		
		//TODO Gravar Log com cpf do usuário
		// InsereLog SALVAR_CONFIG_NOTIFICACAO_AUTOMATICA, CPF_USUARIO, "", ""
		
		try {
			admnoti.arrayAlterarNotificacao(this.hdnNotificacao);
			setupObjRsAdmNoti();
			RequestContext.getCurrentInstance().execute("alert('Alterações efetuadas com sucesso!')");			
		} catch (Exception e) {
	    	try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	
	}
	
	public void setActionNoti(String hdnNotificacao) {
		this.setTxtHdAction("A");

		
		this.setHdnNotificacao(hdnNotificacao);
		System.out.println("hdnNoti bean");
		System.out.println(this.hdnNotificacao);
		System.out.println(hdnNotificacao);
		
	}
	
	public List<AdmWorkflowNotificacaoParaModel> getObjRsAdmNoti() {
		return objRsAdmNoti;
	}

	public void setObjRsAdmNoti(List<AdmWorkflowNotificacaoParaModel> objRsAdmNoti) {
		this.objRsAdmNoti = objRsAdmNoti;
	}

	public Object getstrMsg() {
		return strMsg;
	}
	
	public void setstrMsg(String strMsg) {
		this.strMsg = strMsg;
	}

	public String getTxtHdAction() {
		return txtHdAction;
	}

	public void setTxtHdAction(String txtHdAction) {
		this.txtHdAction = txtHdAction;
	}
	
	public String getHdnNotificacao() {
		return hdnNotificacao;
	}

	public void setHdnNotificacao(String hdnNotificacao) {
		this.hdnNotificacao = hdnNotificacao;
	}
	
}
