package com.altec.bsbr.app.hyb.dto;

import java.util.Date;

public class CadOsAcoesComplModel {

	/*
	 * variables used in the two beans: CadOSAcoesComplBean and
	 * CadOsAcoesComplActionBean
	 */
	private String strNrSeqOs;
	private String strMsg;
	private String strErro;

	// variables used in the bean: CadOsAcoesComplBean
	private String strTitulo;

	// variables used in the bean: CadOsAcoesComplAction
	private String intNrBo; // NR_BOLE_OCOR
	private String intInProcJuri; // IN_PROC_JURI //0 ou 1
	private String strNmDelegacia; // NM_DELC
	private String strNmForum; // NM_FORU_PROC_JURI
	private String strCpfUsuario;
	private String dteRegistro; // DT_REGT_BOLE_OCOR

	public CadOsAcoesComplModel() {

	}

	public CadOsAcoesComplModel(String strNrSeqOs, String intNrBo, String strNmDelegacia, String dteRegistro,
			String intInProcJuri, String strNmForum, String strErro) {
		this.strNrSeqOs = strNrSeqOs;
		this.intNrBo = intNrBo;
		this.strNmDelegacia = strNmDelegacia;
		this.dteRegistro = dteRegistro;
		this.intInProcJuri = intInProcJuri;
		this.strNmForum = strNmForum;
		this.strErro = strErro;
	}

	public String getStrNrSeqOs() {
		return strNrSeqOs;
	}

	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}

	public String getStrMsg() {
		return strMsg;
	}

	public void setStrMsg(String strMsg) {
		this.strMsg = strMsg;
	}

	public String getStrErro() {
		return strErro;
	}

	public void setStrErro(String strErro) {
		this.strErro = strErro;
	}

	public String getStrTitulo() {
		return strTitulo;
	}

	public void setStrTitulo(String strTitulo) {
		this.strTitulo = strTitulo;
	}

	public String getIntNrBo() {
		return intNrBo;
	}

	public void setIntNrBo(String intNrBo) {
		this.intNrBo = intNrBo;
	}

	public String getIntInProcJuri() {
		return intInProcJuri;
	}

	public void setIntInProcJuri(String intInProcJuri) {
		this.intInProcJuri = intInProcJuri;
	}

	public String getStrNmDelegacia() {
		return strNmDelegacia;
	}

	public void setStrNmDelegacia(String strNmDelegacia) {
		this.strNmDelegacia = strNmDelegacia;
	}

	public String getStrNmForum() {
		return strNmForum;
	}

	public void setStrNmForum(String strNmForum) {
		this.strNmForum = strNmForum;
	}

	public String getStrCpfUsuario() {
		return strCpfUsuario;
	}

	public void setStrCpfUsuario(String strCpfUsuario) {
		this.strCpfUsuario = strCpfUsuario;
	}

	public String getDteRegistro() {
		return dteRegistro;
	}

	public void setDteRegistro(String dteRegistro) {
		this.dteRegistro = dteRegistro;
	}


}
