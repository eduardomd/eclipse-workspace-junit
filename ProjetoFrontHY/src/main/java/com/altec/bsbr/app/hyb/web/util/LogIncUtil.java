package com.altec.bsbr.app.hyb.web.util;

public class LogIncUtil {

	/* CONSTANTES PARA OS CODIGOS DOS LOGS */

	public static final int TESTE = 1;

	// Administração Gerencial
	public static final int ADICIONAR_CANAL_ORIGEM = 1001;
	public static final int SALVAR_CANAL_ORIGEM = 1002;
	public static final int DELETAR_CANAL_ORIGEM = 1003;
	public static final int ADICIONAR_PENALIDADE = 1004;
	public static final int SALVAR_PENALIDADE = 1005;
	public static final int DELETAR_PENALIDADE = 1006;
	public static final int ADICIONAR_MORIVO_PENALIDADE = 1007;
	public static final int SALVAR_MOTIVO_PENALIDADE = 1008;
	public static final int DELETAR_MOTIVO_PENALIDADE = 1009;
	public static final int ADICIONAR_EVENTO = 1010;
	public static final int SALVAR_EVENTO = 1011;
	public static final int DELETAR_EVENTO = 1012;
	public static final int ADICIONAR_CANAL = 1013;
	public static final int SALVAR_CANAL = 1014;
	public static final int DELETAR_CANAL = 1015;
	public static final int SALVAR_PROD_CORP = 1016;
	public static final int ADICIONAR_PROD_AUX = 1017;
	public static final int SALVAR_PROD_AUX = 1018;
	public static final int DELETAR_PROD_AUX = 1019;
	public static final int CADASTRAR_RECURSO = 1020;
	public static final int DELETAR_RECURSO = 1021;
	public static final int ALTERAR_DADOS_RECURSO = 1022;

	// ORDEM DE SERVI�O

	public static final int CRIAR_NOVA_OS = 2001;
	public static final int SALVAR_DADOS_INICIAIS_OS = 2002;
	public static final int SALVAR_DATAS_OS = 2003;
	public static final int SALVAR_CANAL_ORIGEM_OS = 2004;
	public static final int SALVAR_UNIDADE_ENVOLVIDA_OS = 2005;
	public static final int ADICIONAR_PARTICIPANTE_OS = 2006;
	public static final int DELETAR_PARTICIPANTE_OS = 2007;
	public static final int SALVAR_DETALHES_PARTICIPANTE_OS = 2008;
	public static final int SALVAR_EVENTO_OS = 2009;
	public static final int ADICIONAR_CANAL_OS = 2010;
	public static final int DELETAR_PARTICIPANTE_CANAL_OS = 2011;
	public static final int SALVAR_DETALHES_CANAL_OS = 2012;
	public static final int SALVAR_RISCO_POTENCIAL_OS = 2013;
	public static final int SALVAR_LANCAMENTO_ANALITICO_OS = 2014;
	public static final int SALVAR_LANCAMENTO_TOTAL_OS = 2015;
	public static final int SALVAR_DADOS_CONTABEIS_OS = 2016;
	public static final int SALVAR_EQUIPE_ATRIBUIDA_OS = 2017;
	public static final int DELETAR_EQUIPE_ATRIBUIDA_OS = 2018;
	public static final int LANCAR_HORAS_TRABALHADAS_OS = 2019;
	public static final int SALVAR_INFORMACOES_GERAIS_OS = 2020;
	public static final int SALVAR_ACOES_COMPLEMENTARES_OS = 2021;

	/* ARQUIVO RO */
	public static final int GERAR_ARQUIVO_RO = 3001;

	// Notificacoes workflow)
	public static final int SALVAR_CONFIG_NOTIFICACAO_AUTOMATICA = 4001;
	public static final int ENVIAR_NOTIFICACAO_MANUAL = 4002;
	public static final int REENVIAR_NOTIFICACAO_MANUAL = 4003;

	// Treinamento
	public static final int CADASTRAR_TREINAMENTO = 5001;
	public static final int ALTERAR_TREINAMENTO = 5002;
	public static final int DELETAR_TREINAMENTO = 5003;

	// Relatorios
	public static final int GERAR_RELATORIO = 6001;

	// Pesquisa OS - Acoes
	public static final int ALTERAR_OS = 7001;
	public static final int EXCLUIR_OS = 7002;
	public static final int APROVAR_OS = 7003;
	public static final int REPROVAR_OS = 7004;
	public static final int APROVAR_RELATORIO_PROPOSICAO_OS = 7005;
	public static final int REPROVAR_RELATORIO_PROPOSICAO_OS = 7006;
	public static final int IMPRIMIR_RELATORIO_PROPOSICAO_OS = 7007;
	public static final int APROVAR_DADOS_CONTABEIS_OS = 7008;
	public static final int REPROVAR_DADOS_CONTABEIS_OS = 7009;
	public static final int ENCERRAR_OS = 7010;
	public static final int IMPRIMIR_ENCERRAR_OS = 7011;
	public static final int EXPORTAR_PESQUISA_EXCEL_OS = 7012;
	public static final int VISUALIZAR_DETALHES_OS = 7013;

	public static final int ACESSO_NEGADO = 7014;

}
