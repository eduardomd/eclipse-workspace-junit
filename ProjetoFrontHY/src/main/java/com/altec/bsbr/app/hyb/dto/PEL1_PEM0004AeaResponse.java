package com.altec.bsbr.app.hyb.dto;

public class PEL1_PEM0004AeaResponse {
	
	public PEL1_PEM0004AeaResponse() {}

	public PEL1_PEM0004AeaResponse(String dC_FORMATO, String pENUMPE, String tIPDOC, String pENUMDO, String pETIPPE,
			String pENOMPE, String pEPRIAP, String pESEGAP, String pECONPE) {
		super();
		DC_FORMATO = dC_FORMATO;
		PENUMPE = pENUMPE;
		TIPDOC = tIPDOC;
		PENUMDO = pENUMDO;
		PETIPPE = pETIPPE;
		PENOMPE = pENOMPE;
		PEPRIAP = pEPRIAP;
		PESEGAP = pESEGAP;
		PECONPE = pECONPE;
	}

	private String DC_FORMATO;
	
	private String PENUMPE;
	
	private String TIPDOC;
	
	private String PENUMDO;
	
	private String PETIPPE;
	
	private String PENOMPE;
	
	private String PEPRIAP;
	
	private String PESEGAP;
	
	private String PECONPE;

	public String getDC_FORMATO() {
		return DC_FORMATO;
	}

	public void setDC_FORMATO(String dC_FORMATO) {
		DC_FORMATO = dC_FORMATO;
	}

	public String getPENUMPE() {
		return PENUMPE;
	}

	public void setPENUMPE(String pENUMPE) {
		PENUMPE = pENUMPE;
	}

	public String getTIPDOC() {
		return TIPDOC;
	}

	public void setTIPDOC(String tIPDOC) {
		TIPDOC = tIPDOC;
	}

	public String getPENUMDO() {
		return PENUMDO;
	}

	public void setPENUMDO(String pENUMDO) {
		PENUMDO = pENUMDO;
	}

	public String getPETIPPE() {
		return PETIPPE;
	}

	public void setPETIPPE(String pETIPPE) {
		PETIPPE = pETIPPE;
	}

	public String getPENOMPE() {
		return PENOMPE;
	}

	public void setPENOMPE(String pENOMPE) {
		PENOMPE = pENOMPE;
	}

	public String getPEPRIAP() {
		return PEPRIAP;
	}

	public void setPEPRIAP(String pEPRIAP) {
		PEPRIAP = pEPRIAP;
	}

	public String getPESEGAP() {
		return PESEGAP;
	}

	public void setPESEGAP(String pESEGAP) {
		PESEGAP = pESEGAP;
	}

	public String getPECONPE() {
		return PECONPE;
	}

	public void setPECONPE(String pECONPE) {
		PECONPE = pECONPE;
	}

}
