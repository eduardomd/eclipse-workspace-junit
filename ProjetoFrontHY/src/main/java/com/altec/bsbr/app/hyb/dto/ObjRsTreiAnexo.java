package com.altec.bsbr.app.hyb.dto;

import java.io.File;

public class ObjRsTreiAnexo {
	
	private String codigo;
	private String nmCamiAnex;
	
	public ObjRsTreiAnexo() {
		
	}

	public ObjRsTreiAnexo(String codigo, String nmCamiAnex) {
		this.codigo = codigo;
		this.nmCamiAnex = nmCamiAnex;
	}
	
	public String getCodigo() {
		return codigo;
	}
	
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	public String getNmCamiAnex() {
		return nmCamiAnex;
	}
	
	public void setNmCamiAnex(String nmCamiAnex) {
		this.nmCamiAnex = nmCamiAnex;
	}
	
	public String getFileName() {
		return new File(this.nmCamiAnex).getName().toString();
	}

	
}
