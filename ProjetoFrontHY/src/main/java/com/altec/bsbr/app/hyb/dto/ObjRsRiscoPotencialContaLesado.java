package com.altec.bsbr.app.hyb.dto;

public class ObjRsRiscoPotencialContaLesado {

	private String NM_PARP;
	private String CD_BANC;
	private String NR_CNTA;
	
	public ObjRsRiscoPotencialContaLesado(String nM_PARP, String cD_BANC, String nR_CNTA) {
		NM_PARP = nM_PARP;
		CD_BANC = cD_BANC;
		NR_CNTA = nR_CNTA;
	}

	public ObjRsRiscoPotencialContaLesado() {}
	
	public String getNM_PARP() {
		return NM_PARP;
	}
	public void setNM_PARP(String nM_PARP) {
		NM_PARP = nM_PARP;
	}
	public String getCD_BANC() {
		return CD_BANC;
	}
	public void setCD_BANC(String cD_BANC) {
		CD_BANC = cD_BANC;
	}
	public String getNR_CNTA() {
		return NR_CNTA;
	}
	public void setNR_CNTA(String nR_CNTA) {
		NR_CNTA = nR_CNTA;
	}
	
}
