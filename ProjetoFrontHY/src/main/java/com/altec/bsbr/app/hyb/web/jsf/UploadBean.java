package com.altec.bsbr.app.hyb.web.jsf;

import java.io.Serializable;

import javax.faces.context.FacesContext;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


@Component("UploadBean")
@Scope("request")
public class UploadBean implements Serializable {
     
	private static final long serialVersionUID = 1L;
	
	/* Query String */
	private Object strNrSeqOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs");
	private Object strTpAnexo = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strTpAnexo");
	private Object strSeqTrei = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strSeqTrei");
	
	
	public Object getStrNrSeqOs() {
		return strNrSeqOs;
	}
	public void setStrNrSeqOs(Object strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}
	public Object getStrTpAnexo() {
		return strTpAnexo;
	}
	public void setStrTpAnexo(Object strTpAnexo) {
		this.strTpAnexo = strTpAnexo;
	}
	public Object getStrSeqTrei() {
		return strSeqTrei;
	}
	public void setStrSeqTrei(Object strSeqTrei) {
		this.strSeqTrei = strSeqTrei;
	}
	
}