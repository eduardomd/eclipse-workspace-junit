package com.altec.bsbr.app.hyb.web.jsf;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.altec.bsbr.app.hyb.dto.Hyb_cados_detalheriscopot_t_objRsProdAux;
import com.altec.bsbr.app.hyb.dto.Hyb_cados_detalheriscopot_t_objRsProdCorp;

@ManagedBean(name="hyb_cados_detalheriscopot_t_bean")
@ViewScoped
public class Hyb_cados_detalheriscopot_t_bean {
	
	private Object strTitulo = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strTitulo");
	private Object strNrOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs");
	// Quantidade de linhas Corp
	private int intRecCountCorp;
	// Quantidade de linhas Aux
	private int intRecCountAuxi;
	private String strIN_RESS;
	private List<Hyb_cados_detalheriscopot_t_objRsProdCorp> objRsProdCorp;
	private List<Hyb_cados_detalheriscopot_t_objRsProdAux> objRsProdAux;
	
	public Hyb_cados_detalheriscopot_t_bean() {
		objRsProdCorp = new ArrayList<Hyb_cados_detalheriscopot_t_objRsProdCorp>();
		objRsProdAux = new ArrayList<Hyb_cados_detalheriscopot_t_objRsProdAux>();
		
		for(int i = 0; i < 3; i++) {
			Hyb_cados_detalheriscopot_t_objRsProdCorp prodCorpModel = new Hyb_cados_detalheriscopot_t_objRsProdCorp();
			Hyb_cados_detalheriscopot_t_objRsProdAux prodAuxModel = new Hyb_cados_detalheriscopot_t_objRsProdAux();
			prodCorpModel.setDT_EXCL_TOTL("17/01/2019");
			prodCorpModel.setDT_RECR_FORA_MES_TOTL("17/01/2019");
			prodCorpModel.setDT_RECR_MES_TOTL("17/01/2019");
			prodCorpModel.setIN_RESS(null);
			prodCorpModel.setNM_OPER("test"+i);
			prodCorpModel.setNM_OPER("test"+i);
			prodCorpModel.setNR_SEQU_TRAN_FRAU_CNAL("test"+i);
			prodCorpModel.setVL_EXCL_TOTL("test"+i);
			prodCorpModel.setVL_RECR_FORA_MES_TOTL("test"+i);
			prodCorpModel.setVL_RECR_MES_TOTL("test"+i);
			objRsProdCorp.add(prodCorpModel);
			
			prodAuxModel.setDT_EXCL_TOTL("17/01/2019");
			prodAuxModel.setDT_RECR_FORA_MES_TOTL("17/01/2019");
			prodAuxModel.setDT_RECR_MES_TOTL("17/01/2019");
			prodAuxModel.setIN_RESS(null);
			prodAuxModel.setNM_PROD_AUXI("test"+i);
			prodAuxModel.setNR_SEQU_TRAN_FRAU_CNAL("test"+i);
			prodAuxModel.setVL_EXCL_TOTL("test"+i);
			prodAuxModel.setVL_RECR_FORA_MES_TOTL("test"+i);
			prodAuxModel.setVL_RECR_MES_TOTL("test"+i);
			objRsProdAux.add(prodAuxModel);
			
			this.setStrIN_RESS(prodCorpModel.getIN_RESS());
			this.setStrIN_RESS(prodAuxModel.getIN_RESS());
			
		}
		
	}

	public String getStrIN_RESS() {
		return strIN_RESS;
	}

	public void setStrIN_RESS(String strIN_RESS) {
		this.strIN_RESS = strIN_RESS;
	}

	public Object getStrTitulo() {
		return strTitulo;
	}

	public void setStrTitulo(Object strTitulo) {
		this.strTitulo = strTitulo;
	}

	public Object getStrNrOs() {
		return strNrOs;
	}

	public void setStrNrOs(Object strNrOs) {
		this.strNrOs = strNrOs;
	}

	public int getIntRecCountCorp() {
		return intRecCountCorp;
	}

	public void setIntRecCountCorp(int intRecCountCorp) {
		this.intRecCountCorp = intRecCountCorp;
	}

	public int getIntRecCountAuxi() {
		return intRecCountAuxi;
	}

	public void setIntRecCountAuxi(int intRecCountAuxi) {
		this.intRecCountAuxi = intRecCountAuxi;
	}

	public List<Hyb_cados_detalheriscopot_t_objRsProdCorp> getObjRsProdCorp() {
		return objRsProdCorp;
	}

	public void setObjRsProdCorp(List<Hyb_cados_detalheriscopot_t_objRsProdCorp> objRsProdCorp) {
		this.objRsProdCorp = objRsProdCorp;
	}

	public List<Hyb_cados_detalheriscopot_t_objRsProdAux> getObjRsProdAux() {
		return objRsProdAux;
	}

	public void setObjRsProdAux(List<Hyb_cados_detalheriscopot_t_objRsProdAux> objRsProdAux) {
		this.objRsProdAux = objRsProdAux;
	}
	
	
}
