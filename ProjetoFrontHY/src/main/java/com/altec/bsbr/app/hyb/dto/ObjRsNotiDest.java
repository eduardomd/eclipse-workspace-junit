package com.altec.bsbr.app.hyb.dto;

public class ObjRsNotiDest {

	private String NM_EMAIL_DEST;
	
	public ObjRsNotiDest() {}

	public ObjRsNotiDest(String nM_EMAIL_DEST) {
		NM_EMAIL_DEST = nM_EMAIL_DEST;
	}
	
	public String getNM_EMAIL_DEST() {
		return NM_EMAIL_DEST;
	}

	public void setNM_EMAIL_DEST(String nM_EMAIL_DEST) {
		NM_EMAIL_DEST = nM_EMAIL_DEST;
	}

}
