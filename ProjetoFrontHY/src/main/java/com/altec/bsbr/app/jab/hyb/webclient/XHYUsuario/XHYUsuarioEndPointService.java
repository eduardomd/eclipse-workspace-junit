//
// Generated By:JAX-WS RI 2.2.9-b130926.1035 (JAXB RI IBM 2.2.8-b130911.1802)
//


package com.altec.bsbr.app.jab.hyb.webclient.XHYUsuario;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;

@WebServiceClient(name = "xHY_UsuarioEndPointService", targetNamespace = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/", wsdlLocation = "http://d2240769.bs.br.bsch:9080/JAB-HYBSRVOESP/xHY_UsuarioEndPointService/XHYUsuarioEndPointService.wsdl")
public class XHYUsuarioEndPointService
    extends Service
{

    private final static URL XHYUSUARIOENDPOINTSERVICE_WSDL_LOCATION;
    private final static WebServiceException XHYUSUARIOENDPOINTSERVICE_EXCEPTION;
    private final static QName XHYUSUARIOENDPOINTSERVICE_QNAME = new QName("http://webservice.srv001.hyb.jab.app.bsbr.altec.com/", "xHY_UsuarioEndPointService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://d2240769.bs.br.bsch:9080/JAB-HYBSRVOESP/xHY_UsuarioEndPointService/XHYUsuarioEndPointService.wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        XHYUSUARIOENDPOINTSERVICE_WSDL_LOCATION = url;
        XHYUSUARIOENDPOINTSERVICE_EXCEPTION = e;
    }

    public XHYUsuarioEndPointService() {
        super(__getWsdlLocation(), XHYUSUARIOENDPOINTSERVICE_QNAME);
    }

    public XHYUsuarioEndPointService(WebServiceFeature... features) {
        super(__getWsdlLocation(), XHYUSUARIOENDPOINTSERVICE_QNAME, features);
    }

    public XHYUsuarioEndPointService(URL wsdlLocation) {
        super(wsdlLocation, XHYUSUARIOENDPOINTSERVICE_QNAME);
    }

    public XHYUsuarioEndPointService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, XHYUSUARIOENDPOINTSERVICE_QNAME, features);
    }

    public XHYUsuarioEndPointService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public XHYUsuarioEndPointService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns XHYUsuarioEndPoint
     */
    @WebEndpoint(name = "xHY_UsuarioEndPointPort")
    public XHYUsuarioEndPoint getXHYUsuarioEndPointPort() {
        return super.getPort(new QName("http://webservice.srv001.hyb.jab.app.bsbr.altec.com/", "xHY_UsuarioEndPointPort"), XHYUsuarioEndPoint.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns XHYUsuarioEndPoint
     */
    @WebEndpoint(name = "xHY_UsuarioEndPointPort")
    public XHYUsuarioEndPoint getXHYUsuarioEndPointPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://webservice.srv001.hyb.jab.app.bsbr.altec.com/", "xHY_UsuarioEndPointPort"), XHYUsuarioEndPoint.class, features);
    }

    private static URL __getWsdlLocation() {
        if (XHYUSUARIOENDPOINTSERVICE_EXCEPTION!= null) {
            throw XHYUSUARIOENDPOINTSERVICE_EXCEPTION;
        }
        return XHYUSUARIOENDPOINTSERVICE_WSDL_LOCATION;
    }

}
