package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.fw.web.jsf.BasicBBean;


@Component("histPesquisa")
@Scope("session")

public class HistPesquisaBean extends BasicBBean {
 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String codigoEvento;
	private String codigoAgencia;
	private String tituloAbertuda; 
	
	@PostConstruct
	public void init()
	{
		System.out.println("Cheguei  ");
	}
	
	public void pesquisarHistorico() {
		
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		try {
			getCodigoAgencia();
			getCodigoEvento();
			getTituloAbertuda();  
			ec.redirect("hyb_hist_result_pesq.xhtml?strCodEven="+getCodigoEvento()+"&strCodAgen="+getCodigoAgencia()+"&strDsTitu="+getTituloAbertuda());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public String getTituloAbertuda() {
		return tituloAbertuda;
	}

	public void setTituloAbertuda(String tituloAbertuda) {
		this.tituloAbertuda = tituloAbertuda;
	}

	public String getCodigoEvento() {
		return codigoEvento;
	}
	public void setCodigoEvento(String codigoEvento) {
		this.codigoEvento = codigoEvento;
	}
	public String getCodigoAgencia() {
		return codigoAgencia;
	}
	public void setCodigoAgencia(String codigoAgencia) {
		this.codigoAgencia = codigoAgencia;
	}	
	
	
	
}
