package com.altec.bsbr.app.hyb.web.jsf;

import java.io.Serializable;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;



@Component("AdmWorkflowNotificacaoPesqEmailBean")
@Scope("session")
public class AdmWorkflowNotificacaoPesqEmailBean implements Serializable {
    
	private static final long serialVersionUID = 1L;
    
	private String txtNmFunc;
	private String txtNmEmail;
		
	public String getTxtNmFunc() {
		return txtNmFunc;
	}
	public void setTxtNmFunc(String txtNmFunc) {
		this.txtNmFunc = txtNmFunc;
	}
	public String getTxtNmEmail() {
		return txtNmEmail;
	}
	public void setTxtNmEmail(String txtNmEmail) {
		this.txtNmEmail = txtNmEmail;
	}
	
	
}