package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServlet;

@ManagedBean(name = "cadastroOsDetalheDnvolvnaCclienteBean")
@ViewScoped
public class Hy_CadastroOs_DetalheDnvolvnaCclienteBean extends HttpServlet implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String strId;
	private String strMatricula;
	private String strParticipacao;
	private String strgNrDocto;
	private String strgNrSeqPart;
	private String strgNrSeqOs;
	private String strTpPart;
	private String strEndereco;
	private String strNumero;
	private String strComplemento;
	private String strBairro;
	private String strCEP;
	private String strCidade;
	private String strEstado;
	private String strTelefone;
	private String strCelular;
	private String strTipo;
	private String strDoctoPart;
	private String strNomepart;
	private String strOSTitulo;
	
	//--------------
	
	private String strOS;
	private String strNome;
	private String strCPF_CNPJ;
	private String strRG;
	private String strOrgEmissor;
	private String strDtEmissaoRG;
	private String strProfissao;
	private String strRenda;
	private String strEnderecoApur;
	private String strNumeroApur;
	private String strComplApur;
	private String strBairroApur;
	private String strCEPApur;
	private String strTelefoneApur;
	private String strCelularApur;
	private String strDTAbertura;
	private String strOPER_VENCER;
	private String strOPER_VENCIDA;
	private String strOPER_CRELI;
	private String strOPER_PREJU;
	private String strOPER_PASSIVA;
	private String strRECIPROCIDADE;
	
	//--------------
	
	/*dim objParticip, objRs
	dim objRestParticip, objRsRest*/
	private String strSelected1;
	private String strSelected2;
	private String strSelected3;
	private String strSelected4;
	private int intRecCount;
	private int iCountLinhas;
	private String strErro;
	private int intTpParticipacao;
	
	public int getIntTpParticipacao() {
		return intTpParticipacao;
	}
	public void setIntTpParticipacao(int intTpParticipacao) {
		this.intTpParticipacao = intTpParticipacao;
	}
	public String getStrId() {
		return strId;
	}
	public void setStrId(String strId) {
		this.strId = strId;
	}
	public String getStrMatricula() {
		return strMatricula;
	}
	public void setStrMatricula(String strMatricula) {
		this.strMatricula = strMatricula;
	}
	public String getStrParticipacao() {
		return strParticipacao;
	}
	public void setStrParticipacao(String strParticipacao) {
		this.strParticipacao = strParticipacao;
	}
	public String getStrgNrDocto() {
		return strgNrDocto;
	}
	public void setStrgNrDocto(String strgNrDocto) {
		this.strgNrDocto = strgNrDocto;
	}
	public String getStrgNrSeqPart() {
		return strgNrSeqPart;
	}
	public void setStrgNrSeqPart(String strgNrSeqPart) {
		this.strgNrSeqPart = strgNrSeqPart;
	}
	public String getStrgNrSeqOs() {
		return strgNrSeqOs;
	}
	public void setStrgNrSeqOs(String strgNrSeqOs) {
		this.strgNrSeqOs = strgNrSeqOs;
	}
	public String getStrTpPart() {
		return strTpPart;
	}
	public void setStrTpPart(String strTpPart) {
		this.strTpPart = strTpPart;
	}
	public String getStrEndereco() {
		return strEndereco;
	}
	public void setStrEndereco(String strEndereco) {
		this.strEndereco = strEndereco;
	}
	public String getStrNumero() {
		return strNumero;
	}
	public void setStrNumero(String strNumero) {
		this.strNumero = strNumero;
	}
	public String getStrComplemento() {
		return strComplemento;
	}
	public void setStrComplemento(String strComplemento) {
		this.strComplemento = strComplemento;
	}
	public String getStrBairro() {
		return strBairro;
	}
	public void setStrBairro(String strBairro) {
		this.strBairro = strBairro;
	}
	public String getStrCEP() {
		return strCEP;
	}
	public void setStrCEP(String strCEP) {
		this.strCEP = strCEP;
	}
	public String getStrCidade() {
		return strCidade;
	}
	public void setStrCidade(String strCidade) {
		this.strCidade = strCidade;
	}
	public String getStrEstado() {
		return strEstado;
	}
	public void setStrEstado(String strEstado) {
		this.strEstado = strEstado;
	}
	public String getStrTelefone() {
		return strTelefone;
	}
	public void setStrTelefone(String strTelefone) {
		this.strTelefone = strTelefone;
	}
	public String getStrCelular() {
		return strCelular;
	}
	public void setStrCelular(String strCelular) {
		this.strCelular = strCelular;
	}
	public String getStrTipo() {
		return strTipo;
	}
	public void setStrTipo(String strTipo) {
		this.strTipo = strTipo;
	}
	public String getStrDoctoPart() {
		return strDoctoPart;
	}
	public void setStrDoctoPart(String strDoctoPart) {
		this.strDoctoPart = strDoctoPart;
	}
	public String getStrNomepart() {
		return strNomepart;
	}
	public void setStrNomepart(String strNomepart) {
		this.strNomepart = strNomepart;
	}
	public String getStrOSTitulo() {
		return strOSTitulo;
	}
	public void setStrOSTitulo(String strOSTitulo) {
		this.strOSTitulo = strOSTitulo;
	}
	public String getStrOS() {
		return strOS;
	}
	public void setStrOS(String strOS) {
		this.strOS = strOS;
	}
	public String getStrNome() {
		return strNome;
	}
	public void setStrNome(String strNome) {
		this.strNome = strNome;
	}
	public String getStrCPF_CNPJ() {
		return strCPF_CNPJ;
	}
	public void setStrCPF_CNPJ(String strCPF_CNPJ) {
		this.strCPF_CNPJ = strCPF_CNPJ;
	}
	public String getStrRG() {
		return strRG;
	}
	public void setStrRG(String strRG) {
		this.strRG = strRG;
	}
	public String getStrOrgEmissor() {
		return strOrgEmissor;
	}
	public void setStrOrgEmissor(String strOrgEmissor) {
		this.strOrgEmissor = strOrgEmissor;
	}
	public String getStrDtEmissaoRG() {
		return strDtEmissaoRG;
	}
	public void setStrDtEmissaoRG(String strDtEmissaoRG) {
		this.strDtEmissaoRG = strDtEmissaoRG;
	}
	public String getStrProfissao() {
		return strProfissao;
	}
	public void setStrProfissao(String strProfissao) {
		this.strProfissao = strProfissao;
	}
	public String getStrRenda() {
		return strRenda;
	}
	public void setStrRenda(String strRenda) {
		this.strRenda = strRenda;
	}
	public String getStrEnderecoApur() {
		return strEnderecoApur;
	}
	public void setStrEnderecoApur(String strEnderecoApur) {
		this.strEnderecoApur = strEnderecoApur;
	}
	public String getStrNumeroApur() {
		return strNumeroApur;
	}
	public void setStrNumeroApur(String strNumeroApur) {
		this.strNumeroApur = strNumeroApur;
	}
	public String getStrComplApur() {
		return strComplApur;
	}
	public void setStrComplApur(String strComplApur) {
		this.strComplApur = strComplApur;
	}
	public String getStrBairroApur() {
		return strBairroApur;
	}
	public void setStrBairroApur(String strBairroApur) {
		this.strBairroApur = strBairroApur;
	}
	public String getStrCEPApur() {
		return strCEPApur;
	}
	public void setStrCEPApur(String strCEPApur) {
		this.strCEPApur = strCEPApur;
	}
	public String getStrTelefoneApur() {
		return strTelefoneApur;
	}
	public void setStrTelefoneApur(String strTelefoneApur) {
		this.strTelefoneApur = strTelefoneApur;
	}
	public String getStrCelularApur() {
		return strCelularApur;
	}
	public void setStrCelularApur(String strCelularApur) {
		this.strCelularApur = strCelularApur;
	}
	public String getStrDTAbertura() {
		return strDTAbertura;
	}
	public void setStrDTAbertura(String strDTAbertura) {
		this.strDTAbertura = strDTAbertura;
	}
	public String getStrOPER_VENCER() {
		return strOPER_VENCER;
	}
	public void setStrOPER_VENCER(String strOPER_VENCER) {
		this.strOPER_VENCER = strOPER_VENCER;
	}
	public String getStrOPER_VENCIDA() {
		return strOPER_VENCIDA;
	}
	public void setStrOPER_VENCIDA(String strOPER_VENCIDA) {
		this.strOPER_VENCIDA = strOPER_VENCIDA;
	}
	public String getStrOPER_CRELI() {
		return strOPER_CRELI;
	}
	public void setStrOPER_CRELI(String strOPER_CRELI) {
		this.strOPER_CRELI = strOPER_CRELI;
	}
	public String getStrOPER_PREJU() {
		return strOPER_PREJU;
	}
	public void setStrOPER_PREJU(String strOPER_PREJU) {
		this.strOPER_PREJU = strOPER_PREJU;
	}
	public String getStrOPER_PASSIVA() {
		return strOPER_PASSIVA;
	}
	public void setStrOPER_PASSIVA(String strOPER_PASSIVA) {
		this.strOPER_PASSIVA = strOPER_PASSIVA;
	}
	public String getStrRECIPROCIDADE() {
		return strRECIPROCIDADE;
	}
	public void setStrRECIPROCIDADE(String strRECIPROCIDADE) {
		this.strRECIPROCIDADE = strRECIPROCIDADE;
	}
	public String getStrSelected1() {
		return strSelected1;
	}
	public void setStrSelected1(String strSelected1) {
		this.strSelected1 = strSelected1;
	}
	public String getStrSelected2() {
		return strSelected2;
	}
	public void setStrSelected2(String strSelected2) {
		this.strSelected2 = strSelected2;
	}
	public String getStrSelected3() {
		return strSelected3;
	}
	public void setStrSelected3(String strSelected3) {
		this.strSelected3 = strSelected3;
	}
	public String getStrSelected4() {
		return strSelected4;
	}
	public void setStrSelected4(String strSelected4) {
		this.strSelected4 = strSelected4;
	}
	public int getIntRecCount() {
		return intRecCount;
	}
	public void setIntRecCount(int intRecCount) {
		this.intRecCount = intRecCount;
	}
	public int getiCountLinhas() {
		return iCountLinhas;
	}
	public void setiCountLinhas(int iCountLinhas) {
		this.iCountLinhas = iCountLinhas;
	}
	public String getStrErro() {
		return strErro;
	}
	public void setStrErro(String strErro) {
		this.strErro = strErro;
	}
	
	public Hy_CadastroOs_DetalheDnvolvnaCclienteBean() throws IOException {
		FacesContext fc = FacesContext.getCurrentInstance();
		@SuppressWarnings("unchecked")
		Map<String, String> params = fc.getExternalContext().getRequestParameterMap();

		this.strgNrSeqPart = params.get("NrSeqPart");
		this.strgNrSeqOs = params.get("strNrSeqOs");

		this.iCountLinhas = 0;
		this.intRecCount = 0;
		
		if (!"".equals(this.strgNrSeqPart)) {
			
			CadOsPartici objParticip = new CadOsPartici();
			@SuppressWarnings("unused")
			CadOsPartici objRs = objParticip.ConsultarDetalhePart(this.strgNrSeqPart, this.strErro);
			VerificaErro(objRs.getStErro());
			
			switch (objRs.getTP_PARTICIPACAO()) {
			case 1:
				this.strSelected1 = "Selected";
				break;

			case 2:
				this.strSelected2 = "Selected";
				break;
				
			case 3:
				this.strSelected3 = "Selected";
				break;
				
			case 4:
				this.strSelected4 = "Selected";
				break;
				
			default:
				break;
			}
			
			switch (objRs.getTP_PARTICIPACAO()) {
			case 1:
				this.strTpPart = "Envolvido";
				break;

			case 2:
				this.strTpPart = "Informante";
				break;
				
			case 3:
				this.strTpPart = "Reclamante";
				break;
				
			case 4:
				this.strTpPart = "Reclamado/Lesado";
				break;
				
			default:
				break;
			}
			
			this.strOS = objRs.getOS();
			this.strNome = objRs.getNAME();
			this.strCPF_CNPJ = objRs.getCPF_CNPJ();
			this.strRG = objRs.getRG();
			this.strOrgEmissor = objRs.getORGAO_EMISSOR();
			this.strDtEmissaoRG = objRs.getDT_EMISSAO_RG();
			this.strProfissao = objRs.getPROFISSAO();
			this.strRenda = objRs.getRENDA();
			this.strEnderecoApur = objRs.getENDERECO();
			this.strNumeroApur = objRs.getNUMERO();
			this.strComplApur = objRs.getCOMPLEMENTO();
			this.strBairroApur = objRs.getBAIRRO();
			this.strCEPApur = objRs.getCEP();
			this.strTelefoneApur = objRs.getTELEFONE();
			this.strCelularApur = objRs.getCELULAR();
			this.strDTAbertura = objRs.getDT_ABERTURA();
			this.strOPER_VENCER = objRs.getOPER_VENCER();
			this.strOPER_VENCIDA = objRs.getOPER_VENCIDA();
			this.strOPER_CRELI = objRs.getOPER_CRELI();
			this.strOPER_PREJU = objRs.getOPER_PREJU();
			this.strOPER_PASSIVA = objRs.getOPER_PASSIVA();
			this.strRECIPROCIDADE = objRs.getRECIPROCIDADE();
			this.intTpParticipacao = objRs.getTP_PARTICIPACAO();

			/*
			 * Set objRsRest = objParticip.ConsultarRestricaoPart(strgNrSeqPart, strErro)
			 * VerificaErro(strErro)
			 * 
			 * intRecCount = objRsRest.recordcount
			 */
			
			if ("".equals(this.strgNrSeqOs)) {
				this.strgNrSeqOs = this.strOS;
			} else {
				this.strOSTitulo = this.strgNrSeqOs;
			}
		}
	}	

	public void VerificaErro(String strErro) throws IOException {
		if (!"".equals(strErro)) {
			ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            //ec.redirect("hy_erro.xhtml?&strErro=" + strErro);
		}
	}
	
	public String FillCboEndereco(String strgNrSeqPart) throws IOException {
		String strRetorno;
		
		CadOsPartici objParticip = new CadOsPartici();
		@SuppressWarnings("unused")
		CadOsPartici objRs = objParticip.ConsultarEndeCliente(strgNrSeqPart, 0, this.strErro);
		//VerificaErro(objRs.getStErro());
		
		strRetorno = "<select id='cboEndereco' onchange='fnBuscaEndereco();'>" + "\r\n";
		
		if (objRs.getIntTipoEnd() == 1) {
			strRetorno += "<option value='" +  objRs.getSQ_ENDE() + "'>Endere�o Principal</option>";
			this.strEndereco = objRs.getStrEndereco();
			this.strNumero = objRs.getStrNumero();
			this.strComplemento = objRs.getStrComplemento();
			this.strBairro = objRs.getStrBairro();
			this.strCEP = objRs.getStrCEP();
			this.strCidade = objRs.getStrCidade();
			this.strEstado = objRs.getStrEstado();
			this.strTelefone = objRs.getStrTelefone();
			this.strCelular = objRs.getStrCelular();
		} else {
			strRetorno += "<option value='" + objRs.getSQ_ENDE() + "'>Endere�o Adicional</option>";
		}
		
		strRetorno += "</select>";
		
		return strRetorno;
	}
	

}
