package com.altec.bsbr.app.hyb.web.jsf;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmPenalidade.XHYAdmPenalidadeEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmPenalidade.WebServiceException;

import com.altec.bsbr.app.hyb.dto.AdmGerManutPenaModel;

@Component("admGerManutPenaBean")
@Scope("session")
public class AdmGerManutPenaBean extends BasicBBean {
    private static final long serialVersionUID = 1L;
    
	@Autowired
    private XHYAdmPenalidadeEndPoint objRsPenal;
    
	private String txtAddPenalidade;
    private String txtHdCodPenalidade;
    private String txtHdUnload = "0";
    private String txtHdAuxPenalidade;
    private String retorno;
    
    private AdmGerManutPenaModel objRsAdmPenalModel;    
    private List<AdmGerManutPenaModel> objRsAdmPenal;
    
    @PostConstruct
    public void init() {
    	ObjRsAdmPenal();
    }
    
    public void ObjRsAdmPenal() {
       objRsAdmPenal = new ArrayList<AdmGerManutPenaModel>();
       
       try {			
			retorno = objRsPenal.consultarPenalidade();
	        
	        JSONObject consultarPenalidade = new JSONObject(retorno);
	        JSONArray pcursor = consultarPenalidade.getJSONArray("PCURSOR");
	        
	        for (int i = 0; i < pcursor.length(); i++) {
	        	JSONObject f = pcursor.getJSONObject(i);
	            objRsAdmPenalModel = new AdmGerManutPenaModel();
	            objRsAdmPenalModel.setCODIGO(f.isNull("CODIGO") ? 0 : f.getInt("CODIGO"));
	            objRsAdmPenalModel.setNOME(f.isNull("NOME") ? "" : f.getString("NOME"));
	            objRsAdmPenalModel.setEXCLUSAO(f.getInt("EXCLUSAO") == 1 ? true : false);
	            
	            objRsAdmPenal.add(objRsAdmPenalModel); 	            
	        }
	
		} catch (WebServiceException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		    FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "hyb_erro.xhtml?&strErro=" + e.getMessage());
        }
    }
    
    public String getObjRsAdmPenalJSON() {
    	JSONArray arr = new JSONArray(objRsAdmPenal);
    	return arr.toString();
    }

    public void adicionar() {
    	Map<String, String> requestParamMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		String penalidadeAdd = requestParamMap.get("frm:txtAddPenalidade");
    	try {
			objRsPenal.incluirPenalidade(penalidadeAdd);
			setTxtAddPenalidade(null);
			RequestContext.getCurrentInstance().execute("alert('Penalidade incluída com sucesso!')");
			RequestContext.getCurrentInstance().execute("limparCampo()");
			
			ObjRsAdmPenal();
	
		} catch (Exception e) {
		    // TODO Auto-generated catch block
			try {
	    		FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (Exception ex) {
			    // TODO Auto-generated catch block
			    e.printStackTrace();
			}
		}   	
    }
    public void salvar() {
    	try {
    		String arrPenalidades = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("ArrPenalidade").toString();
        	
        	objRsPenal.arrayAlterarPenalidade(arrPenalidades.replace("@", ","));  
        
			RequestContext.getCurrentInstance().execute("alert('Alterações efetuadas com sucesso!')"); 
	    	ObjRsAdmPenal();    
	
		} catch (Exception e) {
		    // TODO Auto-generated catch block
			try {
	    		FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (Exception ex) {
			    // TODO Auto-generated catch block
			    e.printStackTrace();
			}
		}  
    }
    public void deletar() {
    	long codPenal = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("CodPenalidade").toString());
    	try {
			objRsPenal.apagarPenalidade(codPenal);  
			RequestContext.getCurrentInstance().execute("alert('Penalidade excluída com sucesso!')");
	    	ObjRsAdmPenal();
		} catch (Exception e) {
		    // TODO Auto-generated catch block
			try {
	    		FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (Exception ex) {
			    // TODO Auto-generated catch block
			    e.printStackTrace();
			}
		}
    }
    
    public List<AdmGerManutPenaModel> getObjRsAdmPenal() {
        return objRsAdmPenal;
    }
    public void setObjRsAdmPenal(List<AdmGerManutPenaModel> objRsAdmPenal) {
    	this.objRsAdmPenal = objRsAdmPenal;
    } 

    public String getTxtAddPenalidade() {
		return txtAddPenalidade;
	}

	public void setTxtAddPenalidade(String txtAddPenalidade) {
		this.txtAddPenalidade = txtAddPenalidade;
	}
	
    public String getTxtHdCodPenalidade() {
        return txtHdCodPenalidade;
    }
    public void setTxtHdCodPenalidade(String txtHdCodPenalidade) {
        this.txtHdCodPenalidade = txtHdCodPenalidade;
    }
    public String getTxtHdUnload() {
        return txtHdUnload;
    }
    public void setTxtHdUnload(String txtHdUnload) {
        this.txtHdUnload = txtHdUnload;
    }
    public String getTxtHdAuxPenalidade() {
        return txtHdAuxPenalidade;
    }
    public void setTxtHdAuxPenalidade(String txtHdAuxPenalidade) {
        this.txtHdAuxPenalidade = txtHdAuxPenalidade;
    }
}
