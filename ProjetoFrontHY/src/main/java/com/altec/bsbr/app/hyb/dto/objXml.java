package com.altec.bsbr.app.hyb.dto;

public class objXml {
	public String strAcao;
	public String NrSeqOper;
	public String NrSeqOperAux;
	public String TipoProd;
	public String TipoContest;
	public String CTROORIG;
	public String CTRODEST;
	public String CTROOPER;
	public String QTDEOCORR;
	public String NrSeqFrauCanal;
	

	public objXml() {}
	
	public String getStrAcao() {
		return strAcao;
	}

	public void setStrAcao(String stracao) {
		strAcao = stracao;
	}

	public String getNrSeqOper() {
		return NrSeqOper;
	}

	public void setNrSeqOper(String nrSeqOper) {
		NrSeqOper = nrSeqOper;
	}

	public String getNrSeqOperAux() {
		return NrSeqOperAux;
	}

	public void setNrSeqOperAux(String nrSeqOperAux) {
		NrSeqOperAux = nrSeqOperAux;
	}

	public String getTipoProd() {
		return TipoProd;
	}

	public void setTipoProd(String tipoProd) {
		TipoProd = tipoProd;
	}

	public String getTipoContest() {
		return TipoContest;
	}

	public void setTipoContest(String tipoContest) {
		TipoContest = tipoContest;
	}

	public String getCTROORIG() {
		return CTROORIG;
	}

	public void setCTROORIG(String cTROORIG) {
		CTROORIG = cTROORIG;
	}

	public String getCTRODEST() {
		return CTRODEST;
	}

	public void setCTRODEST(String cTRODEST) {
		CTRODEST = cTRODEST;
	}

	public String getCTROOPER() {
		return CTROOPER;
	}

	public void setCTROOPER(String cTROOPER) {
		CTROOPER = cTROOPER;
	}

	public String getQTDEOCORR() {
		return QTDEOCORR;
	}

	public void setQTDEOCORR(String qTDEOCORR) {
		QTDEOCORR = qTDEOCORR;
	}

	public String getNrSeqFrauCanal() {
		return NrSeqFrauCanal;
	}

	public void setNrSeqFrauCanal(String nrSeqFrauCanal) {
		NrSeqFrauCanal = nrSeqFrauCanal;
	}
	
}
