package com.altec.bsbr.app.hyb.web.jsf;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.RelCartaApresentacaoEquipeModel;
import com.altec.bsbr.app.hyb.dto.RelCartaApresentacaoModel;
import com.altec.bsbr.app.hyb.dto.UsuarioIncModel;
import com.altec.bsbr.app.hyb.web.util.XHYUsuarioIncService;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAcoesOs.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAcoesOs.XHYAcoesOsEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;

@Component("relCartaApresentacaoBean")
@Scope("request")
public class RelCartaApresentacaoBean extends BasicBBean {

	private static final long serialVersionUID = 1L;
	
	private String strNrSeqOs;
	private String txtApre;
	private String login_usuario;
	private String retorno;
	
	@Autowired
	private XHYUsuarioIncService user;
	
	@Autowired
    private XHYAcoesOsEndPoint objRs;
	

    private RelCartaApresentacaoModel objRsCartaApreModel;
    private RelCartaApresentacaoEquipeModel objRsCartaApreEquipeModel;
    private List<RelCartaApresentacaoEquipeModel> objRsCartaApreEquipe;
    private List<RelCartaApresentacaoEquipeModel> objRsCartaApreEquipe1;
   
    
	@PostConstruct
    public void init(){
		strNrSeqOs = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs");
		//data_atual = new SimpleDateFormat("dd/MM/yyyy").format(new Date()).toString();
		//hora_atual = new SimpleDateFormat("HH:mm").format(new Date()).toString();
		
		ObjRsMinuta();

		String busca = "";
	    objRsCartaApreEquipe = new ArrayList<RelCartaApresentacaoEquipeModel>();
	    ObjRsMinutaEquipe(objRsCartaApreEquipe, busca);

	    
	    busca = "B70555";
	    objRsCartaApreEquipe1 = new ArrayList<RelCartaApresentacaoEquipeModel>();
	    ObjRsMinutaEquipe(objRsCartaApreEquipe1, busca);
		
		if (user != null) {			
			try {
				UsuarioIncModel dadosUser = user.getDadosUsuario();
				setLogin_usuario(dadosUser.getLoginUsuario());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}		
	}
	
	public void ObjRsMinuta() {	       
       try {
			retorno = objRs.consultarOS(strNrSeqOs);
	        JSONObject rstemp = new JSONObject(retorno);
	        JSONArray pcursor = rstemp.getJSONArray("PCURSOR");
	        JSONObject f = pcursor.getJSONObject(0);
	        
        	objRsCartaApreModel = new RelCartaApresentacaoModel();	        
        	objRsCartaApreModel.setDataAtual(f.isNull("DATA_ATUAL") ? "" : f.getString("DATA_ATUAL"));
        	objRsCartaApreModel.setHoraAtual(f.isNull("HORA_ATUAL") ? "" : f.getString("HORA_ATUAL"));
        	objRsCartaApreModel.setDataAbertura(f.isNull("DT_ABERTURA") ? "" : f.getString("DT_ABERTURA"));
        	objRsCartaApreModel.setDataEncerramento(f.isNull("DT_PREV_ENCERRAMENTO") ? "" : f.getString("DT_PREV_ENCERRAMENTO"));
        	objRsCartaApreModel.setSituacao(f.isNull("SITUACAO") ? "" : f.getString("SITUACAO"));
        	objRsCartaApreModel.setArea(f.isNull("AREA") ? "" : f.getString("AREA"));
        		        	
		} catch (WebServiceException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		    FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "hyb_erro.xhtml?&strErro=" + e.getMessage());
        }
    }
	
	public void ObjRsMinutaEquipe(List<RelCartaApresentacaoEquipeModel> lista, String busca) {	
       try {
	        retorno = objRs.consultarEquipeOs(strNrSeqOs, busca);
	        JSONObject rstemp = new JSONObject(retorno);
	        JSONArray pcursor = rstemp.getJSONArray("PCURSOR");
	        String tpDesi;
	        String cargo;
	        
	        for (int i = 0; i < pcursor.length(); i++) {
		        JSONObject f = pcursor.getJSONObject(i);
	        	objRsCartaApreEquipeModel = new RelCartaApresentacaoEquipeModel();
	        	objRsCartaApreEquipeModel.setNome(f.isNull("NM_RECU_OCOR_ESPC") ? "" : f.getString("NM_RECU_OCOR_ESPC"));
	        	objRsCartaApreEquipeModel.setMatricula(f.isNull("NR_MATR_PRSV") ? "" : f.getString("NR_MATR_PRSV"));
	        	objRsCartaApreEquipeModel.setDesignacao(f.isNull("NM_DESIGNACAO") ? "" : f.getString("NM_DESIGNACAO"));
	        	tpDesi=(f.isNull("TP_DESI") ? "" : f.getString("TP_DESI"));
	        	cargo=(f.isNull("CD_CARG") ? "" : f.getString("CD_CARG"));

		        if ( ( busca != "" && (cargo.equals(busca)) ) || ( busca == "" && ( tpDesi.equals("AD") || tpDesi.equals("AA") || tpDesi.equals("PSD") || tpDesi.equals("PSA") || tpDesi.equals("NA") ) ) ){
	        		lista.add( new RelCartaApresentacaoEquipeModel(
		            		objRsCartaApreEquipeModel.getNome(), 
		            		objRsCartaApreEquipeModel.getMatricula(), 
		            		objRsCartaApreEquipeModel.getDesignacao())
		    		); 
	        	}
	        }
	
		} catch (WebServiceException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		    FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "hyb_erro.xhtml?&strErro=" + e.getMessage());
        }
    }

	public String getStrNrSeqOs() {
		return strNrSeqOs;
	}

	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}

	public String getTxtApre() {
		return txtApre;
	}

	public void setTxtApre(String txtApre) {
		this.txtApre = txtApre;
	}

	public String getLogin_usuario() {
		return login_usuario;
	}

	public void setLogin_usuario(String login_usuario) {
		this.login_usuario = login_usuario;
	}

	public RelCartaApresentacaoModel getObjRsMinutaModel() {
		return objRsCartaApreModel;
	}

	public void setObjRsMinutaModel(RelCartaApresentacaoModel objRsCartaApreModel) {
		this.objRsCartaApreModel = objRsCartaApreModel;
	}

	public List<RelCartaApresentacaoEquipeModel> getObjRsCartaApreEquipe() {
		return objRsCartaApreEquipe;
	}

	public void setObjRsCartaApreEquipe(List<RelCartaApresentacaoEquipeModel> objRsCartaApreEquipe) {
		this.objRsCartaApreEquipe = objRsCartaApreEquipe;
	}

	public List<RelCartaApresentacaoEquipeModel> getObjRsCartaApreEquipe1() {
		return objRsCartaApreEquipe1;
	}

	public void setObjRsCartaApreEquipe1(List<RelCartaApresentacaoEquipeModel> objRsCartaApreEquipe1) {
		this.objRsCartaApreEquipe1 = objRsCartaApreEquipe1;
	}
	
}
