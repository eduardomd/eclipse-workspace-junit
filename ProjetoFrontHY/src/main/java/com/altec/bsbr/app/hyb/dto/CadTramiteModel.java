package com.altec.bsbr.app.hyb.dto;

public class CadTramiteModel {
	private String OS;		
	private String Tramite;
	private String Registro;
	private String Ret_plan;
	private String retorno;
	private String Destino;
	private String hist_envio;
	private String hist_ret;
	
	public CadTramiteModel() {}

	public String getOS() {
		return OS;
	}
	public void setOS(String oS) {
		OS = oS;
	}

	public String getTramite() {
		return Tramite;
	}
	public void setTramite(String tramite) {
		Tramite = tramite;
	}

	public String getRegistro() {
		return Registro;
	}
	public void setRegistro(String registro) {
		Registro = registro;
	}

	public String getRet_plan() {
		return Ret_plan;
	}
	public void setRet_plan(String ret_plan) {
		Ret_plan = ret_plan;
	}

	public String getRetorno() {
		return retorno;
	}
	public void setRetorno(String retorno) {
		this.retorno = retorno;
	}

	public String getDestino() {
		return Destino;
	}
	public void setDestino(String destino) {
		Destino = destino;
	}

	public String getHist_envio() {
		return hist_envio;
	}
	public void setHist_envio(String hist_envio) {
		this.hist_envio = hist_envio;
	}

	public String getHist_ret() {
		return hist_ret;
	}
	public void setHist_ret(String hist_ret) {
		this.hist_ret = hist_ret;
	}
}
