package com.altec.bsbr.app.hyb.web.jsf; 

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.ObjRsLocalTrein;
import com.altec.bsbr.app.hyb.dto.ObjRsTreiAnexo;
import com.altec.bsbr.app.hyb.dto.ObjRsTrein;
import com.altec.bsbr.app.jab.hyb.webclient.XHYTreinamento.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYTreinamento.XHYTreinamentoEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;


@Component("treinamentoPesq")
@Scope("session")
public class TreinamentoPesquisaBean extends BasicBBean {
 
	private static final long serialVersionUID = 1L;

	@Autowired 
	XHYTreinamentoEndPoint objTrein;
	
	private String strTreiNmArquivo;
	
	private String txtNome;
	private String cboLocal;
	private Date txtDtInicio;
	private Date txtDtFim;
	
	private boolean objTreinEOF;
	
	private List<ObjRsLocalTrein> objRsLocalTrein;
	private List<ObjRsTrein> objRsTrein;
	private List<ObjRsTreiAnexo> objRsTreiAnexo;
	
	private String selectedRadio;
	
	private StreamedContent file;
	
	@PostConstruct
	public void init() {
		this.objTreinEOF = false;
		this.selectedRadio = "";
		this.setupObjRsLocalTrein();
		
	}
	
	public void pesquisar() {
	    
	    SimpleDateFormat formatStr = new SimpleDateFormat("dd/MM/yyyy");
	    SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	    
		String strDtInicio = "";
		String strDtFim = "";
		
		System.out.println("TXT INICIO : " + txtDtInicio);
		System.out.println("TXT FIM" + txtDtFim);
		
		if (this.txtDtInicio == null) {
			strDtInicio = "";
		} else {
			if (!this.txtDtInicio.equals("")) {
				strDtInicio = newFormat.format(this.txtDtInicio);
			} else {
				Date strDateTwo;
				try {
					strDateTwo = formatStr.parse("01/01/2000");
					strDtInicio = newFormat.format(strDateTwo);
				} catch (ParseException e) {
					try {
						FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
		            } catch (IOException e1) {
		               e1.printStackTrace();
		            }
				}
			}
		}
		
		if (this.txtDtFim == null) {
			strDtFim = "";
		} else {
			if (!this.txtDtFim.equals("")) {
				strDtFim = newFormat.format(this.txtDtFim);
			} else {
				Date strDateTwo = Calendar.getInstance().getTime();
				strDtFim = newFormat.format(strDateTwo);
			}
		}
		
		System.out.println("DATE: " + strDtFim);
		
		try {
			String retorno = objTrein.consultarTreinamento("0", this.txtNome, strDtInicio, strDtFim, Integer.parseInt("0" + this.cboLocal));
            
            JSONObject consultarTrei = new JSONObject(retorno);
            JSONArray pcursor = consultarTrei.getJSONArray("PCURSOR");
            
            objRsTrein = new ArrayList<ObjRsTrein>();
            
            System.out.println("CURSOR s: " + pcursor);
            
            if (pcursor.length() > 0) {
            	this.objTreinEOF = false;
                for (int i = 0; i < pcursor.length(); i++) {
                	   JSONObject item = pcursor.getJSONObject(i);
                       String codigo = item.isNull("CODIGO") ? "" : item.get("CODIGO").toString();
                       String nome = item.isNull("NOME") ? "" : item.get("NOME").toString();
                       String data = item.isNull("DATA") ? "" : item.get("DATA").toString();
                       String qtde = item.isNull("QTDE") ? "" : item.get("QTDE").toString();
                       String local = item.isNull("LOCAL") ? "" : item.get("LOCAL").toString();
                       
                       this.setupObjRsTreiAnexo(codigo);
                       
                       objRsTrein.add(new ObjRsTrein(codigo, nome, data, qtde, local, objRsTreiAnexo));
                }
            } else {
            	this.objTreinEOF = true;
            }
	
		} catch (NumberFormatException | WebServiceException e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
            } catch (IOException e1) {
               e1.printStackTrace();
            }
		}
	}
	
	public void alterar() {
		System.out.println("ALTERAR: " + selectedRadio);
		if (this.selectedRadio != "") {
			RequestContext.getCurrentInstance().execute("window.location.href = 'hyb_treinamento_cadastro.xhtml?strSeqTrei="+this.selectedRadio+"'");
		} else {
			RequestContext.getCurrentInstance().execute("alert('Favor selecionar um item.')");
		}
		
	}
	
	public void setupObjRsTreiAnexo(String codigo) {
		
		//List<ObjRsTreiAnexo> anexoList = new ArrayList<ObjRsTreiAnexo>();

		try {
			String retorno = objTrein.consultarAnexo(codigo);
            
            JSONObject consultarTreiAnexo = new JSONObject(retorno);
            JSONArray pcursor = consultarTreiAnexo.getJSONArray("PCURSOR");
            
            objRsTreiAnexo = new ArrayList<ObjRsTreiAnexo>();
            
            System.out.println("FILE s Cs URS OR a: " + pcursor);
            if (pcursor.length() > 0) {
            	
                for (int i = 0; i < pcursor.length(); i++) {
                       JSONObject item = pcursor.getJSONObject(i);
                       String codigoAnexo = item.isNull("CODIGO") ? "" : item.get("CODIGO").toString();
                       String nmCamiAnex = item.isNull("NM_CAMI_ANEX") ? "" : item.get("NM_CAMI_ANEX").toString();
                       
                       objRsTreiAnexo.add(new ObjRsTreiAnexo(codigoAnexo, nmCamiAnex));
                       System.out.println("ANEXsO LIST : " + objRsTreiAnexo.get(i).getNmCamiAnex());
                }
                
            }
            
		} catch (NumberFormatException | WebServiceException e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
            } catch (IOException e1) {
               e1.printStackTrace();
            }
		}
		
	}
	
	public void setupObjRsLocalTrein() {
		try {
			String retorno = objTrein.consultarLocalTreinamento();
            
            JSONObject consultarLocalTrei = new JSONObject(retorno);
            JSONArray pcursor = consultarLocalTrei.getJSONArray("PCURSOR");
            
            objRsLocalTrein = new ArrayList<ObjRsLocalTrein>();
            
            if (pcursor.length() > 0) {
                for (int i = 0; i < pcursor.length(); i++) {
                       JSONObject item = pcursor.getJSONObject(i);
                       String codigo = item.isNull("CODIGO") ? "" : item.get("CODIGO").toString();
                       String nome = item.isNull("NOME") ? "" : item.get("NOME").toString();
                       
                       objRsLocalTrein.add(new ObjRsLocalTrein(codigo, nome));
                }
            }
	
		} catch (NumberFormatException | WebServiceException e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
            } catch (IOException e1) {
               e1.printStackTrace();
            }
		}
	}
	
	public void saveFile(String filePath) {
		String fileName = new File(filePath).getName().toString();
		String contentType = FacesContext.getCurrentInstance().getExternalContext().getMimeType(filePath);
		InputStream stream;
		try {
			stream = new FileInputStream(filePath);
			file = new DefaultStreamedContent(stream, contentType, fileName);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public XHYTreinamentoEndPoint getObjTrein() {
		return objTrein;
	}

	public void setObjTrein(XHYTreinamentoEndPoint objTrein) {
		this.objTrein = objTrein;
	}
	public String getStrTreiNmArquivo() {
		return strTreiNmArquivo;
	}

	public void setStrTreiNmArquivo(String strTreiNmArquivo) {
		this.strTreiNmArquivo = strTreiNmArquivo;
	}

	public String getTxtNome() {
		return txtNome;
	}

	public void setTxtNome(String txtNome) {
		this.txtNome = txtNome;
	}

	public String getCboLocal() {
		return cboLocal;
	}

	public void setCboLocal(String cboLocal) {
		this.cboLocal = cboLocal;
	}

	public Date getTxtDtInicio() {
		return txtDtInicio;
	}

	public void setTxtDtInicio(Date txtDtInicio) {
		this.txtDtInicio = txtDtInicio;
	}

	public Date getTxtDtFim() {
		return txtDtFim;
	}

	public void setTxtDtFim(Date txtDtFim) {
		this.txtDtFim = txtDtFim;
	}

	public boolean isObjTreinEOF() {
		return objTreinEOF;
	}

	public void setObjTreinEOF(boolean objTreinEOF) {
		this.objTreinEOF = objTreinEOF;
	}

	public List<ObjRsTrein> getObjRsTrein() {
		return objRsTrein;
	}

	public void setObjRsTrein(List<ObjRsTrein> objRsTrein) {
		this.objRsTrein = objRsTrein;
	}

	public void setObjRsLocalTrein(List<ObjRsLocalTrein> objRsLocalTrein) {
		this.objRsLocalTrein = objRsLocalTrein;
	}

	public List<ObjRsTreiAnexo> getObjRsTreiAnexo() {
		return objRsTreiAnexo;
	}

	public void setObjRsTreiAnexo(List<ObjRsTreiAnexo> objRsTreiAnexo) {
		this.objRsTreiAnexo = objRsTreiAnexo;
	}

	public List<ObjRsLocalTrein> getObjRsLocalTrein() {
		return objRsLocalTrein;
	}

	public String getSelectedRadio() {
		return selectedRadio;
	}

	public void setSelectedRadio(String selectedRadio) {
		this.selectedRadio = selectedRadio;
	}

	public StreamedContent getFile() {
		return file;
	}

	public void setFile(StreamedContent file) {
		this.file = file;
	}
}
 