package com.altec.bsbr.app.hyb.dto;

public class AdmGerManutPCorpModel {
	private int CODIGO;
	private String NOME;
	private String SUBNOME;
	private int SLA;
	private boolean OPERACAODEB;
	private boolean OPERACAOCRED;
	private boolean VISU;

	public AdmGerManutPCorpModel(int cODIGO, String nOME, String sUBNOME, int sLA, boolean OPDEB, boolean OPCRED, boolean vISU) {
		setCODIGO(cODIGO);
		setNOME(nOME);
		setSUBNOME(sUBNOME);
		setSLA(sLA);
		setOPERACAODEB(OPDEB);
		setOPERACAOCRED(OPCRED);
		setVISU(vISU);
	}

	public AdmGerManutPCorpModel() {
	}

	public int getCODIGO() {
		return CODIGO;
	}	
	public void setCODIGO(int cODIGO) {
		CODIGO = cODIGO;
	}
	
	public String getNOME() {
		return NOME;
	}
	public void setNOME(String nOME) {
		NOME = nOME;
	}

	public String getSUBNOME() {
		return SUBNOME;
	}

	public void setSUBNOME(String sUBNOME) {
		SUBNOME = sUBNOME;
	}
	
	public int getSLA() {
		return SLA;
	}
	public void setSLA(int sLA) {
		SLA = sLA;
	}

	public boolean getOPERACAODEB() {
		return OPERACAODEB;
	}
	public void setOPERACAODEB(boolean oPERACAODEB) {
		OPERACAODEB = oPERACAODEB;
	}

	public boolean getOPERACAOCRED() {
		return OPERACAOCRED;
	}
	public void setOPERACAOCRED(boolean oPERACAOCRED) {
		OPERACAOCRED = oPERACAOCRED;
	}

	public boolean getVISU() {
		return VISU;
	}

	public void setVISU(boolean vISU) {
		VISU = vISU;
	}
}
