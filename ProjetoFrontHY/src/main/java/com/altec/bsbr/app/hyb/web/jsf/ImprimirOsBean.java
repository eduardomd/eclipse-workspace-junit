package com.altec.bsbr.app.hyb.web.jsf;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.PrintObjRs;
import com.altec.bsbr.app.hyb.dto.PrintObjRsCadOSArqAnex;
import com.altec.bsbr.app.hyb.dto.PrintObjRsEqpAtrbOs;
import com.altec.bsbr.app.hyb.dto.PrintObjRsEventosOs;
import com.altec.bsbr.app.hyb.dto.PrintObjRsOs;
import com.altec.bsbr.app.hyb.dto.PrintObjRsUnidEnvolv;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAcoesOs.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAcoesOs.XHYAcoesOsEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsEquipAtrib.XHYCadOsEquipAtribEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsInfGerais.XHYCadOsInfGeraisEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsParticip.XHYCadOsParticipEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsUnidEnvol.XHYCadOsUnidEnvolEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.ibm.icu.text.DecimalFormat;

@Component("imprimirOS")
@Scope("request")
public class ImprimirOsBean extends BasicBBean {
	private static final long serialVersionUID = 1L;
	
	private String strNrSeqOs;
	private String strRetorno;
	private String strCdUor;
	private String strTpUor;
	private String strBgColor;
	
	private List<PrintObjRs> objRsClienteBanco;
	private List<PrintObjRs> objRsParticipProcesso;
	private PrintObjRsOs objRsOs;
	private PrintObjRsEventosOs objRsEventosOs;
	private List<PrintObjRsEqpAtrbOs> objRsEqpAtrbOs;
	private List<PrintObjRsCadOSArqAnex> objRsCadOSArqAnex;
	private PrintObjRsUnidEnvolv objRsUnidEnvolv;
	
	//Para renderizar os paineis em caso de campos voltando uma string "null" ou cursor vazio
	private Boolean renderRsUnidEnvolv;
	
	@Autowired
	private XHYAcoesOsEndPoint acoesOs;
	
	@Autowired 
	private XHYCadOsEquipAtribEndPoint cadOsEquipAtrib;
	
	@Autowired
	private XHYCadOsInfGeraisEndPoint cadOsInfGerais;
	
	@Autowired
	private XHYCadOsUnidEnvolEndPoint cadOsUnidEnvolv;
	
	@Autowired
	private XHYCadOsParticipEndPoint cadOsParticip;
		
	public void setupObjRsOs() {
		String retorno = "";
		try {
			retorno = acoesOs.consultarOS(this.strNrSeqOs);
	        System.out.println("retorno consultarOS"+retorno);
		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
        JSONObject jsonRetorno = new JSONObject(retorno);
        JSONArray pcursor = jsonRetorno.getJSONArray("PCURSOR");
        for(int i = 0; i < pcursor.length(); i++) {
        	JSONObject curr = pcursor.getJSONObject(i);
        	objRsOs.setDATA_ATUAL(curr.isNull("DATA_ATUAL") ? "" : curr.getString("DATA_ATUAL"));
    		objRsOs.setHORA_ATUAL(curr.isNull("HORA_ATUAL") ? "" : curr.getString("HORA_ATUAL"));
    		objRsOs.setCODIGO(curr.isNull("CODIGO") ? "" : curr.getString("CODIGO"));
    		objRsOs.setSITUACAO(curr.isNull("SITUACAO") ? "" : curr.getString("SITUACAO"));
    		objRsOs.setCD_NOTI(curr.isNull("CD_NOTI") ? "" : String.valueOf(curr.getInt("CD_NOTI")));
    		objRsOs.setNM_NOTI(curr.isNull("NM_NOTI") ? "" : curr.getString("NM_NOTI"));
    		objRsOs.setNM_CRITICIDADE(curr.isNull("NM_CRITICIDADE") ? "" : curr.getString("NM_CRITICIDADE"));
    		objRsOs.setNM_FASE_AN(curr.isNull("NM_FASE_AN") ? "" : curr.getString("NM_FASE_AN"));
    		objRsOs.setDT_ABERTURA(curr.isNull("DT_ABERTURA") ? "" : curr.getString("DT_ABERTURA"));
    		objRsOs.setDT_DETECCAO(curr.isNull("DT_DETECCAO") ? "" : curr.getString("DT_DETECCAO"));
    		objRsOs.setTIPO_DT_EVENTO(curr.isNull("TIPO_DT_EVENTO") ? "" : String.valueOf(curr.getInt("TIPO_DT_EVENTO")));
    		objRsOs.setDT_ACOR_FIXA(curr.isNull("DT_ACOR_FIXA") ? "" : curr.getString("DT_ACOR_FIXA"));
    		objRsOs.setDT_OCOR_PERI_INIC(curr.isNull("DT_OCOR_PERI_INIC") ? "" : curr.getString("DT_OCOR_PERI_INIC"));
    		objRsOs.setDT_OCOR_PERI_FINA(curr.isNull("DT_OCOR_PERI_FINA") ? "" : curr.getString("DT_OCOR_PERI_FINA"));
    		objRsOs.setCANAL_ORIGEM(curr.isNull("CANAL_ORIGEM") ? "" : curr.getString("CANAL_ORIGEM"));
    		objRsOs.setTP_OFICIO(curr.isNull("TP_OFICIO") ? "" : curr.getString("TP_OFICIO"));
    		objRsOs.setTP_LYNX(curr.isNull("TP_LYNX") ? "" : curr.getString("TP_LYNX"));
    		objRsOs.setTP_MONITORAMENTO(curr.isNull("MONITORAMENTO") ? "" : curr.getString("MONITORAMENTO"));
    		objRsOs.setIN_OMINTE(curr.isNull("IN_OMINTE") ? "" : String.valueOf(curr.getInt("IN_OMINTE")));
    		objRsOs.setIN_OMINTR(curr.isNull("IN_OMINTR") ? "" : String.valueOf(curr.getInt("IN_OMINTR")));
    		objRsOs.setIN_OMINSTR(curr.isNull("IN_OMINTR") ? "" : String.valueOf(curr.getInt("IN_OMINTR")));
    		objRsOs.setIN_OMTELE(curr.isNull("IN_OMTELE") ? "" : String.valueOf(curr.getInt("IN_OMTELE")));
    		objRsOs.setIN_OMMALT(curr.isNull("IN_OMMALT") ? "" : String.valueOf(curr.getInt("IN_OMMALT")));
    		objRsOs.setIN_OMDENC(curr.isNull("IN_OMDENC") ? "" : String.valueOf(curr.getInt("IN_OMDENC")));
    		objRsOs.setIN_OMDEPT(curr.isNull("IN_OMDEPT") ? "" : String.valueOf(curr.getInt("IN_OMDEPT")));
    		objRsOs.setIN_OMAGEN(curr.isNull("IN_OMAGEN") ? "" : String.valueOf(curr.getInt("IN_OMAGEN")));
    		objRsOs.setCD_CANAL_ORIGEM(curr.isNull("CD_CANAL_ORIGEM") ? "" : String.valueOf(curr.getInt("CD_CANAL_ORIGEM")));
    		objRsOs.setVL_ENVOLVIDO(curr.isNull("VL_ENVOLVIDO") ? "" : String.valueOf(curr.getInt("VL_ENVOLVIDO")));
    		objRsOs.setVL_RECUPERADO(curr.isNull("VL_RECUPERADO") ? "" : String.valueOf(curr.getInt("VL_RECUPERADO")));
    		objRsOs.setVL_EVITADO(curr.isNull("VL_EVITADO") ? "" : String.valueOf(curr.getInt("VL_EVITADO")));
    		objRsOs.setVL_NEGADO(curr.isNull("VL_NEGADO") ? "" : String.valueOf(curr.getInt("VL_NEGADO")));
    		objRsOs.setVL_TRANSITORIO(curr.isNull("VL_TRANSITORIO") ? "" : String.valueOf(curr.getInt("VL_TRANSITORIO")));
    		objRsOs.setVL_PERDA_EFETIVA(curr.isNull("VL_PERDA_EFETIVA") ? "" : String.valueOf(curr.getInt("VL_PERDA_EFETIVA")));
    		objRsOs.setVL_EVITADO_NORMAL(curr.isNull("VL_EVITADO_NORMAL") ? "" : String.valueOf(curr.getInt("VL_EVITADO_NORMAL")));
    		objRsOs.setVL_PREJUIZO(curr.isNull("VL_PREJUIZO") ? "" : String.valueOf(curr.getInt("VL_PREJUIZO")));
    		objRsOs.setDT_CONTABILIZACAO(curr.isNull("DT_CONTABILIZACAO") ? "" : curr.getString("DT_CONTABILIZACAO"));
    		objRsOs.setCONTA_CONTABIL(curr.isNull("CONTA_CONTABIL") ? "" : curr.getString("CONTA_CONTABIL"));
    		objRsOs.setEMPRESA(curr.isNull("EMPRESA") ? "" : curr.getString("EMPRESA"));
    		objRsOs.setCANAL_ORIG_CNTB(curr.isNull("CANAL_ORIG_CNTB") ? "" : curr.getString("CANAL_ORIG_CNTB"));
    		objRsOs.setAREA_COMPL(curr.isNull("AREA_COMPL") ? "" : curr.getString("AREA_COMPL"));
    		objRsOs.setCATEG_N1(curr.isNull("CATEG_N1") ? "" : curr.getString("CATEG_N1"));
    		objRsOs.setCATEG_N2(curr.isNull("CATEG_N2") ? "" : curr.getString("CATEG_N2"));
    		objRsOs.setCATEG_N3(curr.isNull("CATEG_N3") ? "" : curr.getString("CATEG_N3"));
    		objRsOs.setLINHA_NEGO_N1(curr.isNull("LINHA_NEGO_N1") ? "" : curr.getString("LINHA_NEGO_N1"));
    		objRsOs.setLINHA_NEGO_N2(curr.isNull("LINHA_NEGO_N2") ? "" : curr.getString("LINHA_NEGO_N2"));
    		objRsOs.setFATOR_RISCO(curr.isNull("FATOR_RISCO") ? "" : curr.getString("FATOR_RISCO"));
    		objRsOs.setCENTRO_CONTABIL(curr.isNull("CENTRO_CONTABIL") ? "" : curr.getString("CENTRO_CONTABIL"));
    		objRsOs.setCENTRO_ORIGEM(curr.isNull("CENTRO_ORIGEM") ? "" : curr.getString("CENTRO_ORIGEM"));
    		objRsOs.setPONTO_VENDA(curr.isNull("PONTO_VENDA") ? "" : curr.getString("PONTO_VENDA"));
    		objRsOs.setTX_ABERTURA(curr.isNull("TX_ABERTURA") ? "" : curr.getString("TX_ABERTURA"));
    		objRsOs.setTX_ENCERRAMENTO(curr.isNull("TX_ENCERRAMENTO") ? "" : curr.getString("TX_ENCERRAMENTO"));
    		objRsOs.setBO(curr.isNull("BO") ? "" : curr.getString("BO"));
    		objRsOs.setIN_PROC_JURI(curr.isNull("IN_PROC_JURI") ? "" : String.valueOf(curr.getInt("IN_PROC_JURI")));
    		objRsOs.setDELEGACIA(curr.isNull("DELEGACIA") ? "" : curr.getString("DELEGACIA"));
    		objRsOs.setNM_FORUM(curr.isNull("NM_FORUM") ? "" : curr.getString("NM_FORUM"));
    		objRsOs.setDT_REG_BO(curr.isNull("DT_REG_BO") ? "" : curr.getString("DT_REG_BO"));
        }	
	}
	
	public void setupObjRsEventosOs() {
		String retorno = "";
		try {
			retorno = acoesOs.consultarEventosOs(this.strNrSeqOs);
	        System.out.println("retorno consultarEventosOs"+retorno);
		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
        JSONObject jsonRetorno = new JSONObject(retorno);
        JSONArray pcursor = jsonRetorno.getJSONArray("PCURSOR");
        for(int i = 0; i < pcursor.length(); i++) {
        	JSONObject curr = pcursor.getJSONObject(i);
        	objRsEventosOs.setNM_EVENTO(curr.isNull("NM_EVENTO") ? "" : curr.getString("NM_EVENTO"));
    		objRsEventosOs.setNM_CANAL(curr.isNull("NM_CANAL") ? "" : curr.getString("NM_CANAL"));
    		objRsEventosOs.setNM_OPERACAO(curr.isNull("NM_OPERACAO") ? "" : curr.getString("NM_OPERACAO"));
    		objRsEventosOs.setVALOR_OPERACAO(curr.isNull("VALOR_OPERACAO") ? "" : curr.getString("VALOR_OPERACAO"));        	      	    
        }		
	}
	
	public void setupObjRsEqpAtrbOs() {
		String retorno = "";
		try {
			retorno = cadOsEquipAtrib.consultarEquipeAtribuidaOs(this.strNrSeqOs);
	        System.out.println("retorno consultarEquipeAtribuidaOs"+retorno);
		} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsEquipAtrib.WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
        JSONObject jsonRetorno = new JSONObject(retorno);
        JSONArray pcursor = jsonRetorno.getJSONArray("PCURSOR");
        for(int i = 0; i < pcursor.length(); i++) {
        	JSONObject curr = pcursor.getJSONObject(i);
        	PrintObjRsEqpAtrbOs obj = new PrintObjRsEqpAtrbOs();
    		obj.setMATRICULA(curr.isNull("MATRICULA") ? "" : curr.getString("MATRICULA"));
    		obj.setNOME(curr.isNull("NOME") ? "" : curr.getString("NOME"));
    		obj.setCARGO(curr.isNull("CARGO") ? "" : curr.getString("CARGO"));
    		obj.setNM_DESIGNACAO(curr.isNull("NM_DESIGNACAO") ? "" : curr.getString("NM_DESIGNACAO"));
    		this.objRsEqpAtrbOs.add(obj);
        }		
	}
	
	public void setupObjRsCadOsArqAnex() {
		String retorno = "";		
		try {
			retorno = cadOsInfGerais.consultarOSArqAnex(this.strNrSeqOs);
			System.out.println("retorno consultarOSArqAnex"+retorno);
		} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsInfGerais.WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
        JSONObject jsonRetorno = new JSONObject(retorno);
        JSONArray pcursor = jsonRetorno.getJSONArray("PCURSOR");
        for(int i = 0; i < pcursor.length(); i++) {
        	JSONObject curr = pcursor.getJSONObject(i);
        	PrintObjRsCadOSArqAnex obj = new PrintObjRsCadOSArqAnex();
        	obj.setNM_CAMI_ARQU_ANEX(curr.isNull("NM_CAMI_ARQU_ANEX") ? "" : curr.getString("NM_CAMI_ARQU_ANEX"));
        	this.objRsCadOSArqAnex.add(obj);
        }
	}

	public void setupObjRsUnidEnvolv() {
		String retorno = "";
		try {
			retorno = cadOsUnidEnvolv.consultarUnidEnvolvOs(this.strNrSeqOs);
			System.out.println("retorno consultarUnidEnvolvOs"+retorno);
		} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsUnidEnvol.WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	
        JSONObject jsonRetorno = new JSONObject(retorno);
        JSONArray pcursor = jsonRetorno.getJSONArray("PCURSOR");
        this.renderRsUnidEnvolv = pcursor.length() > 0;
        for(int i = 0; i < pcursor.length(); i++) {
        	JSONObject curr = pcursor.getJSONObject(i);
    		objRsUnidEnvolv.setCD_UOR(curr.isNull("CD_UOR") ? "" : String.valueOf(curr.getInt("CD_UOR")));
    		objRsUnidEnvolv.setTP_UOR(curr.isNull("TP_UOR") ? "" : String.valueOf(curr.getInt("TP_UOR")));
    		objRsUnidEnvolv.setNM_REDE(curr.isNull("NM_REDE") ? "" : curr.getString("NM_REDE"));
    		objRsUnidEnvolv.setNM_REGI(curr.isNull("NM_REGI") ? "" : curr.getString("NM_REGI"));
    		objRsUnidEnvolv.setNM_UOR(curr.isNull("NM_UOR") ? "" : curr.getString("NM_UOR"));
//    		System.out.println("NM_REDE "+objRsUnidEnvolv.getNM_REDE());
//    		System.out.println("NM_REGI "+objRsUnidEnvolv.getNM_REGI());
//    		System.out.println("NM_OUR "+objRsUnidEnvolv.getNM_UOR());
//    		System.out.println("----------------------json-----------------------");
//    		System.out.println("NM_REDE "+curr.getString("NM_REDE"));
//    		System.out.println("NM_REGI "+curr.getString("NM_REGI"));
//    		System.out.println("NM_OUR "+curr.getString("NM_UOR"));    		
        }
	}
	
	public void setupObjRsClienteBanco(Integer intTipo) {
		String retorno = this.getConsultarClienteImp(intTipo);
	
        JSONObject jsonRetorno = new JSONObject(retorno);
        JSONArray pcursor = jsonRetorno.getJSONArray("PCURSOR");
        System.out.println("renderRS "+(pcursor.length() > 0));
        for(int i = 0; i < pcursor.length(); i++) {
        	JSONObject curr = pcursor.getJSONObject(i);
        	PrintObjRs obj = new PrintObjRs();
        	obj.setTP_PESSOA(curr.isNull("TP_PESSOA") ? "" : curr.getString("TP_PESSOA"));
        	obj.setSEGMENTO(curr.isNull("SEGMENTO") ? "" : curr.getString("SEGMENTO"));
        	obj.setPARTICIPACAO(curr.isNull("PARTICIPACAO") ? "" : curr.getString("PARTICIPACAO"));
        	obj.setNOME(curr.isNull("NOME") ? "" : curr.getString("NOME"));
        	obj.setMATRICULA(curr.isNull("MATRICULA") ? "" : curr.getString("MATRICULA"));
        	obj.setCPF_CNPJ(curr.isNull("CPF_CNPJ") ? "" : curr.getString("CPF_CNPJ"));
        	obj.setCcorrente(curr.isNull("CCORRENTE") ? "" : String.valueOf(curr.getInt("CCORRENTE")));
        	obj.setCARGO(curr.isNull("CARGO") ? "" : curr.getString("CARGO"));
        	obj.setAgencia(curr.isNull("AGENCIA") ? "" : String.valueOf(curr.getInt("AGENCIA")));
        	this.objRsClienteBanco.add(obj);
        }		
	}

	public void setupObjRsParticipProcesso(Integer intTipo) {
		String retorno = this.getConsultarClienteImp(intTipo);
		
        JSONObject jsonRetorno = new JSONObject(retorno);
        JSONArray pcursor = jsonRetorno.getJSONArray("PCURSOR");
        System.out.println("renderRS "+(pcursor.length() > 0));
        for(int i = 0; i < pcursor.length(); i++) {
        	JSONObject curr = pcursor.getJSONObject(i);
        	PrintObjRs obj = new PrintObjRs();
        	obj.setTP_PESSOA(curr.isNull("TP_PESSOA") ? "" : curr.getString("TP_PESSOA"));
        	obj.setSEGMENTO(curr.isNull("SEGMENTO") ? "" : curr.getString("SEGMENTO"));
        	obj.setPARTICIPACAO(curr.isNull("PARTICIPACAO") ? "" : curr.getString("PARTICIPACAO"));
        	obj.setNOME(curr.isNull("NOME") ? "" : curr.getString("NOME"));
        	obj.setMATRICULA(curr.isNull("MATRICULA") ? "" : curr.getString("MATRICULA"));
        	obj.setCPF_CNPJ(curr.isNull("CPF_CNPJ") ? "" : curr.getString("CPF_CNPJ"));
        	obj.setCcorrente(curr.isNull("CCORRENTE") ? "" : String.valueOf(curr.getInt("CCORRENTE")));
        	obj.setCARGO(curr.isNull("CARGO") ? "" : curr.getString("CARGO"));
        	obj.setAgencia(curr.isNull("AGENCIA") ? "" : String.valueOf(curr.getInt("AGENCIA")));
        	this.objRsParticipProcesso.add(obj);
        }		

	}
	
	public String getConsultarClienteImp(Integer intTipo) {
		String retorno = "";
		try {
			retorno = cadOsParticip.consultarClienteImp(this.strNrSeqOs, intTipo);
			System.out.println("retorno consultarClienteImp"+retorno);
		} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsParticip.WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return retorno;
	}
	
	@PostConstruct
	public void init() {
		this.strNrSeqOs = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs");
		System.out.println("strNrSeqOs "+this.strNrSeqOs);
		
		//ConsultarClienteImp
		objRsClienteBanco = new ArrayList<PrintObjRs>();
		setupObjRsClienteBanco(2);
		
		//ConsultarClienteImp		
		objRsParticipProcesso  = new ArrayList<PrintObjRs>();
		setupObjRsParticipProcesso(1);				
		
		//ConsultarOs
		objRsOs = new PrintObjRsOs();
		setupObjRsOs();
		
		//ConsultarEventosOs
		objRsEventosOs = new PrintObjRsEventosOs();
		setupObjRsEventosOs();
		
		//ConsultarEquipeAtribuidaOs		
		objRsEqpAtrbOs = new ArrayList<PrintObjRsEqpAtrbOs>();
		setupObjRsEqpAtrbOs();
	
		//ConsultarOSArqAnex
		objRsCadOSArqAnex = new ArrayList<PrintObjRsCadOSArqAnex>();
		setupObjRsCadOsArqAnex();
		
		objRsUnidEnvolv = new PrintObjRsUnidEnvolv();
		setupObjRsUnidEnvolv();
		
	}

	public PrintObjRsOs getObjRsOs() {
		return objRsOs;
	}

	public PrintObjRsEventosOs getObjRsEventosOs() {
		return objRsEventosOs;
	}

	public List<PrintObjRsEqpAtrbOs> getObjRsEqpAtrbOs() {
		return objRsEqpAtrbOs;
	}

	public List<PrintObjRsCadOSArqAnex> getObjRsCadOSArqAnex() {
		return objRsCadOSArqAnex;
	}

	public PrintObjRsUnidEnvolv getObjRsUnidEnvolv() {
		return objRsUnidEnvolv;
	}
	
	public Integer randInt(Integer inicio, Integer fim) {
		return inicio + (int)Math.round(Math.random() * (fim - inicio));
	}
    
    public String getStrNrSeqOs() {
		return this.strNrSeqOs;
	}

	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}
	
	public String getStrBgColor() {
		return strBgColor;
	}

	public void setStrBgColor(String strBgColor) {
		this.strBgColor = strBgColor;
	}

	public Date randomDate() {
        GregorianCalendar gc = new GregorianCalendar();

        gc.set(gc.YEAR, randInt(2000, 2010));
        gc.set(gc.DAY_OF_YEAR, randInt(1, gc.getActualMaximum(gc.DAY_OF_YEAR)));
        gc.set(gc.HOUR, randInt(00, 23));
        gc.set(gc.MINUTE, randInt(0, 60));
        gc.set(gc.SECOND, randInt(0, 60));
        return gc.getTime();
	}
	
//	public String CarregaPV() {
//		//if objRsUnidEnvolv.RecordCount < 1 then
//        //exit function
//		//end if
//
//		strCdUor=objRsUnidEnvolv.getCD_UOR();
//		switch (strCdUor.length()) {
//			case 1:
//				strCdUor="000"+strCdUor;
//				break;
//			case 2:
//				strCdUor="00"+strCdUor;
//				break;
//			case 3:
//				strCdUor="0"+strCdUor;
//				break;
//		}
//		
//		strTpUor=objRsUnidEnvolv.getTP_UOR();
//		switch (strTpUor.length()) {
//			case 1:
//				strTpUor="000"+strTpUor;
//				break;
//			case 2:
//				strTpUor="00"+strTpUor;
//				break;
//			case 3:
//				strTpUor="0"+strTpUor;
//				break;
//		}
//	
//	    strRetorno = "";
//	    System.out.println("TP_UOR "+objRsUnidEnvolv.getTP_UOR());
//		if (objRsUnidEnvolv.getTP_UOR().trim() == "1") { //se for agencia
//			strRetorno += "<tr height=\"20\" style=\"background:gray; color:White; font-weight:bold\" >"
//					+ "<td width=\"20%\" >Rede</td>"
//					+ "<td width=\"40%\">Regional</td>"
//					+ "<td>PV</td>"
//					+ "</tr>"
//					+ "<tr>"
//					+ "<td>&nbsp;" + objRsUnidEnvolv.getNM_REDE() + "</td>"
//					+ "<td>" + objRsUnidEnvolv.getNM_REGI() + "</td>"
//					+ "<td><b>" + strCdUor + " - " +objRsUnidEnvolv.getNM_UOR() + "</b></td>"
//					+ "</tr>";
//		}
//		if (objRsUnidEnvolv.getTP_UOR().trim() != "1") { //se for diferente de agencia
//			strRetorno += "<tr height=\"20\" style=\"background:gray; color:White; font-weight:bold\" >"
//					+ "<td width=\"20%\">&nbsp;Nome Depto</td>"
//					+ "</tr>"
//					+ "<tr>"
//					+ "<td><b>" + strCdUor + " - " +objRsUnidEnvolv.getNM_UOR() + "</b></td>"
//					+ "</tr>";
//		}
//		
//		return strRetorno;
//	}
	
//	public String FillTblPart(int intTipo) {
//		//vai percorer todos os registros do objeto objRs
//		setupObjRs(intTipo);
//		
//		for(int i = 0; i < objRs.size(); i++) {
//	        if (strBgColor == "#FFFFFF")
//	            strBgColor = "#E8E8E8";
//	        else
//	            strBgColor = "#FFFFFF";
//		
//		    strRetorno = "";
//		            
//	        if (intTipo == 1) { //Colaboradores
//	            strRetorno += "<tr height=\"20\" style=\"background:" + strBgColor + "\"><td><b>" + objRs.get(i).getTP_PESSOA() + "</b></td>"
//	            		+ "<td>" + objRs.get(i).getMATRICULA() + "</td>"
//			            + "<td>" + objRs.get(i).getNOME() + "</td>"
//			            + "<td>" + objRs.get(i).getCARGO() + "</td>"
//			            + "<td>" + objRs.get(i).getCPF_CNPJ() + "</td>"
//			            + "<td>" + objRs.get(i).getPARTICIPACAO() + "</td>";
//	        } else { //Clientes
//	        	String agencia;
//				if (objRs.get(i).getAgencia() == null) {
//					agencia = "";
//				} else {
//					agencia="0000" + objRs.get(i).getAgencia();
//					agencia= agencia.substring(agencia.length() - 4) + "/" + objRs.get(i).getCcorrente();
//	        	}
//	
//	            strRetorno += "<tr height=\"20\" style=\"background:" + strBgColor + "\"><td><b>" + objRs.get(i).getTP_PESSOA() + "</b></td>"
//	            		+ "<td>" + objRs.get(i).getCPF_CNPJ() + "</td>"
//			            + "<td>" + objRs.get(i).getNOME() + "</td>"
//			            + "<td>" + agencia + "</td>"
//			            + "<td>" + objRs.get(i).getSEGMENTO() + "</td>"
//			            + "<td>" + objRs.get(i).getPARTICIPACAO() + "</td>";
//	        }
//		}
//		
//		return strRetorno;
//	}

	public Boolean getRenderRsUnidEnvolv() {
		return renderRsUnidEnvolv;
	}

	public void setRenderRsUnidEnvolv(Boolean renderRsUnidEnvolv) {
		this.renderRsUnidEnvolv = renderRsUnidEnvolv;
	}

	public List<PrintObjRs> getObjRsClienteBanco() {
		return objRsClienteBanco;
	}

	public void setObjRsClienteBanco(List<PrintObjRs> objRsClienteBanco) {
		this.objRsClienteBanco = objRsClienteBanco;
	}

	public List<PrintObjRs> getObjRsParticipProcesso() {
		return objRsParticipProcesso;
	}

	public void setObjRsParticipProcesso(List<PrintObjRs> objRsParticipProcesso) {
		this.objRsParticipProcesso = objRsParticipProcesso;
	}
	
}