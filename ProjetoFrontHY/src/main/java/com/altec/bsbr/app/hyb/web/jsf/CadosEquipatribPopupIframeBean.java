package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.annotation.PostConstruct;

import org.apache.commons.collections.set.SynchronizedSet;
import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.altec.bsbr.app.hyb.dto.CadosEquipatribPopup;
import com.altec.bsbr.app.hyb.web.util.CadOsEquipatribPopupIframe;
import com.altec.bsbr.app.hyb.web.util.VerificaErroUtil;

//@Component("CadosEquipatribPopupIframeBean")
@Component("CadosEquipatribPopupIframeBean")
@Scope("session")
public class CadosEquipatribPopupIframeBean extends BasicBBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<CadosEquipatribPopup> objRsCadosEquipatribPopupIframe;

	CadosEquipatribPopup cadOsEquipaTribPopup = new CadosEquipatribPopup();

	CadOsEquipatribPopupIframe objCadOs = new CadOsEquipatribPopupIframe();
	
	public CadosEquipatribPopupIframeBean() throws IOException {
		
		objRsCadosEquipatribPopupIframe = objCadOs.ConsultarRecurso(cadOsEquipaTribPopup.getStrNrSeqOs(),
		cadOsEquipaTribPopup.getStrCpf(), cadOsEquipaTribPopup.getStrMatricula(),
		cadOsEquipaTribPopup.getStrNome(), cadOsEquipaTribPopup.getStrCdArea());
		cadOsEquipaTribPopup.setStrCdArea("valor vindo do inc usuario");
		

		// block to get params from url
		FacesContext fc = FacesContext.getCurrentInstance();
		Map<String, String> params = fc.getExternalContext().getRequestParameterMap();

		String strAction = params.get("strAction");

	if(strAction != null) {
		if(strAction.equals("P")){

			objRsCadosEquipatribPopupIframe = objCadOs.ConsultarRecurso(cadOsEquipaTribPopup.getStrNrSeqOs(),
			cadOsEquipaTribPopup.getStrCpf(), cadOsEquipaTribPopup.getStrMatricula(),
			cadOsEquipaTribPopup.getStrNome(), cadOsEquipaTribPopup.getStrCdArea());
			
			System.out.println(objRsCadosEquipatribPopupIframe.size());
			
			
		}else if(strAction.equals("A")){
			System.out.println("A");
		}else {
			objRsCadosEquipatribPopupIframe = null;
		}
	}else {
		objRsCadosEquipatribPopupIframe = null;
	}
		  
		//cadOsEquipaTribPopup.setStrAction(strAction);
       
         
         
         
//		
//		objRsCadosEquipatribPopupIframe = objCadOs.ConsultarRecurso(cadOsEquipaTribPopup.getStrNrSeqOs(),
//				cadOsEquipaTribPopup.getStrCpf(), cadOsEquipaTribPopup.getStrMatricula(),
//				cadOsEquipaTribPopup.getStrNome(), cadOsEquipaTribPopup.getStrCdArea());
//		

//		if (cadOsEquipaTribPopup.getStrAction() == "P" || cadOsEquipaTribPopup.getStrAction() == "A") {
//
//			if (cadOsEquipaTribPopup.getStrAction() == "A") {
//
//				String[] arrAddCpf = cadOsEquipaTribPopup.getTxtHdAddParam().trim().split("|");
//
//				for (int intCont = 0; intCont <= arrAddCpf.length - 1; intCont++) {
//					String retornoAdiciona = objCadOs.AdicionarRecurso(cadOsEquipaTribPopup.getStrNrSeqOs(),
//							arrAddCpf(intCont));
//					if (!retornoAdiciona.isEmpty()) {
//						VerificaErroUtil.verificaErro(retornoAdiciona);
//					}
//				}
//				RequestContext.getCurrentInstance().execute("alert('Recurso(s) atribuído(s) com sucesso!');");
//
//			}
//
//		}

	}

	public void PesquisarRecurso() throws IOException {
		
	
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		ec.redirect("hyb_cados_equipatrib_popup_iframe.xhtml?strAction=P");
		

	
	

	}
	
	public void AdicionarRecurso() throws IOException{
		RequestContext.getCurrentInstance().execute("alert('Recurso adicionado com sucesso');");
	}

	private Object arrAddCpf(int intCont) {

		return null;
	}

	public List<CadosEquipatribPopup> getobjRsCadosEquipatribPopupIframe() {
		return objRsCadosEquipatribPopupIframe;
	}

	public void setObjRsCadOsAcoesCompl(List<CadosEquipatribPopup> objRsCadosEquipatribPopupIframe) {
		this.objRsCadosEquipatribPopupIframe = objRsCadosEquipatribPopupIframe;
	}
	
	public String getStrCpf() {
		return cadOsEquipaTribPopup.getStrCpf();
	}

	public String getStrNome() {
		return cadOsEquipaTribPopup.getStrNome();
	}

	public String getStrMatricula() {
		return cadOsEquipaTribPopup.getStrMatricula();
	}
	
	public String getTxtHdAction() {
		return cadOsEquipaTribPopup.getTxtHdAction();
	}
	
	public void setTxtHdAction(String txtHdAction) {
		cadOsEquipaTribPopup.setTxtHdAction(txtHdAction);
	}

	public void setStrCpf(String strCpf) {
		cadOsEquipaTribPopup.setStrCpf(strCpf);
	}

	public void setStrNome(String strNome) {
		cadOsEquipaTribPopup.setStrNome(strNome);
	}

	public void setStrMatricula(String strMatricula) {
		cadOsEquipaTribPopup.setStrMatricula(strMatricula);
	}



}
