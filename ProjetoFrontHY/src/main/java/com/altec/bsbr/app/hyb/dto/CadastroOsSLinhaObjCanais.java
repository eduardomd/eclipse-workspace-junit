package com.altec.bsbr.app.hyb.dto;

public class CadastroOsSLinhaObjCanais {
	
	private String NR_SEQU_TIPO_OPER_TRAN;
	private String CD_TRAN;
	private String NM_OPER;
	private String NM_SUB_OPER;
	private String CD_PROD_AUXI;
	private String NM_PROD_AUXI;
	private String VL_TRAN;
	private String TP_CNTE;
	private String DT_TRAN;
	private String HR_TRAN;
	private String DT_OCOR_FIXA;
	private String DT_OCOR_PERI_INIC;
	private String DT_OCOR_PERI_FINA;
	private String CARD;
	private String NR_SEQU_TRAN_FRAU_CNAL;
	private String DESC_TRAN;
	private String DESC_CNTE;
	
	private String strDtOper;
	private String strHrOper;
	private String strVlOper;
	
	public CadastroOsSLinhaObjCanais(String nR_SEQU_TIPO_OPER_TRAN, String cD_TRAN, String nM_OPER, String nM_SUB_OPER,
			String cD_PROD_AUXI, String nM_PROD_AUXI, String vL_TRAN, String tP_CNTE, String dT_TRAN, String hR_TRAN,
			String dT_OCOR_FIXA, String dT_OCOR_PERI_INIC, String dT_OCOR_PERI_FINA, String cARD,
			String nR_SEQU_TRAN_FRAU_CNAL, String dESC_TRAN, String dESC_CNTE, String strDtOper, String strHrOper,
			String strVlOper) {
		super();
		NR_SEQU_TIPO_OPER_TRAN = nR_SEQU_TIPO_OPER_TRAN;
		CD_TRAN = cD_TRAN;
		NM_OPER = nM_OPER;
		NM_SUB_OPER = nM_SUB_OPER;
		CD_PROD_AUXI = cD_PROD_AUXI;
		NM_PROD_AUXI = nM_PROD_AUXI;
		VL_TRAN = vL_TRAN;
		TP_CNTE = tP_CNTE;
		DT_TRAN = dT_TRAN;
		HR_TRAN = hR_TRAN;
		DT_OCOR_FIXA = dT_OCOR_FIXA;
		DT_OCOR_PERI_INIC = dT_OCOR_PERI_INIC;
		DT_OCOR_PERI_FINA = dT_OCOR_PERI_FINA;
		CARD = cARD;
		NR_SEQU_TRAN_FRAU_CNAL = nR_SEQU_TRAN_FRAU_CNAL;
		DESC_TRAN = dESC_TRAN;
		DESC_CNTE = dESC_CNTE;
		this.strDtOper = strDtOper;
		this.strHrOper = strHrOper;
		this.strVlOper = strVlOper;
	}
	public String getNR_SEQU_TIPO_OPER_TRAN() {
		return NR_SEQU_TIPO_OPER_TRAN;
	}
	public void setNR_SEQU_TIPO_OPER_TRAN(String nR_SEQU_TIPO_OPER_TRAN) {
		NR_SEQU_TIPO_OPER_TRAN = nR_SEQU_TIPO_OPER_TRAN;
	}
	public String getCD_TRAN() {
		return CD_TRAN;
	}
	public void setCD_TRAN(String cD_TRAN) {
		CD_TRAN = cD_TRAN;
	}
	public String getNM_OPER() {
		return NM_OPER;
	}
	public void setNM_OPER(String nM_OPER) {
		NM_OPER = nM_OPER;
	}
	public String getNM_SUB_OPER() {
		return NM_SUB_OPER;
	}
	public void setNM_SUB_OPER(String nM_SUB_OPER) {
		NM_SUB_OPER = nM_SUB_OPER;
	}
	public String getCD_PROD_AUXI() {
		return CD_PROD_AUXI;
	}
	public void setCD_PROD_AUXI(String cD_PROD_AUXI) {
		CD_PROD_AUXI = cD_PROD_AUXI;
	}
	public String getNM_PROD_AUXI() {
		return NM_PROD_AUXI;
	}
	public void setNM_PROD_AUXI(String nM_PROD_AUXI) {
		NM_PROD_AUXI = nM_PROD_AUXI;
	}
	public String getVL_TRAN() {
		return VL_TRAN;
	}
	public void setVL_TRAN(String vL_TRAN) {
		VL_TRAN = vL_TRAN;
	}
	public String getTP_CNTE() {
		return TP_CNTE;
	}
	public void setTP_CNTE(String tP_CNTE) {
		TP_CNTE = tP_CNTE;
	}
	public String getDT_TRAN() {
		return DT_TRAN;
	}
	public void setDT_TRAN(String dT_TRAN) {
		DT_TRAN = dT_TRAN;
	}
	public String getHR_TRAN() {
		return HR_TRAN;
	}
	public void setHR_TRAN(String hR_TRAN) {
		HR_TRAN = hR_TRAN;
	}
	public String getDT_OCOR_FIXA() {
		return DT_OCOR_FIXA;
	}
	public void setDT_OCOR_FIXA(String dT_OCOR_FIXA) {
		DT_OCOR_FIXA = dT_OCOR_FIXA;
	}
	public String getDT_OCOR_PERI_INIC() {
		return DT_OCOR_PERI_INIC;
	}
	public void setDT_OCOR_PERI_INIC(String dT_OCOR_PERI_INIC) {
		DT_OCOR_PERI_INIC = dT_OCOR_PERI_INIC;
	}
	public String getDT_OCOR_PERI_FINA() {
		return DT_OCOR_PERI_FINA;
	}
	public void setDT_OCOR_PERI_FINA(String dT_OCOR_PERI_FINA) {
		DT_OCOR_PERI_FINA = dT_OCOR_PERI_FINA;
	}
	public String getCARD() {
		return CARD;
	}
	public void setCARD(String cARD) {
		CARD = cARD;
	}
	public String getNR_SEQU_TRAN_FRAU_CNAL() {
		return NR_SEQU_TRAN_FRAU_CNAL;
	}
	public void setNR_SEQU_TRAN_FRAU_CNAL(String nR_SEQU_TRAN_FRAU_CNAL) {
		NR_SEQU_TRAN_FRAU_CNAL = nR_SEQU_TRAN_FRAU_CNAL;
	}
	public String getDESC_TRAN() {
		return DESC_TRAN;
	}
	public void setDESC_TRAN(String dESC_TRAN) {
		DESC_TRAN = dESC_TRAN;
	}
	public String getDESC_CNTE() {
		return DESC_CNTE;
	}
	public void setDESC_CNTE(String dESC_CNTE) {
		DESC_CNTE = dESC_CNTE;
	}
	
	public String getStrDtOper() {
		return strDtOper;
	}

	public void setStrDtOper(String strDtOper) {
		this.strDtOper = strDtOper;
	}

	public String getStrHrOper() {
		return strHrOper;
	}

	public void setStrHrOper(String strHrOper) {
		this.strHrOper = strHrOper;
	}

	public String getStrVlOper() {
		return strVlOper;
	}

	public void setStrVlOper(String strVlOper) {
		this.strVlOper = strVlOper;
	}

	public CadastroOsSLinhaObjCanais() {
		
	}

}
