package com.altec.bsbr.app.hyb.web.jsf;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.ObjRsOs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


@Component("hybAprovarRelPropIframeBean")
@Scope("session")
public class HybAprovarRelPropIframeBean extends BasicBBean {
	private static final long serialVersionUID = 1L;
	private ObjRsOs objRsOs;
	private String strNrSeqOs;
	
	@PostConstruct
		public void init() {
		System.out.println(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs"));
		this.strNrSeqOs = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs");
	}
	
	public ObjRsOs getObjRsOs() {
		objRsOs = new ObjRsOs();
		objRsOs.setAREA("AREA");
		objRsOs.setCD_NOTI("4010");
		objRsOs.setCODIGO("1234");
		objRsOs.setDT_ABERTURA("14/12/2018");
		objRsOs.setDT_RELATORIO("15/12/2018");
		objRsOs.setNM_NOTI("NM_NOTI");
		objRsOs.setPARECER_JURI("PARECER_JURI");
		objRsOs.setSITUACAO("SITUACAO");
		objRsOs.setTX_ABERTURA("TX_ABERTURA");
		objRsOs.setTX_ENCERRAMENTO("");
		objRsOs.setTX_FALHAS("TX_FALHAS");
		objRsOs.setTX_OUTRAS_PROP("TX_OUTRAS_PROP");
		objRsOs.setVL_ENVOLVIDO("1000.00");
		objRsOs.setVL_PREJUIZO("400.00");
		objRsOs.setVL_RECUPERADO("600.00");
		return objRsOs;
	}
	
	public void recusar() {
		System.out.println(objRsOs.getTX_ENCERRAMENTO());
		//TODO: fazer integra��o com backend
		RequestContext.getCurrentInstance().execute("alert(\"O Relat�rio de Proposi��o da Ordem de Servi�o n� " + this.strNrSeqOs + " foi recusado! \\n A notifica��o foi enviada aos respons�veis!\")");
	}

	public String getStrNrSeqOs() {
		return strNrSeqOs;
	}

	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}
}
