package com.altec.bsbr.app.hyb.dto;

public class CadastroOsCDebitoModel {

	private String strNrSeqOs;
	private String strSeqEvento;
	private String strTitular;
	private String strNrTitular;
	private String strSeqConta;
	private String strSeqCartao;
	private String strAgencia; // IIf(Trim(objRsEvento("CD_BANC")) <> Empty, Right("0000" & objRsEvento("CD_BANC"), 4), "")
	private String strConta;
	private String strCartao;
	private String strTpConta; // CInt("0" & objRsEvento("TP_CNTA_BNCR"))
	private String strCdCanal;
	private String strCPF_CNPJ;
	private String strDtEmissao;
	private String strDtAtivacao;
	private String strDtCancel;
	private String strLoginColab;
	private String strDtAbertura;
	private String strNmGerente;
	private String strStatusCnt;
	private String strEndEnviado;
	private String blnInteComl;
	private String blnClieAjud;
	private String strAgenClieAjud; // IIf(Trim(objRsEvento("cd_agen_ajda_terc")) <> Empty, Right("0000" & objRsEvento("cd_agen_ajda_terc"), 4), "")
	private String blnCrtoRece;
	private String blnCrtoReti;
	private String blnCrtoSegr;
	private String strDtSoliCrto;
	private String strDtCancCrto;
	private String strDtAtivCrto;
	private String strDtFrauCrto;
	private String strLocaAtivCrto;
	private String intFragLocal;
	private String strFragNmAgen;
	private String strFragDtOcorr;
	private String strFragNrTerm;
	private String blnFragSensor;
	private String strFragOutros;
	private String intFragClafFrau;
	private String strNrQntdEven;
	private String strCdCentroOrigem;
	private String strCdCentroDestino;
	private String strCdCentroOperante;
	private String strContabilizado;
	private String strFragHrInicio;
	private String strFragHrFinal;
	private String strFragCodAgen;
		
	
	public CadastroOsCDebitoModel(String strNrSeqOs, String strSeqEvento, String strTitular, String strNrTitular,
			String strSeqConta, String strSeqCartao, String strAgencia, String strConta, String strCartao,
			String strTpConta, String strCdCanal, String strCPF_CNPJ, String strDtEmissao, String strDtAtivacao,
			String strDtCancel, String strLoginColab, String strDtAbertura, String strNmGerente, String strStatusCnt,
			String strEndEnviado, String blnInteComl, String blnClieAjud, String strAgenClieAjud, String blnCrtoRece,
			String blnCrtoReti, String blnCrtoSegr, String strDtSoliCrto, String strDtCancCrto, String strDtAtivCrto,
			String strDtFrauCrto, String strLocaAtivCrto, String intFragLocal, String strFragNmAgen, String strFragDtOcorr,
			String strFragNrTerm, String blnFragSensor, String strFragOutros, String intFragClafFrau,
			String strNrQntdEven, String strCdCentroOrigem, String strCdCentroDestino, String strCdCentroOperante,
			String strContabilizado) {
		super();
		this.strNrSeqOs = strNrSeqOs;
		this.strSeqEvento = strSeqEvento;
		this.strTitular = strTitular;
		this.strNrTitular = strNrTitular;
		this.strSeqConta = strSeqConta;
		this.strSeqCartao = strSeqCartao;
		this.strAgencia = strAgencia;
		this.strConta = strConta;
		this.strCartao = strCartao;
		this.strTpConta = strTpConta;
		this.strCdCanal = strCdCanal;
		this.strCPF_CNPJ = strCPF_CNPJ;
		this.strDtEmissao = strDtEmissao;
		this.strDtAtivacao = strDtAtivacao;
		this.strDtCancel = strDtCancel;
		this.strLoginColab = strLoginColab;
		this.strDtAbertura = strDtAbertura;
		this.strNmGerente = strNmGerente;
		this.strStatusCnt = strStatusCnt;
		this.strEndEnviado = strEndEnviado;
		this.blnInteComl = blnInteComl;
		this.blnClieAjud = blnClieAjud;
		this.intFragLocal = intFragLocal;
		this.strAgenClieAjud = strAgenClieAjud;
		this.blnCrtoRece = blnCrtoRece;
		this.blnCrtoReti = blnCrtoReti;
		this.blnCrtoSegr = blnCrtoSegr;
		this.strDtSoliCrto = strDtSoliCrto;
		this.strDtCancCrto = strDtCancCrto;
		this.strDtAtivCrto = strDtAtivCrto;
		this.strDtFrauCrto = strDtFrauCrto;
		this.strLocaAtivCrto = strLocaAtivCrto;
		this.strFragNmAgen = strFragNmAgen;
		this.strFragDtOcorr = strFragDtOcorr;
		this.strFragNrTerm = strFragNrTerm;
		this.blnFragSensor = blnFragSensor;
		this.strFragOutros = strFragOutros;
		this.intFragClafFrau = intFragClafFrau;
		this.strNrQntdEven = strNrQntdEven;
		this.strCdCentroOrigem = strCdCentroOrigem;
		this.strCdCentroDestino = strCdCentroDestino;
		this.strCdCentroOperante = strCdCentroOperante;
		this.strContabilizado = strContabilizado;
		
		
	}	
	
	public String getStrFragHrInicio() {
		return strFragHrInicio;
	}
	public void setStrFragHrInicio(String strFragHrInicio) {
		this.strFragHrInicio = strFragHrInicio;
	}
	public String getStrFragHrFinal() {
		return strFragHrFinal;
	}
	public void setStrFragHrFinal(String strFragHrFinal) {
		this.strFragHrFinal = strFragHrFinal;
	}
	public String getStrFragCodAgen() {
		return strFragCodAgen;
	}
	public void setStrFragCodAgen(String strFragCodAgen) {
		this.strFragCodAgen = strFragCodAgen;
	}
	public String getIntFragLocal() {
		return intFragLocal;
	}
	public void setIntFragLocal(String intFragLocal) {
		this.intFragLocal = intFragLocal;
	}
	public String getStrNrSeqOs() {
		return strNrSeqOs;
	}
	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}
	public String getStrSeqEvento() {
		return strSeqEvento;
	}
	public void setStrSeqEvento(String strSeqEvento) {
		this.strSeqEvento = strSeqEvento;
	}
	public String getStrTitular() {
		return strTitular;
	}
	public void setStrTitular(String strTitular) {
		this.strTitular = strTitular;
	}
	public String getStrNrTitular() {
		return strNrTitular;
	}
	public void setStrNrTitular(String strNrTitular) {
		this.strNrTitular = strNrTitular;
	}
	public String getStrSeqConta() {
		return strSeqConta;
	}
	public void setStrSeqConta(String strSeqConta) {
		this.strSeqConta = strSeqConta;
	}
	public String getStrSeqCartao() {
		return strSeqCartao;
	}
	public void setStrSeqCartao(String strSeqCartao) {
		this.strSeqCartao = strSeqCartao;
	}
	public String getStrAgencia() {
		return strAgencia;
	}
	public void setStrAgencia(String strAgencia) {
		this.strAgencia = strAgencia;
	}
	public String getStrConta() {
		return strConta;
	}
	public void setStrConta(String strConta) {
		this.strConta = strConta;
	}
	public String getStrCartao() {
		return strCartao;
	}
	public void setStrCartao(String strCartao) {
		this.strCartao = strCartao;
	}
	public String getStrTpConta() {
		return strTpConta;
	}
	public void setStrTpConta(String strTpConta) {
		this.strTpConta = strTpConta;
	}
	public String getStrCdCanal() {
		return strCdCanal;
	}
	public void setStrCdCanal(String strCdCanal) {
		this.strCdCanal = strCdCanal;
	}
	public String getStrCPF_CNPJ() {
		return strCPF_CNPJ;
	}
	public void setStrCPF_CNPJ(String strCPF_CNPJ) {
		this.strCPF_CNPJ = strCPF_CNPJ;
	}
	public String getStrDtEmissao() {
		return strDtEmissao;
	}
	public void setStrDtEmissao(String strDtEmissao) {
		this.strDtEmissao = strDtEmissao;
	}
	public String getStrDtAtivacao() {
		return strDtAtivacao;
	}
	public void setStrDtAtivacao(String strDtAtivacao) {
		this.strDtAtivacao = strDtAtivacao;
	}
	public String getStrDtCancel() {
		return strDtCancel;
	}
	public void setStrDtCancel(String strDtCancel) {
		this.strDtCancel = strDtCancel;
	}
	public String getStrLoginColab() {
		return strLoginColab;
	}
	public void setStrLoginColab(String strLoginColab) {
		this.strLoginColab = strLoginColab;
	}
	public String getStrDtAbertura() {
		return strDtAbertura;
	}
	public void setStrDtAbertura(String strDtAbertura) {
		this.strDtAbertura = strDtAbertura;
	}
	public String getStrNmGerente() {
		return strNmGerente;
	}
	public void setStrNmGerente(String strNmGerente) {
		this.strNmGerente = strNmGerente;
	}
	public String getStrStatusCnt() {
		return strStatusCnt;
	}
	public void setStrStatusCnt(String strStatusCnt) {
		this.strStatusCnt = strStatusCnt;
	}
	public String getStrEndEnviado() {
		return strEndEnviado;
	}
	public void setStrEndEnviado(String strEndEnviado) {
		this.strEndEnviado = strEndEnviado;
	}
	public String getBlnInteComl() {
		return blnInteComl;
	}
	public void setBlnInteComl(String blnInteComl) {
		this.blnInteComl = blnInteComl;
	}
	public String getBlnClieAjud() {
		return blnClieAjud;
	}
	public void setBlnClieAjud(String blnClieAjud) {
		this.blnClieAjud = blnClieAjud;
	}
	public String getStrAgenClieAjud() {
		return strAgenClieAjud;
	}
	public void setStrAgenClieAjud(String strAgenClieAjud) {
		this.strAgenClieAjud = strAgenClieAjud;
	}
	public String getBlnCrtoRece() {
		return blnCrtoRece;
	}
	public void setBlnCrtoRece(String blnCrtoRece) {
		this.blnCrtoRece = blnCrtoRece;
	}
	public String getBlnCrtoReti() {
		return blnCrtoReti;
	}
	public void setBlnCrtoReti(String blnCrtoReti) {
		this.blnCrtoReti = blnCrtoReti;
	}
	public String getBlnCrtoSegr() {
		return blnCrtoSegr;
	}
	public void setBlnCrtoSegr(String blnCrtoSegr) {
		this.blnCrtoSegr = blnCrtoSegr;
	}
	public String getStrDtSoliCrto() {
		return strDtSoliCrto;
	}
	public void setStrDtSoliCrto(String strDtSoliCrto) {
		this.strDtSoliCrto = strDtSoliCrto;
	}
	public String getStrDtCancCrto() {
		return strDtCancCrto;
	}
	public void setStrDtCancCrto(String strDtCancCrto) {
		this.strDtCancCrto = strDtCancCrto;
	}
	public String getStrDtAtivCrto() {
		return strDtAtivCrto;
	}
	public void setStrDtAtivCrto(String strDtAtivCrto) {
		this.strDtAtivCrto = strDtAtivCrto;
	}
	public String getStrDtFrauCrto() {
		return strDtFrauCrto;
	}
	public void setStrDtFrauCrto(String strDtFrauCrto) {
		this.strDtFrauCrto = strDtFrauCrto;
	}
	public String getStrLocaAtivCrto() {
		return strLocaAtivCrto;
	}
	public void setStrLocaAtivCrto(String strLocaAtivCrto) {
		this.strLocaAtivCrto = strLocaAtivCrto;
	}
	public String getStrFragNmAgen() {
		return strFragNmAgen;
	}
	public void setStrFragNmAgen(String strFragNmAgen) {
		this.strFragNmAgen = strFragNmAgen;
	}
	public String getStrFragDtOcorr() {
		return strFragDtOcorr;
	}
	public void setStrFragDtOcorr(String strFragDtOcorr) {
		this.strFragDtOcorr = strFragDtOcorr;
	}
	public String getStrFragNrTerm() {
		return strFragNrTerm;
	}
	public void setStrFragNrTerm(String strFragNrTerm) {
		this.strFragNrTerm = strFragNrTerm;
	}
	public String getBlnFragSensor() {
		return blnFragSensor;
	}
	public void setBlnFragSensor(String blnFragSensor) {
		this.blnFragSensor = blnFragSensor;
	}
	public String getStrFragOutros() {
		return strFragOutros;
	}
	public void setStrFragOutros(String strFragOutros) {
		this.strFragOutros = strFragOutros;
	}
	public String getIntFragClafFrau() {
		return intFragClafFrau;
	}
	public void setIntFragClafFrau(String intFragClafFrau) {
		this.intFragClafFrau = intFragClafFrau;
	}
	public String getStrNrQntdEven() {
		return strNrQntdEven;
	}
	public void setStrNrQntdEven(String strNrQntdEven) {
		this.strNrQntdEven = strNrQntdEven;
	}
	public String getStrCdCentroOrigem() {
		return strCdCentroOrigem;
	}
	public void setStrCdCentroOrigem(String strCdCentroOrigem) {
		this.strCdCentroOrigem = strCdCentroOrigem;
	}
	public String getStrCdCentroDestino() {
		return strCdCentroDestino;
	}
	public void setStrCdCentroDestino(String strCdCentroDestino) {
		this.strCdCentroDestino = strCdCentroDestino;
	}
	public String getStrCdCentroOperante() {
		return strCdCentroOperante;
	}
	public void setStrCdCentroOperante(String strCdCentroOperante) {
		this.strCdCentroOperante = strCdCentroOperante;
	}
	public String getStrContabilizado() {
		return strContabilizado;
	}
	public void setStrContabilizado(String strContabilizado) {
		this.strContabilizado = strContabilizado;
	}
	
	
	
}
