package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.JCGG_TCMGEN1AeaResponse;
import com.altec.bsbr.app.hyb.dto.ObjRsCombo;
import com.altec.bsbr.app.hyb.dto.ObjRsParticip;
import com.altec.bsbr.app.hyb.dto.PE95_PEM9310B_PEM931CAeaResponse;
import com.altec.bsbr.app.hyb.dto.PEL1_PEM0004AeaResponse;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsParticip.XHYCadOsParticipEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYTransacao.XHYTransacaoEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;

@Component("cadastroosPesquisaClienteBean")
@Scope("session")
public class CadastroosPesquisaClienteBean extends BasicBBean {
	
	private static final long serialVersionUID = 1L;

	private String strNomepart;
	private String strDoctoPart;
	private String strTipo = "F";
	private String strPeNumPe_Rechamada = "";
	private String strNrSeqOs;
	private String strAcao;
	private List<ObjRsParticip> objRsParticipClientes;
	private List<ObjRsCombo> objRsItemsCombo;
	int iCountPartLesado = 1;
	
	private boolean emptyMsg;
	
	@Autowired
	private XHYCadOsParticipEndPoint cadOsParticipEndPoint;
	
	@Autowired
	private XHYTransacaoEndPoint transacaoEndPoint;
	
	@PostConstruct
	public void init() {
		System.out.println("--");
		buscarQueryString();
		this.emptyMsg = false;
		System.out.println("emptyMsg "+this.emptyMsg);
	}
	
	public void buscarQueryString() {
		this.strNrSeqOs = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs");
		System.out.println(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs"));
	}
	
	public void adicionaCombo() {
		objRsItemsCombo.add(new ObjRsCombo( "1", "Envolvido", "", ""));
		objRsItemsCombo.add(new ObjRsCombo("2", "Informante", "", ""));
		objRsItemsCombo.add(new ObjRsCombo("3", "Reclamante", "", ""));
	}
	
	public void refreshClienteNrOS(String seqOs) {
		System.out.println("refreshClienteNrOS");
		RequestContext.getCurrentInstance().execute("parent.parent.PF('statusDialog').hide();");
		this.strNrSeqOs = seqOs;
		System.out.println(seqOs + " seqOs");
	}
	
	public void GravarParticipante() {
		System.out.println("Chama Gravar --------->");
		try {
			int index = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("indice"));
			System.out.println(index + " index");
			Integer dblGravar = 0;
			String strError = "";
			System.out.println("-");
			System.out.println(ToStringBuilder.reflectionToString(objRsParticipClientes.get(index)));
			
			System.out.println("strTipo "+strTipo);
			int shdTipo = strTipo.equals("J") ? 3 : 2;
			
			String shdnCdSeg;
			if(objRsParticipClientes.get(index).getStrCDSEG().isEmpty()) {
				shdnCdSeg = "0";
			}else {
				shdnCdSeg = objRsParticipClientes.get(index).getStrCDSEG();
			}
			
			String shdnSeg;
			
			if(objRsParticipClientes.get(index).getSEGMENTO().length() >= 30) {
				shdnSeg = objRsParticipClientes.get(index).getSEGMENTO().substring(0, 30);
			}else
				shdnSeg = objRsParticipClientes.get(index).getSEGMENTO();
					
			dblGravar = new JSONObject(cadOsParticipEndPoint.inserirParticipante(objRsParticipClientes.get(index).getPENUMPE(), 
																  objRsParticipClientes.get(index).getCPF_CNPJ(), 
																  this.strNrSeqOs, 
																  shdTipo, 
																  objRsParticipClientes.get(index).getNOME(), 
																  objRsParticipClientes.get(index).getCBO_DESIG(), 
																  "", 
																  shdnSeg, 
																  Integer.valueOf(shdnCdSeg))).getInt("PNR_SEQU_PARP");
			
			if (dblGravar > 0 ) 
				RequestContext.getCurrentInstance().execute("alert('Participante incluído com sucesso.');");
			else 
				RequestContext.getCurrentInstance().execute("alert('Não foi possível gravar o registro.');");
			
			
		} catch (Exception e) {
			e.printStackTrace();
			RequestContext.getCurrentInstance().execute("alert('Não foi possível gravar o registro. Erro:'" + e.getMessage() + "');");
		}		
		
	}
	
	public void FillTblCliPart() {
		System.out.println("FillTblCliPart teste aaa");
		objRsParticipClientes = new ArrayList<ObjRsParticip>();
		objRsItemsCombo = new ArrayList<ObjRsCombo>();
		//RequestContext.getCurrentInstance().execute("alert('Falta MQ.');");
		if (!strNomepart.isEmpty() || !strDoctoPart.isEmpty()) {
			String strSeg = "";
			String xmlSeg = "";
			
			try {				
				
				adicionaCombo();
				iCountPartLesado = cadOsParticipEndPoint.fnExistePartLesado(this.getStrNrSeqOs());
				
				if(iCountPartLesado == 0) {
					objRsItemsCombo.add(new ObjRsCombo("4", "Reclamado/Lesado", "", ""));
				}
				
				/* CHAMADAS MQ */
				String json = transacaoEndPoint.pel1(strNomepart, strDoctoPart, strTipo, strPeNumPe_Rechamada);
				System.out.println("retorno pel1: "+json);			
	
				JSONArray jsonArrayPEL1 = new JSONArray(json);
				
				for (int i = 0; i < jsonArrayPEL1.length(); i++) {
					JSONObject itemPEL1 = jsonArrayPEL1.getJSONObject(i);
					
					ObjRsParticip particip = new ObjRsParticip();
					
					PEL1_PEM0004AeaResponse responsePEL1 = new PEL1_PEM0004AeaResponse();
					
					responsePEL1.setPENUMDO(itemPEL1.isNull("PENUMDO")? "" : itemPEL1.get("PENUMDO").toString());
					
					responsePEL1.setPENOMPE(itemPEL1.isNull("PENOMPE")? "" : itemPEL1.get("PENOMPE").toString());
					responsePEL1.setPEPRIAP(itemPEL1.isNull("PEPRIAP")? "" : itemPEL1.get("PEPRIAP").toString());
					responsePEL1.setPESEGAP(itemPEL1.isNull("PESEGAP")? "" : itemPEL1.get("PESEGAP").toString());
					
					responsePEL1.setPENUMPE(itemPEL1.isNull("PENUMPE")? "" : itemPEL1.get("PENUMPE").toString());
					
					String xmlCpfCnpj = responsePEL1.getPENUMDO(); 
					String xmlNome = responsePEL1.getPENOMPE() + responsePEL1.getPEPRIAP() + responsePEL1.getPESEGAP();
					String xmlPeNumPe = responsePEL1.getPENUMPE();
					String xmlCdSeg = getCdSegmentacao(xmlPeNumPe);
					
					if(xmlCdSeg.trim().length() > 0) {
						xmlSeg = GetNameSegmento(xmlSeg);
					}else {
						xmlSeg = "";
					}
					
					particip.setCPF_CNPJ(xmlCpfCnpj);
					particip.setNOME(xmlNome);
					particip.setSEGMENTO(xmlSeg);
					particip.setStrCDSEG(xmlCdSeg);
					particip.setPENUMPE(xmlPeNumPe);
					System.out.println("particip "+ToStringBuilder.reflectionToString(particip));
					objRsParticipClientes.add(particip);	
				}
				/* FIM CHAMADAS MQ */
				
			} catch (Exception e) {
				e.printStackTrace();
				try {
					FacesContext.getCurrentInstance().getExternalContext()
							.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			} 
		}
		RequestContext.getCurrentInstance().execute("parent.parent.PF('statusDialog').hide()");
		this.emptyMsg = this.objRsParticipClientes.isEmpty();
		FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("frmPesqPart");
		
	}
	
	public String getCdSegmentacao(String strPeNumPe) {
		String getCdSegmentacao = "";
		try {
			JSONArray jsonArrayPE95 = new JSONArray(transacaoEndPoint.pe95(strPeNumPe));
			
			if(jsonArrayPE95.length() == 0) {
				return getCdSegmentacao;
			}
			
			System.out.println("AQUIii ---!!!!");
			for (int i = 0; i < jsonArrayPE95.length(); i++) {
				JSONObject itemPE95 = jsonArrayPE95.getJSONObject(i);
				
				if(itemPE95.get("CLASEG").toString().equals("PRI") ) {
					PE95_PEM9310B_PEM931CAeaResponse responsePE95 = new PE95_PEM9310B_PEM931CAeaResponse();
					
					responsePE95.setSEGCLA(itemPE95.isNull("SEGCLA")? "" : itemPE95.get("SEGCLA").toString());
					getCdSegmentacao = responsePE95.getSEGCLA(); 
					
				}
				
			}
			
			System.out.println("getCdSegmentacao: -----> " + getCdSegmentacao);
			
			return getCdSegmentacao.trim();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	} 
			 

	
	public String GetNameSegmento(String strNrSegmentacao) {
		String retorno = "";
		
		try {
			JSONArray jsonArrayJCGG = new JSONArray(transacaoEndPoint.jcgg(strNrSegmentacao));
			
			for (int i = 0; i < jsonArrayJCGG.length(); i++) {
				JSONObject itemJCGG = jsonArrayJCGG.getJSONObject(i);
				
				JCGG_TCMGEN1AeaResponse responseJCGG = new JCGG_TCMGEN1AeaResponse();
				responseJCGG.setDATOS1(itemJCGG.isNull("DATOS1")? "" : itemJCGG.get("DATOS1").toString());
				retorno = responseJCGG.getDATOS1().substring(0, responseJCGG.getDATOS1().length() - 34);
				
			}
			
			return retorno;
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return retorno;
		
	}
	
	public void limparCampos() {
		System.out.println("limpar campos ------");
		if (this.objRsParticipClientes != null)
			this.objRsParticipClientes.clear();
		this.setStrTipo("F");
		this.setStrNomepart("");
		this.setStrDoctoPart("");
	}
	
	public String getStrAcao() {
		return strAcao;
	}

	public void setStrAcao(String strAcao) {
		this.strAcao = strAcao;
	}

	public String getStrNrSeqOs() {
		return strNrSeqOs;
	}

	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}

	public String getStrNomepart() {
		return strNomepart;
	}

	public void setStrNomepart(String strNomepart) {
		this.strNomepart = strNomepart;
	}

	public String getStrDoctoPart() {
		return strDoctoPart;
	}

	public void setStrDoctoPart(String strDoctoPart) {
		this.strDoctoPart = strDoctoPart;
	}

	public String getStrTipo() {
		return strTipo;
	}

	public void setStrTipo(String strTipo) {
		this.strTipo = strTipo;
	}

	public String getStrPeNumPe_Rechamada() {
		return strPeNumPe_Rechamada;
	}

	public void setStrPeNumPe_Rechamada(String strPeNumPe_Rechamada) {
		this.strPeNumPe_Rechamada = strPeNumPe_Rechamada;
	}

	public List<ObjRsParticip> getObjRsParticipClientes() {
		return objRsParticipClientes;
	}

	public void setObjRsParticipClientes(List<ObjRsParticip> objRsParticipClientes) {
		this.objRsParticipClientes = objRsParticipClientes;
	}

	public List<ObjRsCombo> getObjRsItemsCombo() {
		return objRsItemsCombo;
	}

	public void setObjRsItemsCombo(List<ObjRsCombo> objRsItemsCombo) {
		this.objRsItemsCombo = objRsItemsCombo;
	}

	public int getiCountPartLesado() {
		return iCountPartLesado;
	}

	public void setiCountPartLesado(int iCountPartLesado) {
		this.iCountPartLesado = iCountPartLesado;
	}

	
	public boolean isEmptyMsg() {
		return emptyMsg;
	}

	public void setEmptyMsg(boolean emptyMsg) {
		this.emptyMsg = emptyMsg;
	}
	
	
}


