package com.altec.bsbr.app.hyb.dto;

public class Hyb_cados_detalheriscopot_t_objRsProdCorp {
	private String NR_SEQU_TRAN_FRAU_CNAL;
	private String NM_OPER;
	private String DT_EXCL_TOTL;
	private String VL_EXCL_TOTL;
	private String DT_RECR_MES_TOTL;
	private String VL_RECR_MES_TOTL;
	private String DT_RECR_FORA_MES_TOTL;
	private String VL_RECR_FORA_MES_TOTL;
	private String IN_RESS;
	
	public Hyb_cados_detalheriscopot_t_objRsProdCorp() {
		
	}
	
	public Hyb_cados_detalheriscopot_t_objRsProdCorp(String nR_SEQU_TRAN_FRAU_CNAL, String nM_OPER, String dT_EXCL_TOTL,
			String vL_EXCL_TOTL, String dT_RECR_MES_TOTL, String vL_RECR_MES_TOTL, String dT_RECR_FORA_MES_TOTL,
			String vL_RECR_FORA_MES_TOTL, String iN_RESS) {
		NR_SEQU_TRAN_FRAU_CNAL = nR_SEQU_TRAN_FRAU_CNAL;
		NM_OPER = nM_OPER;
		DT_EXCL_TOTL = dT_EXCL_TOTL;
		VL_EXCL_TOTL = vL_EXCL_TOTL;
		DT_RECR_MES_TOTL = dT_RECR_MES_TOTL;
		VL_RECR_MES_TOTL = vL_RECR_MES_TOTL;
		DT_RECR_FORA_MES_TOTL = dT_RECR_FORA_MES_TOTL;
		VL_RECR_FORA_MES_TOTL = vL_RECR_FORA_MES_TOTL;
		IN_RESS = iN_RESS;
	}
	public String getNR_SEQU_TRAN_FRAU_CNAL() {
		return NR_SEQU_TRAN_FRAU_CNAL;
	}
	public void setNR_SEQU_TRAN_FRAU_CNAL(String nR_SEQU_TRAN_FRAU_CNAL) {
		NR_SEQU_TRAN_FRAU_CNAL = nR_SEQU_TRAN_FRAU_CNAL;
	}
	public String getNM_OPER() {
		return NM_OPER;
	}
	public void setNM_OPER(String nM_OPER) {
		NM_OPER = nM_OPER;
	}
	public String getDT_EXCL_TOTL() {
		return DT_EXCL_TOTL;
	}
	public void setDT_EXCL_TOTL(String dT_EXCL_TOTL) {
		DT_EXCL_TOTL = dT_EXCL_TOTL;
	}
	public String getVL_EXCL_TOTL() {
		return VL_EXCL_TOTL;
	}
	public void setVL_EXCL_TOTL(String vL_EXCL_TOTL) {
		VL_EXCL_TOTL = vL_EXCL_TOTL;
	}
	public String getDT_RECR_MES_TOTL() {
		return DT_RECR_MES_TOTL;
	}
	public void setDT_RECR_MES_TOTL(String dT_RECR_MES_TOTL) {
		DT_RECR_MES_TOTL = dT_RECR_MES_TOTL;
	}
	public String getVL_RECR_MES_TOTL() {
		return VL_RECR_MES_TOTL;
	}
	public void setVL_RECR_MES_TOTL(String vL_RECR_MES_TOTL) {
		VL_RECR_MES_TOTL = vL_RECR_MES_TOTL;
	}
	public String getDT_RECR_FORA_MES_TOTL() {
		return DT_RECR_FORA_MES_TOTL;
	}
	public void setDT_RECR_FORA_MES_TOTL(String dT_RECR_FORA_MES_TOTL) {
		DT_RECR_FORA_MES_TOTL = dT_RECR_FORA_MES_TOTL;
	}
	public String getVL_RECR_FORA_MES_TOTL() {
		return VL_RECR_FORA_MES_TOTL;
	}
	public void setVL_RECR_FORA_MES_TOTL(String vL_RECR_FORA_MES_TOTL) {
		VL_RECR_FORA_MES_TOTL = vL_RECR_FORA_MES_TOTL;
	}
	public String getIN_RESS() {
		return IN_RESS;
	}
	public void setIN_RESS(String iN_RESS) {
		IN_RESS = iN_RESS;
	}
	
}
