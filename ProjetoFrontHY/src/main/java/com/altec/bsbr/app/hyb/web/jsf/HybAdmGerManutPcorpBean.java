package com.altec.bsbr.app.hyb.web.jsf;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.PcorpModel;
import com.altec.bsbr.fw.web.jsf.BasicBBean;


@Component("hyAdmGerManutPcorpBean")
@Scope("session")
public class HybAdmGerManutPcorpBean extends BasicBBean {
	
	private static final long serialVersionUID = 1L;
	    
	private List<PcorpModel> objAdmGer = new ArrayList<PcorpModel>();
	private List<Boolean> objRsCanal = new ArrayList<Boolean>();
	private Object objRsPCorp;
	private String strDescricao;

	private Object strMsg;
	private String strAction = "";
	private String strErro;

	private String txtDescricao;
	private String txtHdAction;
	private String txtHdParamAltPCorp;
	private String txtHdParamAltRelac;
	private String txtHdPCorp;

        
    @PostConstruct
    public void init() {
    	
    	Map<String, String> request = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

    	this.strMsg = request.get("strMsg");

    	if(this.strAction.equals("P")) {
    		// ----- mock
	    	this.objAdmGer.removeAll(objAdmGer);
	    	this.objAdmGer.add(new PcorpModel(1, "Produto " + 1, "sla" + 1, true, false));
	    	
	    	this.objRsCanal.removeAll(objRsCanal);
	    	this.objRsCanal.add(true);
	    	this.objRsCanal.add(false);
	    	this.objRsCanal.add(true);
	    	// ------
    	}
    	else {
	    	// ----- mock
    		this.objRsCanal.removeAll(objRsCanal);
	    	this.objRsCanal.add(true);
	    	this.objRsCanal.add(false);
	    	this.objRsCanal.add(true);
	
	    	this.objAdmGer.removeAll(objAdmGer);
	    	
	    	for (int c = 1; c <= 15; c++) {
	    		this.objAdmGer.add(new PcorpModel(c, "Produto " + c, "sla" + c, (c % 2 != 0), (c % 2 == 0)));
	    	}
	    	// ------
    	}
    }
    
    public void salvar() {
    	Map<String, String> requestParamMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
    	System.out.println("Adicionar: " + requestParamMap.get("frm:txtHdParamAltPCorp"));
    	System.out.println("Adicionar: " + requestParamMap.get("frm:txtHdParamAltRelac"));
	}

	public void pesquisar() {
    	this.strAction = "P";
    	init();
	}
	
	public void listarTodos() {
    	this.strAction = "L";
    	init();
	}

	public List<PcorpModel> getObjAdmGer() {
    	return objAdmGer;
	}

	public void setObjAdmGer(List<PcorpModel> objAdmGer) {
    	this.objAdmGer = objAdmGer;
	}

	public int getObjRsCanalLength() {
    	return this.objRsCanal.size();
	}

	public List<Boolean> getObjRsCanal() {
    	return objRsCanal;
	}

	public void setObjRsCanal(List<Boolean> objRsCanal) {
    	this.objRsCanal = objRsCanal;
	}

	public Object getObjRsPCorp() {
    	return objRsPCorp;
	}

	public void setObjRsPCorp(Object objRsPCorp) {
    	this.objRsPCorp = objRsPCorp;
	}

	public String getStrMsg() {
    	if (strMsg == null) {
    		return "";
    	}
    	return strMsg.toString();
	}

	public void setStrMsg(String strMsg) {
    	this.strMsg = strMsg;
	}

	public String getStrAction() {
    	return strAction;
	}

	public void setStrAction(String strAction) {
    	this.strAction = strAction;
	}

	public String getTxtDescricao() {
    	return txtDescricao;
	}

	public void setTxtDescricao(String txtDescricao) {
    	this.txtDescricao = txtDescricao;
	}

	public String getTxtHdParamAltPCorp() {
    	return txtHdParamAltPCorp;
	}

	public void setTxtHdParamAltPCorp(String txtHdParamAltPCorp) {
    	this.txtHdParamAltPCorp = txtHdParamAltPCorp;
	}

	public String getTxtHdParamAltRelac() {
    	return txtHdParamAltRelac;
	}

	public void setTxtHdParamAltRelac(String txtHdParamAltRelac) {
    	this.txtHdParamAltRelac = txtHdParamAltRelac;
	}

	public String getTxtHdPCorp() {
    	return txtHdPCorp;
	}

	public void setTxtHdPCorp(String txtHdPCorp) {
    	this.txtHdPCorp = txtHdPCorp;
	}

}
