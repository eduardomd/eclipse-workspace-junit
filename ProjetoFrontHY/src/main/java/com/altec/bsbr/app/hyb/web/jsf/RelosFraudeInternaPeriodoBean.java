package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.RelControlePenalidade;
import com.altec.bsbr.app.hyb.dto.RelosFraudeInternaPeriodo;
import com.altec.bsbr.app.jab.hyb.webclient.XHYRelatorios.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYRelatorios.XHYRelatoriosEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component("RelosFraudeInternaPeriodoBean")
@Scope("session")
public class RelosFraudeInternaPeriodoBean extends BasicBBean {

	private static final long serialVersionUID = 1L;

	@Autowired
	private XHYRelatoriosEndPoint objRelat;

	private List<RelosFraudeInternaPeriodo> objRsRelat = new ArrayList<RelosFraudeInternaPeriodo>();

	private String strDtIni;
	private String strDtFim;

	private int total_QNT_AGEN;
	private Double total_VL_ENVO_AGEN;
	private Double total_VL_RECR_AGEN;
	private Double total_VL_PERD_EFET_AGEN;
	private int total_QNT_ADMC;
	private Double total_VL_ENVO_ADMC;
	private Double total_VL_RECR_ADMC;
	private Double total_VL_PERD_EFET_ADMC;
	private int total_QNTD_TOTAL;
	private Double total_VL_ENVO_TOTAL;
	private Double total_VL_RECR_TOTAL;
	private Double total_VL_PERD_EFET_TOTAL;

	ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
	JSONArray pcursorObjRs = new JSONArray();

	@PostConstruct
	public void init() throws IOException {
		FacesContext fc = FacesContext.getCurrentInstance();
		@SuppressWarnings("unchecked")
		Map<String, String> params = fc.getExternalContext().getRequestParameterMap();

		this.strDtIni = params.get("pDtIni");
		this.strDtFim = params.get("pDtFim");

		try {
			String objRs = objRelat.fnRelFraudeInternaPeriodo(this.strDtIni, this.strDtFim);
			JSONObject objResTemp = new JSONObject(objRs);
			pcursorObjRs = objResTemp.getJSONArray("PCURSOR");
		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			ec.redirect("hy_erro.xhtml?&strErro=" + e.getMessage());
		}

		for (int i = 0; i < pcursorObjRs.length(); i++) {
			JSONObject temp = pcursorObjRs.getJSONObject(i);

			objRsRelat.add(new RelosFraudeInternaPeriodo(temp.isNull("ROWNUM") ? "" : temp.get("ROWNUM").toString(),
					temp.isNull("NM_EVEN") ? "" : temp.get("NM_EVEN").toString(),
					temp.isNull("QNT_AGEN") ? "" : temp.get("QNT_AGEN").toString(),
					temp.isNull("VL_ENVO_AGEN") ? "" : temp.get("VL_ENVO_AGEN").toString(),
					temp.isNull("VL_RECR_AGEN") ? "" : temp.get("VL_RECR_AGEN").toString(),
					temp.isNull("VL_PERD_EFET_AGEN") ? "" : temp.get("VL_PERD_EFET_AGEN").toString(),
					temp.isNull("QNT_ADMC") ? "" : temp.get("QNT_ADMC").toString(),
					temp.isNull("VL_ENVO_ADMC") ? "" : temp.get("VL_ENVO_ADMC").toString(),
					temp.isNull("VL_RECR_ADMC") ? "" : temp.get("VL_RECR_ADMC").toString(),
					temp.isNull("VL_PERD_EFET_ADMC") ? "" : temp.get("VL_PERD_EFET_ADMC").toString(),
					temp.isNull("QNTD_TOTAL") ? "" : temp.get("QNTD_TOTAL").toString(),
					temp.isNull("VL_ENVO_TOTAL") ? "" : temp.get("VL_ENVO_TOTAL").toString(),
					temp.isNull("VL_RECR_TOTAL") ? "" : temp.get("VL_RECR_TOTAL").toString(),
					temp.isNull("VL_PERD_EFET_TOTAL") ? "" : temp.get("VL_PERD_EFET_TOTAL").toString(),
					temp.isNull("VL_ENVO_AGEN") ? "" : formataNumero(temp.get("VL_ENVO_AGEN").toString()),
					temp.isNull("VL_RECR_AGEN") ? "" : formataNumero(temp.get("VL_RECR_AGEN").toString()),
					temp.isNull("VL_PERD_EFET_AGEN") ? "" : formataNumero(temp.get("VL_PERD_EFET_AGEN").toString()),
					temp.isNull("VL_ENVO_ADMC") ? "" : formataNumero(temp.get("VL_ENVO_ADMC").toString()),
					temp.isNull("VL_RECR_ADMC") ? "" : formataNumero(temp.get("VL_RECR_ADMC").toString()),
					temp.isNull("VL_PERD_EFET_ADMC") ? "" : formataNumero(temp.get("VL_PERD_EFET_ADMC").toString()),
					temp.isNull("VL_ENVO_TOTAL") ? "" : formataNumero(temp.get("VL_ENVO_TOTAL").toString()),
					temp.isNull("VL_RECR_TOTAL") ? "" : formataNumero(temp.get("VL_RECR_TOTAL").toString()),
					temp.isNull("VL_PERD_EFET_TOTAL") ? "" : formataNumero(temp.get("VL_PERD_EFET_TOTAL").toString())));
		}

		/*
		 * if (objRsRelat.size() == 0) { for (int i = 0; i < 10; i++) {
		 * 
		 * objRsRelat.add( new RelosFraudeInternaPeriodo( "ROWNUM", "NM_EVEN", "5",
		 * "20000", "10000", "10000", "6", "20000", "10000", "10000", "7", "20000",
		 * "10000", "10000", formataNumero("20000"), formataNumero("10000"),
		 * formataNumero("10000"), formataNumero("20000"), formataNumero("10000"),
		 * formataNumero("10000"), formataNumero("20000"), formataNumero("10000"),
		 * formataNumero("10000") )); } }
		 */

		cleanSession();
	}

	public void cleanSession() {
		FacesContext context = FacesContext.getCurrentInstance();
		if (context.getExternalContext().getSessionMap().get("relatorioosBean") != null) {
			context.getExternalContext().getSessionMap().remove("relatorioosBean");
		}
	}

	public String now() {
		DateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		Date date = new Date();
		return dateformat.format(date);
	}

	public String formataNumero(String number) {
		String result = "";
		if (number != null && !number.isEmpty()) {
			DecimalFormat value = new DecimalFormat("###,##0.00");
			result = value.format(Float.parseFloat(number));
			String fp = result.substring(0, result.length() - 3);
			String sp = result.substring(result.length() - 3);
			result = fp.replace(",", ".") + sp.replace(".", ",");
		}

		if (result.equals("0,00")) {
			return "0";
		}

		return result;
	}

	public String dblTotal_QNT_AGEN() {
		this.total_QNT_AGEN = 0;
		objRsRelat.forEach((item) -> {
			this.total_QNT_AGEN += Integer.parseInt(item.getQNT_AGEN());
		});
		return Integer.toString(this.total_QNT_AGEN);
	}

	public String dblTotal_VL_ENVO_AGEN() {
		this.total_VL_ENVO_AGEN = 0.00;
		objRsRelat.forEach((item) -> {
			this.total_VL_ENVO_AGEN += Double.parseDouble(item.getVL_ENVO_AGEN());
		});
		return formataNumero(Double.toString(this.total_VL_ENVO_AGEN));
	}

	public String dblTotal_VL_RECR_AGEN() {
		this.total_VL_RECR_AGEN = 0.00;
		objRsRelat.forEach((item) -> {
			this.total_VL_RECR_AGEN += Double.parseDouble(item.getVL_RECR_AGEN());
		});
		return formataNumero(Double.toString(this.total_VL_RECR_AGEN));
	}

	public String dblTotal_VL_PERD_EFET_AGEN() {
		this.total_VL_PERD_EFET_AGEN = 0.00;
		objRsRelat.forEach((item) -> {
			this.total_VL_PERD_EFET_AGEN += Double.parseDouble(item.getVL_PERD_EFET_AGEN());
		});
		return formataNumero(Double.toString(this.total_VL_PERD_EFET_AGEN));
	}

	public String dblTotal_QNT_ADMC() {
		this.total_QNT_ADMC = 0;
		objRsRelat.forEach((item) -> {
			this.total_QNT_ADMC += Integer.parseInt(item.getQNT_ADMC());
		});
		return Integer.toString(this.total_QNT_ADMC);
	}

	public String dblTotal_VL_ENVO_ADMC() {
		this.total_VL_ENVO_ADMC = 0.00;
		objRsRelat.forEach((item) -> {
			this.total_VL_ENVO_ADMC += Double.parseDouble(item.getVL_ENVO_ADMC());
		});
		return formataNumero(Double.toString(this.total_VL_ENVO_ADMC));
	}

	public String dblTotal_VL_RECR_ADMC() {
		this.total_VL_RECR_ADMC = 0.00;
		objRsRelat.forEach((item) -> {
			this.total_VL_RECR_ADMC += Double.parseDouble(item.getVL_RECR_ADMC());
		});
		return formataNumero(Double.toString(this.total_VL_RECR_ADMC));
	}

	public String dblTotal_VL_PERD_EFET_ADMC() {
		this.total_VL_PERD_EFET_ADMC = 0.00;
		objRsRelat.forEach((item) -> {
			this.total_VL_PERD_EFET_ADMC += Double.parseDouble(item.getVL_PERD_EFET_ADMC());
		});
		return formataNumero(Double.toString(this.total_VL_PERD_EFET_ADMC));
	}

	public String dblTotal_QNTD_TOTAL() {
		this.total_QNTD_TOTAL = 0;
		objRsRelat.forEach((item) -> {
			this.total_QNTD_TOTAL += Integer.parseInt(item.getQNTD_TOTAL());
		});
		return Integer.toString(this.total_QNTD_TOTAL);
	}

	public String dblTotal_VL_ENVO_TOTAL() {
		this.total_VL_ENVO_TOTAL = 0.00;
		objRsRelat.forEach((item) -> {
			this.total_VL_ENVO_TOTAL += Double.parseDouble(item.getVL_ENVO_TOTAL());
		});
		return formataNumero(Double.toString(this.total_VL_ENVO_TOTAL));
	}

	public String dblTotal_VL_RECR_TOTAL() {
		this.total_VL_RECR_TOTAL = 0.00;
		objRsRelat.forEach((item) -> {
			this.total_VL_RECR_TOTAL += Double.parseDouble(item.getVL_RECR_TOTAL());
		});
		return formataNumero(Double.toString(this.total_VL_RECR_TOTAL));
	}

	public String dblTotal_VL_PERD_EFET_TOTAL() {
		this.total_VL_PERD_EFET_TOTAL = 0.00;
		objRsRelat.forEach((item) -> {
			this.total_VL_PERD_EFET_TOTAL += Double.parseDouble(item.getVL_PERD_EFET_TOTAL());
		});
		return formataNumero(Double.toString(this.total_VL_PERD_EFET_TOTAL));
	}

	public void postProcessExcel(Object doc) {
		try {
			XSSFWorkbook wb = (XSSFWorkbook) doc;
			XSSFSheet sheet = wb.getSheetAt(0);
			sheet.setDisplayGridlines(false);

			formatarCabecalho(wb, sheet);
			formatarCabecalhoTabela(wb, sheet);
			formatarSubCabecalhoTabela(wb, sheet);
			formatarMergeDeCelulas(wb, sheet);
			formatarMergedCells(wb, sheet);
			formatarRegistrosTabela(wb, sheet);
			criarRodape(wb, sheet);
			for (int i = 0; i <= sheet.getRow(5).getLastCellNum(); i++) {
				sheet.autoSizeColumn(i);
			}

			sheet.shiftRows(3, sheet.getLastRowNum() + 1, 2);
			sheet.shiftRows(6, sheet.getLastRowNum() + 1, 1);

			XSSFRow linhaCabecalhoTabela = sheet.getRow(7);
			if (linhaCabecalhoTabela != null) {
				linhaCabecalhoTabela.setHeightInPoints(18);
			}
		} catch (Exception e) {
			System.out.println(e);
		}

	}

	private void formatarCabecalho(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyle1 = wb.createCellStyle();
		headerStyle1.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle1.setWrapText(true);
		headerStyle1.setBorderBottom(CellStyle.BORDER_NONE);
		headerStyle1.setBorderLeft(CellStyle.BORDER_NONE);
		headerStyle1.setBorderRight(CellStyle.BORDER_NONE);
		headerStyle1.setBorderTop(CellStyle.BORDER_NONE);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 14);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		headerStyle1.setFont(font);

		XSSFRow primeiraLinha = sheet.getRow(1);
		if (primeiraLinha != null) {
			XSSFCell primeiraLinhaCell = primeiraLinha.getCell(0);
			primeiraLinhaCell.setCellStyle(headerStyle1);
			primeiraLinha.setHeightInPoints((short) 19);
		}
		XSSFRow segundaLinha = sheet.getRow(2);
		if (segundaLinha != null) {
			XSSFCell segundaLinhaCell = segundaLinha.getCell(0);
			segundaLinhaCell.setCellStyle(headerStyle1);
			segundaLinha.setHeightInPoints((short) 19);
		}
	}

	private void formatarCabecalhoTabela(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setWrapText(true);
		headerStyle.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyle.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyle.setBorderRight(CellStyle.BORDER_THIN);
		headerStyle.setBorderTop(CellStyle.BORDER_THIN);
		headerStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.WHITE.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(font);

		XSSFRow linhaCabecalhoTabela = sheet.getRow(4);
		if (linhaCabecalhoTabela != null) {
			for (int i = 0; i <= linhaCabecalhoTabela.getLastCellNum(); i++) {
				XSSFCell celulaHeader = linhaCabecalhoTabela.getCell(i);
				linhaCabecalhoTabela.setHeightInPoints(30);
				if (celulaHeader != null) {
					celulaHeader.setCellStyle(headerStyle);
				}
			}
		}
	}

	private void formatarSubCabecalhoTabela(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setWrapText(true);
		headerStyle.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyle.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyle.setBorderRight(CellStyle.BORDER_THIN);
		headerStyle.setBorderTop(CellStyle.BORDER_THIN);
		headerStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.WHITE.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(font);

		XSSFRow linhaCabecalhoTabela = sheet.getRow(5);
		if (linhaCabecalhoTabela != null) {
			for (int i = 0; i <= linhaCabecalhoTabela.getLastCellNum(); i++) {
				XSSFCell celulaHeader = linhaCabecalhoTabela.getCell(i);
				linhaCabecalhoTabela.setHeightInPoints(30);
				if (celulaHeader != null) {
					celulaHeader.setCellStyle(headerStyle);
				}
			}
		}
	}

	private void formatarMergeDeCelulas(XSSFWorkbook wb, XSSFSheet sheet) {
		CellRangeAddress detalheVazio = new CellRangeAddress(4, 5, 0, 1);
		sheet.addMergedRegion(detalheVazio);
	}

	private void formatarMergedCells(XSSFWorkbook wb, XSSFSheet sheet) {
		for (int i = 3; i < sheet.getNumMergedRegions() - 1; i++) {
			CellRangeAddress regiao = sheet.getMergedRegion(i);

			RegionUtil.setBorderTop(CellStyle.BORDER_THIN, regiao, sheet, wb);
			RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, regiao, sheet, wb);
			RegionUtil.setBorderRight(CellStyle.BORDER_THIN, regiao, sheet, wb);
			RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, regiao, sheet, wb);
		}
	}

	private void formatarRegistrosTabela(XSSFWorkbook wb, XSSFSheet sheet) {

		if (objRsRelat != null && !objRsRelat.isEmpty()) {
			formatarLinhasDetalhe(wb, sheet);
			formatarLinhaTotais(wb, sheet);
		} else {
			formatarDetalheVazio(wb, sheet);
			XSSFRow linhaTotal = sheet.getRow(7);
			sheet.removeRow(linhaTotal);
		}
	}

	private void formatarLinhaTotais(XSSFWorkbook wb, XSSFSheet sheet) {

		XSSFCellStyle headerStyleCenter = wb.createCellStyle();
		headerStyleCenter.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyleCenter.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleCenter.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleCenter.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleCenter.setBorderTop(CellStyle.BORDER_THIN);
		headerStyleCenter.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		headerStyleCenter.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		XSSFCellStyle headerStyleRight = wb.createCellStyle();
		headerStyleRight.setAlignment(CellStyle.ALIGN_RIGHT);
		headerStyleRight.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderTop(CellStyle.BORDER_THIN);
		headerStyleRight.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		headerStyleRight.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.WHITE.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyleCenter.setFont(font);
		headerStyleRight.setFont(font);

		XSSFRow linhaTotal = sheet.getRow(objRsRelat.size() + 6);
		for (int i = 0; i <= linhaTotal.getLastCellNum(); i++) {
			XSSFCell celulaTotal = linhaTotal.getCell(i);
			if (celulaTotal != null) {
				if (i == 0) {
					celulaTotal.setCellStyle(headerStyleCenter);
				} else {
					celulaTotal.setCellStyle(headerStyleRight);
				}
			}
		}
	}

	private void formatarDetalheVazio(XSSFWorkbook wb, XSSFSheet sheet) {

		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setWrapText(true);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(font);

		sheet.shiftRows(6, 7, 1);

		CellRangeAddress detalheVazio = new CellRangeAddress(6, 6, 0, 13);
		sheet.addMergedRegion(detalheVazio);

		XSSFRow linhaVazia = sheet.getRow(6);
		if (linhaVazia == null) {
			linhaVazia = sheet.createRow(6);
		}
		XSSFCell celulaVazia = linhaVazia.createCell(0);
		linhaVazia.setHeightInPoints((short) 20);
		celulaVazia.setCellValue("Não existem dados para o período selecionado.");
		celulaVazia.setCellStyle(headerStyle);

		RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
		RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
		RegionUtil.setBorderRight(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
		RegionUtil.setBorderTop(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
	}

	private void formatarLinhasDetalhe(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyleLeft = wb.createCellStyle();
		headerStyleLeft.setAlignment(CellStyle.ALIGN_LEFT);
		headerStyleLeft.setWrapText(true);
		headerStyleLeft.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderTop(CellStyle.BORDER_THIN);

		XSSFCellStyle headerStyleRight = wb.createCellStyle();
		headerStyleRight.setAlignment(CellStyle.ALIGN_RIGHT);
		headerStyleRight.setWrapText(true);
		headerStyleRight.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderTop(CellStyle.BORDER_THIN);

		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		headerStyleLeft.setFont(font);
		headerStyleRight.setFont(font);

		int linhaInicial = 6;
		int linhasDetalhe = objRsRelat.size();
		int linhaFinal = linhaInicial + linhasDetalhe - 1;

		for (int linha = linhaInicial; linha <= linhaFinal; linha++) {
			XSSFRow detalheTabela = sheet.getRow(linha);
			if (detalheTabela != null) {
				for (int i = 0; i <= detalheTabela.getLastCellNum(); i++) {
					XSSFCell celulaDetalhe = detalheTabela.getCell(i);
					if (celulaDetalhe != null) {
						if (i == 0 || i == 1) {
							celulaDetalhe.setCellStyle(headerStyleLeft);
						} else {
							celulaDetalhe.setCellStyle(headerStyleRight);
						}

					}
				}
			}
		}
	}

	private void criarRodape(XSSFWorkbook wb, XSSFSheet sheet) {
		Font font;
		/* Criação, inclusão e formatação dos dados do footer do excel */
		XSSFRow ultimaLinha = sheet.createRow(sheet.getLastRowNum() + 1);

		XSSFCellStyle footerStyle = wb.createCellStyle();
		footerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		footerStyle.setWrapText(true);
		font = footerStyle.getFont();
		font.setFontHeight((short) 150);
		font.setFontName("Arial");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		footerStyle.setFont(font);

		XSSFCell cell1 = ultimaLinha.createCell(0);
		cell1.setCellValue(now());
		cell1.setCellStyle(footerStyle);

		XSSFCell cell2 = ultimaLinha.createCell(3);
		cell2.setCellValue("Superintêndencia de Ocorrências Especiais");
		cell2.setCellStyle(footerStyle);

		CellRangeAddress region1 = new CellRangeAddress(ultimaLinha.getRowNum(), ultimaLinha.getRowNum(), 0, 2);
		CellRangeAddress region2 = new CellRangeAddress(ultimaLinha.getRowNum(), ultimaLinha.getRowNum(), 3, 13);
		sheet.addMergedRegion(region1);
		sheet.addMergedRegion(region2);
	}

	public List<RelosFraudeInternaPeriodo> getObjRsRelat() {
		return objRsRelat;
	}

	public void setObjRsRelat(List<RelosFraudeInternaPeriodo> objRsRelat) {
		this.objRsRelat = objRsRelat;
	}

	public String getStrDtIni() {
		return strDtIni;
	}

	public void setStrDtIni(String strDtIni) {
		this.strDtIni = strDtIni;
	}

	public String getStrDtFim() {
		return strDtFim;
	}

	public void setStrDtFim(String strDtFim) {
		this.strDtFim = strDtFim;
	}

	public int getTotal_QNT_AGEN() {
		return total_QNT_AGEN;
	}

	public void setTotal_QNT_AGEN(int total_QNT_AGEN) {
		this.total_QNT_AGEN = total_QNT_AGEN;
	}

	public Double getTotal_VL_ENVO_AGEN() {
		return total_VL_ENVO_AGEN;
	}

	public void setTotal_VL_ENVO_AGEN(Double total_VL_ENVO_AGEN) {
		this.total_VL_ENVO_AGEN = total_VL_ENVO_AGEN;
	}

	public Double getTotal_VL_RECR_AGEN() {
		return total_VL_RECR_AGEN;
	}

	public void setTotal_VL_RECR_AGEN(Double total_VL_RECR_AGEN) {
		this.total_VL_RECR_AGEN = total_VL_RECR_AGEN;
	}

	public Double getTotal_VL_PERD_EFET_AGEN() {
		return total_VL_PERD_EFET_AGEN;
	}

	public void setTotal_VL_PERD_EFET_AGEN(Double total_VL_PERD_EFET_AGEN) {
		this.total_VL_PERD_EFET_AGEN = total_VL_PERD_EFET_AGEN;
	}

	public int getTotal_QNT_ADMC() {
		return total_QNT_ADMC;
	}

	public void setTotal_QNT_ADMC(int total_QNT_ADMC) {
		this.total_QNT_ADMC = total_QNT_ADMC;
	}

	public Double getTotal_VL_ENVO_ADMC() {
		return total_VL_ENVO_ADMC;
	}

	public void setTotal_VL_ENVO_ADMC(Double total_VL_ENVO_ADMC) {
		this.total_VL_ENVO_ADMC = total_VL_ENVO_ADMC;
	}

	public Double getTotal_VL_RECR_ADMC() {
		return total_VL_RECR_ADMC;
	}

	public void setTotal_VL_RECR_ADMC(Double total_VL_RECR_ADMC) {
		this.total_VL_RECR_ADMC = total_VL_RECR_ADMC;
	}

	public Double getTotal_VL_PERD_EFET_ADMC() {
		return total_VL_PERD_EFET_ADMC;
	}

	public void setTotal_VL_PERD_EFET_ADMC(Double total_VL_PERD_EFET_ADMC) {
		this.total_VL_PERD_EFET_ADMC = total_VL_PERD_EFET_ADMC;
	}

	public int getTotal_QNTD_TOTAL() {
		return total_QNTD_TOTAL;
	}

	public void setTotal_QNTD_TOTAL(int total_QNTD_TOTAL) {
		this.total_QNTD_TOTAL = total_QNTD_TOTAL;
	}

	public Double getTotal_VL_ENVO_TOTAL() {
		return total_VL_ENVO_TOTAL;
	}

	public void setTotal_VL_ENVO_TOTAL(Double total_VL_ENVO_TOTAL) {
		this.total_VL_ENVO_TOTAL = total_VL_ENVO_TOTAL;
	}

	public Double getTotal_VL_RECR_TOTAL() {
		return total_VL_RECR_TOTAL;
	}

	public void setTotal_VL_RECR_TOTAL(Double total_VL_RECR_TOTAL) {
		this.total_VL_RECR_TOTAL = total_VL_RECR_TOTAL;
	}

	public Double getTotal_VL_PERD_EFET_TOTAL() {
		return total_VL_PERD_EFET_TOTAL;
	}

	public void setTotal_VL_PERD_EFET_TOTAL(Double total_VL_PERD_EFET_TOTAL) {
		this.total_VL_PERD_EFET_TOTAL = total_VL_PERD_EFET_TOTAL;
	}
}
