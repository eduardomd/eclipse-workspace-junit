package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


import com.altec.bsbr.app.hyb.dto.RelosControleProcessosAbertoModel;
import com.altec.bsbr.app.jab.hyb.webclient.XHYRelatorios.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYRelatorios.XHYRelatoriosEndPoint;

@Component("relosControleProcessosAbertoBean")
@Scope("session")
public class RelosControleProcessosAbertoBean extends BasicBBean {
	
	private static final long serialVersionUID = 1L;
	
	@Autowired
	private XHYRelatoriosEndPoint objRelat;
	
	private List<RelosControleProcessosAbertoModel> objRsRelat  = new ArrayList<RelosControleProcessosAbertoModel>();
	private String strDtIni;
	private String strDtFim;
	private String strCodArea;
	
	ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
	JSONArray pcursorObjRs = new JSONArray();

	@PostConstruct
    public void init() throws IOException {
		FacesContext fc = FacesContext.getCurrentInstance();
		@SuppressWarnings("unchecked")
		Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
		
		this.strDtIni = params.get("pDtIni");
		this.strDtFim = params.get("pDtFim");
		this.strCodArea = params.get("pCodArea");
		
		try {
			String objRs = objRelat.fnRelControleProcessosAberto(this.strDtIni, this.strDtFim, this.strCodArea);
			JSONObject objResTemp = new JSONObject(objRs);
			pcursorObjRs = objResTemp.getJSONArray("PCURSOR");
		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			ec.redirect("hy_erro.xhtml?&strErro=" + e.getMessage());
		}
		
		for (int i = 0; i < pcursorObjRs.length(); i++) {
            JSONObject temp = pcursorObjRs.getJSONObject(i);
            String dataFormatadaFinalAprov = ""; 
            String dataFormatadaFinalAber = "";
            if(!temp.isNull("DT_APROV")) {
            	Date date = null;
            	String dateJson = "";
            	if(temp.get("DT_APROV").toString().contains("+")) {
            		dateJson = temp.get("DT_APROV").toString().substring(0,temp.get("DT_APROV").toString().indexOf("+"));
            		
            	} else { 
            		dateJson = temp.get("DT_APROV").toString();
            	}
				try {
					date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(dateJson);
				} catch (JSONException | ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				dataFormatadaFinalAprov = new SimpleDateFormat("dd/MM/yyyy HH:mm").format(date);
            
            } else {
            	dataFormatadaFinalAprov = "";
            }
            
            if(!temp.isNull("DT_ABER_ORDE_SERV")) {
            	Date date = null;
            	String dateJson = "";
            	if(temp.get("DT_ABER_ORDE_SERV").toString().contains("+")) {
            		dateJson = temp.get("DT_ABER_ORDE_SERV").toString().substring(0,temp.get("DT_ABER_ORDE_SERV").toString().indexOf("+"));
            		
            	} else { 
            		dateJson = temp.get("DT_ABER_ORDE_SERV").toString();
            	}
				try {
					date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(dateJson);
				} catch (JSONException | ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				dataFormatadaFinalAber = new SimpleDateFormat("dd/MM/yyyy HH:mm").format(date);
            
            } else {
            	dataFormatadaFinalAber = "";
            }
            
            objRsRelat.add(
            		new RelosControleProcessosAbertoModel(
            				temp.isNull("ROWNUM") ? "" : temp.get("ROWNUM").toString(),
            				temp.isNull("NR_SEQU_ORDE_SERV") ? "" : temp.get("NR_SEQU_ORDE_SERV").toString(),
            				temp.isNull("NM_EVEN") ? "" : temp.get("NM_EVEN").toString(),
            				temp.isNull("TP_UOR") ? "" : temp.get("TP_UOR").toString(),
            				temp.isNull("CD_UOR") ? "" : temp.get("CD_UOR").toString(),
            				temp.isNull("NM_UOR") ? "" : temp.get("NM_UOR").toString(),
            				temp.isNull("NM_REDE") ? "" : temp.get("NM_REDE").toString(),
            				temp.isNull("NM_REGI") ? "" : temp.get("NM_REGI").toString(),
            				temp.isNull("TX_ABER_ORDE_SERV") ? "" : temp.get("TX_ABER_ORDE_SERV").toString(),
            				temp.isNull("VL_ENVO") ? "" : temp.get("VL_ENVO").toString(),
            				temp.isNull("VL_PERD_EFET") ? "" : temp.get("VL_PERD_EFET").toString(),
            				temp.isNull("NM_RECU_OCOR_ESPC") ? "" : temp.get("NM_RECU_OCOR_ESPC").toString(),
            				dataFormatadaFinalAprov,
            				temp.isNull("NM_CRIT") ? "" : temp.get("NM_CRIT").toString(),
            				temp.isNull("NM_SITU_ORDE_SERV") ? "" : temp.get("NM_SITU_ORDE_SERV").toString(),
            				temp.isNull("VL_ENVO") ? "" : formataNumero(temp.get("VL_ENVO").toString()),
            				temp.isNull("VL_PERD_EFET") ? "" : formataNumero(temp.get("VL_PERD_EFET").toString()),
            				temp.isNull("DT_ABER_ORDE_SERV") ? "" : temp.get("DT_ABER_ORDE_SERV").toString()
            		));
        }
		
		/*if (objRsRelat.size() == 0) {
			for (int i = 0; i < 10; i++) {
				String TP_UOR;
				if (i < 4) {
					TP_UOR = "1";
				} else {
					TP_UOR = "0";
				}

	            objRsRelat.add(
	            		new RelosControleProcessosAbertoModel(
	            				"ROWNUM",
	            				"NR_SEQU_ORDE_SERV",
	            				"NM_EVEN",
	            				TP_UOR,
	            				"CD_UOR",
	            				"NM_UOR",
	            				"NM_REDE",
	            				"NM_REGI",
	            				"TX_ABER_ORDE_SERV",
	            				"20000",
	            				"15000",
	            				"NM_RECU_OCOR_ESPC",
	            				"DT_APROV",
	            				"NM_CRIT",
	            				"NM_SITU_ORDE_SERV",
	            				formataNumero("20000"),
	            				formataNumero("15000"),
	            				""
	            		));
	        }
		}*/
		cleanSession();
	}

	public void cleanSession() {
		FacesContext context = FacesContext.getCurrentInstance();
		if (context.getExternalContext().getSessionMap().get("relatorioosBean") != null) {
			context.getExternalContext().getSessionMap().remove("relatorioosBean");
		}
	}

	public String now() {
		DateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		Date date = new Date();
		return dateformat.format(date);
	}
	
	public String formataNumero(String number) {
		String result = "";
		if (number != null && !number.isEmpty()) {
			DecimalFormat value = new DecimalFormat("###,##0.00");
			result = value.format(Float.parseFloat(number));
			String fp = result.substring(0, result.length() - 3);
			String sp = result.substring(result.length() - 3);
			result = fp.replace(",", ".") + sp.replace(".", ",");
		}
		
		if (result.equals("0,00")) {
			return "0";
		}

		return result;
	}
	
	public void postProcessExcel(Object doc) {
		try {
			XSSFWorkbook wb = (XSSFWorkbook) doc;
			XSSFSheet sheet = wb.getSheetAt(0);
			sheet.setDisplayGridlines(false);

			formatarCabecalho(wb, sheet);
			formatarCabecalhoTabela(wb, sheet);
			formatarSubCabecalhoTabela(wb, sheet);
			
			
			formatarRegistrosTabela(wb, sheet);
			criarRodape(wb, sheet);
			MergeCells(wb, sheet);
			formatarMergedCells(wb, sheet);
			
			for (int i = 0; i <= sheet.getRow(4).getLastCellNum(); i++) { //5
					sheet.autoSizeColumn(i, true);
			}
			
			sheet.shiftRows(3, sheet.getLastRowNum() + 1, 2);
			sheet.shiftRows(6, sheet.getLastRowNum() + 1, 1);
		} catch (Exception e) {
			System.out.println(e);
		}

	}
	
	private void MergeCells(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFRow linhaCabecalhoTabela = sheet.getRow(5);
		
		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setWrapText(true);
		headerStyle.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyle.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyle.setBorderRight(CellStyle.BORDER_THIN);
		headerStyle.setBorderTop(CellStyle.BORDER_NONE);
		headerStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.WHITE.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(font);
		
		if (linhaCabecalhoTabela != null) {
			for (int i = 0; i <= linhaCabecalhoTabela.getLastCellNum(); i++) {
				if (i != 3 && i != 4 && i != 5 && i != 6) {
					XSSFCell celulaHeader = linhaCabecalhoTabela.getCell(i);
					if (celulaHeader != null) {
						celulaHeader.setCellStyle(headerStyle);
					}
				}
			}
		}
		
		CellRangeAddress detalheVazio = new CellRangeAddress(4, 4, 5, 6);
		sheet.addMergedRegion(detalheVazio);
	}

	private void formatarCabecalho(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyle1 = wb.createCellStyle();
		headerStyle1.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle1.setWrapText(true);
		headerStyle1.setBorderBottom(CellStyle.BORDER_NONE);
		headerStyle1.setBorderLeft(CellStyle.BORDER_NONE);
		headerStyle1.setBorderRight(CellStyle.BORDER_NONE);
		headerStyle1.setBorderTop(CellStyle.BORDER_NONE);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 14);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		headerStyle1.setFont(font);

		XSSFRow primeiraLinha = sheet.getRow(1);
		if (primeiraLinha != null) {
			XSSFCell primeiraLinhaCell = primeiraLinha.getCell(0);
			primeiraLinhaCell.setCellStyle(headerStyle1);
			primeiraLinha.setHeightInPoints((short) 19);
		}
		XSSFRow segundaLinha = sheet.getRow(2);
		if (segundaLinha != null) {
			XSSFCell segundaLinhaCell = segundaLinha.getCell(0);
			segundaLinhaCell.setCellStyle(headerStyle1);
			segundaLinha.setHeightInPoints((short) 19);
		}
	}

	private void formatarCabecalhoTabela(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		if(objRsRelat.size() > 0) {
			headerStyle.setWrapText(true);
		} else {
			headerStyle.setWrapText(false);
		}
		headerStyle.setBorderBottom(CellStyle.BORDER_NONE);
		headerStyle.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyle.setBorderRight(CellStyle.BORDER_THIN);
		headerStyle.setBorderTop(CellStyle.BORDER_THIN);
		headerStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.WHITE.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(font);

		XSSFRow linhaCabecalhoTabela = sheet.getRow(4);
		linhaCabecalhoTabela.setHeightInPoints(20);
		if (linhaCabecalhoTabela != null) {
			for (int i = 0; i <= linhaCabecalhoTabela.getLastCellNum(); i++) {
				XSSFCell celulaHeader = linhaCabecalhoTabela.getCell(i);
				if (celulaHeader != null) {
					celulaHeader.setCellStyle(headerStyle);
				}
			}
		}
	}
	
	private void formatarSubCabecalhoTabela(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setWrapText(true);
		headerStyle.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyle.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyle.setBorderRight(CellStyle.BORDER_THIN);
		headerStyle.setBorderTop(CellStyle.BORDER_THIN);
		headerStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.WHITE.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(font);
		
		XSSFCellStyle specialStyle = wb.createCellStyle();
		//specialStyle.setIndention((short) 5);
		specialStyle.setAlignment(CellStyle.ALIGN_CENTER);
		specialStyle.setWrapText(false);
		specialStyle.setBorderBottom(CellStyle.BORDER_NONE);
		specialStyle.setBorderLeft(CellStyle.BORDER_THIN);
		specialStyle.setBorderRight(CellStyle.BORDER_THIN);
		specialStyle.setBorderTop(CellStyle.BORDER_THIN);
		specialStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
		specialStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font specialFont = wb.createFont();
		specialFont.setFontHeightInPoints((short) 11);
		specialFont.setFontName("Calibri");
		specialFont.setColor(IndexedColors.WHITE.getIndex());
		specialFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		specialStyle.setFont(specialFont);

		XSSFRow linhaCabecalhoTabela = sheet.getRow(5);
		if (linhaCabecalhoTabela != null) {
			for (int i = 0; i <= linhaCabecalhoTabela.getLastCellNum(); i++) {
				XSSFCell celulaHeader = linhaCabecalhoTabela.getCell(i);
				linhaCabecalhoTabela.setHeightInPoints(30); 
				if (celulaHeader != null) {
					if (i == 5 && objRsRelat.size() == 0) {
						celulaHeader.setCellStyle(specialStyle);
					} else {
						celulaHeader.setCellStyle(headerStyle);
					}
				}
			}
		}
	}
	
	private void formatarMergedCells(XSSFWorkbook wb, XSSFSheet sheet) {
			for (int i = 3; i < sheet.getNumMergedRegions() - 3; i++) {
		        CellRangeAddress regiao = sheet.getMergedRegion(i);

		        RegionUtil.setBorderTop(CellStyle.BORDER_THIN, regiao, sheet, wb);
		        RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, regiao, sheet, wb);
		        RegionUtil.setBorderRight(CellStyle.BORDER_THIN, regiao, sheet, wb);
		        RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, regiao, sheet, wb);
			}
	}

	private void formatarRegistrosTabela(XSSFWorkbook wb, XSSFSheet sheet) {

		if (objRsRelat != null && !objRsRelat.isEmpty()) {
			formatarLinhasDetalhe(wb, sheet);
		} else {
			formatarDetalheVazio(wb, sheet);
		}
	}

	private void formatarDetalheVazio(XSSFWorkbook wb, XSSFSheet sheet) {

		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setWrapText(true);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(font);

		CellRangeAddress detalheVazio = new CellRangeAddress(6, 6, 0, 16);
		sheet.addMergedRegion(detalheVazio);

		XSSFRow linhaVazia = sheet.getRow(6);
		if (linhaVazia == null) {
			linhaVazia = sheet.createRow(6);
		}
		XSSFCell celulaVazia = linhaVazia.createCell(0);
		linhaVazia.setHeightInPoints((short) 20);
		celulaVazia.setCellValue("Não existem dados para o período selecionado.");
		celulaVazia.setCellStyle(headerStyle);

		RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
		RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
		RegionUtil.setBorderRight(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
		RegionUtil.setBorderTop(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
	}

	private void formatarLinhasDetalhe(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyleLeft = wb.createCellStyle();
		headerStyleLeft.setAlignment(CellStyle.ALIGN_LEFT);
		headerStyleLeft.setWrapText(true);
		headerStyleLeft.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderTop(CellStyle.BORDER_THIN);
		
		XSSFCellStyle headerStyleRight = wb.createCellStyle();
		headerStyleRight.setAlignment(CellStyle.ALIGN_RIGHT);
		headerStyleRight.setWrapText(true);
		headerStyleRight.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderTop(CellStyle.BORDER_THIN);
		
		
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		headerStyleLeft.setFont(font);
		headerStyleRight.setFont(font);
		
		
		int linhaInicial = 6;
		int linhasDetalhe = objRsRelat.size();
		int linhaFinal = linhaInicial + linhasDetalhe;

		for (int linha = linhaInicial; linha <= linhaFinal; linha++) {
			XSSFRow detalheTabela = sheet.getRow(linha);
			if (detalheTabela != null) {
				for (int i = 0; i <= detalheTabela.getLastCellNum(); i++) {
					XSSFCell celulaDetalhe = detalheTabela.getCell(i);
					if (celulaDetalhe != null) {
						if (i == 0 || i == 2 || i == 4 || i == 6 || i==7 || i==8 ||i == 9 || i == 12 || i== 15 || i == 16) {
							celulaDetalhe.setCellStyle(headerStyleLeft);
						} else {
							celulaDetalhe.setCellStyle(headerStyleRight);
						}
						
					}
				}
			}
		}
	}

	private void criarRodape(XSSFWorkbook wb, XSSFSheet sheet) {
		Font font;
		/* Criação, inclusão e formatação dos dados do footer do excel */
		XSSFRow ultimaLinha = sheet.createRow(sheet.getLastRowNum() + 1);

		XSSFCellStyle footerStyle = wb.createCellStyle();
		footerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		footerStyle.setWrapText(true);
		font = footerStyle.getFont();
		font.setFontHeight((short) 150);
		font.setFontName("Arial");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		footerStyle.setFont(font);

		XSSFCell cell1 = ultimaLinha.createCell(0);
		cell1.setCellValue(now());
		cell1.setCellStyle(footerStyle);

		XSSFCell cell2 = ultimaLinha.createCell(4);
		cell2.setCellValue("Superintêndencia de Ocorrências Especiais");
		cell2.setCellStyle(footerStyle);

		CellRangeAddress region1 = new CellRangeAddress(ultimaLinha.getRowNum(), ultimaLinha.getRowNum(), 0, 3);
		CellRangeAddress region2 = new CellRangeAddress(ultimaLinha.getRowNum(), ultimaLinha.getRowNum(), 4, 16);
		sheet.addMergedRegion(region1);
		sheet.addMergedRegion(region2);
	}

	public List<RelosControleProcessosAbertoModel> getObjRsRelat() {
		return objRsRelat;
	}


	public void setObjRsRelat(List<RelosControleProcessosAbertoModel> objRsRelat) {
		this.objRsRelat = objRsRelat;
	}


	public String getStrDtIni() {
		return strDtIni;
	}


	public void setStrDtIni(String strDtIni) {
		this.strDtIni = strDtIni;
	}


	public String getStrDtFim() {
		return strDtFim;
	}


	public void setStrDtFim(String strDtFim) {
		this.strDtFim = strDtFim;
	}


	public String getStrCodArea() {
		return strCodArea;
	}


	public void setStrCodArea(String strCodArea) {
		this.strCodArea = strCodArea;
	}

}

