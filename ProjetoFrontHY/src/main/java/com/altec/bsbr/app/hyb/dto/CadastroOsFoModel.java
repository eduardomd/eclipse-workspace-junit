package com.altec.bsbr.app.hyb.dto;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class CadastroOsFoModel {
	
	private String nrSequOrdeServ;
	private String nrSequFrauEven;
	private String nmParp;
	private String nrSequCntaBncr;
	private String nrSequCrto;
	private String cdBanc;
	private String nrCnta;
	private String nrCrto;
	private String tpCntaBncr;
	private String cdCnal;
	private String nrCpfCnpjTitl;
	private String dtAberCntaBncr;
	private String nmGereRespCntaBncr;
	private String nmSituCntaBncr;
	private String cdCentCustOrig;
	private String cdCentCustDest;
	private String cdCentCustOpnt;
	private String contabilizado;
	private String nrQntdEven;

	public String getNrSequOrdeServ() {
		return nrSequOrdeServ;
	}

	public void setNrSequOrdeServ(String nrSequOrdeServ) {
		this.nrSequOrdeServ = nrSequOrdeServ;
	}

	public String getNrSequFrauEven() {
		return nrSequFrauEven;
	}

	public void setNrSequFrauEven(String nrSequFrauEven) {
		this.nrSequFrauEven = nrSequFrauEven;
	}

	public String getNmParp() {
		return nmParp;
	}

	public void setNmParp(String nmParp) {
		this.nmParp = nmParp;
	}

	public String getNrSequCntaBncr() {
		return nrSequCntaBncr;
	}

	public void setNrSequCntaBncr(String nrSequCntaBncr) {
		this.nrSequCntaBncr = nrSequCntaBncr;
	}

	public String getNrSequCrto() {
		return nrSequCrto;
	}

	public void setNrSequCrto(String nrSequCrto) {
		this.nrSequCrto = nrSequCrto;
	}

	public String getCdBanc() {
		return cdBanc;
	}

	public void setCdBanc(String cdBanc) {
		this.cdBanc = cdBanc;
	}

	public String getNrCnta() {
		return nrCnta;
	}

	public void setNrCnta(String nrCnta) {
		this.nrCnta = nrCnta;
	}

	public String getNrCrto() {
		return nrCrto;
	}

	public void setNrCrto(String nrCrto) {
		this.nrCrto = nrCrto;
	}

	public String getTpCntaBncr() {
		return tpCntaBncr;
	}

	public void setTpCntaBncr(String tpCntaBncr) {
		this.tpCntaBncr = tpCntaBncr;
	}

	public String getCdCnal() {
		return cdCnal;
	}

	public void setCdCnal(String cdCnal) {
		this.cdCnal = cdCnal;
	}

	public String getNrCpfCnpjTitl() {
		return nrCpfCnpjTitl;
	}

	public void setNrCpfCnpjTitl(String nrCpfCnpjTitl) {
		this.nrCpfCnpjTitl = nrCpfCnpjTitl;
	}

	public String getDtAberCntaBncr() {
		return dtAberCntaBncr;
	}

	public void setDtAberCntaBncr(String dtAberCntaBncr) {
		this.dtAberCntaBncr = dtAberCntaBncr;
	}

	public String getNmGereRespCntaBncr() {
		return nmGereRespCntaBncr;
	}

	public void setNmGereRespCntaBncr(String nmGereRespCntaBncr) {
		this.nmGereRespCntaBncr = nmGereRespCntaBncr;
	}

	public String getNmSituCntaBncr() {
		return nmSituCntaBncr;
	}

	public void setNmSituCntaBncr(String nmSituCntaBncr) {
		this.nmSituCntaBncr = nmSituCntaBncr;
	}

	public String getCdCentCustOrig() {
		return cdCentCustOrig;
	}

	public void setCdCentCustOrig(String cdCentCustOrig) {
		this.cdCentCustOrig = cdCentCustOrig;
	}

	public String getCdCentCustDest() {
		return cdCentCustDest;
	}

	public void setCdCentCustDest(String cdCentCustDest) {
		this.cdCentCustDest = cdCentCustDest;
	}

	public String getCdCentCustOpnt() {
		return cdCentCustOpnt;
	}

	public void setCdCentCustOpnt(String cdCentCustOpnt) {
		this.cdCentCustOpnt = cdCentCustOpnt;
	}

	public String getContabilizado() {
		return contabilizado;
	}

	public void setContabilizado(String contabilizado) {
		this.contabilizado = contabilizado;
	}

	public String getNrQntdEven() {
		return nrQntdEven;
	}

	public void setNrQntdEven(String nrQntdEven) {
		this.nrQntdEven = nrQntdEven;
	}
	
}
