package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;

import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsRiscPoten.XHYCadOsRiscPotenEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsRiscPoten.WebServiceException;
import com.altec.bsbr.app.hyb.dto.ObjRiscPotenAn;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsDatas.XHYCadOsDatasEndPoint;

@Component("cadOsDetalheRiscoPotAnBean")
@Scope("session")
public class CadOsDetalheRiscoPotAn extends BasicBBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	private XHYCadOsRiscPotenEndPoint objRiscPotenAn;

	@Autowired
	private XHYCadOsDatasEndPoint objCadOsdatas;

	private String strNrOs;
	private String strNmProd;
	private String strIdProd;
	private String strIdTranFrau;
	private String strMsgErro;
	private int intRecCount;
	private int iCountLinhas;
	private String strTitulo;
	private String strVlTran;
	private String dtDetecaoOs;
	private List<ObjRiscPotenAn> objsRiscPotenAn;

	/* daqui pra baixo rever */
	private String NR_SEQU_RECR_EXCL_ANLI;
	private String DT_EXCL_PRCL;
	private String VL_EXCL_PRCL;
	private String DT_RECR_MES_PRCL;
	private String VL_RECR_MES_PRCL;
	private String DT_RECR_FORA_MES_PRCL;
	private String VL_RECR_FORA_MES_PRCL;
	private String DT_COMP;
	private String VL_COMP;
	private String DT_TRST;
	private String VL_TRST;
	private String DT_NEGA;
	private String VL_NEGA;

	public String getNR_SEQU_RECR_EXCL_ANLI() {
		return NR_SEQU_RECR_EXCL_ANLI;
	}

	public void setNR_SEQU_RECR_EXCL_ANLI(String nR_SEQU_RECR_EXCL_ANLI) {
		NR_SEQU_RECR_EXCL_ANLI = nR_SEQU_RECR_EXCL_ANLI;
	}

	public String getDT_EXCL_PRCL() {
		return DT_EXCL_PRCL;
	}

	public void setDT_EXCL_PRCL(String dT_EXCL_PRCL) {
		DT_EXCL_PRCL = dT_EXCL_PRCL;
	}

	public String getVL_EXCL_PRCL() {
		return VL_EXCL_PRCL;
	}

	public void setVL_EXCL_PRCL(String vL_EXCL_PRCL) {
		VL_EXCL_PRCL = vL_EXCL_PRCL;
	}

	public String getDT_RECR_MES_PRCL() {
		return DT_RECR_MES_PRCL;
	}

	public void setDT_RECR_MES_PRCL(String dT_RECR_MES_PRCL) {
		DT_RECR_MES_PRCL = dT_RECR_MES_PRCL;
	}

	public String getVL_RECR_MES_PRCL() {
		return VL_RECR_MES_PRCL;
	}

	public void setVL_RECR_MES_PRCL(String vL_RECR_MES_PRCL) {
		VL_RECR_MES_PRCL = vL_RECR_MES_PRCL;
	}

	public String getDT_RECR_FORA_MES_PRCL() {
		return DT_RECR_FORA_MES_PRCL;
	}

	public void setDT_RECR_FORA_MES_PRCL(String dT_RECR_FORA_MES_PRCL) {
		DT_RECR_FORA_MES_PRCL = dT_RECR_FORA_MES_PRCL;
	}

	public String getVL_RECR_FORA_MES_PRCL() {
		return VL_RECR_FORA_MES_PRCL;
	}

	public void setVL_RECR_FORA_MES_PRCL(String vL_RECR_FORA_MES_PRCL) {
		VL_RECR_FORA_MES_PRCL = vL_RECR_FORA_MES_PRCL;
	}

	public String getDT_COMP() {
		return DT_COMP;
	}

	public void setDT_COMP(String dT_COMP) {
		DT_COMP = dT_COMP;
	}

	public String getVL_COMP() {
		return VL_COMP;
	}

	public void setVL_COMP(String vL_COMP) {
		VL_COMP = vL_COMP;
	}

	public String getDT_TRST() {
		return DT_TRST;
	}

	public void setDT_TRST(String dT_TRST) {
		DT_TRST = dT_TRST;
	}

	public String getVL_TRST() {
		return VL_TRST;
	}

	public void setVL_TRST(String vL_TRST) {
		VL_TRST = vL_TRST;
	}

	public String getDT_NEGA() {
		return DT_NEGA;
	}

	public void setDT_NEGA(String dT_NEGA) {
		DT_NEGA = dT_NEGA;
	}

	public String getVL_NEGA() {
		return VL_NEGA;
	}

	public void setVL_NEGA(String vL_NEGA) {
		VL_NEGA = vL_NEGA;
	}

	public String getStrNrOs() {
		return strNrOs;
	}

	public void setStrNrOs(String strNrOs) {
		this.strNrOs = strNrOs;
	}

	public String getStrNmProd() {
		return strNmProd;
	}

	public void setStrNmProd(String strNmProd) {
		this.strNmProd = strNmProd;
	}

	public String getStrIdProd() {
		return strIdProd;
	}

	public void setStrIdProd(String strIdProd) {
		this.strIdProd = strIdProd;
	}

	public String getStrIdTranFrau() {
		return strIdTranFrau;
	}

	public void setStrIdTranFrau(String strIdTranFrau) {
		this.strIdTranFrau = strIdTranFrau;
	}

	public String getStrMsgErro() {
		return strMsgErro;
	}

	public void setStrMsgErro(String strMsgErro) {
		this.strMsgErro = strMsgErro;
	}

	public int getIntRecCount() {
		return intRecCount;
	}

	public void setIntRecCount(int intRecCount) {
		this.intRecCount = intRecCount;
	}

	public int getiCountLinhas() {
		return iCountLinhas;
	}

	public void setiCountLinhas(int iCountLinhas) {
		this.iCountLinhas = iCountLinhas;
	}

	public String getStrTitulo() {
		return strTitulo;
	}

	public void setStrTitulo(String strTitulo) {
		this.strTitulo = strTitulo;
	}

	public String getStrVlTran() {
		return strVlTran;
	}

	public List<ObjRiscPotenAn> getObjsRiscPotenAn() {
		return objsRiscPotenAn;
	}

	public void setObjsRiscPotenAn(List<ObjRiscPotenAn> objsRiscPotenAn) {
		objsRiscPotenAn = objsRiscPotenAn;
	}

	public void setStrVlTran(String strVlTran) {
		this.strVlTran = strVlTran;
	}

	public String getDtDetecaoOs() {
		return dtDetecaoOs;
	}

	public void setDtDetecaoOs(String dtDetecaoOs) {
		this.dtDetecaoOs = dtDetecaoOs;
	}

	@PostConstruct
	public void PostConstruct() throws IOException {

		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();

		JSONObject f;
		JSONArray pcursorObjRs = new JSONArray();

		// @SuppressWarnings("unchecked")
		Map<String, String> params = fc.getExternalContext().getRequestParameterMap();

		this.strTitulo = params.get("strTitulo");
		this.strNrOs = params.get("strNrSeqOs");
		this.strIdProd = params.get("idProd");
		this.strIdTranFrau = params.get("idTranFrau");
		this.strNmProd = params.get("nmProd").toUpperCase();

		String objRs = "";
		String objRsCadOsDatas = "";
		try {

			objRs = objRiscPotenAn.fnSelRiscPotenAn(this.strIdTranFrau);
			JSONObject objRsJson = new JSONObject(objRs);
			pcursorObjRs = objRsJson.getJSONArray("PCURSOR");

			//this.intRecCount = pcursorObjRs.length();
			this.intRecCount = 4;
			
			objRsCadOsDatas = objCadOsdatas.consultarOs(this.strNrOs);
			JSONObject objRsCadOsDatasJson = new JSONObject(objRsCadOsDatas);
			JSONArray pcursorObjRsCadOsDatas = objRsCadOsDatasJson.getJSONArray("PCURSOR");

			f = pcursorObjRsCadOsDatas.getJSONObject(0);
			this.dtDetecaoOs = f.isNull("DETECCAO") ? "" : f.get("DETECCAO").toString();

		} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsRiscPoten.WebServiceException e) {
			e.printStackTrace();
			ec.redirect("hy_erro.xhtml?&strErro=" + e.getMessage());
		} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsDatas.WebServiceException e) {
			e.printStackTrace();
			ec.redirect("hy_erro.xhtml?&strErro=" + e.getMessage());
		}

		//this.iCountLinhas = 1;
		this.strVlTran = "123345000";

		objsRiscPotenAn = new ArrayList<ObjRiscPotenAn>();
        /*for (int i = 0; i < pcursorObjRs.length(); i++) {
            JSONObject temp = pcursorObjRs.getJSONObject(i);
            
            objsRiscPotenAn.add(
            		new ObjRiscPotenAn(
            				temp.get("NR_SEQU_RECR_EXCL_ANLI").toString(),
            				temp.get("DT_EXCL_PRCL").toString(), 
            				temp.get("VL_EXCL_PRCL").toString(),
            				temp.get("DT_RECR_MES_PRCL").toString(), 
            				temp.get("VL_RECR_MES_PRCL").toString(),
            				temp.get("DT_RECR_FORA_MES_PRCL").toString(),
            				temp.get("VL_RECR_FORA_MES_PRCL").toString(),
            				temp.get("DT_COMP").toString(),
            				temp.get("VL_COMP").toString(),
            				temp.get("DT_TRST").toString(),
            				temp.get("VL_TRST").toString(),
            				temp.get("DT_NEGA").toString(),
            				temp.get("VL_NEGA").toString()
            		));
        }*/
        
        for (int i = 0; i < 4; i++) {
            //JSONObject temp = pcursorObjRs.getJSONObject(i);
            
            objsRiscPotenAn.add(
            		new ObjRiscPotenAn(
            				"NR_SEQU_RECR_EXCL_ANLI",
            				"08/02/2019", 
            				"5000",
            				"08/02/2019", 
            				"5000",
            				"08/02/2019",
            				"5000",
            				"08/02/2019",
            				"5000",
            				"08/02/2019",
            				"5000",
            				"08/02/2019",
            				"5000"
            		));
        }

	}

	public String formataData(String aStrData) {
		String strAuxData = "";
		String strDay;
		String strMonth;
		String strYear;

		return aStrData;
	}

	public String formataNumero(String number) {
		String result = "";
		if (number != null && !number.isEmpty()) {
			DecimalFormat value = new DecimalFormat("###,##0.00");
			result = value.format(Float.parseFloat(number));
			String fp = result.substring(0, result.length() - 3);
			String sp = result.substring(result.length() - 3);
			result = fp.replace(",", ".") + sp.replace(".", ",");
		}

		return result;
	}

	/* EXE */

	private boolean strReturn = false;
	private String strErro;
	private String strNR_SEQU_RECR_EXCL_ANLI;
	private String strNR_SEQU_TRAN_FRAU_CNAL;
	private String strNR_SEQU_ORDE_SERV;
	private String strCD_PROD_CORP;
	private String strDT_EXCL_PRCL;
	private String strDT_RECR_MES_PRCL;
	private String strDT_RECR_FORA_MES_PRCL;
	private String strDT_COMP;
	private String strDT_TRST;
	private String strDT_NEGA;
	private String strVL_EXCL_PRCL;
	private String strVL_RECR_MES_PRCL;
	private String strVL_RECR_FORA_MES_PRCL;
	private String strVL_COMP;
	private String strVL_TRST;
	private String strVL_NEGA;
	private String strCpfUsuario;

	public boolean getStrReturn() {
		return strReturn;
	}

	public void setStrReturn(boolean strReturn) {
		this.strReturn = strReturn;
	}

	public String getStrErro() {
		return strErro;
	}

	public void setStrErro(String strErro) {
		this.strErro = strErro;
	}

	public String getStrNR_SEQU_RECR_EXCL_ANLI() {
		return strNR_SEQU_RECR_EXCL_ANLI;
	}

	public void setStrNR_SEQU_RECR_EXCL_ANLI(String strNR_SEQU_RECR_EXCL_ANLI) {
		this.strNR_SEQU_RECR_EXCL_ANLI = strNR_SEQU_RECR_EXCL_ANLI;
	}

	public String getStrNR_SEQU_TRAN_FRAU_CNAL() {
		return strNR_SEQU_TRAN_FRAU_CNAL;
	}

	public void setStrNR_SEQU_TRAN_FRAU_CNAL(String strNR_SEQU_TRAN_FRAU_CNAL) {
		this.strNR_SEQU_TRAN_FRAU_CNAL = strNR_SEQU_TRAN_FRAU_CNAL;
	}

	public String getStrNR_SEQU_ORDE_SERV() {
		return strNR_SEQU_ORDE_SERV;
	}

	public void setStrNR_SEQU_ORDE_SERV(String strNR_SEQU_ORDE_SERV) {
		this.strNR_SEQU_ORDE_SERV = strNR_SEQU_ORDE_SERV;
	}

	public String getStrCD_PROD_CORP() {
		return strCD_PROD_CORP;
	}

	public void setStrCD_PROD_CORP(String strCD_PROD_CORP) {
		this.strCD_PROD_CORP = strCD_PROD_CORP;
	}

	public String getStrDT_EXCL_PRCL() {
		return strDT_EXCL_PRCL;
	}

	public void setStrDT_EXCL_PRCL(String strDT_EXCL_PRCL) {
		this.strDT_EXCL_PRCL = strDT_EXCL_PRCL;
	}

	public String getStrDT_RECR_MES_PRCL() {
		return strDT_RECR_MES_PRCL;
	}

	public void setStrDT_RECR_MES_PRCL(String strDT_RECR_MES_PRCL) {
		this.strDT_RECR_MES_PRCL = strDT_RECR_MES_PRCL;
	}

	public String getStrDT_RECR_FORA_MES_PRCL() {
		return strDT_RECR_FORA_MES_PRCL;
	}

	public void setStrDT_RECR_FORA_MES_PRCL(String strDT_RECR_FORA_MES_PRCL) {
		this.strDT_RECR_FORA_MES_PRCL = strDT_RECR_FORA_MES_PRCL;
	}

	public String getStrDT_COMP() {
		return strDT_COMP;
	}

	public void setStrDT_COMP(String strDT_COMP) {
		this.strDT_COMP = strDT_COMP;
	}

	public String getStrDT_TRST() {
		return strDT_TRST;
	}

	public void setStrDT_TRST(String strDT_TRST) {
		this.strDT_TRST = strDT_TRST;
	}

	public String getStrDT_NEGA() {
		return strDT_NEGA;
	}

	public void setStrDT_NEGA(String strDT_NEGA) {
		this.strDT_NEGA = strDT_NEGA;
	}

	public String getStrVL_EXCL_PRCL() {
		return strVL_EXCL_PRCL;
	}

	public void setStrVL_EXCL_PRCL(String strVL_EXCL_PRCL) {
		this.strVL_EXCL_PRCL = strVL_EXCL_PRCL;
	}

	public String getStrVL_RECR_MES_PRCL() {
		return strVL_RECR_MES_PRCL;
	}

	public void setStrVL_RECR_MES_PRCL(String strVL_RECR_MES_PRCL) {
		this.strVL_RECR_MES_PRCL = strVL_RECR_MES_PRCL;
	}

	public String getStrVL_RECR_FORA_MES_PRCL() {
		return strVL_RECR_FORA_MES_PRCL;
	}

	public void setStrVL_RECR_FORA_MES_PRCL(String strVL_RECR_FORA_MES_PRCL) {
		this.strVL_RECR_FORA_MES_PRCL = strVL_RECR_FORA_MES_PRCL;
	}

	public String getStrVL_COMP() {
		return strVL_COMP;
	}

	public void setStrVL_COMP(String strVL_COMP) {
		this.strVL_COMP = strVL_COMP;
	}

	public String getStrVL_TRST() {
		return strVL_TRST;
	}

	public void setStrVL_TRST(String strVL_TRST) {
		this.strVL_TRST = strVL_TRST;
	}

	public String getStrVL_NEGA() {
		return strVL_NEGA;
	}

	public void setStrVL_NEGA(String strVL_NEGA) {
		this.strVL_NEGA = strVL_NEGA;
	}

	public String getStrCpfUsuario() {
		return strCpfUsuario;
	}

	public void setStrCpfUsuario(String strCpfUsuario) {
		this.strCpfUsuario = strCpfUsuario;
	}

	public void VerificaErro(String strErro) throws IOException {
		if (!"".equals(strErro)) {
			ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
			// ec.redirect("hy_erro.xhtml?&strErro=" + strErro);
		}
	}

	public String CadOsDetalheRiscoPotAnExe() {
		/*
		 * Set objxmlDoc = Server.CreateObject("Microsoft.XMLDOM") objxmlDoc.async =
		 * false 'Le as informacoes XML que foram passadas para o ASP
		 * objxmlDoc.load(Request)
		 */

		Object objxmlDoc;
		/*
		 * this.strNR_SEQU_ORDE_SERV =
		 * objxmlDoc.documentelement.childnodes(0).text.trim(); this.strCD_PROD_CORP =
		 * objxmlDoc.documentelement.childnodes(1).text.trim();
		 * this.strNR_SEQU_TRAN_FRAU_CNAL =
		 * objxmlDoc.documentelement.childnodes(2).text.trim();
		 * this.strNR_SEQU_RECR_EXCL_ANLI =
		 * objxmlDoc.documentelement.childnodes(3).text.trim(); this.strDT_EXCL_PRCL =
		 * objxmlDoc.documentelement.childnodes(4).text.trim(); this.strVL_EXCL_PRCL =
		 * objxmlDoc.documentelement.childnodes(5).text.replace(".", "").trim();
		 * this.strDT_RECR_MES_PRCL =
		 * objxmlDoc.documentelement.childnodes(6).text.trim(); this.strVL_RECR_MES_PRCL
		 * = objxmlDoc.documentelement.childnodes(7).text.replace(".", "").trim();
		 * this.strDT_RECR_FORA_MES_PRCL =
		 * objxmlDoc.documentelement.childnodes(8).text.trim();
		 * this.strVL_RECR_FORA_MES_PRCL =
		 * objxmlDoc.documentelement.childnodes(9).text.replace(".", "").trim();
		 * this.strDT_COMP = objxmlDoc.documentelement.childnodes(10).text.trim();
		 * this.strVL_COMP = objxmlDoc.documentelement.childnodes(11).text.replace(".",
		 * "").trim(); this.strDT_TRST =
		 * objxmlDoc.documentelement.childnodes(12).text.trim(); this.strVL_TRST =
		 * objxmlDoc.documentelement.childnodes(13).text.replace(".", "").trim();
		 * this.strDT_NEGA = objxmlDoc.documentelement.childnodes(14).text.trim();
		 * this.strVL_NEGA = objxmlDoc.documentelement.childnodes(15).text.replace(".",
		 * "").trim();
		 */

		this.strVL_EXCL_PRCL = "".equals(this.strVL_EXCL_PRCL) ? null : this.strVL_EXCL_PRCL;
		this.strVL_RECR_MES_PRCL = "".equals(this.strVL_RECR_MES_PRCL) ? null : this.strVL_RECR_MES_PRCL;
		this.strVL_RECR_FORA_MES_PRCL = "".equals(this.strVL_RECR_FORA_MES_PRCL) ? null : this.strVL_RECR_FORA_MES_PRCL;
		this.strVL_COMP = "".equals(this.strVL_COMP) ? null : this.strVL_COMP;
		this.strVL_TRST = "".equals(this.strVL_TRST) ? null : this.strVL_TRST;
		this.strVL_NEGA = "".equals(this.strVL_NEGA) ? null : this.strVL_NEGA;

		ClsCadOsRiscPoten objCadOsRiscPoten = new ClsCadOsRiscPoten();

		String strReturn = "";

		/*if ("".equals(this.strNR_SEQU_RECR_EXCL_ANLI)) {
			if (!("".equals(this.strDT_EXCL_PRCL) && this.strVL_EXCL_PRCL == null && "".equals(this.strDT_RECR_MES_PRCL)
					&& this.strVL_RECR_MES_PRCL == null && "".equals(this.strDT_RECR_FORA_MES_PRCL)
					&& this.strVL_RECR_FORA_MES_PRCL == null && "".equals(this.strDT_COMP) && this.strVL_COMP == null
					&& "".equals(this.strDT_TRST) && this.strVL_TRST == null && "".equals(this.strDT_NEGA)
					&& this.strVL_NEGA == null)) {

				strReturn = objCadOsRiscPoten.fnInsValRiscPotenAnli(this.strNR_SEQU_TRAN_FRAU_CNAL,
						this.strDT_EXCL_PRCL, this.strVL_EXCL_PRCL, this.strDT_RECR_MES_PRCL, this.strVL_RECR_MES_PRCL,
						this.strDT_RECR_FORA_MES_PRCL, this.strVL_RECR_FORA_MES_PRCL, this.strDT_COMP, this.strVL_COMP,
						this.strDT_TRST, this.strVL_TRST, this.strDT_NEGA, this.strVL_NEGA, this.strErro);
			} else {
				return "True";
			}
		} else {
			if ("".equals(this.strDT_EXCL_PRCL) && this.strVL_EXCL_PRCL == null && "".equals(this.strDT_RECR_MES_PRCL)
					&& this.strVL_RECR_MES_PRCL == null && "".equals(this.strDT_RECR_FORA_MES_PRCL)
					&& this.strVL_RECR_FORA_MES_PRCL == null && "".equals(this.strDT_COMP) && this.strVL_COMP == null
					&& "".equals(this.strDT_TRST) && this.strVL_TRST == null && "".equals(this.strDT_NEGA)
					&& this.strVL_NEGA == null) {
				strReturn = objCadOsRiscPoten.fnDelValRiscPotenAnli(this.strNR_SEQU_RECR_EXCL_ANLI, this.strErro);
			} else {
				strReturn = objCadOsRiscPoten.fnUpdValRiscPotenAnli(this.strNR_SEQU_RECR_EXCL_ANLI,
						this.strDT_EXCL_PRCL, this.strVL_EXCL_PRCL, this.strDT_RECR_MES_PRCL, this.strVL_RECR_MES_PRCL,
						this.strDT_RECR_FORA_MES_PRCL, this.strVL_RECR_FORA_MES_PRCL, this.strDT_COMP, this.strVL_COMP,
						this.strDT_TRST, this.strVL_TRST, this.strDT_NEGA, this.strVL_NEGA, this.strErro);
			}
		}

		if (!"".equals(this.strErro)) {
			strReturn = this.strErro;
		}*/

		return strReturn;
	}

}
