package com.altec.bsbr.app.hyb.dto;

public class PcorpModel {

	private int id;
	private String produto;
	private String sla;
	private boolean debito;
	private boolean credito;
	
	public PcorpModel(int id, String produto, String sla, boolean debito, boolean credito) {
		
		this.id = id;
		this.produto = produto;
		this.sla = sla;
		this.debito = debito;
		this.credito = credito;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProduto() {
		return produto;
	}

	public void setProduto(String produto) {
		this.produto = produto;
	}

	public String getSla() {
		return sla;
	}

	public void setSla(String sla) {
		this.sla = sla;
	}

	public boolean isDebito() {
		return debito;
	}

	public void setDebito(boolean debito) {
		this.debito = debito;
	}

	public boolean isCredito() {
		return credito;
	}

	public void setCredito(boolean credito) {
		this.credito = credito;
	}	
}
