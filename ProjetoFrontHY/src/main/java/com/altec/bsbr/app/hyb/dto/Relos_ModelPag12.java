package com.altec.bsbr.app.hyb.dto;

public class Relos_ModelPag12 {
	private String mes;
	private int internet;
	private int SuperLinha;
	private int cartaoCredito;
	private int cartaoDebito;
	private int total;
	
	public Relos_ModelPag12(String mes, int internet, int superLinha, int cartaoCredito, int cartaoDebito) {
		this.mes = mes;
		this.internet = internet;
		this.SuperLinha = superLinha;
		this.cartaoCredito = cartaoCredito;
		this.cartaoDebito = cartaoDebito;
		calculaTotal();
	}
	
	private void calculaTotal() {
		this.total = this.internet + this.SuperLinha + this.cartaoCredito + this.cartaoDebito;
	}
	
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
		calculaTotal();
	}
	public int getInternet() {
		return internet;
	}
	public void setInternet(int internet) {
		this.internet = internet;
		calculaTotal();
	}
	public int getSuperLinha() {
		return SuperLinha;
	}
	public void setSuperLinha(int superLinha) {
		SuperLinha = superLinha;
		calculaTotal();
	}
	public int getCartaoCredito() {
		return cartaoCredito;
	}
	public void setCartaoCredito(int cartaoCredito) {
		this.cartaoCredito = cartaoCredito;
		calculaTotal();
	}
	public int getCartaoDebito() {
		return cartaoDebito;
	}
	public void setCartaoDebito(int cartaoDebito) {
		this.cartaoDebito = cartaoDebito;
		calculaTotal();
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
		calculaTotal();
	}
}
