package com.altec.bsbr.app.hyb.dto;

public class EncerrarOsModel {

	private String CD_SITU;
	private String SITUACAO;
	private int CD_NOTI;
	private String NM_NOTI;
	private String TX_ABERTURA;
	private String TX_ENCERRAMENTO;
	private String PARECER_GEST;
	private String TX_FALHAS;
	private String TX_OUTRAS_PROP;
	private String PARECER_JURI;
	
	public EncerrarOsModel() {};
	
	public String getCD_SITU() {
		return CD_SITU;
	}
	public void setCD_SITU(String CD_SITU) {
		this.CD_SITU = CD_SITU;
	}
	
	public String getSITUACAO() {
		return SITUACAO;
	}
	public void setSITUACAO(String SITUACAO) {
		this.SITUACAO = SITUACAO;
	}

	public int getCD_NOTI() {
		return CD_NOTI;
	}
	public void setCD_NOTI(int CD_NOTI) {
		this.CD_NOTI = CD_NOTI;
	}

	public String getNM_NOTI() {
		return NM_NOTI;
	}
	public void setNM_NOTI(String NM_NOTI) {
		this.NM_NOTI = NM_NOTI;
	}

	public String getTX_ABERTURA() {
		return TX_ABERTURA;
	}

	public void setTX_ABERTURA(String tX_ABERTURA) {
		TX_ABERTURA = tX_ABERTURA;
	}

	public String getTX_ENCERRAMENTO() {
		return TX_ENCERRAMENTO;
	}

	public void setTX_ENCERRAMENTO(String tX_ENCERRAMENTO) {
		TX_ENCERRAMENTO = tX_ENCERRAMENTO;
	}

	public String getPARECER_GEST() {
		return PARECER_GEST;
	}

	public void setPARECER_GEST(String pARECER_GEST) {
		PARECER_GEST = pARECER_GEST;
	}

	public String getTX_FALHAS() {
		return TX_FALHAS;
	}

	public void setTX_FALHAS(String tX_FALHAS) {
		TX_FALHAS = tX_FALHAS;
	}

	public String getTX_OUTRAS_PROP() {
		return TX_OUTRAS_PROP;
	}

	public void setTX_OUTRAS_PROP(String tX_OUTRAS_PROP) {
		TX_OUTRAS_PROP = tX_OUTRAS_PROP;
	}

	public String getPARECER_JURI() {
		return PARECER_JURI;
	}

	public void setPARECER_JURI(String pARECER_JURI) {
		PARECER_JURI = pARECER_JURI;
	}
}
