package com.altec.bsbr.app.hyb.dto;

import java.util.List;

public class EventosModel {
	
    private int id;
	private String evento;
	//private List<EventoCanal> eventoCanal;
	private boolean coluna1;
	private boolean coluna2;
	private boolean coluna3;
	
	public EventosModel(int id, String evento, boolean coluna1, boolean coluna2, boolean coluna3) {
		
		this.id = id;
		this.evento = evento;
		this.coluna1 = coluna1;
		this.coluna2 = coluna2;
		this.coluna3 = coluna3;
		//this.setEventoCanal(event);
	}

	public EventosModel(int id, String evento, boolean coluna1) {
		this.id = id;
		this.evento = evento;
		this.coluna1 = coluna1;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEvento() {
		return evento;
	}

	public void setEvento(String evento) {
		this.evento = evento;
	}

	public boolean isColuna1() {
		return coluna1;
	}

	public void setColuna1(boolean coluna1) {
		this.coluna1 = coluna1;
	}

	public boolean isColuna2() {
		return coluna2;
	}

	public void setColuna2(boolean coluna2) {
		this.coluna2 = coluna2;
	}

	public boolean isColuna3() {
		return coluna3;
	}

	public void setColuna3(boolean coluna3) {
		this.coluna3 = coluna3;
	}

/*	public List<EventoCanal> getEventoCanal() {
		return eventoCanal;
	}

	public void setEventoCanal(List<EventoCanal> eventoCanal) {
		this.eventoCanal = eventoCanal;
	}*/


}