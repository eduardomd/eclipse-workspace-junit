package com.altec.bsbr.app.hyb.dto;

public class PE95_PEM9310B_PEM931CAeaResponse {
	
	public PE95_PEM9310B_PEM931CAeaResponse() {}
	
	public PE95_PEM9310B_PEM931CAeaResponse(String dC_FORMATO, String pENUMPE, String tIPDOC, String pENUMDO,
			String sECDOC, String cLASEG, String pENOMFA, String pEPRIAP, String pESEGAP, String pENOMPE,
			String pETIPPE, String pEFECNA, String pEESTPE, String pECONPE, String pENIVAC, String pEINDRE,
			String pEINDCO, String pEINDGR, String pEINDAV, String pEHSTAM, String pETIPVI, String pENOMCA,
			String pENUMBL, String pENOMLO, String pENOMCO, String pECODPO, String pERUTCA, String cODPRV,
			String pECODPA, String pEINDN1, String pEINDN2, String pEINDN3, String pEINDN4, String pEINDN5,
			String pEOBSE1, String pEOBSE2, String pEMARNO, String iNDSEG, String sEGCLA, String fECSEG, String sEGMAN,
			String mARPER, String pESEGCA, String sEGANT, String cRIANT) {
		super();
		DC_FORMATO = dC_FORMATO;
		PENUMPE = pENUMPE;
		TIPDOC = tIPDOC;
		PENUMDO = pENUMDO;
		SECDOC = sECDOC;
		CLASEG = cLASEG;
		PENOMFA = pENOMFA;
		PEPRIAP = pEPRIAP;
		PESEGAP = pESEGAP;
		PENOMPE = pENOMPE;
		PETIPPE = pETIPPE;
		PEFECNA = pEFECNA;
		PEESTPE = pEESTPE;
		PECONPE = pECONPE;
		PENIVAC = pENIVAC;
		PEINDRE = pEINDRE;
		PEINDCO = pEINDCO;
		PEINDGR = pEINDGR;
		PEINDAV = pEINDAV;
		PEHSTAM = pEHSTAM;
		PETIPVI = pETIPVI;
		PENOMCA = pENOMCA;
		PENUMBL = pENUMBL;
		PENOMLO = pENOMLO;
		PENOMCO = pENOMCO;
		PECODPO = pECODPO;
		PERUTCA = pERUTCA;
		CODPRV = cODPRV;
		PECODPA = pECODPA;
		PEINDN1 = pEINDN1;
		PEINDN2 = pEINDN2;
		PEINDN3 = pEINDN3;
		PEINDN4 = pEINDN4;
		PEINDN5 = pEINDN5;
		PEOBSE1 = pEOBSE1;
		PEOBSE2 = pEOBSE2;
		PEMARNO = pEMARNO;
		INDSEG = iNDSEG;
		SEGCLA = sEGCLA;
		FECSEG = fECSEG;
		SEGMAN = sEGMAN;
		MARPER = mARPER;
		PESEGCA = pESEGCA;
		SEGANT = sEGANT;
		CRIANT = cRIANT;
	}

	private String DC_FORMATO;
	
	private String PENUMPE;
	
	private String TIPDOC;
	
	private String PENUMDO;
	
	private String SECDOC;
	
	private String CLASEG;
	
	private String PENOMFA;
	
	private String PEPRIAP;
	
	private String PESEGAP;
	
	private String PENOMPE;
	
	private String PETIPPE;
	
	private String PEFECNA;
	
	private String PEESTPE;

	private String PECONPE;

	private String PENIVAC;

	private String PEINDRE;

	private String PEINDCO;

	private String PEINDGR;
	
	private String PEINDAV;
	
	private String PEHSTAM;

	private String PETIPVI;

	private String PENOMCA;
	
	private String PENUMBL;
	
	private String PENOMLO;
	
	private String PENOMCO;
	
	private String PECODPO;
	
	private String PERUTCA;
	
	private String CODPRV;
	
	private String PECODPA;
	
	private String PEINDN1;
	
	private String PEINDN2;
	
	private String PEINDN3;
	
	private String PEINDN4;
	
	private String PEINDN5;
	
	private String PEOBSE1;
	
	private String PEOBSE2;
	
	private String PEMARNO;
	
	private String INDSEG;
	
	private String SEGCLA;
	
	private String FECSEG;
	
	private String SEGMAN;
	
	private String MARPER;
	
	private String PESEGCA;

	private String SEGANT;
	
	private String CRIANT;

	public String getDC_FORMATO() {
		return DC_FORMATO;
	}

	public void setDC_FORMATO(String dC_FORMATO) {
		DC_FORMATO = dC_FORMATO;
	}

	public String getPENUMPE() {
		return PENUMPE;
	}

	public void setPENUMPE(String pENUMPE) {
		PENUMPE = pENUMPE;
	}

	public String getTIPDOC() {
		return TIPDOC;
	}

	public void setTIPDOC(String tIPDOC) {
		TIPDOC = tIPDOC;
	}

	public String getPENUMDO() {
		return PENUMDO;
	}

	public void setPENUMDO(String pENUMDO) {
		PENUMDO = pENUMDO;
	}

	public String getSECDOC() {
		return SECDOC;
	}

	public void setSECDOC(String sECDOC) {
		SECDOC = sECDOC;
	}

	public String getCLASEG() {
		return CLASEG;
	}

	public void setCLASEG(String cLASEG) {
		CLASEG = cLASEG;
	}

	public String getPENOMFA() {
		return PENOMFA;
	}

	public void setPENOMFA(String pENOMFA) {
		PENOMFA = pENOMFA;
	}

	public String getPEPRIAP() {
		return PEPRIAP;
	}

	public void setPEPRIAP(String pEPRIAP) {
		PEPRIAP = pEPRIAP;
	}

	public String getPESEGAP() {
		return PESEGAP;
	}

	public void setPESEGAP(String pESEGAP) {
		PESEGAP = pESEGAP;
	}

	public String getPENOMPE() {
		return PENOMPE;
	}

	public void setPENOMPE(String pENOMPE) {
		PENOMPE = pENOMPE;
	}

	public String getPETIPPE() {
		return PETIPPE;
	}

	public void setPETIPPE(String pETIPPE) {
		PETIPPE = pETIPPE;
	}

	public String getPEFECNA() {
		return PEFECNA;
	}

	public void setPEFECNA(String pEFECNA) {
		PEFECNA = pEFECNA;
	}

	public String getPEESTPE() {
		return PEESTPE;
	}

	public void setPEESTPE(String pEESTPE) {
		PEESTPE = pEESTPE;
	}

	public String getPECONPE() {
		return PECONPE;
	}

	public void setPECONPE(String pECONPE) {
		PECONPE = pECONPE;
	}

	public String getPENIVAC() {
		return PENIVAC;
	}

	public void setPENIVAC(String pENIVAC) {
		PENIVAC = pENIVAC;
	}

	public String getPEINDRE() {
		return PEINDRE;
	}

	public void setPEINDRE(String pEINDRE) {
		PEINDRE = pEINDRE;
	}

	public String getPEINDCO() {
		return PEINDCO;
	}

	public void setPEINDCO(String pEINDCO) {
		PEINDCO = pEINDCO;
	}

	public String getPEINDGR() {
		return PEINDGR;
	}

	public void setPEINDGR(String pEINDGR) {
		PEINDGR = pEINDGR;
	}

	public String getPEINDAV() {
		return PEINDAV;
	}

	public void setPEINDAV(String pEINDAV) {
		PEINDAV = pEINDAV;
	}

	public String getPEHSTAM() {
		return PEHSTAM;
	}

	public void setPEHSTAM(String pEHSTAM) {
		PEHSTAM = pEHSTAM;
	}

	public String getPETIPVI() {
		return PETIPVI;
	}

	public void setPETIPVI(String pETIPVI) {
		PETIPVI = pETIPVI;
	}

	public String getPENOMCA() {
		return PENOMCA;
	}

	public void setPENOMCA(String pENOMCA) {
		PENOMCA = pENOMCA;
	}

	public String getPENUMBL() {
		return PENUMBL;
	}

	public void setPENUMBL(String pENUMBL) {
		PENUMBL = pENUMBL;
	}

	public String getPENOMLO() {
		return PENOMLO;
	}

	public void setPENOMLO(String pENOMLO) {
		PENOMLO = pENOMLO;
	}

	public String getPENOMCO() {
		return PENOMCO;
	}

	public void setPENOMCO(String pENOMCO) {
		PENOMCO = pENOMCO;
	}

	public String getPECODPO() {
		return PECODPO;
	}

	public void setPECODPO(String pECODPO) {
		PECODPO = pECODPO;
	}

	public String getPERUTCA() {
		return PERUTCA;
	}

	public void setPERUTCA(String pERUTCA) {
		PERUTCA = pERUTCA;
	}

	public String getCODPRV() {
		return CODPRV;
	}

	public void setCODPRV(String cODPRV) {
		CODPRV = cODPRV;
	}

	public String getPECODPA() {
		return PECODPA;
	}

	public void setPECODPA(String pECODPA) {
		PECODPA = pECODPA;
	}

	public String getPEINDN1() {
		return PEINDN1;
	}

	public void setPEINDN1(String pEINDN1) {
		PEINDN1 = pEINDN1;
	}

	public String getPEINDN2() {
		return PEINDN2;
	}

	public void setPEINDN2(String pEINDN2) {
		PEINDN2 = pEINDN2;
	}

	public String getPEINDN3() {
		return PEINDN3;
	}

	public void setPEINDN3(String pEINDN3) {
		PEINDN3 = pEINDN3;
	}

	public String getPEINDN4() {
		return PEINDN4;
	}

	public void setPEINDN4(String pEINDN4) {
		PEINDN4 = pEINDN4;
	}

	public String getPEINDN5() {
		return PEINDN5;
	}

	public void setPEINDN5(String pEINDN5) {
		PEINDN5 = pEINDN5;
	}

	public String getPEOBSE1() {
		return PEOBSE1;
	}

	public void setPEOBSE1(String pEOBSE1) {
		PEOBSE1 = pEOBSE1;
	}

	public String getPEOBSE2() {
		return PEOBSE2;
	}

	public void setPEOBSE2(String pEOBSE2) {
		PEOBSE2 = pEOBSE2;
	}

	public String getPEMARNO() {
		return PEMARNO;
	}

	public void setPEMARNO(String pEMARNO) {
		PEMARNO = pEMARNO;
	}

	public String getINDSEG() {
		return INDSEG;
	}

	public void setINDSEG(String iNDSEG) {
		INDSEG = iNDSEG;
	}

	public String getSEGCLA() {
		return SEGCLA;
	}

	public void setSEGCLA(String sEGCLA) {
		SEGCLA = sEGCLA;
	}

	public String getFECSEG() {
		return FECSEG;
	}

	public void setFECSEG(String fECSEG) {
		FECSEG = fECSEG;
	}

	public String getSEGMAN() {
		return SEGMAN;
	}

	public void setSEGMAN(String sEGMAN) {
		SEGMAN = sEGMAN;
	}

	public String getMARPER() {
		return MARPER;
	}

	public void setMARPER(String mARPER) {
		MARPER = mARPER;
	}

	public String getPESEGCA() {
		return PESEGCA;
	}

	public void setPESEGCA(String pESEGCA) {
		PESEGCA = pESEGCA;
	}

	public String getSEGANT() {
		return SEGANT;
	}

	public void setSEGANT(String sEGANT) {
		SEGANT = sEGANT;
	}

	public String getCRIANT() {
		return CRIANT;
	}

	public void setCRIANT(String cRIANT) {
		CRIANT = cRIANT;
	}
}
