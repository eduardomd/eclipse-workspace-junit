package com.altec.bsbr.app.hyb.web.util;

import java.io.IOException;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

public class VerificaErroUtil {

	public static final String empty = new String();

	public static void verificaErro(String strErro) throws IOException {

		if (!strErro.equals(empty)) {
			ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
			ec.redirect("hy_erro.xhtml?&strErro=" + strErro);
		}
		
		

	}

}
