package com.altec.bsbr.app.hyb.dto;

import javax.faces.bean.ManagedBean;

@ManagedBean(name = "objRsEquipe")
public class ObjRsEquipe {
	private String NR_MATR_PRSV;
	private String NM_RECU_OCOR_ESPC;
	private String NM_DESIGNACAO;
	private String TP_DESI;
	private String CD_CARG;	
	
	public ObjRsEquipe() {}

	public ObjRsEquipe(String nR_MATR_PRSV, String nM_RECU_OCOR_ESPC, String nM_DESIGNACAO, String tP_DESI,
			String cD_CARG) {
		super();
		NR_MATR_PRSV = nR_MATR_PRSV;
		NM_RECU_OCOR_ESPC = nM_RECU_OCOR_ESPC;
		NM_DESIGNACAO = nM_DESIGNACAO;
		TP_DESI = tP_DESI;
		CD_CARG = cD_CARG;
	}

	public String getNR_MATR_PRSV() {
		return NR_MATR_PRSV;
	}

	public void setNR_MATR_PRSV(String nR_MATR_PRSV) {
		NR_MATR_PRSV = nR_MATR_PRSV;
	}

	public String getNM_RECU_OCOR_ESPC() {
		return NM_RECU_OCOR_ESPC;
	}

	public void setNM_RECU_OCOR_ESPC(String nM_RECU_OCOR_ESPC) {
		NM_RECU_OCOR_ESPC = nM_RECU_OCOR_ESPC;
	}

	public String getNM_DESIGNACAO() {
		return NM_DESIGNACAO;
	}

	public void setNM_DESIGNACAO(String nM_DESIGNACAO) {
		NM_DESIGNACAO = nM_DESIGNACAO;
	}

	public String getTP_DESI() {
		return TP_DESI;
	}

	public void setTP_DESI(String tP_DESI) {
		TP_DESI = tP_DESI;
	}
	
	public String getCD_CARG() {
		return CD_CARG;
	}

	public void setCD_CARG(String cD_CARG) {
		CD_CARG = cD_CARG;
	}
	
}
