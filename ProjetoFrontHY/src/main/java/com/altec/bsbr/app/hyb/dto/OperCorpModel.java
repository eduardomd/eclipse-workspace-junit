package com.altec.bsbr.app.hyb.dto;

public class OperCorpModel {
	
	private String NR_SEQU_TIPO_OPER_TRAN;
	private String CD_TRAN;
	private String NM_SUB_OPER;
	private String NM_OPER;
		
	public OperCorpModel() {
		
	}
	
	public OperCorpModel(String nR_SEQU_TIPO_OPER_TRAN, String cD_TRAN, String nM_SUB_OPER, String nM_OPER) {
		super();
		NR_SEQU_TIPO_OPER_TRAN = nR_SEQU_TIPO_OPER_TRAN;
		CD_TRAN = cD_TRAN;
		NM_SUB_OPER = nM_SUB_OPER;
		NM_OPER = nM_OPER;
	}
	public String getNR_SEQU_TIPO_OPER_TRAN() {
		return NR_SEQU_TIPO_OPER_TRAN;
	}
	public void setNR_SEQU_TIPO_OPER_TRAN(String nR_SEQU_TIPO_OPER_TRAN) {
		NR_SEQU_TIPO_OPER_TRAN = nR_SEQU_TIPO_OPER_TRAN;
	}
	public String getCD_TRAN() {
		return CD_TRAN;
	}
	public void setCD_TRAN(String cD_TRAN) {
		CD_TRAN = cD_TRAN;
	}
	public String getNM_SUB_OPER() {
		return NM_SUB_OPER;
	}
	public void setNM_SUB_OPER(String nM_SUB_OPER) {
		NM_SUB_OPER = nM_SUB_OPER;
	}

	public String getLabel() {
		return this.CD_TRAN + (this.NM_OPER.isEmpty() ? "": " - ".concat(this.NM_OPER)) + (this.NM_SUB_OPER.isEmpty() ? "" : " - ".concat(this.NM_SUB_OPER) );
	}

	public String getNM_OPER() {
		return NM_OPER;
	}

	public void setNM_OPER(String nM_OPER) {
		NM_OPER = nM_OPER;
	}
}
