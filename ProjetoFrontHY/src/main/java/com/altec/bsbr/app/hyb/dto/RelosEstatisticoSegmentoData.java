package com.altec.bsbr.app.hyb.dto;

public class RelosEstatisticoSegmentoData {

	private String DT_MM_YYYY;
	private String DT_MONTH_YYYY;
	
	public RelosEstatisticoSegmentoData() {}

	public String getDT_MM_YYYY() {
		return DT_MM_YYYY;
	}

	public void setDT_MM_YYYY(String dT_MM_YYYY) {
		DT_MM_YYYY = dT_MM_YYYY;
	}

	public String getDT_MONTH_YYYY() {
		return DT_MONTH_YYYY;
	}

	public void setDT_MONTH_YYYY(String dT_MONTH_YYYY) {
		DT_MONTH_YYYY = dT_MONTH_YYYY;
	}

	
	
}
