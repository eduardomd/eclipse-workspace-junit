package com.altec.bsbr.app.hyb.web.jsf;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.swing.JOptionPane;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

 
@Component("hyRelosCustoTotalOs")
@Scope("session")

public class HyRelosTotalOsBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Date data = new Date(System.currentTimeMillis());
	private Number update = 7;
	
	//forma de pegar o valor pela url
	//private Object strDtIni = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("pDtIni");
	//private Object strDtFim = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("pDtFim");

	private String strErro;
	private String objRelat;
	private Object objRsRelat = "";
	private String strDtIni = "11/11/2011";
	private String strDtFim = "12/12/2012";

	private String ROWNUM;
	private String NR_SEQU_ORDE_SERV;
	private String NM_SITU_ORDE_SERV;
	private String DT_ABER_ORDE_SERV;
	private String DT_ENCERRAMENTO;
	private String VL_ENVO;
	private String VL_PERD_EFET;
	private Number VL_CUST_ORDE_DE_SERV;
	private Number dblTotalCusto;

	
	public HyRelosTotalOsBean() {
		
	}
	
	public HyRelosTotalOsBean (String ROWNUM, String NR_SEQU_ORDE_SERV, String NM_SITU_ORDE_SERV, String DT_ABER_ORDE_SERV, String DT_ENCERRAMENTO, 
			String VL_ENVO, String VL_PERD_EFET, Number VL_CUST_ORDE_DE_SERV) {
		super();
		this.setROWNUM(ROWNUM);
		this.setNR_SEQU_ORDE_SERV(NR_SEQU_ORDE_SERV);
		this.setNM_SITU_ORDE_SERV(NM_SITU_ORDE_SERV);
		this.setDT_ABER_ORDE_SERV(DT_ABER_ORDE_SERV);
		this.setDT_ENCERRAMENTO(DT_ENCERRAMENTO);
		this.setVL_ENVO(VL_ENVO);
		this.setVL_PERD_EFET(VL_PERD_EFET);
		this.setVL_CUST_ORDE_DE_SERV(VL_CUST_ORDE_DE_SERV);
		
	}
	

	/**
	 * @return the strErro
	 */
	public String getStrErro() {
		return strErro;
	}

	/**
	 * @param strErro the strErro to set
	 */
	public void setStrErro(String strErro) {
		this.strErro = strErro;
	}

	/**
	 * @return the objRelat
	 */
	public String getObjRelat() {
		return objRelat;
	}

	/**
	 * @param objRelat the objRelat to set
	 */
	public void setObjRelat(String objRelat) {
		this.objRelat = objRelat;
	}

	/**
	 * @return the objRsRelat
	 */
	public Object getObjRsRelat() {
		return objRsRelat;
	}

	/**
	 * @param objRsRelat the objRsRelat to set
	 */
	public void setObjRsRelat(Object objRsRelat) {
		this.objRsRelat = objRsRelat;
	}

	/**
	 * @return the strDtIni
	 */
	public String getStrDtIni() {
		return strDtIni;
	}

	/**
	 * @param strDtIni the strDtIni to set
	 */
	public void setStrDtIni(String strDtIni) {
		this.strDtIni = strDtIni;
	}

	/**
	 * @return the strDtFim
	 */
	public String getStrDtFim() {
		return strDtFim;
	}

	/**
	 * @param strDtFim the strDtFim to set
	 */
	public void setStrDtFim(String strDtFim) {
		this.strDtFim = strDtFim;
	}

	
	/**
	 * @return the data
	 */
	public Date getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(Date data) {
		this.data = data;
	}

	/**
	 * @return the update
	 */
	public Number getUpdate() {
		return update;
	}

	/**
	 * @param update the update to set
	 */
	public void setUpdate(Number update) {
		this.update = update;
	}

	/**
	 * @return the nM_SITU_ORDE_SERV
	 */
	public String getNM_SITU_ORDE_SERV() {
		return NM_SITU_ORDE_SERV;
	}

	/**
	 * @param nM_SITU_ORDE_SERV the nM_SITU_ORDE_SERV to set
	 */
	public void setNM_SITU_ORDE_SERV(String nM_SITU_ORDE_SERV) {
		NM_SITU_ORDE_SERV = nM_SITU_ORDE_SERV;
	}

	/**
	 * @return the dT_ENCERRAMENTO
	 */
	public String getDT_ENCERRAMENTO() {
		return DT_ENCERRAMENTO;
	}

	/**
	 * @param dT_ENCERRAMENTO the dT_ENCERRAMENTO to set
	 */
	public void setDT_ENCERRAMENTO(String dT_ENCERRAMENTO) {
		DT_ENCERRAMENTO = dT_ENCERRAMENTO;
	}

	/**
	 * @return the vL_PERD_EFET
	 */
	public String getVL_PERD_EFET() {
		return VL_PERD_EFET;
	}

	/**
	 * @param vL_PERD_EFET the vL_PERD_EFET to set
	 */
	public void setVL_PERD_EFET(String vL_PERD_EFET) {
		VL_PERD_EFET = vL_PERD_EFET;
	}

	/**
	 * @return the vL_CUST_ORDE_DE_SERV
	 */
	public Number getVL_CUST_ORDE_DE_SERV() {
		return VL_CUST_ORDE_DE_SERV;
	}

	/**
	 * @param vL_CUST_ORDE_DE_SERV the vL_CUST_ORDE_DE_SERV to set
	 */
	public void setVL_CUST_ORDE_DE_SERV(Number vL_CUST_ORDE_DE_SERV) {
		VL_CUST_ORDE_DE_SERV = vL_CUST_ORDE_DE_SERV;
	}

	/**
	 * @return the nR_SEQU_ORDE_SERV
	 */
	public String getNR_SEQU_ORDE_SERV() {
		return NR_SEQU_ORDE_SERV;
	}

	/**
	 * @param nR_SEQU_ORDE_SERV the nR_SEQU_ORDE_SERV to set
	 */
	public void setNR_SEQU_ORDE_SERV(String nR_SEQU_ORDE_SERV) {
		NR_SEQU_ORDE_SERV = nR_SEQU_ORDE_SERV;
	}

	/**
	 * @return the dT_ABER_ORDE_SERV
	 */
	public String getDT_ABER_ORDE_SERV() {
		return DT_ABER_ORDE_SERV;
	}

	/**
	 * @param dT_ABER_ORDE_SERV the dT_ABER_ORDE_SERV to set
	 */
	public void setDT_ABER_ORDE_SERV(String dT_ABER_ORDE_SERV) {
		DT_ABER_ORDE_SERV = dT_ABER_ORDE_SERV;
	}

	/**
	 * @return the vL_ENVO
	 */
	public String getVL_ENVO() {
		return VL_ENVO;
	}

	/**
	 * @param vL_ENVO the vL_ENVO to set
	 */
	public void setVL_ENVO(String vL_ENVO) {
		VL_ENVO = vL_ENVO;
	}

	/**
	 * @return the dblTotalCusto
	 */
	public Number getDblTotalCusto() {
		return dblTotalCusto;
	}

	/**
	 * @param dblTotalCusto the dblTotalCusto to set
	 */
	public void setDblTotalCusto(Number dblTotalCusto) {
		this.dblTotalCusto = dblTotalCusto;
	}

	/**
	 * @return the rOWNUM
	 */
	public String getROWNUM() {
		return ROWNUM;
	}

	/**
	 * @param rOWNUM the rOWNUM to set
	 */
	public void setROWNUM(String rOWNUM) {
		ROWNUM = rOWNUM;
	}
}
