package com.altec.bsbr.app.hyb.dto;

public class PE57_PEM802_PEM80AAeaResponse {

	public PE57_PEM802_PEM80AAeaResponse() {}
	
	public PE57_PEM802_PEM80AAeaResponse(String dC_FORMATO, String pEOBSE1, String pEOBSE2, String pENUMCO,
			String pECODOF, String pECODEN) {
		super();
		DC_FORMATO = dC_FORMATO;
		PEOBSE1 = pEOBSE1;
		PEOBSE2 = pEOBSE2;
		PENUMCO = pENUMCO;
		PECODOF = pECODOF;
		PECODEN = pECODEN;
	}

	private String DC_FORMATO;

	private String PEOBSE1;
	
	private String PEOBSE2;
	
	private String PENUMCO;
	
	private String PECODOF;
	
	private String PECODEN;
	
	public String getDC_FORMATO() {
		return DC_FORMATO;
	}
	
	public void setDC_FORMATO(String dC_FORMATO) {
		DC_FORMATO = dC_FORMATO;
	}

	public String getPEOBSE1() {
		return PEOBSE1;
	}

	public void setPEOBSE1(String pEOBSE1) {
		PEOBSE1 = pEOBSE1;
	}

	public String getPEOBSE2() {
		return PEOBSE2;
	}

	public void setPEOBSE2(String pEOBSE2) {
		PEOBSE2 = pEOBSE2;
	}

	public String getPENUMCO() {
		return PENUMCO;
	}

	public void setPENUMCO(String pENUMCO) {
		PENUMCO = pENUMCO;
	}

	public String getPECODOF() {
		return PECODOF;
	}

	public void setPECODOF(String pECODOF) {
		PECODOF = pECODOF;
	}

	public String getPECODEN() {
		return PECODEN;
	}

	public void setPECODEN(String pECODEN) {
		PECODEN = pECODEN;
	}

}
