package com.altec.bsbr.app.hyb.dto;

public class EncerrarOsPenaModel {

	private int CODIGO;
	private String NOME;
	
	public EncerrarOsPenaModel() {}

	public int getCODIGO() {
		return CODIGO;
	}

	public void setCODIGO(int CODIGO) {
		this.CODIGO = CODIGO;
	}

	public String getNOME() {
		return NOME;
	}

	public void setNOME(String NOME) {
		this.NOME = NOME;
	}

}
