package com.altec.bsbr.app.hyb.dto;

public class OperAuxModel {
	
	private String CD_PROD_AUXI;
	private String NM_PROD_AUXI;
	
	public OperAuxModel() {
		
	}
	
	public OperAuxModel(String cD_PROD_AUXI, String nM_PROD_AUXI) {
		super();
		CD_PROD_AUXI = cD_PROD_AUXI;
		NM_PROD_AUXI = nM_PROD_AUXI;
	}
	public String getCD_PROD_AUXI() {
		return CD_PROD_AUXI;
	}
	public void setCD_PROD_AUXI(String cD_PROD_AUXI) {
		CD_PROD_AUXI = cD_PROD_AUXI;
	}
	public String getNM_PROD_AUXI() {
		return NM_PROD_AUXI;
	}
	public void setNM_PROD_AUXI(String nM_PROD_AUXI) {
		NM_PROD_AUXI = nM_PROD_AUXI;
	}
	
	
	

}
