package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.CadastroosDetalheprodutofe;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsCanais.XHYCadOsCanaisEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;


@Component("CadastroOsDetalheProdutoFEBean")
@Scope("session")
public class CadastroOsDetalheProdutoFEBean extends BasicBBean {

	private static final long serialVersionUID = 1L;

	private String strNrSeqTranFrau;
	
	private CadastroosDetalheprodutofe cadastroosDetalheprodutofe;

	@Autowired
	private XHYCadOsCanaisEndPoint cadOsCanais;
	
	@PostConstruct
	public void init() {
		System.out.println("init CadastroOsDetalheProdutoFEBean!!!!: "+FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqTranFrau"));

		if (FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqTranFrau") != null) {
			this.strNrSeqTranFrau = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqTranFrau").toString();
			retornaOperacaoEspecifica();
		}
	}
	
	public String formataRenavam(String renavam) {
		return ("000000000000" + renavam).substring(("000000000000"+renavam).length() - 12);
	}
	
	public void retornaOperacaoEspecifica() {
		try {
			JSONObject json = new JSONObject(
					cadOsCanais.retornaOperacaoEspecifica(this.strNrSeqTranFrau));
			JSONArray pcursor = json.getJSONArray("PCURSOR");

			for(int i = 0; i < pcursor.length(); i++)			
				this.cadastroosDetalheprodutofe = new CadastroosDetalheprodutofe(pcursor.getJSONObject(i).isNull("NR_SEQU_ORDE_SERV") ? "" : pcursor.getJSONObject(i).getString("NR_SEQU_ORDE_SERV")  , 
						pcursor.getJSONObject(i).isNull("DESC_TRAN") ? "" : pcursor.getJSONObject(i).getString("DESC_TRAN")  , 
						pcursor.getJSONObject(i).isNull("NM_LOCA_TRAN") ? "" : pcursor.getJSONObject(i).getString("NM_LOCA_TRAN")  , 
						pcursor.getJSONObject(i).isNull("CD_BARR") ? "" : pcursor.getJSONObject(i).getString("CD_BARR")  , 
						pcursor.getJSONObject(i).isNull("TX_DADO_COMP") ? "" : pcursor.getJSONObject(i).getString("TX_DADO_COMP")  , 
						pcursor.getJSONObject(i).isNull("NM_CLIE_FAVD") ? "" : pcursor.getJSONObject(i).getString("NM_CLIE_FAVD")  , 
						pcursor.getJSONObject(i).isNull("NR_CPF_CNPJ_CLIE_FAVD") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("NR_CPF_CNPJ_CLIE_FAVD"))  , 
						pcursor.getJSONObject(i).isNull("NR_CNTA_CLIE_FAVD") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("NR_CNTA_CLIE_FAVD"))  , 
						pcursor.getJSONObject(i).isNull("NR_RENAVAM") ? "" : String.valueOf(pcursor.getJSONObject(i).getLong("NR_RENAVAM"))  , 
						pcursor.getJSONObject(i).isNull("CD_VEIC") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("CD_VEIC"))  , 
						pcursor.getJSONObject(i).isNull("NM_COR_VEIC") ? "" : pcursor.getJSONObject(i).getString("NM_COR_VEIC")  , 
						pcursor.getJSONObject(i).isNull("NR_ANO_VEIC") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("NR_ANO_VEIC"))  , 
						pcursor.getJSONObject(i).isNull("NR_PLAC_VEIC") ? "" : pcursor.getJSONObject(i).getString("NR_PLAC_VEIC")  , 
						pcursor.getJSONObject(i).isNull("NR_CHAS_VEIC") ? "" : pcursor.getJSONObject(i).getString("NR_CHAS_VEIC")  , 
						pcursor.getJSONObject(i).isNull("NR_TITU") ? "" : pcursor.getJSONObject(i).getString("NR_TITU")  , 
						pcursor.getJSONObject(i).isNull("NM_BANC_CRED") ? "" : pcursor.getJSONObject(i).getString("NM_BANC_CRED")  , 
						pcursor.getJSONObject(i).isNull("NM_CEDE_TITU") ? "" : pcursor.getJSONObject(i).getString("NM_CEDE_TITU")  , 
						pcursor.getJSONObject(i).isNull("NM_DEVE") ? "" : pcursor.getJSONObject(i).getString("NM_DEVE")  , 
						pcursor.getJSONObject(i).isNull("NR_CPF_CNPJ_DEVE") ? "" : pcursor.getJSONObject(i).getString("NR_CPF_CNPJ_DEVE")  , 
						pcursor.getJSONObject(i).isNull("NR_CELU") ? "" : pcursor.getJSONObject(i).getString("NR_CELU")  , 
						pcursor.getJSONObject(i).isNull("VL_TRAN") ? "" : pcursor.getJSONObject(i).get("VL_TRAN").toString() , 
						pcursor.getJSONObject(i).isNull("TP_CNTA_CLIE_FAVD") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("TP_CNTA_CLIE_FAVD")));
			
			//Formatar RENAVAM
			this.cadastroosDetalheprodutofe.setNR_RENAVAM(this.formataRenavam(this.cadastroosDetalheprodutofe.getNR_RENAVAM()));
			
		} catch (Exception e) {
			e.printStackTrace();
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	public void salvar() {
		try {
			System.out.println("Model "+ToStringBuilder.reflectionToString(this.cadastroosDetalheprodutofe));
		
			JSONObject json = new JSONObject(
					cadOsCanais.salvarDetalheOperacaoFE(this.strNrSeqTranFrau,
						this.cadastroosDetalheprodutofe.getNM_LOCA_TRAN(),
						this.cadastroosDetalheprodutofe.getCD_BARR(),
						this.cadastroosDetalheprodutofe.getTX_DADO_COMP(),
						this.cadastroosDetalheprodutofe.getNM_CLIE_FAVD(),
						this.cadastroosDetalheprodutofe.getNR_CNTA_CLIE_FAVD(),
						this.cadastroosDetalheprodutofe.getTP_CNTA_CLIE_FAVD(),
						this.cadastroosDetalheprodutofe.getNR_CPF_CNPJ_CLIE_FAVD(),
						this.cadastroosDetalheprodutofe.getNR_RENAVAM(),
						this.cadastroosDetalheprodutofe.getCD_VEIC(),
						this.cadastroosDetalheprodutofe.getNM_COR_VEIC(),
						this.cadastroosDetalheprodutofe.getNR_ANO_VEIC(),
						this.cadastroosDetalheprodutofe.getNR_PLAC_VEIC(),
						this.cadastroosDetalheprodutofe.getNR_CHAS_VEIC(),
						this.cadastroosDetalheprodutofe.getNR_TITU(),
						this.cadastroosDetalheprodutofe.getNM_BANC_CRED(),
						this.cadastroosDetalheprodutofe.getNM_CEDE_TITU(),
						this.cadastroosDetalheprodutofe.getNM_DEVE(),
						this.cadastroosDetalheprodutofe.getNR_CPF_CNPJ_DEVE(),
						this.cadastroosDetalheprodutofe.getNR_CELU()));
			
			RequestContext.getCurrentInstance().execute("alert('Dados Salvos com Sucesso!');");
		} catch (Exception e) {
			e.printStackTrace();
			RequestContext.getCurrentInstance().execute("alert('Erro: Não foi possível salvar os dados!');");
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	public String getStrNrSeqTranFrau() {
		return strNrSeqTranFrau;
	}

	public void setStrNrSeqTranFrau(String strNrSeqTranFrau) {
		this.strNrSeqTranFrau = strNrSeqTranFrau;
	}

	public CadastroosDetalheprodutofe getCadastroosDetalheprodutofe() {
		return cadastroosDetalheprodutofe;
	}

	public void setCadastroosDetalheprodutofe(CadastroosDetalheprodutofe cadastroosDetalheprodutofe) {
		this.cadastroosDetalheprodutofe = cadastroosDetalheprodutofe;
	}
	
	

}