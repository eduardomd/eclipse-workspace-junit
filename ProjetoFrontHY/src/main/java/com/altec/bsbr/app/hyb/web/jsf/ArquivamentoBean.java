package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;

import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.Hyb_arquivamentoos_model;
import com.altec.bsbr.app.hyb.web.util.XHYUsuarioIncService;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAcoesOs.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAcoesOs.XHYAcoesOsEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;

@Component("arquivamentoOsBean")
@Scope("session")
public class ArquivamentoBean extends BasicBBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String strNrSeqOs;
	private String strAction;
	private String txtHdNrSeqOs;
	private String strXml;
	private Date txtDate_PL;
	private Date txtDate_PG;
	private Date txtDate_AL;
	private Date txtDate_AG;
	
	@Autowired
	private XHYAcoesOsEndPoint admAcoes;

	@Autowired 
	private XHYUsuarioIncService userServ;
	
	private Hyb_arquivamentoos_model objRsOs; 

	public ArquivamentoBean() {
	}
	
	@PostConstruct
	public void init() throws JSONException, ParseException {
		setStrNrSeqOs(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs"));
		consultaDados();
		
	}

	public void verificarPermissao(ComponentSystemEvent event) {
		String[] perfis = {"SUPERINT","GER"};
		
		userServ.verificarPermissao(perfis);
	}
	
	/*public ArquivamentoBean(String strNrSeqOs, Hyb_arquivamentoos_model objRsOs) {
		this.strNrSeqOs = strNrSeqOs;
		this.objRsOs = objRsOs;
	}*/
	
	public void consultaDados()
	{
		String retorno;
		objRsOs = new Hyb_arquivamentoos_model();
		System.out.println("chegk" + (String) this.getStrNrSeqOs());
		try {
			
			retorno = admAcoes.consultarOS((String) this.getStrNrSeqOs());
			System.out.println(retorno);
			JSONObject pesqOS = new JSONObject(retorno);
			JSONArray pcursor = pesqOS.getJSONArray("PCURSOR");
			
			JSONObject item = pcursor.getJSONObject(0);
			
			SimpleDateFormat parseFormat =  new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyy", Locale.US);
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
			
			// format.format(parseFormat.parse(item.get("DT_ARQUIVAMENTO_L").toString()));
		
			this.objRsOs.setARQUIVO_GERAL(item.isNull("ARQUIVO_GERAL") == true ? "" :  item.get("ARQUIVO_GERAL").toString());
			this.objRsOs.setPASTA_L(item.isNull("PASTA_L") == true ? "" : item.get("PASTA_L").toString());
			this.objRsOs.setRESP_ARQ(item.isNull("RESP_ARQ") == true ? "" :  item.get("RESP_ARQ").toString());
			this.objRsOs.setCAIXA(item.isNull("CAIXA") == true ? "" :  item.get("CAIXA").toString()); 
			this.objRsOs.setPASTA_G(item.isNull("PASTA_G") == true ? "" :  item.get("PASTA_G").toString());
			this.objRsOs.setEMPRESA_RESP(item.isNull("EMPRESA_RESP") == true ? "" :  item.get("EMPRESA_RESP").toString());
			this.objRsOs.setPARECER_JURI(item.isNull("PARECER_JURI") == true ? "" :  item.get("PARECER_JURI").toString());
			this.objRsOs.setCD_NOTI(item.isNull("CD_NOTI") == true ? "" :  item.get("CD_NOTI").toString());
			this.objRsOs.setCD_SITU(item.isNull("CD_SITU") == true ? "" :  item.get("CD_SITU").toString());
			this.objRsOs.setDT_ARQUIVAMENTO_L(item.isNull("DT_ARQUIVAMENTO_L") == true ? "" :  item.get("DT_ARQUIVAMENTO_L").toString());
			this.objRsOs.setDT_PRESC_L(item.isNull("DT_PRESC_L") == true ? "" :  item.get("DT_PRESC_L").toString());
			this.objRsOs.setDT_ARQUIVAMENTO_G(item.isNull("DT_ARQUIVAMENTO_G") == true ? "" :  item.get("DT_ARQUIVAMENTO_G").toString());
			this.objRsOs.setDT_PRESC_G(item.isNull("DT_PRESC_G") == true ? "" :  item.get("DT_PRESC_G").toString());
			

		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public String getStrXml() {
		return strXml;
	}

	public void setStrXml(String strXml) {
		this.strXml = strXml;
	}

	public String getTxtHdNrSeqOs() {
		return txtHdNrSeqOs;
	}

	public void setTxtHdNrSeqOs(String txtHdNrSeqOs) {
		this.txtHdNrSeqOs = txtHdNrSeqOs;
	}

	public Hyb_arquivamentoos_model getObjRsOs() {
		return objRsOs;
	}

	public void setObjRsOs(Hyb_arquivamentoos_model objRsOs) {
		this.objRsOs = objRsOs;
	}
	
	public String getStrNrSeqOs() {
		return strNrSeqOs;
	}

	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}
	public String getStrAction() {
		return strAction;
	}
	public void setStrAction(String strAction) {
		this.strAction = strAction;
	}

	/*public static boolean ValidaData(String inDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        dateFormat.setLenient(false);
        try {
            dateFormat.parse(inDate.trim());
        } catch (ParseException pe) {
            return false;
        }
        return true;
    }*/
	
	public void setStrActionStr() {
		this.setStrAction("S");
	}

	public void formSubmit() {
			
			String retorno = "";
			System.out.println(convertDataComply(this.objRsOs.getDT_ARQUIVAMENTO_G()) + "- " +this.objRsOs.getDT_PRESC_L() +"-"+ this.objRsOs.getDT_PRESC_G() +"-"+ this.objRsOs.getDT_ARQUIVAMENTO_L() +"-" + this.objRsOs.getDT_ARQUIVAMENTO_G());
			
			System.out.println(this.getTxtDate_AG());

		    String dtArquivL = convertDataComply(this.objRsOs.getDT_ARQUIVAMENTO_L());
			String dtPrescL = convertDataComply(this.objRsOs.getDT_PRESC_L());
			String dtArquivG = convertDataComply(this.objRsOs.getDT_ARQUIVAMENTO_G());
			String dtPrescG = convertDataComply(this.objRsOs.getDT_PRESC_G());
		
			System.out.println(dtArquivL + " " + dtPrescG + " " +  dtArquivG + " " + dtPrescL);
			
			//Instanciando a classe que salva o arquivamento.
			try {
				retorno =  admAcoes.salvarArquivamentoOs(this.getStrNrSeqOs().toString(), 
														 this.objRsOs.getARQUIVO_GERAL(),
														 this.objRsOs.getPASTA_L(), 
														 dtArquivL,
														 this.objRsOs.getRESP_ARQ(),
														 dtPrescL,
														 this.objRsOs.getCAIXA(),
														 this.objRsOs.getPASTA_G(),
														 dtArquivG,
														 this.objRsOs.getEMPRESA_RESP(),
														 dtPrescG,
														 this.objRsOs.getPARECER_JURI(), "", "", "", "");
				
				System.out.println("TEST: ");
				System.out.println(retorno);
				
			} catch (WebServiceException e) {
				e.printStackTrace();
			}
					
		
			
			consultaDados();
			//System.out.println(retorno);
			RequestContext.getCurrentInstance().execute("alert('Dados salvos com sucesso!')");
			
			//cleanSession();
			System.out.println(this.getStrNrSeqOs().toString());
			
		
	}

	public XHYAcoesOsEndPoint getAdmAcoes() {
		return admAcoes;
	}

	public void setAdmAcoes(XHYAcoesOsEndPoint admAcoes) {
		this.admAcoes = admAcoes;
	}
/*	
	public String convertData(String dataInput) {
		
		String novaData = "" ;
        
        try {
               novaData = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyy", Locale.US).parse(dataInput));
        } catch (ParseException e) {
               // TODO Auto-generated catch block
               e.printStackTrace();
        }
        
        return novaData;

	}*/
	
	public void Voltar() throws IOException {
		
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		ec.redirect("hyb_pesq_tramite.xhtml");
		
	}

	public String convertDataComply(String dataString)
	{
		SimpleDateFormat parseFormat =  new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyy", Locale.US);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		String result = "";
		
		try {
			Date date = parseFormat.parse(dataString);
			result = format.format(date);
			System.out.println("NEW DATE: " + result);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
		
	}
	
	/*public Date convertToDate(String dateInput)
	{
		DateFormat sourceFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = null;
		try {
			date = sourceFormat.parse(dateInput);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}*/
	
	public void cleanSession() {
        System.out.println("\n --CLEAN SESSION  --");
        FacesContext context = FacesContext.getCurrentInstance(); 
        context.getExternalContext().getSessionMap().remove("PesqTramiteBean");
        RequestContext.getCurrentInstance().execute("window.location.href = window.location.href");
	}
	
	public void reload() throws IOException {
	    ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
	    ec.redirect("hyb_pesq_tramite.xhtml");
	}

	
	public Date getTxtDate_PL() {
		return txtDate_PL;
	}

	public void setTxtDate_PL(Date txtDate_PL) {
		this.txtDate_PL = txtDate_PL;
	}

	public Date getTxtDate_PG() {
		return txtDate_PG;
	}

	public void setTxtDate_PG(Date txtDate_PG) {
		this.txtDate_PG = txtDate_PG;
	}

	public Date getTxtDate_AL() {
		return txtDate_AL;
	}

	public void setTxtDate_AL(Date txtDate_AL) {
		this.txtDate_AL = txtDate_AL;
	}

	public Date getTxtDate_AG() {
		return txtDate_AG;
	}

	public void setTxtDate_AG(Date txtDate_AG) {
		this.txtDate_AG = txtDate_AG;
	}
	
}
