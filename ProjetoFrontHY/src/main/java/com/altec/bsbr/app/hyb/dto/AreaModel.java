package com.altec.bsbr.app.hyb.dto;

import java.util.ArrayList;
import java.util.List;

public class AreaModel {
	private int id;
	private String area;

	public AreaModel(int codigo, String nome) {
		this.id = codigo;
		this.area = nome;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

}
