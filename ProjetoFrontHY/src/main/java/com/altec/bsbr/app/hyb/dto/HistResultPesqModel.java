package com.altec.bsbr.app.hyb.dto;

public class HistResultPesqModel {
	
	private String CD_EVEN;
	private String NM_RESP;
	private String DT_ENTR;
	private String CD_AGEN;
	private String NM_SITU;
	private String DS_TITU;
	
	public HistResultPesqModel()
	{
		
	}
	
	public HistResultPesqModel(String cd_EVEN, String nm_RESP, String dt_ENTR, String cd_AGEN, String nm_SITU, String ds_TITU)
	{
		super();
		this.CD_EVEN = cd_EVEN;
		this.NM_RESP = nm_RESP;
		this.DT_ENTR = dt_ENTR;
		this.CD_AGEN = cd_AGEN;
		this.NM_SITU = nm_SITU;
		this.DS_TITU = ds_TITU;
		
	}
	
	public String getNM_RESP() {
		return NM_RESP;
	}
	public void setNM_RESP(String nM_RESP) {
		NM_RESP = nM_RESP;
	}
	public String getCD_EVEN() {
		return CD_EVEN;
	}
	public void setCD_EVEN(String cD_EVEN) {
		CD_EVEN = cD_EVEN;
	}
	public String getDT_ENTR() {
		return DT_ENTR;
	}
	public void setDT_ENTR(String dT_ENTR) {
		DT_ENTR = dT_ENTR;
	}
	public String getCD_AGEN() {
		return CD_AGEN;
	}
	public void setCD_AGEN(String cD_AGEN) {
		CD_AGEN = cD_AGEN;
	}
	public String getNM_SITU() {
		return NM_SITU;
	}
	public void setNM_SITU(String nM_SITU) {
		NM_SITU = nM_SITU;
	}
	public String getDS_TITU() {
		return DS_TITU;
	}
	public void setDS_TITU(String dS_TITU) {
		DS_TITU = dS_TITU;
	}
	
}
