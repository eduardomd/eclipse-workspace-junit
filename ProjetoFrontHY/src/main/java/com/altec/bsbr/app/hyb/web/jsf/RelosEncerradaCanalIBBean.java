package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.RelosEncerradaCanalIBBModel;
import com.altec.bsbr.app.jab.hyb.webclient.XHYRelatorios.XHYRelatoriosEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.ibm.icu.text.NumberFormat;

@Component("relosEncerradaCanalIBBean")
@Scope("session")
public class RelosEncerradaCanalIBBean extends BasicBBean {
private static final long serialVersionUID = 1L;
	
	@Autowired
	private XHYRelatoriosEndPoint objRelat;
	
	private List<RelosEncerradaCanalIBBModel> objRsRelat;

	
	private String strArea;
	private String pCodArea;
	private String strTpRel;
	private String strNmRelat;
	private String strDtIni;
	private String strDtFim;
	private String intCanal;
	private int linhaAtual; 
	
	ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
	JSONArray pcursorObjRs = new JSONArray();
	
	@PostConstruct
    public void init() {	
		objRsRelat = new ArrayList<RelosEncerradaCanalIBBModel>();
		FacesContext fc = FacesContext.getCurrentInstance();
		linhaAtual = 0;
		try {
			Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
							
			strArea = params.get("pArea");
			pCodArea = params.get("pCodArea");
		    strTpRel = params.get("pTpRel");
		    strNmRelat = params.get("pNmRel");
		    strDtIni = params.get("pDtIni");
		    strDtFim = params.get("pDtFim");
		    intCanal = params.get("pCdCanal");
		    
		    
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
		
		relatoriosDadosGeraisOs();
		relatoriosDadosCartaoSeguranca();
		relatoriosDebitosEfetivados();
		relatoriosCreditosEfetivados();
		relatoriosDadosAbaterPrejuizoExclusoes();
		relatoriosDadosAbaterPrejuizoRecuperacoes();
		relatoriosContestacaoADebito();
		relatoriosContestacaoACredito();
		cleanSession();
	}
	
	public void cleanSession() {
		FacesContext context = FacesContext.getCurrentInstance();
		if (context.getExternalContext().getSessionMap().get("relatorioosBean") != null) {
			context.getExternalContext().getSessionMap().remove("relatorioosBean");
		}
	}
	
	public void relatoriosDadosGeraisOs() {
		try {
			String objRs = objRelat.fnRelOSEncerradasCanais(this.intCanal, "1", this.strDtIni, this.strDtFim, this.pCodArea );
			JSONObject objResTemp = new JSONObject(objRs);
			pcursorObjRs = objResTemp.getJSONArray("PCURSOR");
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		for (int i = 0; i < pcursorObjRs.length(); i++) {
            JSONObject temp = pcursorObjRs.getJSONObject(i);
       
            
            RelosEncerradaCanalIBBModel relosCd = new RelosEncerradaCanalIBBModel();
            relosCd.setTipoTabela(1);
            relosCd.setRow(temp.isNull("ROWNUM") ? i+1:Integer.valueOf(temp.get("ROWNUM").toString()));
            relosCd.setnOS(temp.isNull("NR_SEQU_ORDE_SERV") ? "":temp.get("NR_SEQU_ORDE_SERV").toString());
            relosCd.setDataAbertura(temp.isNull("DT_ABER_ORDE_SERV") ? "":temp.get("DT_ABER_ORDE_SERV").toString());
            relosCd.setDataEvento(temp.isNull("DATA") ? "":temp.get("DATA").toString());
            if(!temp.isNull("DT_PREV_ENCE_ORDE_SERV")) {
            	Date date = null;
            	String dateJson = "";
            	if(temp.get("DT_PREV_ENCE_ORDE_SERV").toString().contains("+")) {
            		dateJson = temp.get("DT_PREV_ENCE_ORDE_SERV").toString().substring(0,temp.get("DT_PREV_ENCE_ORDE_SERV").toString().indexOf("+"));
            		
            	} else { 
            		dateJson = temp.get("DT_PREV_ENCE_ORDE_SERV").toString();
            	}
				try {
					date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(dateJson);
				} catch (JSONException | ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            	String dataFormatada = new SimpleDateFormat("dd/MM/yyyy").format(date);
            	 relosCd.setDataEncerramento(dataFormatada);
            } else {
            	 relosCd.setDataEncerramento("");
            }
            relosCd.setAgencia(temp.isNull("CD_BANC") ? "":temp.get("CD_BANC").toString());
            relosCd.setCc(temp.isNull("NR_CNTA") ? "":temp.get("NR_CNTA").toString());
            relosCd.setCliente(temp.isNull("NM_PARP") ? "":temp.get("NM_PARP").toString());
            relosCd.setPfPj(temp.isNull("TP_PESSOA") ? "":temp.get("TP_PESSOA").toString());
            relosCd.setValorEnvolvido(temp.isNull("VL_ENVO") ? 0:Double.valueOf(temp.get("VL_ENVO").toString()));
            relosCd.setValorPrejuizo(temp.isNull("VL_PREJ") ? 0:Double.valueOf(temp.get("VL_PREJ").toString()));
            relosCd.setIpCliente(temp.isNull("NR_ENDE_IP_CLIE") ? "":temp.get("NR_ENDE_IP_CLIE").toString());
            relosCd.setIpFraude(temp.isNull("NR_ENDE_IP_FRAU") ? "":temp.get("NR_ENDE_IP_FRAU").toString());
            objRsRelat.add(relosCd);
        }
		
		
	}	

	
	public void relatoriosDadosCartaoSeguranca() {
		try {
			String objRs = objRelat.fnRelOSEncerradasCanais(this.intCanal, "4", this.strDtIni, this.strDtFim, this.pCodArea );
			JSONObject objResTemp = new JSONObject(objRs);
			pcursorObjRs = objResTemp.getJSONArray("PCURSOR");
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		for (int i = 0; i < pcursorObjRs.length(); i++) {
            JSONObject temp = pcursorObjRs.getJSONObject(i);

            RelosEncerradaCanalIBBModel relosCd = new RelosEncerradaCanalIBBModel();
            relosCd.setTipoTabela(4);
            relosCd.setRow(temp.isNull("ROWNUM") ? i+1:Integer.valueOf(temp.get("ROWNUM").toString()));
            relosCd.setnOS(temp.isNull("NR_SEQU_ORDE_SERV") ? "":temp.get("NR_SEQU_ORDE_SERV").toString());
            relosCd.setSolicitacao(temp.isNull("DT_SOLI_CRTO_SEGR") ? "":temp.get("DT_SOLI_CRTO_SEGR").toString());
            relosCd.setAtivacao(temp.isNull("DT_ATIV_CRTO_SEGR") ? "":temp.get("DT_ATIV_CRTO_SEGR").toString());
            relosCd.setCancelamento(temp.isNull("DT_CANC_CRTO_SEGR") ? "":temp.get("DT_CANC_CRTO_SEGR").toString());
            relosCd.setIndeferidas(temp.isNull("DATA") ? "":temp.get("DATA").toString());
            relosCd.setClienteInformou(temp.isNull("IN_INFO_PPUP") ? "":temp.get("IN_INFO_PPUP").toString());
            relosCd.setRessarcido(temp.isNull("IN_RESS") ? "":temp.get("IN_RESS").toString());
            objRsRelat.add(relosCd);
     
        }
	}
	public void relatoriosDebitosEfetivados() {
		try {
			String objRs = objRelat.fnRelOSEncerradasCanaisOper(this.intCanal, "1", this.strDtIni, this.strDtFim, this.pCodArea );
			JSONObject objResTemp = new JSONObject(objRs);
			pcursorObjRs = objResTemp.getJSONArray("PCURSOR");
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		for (int i = 0; i < pcursorObjRs.length(); i++) {
            JSONObject temp = pcursorObjRs.getJSONObject(i);

            RelosEncerradaCanalIBBModel relosCd = new RelosEncerradaCanalIBBModel();
            relosCd.setTipoTabela(5);
            relosCd.setRow(temp.isNull("ROWNUM") ? i+1:Integer.valueOf(temp.get("ROWNUM").toString()));
            relosCd.setnOS(temp.isNull("NR_SEQU_ORDE_SERV") ? "":temp.get("NR_SEQU_ORDE_SERV").toString());
            relosCd.setTipoProduto(temp.isNull("TP_PRODUTO") ? "":temp.get("TP_PRODUTO").toString());
            relosCd.setOperacoes(temp.isNull("OPERACAO") ? "":temp.get("OPERACAO").toString());
            relosCd.setQuantidade(temp.isNull("QUANTIDADE") ? 0:Integer.valueOf(temp.get("QUANTIDADE").toString()));
            objRsRelat.add(relosCd);
     
        }
	}
	public void relatoriosCreditosEfetivados() {
		try {
			String objRs = objRelat.fnRelOSEncerradasCanaisOper(this.intCanal, "2", this.strDtIni, this.strDtFim, this.pCodArea );
			JSONObject objResTemp = new JSONObject(objRs);
			pcursorObjRs = objResTemp.getJSONArray("PCURSOR");
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		for (int i = 0; i < pcursorObjRs.length(); i++) {
            JSONObject temp = pcursorObjRs.getJSONObject(i);

            RelosEncerradaCanalIBBModel relosCd = new RelosEncerradaCanalIBBModel();
            relosCd.setTipoTabela(6);
            relosCd.setRow(temp.isNull("ROWNUM") ? i+1:Integer.valueOf(temp.get("ROWNUM").toString()));
            relosCd.setnOS(temp.isNull("NR_SEQU_ORDE_SERV") ? "":temp.get("NR_SEQU_ORDE_SERV").toString());
            relosCd.setTipoProduto(temp.isNull("TP_PRODUTO") ? "":temp.get("TP_PRODUTO").toString());
            relosCd.setOperacoes(temp.isNull("OPERACAO") ? "":temp.get("OPERACAO").toString());
            relosCd.setQuantidade(temp.isNull("QUANTIDADE") ? 0:Integer.valueOf(temp.get("QUANTIDADE").toString()));
            objRsRelat.add(relosCd);
     
        }
	}
	public void relatoriosDadosAbaterPrejuizoExclusoes() {
		try {
			String objRs = objRelat.fnRelOSEncerradasCanaisRisc(this.intCanal, "1", this.strDtIni, this.strDtFim, this.pCodArea );
			JSONObject objResTemp = new JSONObject(objRs);
			pcursorObjRs = objResTemp.getJSONArray("PCURSOR");
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		for (int i = 0; i < pcursorObjRs.length(); i++) {
            JSONObject temp = pcursorObjRs.getJSONObject(i);

            RelosEncerradaCanalIBBModel relosCd = new RelosEncerradaCanalIBBModel();
            relosCd.setTipoTabela(7);
            relosCd.setRow(temp.isNull("ROWNUM") ? i+1:Integer.valueOf(temp.get("ROWNUM").toString()));
            relosCd.setnOS(temp.isNull("NR_SEQU_ORDE_SERV") ? "":temp.get("NR_SEQU_ORDE_SERV").toString());
            relosCd.setOperacoes(temp.isNull("OPERACAO") ? "":temp.get("OPERACAO").toString());
            relosCd.setQuantidade(temp.isNull("QUANTIDADE") ? 0:Integer.valueOf(temp.get("QUANTIDADE").toString()));
            objRsRelat.add(relosCd);
     
        }
	}
	public void relatoriosDadosAbaterPrejuizoRecuperacoes() {
		try {
			String objRs = objRelat.fnRelOSEncerradasCanaisRisc(this.intCanal, "2", this.strDtIni, this.strDtFim, this.pCodArea );
			JSONObject objResTemp = new JSONObject(objRs);
			pcursorObjRs = objResTemp.getJSONArray("PCURSOR");
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		for (int i = 0; i < pcursorObjRs.length(); i++) {
            JSONObject temp = pcursorObjRs.getJSONObject(i);

            RelosEncerradaCanalIBBModel relosCd = new RelosEncerradaCanalIBBModel();
            relosCd.setTipoTabela(8);
            relosCd.setRow(temp.isNull("ROWNUM") ? i+1:Integer.valueOf(temp.get("ROWNUM").toString()));
            relosCd.setnOS(temp.isNull("NR_SEQU_ORDE_SERV") ? "":temp.get("NR_SEQU_ORDE_SERV").toString());
            relosCd.setOperacoes(temp.isNull("OPERACAO") ? "":temp.get("OPERACAO").toString());
            relosCd.setQuantidade(temp.isNull("QUANTIDADE") ? 0:Integer.valueOf(temp.get("QUANTIDADE").toString()));
            objRsRelat.add(relosCd);
     
        }
	}
	public void relatoriosContestacaoADebito() {
		try {
			String objRs = objRelat.fnRelOSEncerradasCanaisTrans(this.intCanal, "1", this.strDtIni, this.strDtFim, this.pCodArea );
			JSONObject objResTemp = new JSONObject(objRs);
			pcursorObjRs = objResTemp.getJSONArray("PCURSOR");
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		for (int i = 0; i < pcursorObjRs.length(); i++) {
            JSONObject temp = pcursorObjRs.getJSONObject(i);

            RelosEncerradaCanalIBBModel relosCd = new RelosEncerradaCanalIBBModel();
            relosCd.setTipoTabela(9);
            relosCd.setRow(temp.isNull("ROWNUM") ? i+1:Integer.valueOf(temp.get("ROWNUM").toString()));
            relosCd.setOperacoes(temp.isNull("OPERACAO") ? "":temp.get("OPERACAO").toString());
            relosCd.setValorTotal(temp.isNull("VL_TOTAL") ? 0.00:Double.valueOf(temp.get("VL_TOTAL").toString()));
            relosCd.setValorExcluido(temp.isNull("VL_EXCL") ? 0.00:Double.valueOf(temp.get("VL_EXCL").toString()));
            relosCd.setValorRecuperado(temp.isNull("VL_RECU") ? 0:Integer.valueOf(temp.get("VL_RECU").toString()));
            objRsRelat.add(relosCd);
     
        }
	}
	public void relatoriosContestacaoACredito() {
		try {
			String objRs = objRelat.fnRelOSEncerradasCanaisTrans(this.intCanal, "2", this.strDtIni, this.strDtFim, this.pCodArea );
			JSONObject objResTemp = new JSONObject(objRs);
			pcursorObjRs = objResTemp.getJSONArray("PCURSOR");
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		for (int i = 0; i < pcursorObjRs.length(); i++) {
            JSONObject temp = pcursorObjRs.getJSONObject(i);

            RelosEncerradaCanalIBBModel relosCd = new RelosEncerradaCanalIBBModel();
            relosCd.setTipoTabela(10);
            relosCd.setRow(temp.isNull("ROWNUM") ? i+1:Integer.valueOf(temp.get("ROWNUM").toString()));
            relosCd.setOperacoes(temp.isNull("OPERACAO") ? "":temp.get("OPERACAO").toString());
            relosCd.setValorTotal(temp.isNull("VL_TOTAL") ? 0.00:Double.valueOf(temp.get("VL_TOTAL").toString()));
            relosCd.setValorExcluido(temp.isNull("VL_EXCL") ? 0.00:Double.valueOf(temp.get("VL_EXCL").toString()));
            relosCd.setValorRecuperado(temp.isNull("VL_RECU") ? 0:Integer.valueOf(temp.get("VL_RECU").toString()));
            objRsRelat.add(relosCd);
     
        }
	}
	
	
	
	public void postProcessExcel(Object doc) {
		
		try {
			XSSFWorkbook wb = (XSSFWorkbook) doc;
			XSSFSheet sheet = wb.getSheetAt(0);
			sheet.setDisplayGridlines(false);
			
			formatarCabecalho(wb, sheet);
			linhaAtual = 8;
			if(!objRsRelat.isEmpty() && objRsRelat != null) {
				if(possuiTipoDeTabela(1,objRsRelat)) {
					montarPrimeiraTabela(linhaAtual,sheet, wb);
					linhaAtual +=2;
					montarDetalheTabela(linhaAtual,1,sheet, wb);
					linhaAtual +=1;
				}
				if(possuiTipoDeTabela(4,objRsRelat)) {
					montarQuartaTabela(linhaAtual,sheet, wb);
					linhaAtual +=2;
					montarDetalheTabela(linhaAtual,4,sheet, wb);
					linhaAtual +=1;
				}
				if(possuiTipoDeTabela(5,objRsRelat)) {
					montarQuintaTabela(linhaAtual,sheet, wb);
					linhaAtual +=2;
					montarDetalheTabela(linhaAtual,5,sheet, wb);
					linhaAtual +=1;
				}
				if(possuiTipoDeTabela(6,objRsRelat)) {
					montarSextaTabela(linhaAtual,sheet, wb);
					linhaAtual +=2;
					montarDetalheTabela(linhaAtual,6,sheet, wb);
					linhaAtual +=1;
				}
				if(possuiTipoDeTabela(7,objRsRelat)) {
					montarSetimaTabela(linhaAtual,sheet, wb);
					linhaAtual +=2;
					montarDetalheTabela(linhaAtual,7,sheet, wb);
					linhaAtual +=1;
				}
				if(possuiTipoDeTabela(8,objRsRelat)) {
					montarOitavaTabela(linhaAtual,sheet, wb);
					linhaAtual +=2;
					montarDetalheTabela(linhaAtual,8,sheet, wb);
					linhaAtual +=1;
				}
				if(possuiTipoDeTabela(9,objRsRelat)) {
					montarNonaTabela(linhaAtual,sheet, wb);
					linhaAtual +=2;
					montarDetalheTabela(linhaAtual,9,sheet, wb);
					linhaAtual +=1;
				}
				if(possuiTipoDeTabela(10,objRsRelat)) {
					montarDecimaTabela(linhaAtual,sheet, wb);
					linhaAtual +=2;
					montarDetalheTabela(linhaAtual,10,sheet, wb);
					linhaAtual +=1;
				}
				
				
				
				criarRodape(wb, sheet);
				redefinirColunas(wb,13,false);
			
			} else {
				criarRodape(wb, sheet);
				redefinirColunas(wb,13,false);
			}
			
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	public void redefinirColunas(XSSFWorkbook wb,int n, Boolean merged) {
		XSSFSheet sheet = wb.getSheetAt(0);
		for (int i = 0; i <= n; i++) {
			sheet.autoSizeColumn(i,merged);
		}
	
		
	
	}
	
	public Boolean possuiTipoDeTabela(int tipoTabela, List<RelosEncerradaCanalIBBModel> objRs) {
		for(int i = 0 ; i < objRs.size(); i++) {
			if(objRs.get(i).getTipoTabela() == tipoTabela) {
				return true;
			}
		}
		return false;
	}
	
	private void criarRodape(XSSFWorkbook wb, XSSFSheet sheet) {
		Font fontCenter;
		/*Criação, inclusão e formatação dos dados do footer do excel*/
		XSSFRow ultimaLinha = sheet.createRow(sheet.getLastRowNum() + 2);
		
		XSSFCellStyle footerStyle = wb.createCellStyle();
		footerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		footerStyle.setWrapText(true);
		fontCenter = footerStyle.getFont();
		fontCenter.setFontHeight((short) 150);
		fontCenter.setFontName("Arial");
		fontCenter.setColor(IndexedColors.BLACK.getIndex());
		fontCenter.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		footerStyle.setFont(fontCenter);
		
		XSSFCell cell1 = ultimaLinha.createCell(0);
		cell1.setCellValue(now());
		cell1.setCellStyle(footerStyle);
		
		XSSFCell cell2 = ultimaLinha.createCell(1);
		cell2.setCellValue("Superintêndencia de Ocorrências Especiais");
		cell2.setCellStyle(footerStyle);
		
		
		CellRangeAddress region2 = new CellRangeAddress(ultimaLinha.getRowNum(), ultimaLinha.getRowNum(), 1, 12);
		sheet.addMergedRegion(region2);
		if(objRsRelat.isEmpty() || objRsRelat == null){
			sheet.shiftRows(13, 14, -5);
		}
	
	}
	
	private void formatarCabecalho(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyle1 = wb.createCellStyle();
		headerStyle1.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle1.setWrapText(true);
		headerStyle1.setBorderBottom(CellStyle.BORDER_NONE);
		headerStyle1.setBorderLeft(CellStyle.BORDER_NONE);
		headerStyle1.setBorderRight(CellStyle.BORDER_NONE);
		headerStyle1.setBorderTop(CellStyle.BORDER_NONE);
		Font fontCenter = wb.createFont();
		fontCenter.setFontHeightInPoints((short) 14);
		fontCenter.setFontName("Calibri");
		fontCenter.setColor(IndexedColors.BLACK.getIndex());
		fontCenter.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		headerStyle1.setFont(fontCenter);
		
		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setWrapText(true);
		headerStyle.setBorderBottom(CellStyle.BORDER_NONE);
		headerStyle.setBorderLeft(CellStyle.BORDER_NONE);
		headerStyle.setBorderRight(CellStyle.BORDER_NONE);
		headerStyle.setBorderTop(CellStyle.BORDER_NONE);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 14);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(font);
		
		XSSFRow primeiraLinha = sheet.getRow(1);
		if (primeiraLinha != null) {
			XSSFCell primeiraLinhaCell = primeiraLinha.getCell(0);
			primeiraLinhaCell.setCellStyle(headerStyle);
			primeiraLinha.setHeightInPoints((short) 19);
		}
		XSSFRow segundaLinha = sheet.getRow(3);
		if (segundaLinha != null) {
			XSSFCell segundaLinhaCell = segundaLinha.getCell(0);
			segundaLinhaCell.setCellStyle(headerStyle1);
			segundaLinha.setHeightInPoints((short) 19);
		}

	}
	public void montarPrimeiraTabela(int linha,XSSFSheet sheet,XSSFWorkbook wb) {
		List<String>cabecalhos = new ArrayList<String>();
		//"" - Nº OS - Data/Abertura - Data/Evento  - Data/Encerramento - Agencia - C/C - Cliente - PF/PJ - Valor Envolvido
		//Valor Prejuízo - Analista
		String titulo = "Dados Gerais OS";
		cabecalhos.add(" ");
		cabecalhos.add("Nº OS");
		cabecalhos.add("Data/Abertura");
		cabecalhos.add("Data/Evento");
		cabecalhos.add("Data/Encerramento");
		cabecalhos.add("Agencia");
		cabecalhos.add("C/C");
		cabecalhos.add("Cliente");
		cabecalhos.add("PF/PJ");
		cabecalhos.add("Valor Envolvido");
		cabecalhos.add("Valor Prejuízo");
		cabecalhos.add("IP Cliente");
		cabecalhos.add("IP Fraude");
		montarCabecalhoTabela(cabecalhos,linha,sheet,wb,titulo,1);
	}
	
	
	public void montarQuartaTabela(int linha,XSSFSheet sheet,XSSFWorkbook wb) {
		List<String>cabecalhos = new ArrayList<String>();
		String titulo = "Dados Cartão de Segurança";
		cabecalhos.add(" ");
		cabecalhos.add("Nº OS");
		cabecalhos.add("Solicitação");
		cabecalhos.add("Ativação");
		cabecalhos.add("Cancelamento");
		cabecalhos.add("Data Evento");
		cabecalhos.add("Cliente Informou pop up?");
		cabecalhos.add("Ressarcido/Negado");
		montarCabecalhoTabela(cabecalhos,linha,sheet,wb,titulo,4);
	}
	public void montarQuintaTabela(int linha,XSSFSheet sheet,XSSFWorkbook wb) {
		List<String>cabecalhos = new ArrayList<String>();
		String titulo = "Débitos Efetivados";
		cabecalhos.add(" ");
		cabecalhos.add("Nº OS");
		cabecalhos.add("Tipo de Produto");
		cabecalhos.add("Operações");
		cabecalhos.add("Quantidade");
		montarCabecalhoTabela(cabecalhos,linha,sheet,wb,titulo,5);
	}
	public void montarSextaTabela(int linha,XSSFSheet sheet,XSSFWorkbook wb) {
		List<String>cabecalhos = new ArrayList<String>();
		String titulo = "Créditos Efetivados";
		cabecalhos.add(" ");
		cabecalhos.add("Nº OS");
		cabecalhos.add("Tipo de Produto");
		cabecalhos.add("Operações");
		cabecalhos.add("Quantidade");
		montarCabecalhoTabela(cabecalhos,linha,sheet,wb,titulo,6);
	}
	public void montarSetimaTabela(int linha,XSSFSheet sheet,XSSFWorkbook wb) {
		List<String>cabecalhos = new ArrayList<String>();
		String titulo = "Valores a abater do Prejuízo - EXCLUSÕES";
		cabecalhos.add(" ");
		cabecalhos.add("Nº OS");
		cabecalhos.add("Operações");
		cabecalhos.add("Quantidade");
		montarCabecalhoTabela(cabecalhos,linha,sheet,wb,titulo,7);
	
	}
	public void montarOitavaTabela(int linha,XSSFSheet sheet,XSSFWorkbook wb) {
		List<String>cabecalhos = new ArrayList<String>();
		String titulo = "Valores a abater do Prejuízo - RECUPERAÇÕES";
		cabecalhos.add(" ");
		cabecalhos.add("Nº OS");
		cabecalhos.add("Operações");
		cabecalhos.add("Quantidade");
		montarCabecalhoTabela(cabecalhos,linha,sheet,wb,titulo,8);
	
	}
	public void montarNonaTabela(int linha,XSSFSheet sheet,XSSFWorkbook wb) {
		List<String>cabecalhos = new ArrayList<String>();
		String titulo = "Contestação - Á Débito";
		cabecalhos.add("Tipo de Produto");
		cabecalhos.add("");
		cabecalhos.add("Valor Total");
		cabecalhos.add("Percentual");
		montarCabecalhoTabela(cabecalhos,linha,sheet,wb,titulo,9);
	
	}
	public void montarDecimaTabela(int linha,XSSFSheet sheet,XSSFWorkbook wb) {
		List<String>cabecalhos = new ArrayList<String>();
		String titulo = "Contestação - Á Crédito";
		cabecalhos.add("Tipo de Produto");
		cabecalhos.add("");
		cabecalhos.add("Valor Total");
		cabecalhos.add("Percentual");
		montarCabecalhoTabela(cabecalhos,linha,sheet,wb,titulo,10);
	
	}
	
	public void montarCabecalhoTabela(List<String>cabecalhos,int linha,XSSFSheet sheet,XSSFWorkbook wb,String titulo,int tipoTabela) {
		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setWrapText(true);
		headerStyle.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyle.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyle.setBorderRight(CellStyle.BORDER_THIN);
		headerStyle.setBorderTop(CellStyle.BORDER_THIN);
		headerStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font fontCenter = wb.createFont();
		fontCenter.setFontHeightInPoints((short) 8);
		fontCenter.setFontName("Arial");
		fontCenter.setColor(IndexedColors.WHITE.getIndex());
		fontCenter.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(fontCenter);
		
		XSSFCellStyle tituloStyle = wb.createCellStyle();
		tituloStyle.setAlignment(CellStyle.ALIGN_CENTER);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		tituloStyle.setFont(font);
		
		XSSFRow tituloLinha = sheet.getRow(linha);
		if (tituloLinha == null) {
			tituloLinha = sheet.createRow(linha);
		}

		XSSFCell cell1 = tituloLinha.createCell(0);
		cell1.setCellValue(titulo);
		cell1.setCellStyle(tituloStyle);
		
		
		if(tipoTabela == 1) {
			CellRangeAddress region2 = new CellRangeAddress(tituloLinha.getRowNum(), tituloLinha.getRowNum(), 0, cabecalhos.size()-3);
			sheet.addMergedRegion(region2);
			linha++;
			XSSFRow tituloLinhaDois = sheet.getRow(linha);
			if (tituloLinhaDois == null) {
				tituloLinhaDois = sheet.createRow(linha);
			}

			XSSFCell cell2 = tituloLinha.createCell(1);
			cell2.setCellValue("Dados Específicos Canal - IB");
			cell2.setCellStyle(tituloStyle);
			CellRangeAddress region3 = new CellRangeAddress(tituloLinhaDois.getRowNum(), tituloLinhaDois.getRowNum(), cabecalhos.size()-2, cabecalhos.size()+1);
			sheet.addMergedRegion(region3);
			
		} else {
			CellRangeAddress region2 = new CellRangeAddress(tituloLinha.getRowNum(), tituloLinha.getRowNum(), 0, cabecalhos.size()-2);
			sheet.addMergedRegion(region2);
		}
		
		
		
		
		XSSFRow primeiraLinha = sheet.getRow(linha+1);
		if (primeiraLinha == null) {
			primeiraLinha = sheet.createRow(linha+1);
		}
		if(tipoTabela == 9 || tipoTabela == 10) {
			CellRangeAddress region3 = new CellRangeAddress(primeiraLinha.getRowNum(), primeiraLinha.getRowNum(), 0, 1);
			sheet.addMergedRegion(region3);
		}
		primeiraLinha.setHeightInPoints((short) 25);
		for(int i =0; i < cabecalhos.size(); i++) {
			XSSFCell primeiraLinhaCell = primeiraLinha.getCell(i);
			if(primeiraLinhaCell == null) {
				primeiraLinhaCell = primeiraLinha.createCell(i);
			}
			primeiraLinhaCell.setCellStyle(headerStyle);
			primeiraLinhaCell.setCellValue(cabecalhos.get(i));
		}
	}
	public void montarDetalheTabela(int linha,int tipoTabela,XSSFSheet sheet,XSSFWorkbook wb) {
		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyle.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyle.setBorderRight(CellStyle.BORDER_THIN);
		headerStyle.setBorderTop(CellStyle.BORDER_THIN);
		Font fontCenter = wb.createFont();
		fontCenter.setFontHeightInPoints((short) 8);
		fontCenter.setFontName("Arial");
		fontCenter.setColor(IndexedColors.BLACK.getIndex());
		fontCenter.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		headerStyle.setFont(fontCenter);
		
	
		for(int i = 0; i < objRsRelat.size(); i++) {
			if(objRsRelat.get(i).getTipoTabela() == tipoTabela) {
				XSSFRow detalheLinha = sheet.getRow(linhaAtual);
				if (detalheLinha == null) {
					detalheLinha = sheet.createRow(linhaAtual);
				}
				criarColunasTabela(tipoTabela,detalheLinha,objRsRelat.get(i),headerStyle,sheet,wb);
				linhaAtual+=1;
			}
		}
		
	}
	
	public void criarColunasTabela(int tipo,XSSFRow detalheLinha,RelosEncerradaCanalIBBModel obj,XSSFCellStyle headerStyle,XSSFSheet sheet,XSSFWorkbook wb){
		
		switch(tipo) {
		case 1:
			colunasTabelaUm(detalheLinha,obj,headerStyle);
			break;
		case 4:
			colunasTabelaQuatro(detalheLinha,obj,headerStyle);
			break;
		case 5:
			colunasTabelaCinco(detalheLinha,obj,headerStyle);
			break;
		case 6:
			colunasTabelaSeis(detalheLinha,obj,headerStyle);
			break;
		case 7:
			colunasTabelaSete(detalheLinha,obj,headerStyle);
			break;
		case 8:
			colunasTabelaOito(detalheLinha,obj,headerStyle);
			break;
		case 9:
			colunasTabelaNove(detalheLinha,obj,headerStyle,sheet,wb);
			break;	
		case 10:
			colunasTabelaNove(detalheLinha,obj,headerStyle,sheet,wb);
			break;	
		
		}
		
		
		
	}
	
	public void colunasTabelaUm(XSSFRow detalheLinha,RelosEncerradaCanalIBBModel obj,XSSFCellStyle headerStyle) {
		XSSFCell detalheLinhaCell = detalheLinha.getCell(0);
		if(detalheLinhaCell == null) {
			detalheLinhaCell = detalheLinha.createCell(0);
		}
		detalheLinhaCell.setCellStyle(headerStyle);
		detalheLinhaCell.setCellValue(obj.getRow());
		
		XSSFCell detalheLinhaCellDois = detalheLinha.getCell(1);
		if(detalheLinhaCellDois == null) {
			detalheLinhaCellDois = detalheLinha.createCell(1);
		}
		detalheLinhaCellDois.setCellStyle(headerStyle);
		detalheLinhaCellDois.setCellValue(obj.getnOS());
		
		XSSFCell detalheLinhaCellTres = detalheLinha.getCell(2);
		if(detalheLinhaCellTres == null) {
			detalheLinhaCellTres = detalheLinha.createCell(2);
		}
		detalheLinhaCellTres.setCellStyle(headerStyle);
		detalheLinhaCellTres.setCellValue(obj.getDataAbertura());
		
		XSSFCell detalheLinhaCellQuatro = detalheLinha.getCell(3);
		if(detalheLinhaCellQuatro == null) {
			detalheLinhaCellQuatro = detalheLinha.createCell(3);
		}
		detalheLinhaCellQuatro.setCellStyle(headerStyle);
		detalheLinhaCellQuatro.setCellValue(obj.getDataEvento());
		
		XSSFCell detalheLinhaCellCinco = detalheLinha.getCell(4);
		if(detalheLinhaCellCinco == null) {
			detalheLinhaCellCinco = detalheLinha.createCell(4);
		}
		detalheLinhaCellCinco.setCellStyle(headerStyle);
		detalheLinhaCellCinco.setCellValue(obj.getDataEncerramento());
		
		XSSFCell detalheLinhaCellSeis = detalheLinha.getCell(5);
		if(detalheLinhaCellSeis == null) {
			detalheLinhaCellSeis = detalheLinha.createCell(5);
		}
		detalheLinhaCellSeis.setCellStyle(headerStyle);
		detalheLinhaCellSeis.setCellValue(obj.getAgencia());
		
		XSSFCell detalheLinhaCellSete = detalheLinha.getCell(6);
		if(detalheLinhaCellSete == null) {
			detalheLinhaCellSete = detalheLinha.createCell(6);
		}
		detalheLinhaCellSete.setCellStyle(headerStyle);
		detalheLinhaCellSete.setCellValue(obj.getCc());
		
		XSSFCell detalheLinhaCellOito = detalheLinha.getCell(7);
		if(detalheLinhaCellOito == null) {
			detalheLinhaCellOito = detalheLinha.createCell(7);
		}
		detalheLinhaCellOito.setCellStyle(headerStyle);
		detalheLinhaCellOito.setCellValue(obj.getCliente());
		
		XSSFCell detalheLinhaCellNove = detalheLinha.getCell(8);
		if(detalheLinhaCellNove == null) {
			detalheLinhaCellNove = detalheLinha.createCell(8);
		}
		detalheLinhaCellNove.setCellStyle(headerStyle);
		detalheLinhaCellNove.setCellValue(obj.getPfPj());
		
		XSSFCell detalheLinhaCellDez = detalheLinha.getCell(9);
		if(detalheLinhaCellDez == null) {
			detalheLinhaCellDez = detalheLinha.createCell(9);
		}
		detalheLinhaCellDez.setCellStyle(headerStyle);
		if(obj.getValorEnvolvido() == 0) {
			detalheLinhaCellDez.setCellValue(" ");
		} else { 
			detalheLinhaCellDez.setCellValue(obj.getValorEnvolvido());
		}
		
		XSSFCell detalheLinhaCellOnze = detalheLinha.getCell(10);
		if(detalheLinhaCellOnze == null) {
			detalheLinhaCellOnze = detalheLinha.createCell(10);
		}
		detalheLinhaCellOnze.setCellStyle(headerStyle);
		if(obj.getValorPrejuizo() == 0) {
			detalheLinhaCellOnze.setCellValue(" ");
		} else { 
			detalheLinhaCellOnze.setCellValue(obj.getValorPrejuizo());
		}
		
		XSSFCell detalheLinhaCellDoze = detalheLinha.getCell(11);
		if(detalheLinhaCellDoze == null) {
			detalheLinhaCellDoze = detalheLinha.createCell(11);
		}
		detalheLinhaCellDoze.setCellStyle(headerStyle);
		detalheLinhaCellDoze.setCellValue(obj.getIpCliente());
		
		XSSFCell detalheLinhaCellTreze = detalheLinha.getCell(12);
		if(detalheLinhaCellTreze == null) {
			detalheLinhaCellTreze = detalheLinha.createCell(12);
		}
		detalheLinhaCellTreze.setCellStyle(headerStyle);
		detalheLinhaCellTreze.setCellValue(obj.getIpFraude());
	}	
	public void colunasTabelaQuatro(XSSFRow detalheLinha,RelosEncerradaCanalIBBModel obj,XSSFCellStyle headerStyle) {
		XSSFCell detalheLinhaCell = detalheLinha.getCell(0);
		if(detalheLinhaCell == null) {
			detalheLinhaCell = detalheLinha.createCell(0);
		}
		detalheLinhaCell.setCellStyle(headerStyle);
		detalheLinhaCell.setCellValue(obj.getRow());
		
		XSSFCell detalheLinhaCellDois = detalheLinha.getCell(1);
		if(detalheLinhaCellDois == null) {
			detalheLinhaCellDois = detalheLinha.createCell(1);
		}
		detalheLinhaCellDois.setCellStyle(headerStyle);
		detalheLinhaCellDois.setCellValue(obj.getnOS());
		
		XSSFCell detalheLinhaCellTres = detalheLinha.getCell(2);
		if(detalheLinhaCellTres == null) {
			detalheLinhaCellTres = detalheLinha.createCell(2);
		}
		detalheLinhaCellTres.setCellStyle(headerStyle);
		detalheLinhaCellTres.setCellValue(obj.getSolicitacao());
		
		XSSFCell detalheLinhaCellQuatro = detalheLinha.getCell(3);
		if(detalheLinhaCellQuatro == null) {
			detalheLinhaCellQuatro = detalheLinha.createCell(3);
		}
		detalheLinhaCellQuatro.setCellStyle(headerStyle);
		detalheLinhaCellQuatro.setCellValue(obj.getAtivacao());
		
		XSSFCell detalheLinhaCellCinco = detalheLinha.getCell(4);
		if(detalheLinhaCellCinco == null) {
			detalheLinhaCellCinco = detalheLinha.createCell(4);
		}
		detalheLinhaCellCinco.setCellStyle(headerStyle);
		detalheLinhaCellCinco.setCellValue(obj.getCancelamento());
		
		XSSFCell detalheLinhaCellSeis = detalheLinha.getCell(5);
		if(detalheLinhaCellSeis == null) {
			detalheLinhaCellSeis = detalheLinha.createCell(5);
		}
		detalheLinhaCellSeis.setCellStyle(headerStyle);
		detalheLinhaCellSeis.setCellValue(obj.getIndeferidas());
		
		XSSFCell detalheLinhaCellSete = detalheLinha.getCell(6);
		if(detalheLinhaCellSete == null) {
			detalheLinhaCellSete = detalheLinha.createCell(6);
		}
		detalheLinhaCellSete.setCellStyle(headerStyle);
		detalheLinhaCellSete.setCellValue(obj.getClienteInformou());
		
		XSSFCell detalheLinhaCellOito = detalheLinha.getCell(7);
		if(detalheLinhaCellOito == null) {
			detalheLinhaCellOito = detalheLinha.createCell(7);
		}
		detalheLinhaCellOito.setCellStyle(headerStyle);
		detalheLinhaCellOito.setCellValue(obj.getRessarcido());
		
		}
	
	public void colunasTabelaCinco(XSSFRow detalheLinha, RelosEncerradaCanalIBBModel obj, XSSFCellStyle headerStyle) {
		XSSFCell detalheLinhaCell = detalheLinha.getCell(0);
		if (detalheLinhaCell == null) {
			detalheLinhaCell = detalheLinha.createCell(0);
		}
		detalheLinhaCell.setCellStyle(headerStyle);
		detalheLinhaCell.setCellValue(obj.getRow());

		XSSFCell detalheLinhaCellDois = detalheLinha.getCell(1);
		if (detalheLinhaCellDois == null) {
			detalheLinhaCellDois = detalheLinha.createCell(1);
		}
		detalheLinhaCellDois.setCellStyle(headerStyle);
		detalheLinhaCellDois.setCellValue(obj.getnOS());

		XSSFCell detalheLinhaCellTres = detalheLinha.getCell(2);
		if (detalheLinhaCellTres == null) {
			detalheLinhaCellTres = detalheLinha.createCell(2);
		}
		detalheLinhaCellTres.setCellStyle(headerStyle);
		detalheLinhaCellTres.setCellValue(obj.getTipoProduto());

		XSSFCell detalheLinhaCellQuatro = detalheLinha.getCell(3);
		if (detalheLinhaCellQuatro == null) {
			detalheLinhaCellQuatro = detalheLinha.createCell(3);
		}
		detalheLinhaCellQuatro.setCellStyle(headerStyle);
		detalheLinhaCellQuatro.setCellValue(obj.getOperacoes());

		XSSFCell detalheLinhaCellCinco = detalheLinha.getCell(4);
		if (detalheLinhaCellCinco == null) {
			detalheLinhaCellCinco = detalheLinha.createCell(4);
		}
		detalheLinhaCellCinco.setCellStyle(headerStyle);
		detalheLinhaCellCinco.setCellValue(obj.getQuantidade());

	}

	public void colunasTabelaSeis(XSSFRow detalheLinha, RelosEncerradaCanalIBBModel obj, XSSFCellStyle headerStyle) {
		XSSFCell detalheLinhaCell = detalheLinha.getCell(0);
		if (detalheLinhaCell == null) {
			detalheLinhaCell = detalheLinha.createCell(0);
		}
		detalheLinhaCell.setCellStyle(headerStyle);
		detalheLinhaCell.setCellValue(obj.getRow());

		XSSFCell detalheLinhaCellDois = detalheLinha.getCell(1);
		if (detalheLinhaCellDois == null) {
			detalheLinhaCellDois = detalheLinha.createCell(1);
		}
		detalheLinhaCellDois.setCellStyle(headerStyle);
		detalheLinhaCellDois.setCellValue(obj.getnOS());

		XSSFCell detalheLinhaCellTres = detalheLinha.getCell(2);
		if (detalheLinhaCellTres == null) {
			detalheLinhaCellTres = detalheLinha.createCell(2);
		}
		detalheLinhaCellTres.setCellStyle(headerStyle);
		detalheLinhaCellTres.setCellValue(obj.getTipoProduto());

		XSSFCell detalheLinhaCellQuatro = detalheLinha.getCell(3);
		if (detalheLinhaCellQuatro == null) {
			detalheLinhaCellQuatro = detalheLinha.createCell(3);
		}
		detalheLinhaCellQuatro.setCellStyle(headerStyle);
		detalheLinhaCellQuatro.setCellValue(obj.getOperacoes());

		XSSFCell detalheLinhaCellCinco = detalheLinha.getCell(4);
		if (detalheLinhaCellCinco == null) {
			detalheLinhaCellCinco = detalheLinha.createCell(4);
		}
		detalheLinhaCellCinco.setCellStyle(headerStyle);
		detalheLinhaCellCinco.setCellValue(obj.getQuantidade());

	}	
	public void colunasTabelaSete(XSSFRow detalheLinha, RelosEncerradaCanalIBBModel obj, XSSFCellStyle headerStyle) {
		XSSFCell detalheLinhaCell = detalheLinha.getCell(0);
		if (detalheLinhaCell == null) {
			detalheLinhaCell = detalheLinha.createCell(0);
		}
		detalheLinhaCell.setCellStyle(headerStyle);
		detalheLinhaCell.setCellValue(obj.getRow());

		XSSFCell detalheLinhaCellDois = detalheLinha.getCell(1);
		if (detalheLinhaCellDois == null) {
			detalheLinhaCellDois = detalheLinha.createCell(1);
		}
		detalheLinhaCellDois.setCellStyle(headerStyle);
		detalheLinhaCellDois.setCellValue(obj.getnOS());

		XSSFCell detalheLinhaCellTres = detalheLinha.getCell(2);
		if (detalheLinhaCellTres == null) {
			detalheLinhaCellTres = detalheLinha.createCell(2);
		}
		detalheLinhaCellTres.setCellStyle(headerStyle);
		detalheLinhaCellTres.setCellValue(obj.getOperacoes());

		XSSFCell detalheLinhaCellQuatro = detalheLinha.getCell(3);
		if (detalheLinhaCellQuatro == null) {
			detalheLinhaCellQuatro = detalheLinha.createCell(3);
		}
		detalheLinhaCellQuatro.setCellStyle(headerStyle);
		detalheLinhaCellQuatro.setCellValue(obj.getQuantidade());


	}
	public void colunasTabelaOito(XSSFRow detalheLinha, RelosEncerradaCanalIBBModel obj, XSSFCellStyle headerStyle) {
		XSSFCell detalheLinhaCell = detalheLinha.getCell(0);
		if (detalheLinhaCell == null) {
			detalheLinhaCell = detalheLinha.createCell(0);
		}
		detalheLinhaCell.setCellStyle(headerStyle);
		detalheLinhaCell.setCellValue(obj.getRow());

		XSSFCell detalheLinhaCellDois = detalheLinha.getCell(1);
		if (detalheLinhaCellDois == null) {
			detalheLinhaCellDois = detalheLinha.createCell(1);
		}
		detalheLinhaCellDois.setCellStyle(headerStyle);
		detalheLinhaCellDois.setCellValue(obj.getnOS());

		XSSFCell detalheLinhaCellTres = detalheLinha.getCell(2);
		if (detalheLinhaCellTres == null) {
			detalheLinhaCellTres = detalheLinha.createCell(2);
		}
		detalheLinhaCellTres.setCellStyle(headerStyle);
		detalheLinhaCellTres.setCellValue(obj.getOperacoes());

		XSSFCell detalheLinhaCellQuatro = detalheLinha.getCell(3);
		if (detalheLinhaCellQuatro == null) {
			detalheLinhaCellQuatro = detalheLinha.createCell(3);
		}
		detalheLinhaCellQuatro.setCellStyle(headerStyle);
		detalheLinhaCellQuatro.setCellValue(obj.getQuantidade());
	}
	public void colunasTabelaNove(XSSFRow detalheLinha, RelosEncerradaCanalIBBModel obj, XSSFCellStyle headerStyle,XSSFSheet sheet,XSSFWorkbook wb) {
		
		XSSFCellStyle totalStyle = wb.createCellStyle();
		totalStyle.setAlignment(CellStyle.ALIGN_CENTER);
		totalStyle.setBorderBottom(CellStyle.BORDER_THIN);
		totalStyle.setBorderLeft(CellStyle.BORDER_THIN);
		totalStyle.setBorderRight(CellStyle.BORDER_THIN);
		totalStyle.setBorderTop(CellStyle.BORDER_THIN);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		totalStyle.setFont(font);
		
		
		NumberFormat formatacao = NumberFormat.getCurrencyInstance(Locale.getDefault());
		
		CellRangeAddress region2 = new CellRangeAddress(detalheLinha.getRowNum(), detalheLinha.getRowNum(), 0, 1);
		sheet.addMergedRegion(region2);
		
		XSSFCell detalheLinhaCell = detalheLinha.getCell(0);
		if (detalheLinhaCell == null) {
			detalheLinhaCell = detalheLinha.createCell(0);
		}
		detalheLinhaCell.setCellStyle(headerStyle);
		detalheLinhaCell.setCellValue(obj.getOperacoes());

		XSSFCell detalheLinhaCellDois = detalheLinha.getCell(2);
		if (detalheLinhaCellDois == null) {
			detalheLinhaCellDois = detalheLinha.createCell(2);
		}
		detalheLinhaCellDois.setCellStyle(headerStyle);
		if(obj.getValorTotal() == 0) {
			detalheLinhaCellDois.setCellValue(0);
		} else {
			detalheLinhaCellDois.setCellValue(NumberFormat.getCurrencyInstance().format(obj.getValorTotal()).replace(formatacao.getCurrency().getSymbol(), ""));
		}

		XSSFCell detalheLinhaCellTres = detalheLinha.getCell(3);
		if (detalheLinhaCellTres == null) {
			detalheLinhaCellTres = detalheLinha.createCell(3);
		}
		detalheLinhaCellTres.setCellStyle(headerStyle);
		detalheLinhaCellTres.setCellValue("");
		
		XSSFRow subDetalheLinha = sheet.getRow(detalheLinha.getRowNum()+1);
		if (subDetalheLinha == null) {
			subDetalheLinha = sheet.createRow(detalheLinha.getRowNum()+1);
		}
		
		XSSFCell subDetalheLinhaCell = subDetalheLinha.getCell(0);
		if (subDetalheLinhaCell == null) {
			subDetalheLinhaCell = subDetalheLinha.createCell(0);
		}
		subDetalheLinhaCell.setCellStyle(totalStyle);
		subDetalheLinhaCell.setCellValue("Total");

		XSSFCell subDetalheLinhaCellDois = subDetalheLinha.getCell(1);
		if (subDetalheLinhaCellDois == null) {
			subDetalheLinhaCellDois = subDetalheLinha.createCell(1);
		}
		subDetalheLinhaCellDois.setCellStyle(headerStyle);
		subDetalheLinhaCellDois.setCellValue(NumberFormat.getCurrencyInstance().format(obj.getValorTotal()).replace(formatacao.getCurrency().getSymbol(), ""));

		XSSFCell subDetalheLinhaCellTres = subDetalheLinha.getCell(2);
		if (subDetalheLinhaCellTres == null) {
			subDetalheLinhaCellTres = subDetalheLinha.createCell(2);
		}
		subDetalheLinhaCellTres.setCellStyle(headerStyle);
		subDetalheLinhaCellTres.setCellValue("");
		
		XSSFCell subDetalheLinhaCellQuatro = subDetalheLinha.getCell(3);
		if (subDetalheLinhaCellQuatro == null) {
			subDetalheLinhaCellQuatro = subDetalheLinha.createCell(3);
		}
		subDetalheLinhaCellQuatro.setCellValue("");
		
		XSSFRow subDetalheLinhaDois = sheet.getRow(detalheLinha.getRowNum()+2);
		if (subDetalheLinhaDois == null) {
			subDetalheLinhaDois = sheet.createRow(detalheLinha.getRowNum()+2);
		}
		
		XSSFCell subDetalheLinhaDoisCell = subDetalheLinhaDois.getCell(0);
		if (subDetalheLinhaDoisCell == null) {
			subDetalheLinhaDoisCell = subDetalheLinhaDois.createCell(0);
		}
		subDetalheLinhaDoisCell.setCellStyle(headerStyle);
		subDetalheLinhaDoisCell.setCellValue("");

		XSSFCell subDetalheLinhaDoisCellDois = subDetalheLinhaDois.getCell(1);
		if (subDetalheLinhaDoisCellDois == null) {
			subDetalheLinhaDoisCellDois = subDetalheLinhaDois.createCell(1);
		}
		subDetalheLinhaDoisCellDois.setCellStyle(totalStyle);
		subDetalheLinhaDoisCellDois.setCellValue("Total Recuperado");

		XSSFCell subDetalheLinhaDoisCellTres = subDetalheLinhaDois.getCell(2);
		if (subDetalheLinhaDoisCellTres == null) {
			subDetalheLinhaDoisCellTres = subDetalheLinhaDois.createCell(2);
		}
		subDetalheLinhaDoisCellTres.setCellStyle(headerStyle);
		if(obj.getValorRecuperado() == 0) {
			subDetalheLinhaDoisCellTres.setCellValue(0);
		} else {
			subDetalheLinhaDoisCellTres.setCellValue(NumberFormat.getCurrencyInstance().format(obj.getValorRecuperado()).replace(formatacao.getCurrency().getSymbol(), ""));
		}
		
		XSSFCell subDetalheLinhaDoisCellQuatro = subDetalheLinhaDois.getCell(3);
		if (subDetalheLinhaDoisCellQuatro == null) {
			subDetalheLinhaDoisCellQuatro = subDetalheLinhaDois.createCell(3);
		}
		subDetalheLinhaDoisCellQuatro.setCellValue("");
		
		XSSFRow subDetalheLinhaTres = sheet.getRow(detalheLinha.getRowNum()+3);
		if (subDetalheLinhaTres == null) {
			subDetalheLinhaTres = sheet.createRow(detalheLinha.getRowNum()+3);
		}
		
		XSSFCell subDetalheLinhaTresCell = subDetalheLinhaTres.getCell(0);
		if (subDetalheLinhaTresCell == null) {
			subDetalheLinhaTresCell = subDetalheLinhaTres.createCell(0);
		}
		subDetalheLinhaTresCell.setCellStyle(headerStyle);
		subDetalheLinhaTresCell.setCellValue("");

		XSSFCell subDetalheLinhaTresCellDois = subDetalheLinhaTres.getCell(1);
		if (subDetalheLinhaTresCellDois == null) {
			subDetalheLinhaTresCellDois = subDetalheLinhaTres.createCell(1);
		}
		subDetalheLinhaTresCellDois.setCellStyle(totalStyle);
		subDetalheLinhaTresCellDois.setCellValue("Total Excluído");

		XSSFCell subDetalheLinhaTresCellTres = subDetalheLinhaTres.getCell(2);
		if (subDetalheLinhaTresCellTres == null) {
			subDetalheLinhaTresCellTres = subDetalheLinhaTres.createCell(2);
		}
		subDetalheLinhaTresCellTres.setCellStyle(headerStyle);
		if(obj.getValorExcluido() == 0) {
			subDetalheLinhaTresCellTres.setCellValue(0);
		} else {
			subDetalheLinhaTresCellTres.setCellValue(NumberFormat.getCurrencyInstance().format(obj.getValorExcluido()).replace(formatacao.getCurrency().getSymbol(), ""));
		}
		
		XSSFCell subDetalheLinhaTresCellQuatro = subDetalheLinhaTres.getCell(3);
		if (subDetalheLinhaTresCellQuatro == null) {
			subDetalheLinhaTresCellQuatro = subDetalheLinhaTres.createCell(3);
		}
		subDetalheLinhaTresCellQuatro.setCellValue("");
		
		XSSFRow subDetalheLinhaQuatro = sheet.getRow(detalheLinha.getRowNum()+4);
		if (subDetalheLinhaQuatro == null) {
			subDetalheLinhaQuatro = sheet.createRow(detalheLinha.getRowNum()+4);
		}
		
		XSSFCell subDetalheLinhaQuatroCell = subDetalheLinhaQuatro.getCell(0);
		if (subDetalheLinhaQuatroCell == null) {
			subDetalheLinhaQuatroCell = subDetalheLinhaQuatro.createCell(0);
		}
		subDetalheLinhaQuatroCell.setCellStyle(headerStyle);
		subDetalheLinhaQuatroCell.setCellValue("");

		XSSFCell subDetalheLinhaQuatroCellDois = subDetalheLinhaQuatro.getCell(1);
		if (subDetalheLinhaQuatroCellDois == null) {
			subDetalheLinhaQuatroCellDois = subDetalheLinhaQuatro.createCell(1);
		}
		subDetalheLinhaQuatroCellDois.setCellStyle(totalStyle);
		subDetalheLinhaQuatroCellDois.setCellValue("Total Prejuízo");

		XSSFCell subDetalheLinhaQuatroCellTres = subDetalheLinhaQuatro.getCell(2);
		if (subDetalheLinhaQuatroCellTres == null) {
			subDetalheLinhaQuatroCellTres = subDetalheLinhaQuatro.createCell(2);
		}
		subDetalheLinhaQuatroCellTres.setCellStyle(headerStyle);
		if(obj.getValorTotal() == 0) {
			subDetalheLinhaQuatroCellTres.setCellValue(0);
		} else {
			subDetalheLinhaQuatroCellTres.setCellValue(NumberFormat.getCurrencyInstance().format(obj.getValorTotal()).replace(formatacao.getCurrency().getSymbol(), ""));
		}
		
		XSSFCell subDetalheLinhaQuatroCellQuatro = subDetalheLinhaQuatro.getCell(3);
		if (subDetalheLinhaQuatroCellQuatro == null) {
			subDetalheLinhaQuatroCellQuatro = subDetalheLinhaQuatro.createCell(3);
		}
		subDetalheLinhaQuatroCellQuatro.setCellValue("");
		
		
		
	}
	public void colunasTabelaDez(XSSFRow detalheLinha, RelosEncerradaCanalIBBModel obj, XSSFCellStyle headerStyle,XSSFSheet sheet,XSSFWorkbook wb) {
		XSSFCellStyle totalStyle = wb.createCellStyle();
		totalStyle.setAlignment(CellStyle.ALIGN_CENTER);
		totalStyle.setBorderBottom(CellStyle.BORDER_THIN);
		totalStyle.setBorderLeft(CellStyle.BORDER_THIN);
		totalStyle.setBorderRight(CellStyle.BORDER_THIN);
		totalStyle.setBorderTop(CellStyle.BORDER_THIN);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		totalStyle.setFont(font);
		
		
		NumberFormat formatacao = NumberFormat.getCurrencyInstance(Locale.getDefault());
		
		CellRangeAddress region2 = new CellRangeAddress(detalheLinha.getRowNum(), detalheLinha.getRowNum(), 0, 1);
		sheet.addMergedRegion(region2);
		
		XSSFCell detalheLinhaCell = detalheLinha.getCell(0);
		if (detalheLinhaCell == null) {
			detalheLinhaCell = detalheLinha.createCell(0);
		}
		detalheLinhaCell.setCellStyle(headerStyle);
		detalheLinhaCell.setCellValue(obj.getOperacoes());

		XSSFCell detalheLinhaCellDois = detalheLinha.getCell(2);
		if (detalheLinhaCellDois == null) {
			detalheLinhaCellDois = detalheLinha.createCell(2);
		}
		detalheLinhaCellDois.setCellStyle(headerStyle);
		if(obj.getValorTotal() == 0) {
			detalheLinhaCellDois.setCellValue(0);
		} else {
			detalheLinhaCellDois.setCellValue(NumberFormat.getCurrencyInstance().format(obj.getValorTotal()).replace(formatacao.getCurrency().getSymbol(), ""));
		}

		XSSFCell detalheLinhaCellTres = detalheLinha.getCell(3);
		if (detalheLinhaCellTres == null) {
			detalheLinhaCellTres = detalheLinha.createCell(3);
		}
		detalheLinhaCellTres.setCellStyle(headerStyle);
		detalheLinhaCellTres.setCellValue("");
		
		XSSFRow subDetalheLinha = sheet.getRow(detalheLinha.getRowNum()+1);
		if (subDetalheLinha == null) {
			subDetalheLinha = sheet.createRow(detalheLinha.getRowNum()+1);
		}
		
		XSSFCell subDetalheLinhaCell = subDetalheLinha.getCell(0);
		if (subDetalheLinhaCell == null) {
			subDetalheLinhaCell = subDetalheLinha.createCell(0);
		}
		subDetalheLinhaCell.setCellStyle(totalStyle);
		subDetalheLinhaCell.setCellValue("Total");

		XSSFCell subDetalheLinhaCellDois = subDetalheLinha.getCell(1);
		if (subDetalheLinhaCellDois == null) {
			subDetalheLinhaCellDois = subDetalheLinha.createCell(1);
		}
		subDetalheLinhaCellDois.setCellStyle(headerStyle);
		subDetalheLinhaCellDois.setCellValue(NumberFormat.getCurrencyInstance().format(obj.getValorTotal()).replace(formatacao.getCurrency().getSymbol(), ""));

		XSSFCell subDetalheLinhaCellTres = subDetalheLinha.getCell(2);
		if (subDetalheLinhaCellTres == null) {
			subDetalheLinhaCellTres = subDetalheLinha.createCell(2);
		}
		subDetalheLinhaCellTres.setCellStyle(headerStyle);
		subDetalheLinhaCellTres.setCellValue("");
		
		XSSFCell subDetalheLinhaCellQuatro = subDetalheLinha.getCell(3);
		if (subDetalheLinhaCellQuatro == null) {
			subDetalheLinhaCellQuatro = subDetalheLinha.createCell(3);
		}
		subDetalheLinhaCellQuatro.setCellValue("");
		
		XSSFRow subDetalheLinhaDois = sheet.getRow(detalheLinha.getRowNum()+2);
		if (subDetalheLinhaDois == null) {
			subDetalheLinhaDois = sheet.createRow(detalheLinha.getRowNum()+2);
		}
		
		XSSFCell subDetalheLinhaDoisCell = subDetalheLinhaDois.getCell(0);
		if (subDetalheLinhaDoisCell == null) {
			subDetalheLinhaDoisCell = subDetalheLinhaDois.createCell(0);
		}
		subDetalheLinhaDoisCell.setCellStyle(headerStyle);
		subDetalheLinhaDoisCell.setCellValue("");

		XSSFCell subDetalheLinhaDoisCellDois = subDetalheLinhaDois.getCell(1);
		if (subDetalheLinhaDoisCellDois == null) {
			subDetalheLinhaDoisCellDois = subDetalheLinhaDois.createCell(1);
		}
		subDetalheLinhaDoisCellDois.setCellStyle(totalStyle);
		subDetalheLinhaDoisCellDois.setCellValue("Total Recuperado");

		XSSFCell subDetalheLinhaDoisCellTres = subDetalheLinhaDois.getCell(2);
		if (subDetalheLinhaDoisCellTres == null) {
			subDetalheLinhaDoisCellTres = subDetalheLinhaDois.createCell(2);
		}
		subDetalheLinhaDoisCellTres.setCellStyle(headerStyle);
		if(obj.getValorRecuperado() == 0) {
			subDetalheLinhaDoisCellTres.setCellValue(0);
		} else {
			subDetalheLinhaDoisCellTres.setCellValue(NumberFormat.getCurrencyInstance().format(obj.getValorRecuperado()).replace(formatacao.getCurrency().getSymbol(), ""));
		}
		
		XSSFCell subDetalheLinhaDoisCellQuatro = subDetalheLinhaDois.getCell(3);
		if (subDetalheLinhaDoisCellQuatro == null) {
			subDetalheLinhaDoisCellQuatro = subDetalheLinhaDois.createCell(3);
		}
		subDetalheLinhaDoisCellQuatro.setCellValue("");
		
		XSSFRow subDetalheLinhaTres = sheet.getRow(detalheLinha.getRowNum()+3);
		if (subDetalheLinhaTres == null) {
			subDetalheLinhaTres = sheet.createRow(detalheLinha.getRowNum()+3);
		}
		
		XSSFCell subDetalheLinhaTresCell = subDetalheLinhaTres.getCell(0);
		if (subDetalheLinhaTresCell == null) {
			subDetalheLinhaTresCell = subDetalheLinhaTres.createCell(0);
		}
		subDetalheLinhaTresCell.setCellStyle(headerStyle);
		subDetalheLinhaTresCell.setCellValue("");

		XSSFCell subDetalheLinhaTresCellDois = subDetalheLinhaTres.getCell(1);
		if (subDetalheLinhaTresCellDois == null) {
			subDetalheLinhaTresCellDois = subDetalheLinhaTres.createCell(1);
		}
		subDetalheLinhaTresCellDois.setCellStyle(totalStyle);
		subDetalheLinhaTresCellDois.setCellValue("Total Excluído");

		XSSFCell subDetalheLinhaTresCellTres = subDetalheLinhaTres.getCell(2);
		if (subDetalheLinhaTresCellTres == null) {
			subDetalheLinhaTresCellTres = subDetalheLinhaTres.createCell(2);
		}
		subDetalheLinhaTresCellTres.setCellStyle(headerStyle);
		if(obj.getValorExcluido() == 0) {
			subDetalheLinhaTresCellTres.setCellValue(0);
		} else {
			subDetalheLinhaTresCellTres.setCellValue(NumberFormat.getCurrencyInstance().format(obj.getValorExcluido()).replace(formatacao.getCurrency().getSymbol(), ""));
		}
		
		XSSFCell subDetalheLinhaTresCellQuatro = subDetalheLinhaTres.getCell(3);
		if (subDetalheLinhaTresCellQuatro == null) {
			subDetalheLinhaTresCellQuatro = subDetalheLinhaTres.createCell(3);
		}
		subDetalheLinhaTresCellQuatro.setCellValue("");
		
		XSSFRow subDetalheLinhaQuatro = sheet.getRow(detalheLinha.getRowNum()+4);
		if (subDetalheLinhaQuatro == null) {
			subDetalheLinhaQuatro = sheet.createRow(detalheLinha.getRowNum()+4);
		}
		
		XSSFCell subDetalheLinhaQuatroCell = subDetalheLinhaQuatro.getCell(0);
		if (subDetalheLinhaQuatroCell == null) {
			subDetalheLinhaQuatroCell = subDetalheLinhaQuatro.createCell(0);
		}
		subDetalheLinhaQuatroCell.setCellStyle(headerStyle);
		subDetalheLinhaQuatroCell.setCellValue("");

		XSSFCell subDetalheLinhaQuatroCellDois = subDetalheLinhaQuatro.getCell(1);
		if (subDetalheLinhaQuatroCellDois == null) {
			subDetalheLinhaQuatroCellDois = subDetalheLinhaQuatro.createCell(1);
		}
		subDetalheLinhaQuatroCellDois.setCellStyle(totalStyle);
		subDetalheLinhaQuatroCellDois.setCellValue("Total Prejuízo");

		XSSFCell subDetalheLinhaQuatroCellTres = subDetalheLinhaQuatro.getCell(2);
		if (subDetalheLinhaQuatroCellTres == null) {
			subDetalheLinhaQuatroCellTres = subDetalheLinhaQuatro.createCell(2);
		}
		subDetalheLinhaQuatroCellTres.setCellStyle(headerStyle);
		if(obj.getValorTotal() == 0) {
			subDetalheLinhaQuatroCellTres.setCellValue(0);
		} else {
			subDetalheLinhaQuatroCellTres.setCellValue(NumberFormat.getCurrencyInstance().format(obj.getValorTotal()).replace(formatacao.getCurrency().getSymbol(), ""));
		}
		
		XSSFCell subDetalheLinhaQuatroCellQuatro = subDetalheLinhaQuatro.getCell(3);
		if (subDetalheLinhaQuatroCellQuatro == null) {
			subDetalheLinhaQuatroCellQuatro = subDetalheLinhaQuatro.createCell(3);
		}
		subDetalheLinhaQuatroCellQuatro.setCellValue("");
	}

public String now() {
	DateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	Date date = new Date();
	return dateformat.format(date);
}

	public String getStrDtIni() {
		return strDtIni;
	}

	public void setStrDtIni(String strDtIni) {
		this.strDtIni = strDtIni;
	}

	public String getStrDtFim() {
		return strDtFim;
	}

	public void setStrDtFim(String strDtFim) {
		this.strDtFim = strDtFim;
	}



	public String getStrNmRelat() {
		return strNmRelat;
	}

	public void setStrNmRelat(String strNmRelat) {
		this.strNmRelat = strNmRelat;
	}

	public List<RelosEncerradaCanalIBBModel> getObjRsRelat() {
		return objRsRelat;
	}

	public void setObjRsRelat(List<RelosEncerradaCanalIBBModel> objRsRelat) {
		this.objRsRelat = objRsRelat;
	}

	public String getStrArea() {
		return strArea;
	}

	public void setStrArea(String strArea) {
		this.strArea = strArea;
	}

	public String getStrTpRel() {
		return strTpRel;
	}

	public void setStrTpRel(String strTpRel) {
		this.strTpRel = strTpRel;
	}

	public String getIntCanal() {
		return intCanal;
	}

	public void setIntCanal(String intCanal) {
		this.intCanal = intCanal;
	}
	
	
}
