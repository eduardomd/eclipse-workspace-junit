package com.altec.bsbr.app.hyb.dto;

public class TreinamentoIframeModel{ 
 
private String txtHdAction;
private String txthdTreCaminho;
private String txthdNmNome;
private String txthdSeqTrei;
private String txthdOrigem;
private String txtHdInfArq;
public TreinamentoIframeModel(){ 
 } 
 
public String getTxTHdAcTion() { 
   return txtHdAction; 
} 
 
public void setTxTHdAcTion(String txtHdAction) { 
   this.txtHdAction = txtHdAction; 
}
 
public String getTxThdTreCaminho() { 
   return txthdTreCaminho; 
} 
 
public void setTxThdTreCaminho(String txthdTreCaminho) { 
   this.txthdTreCaminho = txthdTreCaminho; 
}
 
public String getTxThdNmNome() { 
   return txthdNmNome; 
} 
 
public void setTxThdNmNome(String txthdNmNome) { 
   this.txthdNmNome = txthdNmNome; 
}
 
public String getTxThdSeqTrei() { 
   return txthdSeqTrei; 
} 
 
public void setTxThdSeqTrei(String txthdSeqTrei) { 
   this.txthdSeqTrei = txthdSeqTrei; 
}
 
public String getTxThdOrigem() { 
   return txthdOrigem; 
} 
 
public void setTxThdOrigem(String txthdOrigem) { 
   this.txthdOrigem = txthdOrigem; 
}
 
public String getTxTHdInfArq() { 
   return txtHdInfArq; 
} 
 
public void setTxTHdInfArq(String txtHdInfArq) { 
   this.txtHdInfArq = txtHdInfArq; 
}
 
} 
 
