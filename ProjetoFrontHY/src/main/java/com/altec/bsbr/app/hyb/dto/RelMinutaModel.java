package com.altec.bsbr.app.hyb.dto;

public class RelMinutaModel {
	private String dataAtual;
	private String horaAtual;
	private String dataAbertura;	
	private String txAbertura;
	
	public RelMinutaModel(String dataAtual, String horaAtual, String dataAbertura, String txAbertura) {
		this.dataAtual = dataAtual;
		this.horaAtual = horaAtual;
		this.dataAbertura = dataAbertura;
		this.txAbertura = txAbertura;
	}

	public RelMinutaModel() {
	}

	public String getDataAtual() {
		return dataAtual;
	}

	public void setDataAtual(String dataAtual) {
		this.dataAtual = dataAtual;
	}

	public String getHoraAtual() {
		return horaAtual;
	}

	public void setHoraAtual(String horaAtual) {
		this.horaAtual = horaAtual;
	}

	public String getDataAbertura() {
		return dataAbertura;
	}

	public void setDataAbertura(String dataAbertura) {
		this.dataAbertura = dataAbertura;
	}

	public String getTxAbertura() {
		return txAbertura;
	}

	public void setTxAbertura(String txAbertura) {
		this.txAbertura = txAbertura;
	}

}
