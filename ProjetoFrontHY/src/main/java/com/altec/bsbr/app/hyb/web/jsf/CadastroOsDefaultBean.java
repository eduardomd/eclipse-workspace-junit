package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.CadastroOsFoModel;
import com.altec.bsbr.app.hyb.dto.CadastroOsSLinhaObjCanais;
import com.altec.bsbr.app.hyb.dto.CadastroOsSLinhaObjRsCanal;
import com.altec.bsbr.app.hyb.dto.OperAuxModel;
import com.altec.bsbr.app.hyb.dto.OperCorpModel;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsCanais.XHYCadOsCanaisEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsEventos.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsEventos.XHYCadOsEventosEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsUnidEnvol.XHYCadOsUnidEnvolEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;

@Component("cadastroOsDefaultBean")
@Scope("session")
public class CadastroOsDefaultBean extends BasicBBean {
	private static final long serialVersionUID = 1L;

	private long strNrSeqDetalhe;
	private String strNrCanal;
	private String strNrEvento;
	private String retorno;
	private boolean retSalvar;

	private CadastroOsFoModel objCadastroOsDefault;

	private CadastroOsSLinhaObjRsCanal objRsCanal;
	private List<CadastroOsSLinhaObjCanais> objCanais;
	private String selectRdoContestacao;

	private List<OperCorpModel> objRsOperCorp;
	private List<OperAuxModel> objRsOperAux;
	private String selectedOperCorp;
	private String selectedOperAux;
	private String strDtPeriFixa;
	private String strDtPeriIni;
	private String strDtPeriFim;
	private int intQntLancDbto = 0;
	private int ibtQntLancCrdt = 0;
	private int intQntLancCrdt = 0;
	private double intValAux = 0;
	private boolean destExists;
	private boolean isCredito;
	private double intQntValDbto;
	private double intQntValCrdt;
	private Locale ptBR = new Locale("pt", "BR");
	private boolean chkItssComl;

	@Autowired
	private XHYCadOsEventosEndPoint cadOsEvento;
	@Autowired
	private XHYCadOsCanaisEndPoint cadOsCanais;
	@Autowired
	private XHYCadOsUnidEnvolEndPoint cadOsUnidEnvolv;

	@PostConstruct
	public void init() {
		try {
			Map<String, String> paramMap = (Map<String, String>) FacesContext.getCurrentInstance().getExternalContext()
					.getRequestParameterMap();
			strNrSeqDetalhe = Long.parseLong(paramMap.get("NrSeqDetalhe"));
			strNrCanal = paramMap.get("qsCanal");
			strNrEvento = paramMap.get("qsEvento");

			pegarDados();
			
			consultarFraudeEvento();
			consultarOperacaoCanal();
			carregaComboOper(1);
			carregaComboOperAux(1);
			
			
			System.out.println("CARREGAR DADOS");
		} catch (Exception e) {
			logger.debug("error", e);
		}
	}
	
	public void pegarDados() {
		objCadastroOsDefault = new CadastroOsFoModel();

		try {
			retorno = cadOsEvento.consultarFraudeCanalDetalhe(strNrSeqDetalhe);
			JSONObject rstemp = new JSONObject(retorno);
			JSONArray pcursor = rstemp.getJSONArray("PCURSOR");
			JSONObject f = pcursor.getJSONObject(0);

			objCadastroOsDefault.setNrSequOrdeServ(f.isNull("NR_SEQU_ORDE_SERV") ? "" : f.get("NR_SEQU_ORDE_SERV").toString());
			objCadastroOsDefault.setNrSequFrauEven(f.isNull("NR_SEQU_FRAU_EVEN") ? "" : f.get("NR_SEQU_FRAU_EVEN").toString());
			objCadastroOsDefault.setNmParp(f.isNull("NM_PARP") ? "" : f.get("NM_PARP").toString());
			objCadastroOsDefault.setNrSequCntaBncr(f.isNull("NR_SEQU_CNTA_BNCR") ? "" : f.get("NR_SEQU_CNTA_BNCR").toString());
			objCadastroOsDefault.setNrSequCrto(f.isNull("NR_SEQU_CRTO") ? "" : f.get("NR_SEQU_CRTO").toString());
			objCadastroOsDefault.setNrCnta(f.isNull("NR_CNTA") ? "" : f.get("NR_CNTA").toString());
			objCadastroOsDefault.setNrCpfCnpjTitl(f.isNull("NR_CPF_CNPJ_TITL") ? "" : f.get("NR_CPF_CNPJ_TITL").toString());
			objCadastroOsDefault.setNrQntdEven(f.isNull("NR_QNTD_EVEN") ? "" : f.get("NR_QNTD_EVEN").toString());
			objCadastroOsDefault.setCdCentCustOrig(f.isNull("CD_CENT_CUST_ORIG") ? "" : f.get("CD_CENT_CUST_ORIG").toString());
			objCadastroOsDefault.setCdCentCustDest(f.isNull("CD_CENT_CUST_DEST") ? "" : f.get("CD_CENT_CUST_DEST").toString());
			objCadastroOsDefault.setCdCentCustOpnt(f.isNull("CD_CENT_CUST_OPNT") ? "" : f.get("CD_CENT_CUST_OPNT").toString());
			objCadastroOsDefault.setContabilizado(f.isNull("contabilizado") ? "" : f.get("contabilizado").toString());
		} catch (WebServiceException e) {
			// TODO Auto-generated catch block

			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (Exception ex) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public void salvar() {
		RequestContext.getCurrentInstance().execute("validarCentros('0')");
	}

	// INC
	public void salvarOperacao() {
		try {
			for (CadastroOsSLinhaObjCanais canal : this.objCanais) {
				cadOsCanais.salvarOperacao(canal.getNR_SEQU_TRAN_FRAU_CNAL(), canal.getStrDtOper(),
						canal.getStrHrOper(), canal.getStrVlOper().replace(".", "").replace(",", "."),
						this.objCadastroOsDefault.getCdCentCustOrig(), this.objCadastroOsDefault.getCdCentCustDest(),
						this.objCadastroOsDefault.getCdCentCustOpnt(), this.objCadastroOsDefault.getNrQntdEven());
			}

			// Consultar de novo
			System.out.println("AQUI!!!");
			FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds()
					.add("frmOs:tbInserirOp");		

			RequestContext.getCurrentInstance().execute("AtualizaTotais()");
		} catch (Exception e) {
			e.printStackTrace();
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	// ------------------------------
	public void clearBean() {
		String type = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("pageType").toString();
		String nrSeq = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("nrSeq").toString();
		switch(type) {
			case "FE":
				System.out.println("nrSeq - "+nrSeq+" - FE "+FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("CadastroOsDetalheProdutoFEBean"));
				if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("CadastroOsDetalheProdutoFEBean") != null)
					FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CadastroOsDetalheProdutoFEBean");
				RequestContext.getCurrentInstance().execute("document.getElementById('modal-detalhe-produto-fo-fe').src = 'hyb_cadastroos_detalheprodutofe.xhtml?strNrSeqTranFrau="+nrSeq+"'");
				RequestContext.getCurrentInstance().execute("PF('detalhe-produto-fo-fe').show();");
				break;
			
			case "FO":
				System.out.println("nrSeq - "+nrSeq+" - FO "+FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("CadastroOsDetalheProdutoFOBean"));
				if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("CadastroOsDetalheProdutoFOBean") != null)
					FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CadastroOsDetalheProdutoFOBean");
				RequestContext.getCurrentInstance().execute("document.getElementById('modal-detalhe-produto-fo-fe').src = 'hyb_cadastroos_detalheprodutofo.xhtml?strNrSeqTranFrau="+nrSeq+"'");
				RequestContext.getCurrentInstance().execute("PF('detalhe-produto-fo-fe').show();");
				break;
			
			default:
				System.out.println("clearBean - não encontrado");
				break;
		}
	}


	public void consultarOperacaoCanal() {
		try {
			this.intQntLancDbto = 0;
			this.intQntLancCrdt = 0;

			JSONObject json = new JSONObject(cadOsCanais.retornaOperacaoCanal(String.valueOf(this.strNrSeqDetalhe)));

			JSONArray pcursor = json.getJSONArray("PCURSOR");

			System.out.println("CURS OR: " + pcursor);

			objCanais = new ArrayList<CadastroOsSLinhaObjCanais>();

			for (int i = 0; i < pcursor.length(); i++) {
				JSONObject item = pcursor.getJSONObject(i);

				String nR_SEQU_TIPO_OPER_TRAN = item.isNull("NR_SEQU_TIPO_OPER_TRAN") ? ""
						: item.get("NR_SEQU_TIPO_OPER_TRAN").toString();
				String cD_TRAN = item.isNull("CD_TRAN") ? "" : item.get("CD_TRAN").toString();
				String nM_OPER = item.isNull("NM_OPER") ? "" : item.get("NM_OPER").toString();
				String nM_SUB_OPER = item.isNull("NM_SUB_OPER") ? "" : item.get("NM_SUB_OPER").toString();
				String cD_PROD_AUXI = item.isNull("CD_PROD_AUXI") ? "" : item.get("CD_PROD_AUXI").toString();
				String nM_PROD_AUXI = item.isNull("NM_PROD_AUXI") ? "" : item.get("NM_PROD_AUXI").toString();
				String vL_TRAN = item.isNull("VL_TRAN") ? "0" : item.get("VL_TRAN").toString();
				String tP_CNTE = item.isNull("TP_CNTE") ? "" : item.get("TP_CNTE").toString();
				String dT_TRAN = item.isNull("DT_TRAN") ? "" : item.get("DT_TRAN").toString();
				String hR_TRAN = item.isNull("HR_TRAN") ? "" : item.get("HR_TRAN").toString();
				String dT_OCOR_FIXA = item.isNull("DT_OCOR_FIXA") ? "" : item.get("DT_OCOR_FIXA").toString();
				String dT_OCOR_PERI_INIC = item.isNull("DT_OCOR_PERI_INIC") ? ""
						: item.get("DT_OCOR_PERI_INIC").toString();
				String dT_OCOR_PERI_FINA = item.isNull("DT_OCOR_PERI_FINA") ? ""
						: item.get("DT_OCOR_PERI_FINA").toString();
				String cARD = item.isNull("CARD") ? "" : item.get("CARD").toString();
				String nR_SEQU_TRAN_FRAU_CNAL = item.isNull("NR_SEQU_TRAN_FRAU_CNAL") ? ""
						: item.get("NR_SEQU_TRAN_FRAU_CNAL").toString();
				String dESC_TRAN = item.isNull("DESC_TRAN") ? "" : item.get("DESC_TRAN").toString();
				String dESC_CNTE = item.isNull("DESC_CNTE") ? "" : item.get("DESC_CNTE").toString();

				String strDtOper;
				String strHrOper;
				String strVlOper;

				if (item.isNull("VL_TRAN")) {
					this.intValAux = 0;
					strVlOper = "";
				} else {
					this.intValAux = Double.parseDouble(item.get("VL_TRAN").toString().replace(",", "."));
					strVlOper = formataValorRecebido(item.get("VL_TRAN").toString());
				}

				if (tP_CNTE.equals("1")) {
					this.intQntLancDbto += 1;
				} else if (tP_CNTE.equals("2")) {
					this.intQntLancCrdt += 1;
				}

				if (dT_TRAN.equals("01/01/1900")) {
					strDtOper = "";
				} else {
					strDtOper = dT_TRAN;
				}

				if (hR_TRAN.equals("00:00")) {
					strHrOper = "";
				} else {
					strHrOper = hR_TRAN;
				}

				if (!item.isNull("DT_TRAN") && strDtOper.equals("") && !dT_OCOR_FIXA.equals("")) {
					strDtOper = dT_OCOR_FIXA;
				}

				CadastroOsSLinhaObjCanais modelCanais = new CadastroOsSLinhaObjCanais(nR_SEQU_TIPO_OPER_TRAN, cD_TRAN,
						nM_OPER, nM_SUB_OPER, cD_PROD_AUXI, nM_PROD_AUXI, vL_TRAN, tP_CNTE, dT_TRAN, hR_TRAN,
						dT_OCOR_FIXA, dT_OCOR_PERI_INIC, dT_OCOR_PERI_FINA, cARD, nR_SEQU_TRAN_FRAU_CNAL, dESC_TRAN,
						dESC_CNTE, strDtOper, strHrOper, strVlOper);
				objCanais.add(modelCanais);

				this.strDtPeriFixa = dT_OCOR_FIXA;
				this.strDtPeriIni = dT_OCOR_PERI_INIC;
				this.strDtPeriFim = dT_OCOR_PERI_FINA;
			}

			atualizarValores();

		} catch (JSONException | com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsCanais.WebServiceException e) {
			e.printStackTrace();
		}
	}

	public void verifUnidadeEnvolvCPV() {
		System.out.println("VERIF 1");
		try {
			String tipoProduto = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
					.get("tipoProduto").toString();

			if (!objRsCanal.getCD_CENT_CUST_ORIG().equals("6494")) {
				JSONObject json = new JSONObject();
				json = new JSONObject(cadOsUnidEnvolv.fnSelPVNr("1", objRsCanal.getCD_CENT_CUST_ORIG(), ""));
				JSONArray pcursor = json.getJSONArray("PCURSOR");

				if (pcursor.length() <= 0) {
					RequestContext.getCurrentInstance().execute("confirmarCentroOrigem(" + tipoProduto + ");");
				} else {
					// ret = true;
					verifUnidadeEnvolvCPVDest(tipoProduto);
				}
			} else {
				verifUnidadeEnvolvCPVDest(tipoProduto);
			}

		} catch (JSONException | com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsUnidEnvol.WebServiceException e) {
			e.printStackTrace();
		}
	}

	public void verifUnidadeEnvolvCPVDest(String tipoProduto) {
		System.out.println("VERIF 2");
		try {
			if (tipoProduto.isEmpty()) {
				tipoProduto = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
						.get("tipoProduto").toString();
			}

			if (!this.objRsCanal.getCD_CENT_CUST_DEST().equals("6494")) {

				JSONObject json = new JSONObject();
				json = new JSONObject(cadOsUnidEnvolv.fnSelPVNr("1", this.objRsCanal.getCD_CENT_CUST_DEST(), ""));
				JSONArray pcursor = json.getJSONArray("PCURSOR");
				if (pcursor.length() <= 0) {
					this.destExists = false;
					RequestContext.getCurrentInstance().execute("confirmarCentroDest(" + tipoProduto + ");");
				} else {
					adicionarProduto(tipoProduto);
				}

			} else {
				adicionarProduto(tipoProduto);
			}

		} catch (JSONException | com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsUnidEnvol.WebServiceException e) {
			e.printStackTrace();
		}
	}

	public void carregaComboOper(Integer intContestacao) {
		this.objRsOperCorp = new ArrayList<OperCorpModel>();
		try {
			JSONObject json = new JSONObject();
			json = new JSONObject(cadOsCanais.consultarOperacao(this.strNrCanal, intContestacao));
			// NM_OPER
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			for (int i = 0; i < pcursor.length(); i++)
				this.objRsOperCorp.add(new OperCorpModel(
						pcursor.getJSONObject(i).isNull("NR_SEQU_TIPO_OPER_TRAN") ? ""
								: String.valueOf(pcursor.getJSONObject(i).getInt("NR_SEQU_TIPO_OPER_TRAN")),
						pcursor.getJSONObject(i).isNull("CD_TRAN") ? "" : pcursor.getJSONObject(i).getString("CD_TRAN"),
						pcursor.getJSONObject(i).isNull("NM_SUB_OPER") ? ""
								: pcursor.getJSONObject(i).getString("NM_SUB_OPER"),
						pcursor.getJSONObject(i).isNull("NM_OPER") ? ""
								: pcursor.getJSONObject(i).getString("NM_OPER")));

		} catch (Exception e) {
			e.printStackTrace();
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	public void carregaComboOperAux(Integer intContestacao) {
		this.objRsOperAux = new ArrayList<OperAuxModel>();
		try {
			JSONObject json = new JSONObject();
			json = new JSONObject(cadOsCanais.consultarOperacaoAux(this.strNrCanal, intContestacao));
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			for (int i = 0; i < pcursor.length(); i++)
				this.objRsOperAux.add(new OperAuxModel(
						pcursor.getJSONObject(i).isNull("CD_PROD_AUXI") ? ""
								: String.valueOf(pcursor.getJSONObject(i).getInt("CD_PROD_AUXI")),
						pcursor.getJSONObject(i).isNull("NM_PROD_AUXI") ? ""
								: pcursor.getJSONObject(i).getString("NM_PROD_AUXI")));

			System.out.println("objRsOperAux " + this.objRsOperAux.size());

		} catch (Exception e) {
			e.printStackTrace();
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	public void handleIsCredito(AjaxBehaviorEvent abe) {
		this.isCredito = !this.isCredito;
		if (this.isCredito) {
			carregaComboOper(2);
			carregaComboOperAux(2);
		} else {
			carregaComboOper(1);
			carregaComboOperAux(1);
		}
	}

	public void adicionarProduto(String tipoProduto) {
		System.out.println("ADICIONAR PRODUTO");
		// 1-Corp 2-Aux

		if (tipoProduto.isEmpty()) {
			tipoProduto = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
					.get("tipoProduto").toString();
		}
		
		if (tipoProduto == "1" || tipoProduto == "2") {
			String intTpContestacao = "";
			String strTipoContestacao = "";
	
			String strDescOperacao;
	
			if (tipoProduto.equals("1") && this.selectedOperCorp.isEmpty()) {
				RequestContext.getCurrentInstance().execute("alert('Selecione uma Operação')");
			} else if (tipoProduto.equals("2") && this.selectedOperAux.isEmpty()) {
				// alert
				RequestContext.getCurrentInstance().execute("alert('Selecione uma Operação')");
			} else {
	
				try {
	
					if (tipoProduto.equals("1")) {
	
						strDescOperacao = this.objRsOperCorp.stream()
								.filter(item -> item.getNR_SEQU_TIPO_OPER_TRAN().equals(this.selectedOperCorp))
								.collect(Collectors.toList()).get(0).getLabel().substring(7);
	
						System.out.println("DESCOPERACAO ->" + strDescOperacao);
					} else {
						strDescOperacao = this.objRsOperAux.stream()
								.filter(item -> item.getCD_PROD_AUXI().equals(this.selectedOperAux))
								.collect(Collectors.toList()).get(0).getNM_PROD_AUXI();
					}
	
					// Checked
					if (this.isCredito) {
						intTpContestacao = "2"; // Credito
						strTipoContestacao = "Crédito";
					} else {
						strTipoContestacao = "Débito";
						intTpContestacao = "1"; // Debito
					}
	
					CadastroOsSLinhaObjCanais canaisModel = new CadastroOsSLinhaObjCanais();
	
					JSONObject json = new JSONObject();
					System.out.println(strNrSeqDetalhe + "," + this.selectedOperCorp + "," + this.selectedOperAux + ","
							+ "1" + "," + intTpContestacao + "," + this.objRsCanal.getCD_CENT_CUST_ORIG() + ","
							+ this.objRsCanal.getCD_CENT_CUST_DEST() + "," + this.objRsCanal.getCD_CENT_CUST_OPNT() + ","
							+ this.objRsCanal.getNR_QNTD_EVEN());
	
					if (tipoProduto.equals("1")) {
						json = new JSONObject(cadOsCanais.inserirOperacaoRetSeqTranFrau(String.valueOf(strNrSeqDetalhe),
								this.selectedOperCorp, "", "1", intTpContestacao, this.objRsCanal.getCD_CENT_CUST_ORIG(),
								this.objRsCanal.getCD_CENT_CUST_DEST(), this.objRsCanal.getCD_CENT_CUST_OPNT(),
								this.objRsCanal.getNR_QNTD_EVEN()));
					} else {
						json = new JSONObject(cadOsCanais.inserirOperacaoRetSeqTranFrau(String.valueOf(strNrSeqDetalhe), "",
								this.selectedOperAux, "2", intTpContestacao, this.objRsCanal.getCD_CENT_CUST_ORIG(),
								this.objRsCanal.getCD_CENT_CUST_DEST(), this.objRsCanal.getCD_CENT_CUST_OPNT(),
								this.objRsCanal.getNR_QNTD_EVEN()));
					}
	
					JSONArray pcursor = json.getJSONArray("PCURSOR");
					JSONObject item = pcursor.getJSONObject(0);
	
					if (this.selectRdoContestacao.equals("1")) {
						this.intQntLancDbto += 1;
					} else if (this.selectRdoContestacao.equals("2")) {
						this.intQntLancCrdt += 1;
					}
	
					canaisModel.setNR_SEQU_TRAN_FRAU_CNAL(item.get("NR_SEQU_TRAN_FRAU_CNAL").toString());
					canaisModel.setDESC_TRAN(strDescOperacao);
					canaisModel.setDESC_CNTE(strTipoContestacao);
					canaisModel.setTP_CNTE(intTpContestacao);
	
					objCanais.add(canaisModel);
					this.atualizarValores();
					System.out.println("CLEAR RENDER IDS AQUI");
					FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds()
							.add("frmDetalheSuperLinha:tbInserirOp");
	
				} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsCanais.WebServiceException e) {
					e.printStackTrace();
					try {
						FacesContext.getCurrentInstance().getExternalContext()
								.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
		}else {
			salvarOperacao();
		}
	}

	public void atualizarValores() {
		double valDbt = 0;
		double valCrdt = 0;

		for (int i = 0; i < objCanais.size(); i++) {
			System.out.println("CNTE: " + objCanais.get(i).getTP_CNTE());
			if (objCanais.get(i).getTP_CNTE() != null) {
				if (objCanais.get(i).getTP_CNTE().equals("2")) {
					if (objCanais.get(i).getStrVlOper() == null || objCanais.get(i).getStrVlOper().equals("")) {
						valCrdt += 0;
					} else {
						valCrdt += Double.parseDouble(
								objCanais.get(i).getStrVlOper().toString().replace(".", "").replace(",", "."));
					}

				} else {
					if (objCanais.get(i).getStrVlOper() == null || objCanais.get(i).getStrVlOper().equals("")) {
						valDbt += 0;
					} else {
						valDbt += Double.parseDouble(
								objCanais.get(i).getStrVlOper().toString().replace(".", "").replace(",", "."));
					}
				}
			}
		}

		this.intQntValCrdt = valCrdt;
		this.intQntValDbto = valDbt;
	}

	public String formataValorRecebido(String valorRecebido) {
		String valor = NumberFormat.getCurrencyInstance(ptBR)
				.format(Double.parseDouble(valorRecebido.replace(",", ".")));
		return valor.replace("R$", "").trim();
	}

	public void excluirOperacao() {
		String nrSeqFrauCanal = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
				.get("strNrSeqTranFrau");
		int index = Integer
				.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("index"));
		String descCnte = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
				.get("desc_cnte").toString();

		try {
			cadOsCanais.excluirOperacao(nrSeqFrauCanal);

			if (descCnte.equals("Débito") && this.intQntLancDbto >= 0) {
				this.intQntLancDbto -= 1;
			} else if (descCnte.equals("Crédito") && this.intQntLancCrdt >= 0) {
				this.intQntLancCrdt -= 1;
			}

			// objCanais.clear();
			System.out.println("dsadasd");
			objCanais.remove(index);
			this.atualizarValores();
		} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsCanais.WebServiceException e) {
			e.printStackTrace();
		}
	}

	public String[] formataTelefone(String str) {
		String newStr = ("0000000000" + str).substring(str.length());

		String n2 = newStr.substring(2, newStr.length() - 4);
		String ddd = newStr.substring(0, 2);
		String n3 = newStr.substring(newStr.length() - 4);
		String fullNumber = n2 + "-" + n3;
		String formattefTel = "(" + ddd + ") " + fullNumber;
		String[] numArr = { ddd, fullNumber, formattefTel };
		return numArr;
	}

	public void consultarFraudeEvento() {
		try {

			JSONObject json = new JSONObject(cadOsEvento.consultarFraudeCanalDetalhe(this.strNrSeqDetalhe));

			JSONArray pcursor = json.getJSONArray("PCURSOR");

			JSONObject item = pcursor.getJSONObject(0);

			String strTelfClieDDD = formataTelefone(
					item.isNull("NR_TELF_CLIE") ? "" : item.get("NR_TELF_CLIE").toString())[0];
			String strTelfClie = formataTelefone(
					item.isNull("NR_TELF_CLIE") ? "" : item.get("NR_TELF_CLIE").toString())[1];

			String strAssTelSolicDDD = formataTelefone(
					item.isNull("NR_TELF_SOLI_ASSN_ELET") ? "" : item.get("NR_TELF_SOLI_ASSN_ELET").toString())[0];
			String strAssTelSolic = formataTelefone(
					item.isNull("NR_TELF_SOLI_ASSN_ELET") ? "" : item.get("NR_TELF_SOLI_ASSN_ELET").toString())[1];

			String strAssTelCadDDD = formataTelefone(
					item.isNull("NR_TELF_CADR_ASSN_ELET") ? "" : item.get("NR_TELF_CADR_ASSN_ELET").toString())[0];
			String strAssTelCad = formataTelefone(
					item.isNull("NR_TELF_CADR_ASSN_ELET") ? "" : item.get("NR_TELF_CADR_ASSN_ELET").toString())[1];

			String cdBanc = item.isNull("CD_BANC") ? "" : item.get("CD_BANC").toString();
			String strAgencia = ("0000" + cdBanc).substring(cdBanc.length());

			this.chkItssComl = item.get("IN_ITSS_COME").toString().equals("0") ? false : true;

			Date dtPrmFrau;
			if (!item.isNull("DT_PRMR_FRAU"))
				dtPrmFrau = new Date((long) item.getInt("DT_PRMR_FRAU") * 1000);
			else
				dtPrmFrau = null;

			objRsCanal = new CadastroOsSLinhaObjRsCanal(
					item.isNull("NR_SEQU_ORDE_SERV") ? "" : item.get("NR_SEQU_ORDE_SERV").toString(),
					item.isNull("NM_PARP") ? "" : item.get("NM_PARP").toString(),
					item.isNull("NR_SEQU_PARP_PROC") ? "" : item.get("NR_SEQU_PARP_PROC").toString(),
					item.isNull("NR_CNTA") ? "" : item.get("NR_CNTA").toString(),
					item.isNull("NR_CPF_CNPJ_TITL") ? "" : item.get("NR_CPF_CNPJ_TITL").toString(),
					item.isNull("DT_FRAU") ? "" : item.get("DT_FRAU").toString(), strTelfClie, strTelfClieDDD,
					item.isNull("IN_BLOQ_TELF") ? "0" : item.get("IN_BLOQ_TELF").toString(),
					item.isNull("DT_BLOQ_TELF") ? "" : item.get("DT_BLOQ_TELF").toString(),
					item.isNull("HR_BLOQ_TELF") ? "" : item.get("HR_BLOQ_TELF").toString(),
					item.isNull("DT_SOLI_ASSN_ELET") ? "" : item.get("DT_SOLI_ASSN_ELET").toString(), strAssTelSolic,
					strAssTelSolicDDD, item.isNull("HR_SOLI_ASSN_ELET") ? "" : item.get("HR_SOLI_ASSN_ELET").toString(),
					item.isNull("DT_CADR_ASSN_ELET") ? "" : item.get("DT_CADR_ASSN_ELET").toString(), strAssTelCad,
					strAssTelCadDDD, item.isNull("HR_CADR_ASSN_ELET") ? "" : item.get("HR_CADR_ASSN_ELET").toString(),
					item.isNull("TP_CNTA_BNCR") ? "" : item.get("TP_CNTA_BNCR").toString(),
					item.isNull("DT_ABER_CNTA_BNCR") ? "" : item.get("DT_ABER_CNTA_BNCR").toString(),
					item.isNull("NM_GERE_RESP_CNTA_BNCR") ? "" : item.get("NM_GERE_RESP_CNTA_BNCR").toString(),
					item.isNull("IN_CRTO_SEGR") ? "" : item.get("IN_CRTO_SEGR").toString(),
					item.isNull("DT_SOLI_CRTO_SEGR") ? "" : item.get("DT_SOLI_CRTO_SEGR").toString(),
					item.isNull("DT_CANC_CRTO_SEGR") ? "" : item.get("DT_CANC_CRTO_SEGR").toString(),
					item.isNull("DT_ATIV_CRTO_SEGR") ? "" : item.get("DT_ATIV_CRTO_SEGR").toString(),
					dtPrmFrau == null ? "" : new SimpleDateFormat("dd/MM/yyyy").format(dtPrmFrau),
					item.isNull("NR_SEQU_CNTA_BNCR") ? "" : item.get("NR_SEQU_CNTA_BNCR").toString(),
					item.isNull("NR_SEQU_CRTO") ? "" : item.get("NR_SEQU_CRTO").toString(),
					item.isNull("NR_SEQU_FRAU_EVEN") ? "" : item.get("NR_SEQU_FRAU_EVEN").toString(),
					item.isNull("NR_SEQU_FRAU_CNAL") ? "" : item.get("NR_SEQU_FRAU_CNAL").toString(),
					item.isNull("CD_CNAL") ? "" : item.get("CD_CNAL").toString(),
					item.isNull("IN_ITSS_COME") ? "" : item.get("IN_ITSS_COME").toString(), strAgencia,
					item.isNull("DT_EMIS") ? "" : item.get("DT_EMIS").toString(),
					item.isNull("DT_ATIV") ? "" : item.get("DT_ATIV").toString(),
					item.isNull("DT_CANC") ? "" : item.get("DT_CANC").toString(),
					item.isNull("NM_LOG_RECU_SOLI") ? "" : item.get("NM_LOG_RECU_SOLI").toString(),
					item.isNull("NM_SITU_CNTA_BNCR") ? "" : item.get("NM_SITU_CNTA_BNCR").toString(),
					item.isNull("NM_ENDE_ENVI") ? "" : item.get("NM_ENDE_ENVI").toString(),
					item.isNull("NR_QNTD_EVEN") ? "" : item.get("NR_QNTD_EVEN").toString(),
					item.isNull("CD_CENT_CUST_ORIG") ? "" : item.get("CD_CENT_CUST_ORIG").toString(),
					item.isNull("CD_CENT_CUST_DEST") ? "" : item.get("CD_CENT_CUST_DEST").toString(), "6494", // CD_CENT_CUST_OPNT
					item.isNull("CONTABILIZADO") ? "" : item.get("CONTABILIZADO").toString());

		} catch (WebServiceException e) {
			e.printStackTrace();
		}
	}
	// -------------------------
	//-------------------------
	
	public CadastroOsFoModel getobjCadastroOsDefault() {
		return objCadastroOsDefault;
	}

	public long getstrNrSeqDetalhe() {
		return strNrSeqDetalhe;
	}

	public String getstrNrCanal() {
		return strNrCanal;
	}

	public String getstrNrEvento() {
		return strNrEvento;
	}

	public long getStrNrSeqDetalhe() {
		return strNrSeqDetalhe;
	}

	public void setStrNrSeqDetalhe(long strNrSeqDetalhe) {
		this.strNrSeqDetalhe = strNrSeqDetalhe;
	}

	public String getStrNrCanal() {
		return strNrCanal;
	}

	public void setStrNrCanal(String strNrCanal) {
		this.strNrCanal = strNrCanal;
	}

	public String getStrNrEvento() {
		return strNrEvento;
	}

	public void setStrNrEvento(String strNrEvento) {
		this.strNrEvento = strNrEvento;
	}

	public String getRetorno() {
		return retorno;
	}

	public void setRetorno(String retorno) {
		this.retorno = retorno;
	}

	public boolean isRetSalvar() {
		return retSalvar;
	}

	public void setRetSalvar(boolean retSalvar) {
		this.retSalvar = retSalvar;
	}

	public CadastroOsFoModel getObjCadastroOsDefault() {
		return objCadastroOsDefault;
	}

	public void setObjCadastroOsDefault(CadastroOsFoModel objCadastroOsDefault) {
		this.objCadastroOsDefault = objCadastroOsDefault;
	}

	public CadastroOsSLinhaObjRsCanal getObjRsCanal() {
		return objRsCanal;
	}

	public void setObjRsCanal(CadastroOsSLinhaObjRsCanal objRsCanal) {
		this.objRsCanal = objRsCanal;
	}

	public List<CadastroOsSLinhaObjCanais> getObjCanais() {
		return objCanais;
	}

	public void setObjCanais(List<CadastroOsSLinhaObjCanais> objCanais) {
		this.objCanais = objCanais;
	}

	public String getSelectRdoContestacao() {
		return selectRdoContestacao;
	}

	public void setSelectRdoContestacao(String selectRdoContestacao) {
		this.selectRdoContestacao = selectRdoContestacao;
	}

	public List<OperCorpModel> getObjRsOperCorp() {
		return objRsOperCorp;
	}

	public void setObjRsOperCorp(List<OperCorpModel> objRsOperCorp) {
		this.objRsOperCorp = objRsOperCorp;
	}

	public List<OperAuxModel> getObjRsOperAux() {
		return objRsOperAux;
	}

	public void setObjRsOperAux(List<OperAuxModel> objRsOperAux) {
		this.objRsOperAux = objRsOperAux;
	}

	public String getSelectedOperCorp() {
		return selectedOperCorp;
	}

	public void setSelectedOperCorp(String selectedOperCorp) {
		this.selectedOperCorp = selectedOperCorp;
	}

	public String getSelectedOperAux() {
		return selectedOperAux;
	}

	public void setSelectedOperAux(String selectedOperAux) {
		this.selectedOperAux = selectedOperAux;
	}

	public String getStrDtPeriFixa() {
		return strDtPeriFixa;
	}

	public void setStrDtPeriFixa(String strDtPeriFixa) {
		this.strDtPeriFixa = strDtPeriFixa;
	}

	public String getStrDtPeriIni() {
		return strDtPeriIni;
	}

	public void setStrDtPeriIni(String strDtPeriIni) {
		this.strDtPeriIni = strDtPeriIni;
	}

	public String getStrDtPeriFim() {
		return strDtPeriFim;
	}

	public void setStrDtPeriFim(String strDtPeriFim) {
		this.strDtPeriFim = strDtPeriFim;
	}

	public int getIntQntLancDbto() {
		return intQntLancDbto;
	}

	public void setIntQntLancDbto(int intQntLancDbto) {
		this.intQntLancDbto = intQntLancDbto;
	}

	public int getIbtQntLancCrdt() {
		return ibtQntLancCrdt;
	}

	public void setIbtQntLancCrdt(int ibtQntLancCrdt) {
		this.ibtQntLancCrdt = ibtQntLancCrdt;
	}

	public int getIntQntLancCrdt() {
		return intQntLancCrdt;
	}

	public void setIntQntLancCrdt(int intQntLancCrdt) {
		this.intQntLancCrdt = intQntLancCrdt;
	}

	public double getIntValAux() {
		return intValAux;
	}

	public void setIntValAux(double intValAux) {
		this.intValAux = intValAux;
	}

	public boolean isDestExists() {
		return destExists;
	}

	public void setDestExists(boolean destExists) {
		this.destExists = destExists;
	}

	public boolean isCredito() {
		return isCredito;
	}

	public void setCredito(boolean isCredito) {
		this.isCredito = isCredito;
	}

	public double getIntQntValDbto() {
		return intQntValDbto;
	}

	public void setIntQntValDbto(double intQntValDbto) {
		this.intQntValDbto = intQntValDbto;
	}

	public double getIntQntValCrdt() {
		return intQntValCrdt;
	}

	public void setIntQntValCrdt(double intQntValCrdt) {
		this.intQntValCrdt = intQntValCrdt;
	}

	public Locale getPtBR() {
		return ptBR;
	}

	public void setPtBR(Locale ptBR) {
		this.ptBR = ptBR;
	}

	public boolean isChkItssComl() {
		return chkItssComl;
	}

	public void setChkItssComl(boolean chkItssComl) {
		this.chkItssComl = chkItssComl;
	}

}
