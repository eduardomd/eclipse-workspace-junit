package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.CadTramiteModel;
import com.altec.bsbr.app.jab.hyb.webclient.XHYTramite.XHYTramiteEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;


@Component("cadTramite")
@Scope("session")
public class CadTramiteBean extends BasicBBean {
	private static final long serialVersionUID = 1L;

	private String strReadOnly;
	private String strTitulo;
	private String strMsg;
	
	// Textarea
	private String strHistEnvio;
	private String strHistRetorno;
			
	private String strNumeroTra;
	private String strOS;
	private String strOSField;
	private Date strDataRegistro;
	private Date strDtRetPlanejado;
	private Date strDtRetorno;
	private String strDestino;
	
	private String strOsQuery = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrOs") == null ? "" : (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrOs");
	private String nrTramiteQuery = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("NrTramite") == null ? "" : (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("NrTramite");

	private CadTramiteModel objRsOS;
	
	private String checkOSStatus;
	
	private String lever;
	
	@Autowired
	private XHYTramiteEndPoint admtramite;

	@PostConstruct
	public void init() {
		this.setStrReadOnly("false");
		
		System.out.println("strOsQuery: " + strOsQuery + " nrTramiteQuery " + nrTramiteQuery);
		if ( strOsQuery.isEmpty() && nrTramiteQuery.isEmpty() ) {
			this.setLever("true");
		} else {
			this.setLever("false");
		}
		
	}
	
	public void alterar() {
		System.out.println("strOsQuery:" + strOsQuery + " nrTramiteQuery: " + nrTramiteQuery);
		if ( strOsQuery.isEmpty() && nrTramiteQuery.isEmpty() ) {
			this.setLever("true");
		} else {
			this.setLever("false");
		}

		SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String newStrDataRegistro = newFormat.format(strDataRegistro);
		String newStrDtRetPlanejado = newFormat.format(strDtRetPlanejado);
		String newStrDtRetorno;
		
		if (strDtRetorno != null) {
			newStrDtRetorno = newFormat.format(strDtRetorno);
		} else {
			newStrDtRetorno = "";
		}
		
		System.out.println("\nATRIBUTOS : " + "\n strOSs: " + strOS + "\n strNumeroTra: " + strNumeroTra + "\n strDataRegistro:" + newStrDataRegistro + "\n strDtRetPlanejado: " + newStrDtRetPlanejado + "\n strDtRetorno: " + newStrDtRetorno + "\n strDestino:" + strDestino);
		
		try {
			admtramite.alterarTramite(strOS, strNumeroTra, newStrDataRegistro, newStrDtRetPlanejado, newStrDtRetorno, strDestino);
			RequestContext.getCurrentInstance().execute("parent.document.location.href = parent.document.location.href");  
		} catch (Exception e) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
         } catch (IOException e1) {
               e1.printStackTrace();
         }
		}
		this.setStrReadOnly("true");
	}

	public void salvar() {
		
		System.out.println("strOsQuery" + strOsQuery + "  nrTramiteQuery: " + nrTramiteQuery);
		if ( strOsQuery.isEmpty() && nrTramiteQuery.isEmpty() ) {
			this.setLever("true");
		} else {
			this.setLever("false");
		}
		
		SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String newStrDataRegistro = newFormat.format(strDataRegistro);
		String newStrDtRetPlanejado = "";
		
		if (strDtRetPlanejado != null) {
			newStrDtRetPlanejado = newFormat.format(strDtRetPlanejado);
		} else {
			newStrDataRegistro = "";
		}
		String newStrDtRetorno = newFormat.format(strDtRetPlanejado);
		
		System.out.println("\nATRIBUTOS: " + "\n strOsS: " + strOS + "\n strNumeroTra: " + strNumeroTra + "\n strDataRegistro:" + newStrDataRegistro + "\n strDtRetPlanejado: " + newStrDtRetPlanejado + "\n strDtRetorno: " + newStrDtRetorno + "\n strDestino:" + strDestino);
		
		try {
			admtramite.inserirTramite(strOS, strNumeroTra, newStrDataRegistro, newStrDtRetPlanejado, newStrDtRetorno, strDestino);
			this.setCheckOSStatus("A");
			RequestContext.getCurrentInstance().execute("parent.document.location.href = parent.document.location.href");
		} catch (Exception e) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
         } catch (IOException e1) {
               e1.printStackTrace();
         }
  }

        
		this.setStrReadOnly("true");
		
	}
	
	public void activateBean() {
		System.out.println("TEST OS:" + this.strOS + "TEST TRAMITE: " + this.strNumeroTra);
	}
	
	public void preencheNrTramite() {
		try {
			String usuaRetorno = admtramite.consultarProximoTramite(strOS);
			
			JSONObject consultarProximoTramite = new JSONObject(usuaRetorno);
            JSONArray pcursor = consultarProximoTramite.getJSONArray("PCURSOR");
            
            JSONObject item = pcursor.getJSONObject(0);
            
            
            this.setStrNumeroTra(String.valueOf(item.getInt("MAIOR")));
			
		} catch (Exception e) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
            } catch (IOException e1) {
	               e1.printStackTrace();
	        }
		}

	}
	
	public void salvarEnvio() {
		try {
			System.out.println("\n SALVAR ENVIO: " + "\n strOSField: " + strOSField + "\n strNumeroTra: " + strNumeroTra + "\n strHistEnvio: " + strHistEnvio);
			admtramite.inserirTramiteHistoricoEnvio(strOSField, strNumeroTra, strHistEnvio.replaceAll("'", "''"));
		} catch (Exception e) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
         } catch (IOException e1) {
               e1.printStackTrace();
         }
		}

	}
	
	public void salvarRetorno() {
		try {
			System.out.println("OS RETORN: " + this.getStrOS());
			admtramite.inserirTramiteHistoricoRetorno(strOSField, strNumeroTra, strHistRetorno.replaceAll("'", "''"));
		} catch (Exception e) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
	         } catch (IOException e1) {
	        	e1.printStackTrace();
	         }
		}

		
	}
	
	public void changeOsVar() {
		this.setStrOSField(this.strOS);
	}
	
	public void changeStatus() {
		this.setCheckOSStatus("A");
	}
	
	public void limparCampoOs() {
		this.setStrOS("");
		System.out.println("LIMPAR CAMP OS");
	}
	
	// GETTER'S & SETTER'S
	public String getStrOsQuery() {
		return strOsQuery;
	}

	public void setStrOsQuery(String strOsQuery) {
		this.strOsQuery = strOsQuery;
	}

	public String getNrTramiteQuery() {
		return nrTramiteQuery;
	}

	public void setNrTramiteQuery(String nrTramiteQuery) {
		this.nrTramiteQuery = nrTramiteQuery;
	}
	
	public String getStrReadOnly() {
		return strReadOnly;
	}

	public void setStrReadOnly(String strReadOnly) {
		this.strReadOnly = strReadOnly;
	}

	public String getStrTitulo() {
		return strTitulo;
	}

	public void setStrTitulo(String strTitulo) {
		this.strTitulo = strTitulo;
	}

	public String getStrMsg() {
		return strMsg;
	}

	public void setStrMsg(String strMsg) {
		this.strMsg = strMsg;
	}

	public String getStrNumeroTra() {
		return strNumeroTra;
	}

	public void setStrNumeroTra(String strNumeroTra) {
		this.strNumeroTra = strNumeroTra;
	}

	public String getStrOS() {
		return strOS;
	}

	public void setStrOS(String strOS) {
		this.strOS = strOS;
	}

	public Date getStrDataRegistro() {
		return strDataRegistro;
	}

	public void setStrDataRegistro(Date strDataRegistro) {
		this.strDataRegistro = strDataRegistro;
	}

	public Date getStrDtRetPlanejado() {
		return strDtRetPlanejado;
	}

	public void setStrDtRetPlanejado(Date strDtRetPlanejado) {
		this.strDtRetPlanejado = strDtRetPlanejado;
	}

	public Date getStrDtRetorno() {
		return strDtRetorno;
	}

	public void setStrDtRetorno(Date strDtRetorno) {
		this.strDtRetorno = strDtRetorno;
	}

	public String getStrDestino() {
		return strDestino;
	}

	public void setStrDestino(String strDestino) {
		this.strDestino = strDestino;
	}

	public CadTramiteModel getObjRsOS() {
		return objRsOS;
	}

	public void setObjRsOS(CadTramiteModel objRsOS) {
		this.objRsOS = objRsOS;
	}

	public String getCheckOSStatus() {
		return checkOSStatus;
	}

	public void setCheckOSStatus(String checkOSStatus) {
		this.checkOSStatus = checkOSStatus;
	}

	public XHYTramiteEndPoint getAdmtramite() {
		return admtramite;
	}

	public void setAdmtramite(XHYTramiteEndPoint admtramite) {
		this.admtramite = admtramite;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getStrHistEnvio() {
		return strHistEnvio;
	}

	public void setStrHistEnvio(String strHistEnvio) {
		this.strHistEnvio = strHistEnvio;
	}

	public String getStrHistRetorno() {
		return strHistRetorno;
	}

	public void setStrHistRetorno(String strHistRetorno) {
		this.strHistRetorno = strHistRetorno;
	}

	public String getLever() {
		return lever;
	}

	public void setLever(String lever) {
		this.lever = lever;
	}

	public String getStrOSField() {
		return strOSField;
	}

	public void setStrOSField(String strOSField) {
		this.strOSField = strOSField;
	}
	
}