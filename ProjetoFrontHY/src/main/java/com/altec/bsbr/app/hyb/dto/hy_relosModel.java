package com.altec.bsbr.app.hyb.dto;

public class hy_relosModel {
	
	//Variaveis com nome na tabela
	private String strVazio; //"     "
	private String strNumOs; //"N� OS" 
	private String strEvnto; //"Evento"
	private String strPart; //Participa��o
	private String strTpColab; //Tipo colaborador
	private String strCargo; //Cargo
	private String strMatricCpf; //Matricula CPF
	private String strPenalidade; //Penalidade
	private String strMotivo; //Motivo
	private String strNumPv; //Numero PV
	private String strNomPv; //Nome PV / Nome Uniorg
	private String strRegional; //Regional
	private String strRede; //Rede / PV
	private String strDataEnvRh; // Data Envio RH
	private String strDataEfetivRh; //Data efetiva��o RH
	private int strCartaoDebit; //Cart�o D�bito
	private int strCartaoCredit; //Cart�o Cr�dito
	private int strInternet; //Internet
	private int strSuperLinha; //Super Linha
	private String strTotal; //Total
	private String strDataAbertOS; //Data abertura OS
	private String strDataDetecOS; //Data detec��o OS
	private String strDataEncerramentoOS; //Data encerramento OS
	private String strStatusReport; //Status Report
	private String strEmpresa; //Empresa
	private String strLinhaNegoNvl1; //Linha Nego 1
	private String strLinhaNegoNvl2; //Linha Nego 2 
	private String strContaContab; //Conta Cont�bil
	private String strNvl1; //N�vel 1
	private String strNvl2; //N�vel 2
	private String strNvl3; //N�vel 3
	private String strFatorRisc; //Fator Risco
	private String strDataContab; //Data Contabiliza��o
	private double strValorEnvolv; //Valor Envolvido
	private double strValorEvit; //Valor Evitado
	private double strValorRecup; //Valor Recuperado
	private double strValorPreju; //Valor Preju�zo
	private String strBanco; //Banco
	private double strQuant; //Quantidade
	private String strCanal; //Canal                            
	private double strSegm; //Segmento
	private double strPreju; //Preju�zo
	private String strPorcento; //%   
	
	//Variaveis utilizadas em fun��es
	private String strDT;
	private int TP_PESS_CLIE;
	private int PREJUIZO;
	private int COUNT;
	private String NM_SEGM;
	private int CD_CNAL;
	private String NR_MES;
	private int VL_RECR;
	
	{
		this.strNumOs = "teste tabela";
		this.strEvnto = "teste tabela"; 
		this.strPart = "teste tabela";
		this.strTpColab = "teste tabela";
		this.strCargo = "teste tabela";
		this.strMatricCpf = "teste tabela";
		this.strPenalidade = "teste tabela";
		this.strMotivo = "teste tabela";
		this.strNumPv = "teste tabela";
		this.strNomPv = "teste tabela";
		this.strRegional = "teste tabela";
		this.strRede = "teste tabela";
		this.strDataEnvRh = "teste tabela";
		this.strDataEfetivRh = "teste tabela";
		this.strCartaoDebit = 3;
		this.strCartaoCredit = 4;
		this.strInternet = 4;
		this.strSuperLinha = 5;
		this.strTotal = "teste tabela";
		this.strDataAbertOS = "teste tabela";
		this.strDataDetecOS = "teste tabela";
		this.strDataEncerramentoOS = "teste tabela";
		this.strStatusReport = "teste tabela";
		this.strEmpresa = "teste tabela";
		this.strLinhaNegoNvl1 = "teste tabela";
		this.strLinhaNegoNvl2 = "teste tabela";
		this.strContaContab = "teste tabela";
		this.strNvl1 = "teste tabela";
		this.strNvl2 = "teste tabela";
		this.strNvl3 = "teste tabela";
		this.strFatorRisc = "teste tabela";
		this.strDataContab = "teste tabela";
		this.strValorEnvolv = 4;
		this.strValorEvit = 3;
		this.strValorRecup = 2;
		this.strValorPreju = 1;
		this.strBanco = "teste tabela";
		this.strQuant = 3;
		this.strCanal = "teste tabela";
		this.strSegm = 2;
		this.strPreju = 1;
		this.strPorcento = "teste tabela";
		
		//Refer�ncias
		this.strDT = "99/99";
		this.COUNT = 1;
		this.PREJUIZO = 1;
		this.TP_PESS_CLIE = 1;
		this.NM_SEGM = "teste banco";
		this.VL_RECR = 3;
	}
	
	//Construtor para alterar testes
	public hy_relosModel(String strDT, int TP_PESS_CLIE, int CD_CNAL, String NR_MES) {
		this.strDT = strDT;
		this.TP_PESS_CLIE = TP_PESS_CLIE;
		this.CD_CNAL = CD_CNAL;
		this.NR_MES = NR_MES;
	}
	
	public String getStrVazio() {
		return strVazio;
	}
	public void setStrVazio(String strVazio) {
		this.strVazio = strVazio;
	}
	public String getStrNumOs() {
		return strNumOs;
	}
	public void setStrNumOs(String strNumOs) {
		this.strNumOs = strNumOs;
	}
	public String getStrEvnto() {
		return strEvnto;
	}
	public void setStrEvnto(String strEvnto) {
		this.strEvnto = strEvnto;
	}
	public String getStrPart() {
		return strPart;
	}
	public void setStrPart(String strPart) {
		this.strPart = strPart;
	}
	public String getStrTpColab() {
		return strTpColab;
	}
	public void setStrTpColab(String strTpColab) {
		this.strTpColab = strTpColab;
	}
	public String getStrCargo() {
		return strCargo;
	}
	public void setStrCargo(String strCargo) {
		this.strCargo = strCargo;
	}
	public String getStrMatricCpf() {
		return strMatricCpf;
	}
	public void setStrMatricCpf(String strMatricCpf) {
		this.strMatricCpf = strMatricCpf;
	}
	public String getStrPenalidade() {
		return strPenalidade;
	}
	public void setStrPenalidade(String strPenalidade) {
		this.strPenalidade = strPenalidade;
	}
	public String getStrMotivo() {
		return strMotivo;
	}
	public void setStrMotivo(String strMotivo) {
		this.strMotivo = strMotivo;
	}
	public String getStrNumPv() {
		return strNumPv;
	}
	public void setStrNumPv(String strNumPv) {
		this.strNumPv = strNumPv;
	}
	public String getStrNomPv() {
		return strNomPv;
	}
	public void setStrNomPv(String strNomPv) {
		this.strNomPv = strNomPv;
	}
	public String getStrRegional() {
		return strRegional;
	}
	public void setStrRegional(String strRegional) {
		this.strRegional = strRegional;
	}
	public String getStrRede() {
		return strRede;
	}
	public void setStrRede(String strRede) {
		this.strRede = strRede;
	}
	public String getStrDataEnvRh() {
		return strDataEnvRh;
	}
	public void setStrDataEnvRh(String strDataEnvRh) {
		this.strDataEnvRh = strDataEnvRh;
	}
	public String getStrDataEfetivRh() {
		return strDataEfetivRh;
	}
	public void setStrDataEfetivRh(String strDataEfetivRh) {
		this.strDataEfetivRh = strDataEfetivRh;
	}
	public int getStrCartaoDebit() {
		return strCartaoDebit;
	}
	public void setStrCartaoDebit(int strCartaoDebit) {
		this.strCartaoDebit = strCartaoDebit;
	}
	public int getStrCartaoCredit() {
		return strCartaoCredit;
	}
	public void setStrCartaoCredit(int strCartaoCredit) { 
		this.strCartaoCredit = strCartaoCredit;
	}
	public int getStrInternet() {
		return strInternet;
	}
	public void setStrInternet(int strInternet) {
		this.strInternet = strInternet;
	}
	public int getStrSuperLinha() {
		return strSuperLinha;
	}
	public void setStrSuperLinha(int strSuperLinha) {
		this.strSuperLinha = strSuperLinha;
	}
	public String getStrTotal() {
		return strTotal;
	}
	public void setStrTotal(String strTotal) {
		this.strTotal = strTotal;
	}
	public String getStrDataAbertOS() {
		return strDataAbertOS;
	}
	public void setStrDataAbertOS(String strDataAbertOS) {
		this.strDataAbertOS = strDataAbertOS;
	}
	public String getStrDataDetecOS() {
		return strDataDetecOS;
	}
	public void setStrDataDetecOS(String strDataDetecOS) {
		this.strDataDetecOS = strDataDetecOS;
	}
	public String getStrDataEncerramentoOS() {
		return strDataEncerramentoOS;
	}
	public void setStrDataEncerramentoOS(String strDataEncerramentoOS) {
		this.strDataEncerramentoOS = strDataEncerramentoOS;
	}
	public String getStrStatusReport() {
		return strStatusReport;
	}
	public void setStrStatusReport(String strStatusReport) {
		this.strStatusReport = strStatusReport;
	}
	public String getStrEmpresa() {
		return strEmpresa;
	}
	public void setStrEmpresa(String strEmpresa) {
		this.strEmpresa = strEmpresa;
	}
	public String getStrLinhaNegoNvl1() {
		return strLinhaNegoNvl1;
	}
	public void setStrLinhaNegoNvl1(String strLinhaNegoNvl1) {
		this.strLinhaNegoNvl1 = strLinhaNegoNvl1;
	}
	public String getStrLinhaNegoNvl2() {
		return strLinhaNegoNvl2;
	}
	public void setStrLinhaNegoNvl2(String strLinhaNegoNvl2) {
		this.strLinhaNegoNvl2 = strLinhaNegoNvl2;
	}
	public String getStrContaContab() {
		return strContaContab;
	}
	public void setStrContaContab(String strContaContab) {
		this.strContaContab = strContaContab;
	}
	public String getStrNvl1() {
		return strNvl1;
	}
	public void setStrNvl1(String strNvl1) {
		this.strNvl1 = strNvl1;
	}
	public String getStrNvl2() {
		return strNvl2;
	}
	public void setStrNvl2(String strNvl2) {
		this.strNvl2 = strNvl2;
	}
	public String getStrNvl3() {
		return strNvl3;
	}
	public void setStrNvl3(String strNvl3) {
		this.strNvl3 = strNvl3;
	}
	public String getStrFatorRisc() {
		return strFatorRisc;
	}
	public void setStrFatorRisc(String strFatorRisc) {
		this.strFatorRisc = strFatorRisc;
	}
	public String getStrDataContab() {
		return strDataContab;
	}
	public void setStrDataContab(String strDataContab) {
		this.strDataContab = strDataContab;
	}
	public double getStrValorEnvolv() {
		return strValorEnvolv;
	}
	public void setStrValorEnvolv(double strValorEnvolv) {
		this.strValorEnvolv = strValorEnvolv;
	}
	public double getStrValorEvit() {
		return strValorEvit;
	}
	public void setStrValorEvit(double strValorEvit) {
		this.strValorEvit = strValorEvit;
	}
	public double getStrValorRecup() {
		return strValorRecup;
	}
	public void setStrValorRecup(double strValorRecup) {
		this.strValorRecup = strValorRecup;
	}
	public double getStrValorPreju() {
		return strValorPreju;
	}
	public void setStrValorPreju(double strValorPreju) {
		this.strValorPreju = strValorPreju;
	}
	public String getStrBanco() {
		return strBanco;
	}
	public void setStrBanco(String strBanco) {
		this.strBanco = strBanco;
	}
	public double getStrQuant() {
		return strQuant;
	}
	public void setStrQuant(double strQuant) {
		this.strQuant = strQuant;
	}
	public String getStrCanal() {
		return strCanal;
	}
	public void setStrCanal(String strCanal) {
		this.strCanal = strCanal;
	}
	public double getStrSegm() {
		return strSegm;
	}
	public void setStrSegm(double strSegm) {
		this.strSegm = strSegm;
	}
	public double getStrPreju() {
		return strPreju;
	}
	public void setStrPreju(double strPreju) {
		this.strPreju = strPreju;
	}
	public String getStrPorcento() {
		return strPorcento;
	}
	public void setStrPorcento(String strPorcento) {
		this.strPorcento = strPorcento;
	}
	public String getStrDT() {
		return strDT;
	}
	public void setStrDT(String strDT) {
		this.strDT = strDT;
	}
	public int getTP_PESS_CLIE() {
		return TP_PESS_CLIE;
	}
	public void setTP_PESS_CLIE(int tP_PESS_CLIE) {
		TP_PESS_CLIE = tP_PESS_CLIE;
	}
	public int getPREJUIZO() {
		return PREJUIZO;
	}
	public void setPREJUIZO(int pREJUIZO) {
		PREJUIZO = pREJUIZO;
	}
	public int getCOUNT() {
		return COUNT;
	}
	public void setCOUNT(int cOUNT) {
		COUNT = cOUNT;
	}
	public String getNM_SEGM() {
		return NM_SEGM;
	}
	public void setNM_SEGM(String nM_SEGM) {
		NM_SEGM = nM_SEGM;
	}

	public int getCD_CNAL() {
		return CD_CNAL;
	}

	public void setCD_CNAL(int cD_CNAL) {
		CD_CNAL = cD_CNAL;
	}

	public String getNR_MES() {
		return NR_MES;
	}

	public void setNR_MES(String nR_MES) {
		NR_MES = nR_MES;
	}

	public int getVL_RECR() {
		return VL_RECR;
	}

	public void setVL_RECR(int vL_RECR) {
		VL_RECR = vL_RECR;
	}

}
