package com.altec.bsbr.app.hyb.web.util;

import java.util.ArrayList;
import java.util.List;

import com.altec.bsbr.app.hyb.dto.OSCanalOrigem;

/*****SIMULACAO BACK******/
public class ClsCadOSCanalOrig {
	public ClsCadOSCanalOrig(){}
	
	public List<OSCanalOrigem> consultarOSCanalOrigem(String strNrSeqOs) {
		OSCanalOrigem canalOrigEvento = new OSCanalOrigem();
		canalOrigEvento.setNOME("Nome Test");
		canalOrigEvento.setCODIGO("123");
		canalOrigEvento.setStrErro("");
		canalOrigEvento.setTP_OFCI("C");
		canalOrigEvento.setTP_MONI_LYNX("I");
		canalOrigEvento.setIN_MINSTR("E");
		
		canalOrigEvento.setIN_OMINTE(true);
		canalOrigEvento.setIN_OMINTR(false);
        canalOrigEvento.setIN_OMINSTR(true);
        canalOrigEvento.setIN_OMTELE(false);
        
        canalOrigEvento.setIN_OMMALT(true);
        canalOrigEvento.setIN_OMDENC(true);
        canalOrigEvento.setIN_OMDEPT(true);
        canalOrigEvento.setIN_OMAGEN(false);
        
        canalOrigEvento.setCD_CNAL_ORIG(123);
        
        
		List<OSCanalOrigem> canais = new ArrayList<OSCanalOrigem>();
		canais.add(canalOrigEvento);
		
		 return canais;
	}
	
	public List<OSCanalOrigem> consultarCnAlOrigEvento(String cd) {
		OSCanalOrigem canalOrigEvento = new OSCanalOrigem();
		canalOrigEvento.setNOME("Nome Test");
		canalOrigEvento.setCODIGO("123");
		canalOrigEvento.setStrErro("");
		canalOrigEvento.setTP_OFCI("C");
		canalOrigEvento.setTP_MONI_LYNX("I");
		canalOrigEvento.setIN_MINSTR("E");
		
		canalOrigEvento.setIN_OMINTE(true);
		canalOrigEvento.setIN_OMINTR(false);
        canalOrigEvento.setIN_OMINSTR(true);
        canalOrigEvento.setIN_OMTELE(false);
        
        canalOrigEvento.setIN_OMMALT(true);
        canalOrigEvento.setIN_OMDENC(true);
        canalOrigEvento.setIN_OMDEPT(true);
        canalOrigEvento.setIN_OMAGEN(false);
        
        canalOrigEvento.setCD_CNAL_ORIG(123);
        
        
		List<OSCanalOrigem> canais = new ArrayList<OSCanalOrigem>();
		canais.add(canalOrigEvento);
		return canais;
	}
	
	public String alterarOSCanalOrigem(String strNrSeqOs,int intCboOrigemOS,String strRdoOficio, 
			   String strRdoLynx,String strRdoMonitInst,int intChkInte,int intChkIntra, 
			   int intChkEmail, int intChkTele, int intChkMalote, int intChkDenuncia, 
			   int intChkDepartamento,int intChkAgencia) {
		return "alterado";
	}
}
