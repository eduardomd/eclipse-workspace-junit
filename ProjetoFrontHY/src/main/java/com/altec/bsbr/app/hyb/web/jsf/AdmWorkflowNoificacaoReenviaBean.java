package com.altec.bsbr.app.hyb.web.jsf;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.ObjRsNotiDest;
import com.altec.bsbr.app.hyb.dto.ObjRsNotiEnv;
import com.altec.bsbr.app.hyb.dto.RsNotiAnexo;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmNotificacao.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmNotificacao.XHYAdmNotificacaoEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.ibm.icu.text.SimpleDateFormat;

@Component("AdmWorkflowNoificacaoReenviaBean")
@Scope("request")
public class AdmWorkflowNoificacaoReenviaBean extends BasicBBean {
	
	private static final long serialVersionUID = 1L;
	private List<ObjRsNotiEnv> objRsNotiEnvs;
	private List<ObjRsNotiDest> objRsNotiDests; 
	
	private Object strNrSeqNoti = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqNoti");
	private Object strMsg = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strMsg");
	private Object strNmArqAnex = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNmArqAnex");
	
	private String strDest = "";

	@Autowired
	private XHYAdmNotificacaoEndPoint admnoti;
	
	@PostConstruct
	public void init() {
		objRsNotiEnvs = new ArrayList<ObjRsNotiEnv>();
		objRsNotiDests = new ArrayList<ObjRsNotiDest>();
		String retornoNotificacaoManual = "";
		String retornoDestinatario = "";
		JSONObject jsonRetorno;
		JSONArray pcursor;
		
		try {
//			retornoNotificacaoManual = admnoti.consultarNotificacaoManual("", "", Long.valueOf(strNrSeqNoti.toString()));
			String dtInicio = "1900-01-01 00:00:00";
			String dtFinal = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
			
			retornoNotificacaoManual = admnoti.consultarNotificacaoManual(dtInicio, dtFinal, Long.valueOf(strNrSeqNoti.toString()));
			retornoDestinatario = admnoti.consultarDestinatario(Long.valueOf(strNrSeqNoti.toString()));
			
	    } catch (WebServiceException e) {
	    	// TODO Auto-generated catch block
	    	e.printStackTrace();
        }
		
		try {
			/* Notificacao Manual */
	        jsonRetorno = new JSONObject(retornoNotificacaoManual);
	        pcursor = jsonRetorno.getJSONArray("PCURSOR");
	                    
	        for(int i = 0; i < pcursor.length(); i++) {
	        	JSONObject curr = pcursor.getJSONObject(i);
	        	objRsNotiEnvs.add(new ObjRsNotiEnv(curr.getString("NR_SEQU_ORDE_SERV"),
	        							           curr.getString("NM_NOTI"),
	        							           curr.getString("DATA_RET"),
	        							           curr.getString("TX_MESG")));
	        	
	        }
	        
			/* Destinatario */
	        jsonRetorno = new JSONObject(retornoDestinatario);
	        pcursor = jsonRetorno.getJSONArray("PCURSOR");
	                    
	        for(int i = 0; i < pcursor.length(); i++) {
	        	JSONObject curr = pcursor.getJSONObject(i);
	        	strDest += curr.getString("NM_EMAIL_DEST") + ";";
	        }
	        
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public List<ObjRsNotiEnv> getObjRsNotiEnvs(){
		//TODO: Backend para consultar Notificacao Enviada
//		objRsNotiEnvs = new ArrayList<ObjRsNotiEnv>();
//		objRsNotiEnvs.add(new ObjRsNotiEnv("3210", "1510321", "10/09/2018", "tX_MESG"));
//		objRsNotiEnvs.add(new ObjRsNotiEnv("9871", "8980014", "10/12/2018", "tX_MESG2"));
		return objRsNotiEnvs;
	}
	
	public List<ObjRsNotiDest> getObjRsNotiDests(){
		//TODO: Backend para consultar os destinarios
//		objRsNotiDests = new ArrayList<ObjRsNotiDest>();
//		objRsNotiDests.add(new ObjRsNotiDest("email@email.com"));
//		objRsNotiDests.add(new ObjRsNotiDest("segundoemail@email.com"));
		return objRsNotiDests;
	}

	public String getStrDest() {
		return strDest;
	}

	public void setStrDest(String strDest) {
		this.strDest = strDest;
	}
	
	public String getStrNrSeqNoti() {
		return this.strNrSeqNoti.toString();
	}
	
}
