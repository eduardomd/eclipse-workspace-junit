package com.altec.bsbr.app.hyb.web.jsf;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.fw.web.jsf.BasicBBean;

@Component("AdmGerManutLynx")
@Scope("session")
public class AdmGerManutLynxBean extends BasicBBean {
	
	
	public String getMessage() {
		return "Carga do Lynx já foi executada na data atual.";
	}

	public void carregaLynx() {
		RequestContext context = RequestContext.getCurrentInstance(); 
    	context.execute("PF('carregaLynx').show();");
	}
}
