package com.altec.bsbr.app.hyb.web.jsf;

import java.io.Serializable;
import java.util.Map;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.altec.bsbr.app.hyb.dto.CadosEquipatribLanchr;


@Component("EquipatribLanchr")
@Scope("session")
public class CadosEquipatribLanchrBean extends BasicBBean {

	private static final long serialVersionUID = 1L;
	private CadosEquipatribLanchr cadosEquipatribLanchr;
	private String urlIframe;
	private String strNome;

	public CadosEquipatribLanchrBean() {
		this.cadosEquipatribLanchr = new CadosEquipatribLanchr();

		FacesContext fc = FacesContext.getCurrentInstance();
		@SuppressWarnings("unchecked")
		Map<String, String> params = fc.getExternalContext().getRequestParameterMap();

		strNome = params.get("strNome");
		this.cadosEquipatribLanchr.setStrNrSeqOs(params.get("strNrSeqOs"));
		this.cadosEquipatribLanchr.setStrCpf((params.get("strCpf")));;

		urlIframe = "hyb_cados_equipatrib_lanchr_iframe.xhtml?strNrSeqOs=" + this.cadosEquipatribLanchr.getStrNrSeqOs()
				+ "&strCpf=" + this.cadosEquipatribLanchr.getStrCpf() + "&strNome=" + this.strNome;

	}

	public CadosEquipatribLanchr getCadosEquipatribLanchr() {
		return cadosEquipatribLanchr;
	}

	public void setCadosEquipatribLanchr(CadosEquipatribLanchr cadosEquipatribLanchr) {
		this.cadosEquipatribLanchr = cadosEquipatribLanchr;
	}

	public String getUrlIframe() {
		return urlIframe;
	}

	public void setUrlIframe(String urlIframe) {
		this.urlIframe = urlIframe;
	}

	public String getStrNome() {
		return strNome;
	}

	public void setStrNome(String strNome) {
		this.strNome = strNome;
	}

}
