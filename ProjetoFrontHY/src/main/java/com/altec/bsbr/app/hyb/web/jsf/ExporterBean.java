package com.altec.bsbr.app.hyb.web.jsf;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.fw.web.jsf.BasicBBean;

@Component("exporterBean")
@Scope("session")
public class ExporterBean extends BasicBBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Boolean customExporter;
	
	public ExporterBean() {
		this.customExporter = false;
	}

	public Boolean getCustomExporter() {
		return customExporter;
	}

	public void setCustomExporter(Boolean customExporter) {
		this.customExporter = customExporter;
	}
}
