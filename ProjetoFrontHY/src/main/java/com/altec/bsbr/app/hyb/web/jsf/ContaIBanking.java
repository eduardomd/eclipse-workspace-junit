package com.altec.bsbr.app.hyb.web.jsf;

public class ContaIBanking {
	private String strError;
	private int recordCount;
	private String[][] records;

	public String[][] getRecords() {
		return records;
	}

	public void setRecords(String[][] records) {
		this.records = records;
	}

	public int getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(int recordCount) {
		this.recordCount = recordCount;
	}

	public String getStrError() {
		return strError;
	}

	public void setStrError(String strError) {
		this.strError = strError;
	}

}
