package com.altec.bsbr.app.hyb.dto;

public class CadOsDataModel {
	
	public String strNrSeqOs;
	public String strDtFixa;
	public String strDtIni;
	public String strDtFin;
	public String strTpData;
	public String strDtDeteccao;
	public String strTitulo;
	public String strAction;
	
	public CadOsDataModel()
	{
		
	}
	
	public CadOsDataModel(String strnrSeqOs, String strdtFixa, String strdtIni, String strdtFin,  String strtpData, String strdtDeteccao, String strtitulo, String straction )
	{
		super();
		this.strNrSeqOs = strnrSeqOs;
		this.strDtFixa = strdtFixa;
		this.strDtIni = strdtIni;
		this.strDtFin = strdtFin;
		this.strTpData = strtpData;
		this.strDtDeteccao = strdtDeteccao;
		this.strTitulo = strtitulo;
		this.strAction = straction;
	}
	
	
	public String getStrNrSeqOs() {
		return strNrSeqOs;
	}
	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}
	public String getStrDtFixa() {
		return strDtFixa;
	}
	public void setStrDtFixa(String strDtFixa) {
		this.strDtFixa = strDtFixa;
	}
	public String getStrDtIni() {
		return strDtIni;
	}
	public void setStrDtIni(String strDtIni) {
		this.strDtIni = strDtIni;
	}
	public String getStrDtFin() {
		return strDtFin;
	}
	public void setStrDtFin(String strDtFin) {
		this.strDtFin = strDtFin;
	}
	public String getStrTpData() {
		return strTpData;
	}
	public void setStrTpData(String strTpData) {
		this.strTpData = strTpData;
	}
	public String getStrDtDeteccao() {
		return strDtDeteccao;
	}
	public void setStrDtDeteccao(String strDtDeteccao) {
		this.strDtDeteccao = strDtDeteccao;
	}
	public String getStrTitulo() {
		return strTitulo;
	}
	public void setStrTitulo(String strTitulo) {
		this.strTitulo = strTitulo;
	}
	public String getStrAction() {
		return strAction;
	}
	public void setStrAction(String strAction) {
		this.strAction = strAction;
	}
	
}
