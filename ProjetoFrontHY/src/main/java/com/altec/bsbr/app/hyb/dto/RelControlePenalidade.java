package com.altec.bsbr.app.hyb.dto;

public class RelControlePenalidade {
	
	private String os;
	private String evento;
	private String participacao;
	private String tipoColaborador;
	private String cargo;
	private String matricula;
	private String penalidade;
	private String motivo;
	private String numPv;
	private String nmPv;
	private String nmRegi;
	private String nmRede;
	private String dtEnvioRH;
	private String dtRetnJuri;
	
	public RelControlePenalidade() {
		
	}
	
	public RelControlePenalidade(String os,
								String evento,
								String participacao,
								String tipoColaborador,
								String cargo,
								String matricula,
								String penalidade,
								String motivo,
								String numPv,
								String nmPv,
								String nmRegi,
								String nmRede, 
								String dtEnvioRH,
								String dtRetnJuri) {
		this.os = os;
		this.evento = evento;
		this.participacao = participacao;
		this.tipoColaborador = tipoColaborador;
		this.cargo = cargo;
		this.matricula = matricula;
		this.penalidade = penalidade;
		this.motivo = motivo;
		this.numPv = numPv;
		this.nmPv = nmPv;
		this.nmRegi = nmRegi;
		this.nmRede = nmRede;
		this.dtEnvioRH = dtEnvioRH;
		this.dtRetnJuri = dtRetnJuri;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getEvento() {
		return evento;
	}

	public void setEvento(String evento) {
		this.evento = evento;
	}

	public String getParticipacao() {
		return participacao;
	}

	public void setParticipacao(String participacao) {
		this.participacao = participacao;
	}

	public String getTipoColaborador() {
		return tipoColaborador;
	}

	public void setTipoColaborador(String tipoColaborador) {
		this.tipoColaborador = tipoColaborador;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getPenalidade() {
		return penalidade;
	}

	public void setPenalidade(String penalidade) {
		this.penalidade = penalidade;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public String getNumPv() {
		return numPv;
	}

	public void setNumPv(String numPv) {
		this.numPv = numPv;
	}

	public String getNmPv() {
		return nmPv;
	}

	public void setNmPv(String nmPv) {
		this.nmPv = nmPv;
	}

	public String getNmRegi() {
		return nmRegi;
	}

	public void setNmRegi(String nmRegi) {
		this.nmRegi = nmRegi;
	}

	public String getNmRede() {
		return nmRede;
	}

	public void setNmRede(String nmRede) {
		this.nmRede = nmRede;
	}

	public String getDtEnvioRH() {
		return dtEnvioRH;
	}

	public void setDtEnvioRH(String dtEnvioRH) {
		this.dtEnvioRH = dtEnvioRH;
	}

	public String getDtRetnJuri() {
		return dtRetnJuri;
	}

	public void setDtRetnJuri(String dtRetnJuri) {
		this.dtRetnJuri = dtRetnJuri;
	}
		
}
