package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.objRsAdmLog;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmLog.XHYAdmLogEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;


@Component("hyAdmLogBean")
@Scope("session")
public class AdmLogBean extends BasicBBean {

	private static final long serialVersionUID = 1L;
	
	private Date dteInicio;
	private Date dteFim;
	private String strAction = "";
	
	private String strCodUsua;
	private String strCboAcaoUsua;
	
	private List<objRsAdmLog> objRsAcaoUsua;
	private List<objRsAdmLog> objRsAdmLog;
	private List<String> list;
	
	@Autowired
    private XHYAdmLogEndPoint admlog;
	
	@PostConstruct
    public void init() {
		
		try {
			String usuaRetorno = admlog.consultarAcaoUsua();
			
			JSONObject consultarAcaoUsua = new JSONObject(usuaRetorno);
            JSONArray pcursor = consultarAcaoUsua.getJSONArray("PCURSOR");
            
            objRsAcaoUsua = new ArrayList<objRsAdmLog>();
            
            for (int i = 0; i < pcursor.length(); i++) {
                   JSONObject item = pcursor.getJSONObject(i);
                   objRsAcaoUsua.add(new objRsAdmLog(item.get("CODIGO").toString(), item.get("NOME").toString()));
            }
			
		} catch (Exception e) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
	             } catch (IOException e1) {
	                   // TODO Auto-generated catch block
	                   e1.printStackTrace();
	             }
	      }
	}
	
	 public void pesquisar() {
    	this.setStrAction("P");
    	
    	if(this.getStrAction().equals("P")) {
            try {
            	System.out.println("DateI: " + dteInicio);
            	System.out.println("Date F: " + dteFim);
        		SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        		String dateInicio;
        		String dateFim;
        		
        		if (dteInicio == null) {
        			dateInicio = "";
        		} else {
        			dateInicio = newFormat.format(dteInicio);
        		}
        		
        		if (dteFim == null) {
        			dateFim = "";
        		} else {
        			dateFim = newFormat.format(dteFim);
        		}
        		
        		if (strCodUsua == null) {
        			strCodUsua = "";
        		}
        		
        		if (strCboAcaoUsua == null) {
        			strCboAcaoUsua = "";
        		}
        		
				String retorno = admlog.consultarAcaoLog(dateInicio, dateFim, strCodUsua, strCboAcaoUsua);
                
                JSONObject consultarAcaoLog = new JSONObject(retorno);
                JSONArray pcursor = consultarAcaoLog.getJSONArray("PCURSOR");
                objRsAdmLog = new ArrayList<objRsAdmLog>();
                
                if (pcursor.length() > 0) {
	                for (int i = 0; i < pcursor.length(); i++) {
	                       JSONObject item = pcursor.getJSONObject(i);
	                       String dtAcao = item.isNull("DT_ACAO") ? "" : item.get("DT_ACAO").toString();
	                       String dtHora = item.isNull("DT_HORA") ? "" : item.get("DT_HORA").toString();
	                       String nrAcao = item.isNull("NM_ACAO") ? "" : item.get("NM_ACAO").toString();
	                       String codUsu = item.isNull("COD_USUARIO") ? "" : item.get("COD_USUARIO").toString();
	                       String nrOs = item.isNull("NR_OS") ? "" : item.get("NR_OS").toString();
	                       
	                       objRsAdmLog.add(new objRsAdmLog(dtAcao, dtHora, codUsu, nrAcao, nrOs));
	                }
                }
				
			} catch (Exception e) {
                try {
                    FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
		             } catch (IOException e1) {
		                   e1.printStackTrace();
		             }
		      }


		}
    	
    }
	 
	public List<String> getList() {
		return list;
	}

	public void setList(List<String> list) {
		this.list = list;
	}

	public XHYAdmLogEndPoint getAdmlog() {
		return admlog;
	}

	public void setAdmlog(XHYAdmLogEndPoint admlog) {
		this.admlog = admlog;
	}

	public Date getDteInicio() {
		return dteInicio;
	}

	public void setDteInicio(Date dteInicio) {
		this.dteInicio = dteInicio;
	}

	public Date getDteFim() {
		return dteFim;
	}

	public void setDteFim(Date dteFim) {
		this.dteFim = dteFim;
	}

	public String getStrCodUsua() {
		return strCodUsua;
	}

	public void setStrCodUsua(String strCodUsua) {
		this.strCodUsua = strCodUsua;
	}

	public String getStrCboAcaoUsua() {
		return strCboAcaoUsua;
	}

	public void setStrCboAcaoUsua(String strCboAcaoUsua) {
		this.strCboAcaoUsua = strCboAcaoUsua;
	}

	public String getStrAction() {
		return strAction;
	}

	public void setStrAction(String strAction) {
		this.strAction = strAction;
	}

	public List<objRsAdmLog> getObjRsAcaoUsua() {
		return objRsAcaoUsua;
	}

	public void setObjRsAcaoUsua(List<objRsAdmLog> objRsAcaoUsua) {
		this.objRsAcaoUsua = objRsAcaoUsua;
	}

	public List<objRsAdmLog> getObjRsAdmLog() {
		return objRsAdmLog;
	}

	public void setObjRsAdmLog(List<objRsAdmLog> objRsAdmLog) {
		this.objRsAdmLog = objRsAdmLog;
	}
	
}

