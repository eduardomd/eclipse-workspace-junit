package com.altec.bsbr.app.hyb.web.jsf;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.altec.bsbr.app.hyb.dto.ObjRsOs;
@ManagedBean(name = "DadCont")
@RequestScoped

public class HybCadOsDadosContabeisEncerrarModel {
	private ObjRsOs objRsOs = new ObjRsOs();
    public String objDadosContabeisEnc;
    public String objRsOutCadOS;
    public String strNrSequOS;
    public String strDtLancContab; 
    public String strCntaContab;
    public String strNmCanalOrig;
    public String strCodEvent;
    public String strEmpr;
    public String strAreaComp;
    public String strCatgContab1;
    public String strCatgContab2;
    public String strCatgContab3;
    public String strLinhNego1;
    public String strLinhNego2;
    public String strFatRisc;
    public String strTpCaptura;
    public String strCtrContbOUR;
    public String strCtrOrigOUR;
    public String strPTVD;
    public String strCausaEvento;
    public String strTpMani;
    public String strNrSequOSRela;
	public String strErro;
	public String strTitulo;
	public String objRsOsCadOS;
	public String objAcoesOsCadOS;
	public String strAcao;
	public String NuCntC;
	
	public HybCadOsDadosContabeisEncerrarModel() {};
	
	
	
	public ObjRsOs getObjRsOs() {
		return objRsOs;
	}



	public void setObjRsOs(ObjRsOs objRsOs) {
		this.objRsOs = objRsOs;
	}



	public String getNuCntC() {
		return NuCntC;
	}



	public void setNuCntC(String nuCntC) {
		NuCntC = nuCntC;
	}



	public String getObjDadosContabeisEnc() {
		return objDadosContabeisEnc;
	}
	public void setObjDadosContabeisEnc(String objDadosContabeisEnc) {
		this.objDadosContabeisEnc = objDadosContabeisEnc;
	}
	public String getObjRsOutCadOS() {
		return objRsOutCadOS;
	}
	public void setObjRsOutCadOS(String objRsOutCadOS) {
		this.objRsOutCadOS = objRsOutCadOS;
	}
	public String getStrNrSequOS() {
		return strNrSequOS;
	}
	public void setStrNrSequOS(String strNrSequOS) {
		this.strNrSequOS = strNrSequOS;
	}
	public String getStrDtLancContab() {
		return strDtLancContab;
	}
	public void setStrDtLancContab(String strDtLancContab) {
		this.strDtLancContab = strDtLancContab;
	}
	public String getStrCntaContab() {
		return strCntaContab;
	}
	public void setStrCntaContab(String strCntaContab) {
		this.strCntaContab = strCntaContab;
	}
	public String getStrNmCanalOrig() {
		return strNmCanalOrig;
	}
	public void setStrNmCanalOrig(String strNmCanalOrig) {
		this.strNmCanalOrig = strNmCanalOrig;
	}
	public String getStrCodEvent() {
		return strCodEvent;
	}
	public void setStrCodEvent(String strCodEvent) {
		this.strCodEvent = strCodEvent;
	}
	public String getStrEmpr() {
		return strEmpr;
	}
	public void setStrEmpr(String strEmpr) {
		this.strEmpr = strEmpr;
	}
	public String getStrAreaComp() {
		return strAreaComp;
	}
	public void setStrAreaComp(String strAreaComp) {
		this.strAreaComp = strAreaComp;
	}
	public String getStrCatgContab1() {
		return strCatgContab1;
	}
	public void setStrCatgContab1(String strCatgContab1) {
		this.strCatgContab1 = strCatgContab1;
	}
	public String getStrCatgContab2() {
		return strCatgContab2;
	}
	public void setStrCatgContab2(String strCatgContab2) {
		this.strCatgContab2 = strCatgContab2;
	}
	public String getStrCatgContab3() {
		return strCatgContab3;
	}
	public void setStrCatgContab3(String strCatgContab3) {
		this.strCatgContab3 = strCatgContab3;
	}
	public String getStrLinhNego1() {
		return strLinhNego1;
	}
	public void setStrLinhNego1(String strLinhNego1) {
		this.strLinhNego1 = strLinhNego1;
	}
	public String getStrLinhNego2() {
		return strLinhNego2;
	}
	public void setStrLinhNego2(String strLinhNego2) {
		this.strLinhNego2 = strLinhNego2;
	}
	public String getStrFatRisc() {
		return strFatRisc;
	}
	public void setStrFatRisc(String strFatRisc) {
		this.strFatRisc = strFatRisc;
	}
	public String getStrTpCaptura() {
		return strTpCaptura;
	}
	public void setStrTpCaptura(String strTpCaptura) {
		this.strTpCaptura = strTpCaptura;
	}
	public String getStrCtrContbOUR() {
		return strCtrContbOUR;
	}
	public void setStrCtrContbOUR(String strCtrContbOUR) {
		this.strCtrContbOUR = strCtrContbOUR;
	}
	public String getStrCtrOrigOUR() {
		return strCtrOrigOUR;
	}
	public void setStrCtrOrigOUR(String strCtrOrigOUR) {
		this.strCtrOrigOUR = strCtrOrigOUR;
	}
	public String getStrPTVD() {
		return strPTVD;
	}
	public void setStrPTVD(String strPTVD) {
		this.strPTVD = strPTVD;
	}
	public String getStrCausaEvento() {
		return strCausaEvento;
	}
	public void setStrCausaEvento(String strCausaEvento) {
		this.strCausaEvento = strCausaEvento;
	}
	public String getStrTpMani() {
		return strTpMani;
	}
	public void setStrTpMani(String strTpMani) {
		this.strTpMani = strTpMani;
	}
	public String getStrNrSequOSRela() {
		return strNrSequOSRela;
	}
	public void setStrNrSequOSRela(String strNrSequOSRela) {
		this.strNrSequOSRela = strNrSequOSRela;
	}
	public String getStrErro() {
		return strErro;
	}
	public void setStrErro(String strErro) {
		this.strErro = strErro;
	}
	public String getStrTitulo() {
		return strTitulo;
	}
	public void setStrTitulo(String strTitulo) {
		this.strTitulo = strTitulo;
	}
	public String getObjRsOsCadOS() {
		return objRsOsCadOS;
	}
	public void setObjRsOsCadOS(String objRsOsCadOS) {
		this.objRsOsCadOS = objRsOsCadOS;
	}
	public String getObjAcoesOsCadOS() {
		return objAcoesOsCadOS;
	}
	public void setObjAcoesOsCadOS(String objAcoesOsCadOS) {
		this.objAcoesOsCadOS = objAcoesOsCadOS;
	}
	public String getStrAcao() {
		return strAcao;
	}
	public void setStrAcao(String strAcao) {
		this.strAcao = strAcao;
	}

	
}
