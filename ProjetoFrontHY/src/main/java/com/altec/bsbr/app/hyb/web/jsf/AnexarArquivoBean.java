
package com.altec.bsbr.app.hyb.web.jsf;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;


@ManagedBean(name="AnexarArquivoBean")
@ViewScoped
public class AnexarArquivoBean implements Serializable {

	private static final long serialVersionUID = 1L;
    
    private Object strTpAnexo = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strTpAnexo");

	public Object getStrTpAnexo() {
		return strTpAnexo;
	}

	public void setStrTpAnexo(Object strTpAnexo) {
		this.strTpAnexo = strTpAnexo;
	}
}