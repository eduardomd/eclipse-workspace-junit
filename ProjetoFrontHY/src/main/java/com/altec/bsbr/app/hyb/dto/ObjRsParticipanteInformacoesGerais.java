package com.altec.bsbr.app.hyb.dto;

public class ObjRsParticipanteInformacoesGerais {
	
	private String codigo;
	private String posicao;
	private String nome;
	private String tipoParticip;
	private String cpfCnpj;
	private String descTipo;
	private String cdPenalidade;
	private String cdMotivo;
	
	
	public ObjRsParticipanteInformacoesGerais() {
		
	}
	
	public ObjRsParticipanteInformacoesGerais(String codigo, String posicao, String nome, String tipoParticip,
String cpfCnpj, String descTipo, String cdPenalidade, String cdMotivo) {
		this.codigo = codigo;
		this.posicao = posicao;
		this.nome = nome;
		this.tipoParticip = tipoParticip;
		this.cpfCnpj = cpfCnpj;
		this.descTipo = descTipo;
		this.cdPenalidade = cdPenalidade;
		this.cdMotivo = cdMotivo;
	}
	
	public String getCodigo() {
		return codigo;
	}
	
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	public String getPosicao() {
		return posicao;
	}
	
	public void setPosicao(String posicao) {
		this.posicao = posicao;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getTipoParticip() {
		return tipoParticip;
	}
	
	public void setTipoParticip(String tipoParticip) {
		this.tipoParticip = tipoParticip;
	}
	
	public String getCpfCnpj() {
		return cpfCnpj;
	}
	
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}
	
	public String getDescTipo() {
		return descTipo;
	}
	
	public void setDescTipo(String descTipo) {
		this.descTipo = descTipo;
	}
	
	public String getCdPenalidade() {
		return cdPenalidade;
	}
	
	public void setCdPenalidade(String cdPenalidade) {
		this.cdPenalidade = cdPenalidade;
	}
	
	public String getCdMotivo() {
		return cdMotivo;
	}
	
	public void setCdMotivo(String cdMotivo) {
		this.cdMotivo = cdMotivo;
	}
	
}
