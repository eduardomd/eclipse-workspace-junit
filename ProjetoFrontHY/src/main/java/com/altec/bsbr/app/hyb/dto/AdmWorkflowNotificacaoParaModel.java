package com.altec.bsbr.app.hyb.dto;

public class AdmWorkflowNotificacaoParaModel {

	private String NR_NOTI;
	private String NM_NOTI;
	private String QT_HORA_PRAZ_RELZ;
	private String QT_HORA_PERI_REEV;
	private String TP_NOTI = "M";
	private String IN_REEV;
		
	private boolean inReevChk;
	private boolean checkTpNoti;
	
	public AdmWorkflowNotificacaoParaModel() {
		
	}
	
	public String getNR_NOTI() {
		return NR_NOTI;
	}

	public void setNR_NOTI(String nR_NOTI) {
		NR_NOTI = nR_NOTI;
	}

	public String getNM_NOTI() {
		return NM_NOTI;
	}

	public void setNM_NOTI(String nM_NOTI) {
		NM_NOTI = nM_NOTI;
	}

	public String getQT_HORA_PRAZ_RELZ() {
		return QT_HORA_PRAZ_RELZ;
	}

	public void setQT_HORA_PRAZ_RELZ(String qT_HORA_PRAZ_RELZ) {
		QT_HORA_PRAZ_RELZ = qT_HORA_PRAZ_RELZ;
	}

	public String getQT_HORA_PERI_REEV() {
		return QT_HORA_PERI_REEV;
	}

	public void setQT_HORA_PERI_REEV(String qT_HORA_PERI_REEV) {
		QT_HORA_PERI_REEV = qT_HORA_PERI_REEV;
	}

	public String getTP_NOTI() {
		return TP_NOTI;
	}

	public void setTP_NOTI(String tP_NOTI) {
		TP_NOTI = tP_NOTI;
	}

	public String getIN_REEV() {
		return IN_REEV;
	}

	public void setIN_REEV(String iN_REEV) {
		IN_REEV = iN_REEV;
	}

	public boolean isCheckTpNoti() {
		return this.TP_NOTI.equals("M");
	}

	public void setCheckTpNoti(boolean checkTpNoti) {
		this.checkTpNoti = checkTpNoti;
	}

	public boolean isInReevChk() {
		//return this.IN_REEV.equals("1");
		return this.inReevChk;
	}

	public void setInReevChk(boolean inReevChk) {
		this.inReevChk = inReevChk;
	}	
   
}