package com.altec.bsbr.app.hyb.dto;

public class AreaCompModel {

	private String nomeAreaComp;
	private String codigoAreaComp;

	public AreaCompModel(String codigoAreaComp, String nomeAreaComp) {		
		this.nomeAreaComp = nomeAreaComp;
		this.codigoAreaComp = codigoAreaComp;
	}

	public String getNomeAreaComp() {
		return nomeAreaComp;
	}

	public void setNomeAreaComp(String nomeAreaComp) {
		this.nomeAreaComp = nomeAreaComp;
	}

	public String getCodigoAreaComp() {
		return codigoAreaComp;
	}

	public void setCodigoAreaComp(String codigoAreaComp) {
		this.codigoAreaComp = codigoAreaComp;
	}
}
