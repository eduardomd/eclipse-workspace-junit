package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.poi.ss.format.CellTextFormatter;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellUtil;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.RelOSCustoTotalOSModel;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.ibm.icu.util.Calendar;
import com.altec.bsbr.app.jab.hyb.webclient.XHYRelatorios.XHYRelatoriosEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYRelatorios.WebServiceException;

@Component("relOSCustoTotalOSBean")
@Scope("session")
public class RelOSCustoTotalOSBean extends BasicBBean {

	private static final long serialVersionUID = 1L;

	public List<RelOSCustoTotalOSModel> objRsRelat = new ArrayList<RelOSCustoTotalOSModel>();

	@Autowired
	private XHYRelatoriosEndPoint objRelat;

	private String strDtIni;
	private String strDtFim;
	private boolean linhaTotal;

	private Double valorCustoOsTotal;

	FacesContext fc = FacesContext.getCurrentInstance();
	ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
	JSONArray pcursorObjRs = new JSONArray();

	@PostConstruct
	public void init() throws IOException {

		Map<String, String> params = fc.getExternalContext().getRequestParameterMap();

		this.strDtIni = params.get("pDtIni");
		this.strDtFim = params.get("pDtFim");

		try {
			String objRs = objRelat.fnRelCustoTotalOs(this.strDtIni, this.strDtFim);
			JSONObject objResTemp = new JSONObject(objRs);
			pcursorObjRs = objResTemp.getJSONArray("PCURSOR");
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		for (int i = 0; i < pcursorObjRs.length(); i++) {
			JSONObject temp = pcursorObjRs.getJSONObject(i);

			objRsRelat.add(new RelOSCustoTotalOSModel(
					temp.isNull("ROWNUM") ? "" : temp.get("ROWNUM").toString(),
					temp.isNull("NR_SEQU_ORDE_SERV") ? "" : temp.get("NR_SEQU_ORDE_SERV").toString(), 
					temp.isNull("NM_SITU_ORDE_SERV") ? "" : temp.get("NM_SITU_ORDE_SERV").toString(),
					temp.isNull("DT_ABER_ORDE_SERV") ? "" : temp.get("DT_ABER_ORDE_SERV").toString(), 
					temp.isNull("DT_ENCERRAMENTO") ? "" : temp.get("DT_ENCERRAMENTO").toString(),
					temp.isNull("VL_ENVO") ? "" : temp.get("VL_ENVO").toString(), 
					temp.isNull("VL_PERD_EFET") ? "" : temp.get("VL_PERD_EFET").toString(),
					temp.isNull("VL_CUST_ORDE_DE_SERV") ? "" : temp.get("VL_CUST_ORDE_DE_SERV").toString(), 
					temp.isNull("VL_ENVO") ? "" : formataNumero(temp.get("VL_ENVO").toString()),
					temp.isNull("VL_PERD_EFET") ? "" : formataNumero(temp.get("VL_PERD_EFET").toString()),
					temp.isNull("VL_CUST_ORDE_DE_SERV ") ? "" : formataNumero(temp.get("VL_CUST_ORDE_DE_SERV").toString())));
		}

		/*if (objRsRelat.size() == 0) {
			for (int i = 0; i < 10; i++) {
				objRsRelat.add(new RelOSCustoTotalOSModel("0", "2008000001", "NM_SITU_ORDE_SERV", "19/03/2019",
						"20/03/2019", "20000", "15000", "2000", formataNumero("20000"), formataNumero("15000"),
						formataNumero("2000")));
			}
		}*/
		
		this.renderLinhaTotal();
		cleanSession();
	}

	public void cleanSession() {
		FacesContext context = FacesContext.getCurrentInstance();
		if (context.getExternalContext().getSessionMap().get("relatorioosBean") != null) {
			context.getExternalContext().getSessionMap().remove("relatorioosBean");
		}
	}

	public String now() {
		DateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		Date date = new Date();
		return dateformat.format(date);
	}

	public String formataNumero(String number) {
		String result = "";
		if (number != null && !number.isEmpty()) {
			DecimalFormat value = new DecimalFormat("###,##0.00");
			result = value.format(Float.parseFloat(number));
			String fp = result.substring(0, result.length() - 3);
			String sp = result.substring(result.length() - 3);
			result = fp.replace(",", ".") + sp.replace(".", ",");
		}

		if (result.equals("0,00")) {
			return "0";
		}
		
		return result;
	}

	public String valorCustoOsTotalGeral() {
		this.valorCustoOsTotal = 0.00;
		objRsRelat.forEach((item) -> {
			this.valorCustoOsTotal += Double.parseDouble(item.getVL_CUST_ORDE_DE_SERV());
		});
		return formataNumero(Double.toString(this.valorCustoOsTotal));
	}
	
	public void renderLinhaTotal() {
		if (objRsRelat.size() > 0) {
			this.setLinhaTotal(true);
		}
		
		this.setLinhaTotal(false);
	}

	public void postProcessExcel(Object doc) {
		try {
			XSSFWorkbook wb = (XSSFWorkbook) doc;
			XSSFSheet sheet = wb.getSheetAt(0);
			sheet.setDisplayGridlines(false);
			
			formatarCabecalho(wb, sheet);
			formatarCabecalhoTabela(wb, sheet);
			formatarRegistrosTabela(wb, sheet);
			criarRodape(wb, sheet);
			
			sheet.shiftRows(3, sheet.getLastRowNum() + 1, 2);
			sheet.shiftRows(6, sheet.getLastRowNum() + 1, 1);
		} catch (Exception e) {
			System.out.println(e);
		}

	}

	private void formatarCabecalho(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyle1 = wb.createCellStyle();
		headerStyle1.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle1.setWrapText(true);
		headerStyle1.setBorderBottom(CellStyle.BORDER_NONE);
		headerStyle1.setBorderLeft(CellStyle.BORDER_NONE);
		headerStyle1.setBorderRight(CellStyle.BORDER_NONE);
		headerStyle1.setBorderTop(CellStyle.BORDER_NONE);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 14);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		headerStyle1.setFont(font);

		XSSFRow primeiraLinha = sheet.getRow(1);
		if (primeiraLinha != null) {
			XSSFCell primeiraLinhaCell = primeiraLinha.getCell(0);
			primeiraLinhaCell.setCellStyle(headerStyle1);
			primeiraLinha.setHeightInPoints((short) 19);
		}
		XSSFRow segundaLinha = sheet.getRow(2);
		if (segundaLinha != null) {
			XSSFCell segundaLinhaCell = segundaLinha.getCell(0);
			segundaLinhaCell.setCellStyle(headerStyle1);
			segundaLinha.setHeightInPoints((short) 19);
		}
	}

	private void formatarCabecalhoTabela(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setWrapText(true);
		headerStyle.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyle.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyle.setBorderRight(CellStyle.BORDER_THIN);
		headerStyle.setBorderTop(CellStyle.BORDER_THIN);
		headerStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.WHITE.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(font);

		XSSFRow linhaCabecalhoTabela = sheet.getRow(4);
		if (linhaCabecalhoTabela != null) {
			for (int i = 0; i <= linhaCabecalhoTabela.getLastCellNum(); i++) {
				XSSFCell celulaHeader = linhaCabecalhoTabela.getCell(i);
				if (celulaHeader != null) {
					celulaHeader.setCellStyle(headerStyle);
				}
			}
		}
	}

	private void formatarRegistrosTabela(XSSFWorkbook wb, XSSFSheet sheet) {

		if (objRsRelat != null && !objRsRelat.isEmpty()) {
			formatarLinhasDetalhe(wb, sheet);
			formatarLinhaTotal(wb, sheet);
		} else {
			formatarDetalheVazio(wb, sheet);
		}

		for (int i = 0; i <= sheet.getRow(4).getLastCellNum(); i++) {
			sheet.autoSizeColumn(i);
		}
	}

	private void formatarDetalheVazio(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setWrapText(true);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(font);
		
		CellRangeAddress detalheVazio = new CellRangeAddress(5, 5, 0, 9);
		sheet.addMergedRegion(detalheVazio);

		XSSFRow linhaVazia = sheet.getRow(5);
		if (linhaVazia == null) {
			linhaVazia = sheet.createRow(5);
		}
		XSSFCell celulaVazia = linhaVazia.createCell(0);
		linhaVazia.setHeightInPoints((short) 20);
		celulaVazia.setCellValue("Não existem dados para o período selecionado.");
		celulaVazia.setCellStyle(headerStyle);

		RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
		RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
		RegionUtil.setBorderRight(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
		RegionUtil.setBorderTop(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);

	}

	private void formatarLinhasDetalhe(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyleLeft = wb.createCellStyle();
		headerStyleLeft.setAlignment(CellStyle.ALIGN_LEFT);
		headerStyleLeft.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderTop(CellStyle.BORDER_THIN);
		headerStyleLeft.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		headerStyleLeft.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		
		XSSFCellStyle headerStyleRight = wb.createCellStyle();
		headerStyleRight.setAlignment(CellStyle.ALIGN_RIGHT);
		headerStyleRight.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderTop(CellStyle.BORDER_THIN);
		headerStyleRight.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		headerStyleRight.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.WHITE.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyleLeft.setFont(font);
		headerStyleRight.setFont(font);
		
		int linhaInicial = 5;
		int linhasDetalhe = objRsRelat.size();
		int linhaFinal = linhaInicial + linhasDetalhe;

		for (int linha = linhaInicial; linha <= linhaFinal; linha++) {
			XSSFRow detalheTabela = sheet.getRow(linha);
			if (detalheTabela != null) {
				for (int i = 0; i <= detalheTabela.getLastCellNum(); i++) {
					XSSFCell celulaDetalhe = detalheTabela.getCell(i);
					if (celulaDetalhe != null) {
						if (i == 0 || i == 2) {
							celulaDetalhe.setCellStyle(headerStyleLeft);
						} else {
							celulaDetalhe.setCellStyle(headerStyleRight);
						}
						
					}
				}
			}
		}
	}

	private void formatarLinhaTotal(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyleLeft = wb.createCellStyle();
		headerStyleLeft.setAlignment(CellStyle.ALIGN_LEFT);
		headerStyleLeft.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderTop(CellStyle.BORDER_THIN);
		headerStyleLeft.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		headerStyleLeft.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		
		XSSFCellStyle headerStyleRight = wb.createCellStyle();
		headerStyleRight.setAlignment(CellStyle.ALIGN_RIGHT);
		headerStyleRight.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderTop(CellStyle.BORDER_THIN);
		headerStyleRight.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		headerStyleRight.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.WHITE.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyleLeft.setFont(font);
		headerStyleRight.setFont(font);
		
		CellRangeAddress detalheQntd = new CellRangeAddress(4 + objRsRelat.size(), 4 + objRsRelat.size(), 0, 8);
		sheet.addMergedRegion(detalheQntd);
		
		XSSFRow linhaQntd = sheet.getRow(4 + objRsRelat.size());
		if (linhaQntd == null) {
			linhaQntd = sheet.createRow(4 + objRsRelat.size());
		}
		
		XSSFCell celulaVazia = linhaQntd.createCell(0);
		linhaQntd.setHeightInPoints((short) 20);
		celulaVazia.setCellValue("Custo Total OS´s:");
		celulaVazia.setCellStyle(headerStyleRight);
		
		XSSFCell celulaVazia2 = linhaQntd.createCell(9);
		linhaQntd.setHeightInPoints((short) 20);
		celulaVazia2.setCellValue(this.valorCustoOsTotalGeral());
		celulaVazia2.setCellStyle(headerStyleRight);
	}

	private void criarRodape(XSSFWorkbook wb, XSSFSheet sheet) {
		Font font;
		/* Criação, inclusão e formatação dos dados do footer do excel */
		XSSFRow ultimaLinha = sheet.createRow(sheet.getLastRowNum() + 1);

		XSSFCellStyle footerStyle = wb.createCellStyle();
		footerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		footerStyle.setWrapText(true);
		font = footerStyle.getFont();
		font.setFontHeight((short) 150);
		font.setFontName("Arial");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		footerStyle.setFont(font);

		XSSFCell cell1 = ultimaLinha.createCell(0);
		cell1.setCellValue(now());
		cell1.setCellStyle(footerStyle);

		XSSFCell cell2 = ultimaLinha.createCell(3);
		cell2.setCellValue("Superintêndencia de Ocorrências Especiais");
		cell2.setCellStyle(footerStyle);

		CellRangeAddress region1 = new CellRangeAddress(ultimaLinha.getRowNum(), ultimaLinha.getRowNum(), 0, 2);
		CellRangeAddress region2 = new CellRangeAddress(ultimaLinha.getRowNum(), ultimaLinha.getRowNum(), 3, 9);
		sheet.addMergedRegion(region1);
		sheet.addMergedRegion(region2);
	}

	public List<RelOSCustoTotalOSModel> getObjRsRelat() {
		return objRsRelat;
	}

	public void setObjRsRelat(List<RelOSCustoTotalOSModel> objRsRelat) {
		this.objRsRelat = objRsRelat;
	}

	public String getStrDtIni() {
		return strDtIni;
	}

	public void setStrDtIni(String strDtIni) {
		this.strDtIni = strDtIni;
	}

	public String getStrDtFim() {
		return strDtFim;
	}

	public void setStrDtFim(String strDtFim) {
		this.strDtFim = strDtFim;
	}

	public boolean isLinhaTotal() {
		return linhaTotal;
	}

	public void setLinhaTotal(boolean linhaTotal) {
		this.linhaTotal = linhaTotal;
	}

}
