package com.altec.bsbr.app.hyb.web.jsf;

import java.io.Serializable;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.fw.web.jsf.BasicBBean;

@Component("RelatorioBean")
@Scope("session")

public class RelatorioBean extends BasicBBean {
	
	private static final long serialVersionUID = 1L;
	
	//forma de pegar o valor pela url
	//private Object strArea = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("area");
	
	private Number update = 6;
	
	private String strArea = "GOI";

	public String getStrArea() {
		return strArea;
	}

	public void setStrArea(String strArea) {
		this.strArea = strArea;
	}

	public Number getUpdate() {
		return update;
	}

	public void setUpdate(Number update) {
		this.update = update;
	}


}
