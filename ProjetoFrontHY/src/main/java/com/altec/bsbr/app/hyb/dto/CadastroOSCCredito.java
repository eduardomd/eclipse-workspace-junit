package com.altec.bsbr.app.hyb.dto;

public class CadastroOSCCredito {
	private String CD_CANAL ;
	private String strErro;	// private String de Erro
	private String strNrSeqOs;	// private String armazena o nmero da OS
	private String strNrSeqId; // private String que armazena o nmero sequencial da Fraude Canal
	

	private boolean blnSalvar;		// Indicador de gravao
	private String strTitular;		// Nome do Titular
	private String strSeqCartao;	// Nmero Sequencial do Carto
	private String strSeqConta;     // Nmero Sequencial da Conta
	private String strAgencia;		// Nmero da Agncia
	private String strConta;		// Numero da Conta
	private String strTpConta;		// Tipo de Conta
	private String strCartao;		// Nmero do Carto de crdito
	private String strCdCanal;		// Cdigo do Canal
	private String strSeqEvento;	// Nmero Sequencial da Fraude Evento
	private String strCPF_CNPJ;		// Nmero do CPF ou CNPJ
	private String strDtEmissao;	// Data Emissão do Cartão
	private String strDtAtivacao;	// Data de Ativacao do Carto
	private String strDtCancel;		// Data de Cancelamento do carto
	private String strLoginColab;	// Login do Colaborador solicitante
	private String strDtAbertura;	// Data da Abertura da Conta
	private String strNmGerente;	// Nome do gerente
	private String strStatusCnt;	// Status da Conta
	private String strEndEnviado;	// Endereo enviado
	private int intFragLocal;	// Indice do local da Fragilizao do Carto
	private String strFragHrInicio; // Hora Inicial da Fragilizao do Carto
	private String strFragHrFinal;  // Hora Final da Fragilizao do Carto
	private String strFragCodAgen;	// Cdigo da agncia Fragilizao
	private String strFragNmAgen;	// Nome da agncia Fragilizao
	private String strFragDtOcorr;	// Data da Ocorrncia Fragilizao
	private String strFragNrTerm;	// Nmero do terminal da Fragilizao
	private boolean blnFragSensor;	// Possui Sensor Fragilizao
	private String strFragOutros;	// Outros Fragilizao
	private String strLocalFraude;	// Local da Fraude
	private String strDtFraude;		// Data da Fraude
	private String strValorFraude;	// Valor da Fraude
	private String strSldDevFraude;	// Saldo Devedor da Fraude
	private String strEstabFraude;	// Estabelecimentos Fraude
	private String txtHdCD_UOR;     // Cdigo PV 
	private int intFragClafFrau; // Indice da Classificao da Fraude
	private String strAgenClieAjud; // Agncia do Cliente Ajudado
	private String strNrQntdEven;
	private String strCdCentroOrigem;	//Centro de origem		pCD_CTRO_ORIG
	private String strCdCentroDestino;	//Centro de destino		pCD_CTRO_DEST
	private String strCdCentroOperante;	//Centro operante		pCD_CTRO_OPER
	private String strContabilizado;	//valida contablizao
	private String cboSensor;
	private String cboClafFrau;
	private String cboLocal;
	
	public CadastroOSCCredito() {}

	public String getCD_CANAL() {
		return CD_CANAL;
	}

	public void setCD_CANAL(String cD_CANAL) {
		CD_CANAL = cD_CANAL;
	}

	public String getStrErro() {
		return strErro;
	}

	public void setStrErro(String strErro) {
		this.strErro = strErro;
	}

	public String getStrNrSeqOs() {
		return strNrSeqOs;
	}

	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}

	public String getStrNrSeqId() {
		return strNrSeqId;
	}

	public void setStrNrSeqId(String strNrSeqId) {
		this.strNrSeqId = strNrSeqId;
	}

	public boolean isBlnSalvar() {
		return blnSalvar;
	}

	public void setBlnSalvar(boolean blnSalvar) {
		this.blnSalvar = blnSalvar;
	}

	public String getStrTitular() {
		return strTitular;
	}

	public void setStrTitular(String strTitular) {
		this.strTitular = strTitular;
	}

	public String getStrSeqCartao() {
		return strSeqCartao;
	}

	public void setStrSeqCartao(String strSeqCartao) {
		this.strSeqCartao = strSeqCartao;
	}

	public String getStrSeqConta() {
		return strSeqConta;
	}

	public void setStrSeqConta(String strSeqConta) {
		this.strSeqConta = strSeqConta;
	}

	public String getStrAgencia() {
		return strAgencia;
	}

	public void setStrAgencia(String strAgencia) {
		this.strAgencia = strAgencia;
	}

	public String getStrConta() {
		return strConta;
	}

	public void setStrConta(String strConta) {
		this.strConta = strConta;
	}

	public String getStrTpConta() {
		return strTpConta;
	}

	public void setStrTpConta(String strTpConta) {
		this.strTpConta = strTpConta;
	}

	public String getStrCartao() {
		return strCartao;
	}

	public void setStrCartao(String strCartao) {
		this.strCartao = strCartao;
	}

	public String getStrCdCanal() {
		return strCdCanal;
	}

	public void setStrCdCanal(String strCdCanal) {
		this.strCdCanal = strCdCanal;
	}

	public String getStrSeqEvento() {
		return strSeqEvento;
	}

	public void setStrSeqEvento(String strSeqEvento) {
		this.strSeqEvento = strSeqEvento;
	}

	public String getStrCPF_CNPJ() {
		return strCPF_CNPJ;
	}

	public void setStrCPF_CNPJ(String strCPF_CNPJ) {
		this.strCPF_CNPJ = strCPF_CNPJ;
	}

	public String getStrDtEmissao() {
		return strDtEmissao;
	}

	public void setStrDtEmissao(String strDtEmissao) {
		this.strDtEmissao = strDtEmissao;
	}

	public String getStrDtAtivacao() {
		return strDtAtivacao;
	}

	public void setStrDtAtivacao(String strDtAtivacao) {
		this.strDtAtivacao = strDtAtivacao;
	}

	public String getStrDtCancel() {
		return strDtCancel;
	}

	public void setStrDtCancel(String strDtCancel) {
		this.strDtCancel = strDtCancel;
	}

	public String getStrLoginColab() {
		return strLoginColab;
	}

	public void setStrLoginColab(String strLoginColab) {
		this.strLoginColab = strLoginColab;
	}

	public String getStrDtAbertura() {
		return strDtAbertura;
	}

	public void setStrDtAbertura(String strDtAbertura) {
		this.strDtAbertura = strDtAbertura;
	}

	public String getStrNmGerente() {
		return strNmGerente;
	}

	public void setStrNmGerente(String strNmGerente) {
		this.strNmGerente = strNmGerente;
	}

	public String getStrStatusCnt() {
		return strStatusCnt;
	}

	public void setStrStatusCnt(String strStatusCnt) {
		this.strStatusCnt = strStatusCnt;
	}

	public String getStrEndEnviado() {
		return strEndEnviado;
	}

	public void setStrEndEnviado(String strEndEnviado) {
		this.strEndEnviado = strEndEnviado;
	}

	public int getIntFragLocal() {
		return intFragLocal;
	}

	public void setIntFragLocal(int intFragLocal) {
		this.intFragLocal = intFragLocal;
	}

	public String getStrFragHrInicio() {
		return strFragHrInicio;
	}

	public void setStrFragHrInicio(String strFragHrInicio) {
		this.strFragHrInicio = strFragHrInicio;
	}

	public String getStrFragHrFinal() {
		return strFragHrFinal;
	}

	public void setStrFragHrFinal(String strFragHrFinal) {
		this.strFragHrFinal = strFragHrFinal;
	}

	public String getStrFragCodAgen() {
		return strFragCodAgen;
	}

	public void setStrFragCodAgen(String strFragCodAgen) {
		this.strFragCodAgen = strFragCodAgen;
	}

	public String getStrFragNmAgen() {
		return strFragNmAgen;
	}

	public void setStrFragNmAgen(String strFragNmAgen) {
		this.strFragNmAgen = strFragNmAgen;
	}

	public String getStrFragDtOcorr() {
		return strFragDtOcorr;
	}

	public void setStrFragDtOcorr(String strFragDtOcorr) {
		this.strFragDtOcorr = strFragDtOcorr;
	}

	public String getStrFragNrTerm() {
		return strFragNrTerm;
	}

	public void setStrFragNrTerm(String strFragNrTerm) {
		this.strFragNrTerm = strFragNrTerm;
	}

	public boolean isBlnFragSensor() {
		return blnFragSensor;
	}

	public void setBlnFragSensor(boolean blnFragSensor) {
		this.blnFragSensor = blnFragSensor;
	}

	public String getStrFragOutros() {
		return strFragOutros;
	}

	public void setStrFragOutros(String strFragOutros) {
		this.strFragOutros = strFragOutros;
	}

	public String getStrLocalFraude() {
		return strLocalFraude;
	}

	public void setStrLocalFraude(String strLocalFraude) {
		this.strLocalFraude = strLocalFraude;
	}

	public String getStrDtFraude() {
		return strDtFraude;
	}

	public void setStrDtFraude(String strDtFraude) {
		this.strDtFraude = strDtFraude;
	}

	public String getStrValorFraude() {
		return strValorFraude;
	}

	public void setStrValorFraude(String strValorFraude) {
		this.strValorFraude = strValorFraude;
	}

	public String getStrSldDevFraude() {
		return strSldDevFraude;
	}

	public void setStrSldDevFraude(String strSldDevFraude) {
		this.strSldDevFraude = strSldDevFraude;
	}

	public String getStrEstabFraude() {
		return strEstabFraude;
	}

	public void setStrEstabFraude(String strEstabFraude) {
		this.strEstabFraude = strEstabFraude;
	}

	public String getTxtHdCD_UOR() {
		return txtHdCD_UOR;
	}

	public void setTxtHdCD_UOR(String txtHdCD_UOR) {
		this.txtHdCD_UOR = txtHdCD_UOR;
	}

	public int getIntFragClafFrau() {
		return intFragClafFrau;
	}

	public void setIntFragClafFrau(int intFragClafFrau) {
		this.intFragClafFrau = intFragClafFrau;
	}

	public String getStrAgenClieAjud() {
		return strAgenClieAjud;
	}

	public void setStrAgenClieAjud(String strAgenClieAjud) {
		this.strAgenClieAjud = strAgenClieAjud;
	}

	public String getStrNrQntdEven() {
		return strNrQntdEven;
	}

	public void setStrNrQntdEven(String strNrQntdEven) {
		this.strNrQntdEven = strNrQntdEven;
	}

	public String getStrCdCentroOrigem() {
		return strCdCentroOrigem;
	}

	public void setStrCdCentroOrigem(String strCdCentroOrigem) {
		this.strCdCentroOrigem = strCdCentroOrigem;
	}

	public String getStrCdCentroDestino() {
		return strCdCentroDestino;
	}

	public void setStrCdCentroDestino(String strCdCentroDestino) {
		this.strCdCentroDestino = strCdCentroDestino;
	}

	public String getStrCdCentroOperante() {
		return strCdCentroOperante;
	}

	public void setStrCdCentroOperante(String strCdCentroOperante) {
		this.strCdCentroOperante = strCdCentroOperante;
	}

	public String getStrContabilizado() {
		return strContabilizado;
	}

	public void setStrContabilizado(String strContabilizado) {
		this.strContabilizado = strContabilizado;
	}

	public String getCboSensor() {
		return cboSensor;
	}

	public void setCboSensor(String cboSensor) {
		this.cboSensor = cboSensor;
	}

	public String getCboClafFrau() {
		return cboClafFrau;
	}

	public void setCboClafFrau(String cboClafFrau) {
		this.cboClafFrau = cboClafFrau;
	}

	public String getCboLocal() {
		return cboLocal;
	}

	public void setCboLocal(String cboLocal) {
		this.cboLocal = cboLocal;
	}

	
	
	
}
