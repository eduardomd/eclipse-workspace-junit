package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.ObjRsManutCanaisCanalModel;
import com.altec.bsbr.app.hyb.dto.RelosEvento;
import com.altec.bsbr.app.jab.hyb.webclient.XHYRelatorios.XHYRelatoriosEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYRelatorios.WebServiceException;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component("relosEventoBean")
@Scope("session")
public class RelosEventoBean extends BasicBBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//private RelosEvento relosEvento;
	private List<RelosEvento> objRsRelat = new ArrayList<RelosEvento>();
	
	private String strArea;
	private String pCodArea;
	private String strTpRel;
	private String strDtIni;
	private String strDtFim;
	private String intCanal;
	private String strNmRelat;
	
	@Autowired
	private XHYRelatoriosEndPoint objRelat;
	
	ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
	
	@PostConstruct
	public void init() throws IOException {
		FacesContext fc = FacesContext.getCurrentInstance();
		@SuppressWarnings("unchecked")
		Map<String, String> params = fc.getExternalContext().getRequestParameterMap();

		this.strArea = params.get("pArea");
		this.pCodArea = params.get("pCodArea");
		this.strTpRel = params.get("pTpRel");
		this.strDtIni = params.get("pDtIni");
		this.strDtFim = params.get("pDtFim");
		this.intCanal = params.get("pCdCanal");
		this.strNmRelat = params.get("pNmRel");
		
		if (this.objRsRelat != null) {
			this.objRsRelat.clear();
		}
		
		JSONArray pcursorObjRs = new JSONArray();
		
		try {
			String objRes = objRelat.fnRelEventos(this.strDtIni, this.strDtFim);
			//String objRes = objRelat.fnRelEventos("01/01/2016", "01/01/2019");

			JSONObject objResTemp = new JSONObject(objRes);
			pcursorObjRs = objResTemp.getJSONArray("PCURSOR");
		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			ec.redirect("hy_erro.xhtml?&strErro=" + e.getMessage());
		}
		
		for (int i = 0; i < pcursorObjRs.length(); i++) {
            JSONObject temp = pcursorObjRs.getJSONObject(i);

            objRsRelat.add(
            		new RelosEvento(
            				temp.get("NR_SEQU_ORDE_SERV").toString(),
            				temp.get("CNAL_ORIG").toString(),
            				temp.get("STATUS").toString(),
            				temp.get("NM_PARP").toString(),
            				temp.get("TP_PARP").toString(),
            				temp.get("EVENTO").toString(),
            				temp.get("CANAL").toString(),
            				temp.get("TX_OUTR_PRPC").toString(),
            				temp.get("DT_PREV_ENCE_ORDE_SERV").toString(),
            				temp.get("CD_AREA").toString()
            		));
        }
		
		cleanSession();
	}

	public void cleanSession() {
		FacesContext context = FacesContext.getCurrentInstance();
		if (context.getExternalContext().getSessionMap().get("relatorioosBean") != null) {
			context.getExternalContext().getSessionMap().remove("relatorioosBean");
		}
	}
	public String now() {
		DateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();
		return dateformat.format(date);
	}

	public List<RelosEvento> getObjRsRelat() {
		return objRsRelat;
	}

	public void setObjRsRelat(List<RelosEvento> objRsRelat) {
		this.objRsRelat = objRsRelat;
	}

	public String getStrArea() {
		return strArea;
	}

	public void setStrArea(String strArea) {
		this.strArea = strArea;
	}

	public String getpCodArea() {
		return pCodArea;
	}

	public void setpCodArea(String pCodArea) {
		this.pCodArea = pCodArea;
	}

	public String getStrTpRel() {
		return strTpRel;
	}

	public void setStrTpRel(String strTpRel) {
		this.strTpRel = strTpRel;
	}

	public String getStrNmRelat() {
		return strNmRelat;
	}

	public void setStrNmRelat(String strNmRelat) {
		this.strNmRelat = strNmRelat;
	}

	public String getStrDtIni() {
		return strDtIni;
	}

	public void setStrDtIni(String strDtIni) {
		this.strDtIni = strDtIni;
	}

	public String getStrDtFim() {
		return strDtFim;
	}

	public void setStrDtFim(String strDtFim) {
		this.strDtFim = strDtFim;
	}

	public String getIntCanal() {
		return intCanal;
	}

	public void setIntCanal(String intCanal) {
		this.intCanal = intCanal;
	}

}