package com.altec.bsbr.app.hyb.web.jsf;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.altec.bsbr.app.hyb.dto.CadastroosDetalheRiscopot;

@Component("cadastroosDetalhesRiscopot")
@Scope("session")
public class CadastroosDetalhesRiscopotBean extends BasicBBean{

	private static final long serialVersionUID = 1L;
	private CadastroosDetalheRiscopot cadastroosDetalheRiscopot;
	
	public CadastroosDetalheRiscopot getCadastroosDetalheRiscopot() {
		return cadastroosDetalheRiscopot;
	}
	public void setCadastroosDetalheRiscopot(CadastroosDetalheRiscopot cadastroosDetalheRiscopot) {
		this.cadastroosDetalheRiscopot = cadastroosDetalheRiscopot;
	}
	
	public CadastroosDetalhesRiscopotBean() {
		CadastroosDetalheRiscopot cadastroosDetalheRiscopot = new CadastroosDetalheRiscopot();
	}
}
