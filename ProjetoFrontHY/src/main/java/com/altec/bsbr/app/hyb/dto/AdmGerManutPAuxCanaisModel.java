package com.altec.bsbr.app.hyb.dto;

public class AdmGerManutPAuxCanaisModel {
	private int CODIGO;
	private String NOME;
	private boolean EXCLUSAO;
	private boolean CHECKED;
	
	public AdmGerManutPAuxCanaisModel(int cODIGO, String nOME, boolean eXCLUSAO) {
		CODIGO = cODIGO;
		NOME = nOME;
		EXCLUSAO = eXCLUSAO;
	}

	public AdmGerManutPAuxCanaisModel() {
	}

	public int getCODIGO() {
		return CODIGO;
	}	
	public void setCODIGO(int cODIGO) {
		CODIGO = cODIGO;
	}
	
	public String getNOME() {
		return NOME;
	}
	public void setNOME(String nOME) {
		NOME = nOME;
	}
	
	public boolean getEXCLUSAO() {
		return EXCLUSAO;
	}
	public void setEXCLUSAO(boolean eXCLUSAO) {
		EXCLUSAO = eXCLUSAO;
	}

	public boolean getCHECKED() {
		return CHECKED;
	}

	public void setCHECKED(boolean cHECKED) {
		CHECKED = cHECKED;
	}
}
