package com.altec.bsbr.app.hyb.dto;

public class CadOsDatas {
	public String PERIODO;
	public String DETECCAO;
	public String ABERTURA;
	public String ENCERRAMENTO;
	public String TIPO_DT_EVENTO;
	public String CODIGO;
	public String PERIODO_FIN;
	public String PERIODO_INI;
	public String FIXA;
		
	public String getPERIODO() {
		return PERIODO;
	}
	public void setPERIODO(String pERIODO) {
		PERIODO = pERIODO;
	}
	public String getDETECCAO() {
		return DETECCAO;
	}
	public void setDETECCAO(String dETECCAO) {
		DETECCAO = dETECCAO;
	}
	public String getABERTURA() {
		return ABERTURA;
	}
	public void setABERTURA(String aBERTURA) {
		ABERTURA = aBERTURA;
	}
	public String getENCERRAMENTO() {
		return ENCERRAMENTO;
	}
	public void setENCERRAMENTO(String eNCERRAMENTO) {
		ENCERRAMENTO = eNCERRAMENTO;
	}
	public String getTIPO_DT_EVENTO() {
		return TIPO_DT_EVENTO;
	}
	public void setTIPO_DT_EVENTO(String tIPO_DT_EVENTO) {
		TIPO_DT_EVENTO = tIPO_DT_EVENTO;
	}
	public String getCODIGO() {
		return CODIGO;
	}
	public void setCODIGO(String cODIGO) {
		CODIGO = cODIGO;
	}
	
	public String getPERIODO_FIN() {
		return PERIODO_FIN;
	}

	public void setPERIODO_FIN(String pERIODO_FIN) {
		PERIODO_FIN = pERIODO_FIN;
	}

	public String getPERIODO_INI() {
		return PERIODO_INI;
	}

	public void setPERIODO_INI(String pERIODO_INI) {
		PERIODO_INI = pERIODO_INI;
	}

	public String getFIXA() {
		return FIXA;
	}

	public void setFIXA(String fIXA) {
		FIXA = fIXA;
	}
}
