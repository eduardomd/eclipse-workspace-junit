package com.altec.bsbr.app.hyb.web.jsf;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;

import com.altec.bsbr.app.hyb.dto.Contabilizacao_Model;
import com.altec.bsbr.app.hyb.web.util.XHYUsuarioIncService;

@Component("Contabilizacao")
@Scope("session")
public class Contabilizacao_Bean extends BasicBBean {

	private static final long serialVersionUID = 1L;
	private Object strNrSeqOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs");
	private String txtHdAction = "B";
	
	@Autowired 
	private XHYUsuarioIncService userServ;
	
	private Contabilizacao_Model objRsOs = new Contabilizacao_Model();
	
	public Contabilizacao_Bean() {

	}

	public void verificarPermissao(ComponentSystemEvent event) {
		String[] perfis = {"SUPERINT","GER"};
		
		userServ.verificarPermissao(perfis);
	}
	
	public Contabilizacao_Bean(Object strNrSeqOs, String txtHdAction, Contabilizacao_Model objRsOs) {
		super();
		this.strNrSeqOs = strNrSeqOs;
		this.txtHdAction = txtHdAction;
		this.objRsOs = objRsOs;
	}

	public Contabilizacao_Model getobjRsOs() {
		return objRsOs;
	}

	public void setobjRsOs(Contabilizacao_Model objRsOs) {
		this.objRsOs = objRsOs;
	}

	public Object getStrNrSeqOs() {
		return strNrSeqOs;
	}

	public String getTxtHdAction() {
		return txtHdAction;
	}

	public void setTxtHdAction(String txtHdAction) {
		this.txtHdAction = txtHdAction;
	}
	
	@PostConstruct
    public void PostConstruct() {
		this.objRsOs.setSITUACAO("Situacao");
		this.objRsOs.setCD_NOTI("6010");
		this.objRsOs.setNM_NOTI("Nome Notificacao");
		this.objRsOs.setVL_ENVOLVIDO("Valor envolvido");
		this.objRsOs.setDT_CONTABILIZACAO("Data Contabilizacao");
		this.objRsOs.setCONTA_CONTABIL("Conta cont�bil");
		this.objRsOs.setEMPRESA("Empresa");
		this.objRsOs.setAREA_COMPL("Area Compl");
		this.objRsOs.setCANAL_ORIG_CNTB("Canal Origem contabilidade");
		this.objRsOs.setCATEG_N1("test");
		this.objRsOs.setCATEG_N2("test");
		this.objRsOs.setCATEG_N3("test");
		this.objRsOs.setLINHA_NEGO_N1("test");
		this.objRsOs.setLINHA_NEGO_N2("test");
		this.objRsOs.setFATOR_RISCO("test");
		this.objRsOs.setCENTRO_CONTABIL("test");
		this.objRsOs.setCENTRO_ORIGEM("test");
		this.objRsOs.setPONTO_VENDA("test");
	}
	
	public void setAction() {
		this.setTxtHdAction("A");
	}
	
}
