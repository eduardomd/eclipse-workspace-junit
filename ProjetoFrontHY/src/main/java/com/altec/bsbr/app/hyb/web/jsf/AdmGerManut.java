package com.altec.bsbr.app.hyb.web.jsf;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.fw.web.jsf.BasicBBean;

@Component("AdmGerManut")
@Scope("session")
public class AdmGerManut extends BasicBBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
