package com.altec.bsbr.app.hyb.dto;

public class CadosEquipatribLanchr {
	private String strNomeRecurso ;
	private String strNrSeqOs;
	private String strCpf;
	private String HORAS;
	private String GASTOS_ADICIONAIS;
	private String txtHdAction;
	
	public String getStrNomeRecurso() {
		return strNomeRecurso;
	}
	public void setStrNomeRecurso(String strNomeRecurso) {
		this.strNomeRecurso = strNomeRecurso;
	}
	public String getStrNrSeqOs() {
		return strNrSeqOs;
	}
	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}
	public String getStrCpf() {
		return strCpf;
	}
	public void setStrCpf(String strCpf) {
		this.strCpf = strCpf;
	}
	public String getHORAS() {
		return HORAS;
	}
	public void setHORAS(String hORAS) {
		HORAS = hORAS;
	}
	public String getGASTOS_ADICIONAIS() {
		return GASTOS_ADICIONAIS;
	}
	public void setGASTOS_ADICIONAIS(String gASTOS_ADICIONAIS) {
		GASTOS_ADICIONAIS = gASTOS_ADICIONAIS;
	}
	public String getTxtHdAction() {
		return txtHdAction;
	}
	public void setTxtHdAction(String txtHdAction) {
		this.txtHdAction = txtHdAction;
	}
}
