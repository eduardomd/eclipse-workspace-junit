package com.altec.bsbr.app.hyb.dto;

public class Hyb_preenchetramite_model {
	
	private String maior;
	
	public Hyb_preenchetramite_model() {
		
	}

	public Hyb_preenchetramite_model(String maior) {
		this.maior = maior;
	}

	public String getMaior() {
		return maior;
	}

	public void setMaior(String maior) {
		this.maior = maior;
	}
	
}
