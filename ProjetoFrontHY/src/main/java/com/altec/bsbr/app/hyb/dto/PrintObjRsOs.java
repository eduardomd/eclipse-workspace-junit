package com.altec.bsbr.app.hyb.dto;

import javax.faces.bean.ManagedBean;

@ManagedBean(name = "objRsOs")
public class PrintObjRsOs {
	
	private String DATA_ATUAL;
	private String HORA_ATUAL;
	private String CODIGO;
	private String SITUACAO;
	private String CD_NOTI;
	private String NM_NOTI;
	private String NM_CRITICIDADE;
	private String NM_FASE_AN;
	private String DT_ABERTURA;
	private String DT_DETECCAO;
	private String TIPO_DT_EVENTO;
	private String DT_ACOR_FIXA;
	private String DT_OCOR_PERI_INIC;
	private String DT_OCOR_PERI_FINA;
	private String CANAL_ORIGEM;
	private String TP_OFICIO;
	private String TP_LYNX;
	private String TP_MONITORAMENTO;
	private String IN_OMINTE;
	private String IN_OMINTR;
	private String IN_OMINSTR;
	private String IN_OMTELE;
	private String IN_OMMALT;
	private String IN_OMDENC;
	private String IN_OMDEPT;
	private String IN_OMAGEN;
	private String CD_CANAL_ORIGEM;
	private String VL_ENVOLVIDO;
	private String VL_RECUPERADO;
	private String VL_EVITADO;
	private String VL_NEGADO;
	private String VL_TRANSITORIO;
	private String VL_PERDA_EFETIVA;
	private String VL_EVITADO_NORMAL;
	private String VL_PREJUIZO;
	private String DT_CONTABILIZACAO;
	private String CONTA_CONTABIL;
	private String EMPRESA;
	private String CANAL_ORIG_CNTB;
	private String AREA_COMPL;
	private String CATEG_N1;
	private String CATEG_N2;
	private String CATEG_N3;
	private String LINHA_NEGO_N1;
	private String LINHA_NEGO_N2;
	private String FATOR_RISCO;
	private String CENTRO_CONTABIL;
	private String CENTRO_ORIGEM;
	private String PONTO_VENDA;
	private String TX_ABERTURA;
	private String TX_ENCERRAMENTO;
	private String BO;
	private String IN_PROC_JURI;
	private String DELEGACIA;
	private String NM_FORUM;
	private String DT_REG_BO;
	
	public PrintObjRsOs() {}

	public String getDATA_ATUAL() {
		return DATA_ATUAL;
	}

	public void setDATA_ATUAL(String dATA_ATUAL) {
		DATA_ATUAL = dATA_ATUAL;
	}

	public String getHORA_ATUAL() {
		return HORA_ATUAL;
	}

	public void setHORA_ATUAL(String hORA_ATUAL) {
		HORA_ATUAL = hORA_ATUAL;
	}

	public String getCODIGO() {
		return CODIGO;
	}

	public void setCODIGO(String cODIGO) {
		CODIGO = cODIGO;
	}

	public String getSITUACAO() {
		return SITUACAO;
	}

	public void setSITUACAO(String sITUACAO) {
		SITUACAO = sITUACAO;
	}

	public String getCD_NOTI() {
		return CD_NOTI;
	}

	public void setCD_NOTI(String cD_NOTI) {
		CD_NOTI = cD_NOTI;
	}

	public String getNM_NOTI() {
		return NM_NOTI;
	}

	public void setNM_NOTI(String nM_NOTI) {
		NM_NOTI = nM_NOTI;
	}

	public String getNM_CRITICIDADE() {
		return NM_CRITICIDADE;
	}

	public void setNM_CRITICIDADE(String nM_CRITICIDADE) {
		NM_CRITICIDADE = nM_CRITICIDADE;
	}

	public String getNM_FASE_AN() {
		return NM_FASE_AN;
	}

	public void setNM_FASE_AN(String nM_FASE_AN) {
		NM_FASE_AN = nM_FASE_AN;
	}

	public String getDT_ABERTURA() {
		return DT_ABERTURA;
	}

	public void setDT_ABERTURA(String dT_ABERTURA) {
		DT_ABERTURA = dT_ABERTURA;
	}

	public String getDT_DETECCAO() {
		return DT_DETECCAO;
	}

	public void setDT_DETECCAO(String dT_DETECCAO) {
		DT_DETECCAO = dT_DETECCAO;
	}

	public String getTIPO_DT_EVENTO() {
		return TIPO_DT_EVENTO;
	}

	public void setTIPO_DT_EVENTO(String tIPO_DT_EVENTO) {
		TIPO_DT_EVENTO = tIPO_DT_EVENTO;
	}

	public String getDT_ACOR_FIXA() {
		return DT_ACOR_FIXA;
	}

	public void setDT_ACOR_FIXA(String dT_ACOR_FIXA) {
		DT_ACOR_FIXA = dT_ACOR_FIXA;
	}

	public String getDT_OCOR_PERI_INIC() {
		return DT_OCOR_PERI_INIC;
	}

	public void setDT_OCOR_PERI_INIC(String dT_OCOR_PERI_INIC) {
		DT_OCOR_PERI_INIC = dT_OCOR_PERI_INIC;
	}

	public String getDT_OCOR_PERI_FINA() {
		return DT_OCOR_PERI_FINA;
	}

	public void setDT_OCOR_PERI_FINA(String dT_OCOR_PERI_FINA) {
		DT_OCOR_PERI_FINA = dT_OCOR_PERI_FINA;
	}

	public String getCANAL_ORIGEM() {
		return CANAL_ORIGEM;
	}

	public void setCANAL_ORIGEM(String cANAL_ORIGEM) {
		CANAL_ORIGEM = cANAL_ORIGEM;
	}

	public String getTP_OFICIO() {
		return TP_OFICIO;
	}

	public void setTP_OFICIO(String tP_OFICIO) {
		TP_OFICIO = tP_OFICIO;
	}

	public String getTP_LYNX() {
		return TP_LYNX;
	}

	public void setTP_LYNX(String tP_LYNX) {
		TP_LYNX = tP_LYNX;
	}

	public String getTP_MONITORAMENTO() {
		return TP_MONITORAMENTO;
	}

	public void setTP_MONITORAMENTO(String tP_MONITORAMENTO) {
		TP_MONITORAMENTO = tP_MONITORAMENTO;
	}

	public String getIN_OMINTE() {
		return IN_OMINTE;
	}

	public void setIN_OMINTE(String iN_OMINTE) {
		IN_OMINTE = iN_OMINTE;
	}

	public String getIN_OMINTR() {
		return IN_OMINTR;
	}

	public void setIN_OMINTR(String iN_OMINTR) {
		IN_OMINTR = iN_OMINTR;
	}

	public String getIN_OMINSTR() {
		return IN_OMINSTR;
	}

	public void setIN_OMINSTR(String iN_OMINSTR) {
		IN_OMINSTR = iN_OMINSTR;
	}

	public String getIN_OMTELE() {
		return IN_OMTELE;
	}

	public void setIN_OMTELE(String iN_OMTELE) {
		IN_OMTELE = iN_OMTELE;
	}

	public String getIN_OMMALT() {
		return IN_OMMALT;
	}

	public void setIN_OMMALT(String iN_OMMALT) {
		IN_OMMALT = iN_OMMALT;
	}

	public String getIN_OMDENC() {
		return IN_OMDENC;
	}

	public void setIN_OMDENC(String iN_OMDENC) {
		IN_OMDENC = iN_OMDENC;
	}

	public String getIN_OMDEPT() {
		return IN_OMDEPT;
	}

	public void setIN_OMDEPT(String iN_OMDEPT) {
		IN_OMDEPT = iN_OMDEPT;
	}

	public String getIN_OMAGEN() {
		return IN_OMAGEN;
	}

	public void setIN_OMAGEN(String iN_OMAGEN) {
		IN_OMAGEN = iN_OMAGEN;
	}

	public String getCD_CANAL_ORIGEM() {
		return CD_CANAL_ORIGEM;
	}

	public void setCD_CANAL_ORIGEM(String cD_CANAL_ORIGEM) {
		CD_CANAL_ORIGEM = cD_CANAL_ORIGEM;
	}

	public String getVL_ENVOLVIDO() {
		return VL_ENVOLVIDO;
	}

	public void setVL_ENVOLVIDO(String vL_ENVOLVIDO) {
		VL_ENVOLVIDO = vL_ENVOLVIDO;
	}

	public String getVL_RECUPERADO() {
		return VL_RECUPERADO;
	}

	public void setVL_RECUPERADO(String vL_RECUPERADO) {
		VL_RECUPERADO = vL_RECUPERADO;
	}

	public String getVL_EVITADO() {
		return VL_EVITADO;
	}

	public void setVL_EVITADO(String vL_EVITADO) {
		VL_EVITADO = vL_EVITADO;
	}

	public String getVL_NEGADO() {
		return VL_NEGADO;
	}

	public void setVL_NEGADO(String vL_NEGADO) {
		VL_NEGADO = vL_NEGADO;
	}

	public String getVL_TRANSITORIO() {
		return VL_TRANSITORIO;
	}

	public void setVL_TRANSITORIO(String vL_TRANSITORIO) {
		VL_TRANSITORIO = vL_TRANSITORIO;
	}

	public String getVL_PERDA_EFETIVA() {
		return VL_PERDA_EFETIVA;
	}

	public void setVL_PERDA_EFETIVA(String vL_PERDA_EFETIVA) {
		VL_PERDA_EFETIVA = vL_PERDA_EFETIVA;
	}

	public String getVL_EVITADO_NORMAL() {
		return VL_EVITADO_NORMAL;
	}

	public void setVL_EVITADO_NORMAL(String vL_EVITADO_NORMAL) {
		VL_EVITADO_NORMAL = vL_EVITADO_NORMAL;
	}

	public String getVL_PREJUIZO() {
		return VL_PREJUIZO;
	}

	public void setVL_PREJUIZO(String vL_PREJUIZO) {
		VL_PREJUIZO = vL_PREJUIZO;
	}

	public String getDT_CONTABILIZACAO() {
		return DT_CONTABILIZACAO;
	}

	public void setDT_CONTABILIZACAO(String dT_CONTABILIZACAO) {
		DT_CONTABILIZACAO = dT_CONTABILIZACAO;
	}

	public String getCONTA_CONTABIL() {
		return CONTA_CONTABIL;
	}

	public void setCONTA_CONTABIL(String cONTA_CONTABIL) {
		CONTA_CONTABIL = cONTA_CONTABIL;
	}

	public String getEMPRESA() {
		return EMPRESA;
	}

	public void setEMPRESA(String eMPRESA) {
		EMPRESA = eMPRESA;
	}

	public String getCANAL_ORIG_CNTB() {
		return CANAL_ORIG_CNTB;
	}

	public void setCANAL_ORIG_CNTB(String cANAL_ORIG_CNTB) {
		CANAL_ORIG_CNTB = cANAL_ORIG_CNTB;
	}

	public String getAREA_COMPL() {
		return AREA_COMPL;
	}

	public void setAREA_COMPL(String aREA_COMPL) {
		AREA_COMPL = aREA_COMPL;
	}

	public String getCATEG_N1() {
		return CATEG_N1;
	}

	public void setCATEG_N1(String cATEG_N1) {
		CATEG_N1 = cATEG_N1;
	}

	public String getCATEG_N2() {
		return CATEG_N2;
	}

	public void setCATEG_N2(String cATEG_N2) {
		CATEG_N2 = cATEG_N2;
	}

	public String getCATEG_N3() {
		return CATEG_N3;
	}

	public void setCATEG_N3(String cATEG_N3) {
		CATEG_N3 = cATEG_N3;
	}

	public String getLINHA_NEGO_N1() {
		return LINHA_NEGO_N1;
	}

	public void setLINHA_NEGO_N1(String lINHA_NEGO_N1) {
		LINHA_NEGO_N1 = lINHA_NEGO_N1;
	}

	public String getLINHA_NEGO_N2() {
		return LINHA_NEGO_N2;
	}

	public void setLINHA_NEGO_N2(String lINHA_NEGO_N2) {
		LINHA_NEGO_N2 = lINHA_NEGO_N2;
	}

	public String getFATOR_RISCO() {
		return FATOR_RISCO;
	}

	public void setFATOR_RISCO(String fATOR_RISCO) {
		FATOR_RISCO = fATOR_RISCO;
	}

	public String getCENTRO_CONTABIL() {
		return CENTRO_CONTABIL;
	}

	public void setCENTRO_CONTABIL(String cENTRO_CONTABIL) {
		CENTRO_CONTABIL = cENTRO_CONTABIL;
	}

	public String getCENTRO_ORIGEM() {
		return CENTRO_ORIGEM;
	}

	public void setCENTRO_ORIGEM(String cENTRO_ORIGEM) {
		CENTRO_ORIGEM = cENTRO_ORIGEM;
	}

	public String getPONTO_VENDA() {
		return PONTO_VENDA;
	}

	public void setPONTO_VENDA(String pONTO_VENDA) {
		PONTO_VENDA = pONTO_VENDA;
	}

	public String getTX_ABERTURA() {
		return TX_ABERTURA;
	}

	public void setTX_ABERTURA(String tX_ABERTURA) {
		TX_ABERTURA = tX_ABERTURA;
	}

	public String getTX_ENCERRAMENTO() {
		return TX_ENCERRAMENTO;
	}

	public void setTX_ENCERRAMENTO(String tX_ENCERRAMENTO) {
		TX_ENCERRAMENTO = tX_ENCERRAMENTO;
	}

	public String getBO() {
		return BO;
	}

	public void setBO(String bO) {
		BO = bO;
	}

	public String getIN_PROC_JURI() {
		return IN_PROC_JURI;
	}

	public void setIN_PROC_JURI(String iN_PROC_JURI) {
		IN_PROC_JURI = iN_PROC_JURI;
	}

	public String getDELEGACIA() {
		return DELEGACIA;
	}

	public void setDELEGACIA(String dELEGACIA) {
		DELEGACIA = dELEGACIA;
	}

	public String getNM_FORUM() {
		return NM_FORUM;
	}

	public void setNM_FORUM(String nM_FORUM) {
		NM_FORUM = nM_FORUM;
	}

	public String getDT_REG_BO() {
		return DT_REG_BO;
	}

	public void setDT_REG_BO(String dT_REG_BO) {
		DT_REG_BO = dT_REG_BO;
	}
}