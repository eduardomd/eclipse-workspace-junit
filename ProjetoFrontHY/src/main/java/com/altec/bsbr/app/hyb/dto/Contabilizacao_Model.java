package com.altec.bsbr.app.hyb.dto;

public class Contabilizacao_Model {

   private String SITUACAO;
   private String CD_NOTI;
   private String NM_NOTI;
   private String VL_ENVOLVIDO;
   private String DT_CONTABILIZACAO;
   private String CONTA_CONTABIL;
   private String EMPRESA;
   private String AREA_COMPL;
   private String CANAL_ORIG_CNTB;
   private String CATEG_N1;
   private String CATEG_N2;
   private String CATEG_N3;
   private String LINHA_NEGO_N1;
   private String LINHA_NEGO_N2;
   private String FATOR_RISCO;
   private String CENTRO_CONTABIL;
   private String CENTRO_ORIGEM;
   private String PONTO_VENDA;
       
   public Contabilizacao_Model() {
	   
   }
   
   public Contabilizacao_Model(String sITUACAO, String cD_NOTI, String nM_NOTI, String vL_ENVOLVIDO,
		String dT_CONTABILIZACAO, String cONTA_CONTABIL, String eMPRESA, String aREA_COMPL, String cANAL_ORIG_CNTB,
		String cATEG_N1, String cATEG_N2, String cATEG_N3, String lINHA_NEGO_N1, String lINHA_NEGO_N2,
		String fATOR_RISCO, String cENTRO_CONTABIL, String cENTRO_ORIGEM, String pONTO_VENDA) {
	SITUACAO = sITUACAO;
	CD_NOTI = cD_NOTI;
	NM_NOTI = nM_NOTI;
	VL_ENVOLVIDO = vL_ENVOLVIDO;
	DT_CONTABILIZACAO = dT_CONTABILIZACAO;
	CONTA_CONTABIL = cONTA_CONTABIL;
	EMPRESA = eMPRESA;
	AREA_COMPL = aREA_COMPL;
	CANAL_ORIG_CNTB = cANAL_ORIG_CNTB;
	CATEG_N1 = cATEG_N1;
	CATEG_N2 = cATEG_N2;
	CATEG_N3 = cATEG_N3;
	LINHA_NEGO_N1 = lINHA_NEGO_N1;
	LINHA_NEGO_N2 = lINHA_NEGO_N2;
	FATOR_RISCO = fATOR_RISCO;
	CENTRO_CONTABIL = cENTRO_CONTABIL;
	CENTRO_ORIGEM = cENTRO_ORIGEM;
	PONTO_VENDA = pONTO_VENDA;
   }



	public String getSITUACAO() {
		return SITUACAO;
	}

	public void setSITUACAO(String sITUACAO) {
		SITUACAO = sITUACAO;
	}
	
	public String getCD_NOTI() {
		return CD_NOTI;
	}
	
	public void setCD_NOTI(String cD_NOTI) {
		CD_NOTI = cD_NOTI;
	}
	
	public String getNM_NOTI() {
		return NM_NOTI;
	}
	
	public void setNM_NOTI(String nM_NOTI) {
		NM_NOTI = nM_NOTI;
	}
	
	public String getVL_ENVOLVIDO() {
		return VL_ENVOLVIDO;
	}
	
	public void setVL_ENVOLVIDO(String vL_ENVOLVIDO) {
		VL_ENVOLVIDO = vL_ENVOLVIDO;
	}
	
	public String getDT_CONTABILIZACAO() {
		return DT_CONTABILIZACAO;
	}
	
	public void setDT_CONTABILIZACAO(String dT_CONTABILIZACAO) {
		DT_CONTABILIZACAO = dT_CONTABILIZACAO;
	}
	
	public String getCONTA_CONTABIL() {
		return CONTA_CONTABIL;
	}
	
	public void setCONTA_CONTABIL(String cONTA_CONTABIL) {
		CONTA_CONTABIL = cONTA_CONTABIL;
	}
	
	public String getEMPRESA() {
		return EMPRESA;
	}
	
	public void setEMPRESA(String eMPRESA) {
		EMPRESA = eMPRESA;
	}
	
	public String getAREA_COMPL() {
		return AREA_COMPL;
	}
	
	public void setAREA_COMPL(String aREA_COMPL) {
		AREA_COMPL = aREA_COMPL;
	}
	
	public String getCANAL_ORIG_CNTB() {
		return CANAL_ORIG_CNTB;
	}
	
	public void setCANAL_ORIG_CNTB(String cANAL_ORIG_CNTB) {
		CANAL_ORIG_CNTB = cANAL_ORIG_CNTB;
	}
	
	public String getCATEG_N1() {
		return CATEG_N1;
	}
	
	public void setCATEG_N1(String cATEG_N1) {
		CATEG_N1 = cATEG_N1;
	}
	
	public String getCATEG_N2() {
		return CATEG_N2;
	}
	
	public void setCATEG_N2(String cATEG_N2) {
		CATEG_N2 = cATEG_N2;
	}
	
	public String getCATEG_N3() {
		return CATEG_N3;
	}
	
	public void setCATEG_N3(String cATEG_N3) {
		CATEG_N3 = cATEG_N3;
	}
	
	public String getLINHA_NEGO_N1() {
		return LINHA_NEGO_N1;
	}
	
	public void setLINHA_NEGO_N1(String lINHA_NEGO_N1) {
		LINHA_NEGO_N1 = lINHA_NEGO_N1;
	}
	
	public String getLINHA_NEGO_N2() {
		return LINHA_NEGO_N2;
	}
	
	public void setLINHA_NEGO_N2(String lINHA_NEGO_N2) {
		LINHA_NEGO_N2 = lINHA_NEGO_N2;
	}
	
	public String getFATOR_RISCO() {
		return FATOR_RISCO;
	}
	
	public void setFATOR_RISCO(String fATOR_RISCO) {
		FATOR_RISCO = fATOR_RISCO;
	}
	
	public String getCENTRO_CONTABIL() {
		return CENTRO_CONTABIL;
	}
	
	public void setCENTRO_CONTABIL(String cENTRO_CONTABIL) {
		CENTRO_CONTABIL = cENTRO_CONTABIL;
	}
	
	public String getCENTRO_ORIGEM() {
		return CENTRO_ORIGEM;
	}
	
	public void setCENTRO_ORIGEM(String cENTRO_ORIGEM) {
		CENTRO_ORIGEM = cENTRO_ORIGEM;
	}
	
	public String getPONTO_VENDA() {
		return PONTO_VENDA;
	}
	
	public void setPONTO_VENDA(String pONTO_VENDA) {
		PONTO_VENDA = pONTO_VENDA;
	}
   
}