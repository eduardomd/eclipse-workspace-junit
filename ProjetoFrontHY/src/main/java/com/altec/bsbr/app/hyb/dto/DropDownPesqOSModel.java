package com.altec.bsbr.app.hyb.dto;

public class DropDownPesqOSModel {

	private String CODIGO;
	private String NOME;
	
	public String getCODIGO() {
		return CODIGO;
	}
	public void setCODIGO(String cODIGO) {
		CODIGO = cODIGO;
	}
	public String getNOME() {
		return NOME;
	}
	public void setNOME(String nOME) {
		NOME = nOME;
	}
	
	public DropDownPesqOSModel(String CODIGO, String NOME) {
		this.CODIGO = CODIGO;
		this.NOME = NOME;
	}
	
	public DropDownPesqOSModel() {
		
	}
	
	
}
