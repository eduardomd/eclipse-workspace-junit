package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.primefaces.component.export.ExcelOptions;
import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.DropDownPesqOSModel;
import com.altec.bsbr.app.hyb.dto.Hyb_excel_pesquisa_resultado_model;
import com.altec.bsbr.app.hyb.dto.PesquisaOsResultadoModel;
import com.altec.bsbr.app.hyb.dto.UsuarioIncModel;
import com.altec.bsbr.app.hyb.web.util.XHYUsuarioIncService;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAcoesOs.XHYAcoesOsEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYPesquisa.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYPesquisa.XHYPesquisaEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;

@Component("pesquisaOsBean")
@Scope("session")
public class PesquisaOsBean extends BasicBBean {
	
	private static final Logger logger = LoggerFactory.getLogger(PesquisaOsBean.class);

	private List<PesquisaOsResultadoModel> objRsPesquisaOsResultado;
	private List<Hyb_excel_pesquisa_resultado_model> objRsPesquisaOsExcelResultado;

	//Datepickers
	private Date txtDataPocIni;
	private Date txtDataPocFin;
	private Date txtDataEventoIni;
	private Date txtDataEventoFin;
	private Date txtPeriPesqIni;
	private Date txtPeriPesqFin;

	private String txtNrSeqOs;
	private String txtCustoOsIni;
	private String txtCustoOsFin;
	private String txtCpfEnvolvido;
	private String txtNomeEnvolvido;
	private String txtMatrAnDesig;
	private String txtNomeAnDesig;

	private String tipoDocumento;
	private List<DropDownPesqOSModel> cboSituacaoOs; // exibe
	private List<DropDownPesqOSModel> cboFaseOs;
	private List<DropDownPesqOSModel> cboCriticidadeOs;
	private List<DropDownPesqOSModel> cboPenalidade;
	private List<DropDownPesqOSModel> cboMotivoPenalidade;
	private List<DropDownPesqOSModel> cboSituacaoRec;
	private List<DropDownPesqOSModel> cboCanalOrigem;
	private List<DropDownPesqOSModel> cboEvento;
	private List<DropDownPesqOSModel> cboCanal;
	private List<DropDownPesqOSModel> cboPCorp;
	private List<DropDownPesqOSModel> cboPAux;
	
	private String cboSituacaoOsSelecionada; // trabalha com o valor selecionado
	private String cboFaseOsSelecionada;
	private String cboCriticidadeOsSelecionada;
	private String cboPenalidadeSelecionada;
	private String cboMotivoPenalidadeSelecionada;
	private String cboSituacaoRecSelecionada;
	private String cboCanalOrigemSelecionada;
	private String cboEventoSelecionada;
	private String cboCanalSelecionada;
	private String cboPCorpSelecionada;
	private String cboPAuxSelecionada;

	private boolean chkInteresseCom = false;
	private boolean chkRessarcimento = false;

	private String txtTpPv;
	private String txtCdPv;
	private String txtNmPv;
	private String txtTextoAbertura;
	private String txtCidade;
	private String txtEstado;
	private String txtCdDepto;
	private String txtNmDetpo;
	private String txtSituOs = "";
	
	//Datas, tipo string, para formatação e busca no WS
	private String strPeriPesqIni; 
	private String strPeriPesqFin;
	private String strDataEventoIni;
	private String strDataEventoFin;
	private String strDataPocIni; 
	private String strDataPocFin;
	
	private String msgErroPermissao = "Permissão de acesso à esta Ordem de Serviço negada!";
	private UsuarioIncModel dadosUser;
	
	private ExcelOptions excelOptions;
		
	@Autowired
	private XHYPesquisaEndPoint pesquisa;
	
	//Session
	@Autowired
	private XHYUsuarioIncService user;
	
	//Permissao equipe
	@Autowired
	private XHYAcoesOsEndPoint acoes;

	@PostConstruct
	public void init() {
		System.out.println("PesquisaOsBean init!");
		this.objRsPesquisaOsResultado = new ArrayList<PesquisaOsResultadoModel>();
		this.objRsPesquisaOsExcelResultado = new ArrayList<Hyb_excel_pesquisa_resultado_model>();
		consultarSituacaoOS();
		consultarFaseAnaliseOS();
		consultarCriticidadeOs();
		consultarPenalidade();
		consultarMotivoPenalidade();
		consultarSituacaoRecurso();
		consultarCanalOrigem();
		consultarCanal();
		consultarEvento();
		consultarPCorp();
		consultarPAux();
		
		excelCustomizationOptions();
		
		if (user != null) {
			System.out.println("user not null");			
			try {
				dadosUser = user.getDadosUsuario();
				System.out.println("CPF: "+dadosUser.getCpfUsuario());
				System.out.println("Nome: "+dadosUser.getNomeUsuario());
				System.out.println("Código do cargo: "+dadosUser.getCdCargoUsuario());
				System.out.println("Código de area: "+dadosUser.getCdAreaUsuario());
				System.out.println("Nome área: "+dadosUser.getNomeAreaUsuario());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	public String pesquisar() {
		try {											
			objRsPesquisaOsResultado.clear();		
			
			//Tratar valores
			txtNrSeqOs = txtNrSeqOs == null ? "" : txtNrSeqOs.trim();
			cboSituacaoOsSelecionada = cboSituacaoOsSelecionada == null ? "" : cboSituacaoOsSelecionada.trim();
			cboFaseOsSelecionada = cboFaseOsSelecionada == null ? "": cboFaseOsSelecionada.trim(); 
			cboCriticidadeOsSelecionada = cboCriticidadeOsSelecionada == null ? "" : cboCriticidadeOsSelecionada.trim();
			txtCustoOsIni = txtCustoOsIni == null ? "" : txtCustoOsIni.trim();
			txtCpfEnvolvido = txtCpfEnvolvido == null ? "" : txtCpfEnvolvido.trim();
			txtNomeEnvolvido = txtNomeEnvolvido == null ? "" : txtNomeEnvolvido.trim();
			cboPenalidadeSelecionada = cboPenalidadeSelecionada == null ? "" : cboPenalidadeSelecionada.trim();
			cboMotivoPenalidadeSelecionada = cboMotivoPenalidadeSelecionada == null ? "" : cboMotivoPenalidadeSelecionada.trim();
			txtMatrAnDesig = txtMatrAnDesig == null ? "" : txtMatrAnDesig.trim();
			txtNomeAnDesig = txtNomeAnDesig == null ? "" : txtNomeAnDesig.trim();
			cboSituacaoRecSelecionada = cboSituacaoRecSelecionada == null ? "" : cboSituacaoRecSelecionada.trim();
			cboCanalOrigemSelecionada = cboCanalOrigemSelecionada == null ? "" : cboCanalOrigemSelecionada.trim();
			cboEventoSelecionada = cboEventoSelecionada == null ? "" : cboEventoSelecionada.trim();
			cboCanalSelecionada = cboCanalSelecionada == null ? "" : cboCanalSelecionada.trim();
			cboPCorpSelecionada = cboPCorpSelecionada == null ? "" : cboPCorpSelecionada.trim();
			cboPAuxSelecionada = cboPAuxSelecionada == null ? "" : cboPAuxSelecionada.trim();
			strPeriPesqIni = txtPeriPesqIni == null ? "" : this.convertData(txtPeriPesqIni);
			strPeriPesqFin = txtPeriPesqFin == null ? "" : this.convertData(txtPeriPesqFin);
			strDataEventoIni = txtDataEventoIni == null ? "" : this.convertData(txtDataEventoIni);
			strDataEventoFin = txtDataEventoFin == null ? "" : this.convertData(txtDataEventoFin);
			strDataPocIni = txtDataPocIni == null ? "" : this.convertData(txtDataPocIni);
			strDataPocFin = txtDataPocFin == null ? "" : this.convertData(txtDataPocFin);
			txtCidade = txtCidade == null ? "" : txtCidade.trim();
			txtCdDepto = txtCdDepto == null ? "" : txtCdDepto.trim();
			txtNmDetpo = txtNmDetpo == null ? "" : txtNmDetpo.trim();
			txtTpPv = txtTpPv == null ? "" : txtTpPv.trim();
			txtNmPv = txtNmPv == null ? "" : txtNmPv.trim();
			txtTextoAbertura = txtTextoAbertura == null ? "" : txtTextoAbertura.trim();	
			
			System.out.println("InterCom "+(this.chkInteresseCom ? "1" : ""));
			System.out.println("ress "+(this.chkRessarcimento? "1" : ""));
			
			String retorno = "";		

			//parametros vazios (respectivamente): NmRecOs  
			retorno = pesquisa.pesquisarOs(txtNrSeqOs, cboSituacaoOsSelecionada, cboFaseOsSelecionada, cboCriticidadeOsSelecionada, txtCustoOsIni,
					txtCustoOsFin, txtCpfEnvolvido, txtNomeEnvolvido, txtMatrAnDesig, "" , 
					cboSituacaoRecSelecionada, cboPenalidadeSelecionada, cboMotivoPenalidadeSelecionada, cboCanalOrigemSelecionada, cboEventoSelecionada,
					cboCanalSelecionada, cboPCorpSelecionada, cboPAuxSelecionada, this.chkInteresseCom ? "1" : "", this.chkRessarcimento ? "1" : "", strPeriPesqIni,
					strPeriPesqFin, strDataEventoIni, strDataEventoFin, strDataPocIni, strDataPocFin, txtCidade,
					txtEstado, txtNmDetpo, txtTpPv, txtCdPv, txtNmPv, txtTextoAbertura);

			System.out.println("retorno pesquisar" + retorno);
	
			JSONObject jsonRetorno = new JSONObject(retorno);
			JSONArray pcursor = jsonRetorno.getJSONArray("PCURSOR");
	
			for (int i = 0; i < pcursor.length(); i++) {
				JSONObject curr = pcursor.getJSONObject(i);
				objRsPesquisaOsResultado.add(new PesquisaOsResultadoModel(
						curr.get("CODIGO").toString() == null ? "" : curr.get("CODIGO").toString(),
						curr.get("CODIGO_SITUACAO").toString() == null ? "" : curr.get("CODIGO_SITUACAO").toString(),
						curr.get("CODIGO_AREA").toString() == null ? "" : curr.get("CODIGO_AREA").toString(),
						curr.get("SITUACAO").toString() == null ? "" : curr.get("SITUACAO").toString(),
						curr.get("DATA_ABERTURA").toString() == null ? "" : curr.get("DATA_ABERTURA").toString(),
						curr.get("AREA").toString() == null ? "" : curr.get("AREA").toString()));
			}
	
		} catch (Exception e) {
			e.printStackTrace();
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
	         } catch (IOException e1) {
	               // TODO Auto-generated catch block
	               e1.printStackTrace();
	         }
		}
		
		return "hyb_pesquisaos_resultado.xhtml?faces-redirect=true";	
	}
	
	public void pesquisarExcel(ActionEvent event) {
		try {
			String retorno = "";	
			this.objRsPesquisaOsExcelResultado.clear();

			//parametros vazios (respectivamente): NmRecOs, InterCom, Ress  
			retorno = pesquisa.pesquisarOsExcel(txtNrSeqOs, cboSituacaoOsSelecionada, cboFaseOsSelecionada, cboCriticidadeOsSelecionada, txtCustoOsIni,
					txtCustoOsFin, txtCpfEnvolvido, txtNomeEnvolvido, txtMatrAnDesig, "" , 
					cboSituacaoRecSelecionada, cboPenalidadeSelecionada, cboMotivoPenalidadeSelecionada, cboCanalOrigemSelecionada, cboEventoSelecionada,
					cboCanalSelecionada, cboPCorpSelecionada, cboPAuxSelecionada, this.chkInteresseCom ? "1" : "", this.chkRessarcimento ? "1" : "", strPeriPesqIni,
					strPeriPesqFin, strDataEventoIni, strDataEventoFin, strDataPocIni, strDataPocFin, txtCidade,
					txtEstado, txtNmDetpo, txtTpPv, txtCdPv, txtNmPv, txtTextoAbertura);

			System.out.println("retorno pesquisar excel" + retorno);
	
			JSONObject jsonRetorno = new JSONObject(retorno);
			JSONArray pcursor = jsonRetorno.getJSONArray("PCURSOR");
	
			for (int i = 0; i < pcursor.length(); i++) {
				JSONObject curr = pcursor.getJSONObject(i);
				this.objRsPesquisaOsExcelResultado.add(new Hyb_excel_pesquisa_resultado_model(curr.get("CODIGO").toString() == null ? "" : curr.get("CODIGO").toString(), 
								curr.get("CD_SITUACAO").toString() == null ? "" : curr.get("CD_SITUACAO").toString(), 
								curr.get("NM_SITUACAO").toString() == null ? "" : curr.get("NM_SITUACAO").toString(), 
								curr.get("ABERTURA").toString() == null ? "" : curr.get("ABERTURA").toString(), 
								curr.get("AREA").toString() == null ? "" : curr.get("AREA").toString(), 
								curr.get("NOME_AN_DESIG").toString() == null ? "" : curr.get("NOME_AN_DESIG").toString(), 
								curr.get("VALOR_ENVOLVIDO").toString() == null ? "" : curr.get("VALOR_ENVOLVIDO").toString(), 
								curr.get("PREJUIZO").toString() == null ? "" : curr.get("PREJUIZO").toString(), 
								curr.get("VALOR_RECUP").toString() == null ? "" : curr.get("VALOR_RECUP").toString(), 
								curr.get("CUSTO_OS").toString() == null ? "" : curr.get("CUSTO_OS").toString(), 
								curr.get("NM_PV").toString() == null ? "" : curr.get("NM_PV").toString(), 
//								curr.get("NM_REGI").toString() == null ? "" : curr.get("NM_REGI").toString(),
								"",		
								curr.get("NM_REDE").toString() == null ? "" : curr.get("NM_REDE").toString(), 
								curr.get("EVENTO").toString() == null ? "" : curr.get("EVENTO").toString(), 
								curr.get("CANAL").toString() == null ? "" : curr.get("CANAL").toString(), 
								curr.get("OPERACAO").toString() == null ? "" : curr.get("OPERACAO").toString()));
			}
			System.out.println("Size objRsPesquisaOsExcelResultado "+objRsPesquisaOsExcelResultado.size());
			
			if (objRsPesquisaOsExcelResultado.size() <= 0) {
				RequestContext.getCurrentInstance().execute("alert('Não existe dados para esta consulta!');");
			}
			else {
				RequestContext.getCurrentInstance().execute("document.getElementById('frmPesquisaResultado:excelExporter').click(); return false;");
			}
			
	
		} catch (Exception e) {
			e.printStackTrace();
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
	         } catch (IOException e1) {
	               // TODO Auto-generated catch block
	               e1.printStackTrace();
	         }
		}
	}
	
	public void openWindow(String page, String pageId, String beanName) {
		try {
			//RequestContext.getCurrentInstance().execute("window.open('"+page+"', '"+pageId+"', 'dialogHeight:250px;dialogWidth:530px; center: Yes; resizable: no; status: No;');"); 
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove(beanName);
			RequestContext.getCurrentInstance().execute("setDialogDispatch('"+page+"');");
		} 
		catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	public void changePage(String page) {
		try {
			
			/*System.out.println("cadOsMenuBean remove "+FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("CadOsMenuBean"));
			if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("CadOsMenuBean") != null)
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CadOsMenuBean");*/
			
			RequestContext.getCurrentInstance().execute("parent.location.href='"+page+"';");
		} 
		catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	public void iniciaMsgErro() {
		this.msgErroPermissao =  "Permissão de acesso à esta Ordem de Serviço negada!";
	}
	
	public Boolean verificaPermissaoEquipe(UsuarioIncModel usuario, String nrOs) {
		Boolean ret = false;
		String retornoAcoes = "";
		JSONArray pCursor = null;
		
		try {
			retornoAcoes = acoes.consultarEquipeOs(nrOs, "");
			JSONObject consulta = new JSONObject(retornoAcoes);
			pCursor = consulta.getJSONArray("PCURSOR");
			System.out.println("retornoAcoes verificaPermissaoEquipe "+retornoAcoes);
		} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYAcoesOs.WebServiceException e) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
	         } catch (IOException e1) {
	               // TODO Auto-generated catch block
	               e1.printStackTrace();
	         }
		}
	
		if (retornoAcoes.toUpperCase().contains("XHY")) {
			ret = false;
			RequestContext.getCurrentInstance().execute("location.href=hyb_erro.xhtml?strErro="+retornoAcoes);
		}

		if (!usuario.getCdCargoUsuario().equals("B70555")) {
			for (int i = 0; i < pCursor.length(); i++) 
				if(pCursor.getJSONObject(i).getString("CPF").equals(usuario.getCpfUsuario()))
					ret = true;
		} 
		else {
			ret = true;
		}
				
		if(!ret) 
			this.msgErroPermissao += "\n - Usuário não faz parte da equipe atribuída à OS.";
		
		return ret;
	}
	
	public Boolean verificaPermissaoArea(UsuarioIncModel usuario, String cdArea) {
		Boolean ret = false;
		if (!usuario.getCdCargoUsuario().equals("B70555")) {
			System.out.println("verificaPermissaoArea !cdCargoUsuario.equals");
			if(!cdArea.equals(usuario.getCdAreaUsuario())) {
				System.out.println("verificaPermissaoArea !cdArea.equals (cdarea "+cdArea+")");
				this.msgErroPermissao += " - Área do usuário diferente da área da Ordem de Serviço";
				ret = false;
			} 
			else {
				ret = true;
			}			
		}
		else {
			ret = true;
		}
		
		return ret;		
	}
	
	public Boolean verificaPermissao(String nrOs, String areaOs) {
		if(!this.verificaPermissaoArea(dadosUser, areaOs) || 
		   !this.verificaPermissaoEquipe(dadosUser, nrOs)) {
			System.out.println("verificaPermissao false");
			logger.info("Tentativa de acesso indevido à ordem de serviço nº "+nrOs+" - CPF: "+dadosUser.getCpfUsuario());
			RequestContext.getCurrentInstance().execute("alert('"+this.msgErroPermissao+"');");
			this.iniciaMsgErro();
			return false;
		}
		else {
			System.out.println("verificaPermissao true");
			this.iniciaMsgErro();
			return true;
		}
	}
	
	public void excluir() {
		String nrOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("nrOs").toString();
		String areaOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("areaOs").toString();
		
		if(this.verificaPermissao(nrOs, areaOs)) 
			this.openWindow("hyb_excluiros.xhtml?strNrSeqOs="+nrOs, "excluirOs", "excluirOSBean");
	}
	
	public void imprimirOs() {
		String nrOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("nrOs").toString();		
		this.openWindow("hyb_imprimiros.xhtml?strNrSeqOs="+nrOs, "imprimirOs", "imprimirOS");
	}
	
	public void alterar() {
		String nrOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("nrOs").toString();
		String areaOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("areaOs").toString();
		//txtSituOs = CODIGO_SITUACAO da OS selecionada
		this.txtSituOs = objRsPesquisaOsResultado.stream()
												 .filter(osresult -> osresult.getCODIGO().equals(nrOs))
												 .collect(Collectors.toList())
												 .iterator()
												 .next()
												 .getCODIGO_SITUACAO();
		
		System.out.println("CODIGO_SITUACAO SELECIONADO "+this.txtSituOs);
		System.out.println("OS SELECIONADA (ALTERAR) "+nrOs);
		
		if(dadosUser.getCdCargoUsuario().equals("B70555")) {
			if(this.verificaPermissao(nrOs, areaOs)) 
				this.changePage("hyb_cadosmenu.xhtml?strNrSeqOs="+nrOs+"&strReadOnly=0&strTitulo=Alterar OS");

		}
		else {
			if("E".equals(this.txtSituOs)) {
				iniciaMsgErro();
				RequestContext.getCurrentInstance().execute("alert('"+this.msgErroPermissao+"');");
			}
			else {
				if(this.verificaPermissao(nrOs, areaOs)) 
					this.changePage("hyb_cadosmenu.xhtml?strNrSeqOs="+nrOs+"&strReadOnly=0&strTitulo=Alterar OS");
			}
		}
				
	}
	
	public void aprovarOs() {
		String nrOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("nrOs").toString();
		String areaOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("areaOs").toString();
		if(this.verificaPermissao(nrOs, areaOs)) 
			this.openWindow("hyb_aprovaros.xhtml?strNrSeqOs="+nrOs, "aprovarOs", "aprovarOSIframeBean");	
	}
	
	public void encerrarOs() {
		String nrOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("nrOs").toString();
		String areaOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("areaOs").toString();
		if(this.verificaPermissao(nrOs, areaOs)) 
			this.changePage("hyb_encerrarosmenu.xhtml?strNrSeqOs="+nrOs);
	}
	
	public void mostrarStatusOs() {
		String nrOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("nrOs").toString();
		String areaOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("areaOs").toString();
		
		if(dadosUser.getCdCargoUsuario().equals("B70555")) {
			if(this.verificaPermissao(nrOs, areaOs)) 
				this.openWindow("hyb_AlterarStatusOs.xhtml?strNrSeqOs="+nrOs, "statusOs", "alterarStatusOs");			
		}
		else {
			this.iniciaMsgErro();
			RequestContext.getCurrentInstance().execute("alert('"+this.msgErroPermissao+"');");
		}			
	}

	public void mostrarCustoOs() {
		String nrOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("nrOs").toString();
		String areaOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("areaOs").toString();
		if(this.verificaPermissao(nrOs, areaOs)) 
			this.changePage("hyb_CustoOS.xhtml?strNrSeqOs="+nrOs);
	}

	public void mostrarCapaOs() {
		String nrOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("nrOs").toString();
		this.openWindow("hyb_RelCapaOS.xhtml?strNrSeqOs="+nrOs, "capaOs", "relCapaOSBean");		
	}

	public void mostrarCartaAprOs() {
		String nrOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("nrOs").toString();
		this.openWindow("hyb_RelCartaApresentacao.xhtml?strNrSeqOs="+nrOs, "cartaAPrOs", "relCartaApresentacaoBean");		
	}

	public void mostrarMinutaOs() {
		String nrOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("nrOs").toString();
		this.openWindow("hyb_RelMinuta.xhtml?strNrSeqOs="+nrOs, "minutaOs", "relMinutaBean");		
	}

	public void mostrarRastreabilidade() {
		String nrOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("nrOs").toString();
		String areaOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("areaOs").toString();
		if(this.verificaPermissao(nrOs, areaOs)) 
			this.openWindow("hyb_Rastreabilidade.xhtml?strNrSeqOs="+nrOs, "rastreabilidadeOs", "hybRastreabilidadeBean");
	}
	
	public void mostrarDetalheOs() {
		String strNrOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("CODIGO").toString();
		String CODIGO_AREA = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("CODIGO_AREA").toString();
		System.out.println("detalhes:: "+strNrOs+" "+CODIGO_AREA);		
		this.changePage("hyb_CadOsMenu.xhtml?strNrSeqOs=" + strNrOs + "&strReadOnly=1" + "&strTitulo=Detalhes OS");
	}
	
	public void excelCustomizationOptions() {
		excelOptions = new ExcelOptions();
		excelOptions.setFacetBgColor("#FF0000");
		excelOptions.setFacetFontColor("#FFFFFF");
	}
	
	public void setValueAndTypeCell(HSSFCell cell, HSSFCellStyle currencyStyle, HSSFCellStyle dateStyle) {
	    try {
	    	// Tipo de estilo da célula (para formatação de número ou data)
	    	switch(cell.getColumnIndex()) {
	    		case 2:
	    			cell.setCellStyle(dateStyle);
	    			break;
	    			
	    		case 5:
	    		case 6:
	    		case 7:
	    		case 8:
			        Double valor = Double.parseDouble(cell.getStringCellValue().replace(".", "").replace(",", "."));
			        cell.setCellValue(valor);
			        cell.setCellStyle(currencyStyle);		    			
	    			break;
	    			
	    		default:
	    			cell.setCellType(Cell.CELL_TYPE_STRING);
	    			if (cell != null)
			    		if ("null".equals(cell.getStringCellValue()))
			    			cell.setCellValue("");
	    			break;
	    	}		    			      
	    } catch (Exception e) {
	      e.printStackTrace();
	      cell.setCellType(Cell.CELL_TYPE_STRING);
	    }
	  }
	 
	public void postProcessExcel(Object doc) {
		HSSFWorkbook wb = (HSSFWorkbook) doc;
		HSSFSheet sheet = wb.getSheetAt(0);
		
		//Estilo de moeda (numérico)
	    HSSFCellStyle currencyStyle = wb.createCellStyle();
	    currencyStyle.setDataFormat((short) 4);
	   
	    //Estilo para data
	    HSSFCellStyle dateStyle = wb.createCellStyle();
	    dateStyle.setDataFormat((short) 14);
	    	    
	    for (int i = 1; i <= sheet.getLastRowNum(); i++) {
	        HSSFRow row = sheet.getRow(i);
	        
	        if (row != null) 
	          for (int iRow = 0; iRow < row.getLastCellNum(); iRow++) 
	            if (row.getCell(iRow) != null) 
	              this.setValueAndTypeCell(row.getCell(iRow), currencyStyle, dateStyle);           	          
        }
	}
	
//	public void reload() {
//		System.out.println("reload bean!");
//		this.objRsPesquisaOsExcelResultado.clear();
//		this.objRsPesquisaOsResultado.clear();
//		this.iniciaMsgErro();
//		System.out.println("CPF: "+dadosUser.getCpfUsuario());
//		System.out.println("Nome: "+dadosUser.getNomeUsuario());
//		System.out.println("Código do cargo: "+dadosUser.getCdCargoUsuario());
//		System.out.println("Código de area: "+dadosUser.getCdAreaUsuario());
//		System.out.println("Nome área: "+dadosUser.getNomeAreaUsuario());
//	}
	
	public String convertData(Date dataInput) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return dataInput != null ? formatter.format(dataInput) : ""; 		
	}	
		
	public void consultarSituacaoOS() {
		cboSituacaoOs = new ArrayList<DropDownPesqOSModel>();
		try {
			String retorno = pesquisa.consultarSituacaoOs();

			JSONObject consultaArea = new JSONObject(retorno);
			JSONArray pCursorArea = consultaArea.getJSONArray("PCURSOR");

			for (int i = 0; i < pCursorArea.length(); i++) {
				JSONObject f = pCursorArea.getJSONObject(i);

				cboSituacaoOs.add(new DropDownPesqOSModel(f.get("CODIGO").toString(), f.get("NOME").toString()));

			} // close for

		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			System.out.println("Error during call consultarSituacaoOS");
			e.printStackTrace();
		}
	}

	public void consultarFaseAnaliseOS() {

		cboFaseOs = new ArrayList<DropDownPesqOSModel>();

		try {
			String retorno = pesquisa.consultarFaseAnaliseOs();

			JSONObject consultaArea = new JSONObject(retorno);
			JSONArray pCursorArea = consultaArea.getJSONArray("PCURSOR");

			for (int i = 0; i < pCursorArea.length(); i++) {
				JSONObject f = pCursorArea.getJSONObject(i);

				cboFaseOs.add(new DropDownPesqOSModel(f.get("CODIGO").toString(), f.get("NOME").toString()));

			} // close for

		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			System.out.println("Error during call consultarFaseAnaliseOS");
			e.printStackTrace();
		}

	}

	public void consultarCriticidadeOs() {

		cboCriticidadeOs = new ArrayList<DropDownPesqOSModel>();

		try {
			String retorno = pesquisa.consultarCriticidadeOs();

			JSONObject consultaArea = new JSONObject(retorno);
			JSONArray pCursorArea = consultaArea.getJSONArray("PCURSOR");

			for (int i = 0; i < pCursorArea.length(); i++) {
				JSONObject f = pCursorArea.getJSONObject(i);

				cboCriticidadeOs.add(new DropDownPesqOSModel(f.get("CODIGO").toString(), f.get("NOME").toString()));

			} // close for

		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			System.out.println("Error during call consultarCriticidadeOs");
			e.printStackTrace();
		}

	}

	public void consultarPenalidade() {

		cboPenalidade = new ArrayList<DropDownPesqOSModel>();

		try {
			String retorno = pesquisa.consultarPenalidade();

			JSONObject consultaArea = new JSONObject(retorno);
			JSONArray pCursorArea = consultaArea.getJSONArray("PCURSOR");

			for (int i = 0; i < pCursorArea.length(); i++) {
				JSONObject f = pCursorArea.getJSONObject(i);

				cboPenalidade.add(new DropDownPesqOSModel(f.get("CODIGO").toString(), f.get("NOME").toString()));
			} // close for

		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			System.out.println("Error during call consultarPenalidade");
			e.printStackTrace();
		}

	}

	public void consultarMotivoPenalidade() {

		cboMotivoPenalidade = new ArrayList<DropDownPesqOSModel>();

		try {
			String retorno = pesquisa.consultarMotivoPenalidade();

			JSONObject consultaArea = new JSONObject(retorno);
			JSONArray pCursorArea = consultaArea.getJSONArray("PCURSOR");

			for (int i = 0; i < pCursorArea.length(); i++) {
				JSONObject f = pCursorArea.getJSONObject(i);

				cboMotivoPenalidade.add(new DropDownPesqOSModel(f.get("CODIGO").toString(), f.get("NOME").toString()));

			} // close for

		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			System.out.println("Error during call consultarMotivoPenalidade");
			e.printStackTrace();
		}

	}

	public void consultarSituacaoRecurso() {

		cboSituacaoRec = new ArrayList<DropDownPesqOSModel>();

		try {
			String retorno = pesquisa.consultarSituacaoRecurso();

			JSONObject consultaArea = new JSONObject(retorno);
			JSONArray pCursorArea = consultaArea.getJSONArray("PCURSOR");

			for (int i = 0; i < pCursorArea.length(); i++) {
				JSONObject f = pCursorArea.getJSONObject(i);

				cboSituacaoRec.add(new DropDownPesqOSModel(f.get("CODIGO").toString(), f.get("NOME").toString()));

			} // close for

		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			System.out.println("Error during call consultarSituacaoRecurso");
			e.printStackTrace();
		}

	}

	public void consultarCanalOrigem() {

		cboCanalOrigem = new ArrayList<DropDownPesqOSModel>();

		try {
			String retorno = pesquisa.consultarCanalOrigem();

			JSONObject consultaArea = new JSONObject(retorno);
			JSONArray pCursorArea = consultaArea.getJSONArray("PCURSOR");

			for (int i = 0; i < pCursorArea.length(); i++) {
				JSONObject f = pCursorArea.getJSONObject(i);

				cboCanalOrigem.add(new DropDownPesqOSModel(f.get("CODIGO").toString(), f.get("NOME").toString()));

			} // close for

		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void consultarEvento() {

		cboEvento = new ArrayList<DropDownPesqOSModel>();

		try {
			String retorno = pesquisa.consultarEvento();

			JSONObject consultaArea = new JSONObject(retorno);
			JSONArray pCursorArea = consultaArea.getJSONArray("PCURSOR");

			for (int i = 0; i < pCursorArea.length(); i++) {
				JSONObject f = pCursorArea.getJSONObject(i);

				cboEvento.add(new DropDownPesqOSModel(f.get("CODIGO").toString(), f.get("NOME").toString()));

			} // close for

		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void consultarCanal() {

		cboCanal = new ArrayList<DropDownPesqOSModel>();

		try {
			String retorno = pesquisa.consultarCanal();

			JSONObject consultaArea = new JSONObject(retorno);
			JSONArray pCursorArea = consultaArea.getJSONArray("PCURSOR");

			for (int i = 0; i < pCursorArea.length(); i++) {
				JSONObject f = pCursorArea.getJSONObject(i);

				cboCanal.add(new DropDownPesqOSModel(f.get("CODIGO").toString(), f.get("NOME").toString()));

			} // close for

		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void consultarPCorp() {

		cboPCorp = new ArrayList<DropDownPesqOSModel>();

		try {
			String retorno = pesquisa.consultarPCorp();

			JSONObject consultaArea = new JSONObject(retorno);
			JSONArray pCursorArea = consultaArea.getJSONArray("PCURSOR");

			for (int i = 0; i < pCursorArea.length(); i++) {
				JSONObject f = pCursorArea.getJSONObject(i);

				cboPCorp.add(new DropDownPesqOSModel(f.get("CODIGO").toString(), f.get("NOME").toString()));

			} // close for

		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void consultarPAux() {

		cboPAux = new ArrayList<DropDownPesqOSModel>();

		try {
			String retorno = pesquisa.consultarPAux();

			JSONObject consultaArea = new JSONObject(retorno);
			JSONArray pCursorArea = consultaArea.getJSONArray("PCURSOR");

			for (int i = 0; i < pCursorArea.length(); i++) {
				JSONObject f = pCursorArea.getJSONObject(i);

				cboPAux.add(new DropDownPesqOSModel(f.get("CODIGO").toString(), f.get("NOME").toString()));

			} // close for

		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public Date getTxtDataEventoFin() {
		return txtDataEventoFin;
	}

	public void setTxtDataEventoFin(Date txtDataEventoFin) {
		this.txtDataEventoFin = txtDataEventoFin;
	}

	public Date getTxtDataPocIni() {
		return txtDataPocIni;
	}

	public void setTxtDataPocIni(Date txtDataPocIni) {
		this.txtDataPocIni = txtDataPocIni;
	}

	public Date getTxtDataPocFin() {
		return txtDataPocFin;
	}

	public void setTxtDataPocFin(Date txtDataPocFin) {
		this.txtDataPocFin = txtDataPocFin;
	}

	public Date getTxtDataEventoIni() {
		return txtDataEventoIni;
	}

	public void setTxtDataEventoIni(Date txtDataEventoIni) {
		this.txtDataEventoIni = txtDataEventoIni;
	}

	public Date getTxtPeriPesqIni() {
		return txtPeriPesqIni;
	}

	public void setTxtPeriPesqIni(Date txtPeriPesqIni) {
		this.txtPeriPesqIni = txtPeriPesqIni;
	}

	public Date getTxtPeriPesqFin() {
		return txtPeriPesqFin;
	}

	public void setTxtPeriPesqFin(Date txtPeriPesqFin) {
		this.txtPeriPesqFin = txtPeriPesqFin;
	}

	public boolean isChkInteresseCom() {
		return chkInteresseCom;
	}

	public void setChkInteresseCom(boolean chkInteresseCom) {
		this.chkInteresseCom = chkInteresseCom;
	}

	public boolean isChkRessarcimento() {
		return chkRessarcimento;
	}

	public void setChkRessarcimento(boolean chkRessarcimento) {
		this.chkRessarcimento = chkRessarcimento;
	}

	public List<DropDownPesqOSModel> getCboSituacaoOs() {
		return cboSituacaoOs;
	}

	public void setCboSituacaoOs(List<DropDownPesqOSModel> cboSituacaoOs) {
		this.cboSituacaoOs = cboSituacaoOs;
	}

	public List<DropDownPesqOSModel> getCboFaseOs() {
		return cboFaseOs;
	}

	public void setCboFaseOs(List<DropDownPesqOSModel> cboFaseOs) {
		this.cboFaseOs = cboFaseOs;
	}

	public List<DropDownPesqOSModel> getCboCriticidadeOs() {
		return cboCriticidadeOs;
	}

	public void setCboCriticidadeOs(List<DropDownPesqOSModel> cboCriticidadeOs) {
		this.cboCriticidadeOs = cboCriticidadeOs;
	}

	public List<DropDownPesqOSModel> getCboPenalidade() {
		return cboPenalidade;
	}

	public void setCboPenalidade(List<DropDownPesqOSModel> cboPenalidade) {
		this.cboPenalidade = cboPenalidade;
	}

	public List<DropDownPesqOSModel> getCboMotivoPenalidade() {
		return cboMotivoPenalidade;
	}

	public void setCboMotivoPenalidade(List<DropDownPesqOSModel> cboMotivoPenalidade) {
		this.cboMotivoPenalidade = cboMotivoPenalidade;
	}

	public List<DropDownPesqOSModel> getCboSituacaoRec() {
		return cboSituacaoRec;
	}

	public void setCboSituacaoRec(List<DropDownPesqOSModel> cboSituacaoRec) {
		this.cboSituacaoRec = cboSituacaoRec;
	}

	public List<DropDownPesqOSModel> getCboCanalOrigem() {
		return cboCanalOrigem;
	}

	public void setCboCanalOrigem(List<DropDownPesqOSModel> cboCanalOrigem) {
		this.cboCanalOrigem = cboCanalOrigem;
	}

	public List<DropDownPesqOSModel> getCboEvento() {
		return cboEvento;
	}

	public void setCboEvento(List<DropDownPesqOSModel> cboEvento) {
		this.cboEvento = cboEvento;
	}

	public List<DropDownPesqOSModel> getCboCanal() {
		return cboCanal;
	}

	public void setCboCanal(List<DropDownPesqOSModel> cboCanal) {
		this.cboCanal = cboCanal;
	}

	public List<DropDownPesqOSModel> getCboPCorp() {
		return cboPCorp;
	}

	public void setCboPCorp(List<DropDownPesqOSModel> cboPCorp) {
		this.cboPCorp = cboPCorp;
	}

	public List<DropDownPesqOSModel> getCboPAux() {
		return cboPAux;
	}

	public void setCboPAux(List<DropDownPesqOSModel> cboPAux) {
		this.cboPAux = cboPAux;
	}

	public String getTxtNrSeqOs() {
		return txtNrSeqOs;
	}

	public void setTxtNrSeqOs(String txtNrSeqOs) {
		this.txtNrSeqOs = txtNrSeqOs;
	}

	public String getTxtCustoOsIni() {
		return txtCustoOsIni;
	}

	public void setTxtCustoOsIni(String txtCustoOsIni) {
		this.txtCustoOsIni = txtCustoOsIni;
	}

	public String getTxtCustoOsFin() {
		return txtCustoOsFin;
	}

	public void setTxtCustoOsFin(String txtCustoOsFin) {
		this.txtCustoOsFin = txtCustoOsFin;
	}

	public String getTxtCpfEnvolvido() {
		return txtCpfEnvolvido;
	}

	public void setTxtCpfEnvolvido(String txtCpfEnvolvido) {
		this.txtCpfEnvolvido = txtCpfEnvolvido;
	}

	public String getTxtNomeEnvolvido() {
		return txtNomeEnvolvido;
	}

	public void setTxtNomeEnvolvido(String txtNomeEnvolvido) {
		this.txtNomeEnvolvido = txtNomeEnvolvido;
	}

	public String getTxtMatrAnDesig() {
		return txtMatrAnDesig;
	}

	public void setTxtMatrAnDesig(String txtMatrAnDesig) {
		this.txtMatrAnDesig = txtMatrAnDesig;
	}

	public String getTxtNomeAnDesig() {
		return txtNomeAnDesig;
	}

	public void setTxtNomeAnDesig(String txtNomeAnDesig) {
		this.txtNomeAnDesig = txtNomeAnDesig;
	}

	public String getCboSituacaoOsSelecionada() {
		return cboSituacaoOsSelecionada;
	}

	public void setCboSituacaoOsSelecionada(String cboSituacaoOsSelecionada) {
		this.cboSituacaoOsSelecionada = cboSituacaoOsSelecionada.split("=")[0];
	}

	public String getCboFaseOsSelecionada() {
		return cboFaseOsSelecionada;
	}

	public void setCboFaseOsSelecionada(String cboFaseOsSelecionada) {
		this.cboFaseOsSelecionada = cboFaseOsSelecionada.split("=")[0];
	}

	public String getCboCriticidadeOsSelecionada() {
		return cboCriticidadeOsSelecionada;
	}

	public void setCboCriticidadeOsSelecionada(String cboCriticidadeOsSelecionada) {
		this.cboCriticidadeOsSelecionada = cboCriticidadeOsSelecionada.split("=")[0];
	}

	public String getCboPenalidadeSelecionada() {
		return cboPenalidadeSelecionada;
	}

	public void setCboPenalidadeSelecionada(String cboPenalidadeSelecionada) {
		this.cboPenalidadeSelecionada = cboPenalidadeSelecionada.split("=")[0];
	}

	public String getCboMotivoPenalidadeSelecionada() {
		return cboMotivoPenalidadeSelecionada;
	}

	public void setCboMotivoPenalidadeSelecionada(String cboMotivoPenalidadeSelecionada) {
		this.cboMotivoPenalidadeSelecionada = cboMotivoPenalidadeSelecionada.split("=")[0];
	}

	public String getCboSituacaoRecSelecionada() {
		return cboSituacaoRecSelecionada;
	}

	public void setCboSituacaoRecSelecionada(String cboSituacaoRecSelecionada) {
		this.cboSituacaoRecSelecionada = cboSituacaoRecSelecionada.split("=")[0];
	}

	public String getCboCanalOrigemSelecionada() {
		return cboCanalOrigemSelecionada;
	}

	public void setCboCanalOrigemSelecionada(String cboCanalOrigemSelecionada) {
		this.cboCanalOrigemSelecionada = cboCanalOrigemSelecionada.split("=")[0];
	}

	public String getCboEventoSelecionada() {
		return cboEventoSelecionada;
	}

	public void setCboEventoSelecionada(String cboEventoSelecionada) {
		this.cboEventoSelecionada = cboEventoSelecionada.split("=")[0];
	}

	public String getCboCanalSelecionada() {
		return cboCanalSelecionada;
	}

	public void setCboCanalSelecionada(String cboCanalSelecionada) {
		this.cboCanalSelecionada = cboCanalSelecionada.split("=")[0];
	}

	public String getCboPCorpSelecionada() {
		return cboPCorpSelecionada;
	}

	public void setCboPCorpSelecionada(String cboPCorpSelecionada) {
		this.cboPCorpSelecionada = cboPCorpSelecionada.split("=")[0];
	}

	public String getCboPAuxSelecionada() {
		return cboPAuxSelecionada;
	}

	public void setCboPAuxSelecionada(String cboPAuxSelecionada) {
		this.cboPAuxSelecionada = cboPAuxSelecionada.split("=")[0];
	}

	public String getTxtTpPv() {
		return txtTpPv;
	}

	public void setTxtTpPv(String txtTpPv) {
		this.txtTpPv = txtTpPv;
	}

	public String getTxtCdPv() {
		return txtCdPv;
	}

	public void setTxtCdPv(String txtCdPv) {
		this.txtCdPv = txtCdPv;
	}

	public String getTxtNmPv() {
		return txtNmPv;
	}

	public void setTxtNmPv(String txtNmPv) {
		this.txtNmPv = txtNmPv;
	}

	public String getTxtTextoAbertura() {
		return txtTextoAbertura;
	}

	public void setTxtTextoAbertura(String txtTextoAbertura) {
		this.txtTextoAbertura = txtTextoAbertura;
	}

	public String getTxtCidade() {
		return txtCidade;
	}

	public void setTxtCidade(String txtCidade) {
		this.txtCidade = txtCidade;
	}

	public String getTxtEstado() {
		return txtEstado;
	}

	public void setTxtEstado(String txtEstado) {
		this.txtEstado = txtEstado;
	}

	public String getTxtCdDepto() {
		return txtCdDepto;
	}

	public void setTxtCdDepto(String txtCdDepto) {
		this.txtCdDepto = txtCdDepto;
	}

	public String getTxtNmDetpo() {
		return txtNmDetpo;
	}

	public void setTxtNmDetpo(String txtNmDetpo) {
		this.txtNmDetpo = txtNmDetpo;
	}

	public List<PesquisaOsResultadoModel> getObjRsPesquisaOsResultado() {
		return objRsPesquisaOsResultado;
	}

	public void setObjRsPesquisaOsResultado(List<PesquisaOsResultadoModel> objRsPesquisaOsResultado) {
		this.objRsPesquisaOsResultado = objRsPesquisaOsResultado;
	}

	public String getTxtSituOs() {
		return txtSituOs;
	}

	public void setTxtSituOs(String txtSituOs) {
		this.txtSituOs = txtSituOs;
	}

	public ExcelOptions getExcelOptions() {
		return excelOptions;
	}

	public void setExcelOptions(ExcelOptions excelOptions) {
		this.excelOptions = excelOptions;
	}

	public List<Hyb_excel_pesquisa_resultado_model> getObjRsPesquisaOsExcelResultado() {
		return objRsPesquisaOsExcelResultado;
	}

	public void setObjRsPesquisaOsExcelResultado(List<Hyb_excel_pesquisa_resultado_model> objRsPesquisaOsExcelResultado) {
		this.objRsPesquisaOsExcelResultado = objRsPesquisaOsExcelResultado;
	}
}
