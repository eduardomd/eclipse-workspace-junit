package com.altec.bsbr.app.hyb.web.jsf;

import java.io.Serializable;
import java.sql.ResultSet;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;

@ManagedBean(name="CadEventos")
@ViewScoped
public class CadastroOSEventosBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String strErro = "";
	private String strNrSeqOs = "";
	
	private String strCboParticipante; // Armazena o cudigo HTML, para carregar a combo com os participantes
	private String strCboAgenciaConta; // Armazena o cudigo HTML, para carregar a combo com Aguncia e conta
	private String strCboParticipanteNaoClie; //Armazena o cudigo HTML, para carregar a combo com os participantes nuo cliente/outros
	private String strCboParticipanteTodos; //Armazena o cudigo HTML, para carregar a combo com todos os participantes 
	private String objRsEvento;
	
	private int intEvento; // Sequencial de retorno do mutodo Gravar Evento
	private boolean blnCanal; // Boleano de retorno do mutodo Gravar Canal
	private int[] arrLinhaDetalhe; // Array que controla a Linha do Detalhe
	private int[] arrSeqDetalhe; // Array que controla o Numero de Sequencia
	private String[] arrNrDetalhe; //Array do Nr Conta ou Nr Cartuo
	private String[] arrFrCnalDetalhe;  //Array do Sequencial da Fraude Canal
	private String[] arrContaDetalhe; //Array do Sequencial da Fraude Canal
	private String[] arrAgDetalhe;	//Array do Nr da Aguncia
	private int iDetalhe; // undice do Array
	private String strRqtCboEvento; //Request.Form CboEvento
	private String strRqtCboCanal;  // Request.Form CboNivel2
	private String strRqtNmCboCanal; // Request.Form hdnCanal
	private int intApurResp; //IN_APUR_RESP - chkAssaltos
	private int intDivuIndiInfoClie; //IN_DIVU_INVI_INFO_CLIE - Request.Form("chkComportamental0")
	private int intDivuIndiInfoBanc; //IN_DIVU_INVI_INFO_BANC - Request.Form("chkComportamental1")
	private int intTrmsMesgCntdIndq; //IN_TRMS_MESG_CNTD_INDQ - Request.Form("chkComportamental2")
	private int intVendCasd; //IN_VEND_CASD - Request.Form("chkComportamental3")
	private int intVendNaoForz; //IN_VEND_NAO_FORZ - Request.Form("chkComportamental4")
	private int intFals; //IN_FALS - Request.Form("chkComportamental5")
	private String strNmOutrPratIndq; //NM_OUTR_PRAT_INDQ - Request.Form("txtComportamental")
	private int intRstrFincCadr; //IN_RSTR_FINC_CADR - Request.Form("chkComportamental6")
	private int intCdtaProfIndq; //IN_CDTA_PROF_INDQ - Request.Form("chkComportamental7")
	private int intDscmNort; //IN_DSCM_NORT - Request.Form("chkComportamental8")
	private int intViolCodiEtic; //IN_VIOL_CODI_ETIC - Request.Form("chkComportamental9")
	private int intEndiExce; //IN_ENDI_EXCE - Request.Form("chkComportamental10")
	
	private int intApprValoClie; //IN_APPR_VALO_CLIE - Request.Form("chkFraudes0")
	private int intApprValoBanc; //IN_APPR_VALO_BANC - Request.Form("chkFraudes1")
	private int intMoviFincIrre; //IN_MOVI_FINC_IRRE - Request.Form("chkFraudes2")
	private int intOperCred; //IN_OPER_CRED - Request.Form("chkFraudes3")
	private int intCntrProd; //IN_CNTR_PROD - Request.Form("chkFraudes4")
	private int intAberIrreCnta; //IN_ABER_IRRE_CNTA - Request.Form("chkFraudes5")

	private int intEmailQubrSigi; //IN_EMAIL_QUBR_SIGI - Request.Form("chkMonitoramento0")
	private int intEmailVazaInfo; //IN_EMAIL_VAZA_INFO - Request.Form("chkMonitoramento1")
	private int intEmailCrrt; //IN_EMAIL_CRRT - Request.Form("chkMonitoramento2")
	private int intEmailExpcRisc; //IN_EMAIL_EXPC_RISC - Request.Form("chkMonitoramento3")
	private int intEmailPorn; //IN_EMAIL_PORN - Request.Form("chkMonitoramento4")
	private int intEmailBtpp; //IN_EMAIL_BTPP - Request.Form("chkMonitoramento5")
	private int intEmailOutr; //IN_EMAIL_OUTR - Request.Form("chkMonitoramento6")
	private int intRstrInci; //IN_RSTR_INCI - Request.Form("chkMonitoramento7")
	private int intRstrReic; //IN_RSTR_REIC - Request.Form("chkMonitoramento8")
	private int intMoviQntd; //IN_MOVI_QNTD - Request.Form("chkMonitoramento9")
	private int intMoviValo; //IN_MOVI_VALO - Request.Form("chkMonitoramento10")

	private int intDefrAntiNort; //IN_DEFR_ANTI_NORT - Request.Form("chkOperacoes0")
	private int intIrreCnstGara; //IN_IRRE_CNST_GARA - Request.Form("chkOperacoes1")
	private int intUtizTituDuplFria; //IN_UTIZ_TITU_DUPL_FRIA - Request.Form("chkOperacoes2")
	private int intFalsificacao; //IN_FALS - Request.Form("chkOperacoes3")
	private int intIrreForz; //IN_IRRE_FORZ - Request.Form("chkOperacoes4")
	private String strOperCredOutr; //TX_OPER_CRED_OUTR - Request.Form("txtOperacoes")

	private String strOutros; //TX_OPER_CRED_OUTR - Request.Form("txtOutros")
	private int intCdOutrTipoFrau;	//CD_OUTR_TIPO_FRAU - Request.Form("rdoOutros")
	
	private String cdEmprGrup = "-1"; //CD_EMPR_GRUP,   
	private String cdAreaComp = "-1"; //CD_AREA_COMP,   
	private String cdLinhNegonive_1 = "-1"; //CD_LINH_NEGO_NIVE_1 
	private String cdLinhNegonive_2 = "-1"; //CD_LINH_NEGO_NIVE_2 
	private String cdFatrRisc = "-1"; //CD_FATR_RISC,        
	private String cdCptu = "-1"; //CD_CPTU
	private String cdArea = "";
	
	public String getStrErro() {
		return strErro;
	}

	public void setStrErro(String strErro) {
		this.strErro = strErro;
	}

	public String getStrNrSeqOs() {
		return strNrSeqOs;
	}

	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}

	public String getStrCboParticipante() {
		return strCboParticipante;
	}

	public void setStrCboParticipante(String strCboParticipante) {
		this.strCboParticipante = strCboParticipante;
	}

	public String getStrCboAgenciaConta() {
		return strCboAgenciaConta;
	}

	public void setStrCboAgenciaConta(String strCboAgenciaConta) {
		this.strCboAgenciaConta = strCboAgenciaConta;
	}

	public String getStrCboParticipanteNaoClie() {
		return strCboParticipanteNaoClie;
	}

	public void setStrCboParticipanteNaoClie(String strCboParticipanteNaoClie) {
		this.strCboParticipanteNaoClie = strCboParticipanteNaoClie;
	}

	public String getStrCboParticipanteTodos() {
		return strCboParticipanteTodos;
	}

	public void setStrCboParticipanteTodos(String strCboParticipanteTodos) {
		this.strCboParticipanteTodos = strCboParticipanteTodos;
	}

	public String getObjRsEvento() {
		return objRsEvento;
	}

	public void setObjRsEvento(String objRsEvento) {
		this.objRsEvento = objRsEvento;
	}

	public int getIntEvento() {
		return intEvento;
	}

	public void setIntEvento(int intEvento) {
		this.intEvento = intEvento;
	}

	public boolean isBlnCanal() {
		return blnCanal;
	}

	public void setBlnCanal(boolean blnCanal) {
		this.blnCanal = blnCanal;
	}

	public int[] getArrLinhaDetalhe() {
		return arrLinhaDetalhe;
	}

	public void setArrLinhaDetalhe(int[] arrLinhaDetalhe) {
		this.arrLinhaDetalhe = arrLinhaDetalhe;
	}

	public int[] getArrSeqDetalhe() {
		return arrSeqDetalhe;
	}

	public void setArrSeqDetalhe(int[] arrSeqDetalhe) {
		this.arrSeqDetalhe = arrSeqDetalhe;
	}

	public String[] getArrNrDetalhe() {
		return arrNrDetalhe;
	}

	public void setArrNrDetalhe(String[] arrNrDetalhe) {
		this.arrNrDetalhe = arrNrDetalhe;
	}

	public String[] getArrFrCnalDetalhe() {
		return arrFrCnalDetalhe;
	}

	public void setArrFrCnalDetalhe(String[] arrFrCnalDetalhe) {
		this.arrFrCnalDetalhe = arrFrCnalDetalhe;
	}

	public String[] getArrContaDetalhe() {
		return arrContaDetalhe;
	}

	public void setArrContaDetalhe(String[] arrContaDetalhe) {
		this.arrContaDetalhe = arrContaDetalhe;
	}

	public String[] getArrAgDetalhe() {
		return arrAgDetalhe;
	}

	public void setArrAgDetalhe(String[] arrAgDetalhe) {
		this.arrAgDetalhe = arrAgDetalhe;
	}

	public int getiDetalhe() {
		return iDetalhe;
	}

	public void setiDetalhe(int iDetalhe) {
		this.iDetalhe = iDetalhe;
	}

	public String getStrRqtCboEvento() {
		return strRqtCboEvento;
	}

	public void setStrRqtCboEvento(String strRqtCboEvento) {
		this.strRqtCboEvento = strRqtCboEvento;
	}

	public String getStrRqtCboCanal() {
		return strRqtCboCanal;
	}

	public void setStrRqtCboCanal(String strRqtCboCanal) {
		this.strRqtCboCanal = strRqtCboCanal;
	}

	public String getStrRqtNmCboCanal() {
		return strRqtNmCboCanal;
	}

	public void setStrRqtNmCboCanal(String strRqtNmCboCanal) {
		this.strRqtNmCboCanal = strRqtNmCboCanal;
	}

	public int getIntApurResp() {
		return intApurResp;
	}

	public void setIntApurResp(int intApurResp) {
		this.intApurResp = intApurResp;
	}

	public int getIntDivuIndiInfoClie() {
		return intDivuIndiInfoClie;
	}

	public void setIntDivuIndiInfoClie(int intDivuIndiInfoClie) {
		this.intDivuIndiInfoClie = intDivuIndiInfoClie;
	}

	public int getIntDivuIndiInfoBanc() {
		return intDivuIndiInfoBanc;
	}

	public void setIntDivuIndiInfoBanc(int intDivuIndiInfoBanc) {
		this.intDivuIndiInfoBanc = intDivuIndiInfoBanc;
	}

	public int getIntTrmsMesgCntdIndq() {
		return intTrmsMesgCntdIndq;
	}

	public void setIntTrmsMesgCntdIndq(int intTrmsMesgCntdIndq) {
		this.intTrmsMesgCntdIndq = intTrmsMesgCntdIndq;
	}

	public int getIntVendCasd() {
		return intVendCasd;
	}

	public void setIntVendCasd(int intVendCasd) {
		this.intVendCasd = intVendCasd;
	}

	public int getIntVendNaoForz() {
		return intVendNaoForz;
	}

	public void setIntVendNaoForz(int intVendNaoForz) {
		this.intVendNaoForz = intVendNaoForz;
	}

	public int getIntFals() {
		return intFals;
	}

	public void setIntFals(int intFals) {
		this.intFals = intFals;
	}

	public String getStrNmOutrPratIndq() {
		return strNmOutrPratIndq;
	}

	public void setStrNmOutrPratIndq(String strNmOutrPratIndq) {
		this.strNmOutrPratIndq = strNmOutrPratIndq;
	}

	public int getIntRstrFincCadr() {
		return intRstrFincCadr;
	}

	public void setIntRstrFincCadr(int intRstrFincCadr) {
		this.intRstrFincCadr = intRstrFincCadr;
	}

	public int getIntCdtaProfIndq() {
		return intCdtaProfIndq;
	}

	public void setIntCdtaProfIndq(int intCdtaProfIndq) {
		this.intCdtaProfIndq = intCdtaProfIndq;
	}

	public int getIntDscmNort() {
		return intDscmNort;
	}

	public void setIntDscmNort(int intDscmNort) {
		this.intDscmNort = intDscmNort;
	}

	public int getIntViolCodiEtic() {
		return intViolCodiEtic;
	}

	public void setIntViolCodiEtic(int intViolCodiEtic) {
		this.intViolCodiEtic = intViolCodiEtic;
	}

	public int getIntEndiExce() {
		return intEndiExce;
	}

	public void setIntEndiExce(int intEndiExce) {
		this.intEndiExce = intEndiExce;
	}

	public int getIntApprValoClie() {
		return intApprValoClie;
	}

	public void setIntApprValoClie(int intApprValoClie) {
		this.intApprValoClie = intApprValoClie;
	}

	public int getIntApprValoBanc() {
		return intApprValoBanc;
	}

	public void setIntApprValoBanc(int intApprValoBanc) {
		this.intApprValoBanc = intApprValoBanc;
	}

	public int getIntMoviFincIrre() {
		return intMoviFincIrre;
	}

	public void setIntMoviFincIrre(int intMoviFincIrre) {
		this.intMoviFincIrre = intMoviFincIrre;
	}

	public int getIntOperCred() {
		return intOperCred;
	}

	public void setIntOperCred(int intOperCred) {
		this.intOperCred = intOperCred;
	}

	public int getIntCntrProd() {
		return intCntrProd;
	}

	public void setIntCntrProd(int intCntrProd) {
		this.intCntrProd = intCntrProd;
	}

	public int getIntAberIrreCnta() {
		return intAberIrreCnta;
	}

	public void setIntAberIrreCnta(int intAberIrreCnta) {
		this.intAberIrreCnta = intAberIrreCnta;
	}

	public int getIntEmailQubrSigi() {
		return intEmailQubrSigi;
	}

	public void setIntEmailQubrSigi(int intEmailQubrSigi) {
		this.intEmailQubrSigi = intEmailQubrSigi;
	}

	public int getIntEmailVazaInfo() {
		return intEmailVazaInfo;
	}

	public void setIntEmailVazaInfo(int intEmailVazaInfo) {
		this.intEmailVazaInfo = intEmailVazaInfo;
	}

	public int getIntEmailCrrt() {
		return intEmailCrrt;
	}

	public void setIntEmailCrrt(int intEmailCrrt) {
		this.intEmailCrrt = intEmailCrrt;
	}

	public int getIntEmailExpcRisc() {
		return intEmailExpcRisc;
	}

	public void setIntEmailExpcRisc(int intEmailExpcRisc) {
		this.intEmailExpcRisc = intEmailExpcRisc;
	}

	public int getIntEmailPorn() {
		return intEmailPorn;
	}

	public void setIntEmailPorn(int intEmailPorn) {
		this.intEmailPorn = intEmailPorn;
	}

	public int getIntEmailBtpp() {
		return intEmailBtpp;
	}

	public void setIntEmailBtpp(int intEmailBtpp) {
		this.intEmailBtpp = intEmailBtpp;
	}

	public int getIntEmailOutr() {
		return intEmailOutr;
	}

	public void setIntEmailOutr(int intEmailOutr) {
		this.intEmailOutr = intEmailOutr;
	}

	public int getIntRstrInci() {
		return intRstrInci;
	}

	public void setIntRstrInci(int intRstrInci) {
		this.intRstrInci = intRstrInci;
	}

	public int getIntRstrReic() {
		return intRstrReic;
	}

	public void setIntRstrReic(int intRstrReic) {
		this.intRstrReic = intRstrReic;
	}

	public int getIntMoviQntd() {
		return intMoviQntd;
	}

	public void setIntMoviQntd(int intMoviQntd) {
		this.intMoviQntd = intMoviQntd;
	}

	public int getIntMoviValo() {
		return intMoviValo;
	}

	public void setIntMoviValo(int intMoviValo) {
		this.intMoviValo = intMoviValo;
	}

	public int getIntDefrAntiNort() {
		return intDefrAntiNort;
	}

	public void setIntDefrAntiNort(int intDefrAntiNort) {
		this.intDefrAntiNort = intDefrAntiNort;
	}

	public int getIntIrreCnstGara() {
		return intIrreCnstGara;
	}

	public void setIntIrreCnstGara(int intIrreCnstGara) {
		this.intIrreCnstGara = intIrreCnstGara;
	}

	public int getIntUtizTituDuplFria() {
		return intUtizTituDuplFria;
	}

	public void setIntUtizTituDuplFria(int intUtizTituDuplFria) {
		this.intUtizTituDuplFria = intUtizTituDuplFria;
	}

	public int getIntFalsificacao() {
		return intFalsificacao;
	}

	public void setIntFalsificacao(int intFalsificacao) {
		this.intFalsificacao = intFalsificacao;
	}

	public int getIntIrreForz() {
		return intIrreForz;
	}

	public void setIntIrreForz(int intIrreForz) {
		this.intIrreForz = intIrreForz;
	}

	public String getStrOperCredOutr() {
		return strOperCredOutr;
	}

	public void setStrOperCredOutr(String strOperCredOutr) {
		this.strOperCredOutr = strOperCredOutr;
	}

	public String getStrOutros() {
		return strOutros;
	}

	public void setStrOutros(String strOutros) {
		this.strOutros = strOutros;
	}

	public int getIntCdOutrTipoFrau() {
		return intCdOutrTipoFrau;
	}

	public void setIntCdOutrTipoFrau(int intCdOutrTipoFrau) {
		this.intCdOutrTipoFrau = intCdOutrTipoFrau;
	}

	public String getCdEmprGrup() {
		return cdEmprGrup;
	}

	public void setCdEmprGrup(String cdEmprGrup) {
		this.cdEmprGrup = cdEmprGrup;
	}

	public String getCdAreaComp() {
		return cdAreaComp;
	}

	public void setCdAreaComp(String cdAreaComp) {
		this.cdAreaComp = cdAreaComp;
	}

	public String getCdLinhNegonive_1() {
		return cdLinhNegonive_1;
	}

	public void setCdLinhNegonive_1(String cdLinhNegonive_1) {
		this.cdLinhNegonive_1 = cdLinhNegonive_1;
	}

	public String getCdLinhNegonive_2() {
		return cdLinhNegonive_2;
	}

	public void setCdLinhNegonive_2(String cdLinhNegonive_2) {
		this.cdLinhNegonive_2 = cdLinhNegonive_2;
	}

	public String getCdFatrRisc() {
		return cdFatrRisc;
	}

	public void setCdFatrRisc(String cdFatrRisc) {
		this.cdFatrRisc = cdFatrRisc;
	}

	public String getCdCptu() {
		return cdCptu;
	}

	public void setCdCptu(String cdCptu) {
		this.cdCptu = cdCptu;
	}

	public String getCdArea() {
		return cdArea;
	}

	public void setCdArea(String cdArea) {
		this.cdArea = cdArea;
	}

	/**
	 * 
	 */
	public CadastroOSEventosBean() {
		//clsCadOsEventos objEvento = new clsCadOsEventos();
		//objRsEvento = objEvento.ConsultarFraudeEvento(strNrSeqOs, strErro);
		/*
		if(!(strErro.isEmpty())){
			//objEvento = null;
			objRsEvento = null;
			//VerificaErro(strErro);
		}
		
		while(!objRsEvento.hasNext()) {
			
			//configura campos da Ordem de serviuo
		    cdEmprGrup = objRsEvento("CD_EMPR_GRUP"); 
		    cdAreaComp = objRsEvento("CD_AREA_COMP"); 
		    cdLinhNegonive_1 = objRsEvento("CD_LINH_NEGO_NIVE_1");		
		    cdLinhNegonive_2 = objRsEvento("CD_LINH_NEGO_NIVE_2");
		    cdFatrRisc = objRsEvento("CD_FATR_RISC");
		    cdCptu = objRsEvento("CD_CPTU");
		    
		    switch (objRsEvento("CD_EVEN")) {
		    	case 1:
		    		intApurResp = objRsEvento("IN_APUR_RESP");
		    		break;
		    		
		    	case "2": //Complemento
					intDivuIndiInfoClie	 = objRsEvento("IN_DIVU_INVI_INFO_CLIE");
					intDivuIndiInfoBanc	 = objRsEvento("IN_DIVU_INVI_INFO_BANC");
					intTrmsMesgCntdIndq	 = objRsEvento("IN_TRMS_MESG_CNTD_INDQ");
					intVendCasd			 = objRsEvento("IN_VEND_CASD");
					intVendNaoForz		 = objRsEvento("IN_VEND_NAO_FORZ");
					intFals				 = objRsEvento("IN_FALS");
					strNmOutrPratIndq	 = objRsEvento("NM_OUTR_PRAT_INDQ");
					intRstrFincCadr		 = objRsEvento("IN_RSTR_FINC_CADR");
					intCdtaProfIndq		 = objRsEvento("IN_CDTA_PROF_INDQ");
					intDscmNort			 = objRsEvento("IN_DSCM_NORT");
					intViolCodiEtic		 = objRsEvento("IN_VIOL_CODI_ETIC");
					intEndiExce		     = objRsEvento("IN_ENDI_EXCE");
					break;
					
				case "3": //Fraudes
					intApprValoClie		= objRsEvento("IN_APPR_VALO_CLIE");
					intApprValoBanc		= objRsEvento("IN_APPR_VALO_BANC");
					intMoviFincIrre		= objRsEvento("IN_MOVI_FINC_IRRE");
					intOperCred			= objRsEvento("IN_OPER_CRED");
					intCntrProd			= objRsEvento("IN_CNTR_PROD");
					intAberIrreCnta		= objRsEvento("IN_ABER_IRRE_CNTA");
					break;
					
				case "4": //Fraudes Eletrunicas
					break;
					
				case "5": //Fraudes Operacionais
					break;
					
				case "6": //Monitoramento
					intEmailQubrSigi	= objRsEvento("IN_EMAIL_QUBR_SIGI");
					intEmailVazaInfo	= objRsEvento("IN_EMAIL_VAZA_INFO");
					intEmailCrrt		= objRsEvento("IN_EMAIL_CRRT");
					intEmailExpcRisc	= objRsEvento("IN_EMAIL_EXPC_RISC");
					intEmailPorn		= objRsEvento("IN_EMAIL_PORN");
					intEmailBtpp		= objRsEvento("IN_EMAIL_BTPP");
					intEmailOutr		= objRsEvento("IN_EMAIL_OUTR");
					intRstrInci			= objRsEvento("IN_RSTR_INCI");
					intRstrReic			= objRsEvento("IN_RSTR_REIC");
					intMoviQntd			= objRsEvento("IN_MOVI_QNTD");
					intMoviValo			= objRsEvento("IN_MOVI_VALO");
					break;
					
				case "7": //Operauues de Crudito
					intDefrAntiNort		= objRsEvento("IN_DEFR_ANTI_NORT");
					intIrreCnstGara		= objRsEvento("IN_IRRE_CNST_GARA");
					intUtizTituDuplFria	= objRsEvento("IN_UTIZ_TITU_DUPL_FRIA");
					intFalsificacao		= objRsEvento("IN_FALS");
					intIrreForz			= objRsEvento("IN_IRRE_FORZ");
					strOperCredOutr		= objRsEvento("TX_OPER_CRED_OUTR");
					break;
					
				case "8"://Outros
					strOutros			= objRsEvento("TX_OPER_CRED_OUTR");
					intCdOutrTipoFrau	= objRsEvento("CD_OUTR_TIPO_FRAU");
					break;
				
		    }
		    //objRsEvento.NextLine();
		}

		if(cdEmprGrup == "-1") {
		  
			cdEmprGrup = "1010041";
		  
		  if(cdAreaComp == "-1") {		  
			 cdAreaComp = "1390419"; 
		  }  
		} 
		*/
		//futuramente destruir objetos
		//objEvento = null;
		//objRsEvento = null;
	}
	
	public void alterarCbo() {
		RequestContext.getCurrentInstance().execute("cboCanal();");
	}
	
	
		
}
