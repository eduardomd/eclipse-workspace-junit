package com.altec.bsbr.app.hyb.dto;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.altec.bsbr.app.hyb.web.jsf.HyRelosTotalOsBean;

@ManagedBean(name="CustoTotalOSMock")
@ViewScoped
public class CustoTotalOSMock {
public List<HyRelosTotalOsBean> results = new ArrayList<HyRelosTotalOsBean>();
	
	public CustoTotalOSMock() {
        for (int i = 0; i < 10; i++) {
               results.add(generateRandomResult());
        }
	}
   
	         
	  public List<HyRelosTotalOsBean> getResults() {
	        return results;           
	  }
	   
	  public HyRelosTotalOsBean generateRandomResult() {
		  HyRelosTotalOsBean result = new HyRelosTotalOsBean();
		  result.setROWNUM("01");
	      result.setNR_SEQU_ORDE_SERV("11");
	      result.setNM_SITU_ORDE_SERV("22");
	      result.setDT_ABER_ORDE_SERV("33");
	      result.setDT_ENCERRAMENTO("44");
	      result.setVL_ENVO("55");
	      result.setVL_PERD_EFET("66");
	      result.setVL_CUST_ORDE_DE_SERV(77);
	      return result;
	                       
	  }
}
