package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.ObjRsManutCanaisCanalModel;
import com.altec.bsbr.app.hyb.dto.ObjRsManutCanaisEventoModel;
import com.altec.bsbr.app.hyb.dto.objRsManutAdmGerModel;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmGer.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmGer.XHYAdmGerEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;


@Component("admGerManutCanaisBean")
@Scope("session")
public class AdmGerManutCanaisBean extends BasicBBean {
	
	private static final long serialVersionUID = 1L;
	
	private static final String TABLE_CARREGANDO = "Carregando...";
	private static final String TABLE_VAZIO = "Sem registro"; 
	
	@Autowired
	private XHYAdmGerEndPoint objAdmGer;
	
	@Autowired
    private XHYAdmGerEndPoint admger;
	
	private String txtHdCanal;
	private String txtAddCanal;	
	private String txtHdParamAltCanal;
	private String txtHdParamAltRelac;
	private String txtHdParamAltCanais;
	
	private objRsManutAdmGerModel objRsManutAdmGer = new objRsManutAdmGerModel();

	private LazyDataModel<ObjRsManutCanaisCanalModel> objRsCanal;
	private List<ObjRsManutCanaisEventoModel> objRsEvento;
	
	ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
	
	private String emptyMessage;
	
	private Map<String, Integer> manutCanalChecked;
	
	@PostConstruct
	public void init() throws IOException {
		emptyMessage = TABLE_CARREGANDO;   
		montarListaEvento();
	}
	
	private void montarListaEvento() throws IOException {
				
		JSONArray pcursorObjRsEvento = new JSONArray();
		
		try {
			String objRsEventoTemp = objAdmGer.consultarEvento();
			JSONObject objRsEventoTemp2 = new JSONObject(objRsEventoTemp);
            pcursorObjRsEvento = objRsEventoTemp2.getJSONArray("PCURSOR");

		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			ec.redirect("hy_erro.xhtml?&strErro=" + e.getMessage());
		}
		
		objRsEvento = new ArrayList<ObjRsManutCanaisEventoModel>();
		for (int i = 0; i < pcursorObjRsEvento.length(); i++) {
            JSONObject temp = pcursorObjRsEvento.getJSONObject(i);
            
            objRsEvento.add(
            		new ObjRsManutCanaisEventoModel(
            				temp.get("CODIGO").toString(),
            				temp.get("NOME").toString()
            		));
        }
		
	}
	
	public void montarListaCanal_Lazy() {
    	
		emptyMessage = TABLE_VAZIO;   
    	
    	objRsCanal = new LazyDataModel<ObjRsManutCanaisCanalModel>() {

			private static final long serialVersionUID = 1L;
			
			@Override
			public List<ObjRsManutCanaisCanalModel> load(int first, int pageSize, String sortField,
                                                    SortOrder sortOrder, Map<String, Object> filters) {
												
				List<ObjRsManutCanaisCanalModel> objRsManutCanaisCanalModelTemp = null;
				try {
					objRsManutCanaisCanalModelTemp = montarListaCanal();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				setRowCount(objRsManutCanaisCanalModelTemp.size());
				setPageSize(pageSize);
				
				List<ObjRsManutCanaisCanalModel> result = new ArrayList<ObjRsManutCanaisCanalModel>();
				
				if(manutCanalChecked == null)
					manutCanalChecked = new HashMap<String, Integer>();
				
				manutCanalChecked.clear();
				int criarConexao = 0;
				
				for (int i = first; i < (first + pageSize); i++) {
					
					result.add(objRsManutCanaisCanalModelTemp.get(i));
					
					for (int j = 0; j < objRsEvento.size(); j++) {
						
						String chave = objRsEvento.get(j).getCODIGO() + "_" + objRsManutCanaisCanalModelTemp.get(i).getCODIGO();
												
						manutCanalChecked.put(chave, verificarEventoCanal(criarConexao, objRsEvento.get(j).getCODIGO(), objRsManutCanaisCanalModelTemp.get(i).getCODIGO()));					
					
						criarConexao++;
					}
				}
				
				System.out.println("Size MAP: " + manutCanalChecked.size());
				
				return result;
			}
		};
    }
	
	private List<ObjRsManutCanaisCanalModel> montarListaCanal() throws IOException {
		
		List<ObjRsManutCanaisCanalModel> lstObjRsCanalTemp = null;
				
		JSONArray pcursorObjRsCanal = new JSONArray();
		
		try {
			String objRsCanalTemp = objAdmGer.consultarCanal();
			JSONObject objRsCanalTemp2 = new JSONObject(objRsCanalTemp);
            pcursorObjRsCanal = objRsCanalTemp2.getJSONArray("PCURSOR");
		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			ec.redirect("hy_erro.xhtml?&strErro=" + e.getMessage());
		}	
		
		lstObjRsCanalTemp = new ArrayList<ObjRsManutCanaisCanalModel>();
		
		for (int i = 0; i < pcursorObjRsCanal.length(); i++) {
            JSONObject temp = pcursorObjRsCanal.getJSONObject(i);
            
            boolean exclusao = false;
            if (temp.get("EXCLUSAO").toString().equals("0")) {
            	exclusao = true;
            }

            lstObjRsCanalTemp.add(
            		new ObjRsManutCanaisCanalModel(
            				temp.get("CODIGO").toString(),
            				temp.get("NOME").toString(),
            				exclusao
            		));
        }
			
		return lstObjRsCanalTemp;
	} 
	
	private Integer verificarEventoCanal(int index, String intCodEvento, String intCodCanal) {
		String objRs = "";
		Integer retorno = 0;

		try {
			objRs = admger.verificaEventoCanal(index, intCodEvento, intCodCanal);
			}
			catch (Exception e) {
                try {
                       FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
                } catch (IOException e1) {
                      // TODO Auto-generated catch block
                      e1.printStackTrace();
                }
         }

		JSONObject objRsTemp = new JSONObject(objRs);
		JSONArray pcursor = objRsTemp.getJSONArray("PCURSOR");
		JSONObject f = pcursor.getJSONObject(0);

		if (f.get("RELAC").toString().equals("1")) {
			retorno = 1;
		}
		
		return retorno;
	}
	
    public Integer isEventoCanalChecked(int intCodEvento, int intCodCanal) {
		String chave = intCodEvento + "_" + intCodCanal;
		return manutCanalChecked.get(chave);
    }
	
	public objRsManutAdmGerModel getObjRsManutAdmGer() {
		return objRsManutAdmGer;
	}

	public void setObjRsManutAdmGer(objRsManutAdmGerModel objRsManutAdmGer) {
		this.objRsManutAdmGer = objRsManutAdmGer;
	}

	public int getObjRsEventoLength() {
		return objRsEvento.size();
	}
		
	public void adicionar() throws IOException {
		//TODO inserir log no back
		boolean contains = false;
		String canalAdd = txtAddCanal.toUpperCase();
		
		for (ObjRsManutCanaisCanalModel canal : objRsCanal) {
			if (canal.getNOME().equals(canalAdd)) {
				contains = true;
			}
		}
        
		if (contains) {
			RequestContext.getCurrentInstance().execute("alert('Nome jÃ¡ existente!\\nEscolha outro Nome!')");

		} else {
			try {
				objAdmGer.inserirCanal(canalAdd);
				RequestContext.getCurrentInstance().execute("alert('Canal incluÃ­do com sucesso!')");
				txtAddCanal = "";
			} catch (WebServiceException e) {
				// TODO Auto-generated catch block			
				//RequestContext.getCurrentInstance().execute("alert('erro!')");
				ec.redirect("hy_erro.xhtml?&strErro=" + e.getMessage());
			}
		}
		
		montarListaCanal_Lazy();
	}
	
	public void salvar() throws IOException {
		
		Map<String, String> requestParamMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

		String result = requestParamMap.get("frm:txtHdParamAltCanal");
		
		String [] x = result.split("@");
		
		for(int i = 0; i < x.length;  i++) {

			String [] y = x[i].split("/");
			
			//System.out.println("Codigo: " + y[0] + " Nome: " + y[1]);
			
			try {
				objAdmGer.alterarCanal(y[0], y[1]);
			} catch (WebServiceException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				ec.redirect("hy_erro.xhtml?&strErro=" + e.getMessage());
			}
			
		}
		
		result = requestParamMap.get("frm:txtHdParamAltRelac");
		String [] p = result.split("@");
		
		for(int i = 0; i < p.length;  i++) {

			String [] y = p[i].split("/");
			
			//System.out.println("Codigo: " + y[0] + " Nome: " + y[1] + " Marcado: " + y[2]);
			if (y[2].equals("true")) {
				try {
					objAdmGer.inserirEventoCanal(y[0], y[1]);
				} catch (WebServiceException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
					ec.redirect("hy_erro.xhtml?&strErro=" + e.getMessage());
				}
			} else {
				try {
					objAdmGer.apagarEventoCanal(y[0], y[1]);
				} catch (WebServiceException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
					ec.redirect("hy_erro.xhtml?&strErro=" + e.getMessage());
				}
			}
			
		}
		
		montarListaEvento();
		montarListaCanal_Lazy();
	}
	
	public void deletar() throws IOException {
		//TODO inserir log no back
		String codCanal = txtHdCanal;
		
		try {
			objAdmGer.apagarCanal(codCanal);
			RequestContext.getCurrentInstance().execute("alert('Canal excluÃ­do com sucesso!')");
		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			ec.redirect("hy_erro.xhtml?&strErro=" + e.getMessage());
		}
		
		//System.out.println(txtHdCanal + " deletado!");
		
		montarListaEvento();
		montarListaCanal_Lazy();
	}

	public LazyDataModel<ObjRsManutCanaisCanalModel> getObjRsCanal() {
		return objRsCanal;
	}
	
	public List<ObjRsManutCanaisEventoModel> getObjRsEvento() {
		return objRsEvento;
	}

	public void setObjRsEvento(List<ObjRsManutCanaisEventoModel> objRsEvento) {
		this.objRsEvento = objRsEvento;
	}

	public String getTxtHdCanal() {
		return txtHdCanal;
	}
	
	public void setTxtHdCanal(String txtHdCanal) {
		this.txtHdCanal = txtHdCanal;
	}

	public String getTxtAddCanal() {
		return txtAddCanal;
	}
	
	public void setTxtAddCanal(String txtAddCanal) {
		this.txtAddCanal = txtAddCanal;
	}

	public String getTxtHdParamAltCanal() {
		return txtHdParamAltCanal;
	}

	public void setTxtHdParamAltCanal(String txtHdParamAltCanal) {
		this.txtHdParamAltCanal = txtHdParamAltCanal;
	}

	public String getTxtHdParamAltRelac() {
		return txtHdParamAltRelac;
	}

	public void setTxtHdParamAltRelac(String txtHdParamAltRelac) {
		this.txtHdParamAltRelac = txtHdParamAltRelac;
	}

	public String getTxtHdParamAltCanais() {
		return txtHdParamAltCanais;
	}

	public void setTxtHdParamAltCanais(String txtHdParamAltCanais) {
		this.txtHdParamAltCanais = txtHdParamAltCanais;
	}

	public String getEmptyMessage() {
		return emptyMessage;
	}

	public void setEmptyMessage(String emptyMessage) {
		this.emptyMessage = emptyMessage;
	}

	
}
