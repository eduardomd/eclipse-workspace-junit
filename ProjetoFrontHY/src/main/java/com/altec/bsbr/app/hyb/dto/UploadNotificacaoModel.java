package com.altec.bsbr.app.hyb.dto;

public class UploadNotificacaoModel {
	
	private String strNmArquivo;
	private String strCaminho;
	private String strNumOS;

	
	public UploadNotificacaoModel() {}
	
	public UploadNotificacaoModel(String strNmArquivo, String strCaminho) {
		this.strNmArquivo = strNmArquivo;
		this.strCaminho = strCaminho;

	}
	
	public String getStrNmArquivo() {
		return strNmArquivo;
	}
	public void setStrNmArquivo(String strNmArquivo) {
		this.strNmArquivo = strNmArquivo;
	}
	public String getStrCaminho() {
		return strCaminho;
	}
	public void setStrCaminho(String strCaminho) {
		this.strCaminho = strCaminho;
	}
	public String getStrNumOS() {
		return strNumOS;
	}
	public void setStrNumOS(String strNumOS) {
		this.strNumOS = strNumOS;
	}

}
