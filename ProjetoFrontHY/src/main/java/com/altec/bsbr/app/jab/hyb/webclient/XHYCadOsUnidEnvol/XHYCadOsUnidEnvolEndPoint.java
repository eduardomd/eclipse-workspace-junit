//
// Generated By:JAX-WS RI 2.2.9-b130926.1035 (JAXB RI IBM 2.2.8-b130911.1802)
//


package com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsUnidEnvol;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;

@WebService(name = "xHY_CadOsUnidEnvolEndPoint", targetNamespace = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/")
@SOAPBinding(style = SOAPBinding.Style.RPC)
@XmlSeeAlso({
    ObjectFactory.class
})
public interface XHYCadOsUnidEnvolEndPoint {


    /**
     * 
     * @param arg2
     * @param arg1
     * @param arg0
     * @return
     *     returns java.lang.String
     * @throws WebServiceException
     */
    @WebMethod(operationName = "Consultar")
    @WebResult(partName = "return")
    @Action(input = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_CadOsUnidEnvolEndPoint/ConsultarRequest", output = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_CadOsUnidEnvolEndPoint/ConsultarResponse", fault = {
        @FaultAction(className = WebServiceException.class, value = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_CadOsUnidEnvolEndPoint/Consultar/Fault/WebServiceException")
    })
    public String consultar(
        @WebParam(name = "arg0", partName = "arg0")
        String arg0,
        @WebParam(name = "arg1", partName = "arg1")
        String arg1,
        @WebParam(name = "arg2", partName = "arg2")
        String arg2)
        throws WebServiceException
    ;

    /**
     * 
     * @param arg2
     * @param arg1
     * @param arg0
     * @return
     *     returns java.lang.String
     * @throws WebServiceException
     */
    @WebMethod
    @WebResult(partName = "return")
    @Action(input = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_CadOsUnidEnvolEndPoint/fnSelPVNrRequest", output = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_CadOsUnidEnvolEndPoint/fnSelPVNrResponse", fault = {
        @FaultAction(className = WebServiceException.class, value = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_CadOsUnidEnvolEndPoint/fnSelPVNr/Fault/WebServiceException")
    })
    public String fnSelPVNr(
        @WebParam(name = "arg0", partName = "arg0")
        String arg0,
        @WebParam(name = "arg1", partName = "arg1")
        String arg1,
        @WebParam(name = "arg2", partName = "arg2")
        String arg2)
        throws WebServiceException
    ;

    /**
     * 
     * @param arg1
     * @param arg0
     * @return
     *     returns java.lang.String
     * @throws WebServiceException
     */
    @WebMethod
    @WebResult(partName = "return")
    @Action(input = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_CadOsUnidEnvolEndPoint/fnSelAgenciaRequest", output = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_CadOsUnidEnvolEndPoint/fnSelAgenciaResponse", fault = {
        @FaultAction(className = WebServiceException.class, value = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_CadOsUnidEnvolEndPoint/fnSelAgencia/Fault/WebServiceException")
    })
    public String fnSelAgencia(
        @WebParam(name = "arg0", partName = "arg0")
        String arg0,
        @WebParam(name = "arg1", partName = "arg1")
        String arg1)
        throws WebServiceException
    ;

    /**
     * 
     * @param arg0
     * @return
     *     returns java.lang.String
     * @throws WebServiceException
     */
    @WebMethod
    @WebResult(partName = "return")
    @Action(input = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_CadOsUnidEnvolEndPoint/fnSelUnidEnvolvRequest", output = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_CadOsUnidEnvolEndPoint/fnSelUnidEnvolvResponse", fault = {
        @FaultAction(className = WebServiceException.class, value = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_CadOsUnidEnvolEndPoint/fnSelUnidEnvolv/Fault/WebServiceException")
    })
    public String fnSelUnidEnvolv(
        @WebParam(name = "arg0", partName = "arg0")
        String arg0)
        throws WebServiceException
    ;

    /**
     * 
     * @param arg0
     * @return
     *     returns java.lang.String
     * @throws WebServiceException
     */
    @WebMethod
    @WebResult(partName = "return")
    @Action(input = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_CadOsUnidEnvolEndPoint/fnSelRegionalRequest", output = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_CadOsUnidEnvolEndPoint/fnSelRegionalResponse", fault = {
        @FaultAction(className = WebServiceException.class, value = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_CadOsUnidEnvolEndPoint/fnSelRegional/Fault/WebServiceException")
    })
    public String fnSelRegional(
        @WebParam(name = "arg0", partName = "arg0")
        String arg0)
        throws WebServiceException
    ;

    /**
     * 
     * @param arg3
     * @param arg2
     * @param arg1
     * @param arg0
     * @return
     *     returns java.lang.String
     * @throws WebServiceException
     */
    @WebMethod(operationName = "SalvarUnidEnvolv")
    @WebResult(partName = "return")
    @Action(input = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_CadOsUnidEnvolEndPoint/SalvarUnidEnvolvRequest", output = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_CadOsUnidEnvolEndPoint/SalvarUnidEnvolvResponse", fault = {
        @FaultAction(className = WebServiceException.class, value = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_CadOsUnidEnvolEndPoint/SalvarUnidEnvolv/Fault/WebServiceException")
    })
    public String salvarUnidEnvolv(
        @WebParam(name = "arg0", partName = "arg0")
        String arg0,
        @WebParam(name = "arg1", partName = "arg1")
        String arg1,
        @WebParam(name = "arg2", partName = "arg2")
        String arg2,
        @WebParam(name = "arg3", partName = "arg3")
        String arg3)
        throws WebServiceException
    ;

    /**
     * 
     * @param arg3
     * @param arg2
     * @param arg1
     * @param arg0
     * @return
     *     returns boolean
     * @throws WebServiceException
     */
    @WebMethod
    @WebResult(partName = "return")
    @Action(input = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_CadOsUnidEnvolEndPoint/fnUpdUnidadeEnvolvidaRequest", output = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_CadOsUnidEnvolEndPoint/fnUpdUnidadeEnvolvidaResponse", fault = {
        @FaultAction(className = WebServiceException.class, value = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_CadOsUnidEnvolEndPoint/fnUpdUnidadeEnvolvida/Fault/WebServiceException")
    })
    public boolean fnUpdUnidadeEnvolvida(
        @WebParam(name = "arg0", partName = "arg0")
        String arg0,
        @WebParam(name = "arg1", partName = "arg1")
        String arg1,
        @WebParam(name = "arg2", partName = "arg2")
        String arg2,
        @WebParam(name = "arg3", partName = "arg3")
        String arg3)
        throws WebServiceException
    ;

    /**
     * 
     * @param arg0
     * @return
     *     returns java.lang.String
     * @throws WebServiceException
     */
    @WebMethod
    @WebResult(partName = "return")
    @Action(input = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_CadOsUnidEnvolEndPoint/consultarUnidEnvolvOsRequest", output = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_CadOsUnidEnvolEndPoint/consultarUnidEnvolvOsResponse", fault = {
        @FaultAction(className = WebServiceException.class, value = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_CadOsUnidEnvolEndPoint/consultarUnidEnvolvOs/Fault/WebServiceException")
    })
    public String consultarUnidEnvolvOs(
        @WebParam(name = "arg0", partName = "arg0")
        String arg0)
        throws WebServiceException
    ;

    /**
     * 
     * @return
     *     returns java.lang.String
     * @throws WebServiceException
     */
    @WebMethod
    @WebResult(partName = "return")
    @Action(input = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_CadOsUnidEnvolEndPoint/fnSelRedeRequest", output = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_CadOsUnidEnvolEndPoint/fnSelRedeResponse", fault = {
        @FaultAction(className = WebServiceException.class, value = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_CadOsUnidEnvolEndPoint/fnSelRede/Fault/WebServiceException")
    })
    public String fnSelRede()
        throws WebServiceException
    ;

    /**
     * 
     * @param arg0
     * @return
     *     returns java.lang.String
     * @throws WebServiceException
     */
    @WebMethod
    @WebResult(partName = "return")
    @Action(input = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_CadOsUnidEnvolEndPoint/fnSelAdmCentralRequest", output = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_CadOsUnidEnvolEndPoint/fnSelAdmCentralResponse", fault = {
        @FaultAction(className = WebServiceException.class, value = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/xHY_CadOsUnidEnvolEndPoint/fnSelAdmCentral/Fault/WebServiceException")
    })
    public String fnSelAdmCentral(
        @WebParam(name = "arg0", partName = "arg0")
        String arg0)
        throws WebServiceException
    ;

}
