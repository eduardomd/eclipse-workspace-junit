package com.altec.bsbr.app.hyb.web.jsf;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.ObjRsNotiManual;
import com.altec.bsbr.app.hyb.dto.UsuarioIncModel;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmNotificacao.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmNotificacao.XHYAdmNotificacaoEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYUsuario.XHYUsuarioEndPoint;
import com.altec.bsbr.fw.security.SecurityInfo;
import com.altec.bsbr.fw.security.authorization.Authorization;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.ibm.icu.text.SimpleDateFormat;

@Component("AdmWorkflowNotificacaoPesq")
@Scope("session")
public class AdmWorkflowNotificacaoPesq extends BasicBBean {

	// Variáveis para session
	@Autowired
	private XHYUsuarioEndPoint usuario;
	@Autowired
	private Authorization authorization;
	private UsuarioIncModel usuarioIncModel;

	private static final long serialVersionUID = 1L;
	private List<ObjRsNotiManual> objRsNotiManuais;
	private Date txtDtInicio;
	private Date txtDtFim;

	@Autowired
	private XHYAdmNotificacaoEndPoint admnoti;

	@PostConstruct
	public void init() {
		objRsNotiManuais = new ArrayList<ObjRsNotiManual>();
		String retorno = this.pesquisaNotificacao("", "");
		fillObject(retorno);

		carregarUsuario();
	}

	// Overload
	public String pesquisaNotificacao(Date dtInicio, Date dtFim) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		System.out.println("-----------------------------------------");
		System.out.println(dtInicio);
		System.out.println(dtFim);
		return this.pesquisaNotificacao(dtInicio == null ? "" : formatter.format(dtInicio),
				dtFim == null ? "" : formatter.format(dtFim));
	}

	// Overload
	public String pesquisaNotificacao(String dtInicio, String dtFim) {
		String json = "";

		// Mandou data em branco, inicializa dtInicio com o valor fixo e dtFim como a
		// data atual
		if (dtInicio.trim().isEmpty())
			dtInicio = "1900-01-01 00:00:00";

		if (dtFim.trim().isEmpty())
			dtFim = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());

		try {
			System.out.println("dtInicio str "+dtInicio);
			System.out.println("dtFim str "+dtFim);
			json = admnoti.consultarNotificacaoManual(dtInicio, dtFim, 0);
			System.out.println("consultarNotificacaoManual retorno:  " + json);
			setTxtDtInicio(null);
			setTxtDtFim(null);
		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return json;
	}

	public void fillObject(String json) {
		JSONObject jsonRetorno = new JSONObject(json);
		JSONArray pcursor = jsonRetorno.getJSONArray("PCURSOR");

		for (int i = 0; i < pcursor.length(); i++) {
			JSONObject curr = pcursor.getJSONObject(i);
			this.objRsNotiManuais.add(new ObjRsNotiManual(String.valueOf(curr.get("NR_SEQU_NOTI_MANL_ENVI")),
					String.valueOf(curr.get("IN_ENVI_RETN")), curr.getString("DATA"),
					curr.getString("NM_RECU_OCOR_ESPC"), curr.getString("NR_SEQU_ORDE_SERV"),
					String.valueOf(curr.get("IN_PRZ_ESTR")), curr.getString("DATA_RET"),
					String.valueOf(curr.get("CD_SITU_ENVI")), curr.getString("CD_AREA")));
		}
	}

	public void pesquisar() {
		this.objRsNotiManuais.clear();
		String retorno = this.pesquisaNotificacao(this.txtDtInicio, this.txtDtFim);
		fillObject(retorno);
	}

	public List<ObjRsNotiManual> getObjRsNotiManuais() {
//		objRsNotiManuais = new ArrayList<ObjRsNotiManual>();
//		objRsNotiManuais.add(new ObjRsNotiManual("nR_SEQU_NOTI_MANL_ENVI", "iN_ENVI_RETN", "26/12/2018", "nM_RECU_OCOR_ESPC",
//				"321600", "iN_PRZ_ESTR", "dATA_RET", "cD_SITU_ENVI", "cD_AREA"));
//		objRsNotiManuais.add(new ObjRsNotiManual("32123450", "1", "27/12/2018", "nM_RECU_OCOR_ESPC",
//				"111110", "iN_PRZ_ESTR", "dATA_RET", "1", "cD_AREA"));
//		objRsNotiManuais.add(new ObjRsNotiManual("nR_SEQU_NOTI_MANL_ENVI", "iN_ENVI_RETN", "26/12/2018", "nM_RECU_OCOR_ESPC",
//				"321600", "iN_PRZ_ESTR", "dATA_RET", "cD_SITU_ENVI", "cD_AREA"));
		return objRsNotiManuais;
	}

	public void salvar() {
		String retorno = "";
		String txthdAuxRet = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
				.get("txthdAuxRet").toString();
		try {
			retorno = admnoti.arrayNotificacaoManual(txthdAuxRet, "R");
		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Recarregar lista
		retorno = "";
		this.objRsNotiManuais.clear();
		retorno = this.pesquisaNotificacao(this.txtDtInicio, this.txtDtFim);
		fillObject(retorno);
		RequestContext.getCurrentInstance().execute("alert('Alteração com sucesso!')");
	}

	public void reenviar() {
		String retorno = "";
		String txthdAuxRet = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
				.get("txthdAuxRet").toString();
		String txtHdAuxAreaOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
				.get("txtHdAuxAreaOs").toString();
		System.out.println("--------reenviar");
		System.out.println("txtHdAuxRet " + txthdAuxRet);
		System.out.println("txtHdAuxAreaOs " + txtHdAuxAreaOs);
		try {
			retorno = admnoti.incluirNotificacaoReenvio(Long.valueOf(txthdAuxRet));
		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("retorno " + retorno);

		JSONObject jsonRetorno = new JSONObject(retorno);
		String strRetNrSeqNot = String.valueOf(jsonRetorno.getInt("PCURSOR"));
		System.out.println("strRetNrSeqNot " + strRetNrSeqNot);

		String NM_EMAIL_USUARIO = usuarioIncModel.getNmEmailusuario();
		String CD_CARGO_USUARIO = usuarioIncModel.getCpfUsuario();

		try {
			System.out.println("Parametros reenviarEmail "+strRetNrSeqNot+" - "+NM_EMAIL_USUARIO+" - "+txtHdAuxAreaOs+" - "+CD_CARGO_USUARIO);
			retorno = admnoti.reenviarEmail(Long.valueOf(strRetNrSeqNot), NM_EMAIL_USUARIO, Long.valueOf(txtHdAuxAreaOs), CD_CARGO_USUARIO);
			System.out.println("retorno reenviarEmail " + retorno);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		RequestContext.getCurrentInstance().execute("alert('Reenvio de email com sucesso!')");
	}

	public void carregarUsuario() {
		//TODO Fazer um static ou singleton do usuário para recuperar mais facilmente as informações
		
		// Recupera o usuario corrente da rede
		SecurityInfo secInfo = SecurityInfo.getCurrent();
		String userName = secInfo.getUserName().replaceAll("[^0-9]", "");
		System.out.println("username: "+secInfo.getUserName());

		if (userName.isEmpty()) {
			FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(
					FacesContext.getCurrentInstance(), null, "hyb_erro.xhtml?&strErro=Sessão Expirada!");
		} else {
			String retorno = "";
			try {
				retorno = usuario.recuperarDadosUsuario(userName);
				
				JSONObject dadosUsuario = new JSONObject(retorno);
				JSONArray pcursor = dadosUsuario.getJSONArray("PCURSOR");
				for (int i = 0; i < pcursor.length(); i++) {
					JSONObject f = pcursor.getJSONObject(i);
					usuarioIncModel = new UsuarioIncModel();
					usuarioIncModel
							.setMatriculaUsuario(f.isNull("MATRICULA") == true ? "" : f.get("MATRICULA").toString());
					usuarioIncModel
							.setNomeUsuario(f.isNull("NOME_USUARIO") == true ? "" : f.get("NOME_USUARIO").toString());
					usuarioIncModel
							.setCpfUsuario(f.isNull("CPF") == true ? "" : f.get("CPF").toString());
					usuarioIncModel
							.setLoginUsuario(f.isNull("LOGIN") == true ? "" : f.get("LOGIN").toString());
					usuarioIncModel
							.setCdAreaUsuario(f.isNull("CODIGO_AREA") == true ? "" : f.get("CODIGO_AREA").toString());
					usuarioIncModel
							.setNomeAreaUsuario(f.isNull("NOME_AREA") == true ? "" : f.get("NOME_AREA").toString());
					usuarioIncModel
							.setNmEmailusuario(f.isNull("EMAIL") == true ? "" : f.get("EMAIL").toString());
					usuarioIncModel
							.setCdCargoUsuario(f.isNull("CODIGO_CARGO") == true ? "" : f.get("CODIGO_CARGO").toString());
				}
			} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYUsuario.WebServiceException e) {
				// TODO Auto-generated catch block
				FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(
						FacesContext.getCurrentInstance(), null, "hyb_erro.xhtml?&strErro=" + e.getMessage());
			}
		} 

	}


	public Date getTxtDtInicio() {
		return txtDtInicio;
	}

	public void setTxtDtInicio(Date txtDtInicio) {
		this.txtDtInicio = txtDtInicio;
	}

	public Date getTxtDtFim() {
		return txtDtFim;
	}

	public void setTxtDtFim(Date txtDtFim) {
		this.txtDtFim = txtDtFim;
	}

}
