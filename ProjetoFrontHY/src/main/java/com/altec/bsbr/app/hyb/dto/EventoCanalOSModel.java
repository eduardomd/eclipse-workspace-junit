package com.altec.bsbr.app.hyb.dto;

public class EventoCanalOSModel {

	private String NM_EVEN;
	private String NM_CNAL;
	
	public EventoCanalOSModel(String NM_EVEN, String NM_CNAL) {
		this.NM_EVEN = NM_EVEN;	
		this.NM_CNAL = NM_CNAL;		
	}	
	
	public String getNM_EVEN() {
		return NM_EVEN;
	}

	public void setNM_EVEN(String NM_EVEN) {
		this.NM_EVEN = NM_EVEN;
	}

	public String getNM_CNAL() {
		return NM_CNAL;
	}

	public void setNM_CNAL(String NM_CNAL) {
		this.NM_CNAL = NM_CNAL;
	}
	
}
