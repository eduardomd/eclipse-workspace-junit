//
// Generated By:JAX-WS RI 2.2.9-b130926.1035 (JAXB RI IBM 2.2.8-b130911.1802)
//


package com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsEquipAtrib;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;

@WebServiceClient(name = "xHY_CadOsEquipAtribEndPointService", targetNamespace = "http://webservice.srv001.hyb.jab.app.bsbr.altec.com/", wsdlLocation = "http://d2240910.bs.br.bsch:9080/JAB-HYBSRVOESP/xHY_CadOsEquipAtribEndPointService/XHYCadOsEquipAtribEndPointService.wsdl")
public class XHYCadOsEquipAtribEndPointService
    extends Service
{

    private final static URL XHYCADOSEQUIPATRIBENDPOINTSERVICE_WSDL_LOCATION;
    private final static WebServiceException XHYCADOSEQUIPATRIBENDPOINTSERVICE_EXCEPTION;
    private final static QName XHYCADOSEQUIPATRIBENDPOINTSERVICE_QNAME = new QName("http://webservice.srv001.hyb.jab.app.bsbr.altec.com/", "xHY_CadOsEquipAtribEndPointService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://d2240910.bs.br.bsch:9080/JAB-HYBSRVOESP/xHY_CadOsEquipAtribEndPointService/XHYCadOsEquipAtribEndPointService.wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        XHYCADOSEQUIPATRIBENDPOINTSERVICE_WSDL_LOCATION = url;
        XHYCADOSEQUIPATRIBENDPOINTSERVICE_EXCEPTION = e;
    }

    public XHYCadOsEquipAtribEndPointService() {
        super(__getWsdlLocation(), XHYCADOSEQUIPATRIBENDPOINTSERVICE_QNAME);
    }

    public XHYCadOsEquipAtribEndPointService(WebServiceFeature... features) {
        super(__getWsdlLocation(), XHYCADOSEQUIPATRIBENDPOINTSERVICE_QNAME, features);
    }

    public XHYCadOsEquipAtribEndPointService(URL wsdlLocation) {
        super(wsdlLocation, XHYCADOSEQUIPATRIBENDPOINTSERVICE_QNAME);
    }

    public XHYCadOsEquipAtribEndPointService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, XHYCADOSEQUIPATRIBENDPOINTSERVICE_QNAME, features);
    }

    public XHYCadOsEquipAtribEndPointService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public XHYCadOsEquipAtribEndPointService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns XHYCadOsEquipAtribEndPoint
     */
    @WebEndpoint(name = "xHY_CadOsEquipAtribEndPointPort")
    public XHYCadOsEquipAtribEndPoint getXHYCadOsEquipAtribEndPointPort() {
        return super.getPort(new QName("http://webservice.srv001.hyb.jab.app.bsbr.altec.com/", "xHY_CadOsEquipAtribEndPointPort"), XHYCadOsEquipAtribEndPoint.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns XHYCadOsEquipAtribEndPoint
     */
    @WebEndpoint(name = "xHY_CadOsEquipAtribEndPointPort")
    public XHYCadOsEquipAtribEndPoint getXHYCadOsEquipAtribEndPointPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://webservice.srv001.hyb.jab.app.bsbr.altec.com/", "xHY_CadOsEquipAtribEndPointPort"), XHYCadOsEquipAtribEndPoint.class, features);
    }

    private static URL __getWsdlLocation() {
        if (XHYCADOSEQUIPATRIBENDPOINTSERVICE_EXCEPTION!= null) {
            throw XHYCADOSEQUIPATRIBENDPOINTSERVICE_EXCEPTION;
        }
        return XHYCADOSEQUIPATRIBENDPOINTSERVICE_WSDL_LOCATION;
    }

}
