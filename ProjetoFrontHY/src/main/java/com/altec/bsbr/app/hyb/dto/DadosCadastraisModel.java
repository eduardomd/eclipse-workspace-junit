package com.altec.bsbr.app.hyb.dto;

public class DadosCadastraisModel {
	private String tipoPessoa;
	private String cpfCnpj;
	private String nome;
	private String desig; //vem da checkbox
	private String rg;
	private String orgaoEmissor;
	private String dtEmissao;
	private String profissao;
	private String renda;
	private String endereco;
	private String numEndereco;
	private String complementoEndereco;
	private String bairroEndereco;
	private String cepEndereco;
	private String cidadeEndereco;
	private String estadoEndereco;
	private String telefone;
	private String celular;

			
	public DadosCadastraisModel() {
		
	}
	
	public DadosCadastraisModel(String tipoPessoa, String cpfCnpj, String nome, String desig, String rg,
			String orgaoEmissor, String dtEmissao, String profissao, String renda, String endereco, String numEndereco,
			String complementoEndereco, String bairroEndereco, String cepEndereco, String cidadeEndereco, String estadoEndereco,
			String telefone, String celular) {
		super();
		this.tipoPessoa = tipoPessoa;
		this.cpfCnpj = cpfCnpj;
		this.nome = nome;
		this.desig = desig;
		this.rg = rg;
		this.orgaoEmissor = orgaoEmissor;
		this.dtEmissao = dtEmissao;
		this.profissao = profissao;
		this.renda = renda;
		this.endereco = endereco;
		this.numEndereco = numEndereco;
		this.complementoEndereco = complementoEndereco;
		this.bairroEndereco = bairroEndereco;
		this.cepEndereco = cepEndereco;
		this.cidadeEndereco = cidadeEndereco;
		this.estadoEndereco = estadoEndereco;
		this.telefone = telefone;
		this.celular = celular;
	}
	

	public String getTipoPessoa() {
		return tipoPessoa;
	}

	public void setTipoPessoa(String tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDesig() {
		return desig;
	}
	public void setDesig(String desig) {
		this.desig = desig;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public String getOrgaoEmissor() {
		return orgaoEmissor;
	}
	public void setOrgaoEmissor(String orgaoEmissor) {
		this.orgaoEmissor = orgaoEmissor;
	}
	public String getDtEmissao() {
		return dtEmissao;
	}
	public void setDtEmissao(String dtEmissao) {
		this.dtEmissao = dtEmissao;
	}
	public String getProfissao() {
		return profissao;
	}
	public void setProfissao(String profissao) {
		this.profissao = profissao;
	}
	public String getRenda() {
		return renda;
	}
	public void setRenda(String renda) {
		this.renda = renda;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getNumEndereco() {
		return numEndereco;
	}
	public void setNumEndereco(String numEndereco) {
		this.numEndereco = numEndereco;
	}
	public String getComplementoEndereco() {
		return complementoEndereco;
	}
	public void setComplementoEndereco(String complementoEndereco) {
		this.complementoEndereco = complementoEndereco;
	}
	public String getBairroEndereco() {
		return bairroEndereco;
	}
	public void setBairroEndereco(String bairroEndereco) {
		this.bairroEndereco = bairroEndereco;
	}
	public String getCepEndereco() {
		return cepEndereco;
	}
	public void setCepEndereco(String cepEndereco) {
		this.cepEndereco = cepEndereco;
	}

	public String getCidadeEndereco() {
		return cidadeEndereco;
	}

	public void setCidadeEndereco(String cidadeEndereco) {
		this.cidadeEndereco = cidadeEndereco;
	}

	public String getEstadoEndereco() {
		return estadoEndereco;
	}

	public void setEstadoEndereco(String estadoEndereco) {
		this.estadoEndereco = estadoEndereco;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	

}
