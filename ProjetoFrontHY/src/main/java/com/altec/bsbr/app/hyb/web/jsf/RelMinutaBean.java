package com.altec.bsbr.app.hyb.web.jsf;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.RelMinutaEquipeModel;
import com.altec.bsbr.app.hyb.dto.RelMinutaModel;
import com.altec.bsbr.app.hyb.dto.UsuarioIncModel;
import com.altec.bsbr.app.hyb.web.util.XHYUsuarioIncService;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAcoesOs.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAcoesOs.XHYAcoesOsEndPoint;

@Component("relMinutaBean")
@Scope("request")
public class RelMinutaBean {
	
	private String strNrSeqOs;	
	private String textfield;
	private String login_usuario;
	private String retorno;
	
	@Autowired
	private XHYUsuarioIncService user;
	
	@Autowired
    private XHYAcoesOsEndPoint objRs;
	

    private RelMinutaModel objRsMinutaModel;
    private RelMinutaEquipeModel objRsMinutaEquipeModel;
    private List<RelMinutaEquipeModel> objRsMinutaEquipe;
    private List<RelMinutaEquipeModel> objRsMinutaEquipe1;
   
    
	@PostConstruct
    public void init(){
		strNrSeqOs = (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs");
		//data_atual = new SimpleDateFormat("dd/MM/yyyy").format(new Date()).toString();
		//hora_atual = new SimpleDateFormat("HH:mm").format(new Date()).toString();
		
		ObjRsMinuta();
	    
		String busca = "";		
	    objRsMinutaEquipe = new ArrayList<RelMinutaEquipeModel>();
	    ObjRsMinutaEquipe(objRsMinutaEquipe, busca);

	    
	    busca = "B70555";
	    objRsMinutaEquipe1 = new ArrayList<RelMinutaEquipeModel>();
	    ObjRsMinutaEquipe(objRsMinutaEquipe1, busca);
		
		if (user != null) {			
			try {
				UsuarioIncModel dadosUser = user.getDadosUsuario();
				setLogin_usuario(dadosUser.getLoginUsuario());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}		
	}
	
	public void ObjRsMinuta() {	       
       try {
			retorno = objRs.consultarOS(strNrSeqOs);
	        JSONObject rstemp = new JSONObject(retorno);
	        JSONArray pcursor = rstemp.getJSONArray("PCURSOR");
	        JSONObject f = pcursor.getJSONObject(0);
	        
        	objRsMinutaModel = new RelMinutaModel();	        
        	objRsMinutaModel.setDataAtual(f.isNull("DATA_ATUAL") ? "" : f.getString("DATA_ATUAL"));
        	objRsMinutaModel.setHoraAtual(f.isNull("HORA_ATUAL") ? "" : f.getString("HORA_ATUAL"));
        	objRsMinutaModel.setDataAbertura(f.isNull("DT_ABERTURA") ? "" : f.getString("DT_ABERTURA"));
        	objRsMinutaModel.setTxAbertura(f.isNull("TX_ABERTURA") ? "" : f.getString("TX_ABERTURA"));
	        	
		} catch (WebServiceException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		    FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "hyb_erro.xhtml?&strErro=" + e.getMessage());
        }
    }
	
	public void ObjRsMinutaEquipe(List<RelMinutaEquipeModel> lista, String busca) {	
       try {
	        retorno = objRs.consultarEquipeOs(strNrSeqOs, busca);
	        JSONObject rstemp = new JSONObject(retorno);
	        JSONArray pcursor = rstemp.getJSONArray("PCURSOR");
	        String tpDesi;
	        String cargo;
	        
	        for (int i = 0; i < pcursor.length(); i++) {
		        JSONObject f = pcursor.getJSONObject(i);
	        	objRsMinutaEquipeModel = new RelMinutaEquipeModel();
	        	objRsMinutaEquipeModel.setNome(f.isNull("NM_RECU_OCOR_ESPC") ? "" : f.getString("NM_RECU_OCOR_ESPC"));
	        	objRsMinutaEquipeModel.setMatricula(f.isNull("NR_MATR_PRSV") ? "" : f.getString("NR_MATR_PRSV"));
	        	objRsMinutaEquipeModel.setDesignacao(f.isNull("NM_DESIGNACAO") ? "" : f.getString("NM_DESIGNACAO"));
	        	tpDesi=(f.isNull("TP_DESI") ? "" : f.getString("TP_DESI"));
	        	cargo=(f.isNull("CD_CARG") ? "" : f.getString("CD_CARG"));

		        if ( ( busca != "" && (cargo.equals(busca)) ) || ( busca == "" && ( tpDesi.equals("AD") || tpDesi.equals("AA") || tpDesi.equals("PSD") || tpDesi.equals("PSA") || tpDesi.equals("NA") ) ) ){
	        		lista.add(objRsMinutaEquipeModel); 
	        	}
	        }
	
		} catch (WebServiceException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		    FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "hyb_erro.xhtml?&strErro=" + e.getMessage());
        }
    }

	public String getStrNrSeqOs() {
		return strNrSeqOs;
	}

	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}

	public String getTextfield() {
		return textfield;
	}

	public void setTextfield(String textfield) {
		this.textfield = textfield;
	}

	public String getLogin_usuario() {
		return login_usuario;
	}

	public void setLogin_usuario(String login_usuario) {
		this.login_usuario = login_usuario;
	}

	public RelMinutaModel getObjRsMinutaModel() {
		return objRsMinutaModel;
	}

	public void setObjRsMinutaModel(RelMinutaModel objRsMinutaModel) {
		this.objRsMinutaModel = objRsMinutaModel;
	}

	public List<RelMinutaEquipeModel> getObjRsMinutaEquipe() {
		return objRsMinutaEquipe;
	}

	public void setObjRsMinutaEquipe(List<RelMinutaEquipeModel> objRsMinutaEquipe) {
		this.objRsMinutaEquipe = objRsMinutaEquipe;
	}

	public List<RelMinutaEquipeModel> getObjRsMinutaEquipe1() {
		return objRsMinutaEquipe1;
	}

	public void setObjRsMinutaEquipe1(List<RelMinutaEquipeModel> objRsMinutaEquipe1) {
		this.objRsMinutaEquipe1 = objRsMinutaEquipe1;
	}
}
