package com.altec.bsbr.app.hyb.web.util;

public class ProdutosAuxiliares {
	private String NM_PROD_AUXI;
	private String VL_EXCL_PRCL;
	private String VL_RECR_MES_PRCL;
	private String VL_RECR_FORA_MES_PRCL;
	private String VL_COMP;
	private String VL_TRST;
	private String VL_NEGA;
	private String NR_SEQU_TRAN_FRAU_CNAL;
	
	public String getNM_PROD_AUXI() {
		return NM_PROD_AUXI;
	}
	public void setNM_PROD_AUXI(String nM_PROD_AUXI) {
		NM_PROD_AUXI = nM_PROD_AUXI;
	}
	public String getVL_EXCL_PRCL() {
		return VL_EXCL_PRCL;
	}
	public void setVL_EXCL_PRCL(String vL_EXCL_PRCL) {
		VL_EXCL_PRCL = vL_EXCL_PRCL;
	}
	public String getVL_RECR_MES_PRCL() {
		return VL_RECR_MES_PRCL;
	}
	public void setVL_RECR_MES_PRCL(String vL_RECR_MES_PRCL) {
		VL_RECR_MES_PRCL = vL_RECR_MES_PRCL;
	}
	public String getVL_RECR_FORA_MES_PRCL() {
		return VL_RECR_FORA_MES_PRCL;
	}
	public void setVL_RECR_FORA_MES_PRCL(String vL_RECR_FORA_MES_PRCL) {
		VL_RECR_FORA_MES_PRCL = vL_RECR_FORA_MES_PRCL;
	}
	public String getVL_COMP() {
		return VL_COMP;
	}
	public void setVL_COMP(String vL_COMP) {
		VL_COMP = vL_COMP;
	}
	public String getVL_TRST() {
		return VL_TRST;
	}
	public void setVL_TRST(String vL_TRST) {
		VL_TRST = vL_TRST;
	}
	public String getVL_NEGA() {
		return VL_NEGA;
	}
	public void setVL_NEGA(String vL_NEGA) {
		VL_NEGA = vL_NEGA;
	}
	public String getNR_SEQU_TRAN_FRAU_CNAL() {
		return NR_SEQU_TRAN_FRAU_CNAL;
	}
	public void setNR_SEQU_TRAN_FRAU_CNAL(String nR_SEQU_TRAN_FRAU_CNAL) {
		NR_SEQU_TRAN_FRAU_CNAL = nR_SEQU_TRAN_FRAU_CNAL;
	}
}
