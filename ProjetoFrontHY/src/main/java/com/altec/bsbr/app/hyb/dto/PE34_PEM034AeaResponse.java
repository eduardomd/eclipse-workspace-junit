package com.altec.bsbr.app.hyb.dto;

public class PE34_PEM034AeaResponse {

	public PE34_PEM034AeaResponse() {}
	
	public PE34_PEM034AeaResponse(String dC_FORMATO, String pENUMPE, String pECDGEN, String pEMAREM, String pENOMEM,
			String pEEMPEM, String pEDESPR, String pECODSE, String pEDESSE, String pECAREM, String fECINGR,
			String pETIPAC, String pENIVES, String pEFECFA, String pEPERCA, String pETIPRE, String pEIMPIN,
			String pEFECIN, String pEPERIO, String pETIPCO, String pENOMR1, String pEINDC1, String pEPRET1,
			String pETELR1, String pERAMA1, String pENOMR2, String pEINDC2, String pREPET2, String pETELR2,
			String pERAMA2, String pENUDCN, String pEVALPE, String pEFECAD, String pEULTTI) {
		super();
		DC_FORMATO = dC_FORMATO;
		PENUMPE = pENUMPE;
		PECDGEN = pECDGEN;
		PEMAREM = pEMAREM;
		PENOMEM = pENOMEM;
		PEEMPEM = pEEMPEM;
		PEDESPR = pEDESPR;
		PECODSE = pECODSE;
		PEDESSE = pEDESSE;
		PECAREM = pECAREM;
		FECINGR = fECINGR;
		PETIPAC = pETIPAC;
		PENIVES = pENIVES;
		PEFECFA = pEFECFA;
		PEPERCA = pEPERCA;
		PETIPRE = pETIPRE;
		PEIMPIN = pEIMPIN;
		PEFECIN = pEFECIN;
		PEPERIO = pEPERIO;
		PETIPCO = pETIPCO;
		PENOMR1 = pENOMR1;
		PEINDC1 = pEINDC1;
		PEPRET1 = pEPRET1;
		PETELR1 = pETELR1;
		PERAMA1 = pERAMA1;
		PENOMR2 = pENOMR2;
		PEINDC2 = pEINDC2;
		PREPET2 = pREPET2;
		PETELR2 = pETELR2;
		PERAMA2 = pERAMA2;
		PENUDCN = pENUDCN;
		PEVALPE = pEVALPE;
		PEFECAD = pEFECAD;
		PEULTTI = pEULTTI;
	}
	
	private String DC_FORMATO;
	
	private String PENUMPE;
	
	private String PECDGEN;
	
	private String PEMAREM;
	
	private String PENOMEM;
	
	private String PEEMPEM;
	
	private String PEDESPR;
	
	private String PECODSE;
	
	private String PEDESSE;
	
	private String PECAREM;
	
	private String FECINGR;
	
	private String PETIPAC;
	
	private String PENIVES;
	
	private String PEFECFA;
	
	private String PEPERCA;
	
	private String PETIPRE;
	
	private String PEIMPIN;
	
	private String PEFECIN;
	
	private String PEPERIO;
	
	private String PETIPCO;
	
	private String PENOMR1;
	
	private String PEINDC1;
	
	private String PEPRET1;
	
	private String PETELR1;
	
	private String PERAMA1;
	
	private String PENOMR2;
	
	private String PEINDC2;
	
	private String PREPET2;
	
	private String PETELR2;
	
	private String PERAMA2;
	
	private String PENUDCN;
	
	private String PEVALPE;
	
	private String PEFECAD;
	
	private String PEULTTI;
	
	public String getDC_FORMATO() {
		return DC_FORMATO;
	}
	public void setDC_FORMATO(String dC_FORMATO) {
		DC_FORMATO = dC_FORMATO;
	}
	public String getPENUMPE() {
		return PENUMPE;
	}
	public void setPENUMPE(String pENUMPE) {
		PENUMPE = pENUMPE;
	}
	public String getPECDGEN() {
		return PECDGEN;
	}
	public void setPECDGEN(String pECDGEN) {
		PECDGEN = pECDGEN;
	}
	public String getPEMAREM() {
		return PEMAREM;
	}
	public void setPEMAREM(String pEMAREM) {
		PEMAREM = pEMAREM;
	}
	public String getPENOMEM() {
		return PENOMEM;
	}
	public void setPENOMEM(String pENOMEM) {
		PENOMEM = pENOMEM;
	}
	public String getPEEMPEM() {
		return PEEMPEM;
	}
	public void setPEEMPEM(String pEEMPEM) {
		PEEMPEM = pEEMPEM;
	}
	public String getPEDESPR() {
		return PEDESPR;
	}
	public void setPEDESPR(String pEDESPR) {
		PEDESPR = pEDESPR;
	}
	public String getPECODSE() {
		return PECODSE;
	}
	public void setPECODSE(String pECODSE) {
		PECODSE = pECODSE;
	}
	public String getPEDESSE() {
		return PEDESSE;
	}
	public void setPEDESSE(String pEDESSE) {
		PEDESSE = pEDESSE;
	}
	public String getPECAREM() {
		return PECAREM;
	}
	public void setPECAREM(String pECAREM) {
		PECAREM = pECAREM;
	}
	public String getFECINGR() {
		return FECINGR;
	}
	public void setFECINGR(String fECINGR) {
		FECINGR = fECINGR;
	}
	public String getPETIPAC() {
		return PETIPAC;
	}
	public void setPETIPAC(String pETIPAC) {
		PETIPAC = pETIPAC;
	}
	public String getPENIVES() {
		return PENIVES;
	}
	public void setPENIVES(String pENIVES) {
		PENIVES = pENIVES;
	}
	public String getPEFECFA() {
		return PEFECFA;
	}
	public void setPEFECFA(String pEFECFA) {
		PEFECFA = pEFECFA;
	}
	public String getPEPERCA() {
		return PEPERCA;
	}
	public void setPEPERCA(String pEPERCA) {
		PEPERCA = pEPERCA;
	}
	public String getPETIPRE() {
		return PETIPRE;
	}
	public void setPETIPRE(String pETIPRE) {
		PETIPRE = pETIPRE;
	}
	public String getPEIMPIN() {
		return PEIMPIN;
	}
	public void setPEIMPIN(String pEIMPIN) {
		PEIMPIN = pEIMPIN;
	}
	public String getPEFECIN() {
		return PEFECIN;
	}
	public void setPEFECIN(String pEFECIN) {
		PEFECIN = pEFECIN;
	}
	public String getPEPERIO() {
		return PEPERIO;
	}
	public void setPEPERIO(String pEPERIO) {
		PEPERIO = pEPERIO;
	}
	public String getPETIPCO() {
		return PETIPCO;
	}
	public void setPETIPCO(String pETIPCO) {
		PETIPCO = pETIPCO;
	}
	public String getPENOMR1() {
		return PENOMR1;
	}
	public void setPENOMR1(String pENOMR1) {
		PENOMR1 = pENOMR1;
	}
	public String getPEINDC1() {
		return PEINDC1;
	}
	public void setPEINDC1(String pEINDC1) {
		PEINDC1 = pEINDC1;
	}
	public String getPEPRET1() {
		return PEPRET1;
	}
	public void setPEPRET1(String pEPRET1) {
		PEPRET1 = pEPRET1;
	}
	public String getPETELR1() {
		return PETELR1;
	}
	public void setPETELR1(String pETELR1) {
		PETELR1 = pETELR1;
	}
	public String getPERAMA1() {
		return PERAMA1;
	}
	public void setPERAMA1(String pERAMA1) {
		PERAMA1 = pERAMA1;
	}
	public String getPENOMR2() {
		return PENOMR2;
	}
	public void setPENOMR2(String pENOMR2) {
		PENOMR2 = pENOMR2;
	}
	public String getPEINDC2() {
		return PEINDC2;
	}
	public void setPEINDC2(String pEINDC2) {
		PEINDC2 = pEINDC2;
	}
	public String getPREPET2() {
		return PREPET2;
	}
	public void setPREPET2(String pREPET2) {
		PREPET2 = pREPET2;
	}
	public String getPETELR2() {
		return PETELR2;
	}
	public void setPETELR2(String pETELR2) {
		PETELR2 = pETELR2;
	}
	public String getPERAMA2() {
		return PERAMA2;
	}
	public void setPERAMA2(String pERAMA2) {
		PERAMA2 = pERAMA2;
	}
	public String getPENUDCN() {
		return PENUDCN;
	}
	public void setPENUDCN(String pENUDCN) {
		PENUDCN = pENUDCN;
	}
	public String getPEVALPE() {
		return PEVALPE;
	}
	public void setPEVALPE(String pEVALPE) {
		PEVALPE = pEVALPE;
	}
	public String getPEFECAD() {
		return PEFECAD;
	}
	public void setPEFECAD(String pEFECAD) {
		PEFECAD = pEFECAD;
	}
	public String getPEULTTI() {
		return PEULTTI;
	}
	public void setPEULTTI(String pEULTTI) {
		PEULTTI = pEULTTI;
	}

}
