package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.RelosRecuperacaoFraudeModel;
import com.altec.bsbr.app.hyb.dto.UsuarioIncModel;
import com.altec.bsbr.app.hyb.web.util.XHYUsuarioIncService;
import com.altec.bsbr.app.jab.hyb.webclient.XHYRelatorios.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYRelatorios.XHYRelatoriosEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;

@Component("relOsRecuperacaoFraudeBean")
@Scope("session")
public class RelosRecuperacaoFraude extends BasicBBean {
	private static final long serialVersionUID = 1L;

	@Autowired
	private XHYRelatoriosEndPoint objRelat;

	// Session
	@Autowired
	private XHYUsuarioIncService user;

	private UsuarioIncModel userData;

	private String strDtIni;
	private String strDtFim;
	private String strCodArea;
	private String strArea;
	private List<RelosRecuperacaoFraudeModel> objRsRelat = new ArrayList<RelosRecuperacaoFraudeModel>();
	private List<RelosRecuperacaoFraudeModel> objRsRelatMes = new ArrayList<RelosRecuperacaoFraudeModel>();
	private List<RelosRecuperacaoFraudeModel> objRsRelatAcum = new ArrayList<RelosRecuperacaoFraudeModel>();

	ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
	JSONArray pcursorObjRsRelat = new JSONArray();
	JSONArray pcursorObjRelatMes = new JSONArray();
	JSONArray pcursorObjRsRelatAcum = new JSONArray();

	private double valorTotal;

	@PostConstruct
	public void init() {
		FacesContext fc = FacesContext.getCurrentInstance();
		@SuppressWarnings("unchecked")
		Map<String, String> params = fc.getExternalContext().getRequestParameterMap();

		this.strDtIni = params.get("pDtIni");
		this.strDtFim = params.get("pDtFim");
		// this.strCodArea = params.get("pCodArea");
		// this.strArea = params.get("pArea");

		if (user != null) {
			try {
				userData = user.getDadosUsuario();

				if (userData.getNomeAreaUsuario().isEmpty() || userData.getNomeAreaUsuario().equals("")) {
					this.strArea = "GOE";
				} else {
					this.strArea = userData.getNomeAreaUsuario();
				}

				this.strCodArea = userData.getCdAreaUsuario();

				System.out.println("CÃ³digo de area: " + userData.getCdAreaUsuario());
				System.out.println("Nome Ã¡rea: " + userData.getNomeAreaUsuario());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		try {
			String objRsRelat = objRelat.fnRelRecuperacaoFraude(this.strDtIni, this.strDtFim, this.strCodArea);
			JSONObject objResRelatTemp = new JSONObject(objRsRelat);
			pcursorObjRsRelat = objResRelatTemp.getJSONArray("PCURSOR");

			String objRsRelatMes = objRelat.fnRelRecuperacaoFraudeMes(this.strDtIni, this.strDtFim, this.strCodArea);
			JSONObject objRsRelatMesTemp = new JSONObject(objRsRelatMes);
			pcursorObjRelatMes = objRsRelatMesTemp.getJSONArray("PCURSOR");

			String objRsRelatAcum = objRelat.fnRelRecuperacaoFraudeAcum(this.strDtIni, this.strDtFim, this.strCodArea);
			JSONObject objRsRelatAcumTemp = new JSONObject(objRsRelatAcum);
			pcursorObjRsRelatAcum = objRsRelatAcumTemp.getJSONArray("PCURSOR");
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		for (int i = 0; i < pcursorObjRsRelat.length(); i++) {
			JSONObject temp = pcursorObjRsRelat.getJSONObject(i);

			objRsRelat.add(new RelosRecuperacaoFraudeModel(temp.isNull("MES_ANO") ? "" : temp.get("MES_ANO").toString(),
					temp.isNull("TOTAL_MES") ? "" : temp.get("TOTAL_MES").toString(),
					temp.isNull("INTERNET_BANKING") ? "" : temp.get("INTERNET_BANKING").toString(),
					temp.isNull("SUPERLINHA") ? "" : temp.get("SUPERLINHA").toString(),
					temp.isNull("CARTAO_CREDITO") ? "" : temp.get("CARTAO_CREDITO").toString(),
					temp.isNull("CARTAO_DEBITO") ? "" : temp.get("CARTAO_DEBITO").toString(),
					temp.isNull("CONTA_BANCARIA") ? "" : temp.get("CONTA_BANCARIA").toString(),
					temp.isNull("TOTAL_MES") ? "" : formataNumero(temp.get("TOTAL_MES").toString()),
					temp.isNull("INTERNET_BANKING") ? "" : formataNumero(temp.get("INTERNET_BANKING").toString()),
					temp.isNull("SUPERLINHA") ? "" : formataNumero(temp.get("SUPERLINHA").toString()),
					temp.isNull("CARTAO_CREDITO") ? "" : formataNumero(temp.get("CARTAO_CREDITO").toString()),
					temp.isNull("CARTAO_DEBITO") ? "" : formataNumero(temp.get("CARTAO_DEBITO").toString()),
					temp.isNull("CONTA_BANCARIA") ? "" : formataNumero(temp.get("CONTA_BANCARIA").toString())));
		}

		for (int i = 0; i < pcursorObjRelatMes.length(); i++) {
			JSONObject temp = pcursorObjRelatMes.getJSONObject(i);

			objRsRelatMes
					.add(new RelosRecuperacaoFraudeModel(temp.isNull("MES_ANO") ? "" : temp.get("MES_ANO").toString(),
							temp.isNull("TOTAL_MES") ? "" : temp.get("TOTAL_MES").toString(),
							temp.isNull("INTERNET_BANKING") ? "" : temp.get("INTERNET_BANKING").toString(),
							temp.isNull("SUPERLINHA") ? "" : temp.get("SUPERLINHA").toString(),
							temp.isNull("CARTAO_CREDITO") ? "" : temp.get("CARTAO_CREDITO").toString(),
							temp.isNull("CARTAO_DEBITO") ? "" : temp.get("CARTAO_DEBITO").toString(),
							temp.isNull("CONTA_BANCARIA") ? "" : temp.get("CONTA_BANCARIA").toString(),
							temp.isNull("TOTAL_MES") ? "" : formataNumero(temp.get("TOTAL_MES").toString()),
							temp.isNull("INTERNET_BANKING") ? ""
									: formataNumero(temp.get("INTERNET_BANKING").toString()),
							temp.isNull("SUPERLINHA") ? "" : formataNumero(temp.get("SUPERLINHA").toString()),
							temp.isNull("CARTAO_CREDITO") ? "" : formataNumero(temp.get("CARTAO_CREDITO").toString()),
							temp.isNull("CARTAO_DEBITO") ? "" : formataNumero(temp.get("CARTAO_DEBITO").toString()),
							temp.isNull("CONTA_BANCARIA") ? "" : formataNumero(temp.get("CONTA_BANCARIA").toString())));
		}

		for (int i = 0; i < pcursorObjRsRelatAcum.length(); i++) {
			JSONObject temp = pcursorObjRsRelatAcum.getJSONObject(i);

			objRsRelatAcum
					.add(new RelosRecuperacaoFraudeModel(temp.isNull("MES_ANO") ? "" : temp.get("MES_ANO").toString(),
							temp.isNull("TOTAL_MES") ? "" : temp.get("TOTAL_MES").toString(),
							temp.isNull("INTERNET_BANKING") ? "" : temp.get("INTERNET_BANKING").toString(),
							temp.isNull("SUPERLINHA") ? "" : temp.get("SUPERLINHA").toString(),
							temp.isNull("CARTAO_CREDITO") ? "" : temp.get("CARTAO_CREDITO").toString(),
							temp.isNull("CARTAO_DEBITO") ? "" : temp.get("CARTAO_DEBITO").toString(),
							temp.isNull("CONTA_BANCARIA") ? "" : temp.get("CONTA_BANCARIA").toString(),
							temp.isNull("TOTAL_MES") ? "" : formataNumero(temp.get("TOTAL_MES").toString()),
							temp.isNull("INTERNET_BANKING") ? ""
									: formataNumero(temp.get("INTERNET_BANKING").toString()),
							temp.isNull("SUPERLINHA") ? "" : formataNumero(temp.get("SUPERLINHA").toString()),
							temp.isNull("CARTAO_CREDITO") ? "" : formataNumero(temp.get("CARTAO_CREDITO").toString()),
							temp.isNull("CARTAO_DEBITO") ? "" : formataNumero(temp.get("CARTAO_DEBITO").toString()),
							temp.isNull("CONTA_BANCARIA") ? "" : formataNumero(temp.get("CONTA_BANCARIA").toString())));
		}
		cleanSession();
	}

	public void cleanSession() {
		FacesContext context = FacesContext.getCurrentInstance();
		if (context.getExternalContext().getSessionMap().get("relatorioosBean") != null) {
			context.getExternalContext().getSessionMap().remove("relatorioosBean");
		}
	}

	public String formataNumero(String number) {
		String result = "";
		if (number != null && !number.isEmpty()) {
			DecimalFormat value = new DecimalFormat("###,##0.00");
			result = value.format(Float.parseFloat(number));
			String fp = result.substring(0, result.length() - 3);
			String sp = result.substring(result.length() - 3);
			result = fp.replace(",", ".") + sp.replace(".", ",");
		}

		if (result.equals("0,00")) {
			return "0";
		}

		return result;
	}

	public String now() {
		DateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		Date date = new Date();
		return dateformat.format(date);
	}

	public String getStrDtIni() {
		return strDtIni;
	}

	public void setStrDtIni(String strDtIni) {
		this.strDtIni = strDtIni;
	}

	public String getStrDtFim() {
		return strDtFim;
	}

	public void setStrDtFim(String strDtFim) {
		this.strDtFim = strDtFim;
	}

	public List<RelosRecuperacaoFraudeModel> getObjRsRelat() {
		return objRsRelat;
	}

	public void setObjRsRelat(List<RelosRecuperacaoFraudeModel> objRsRelat) {
		this.objRsRelat = objRsRelat;
	}

	public List<RelosRecuperacaoFraudeModel> getObjRsRelatMes() {
		return objRsRelatMes;
	}

	public void setObjRsRelatMes(List<RelosRecuperacaoFraudeModel> objRsRelatMes) {
		this.objRsRelatMes = objRsRelatMes;
	}

	public List<RelosRecuperacaoFraudeModel> getObjRsRelatAcum() {
		return objRsRelatAcum;
	}

	public void setObjRsRelatAcum(List<RelosRecuperacaoFraudeModel> objRsRelatAcum) {
		this.objRsRelatAcum = objRsRelatAcum;
	}

	public String getStrArea() {
		return strArea;
	}

	public void setStrArea(String strArea) {
		this.strArea = strArea;
	}

	public void postProcessExcel(Object doc) {
		try {
			XSSFWorkbook wb = (XSSFWorkbook) doc;
			XSSFSheet sheet = wb.getSheetAt(0);
			sheet.setDisplayGridlines(false);

			formatarCabecalho(wb, sheet);
			formatarCabecalhoTituloTabelas(wb, sheet);
			formatarCabecalhoTabela(wb, sheet);
			formatarRegistrosTabela(wb, sheet);
			criarRodape(wb, sheet);

			sheet.shiftRows(3, sheet.getLastRowNum() + 1, 2);
			sheet.shiftRows(6, sheet.getLastRowNum() + 1, 1);
		} catch (Exception e) {
			System.out.println(e);
		}

	}

	private void formatarCabecalho(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyle1 = wb.createCellStyle();
		headerStyle1.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle1.setWrapText(true);
		headerStyle1.setBorderBottom(CellStyle.BORDER_NONE);
		headerStyle1.setBorderLeft(CellStyle.BORDER_NONE);
		headerStyle1.setBorderRight(CellStyle.BORDER_NONE);
		headerStyle1.setBorderTop(CellStyle.BORDER_NONE);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 14);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		headerStyle1.setFont(font);

		XSSFRow primeiraLinha = sheet.getRow(1);
		if (primeiraLinha != null) {
			XSSFCell primeiraLinhaCell = primeiraLinha.getCell(0);
			primeiraLinhaCell.setCellStyle(headerStyle1);
			primeiraLinha.setHeightInPoints((short) 19);
		}
		XSSFRow segundaLinha = sheet.getRow(2);
		if (segundaLinha != null) {
			XSSFCell segundaLinhaCell = segundaLinha.getCell(0);
			segundaLinhaCell.setCellStyle(headerStyle1);
			segundaLinha.setHeightInPoints((short) 19);
		}
	}

	private void formatarCabecalhoTituloTabelas(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyle1 = wb.createCellStyle();
		headerStyle1.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle1.setWrapText(true);
		headerStyle1.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyle1.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyle1.setBorderRight(CellStyle.BORDER_THIN);
		headerStyle1.setBorderTop(CellStyle.BORDER_THIN);
		headerStyle1.setFillForegroundColor(IndexedColors.BRIGHT_GREEN.getIndex());
		headerStyle1.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle1.setFont(font);

		// Tabela Recuperação do Mês
		XSSFRow primeiroTitulo = sheet.getRow(4);
		if (primeiroTitulo != null) {
			XSSFCell primeiraLinhaCell = primeiroTitulo.getCell(0);
			primeiraLinhaCell.setCellStyle(headerStyle1);
			primeiroTitulo.setHeightInPoints((short) 19);
		}
		CellRangeAddress primeiroTituloMergedCells = new CellRangeAddress(4, 4, 0, 6);
		RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, primeiroTituloMergedCells, sheet, wb);
		RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, primeiroTituloMergedCells, sheet, wb);
		RegionUtil.setBorderRight(CellStyle.BORDER_THIN, primeiroTituloMergedCells, sheet, wb);
		RegionUtil.setBorderTop(CellStyle.BORDER_THIN, primeiroTituloMergedCells, sheet, wb);

		// Tabela Recuperação Relativo à Outros Períodos
		XSSFRow segundoTitulo = sheet.getRow(9 + objRsRelatMes.size());
		if (segundoTitulo != null) {
			XSSFCell segundaLinhaCell = segundoTitulo.getCell(0);
			segundaLinhaCell.setCellStyle(headerStyle1);
			segundoTitulo.setHeightInPoints((short) 19);
		}
		CellRangeAddress segundoTituloMergedCells = new CellRangeAddress(9 + objRsRelatMes.size(),
				9 + objRsRelatMes.size(), 0, 6);
		RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, segundoTituloMergedCells, sheet, wb);
		RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, segundoTituloMergedCells, sheet, wb);
		RegionUtil.setBorderRight(CellStyle.BORDER_THIN, segundoTituloMergedCells, sheet, wb);
		RegionUtil.setBorderTop(CellStyle.BORDER_THIN, segundoTituloMergedCells, sheet, wb);

		// Tabela Recuperação Total - Acumulado
		XSSFRow terceiroTitulo = sheet.getRow(14 + objRsRelat.size() + objRsRelatMes.size());
		if (terceiroTitulo != null) {
			XSSFCell segundaLinhaCell = terceiroTitulo.getCell(0);
			segundaLinhaCell.setCellStyle(headerStyle1);
			terceiroTitulo.setHeightInPoints((short) 19);
		}
		CellRangeAddress terceiroTituloMergedCells = new CellRangeAddress(14 + objRsRelat.size() + objRsRelatMes.size(),
				14 + objRsRelat.size() + objRsRelatMes.size(), 0, 6);
		RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, terceiroTituloMergedCells, sheet, wb);
		RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, terceiroTituloMergedCells, sheet, wb);
		RegionUtil.setBorderRight(CellStyle.BORDER_THIN, terceiroTituloMergedCells, sheet, wb);
		RegionUtil.setBorderTop(CellStyle.BORDER_THIN, terceiroTituloMergedCells, sheet, wb);

	}

	private void formatarCabecalhoTabela(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setWrapText(true);
		headerStyle.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyle.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyle.setBorderRight(CellStyle.BORDER_THIN);
		headerStyle.setBorderTop(CellStyle.BORDER_THIN);
		headerStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.WHITE.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(font);

		// Tabela Recuperação do Mês
		XSSFRow linhaRecuperacaoMesTabela = sheet.getRow(5);
		if (linhaRecuperacaoMesTabela != null) {
			for (int i = 0; i <= linhaRecuperacaoMesTabela.getLastCellNum(); i++) {
				XSSFCell celulaHeader = linhaRecuperacaoMesTabela.getCell(i);
				if (celulaHeader != null) {
					celulaHeader.setCellStyle(headerStyle);
				}
			}
		}

		XSSFRow linhaRecuperacaoMesTabela1 = sheet.getRow(6);
		if (linhaRecuperacaoMesTabela1 != null) {
			for (int i = 0; i <= linhaRecuperacaoMesTabela1.getLastCellNum(); i++) {
				XSSFCell celulaHeader = linhaRecuperacaoMesTabela1.getCell(i);
				if (celulaHeader != null) {
					celulaHeader.setCellStyle(headerStyle);
				}
			}
		}

		CellRangeAddress primeiroPeriodoCanalMergedCells = new CellRangeAddress(5, 6, 0, 0);
		RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, primeiroPeriodoCanalMergedCells, sheet, wb);
		RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, primeiroPeriodoCanalMergedCells, sheet, wb);
		RegionUtil.setBorderRight(CellStyle.BORDER_THIN, primeiroPeriodoCanalMergedCells, sheet, wb);
		RegionUtil.setBorderTop(CellStyle.BORDER_THIN, primeiroPeriodoCanalMergedCells, sheet, wb);

		CellRangeAddress primeiroTotalMergedCells = new CellRangeAddress(5, 6, 6, 6);
		RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, primeiroTotalMergedCells, sheet, wb);
		RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, primeiroTotalMergedCells, sheet, wb);
		RegionUtil.setBorderRight(CellStyle.BORDER_THIN, primeiroTotalMergedCells, sheet, wb);
		RegionUtil.setBorderTop(CellStyle.BORDER_THIN, primeiroTotalMergedCells, sheet, wb);

		// Tabela Recuperação Relativo à Outros Períodos
		XSSFRow linhaRecuparacaoOutrosPeriodosTabela = sheet.getRow(10 + objRsRelatMes.size());
		if (linhaRecuparacaoOutrosPeriodosTabela != null) {
			for (int i = 0; i <= linhaRecuparacaoOutrosPeriodosTabela.getLastCellNum(); i++) {
				XSSFCell celulaHeader = linhaRecuparacaoOutrosPeriodosTabela.getCell(i);
				if (celulaHeader != null) {
					celulaHeader.setCellStyle(headerStyle);
				}
			}
		}

		XSSFRow linhaRecuparacaoOutrosPeriodosTabela2 = sheet.getRow(11 + objRsRelatMes.size());
		if (linhaRecuparacaoOutrosPeriodosTabela2 != null) {
			for (int i = 0; i <= linhaRecuparacaoOutrosPeriodosTabela2.getLastCellNum(); i++) {
				XSSFCell celulaHeader = linhaRecuparacaoOutrosPeriodosTabela2.getCell(i);
				if (celulaHeader != null) {
					celulaHeader.setCellStyle(headerStyle);
				}
			}
		}

		CellRangeAddress segundoPeriodoCanalMergedCells = new CellRangeAddress(10 + objRsRelatMes.size(),
				11 + objRsRelatMes.size(), 0, 0);
		RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, segundoPeriodoCanalMergedCells, sheet, wb);
		RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, segundoPeriodoCanalMergedCells, sheet, wb);
		RegionUtil.setBorderRight(CellStyle.BORDER_THIN, segundoPeriodoCanalMergedCells, sheet, wb);
		RegionUtil.setBorderTop(CellStyle.BORDER_THIN, segundoPeriodoCanalMergedCells, sheet, wb);

		CellRangeAddress segundoTotalMergedCells = new CellRangeAddress(10 + objRsRelatMes.size(),
				11 + objRsRelatMes.size(), 6, 6);
		RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, segundoTotalMergedCells, sheet, wb);
		RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, segundoTotalMergedCells, sheet, wb);
		RegionUtil.setBorderRight(CellStyle.BORDER_THIN, segundoTotalMergedCells, sheet, wb);
		RegionUtil.setBorderTop(CellStyle.BORDER_THIN, segundoTotalMergedCells, sheet, wb);

		// Tabela Recuperação Total - Acumulado
		XSSFRow linhaRecuperacaoTotalTabela = sheet.getRow(15 + objRsRelat.size() + objRsRelatMes.size());
		if (linhaRecuperacaoTotalTabela != null) {
			for (int i = 0; i <= linhaRecuperacaoTotalTabela.getLastCellNum(); i++) {
				XSSFCell celulaHeader = linhaRecuperacaoTotalTabela.getCell(i);
				if (celulaHeader != null) {
					celulaHeader.setCellStyle(headerStyle);
				}
			}
		}

		XSSFRow linhaRecuperacaoTotalTabela2 = sheet.getRow(16 + objRsRelat.size() + objRsRelatMes.size());
		if (linhaRecuperacaoTotalTabela2 != null) {
			for (int i = 0; i <= linhaRecuperacaoTotalTabela2.getLastCellNum(); i++) {
				XSSFCell celulaHeader = linhaRecuperacaoTotalTabela2.getCell(i);
				if (celulaHeader != null) {
					celulaHeader.setCellStyle(headerStyle);
				}
			}
		}

		CellRangeAddress terceiroPeriodoCanalMergedCells = new CellRangeAddress(
				15 + objRsRelat.size() + objRsRelatMes.size(), 16 + objRsRelat.size() + objRsRelatMes.size(), 0, 0);
		RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, terceiroPeriodoCanalMergedCells, sheet, wb);
		RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, terceiroPeriodoCanalMergedCells, sheet, wb);
		RegionUtil.setBorderRight(CellStyle.BORDER_THIN, terceiroPeriodoCanalMergedCells, sheet, wb);
		RegionUtil.setBorderTop(CellStyle.BORDER_THIN, terceiroPeriodoCanalMergedCells, sheet, wb);

		CellRangeAddress terceiroTotalMergedCells = new CellRangeAddress(15 + objRsRelat.size() + objRsRelatMes.size(),
				16 + objRsRelat.size() + objRsRelatMes.size(), 6, 6);
		RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, terceiroTotalMergedCells, sheet, wb);
		RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, terceiroTotalMergedCells, sheet, wb);
		RegionUtil.setBorderRight(CellStyle.BORDER_THIN, terceiroTotalMergedCells, sheet, wb);
		RegionUtil.setBorderTop(CellStyle.BORDER_THIN, terceiroTotalMergedCells, sheet, wb);
	}

	private void formatarRegistrosTabela(XSSFWorkbook wb, XSSFSheet sheet) {

		if (objRsRelatMes != null && !objRsRelatMes.isEmpty()) {
			formatarLinhasDetalheMes(wb, sheet);
			formatarLinhaTotalMes(wb, sheet);
		} else {
			formatarDetalheVazioMes(wb, sheet);
		}

		if (objRsRelat != null && !objRsRelat.isEmpty()) {
			formatarLinhasDetalhe(wb, sheet);
			formatarLinhaTotal(wb, sheet);
		} else {
			formatarDetalheVazio(wb, sheet);
		}

		if (objRsRelatAcum != null && !objRsRelatAcum.isEmpty()) {
			formatarLinhasDetalheAcum(wb, sheet);
			formatarLinhaTotalAcum(wb, sheet);
		} else {
			formatarDetalheVazioAcum(wb, sheet);
		}

		for (int i = 0; i <= sheet.getRow(4).getLastCellNum(); i++) {
			sheet.autoSizeColumn(i);
		}
	}

	private void formatarDetalheVazio(XSSFWorkbook wb, XSSFSheet sheet) {

		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setWrapText(true);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(font);

		int row = 12;
		if (objRsRelatMes.size() > 0) {
			row = row + objRsRelatMes.size();
		}

		CellRangeAddress detalheVazio = new CellRangeAddress(row, row, 0, 6);
		sheet.addMergedRegion(detalheVazio);

		XSSFRow linhaVazia = sheet.getRow(row);
		if (linhaVazia == null) {
			linhaVazia = sheet.createRow(row);
		}
		XSSFCell celulaVazia = linhaVazia.createCell(0);
		linhaVazia.setHeightInPoints((short) 20);
		celulaVazia.setCellValue("Não existem dados para o período selecionado.");
		celulaVazia.setCellStyle(headerStyle);

		RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
		RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
		RegionUtil.setBorderRight(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
		RegionUtil.setBorderTop(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
	}

	private void formatarDetalheVazioMes(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setWrapText(true);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(font);

		CellRangeAddress detalheVazio = new CellRangeAddress(7, 7, 0, 6);
		sheet.addMergedRegion(detalheVazio);

		XSSFRow linhaVazia = sheet.getRow(7);
		if (linhaVazia == null) {
			linhaVazia = sheet.createRow(7);
		}
		XSSFCell celulaVazia = linhaVazia.createCell(0);
		linhaVazia.setHeightInPoints((short) 20);
		celulaVazia.setCellValue("Não existem dados para o período selecionado.");
		celulaVazia.setCellStyle(headerStyle);

		RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
		RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
		RegionUtil.setBorderRight(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
		RegionUtil.setBorderTop(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
	}

	private void formatarDetalheVazioAcum(XSSFWorkbook wb, XSSFSheet sheet) {

		XSSFCellStyle headerStyle = wb.createCellStyle();
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setWrapText(true);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyle.setFont(font);

		int row = 17;

		if (objRsRelat.size() > 0) {
			row = row + objRsRelat.size();
		}

		if (objRsRelatMes.size() > 0) {
			row = row + objRsRelatMes.size();
		}

		CellRangeAddress detalheVazio = new CellRangeAddress(row, row, 0, 6);
		sheet.addMergedRegion(detalheVazio);

		XSSFRow linhaVazia = sheet.getRow(row);
		if (linhaVazia == null) {
			linhaVazia = sheet.createRow(row);
		}
		XSSFCell celulaVazia = linhaVazia.createCell(0);
		linhaVazia.setHeightInPoints((short) 20);
		celulaVazia.setCellValue("Não existem dados para o período selecionado.");
		celulaVazia.setCellStyle(headerStyle);

		RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
		RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
		RegionUtil.setBorderRight(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
		RegionUtil.setBorderTop(CellStyle.BORDER_THIN, detalheVazio, sheet, wb);
	}

	private void formatarLinhasDetalheMes(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyleLeft = wb.createCellStyle();
		headerStyleLeft.setAlignment(CellStyle.ALIGN_LEFT);
		headerStyleLeft.setWrapText(true);
		headerStyleLeft.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderTop(CellStyle.BORDER_THIN);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		headerStyleLeft.setFont(font);

		XSSFCellStyle headerStyleRight = wb.createCellStyle();
		headerStyleRight.setAlignment(CellStyle.ALIGN_RIGHT);
		headerStyleRight.setWrapText(true);
		headerStyleRight.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderTop(CellStyle.BORDER_THIN);
		Font fontRight = wb.createFont();
		fontRight.setFontHeightInPoints((short) 11);
		fontRight.setFontName("Calibri");
		fontRight.setColor(IndexedColors.BLACK.getIndex());
		fontRight.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		headerStyleRight.setFont(fontRight);

		int linhaInicial = 7;
		int linhasDetalhe = objRsRelatMes.size();
		int linhaFinal = linhaInicial + linhasDetalhe;

		for (int linha = linhaInicial; linha <= linhaFinal; linha++) {
			XSSFRow detalheTabela = sheet.getRow(linha);
			if (detalheTabela != null) {
				for (int i = 0; i <= detalheTabela.getLastCellNum(); i++) {
					XSSFCell celulaDetalhe = detalheTabela.getCell(i);
					if (celulaDetalhe != null) {
						if (i == 0) {
							celulaDetalhe.setCellStyle(headerStyleLeft);
						} else {
							celulaDetalhe.setCellStyle(headerStyleRight);
						}

					}
				}
			}
		}
	}

	private void formatarLinhasDetalhe(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyleLeft = wb.createCellStyle();
		headerStyleLeft.setAlignment(CellStyle.ALIGN_LEFT);
		headerStyleLeft.setWrapText(true);
		headerStyleLeft.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderTop(CellStyle.BORDER_THIN);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		headerStyleLeft.setFont(font);

		XSSFCellStyle headerStyleRight = wb.createCellStyle();
		headerStyleRight.setAlignment(CellStyle.ALIGN_RIGHT);
		headerStyleRight.setWrapText(true);
		headerStyleRight.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderTop(CellStyle.BORDER_THIN);
		Font fontRight = wb.createFont();
		fontRight.setFontHeightInPoints((short) 11);
		fontRight.setFontName("Calibri");
		fontRight.setColor(IndexedColors.BLACK.getIndex());
		fontRight.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		headerStyleRight.setFont(fontRight);

		int linhaInicial = 12 + objRsRelatMes.size();
		int linhasDetalhe = objRsRelat.size();
		int linhaFinal = linhaInicial + linhasDetalhe;

		for (int linha = linhaInicial; linha <= linhaFinal; linha++) {
			XSSFRow detalheTabela = sheet.getRow(linha);
			if (detalheTabela != null) {
				for (int i = 0; i <= detalheTabela.getLastCellNum(); i++) {
					XSSFCell celulaDetalhe = detalheTabela.getCell(i);
					if (celulaDetalhe != null) {
						if (i == 0) {
							celulaDetalhe.setCellStyle(headerStyleLeft);
						} else {
							celulaDetalhe.setCellStyle(headerStyleRight);
						}

					}
				}
			}
		}
	}

	private void formatarLinhasDetalheAcum(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyleLeft = wb.createCellStyle();
		headerStyleLeft.setAlignment(CellStyle.ALIGN_LEFT);
		headerStyleLeft.setWrapText(true);
		headerStyleLeft.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderTop(CellStyle.BORDER_THIN);
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setFontName("Calibri");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		headerStyleLeft.setFont(font);

		XSSFCellStyle headerStyleRight = wb.createCellStyle();
		headerStyleRight.setAlignment(CellStyle.ALIGN_RIGHT);
		headerStyleRight.setWrapText(true);
		headerStyleRight.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderTop(CellStyle.BORDER_THIN);
		Font fontRight = wb.createFont();
		fontRight.setFontHeightInPoints((short) 11);
		fontRight.setFontName("Calibri");
		fontRight.setColor(IndexedColors.BLACK.getIndex());
		fontRight.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		headerStyleRight.setFont(fontRight);

		int linhaInicial = 17 + objRsRelatMes.size() + objRsRelat.size();
		int linhasDetalhe = objRsRelatAcum.size();
		int linhaFinal = linhaInicial + linhasDetalhe;

		for (int linha = linhaInicial; linha <= linhaFinal; linha++) {
			XSSFRow detalheTabela = sheet.getRow(linha);
			if (detalheTabela != null) {
				for (int i = 0; i <= detalheTabela.getLastCellNum(); i++) {
					XSSFCell celulaDetalhe = detalheTabela.getCell(i);
					if (celulaDetalhe != null) {
						if (i == 0) {
							celulaDetalhe.setCellStyle(headerStyleLeft);
						} else {
							celulaDetalhe.setCellStyle(headerStyleRight);
						}

					}
				}
			}
		}
	}

	private void formatarLinhaTotalMes(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyleLeft = wb.createCellStyle();
		headerStyleLeft.setAlignment(CellStyle.ALIGN_LEFT);
		headerStyleLeft.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderTop(CellStyle.BORDER_THIN);
		headerStyleLeft.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		headerStyleLeft.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		Font fontCenter = wb.createFont();
		fontCenter.setFontHeightInPoints((short) 11);
		fontCenter.setFontName("Calibri");
		fontCenter.setColor(IndexedColors.WHITE.getIndex());
		fontCenter.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyleLeft.setFont(fontCenter);

		XSSFCellStyle headerStyleRight = wb.createCellStyle();
		headerStyleRight.setAlignment(CellStyle.ALIGN_RIGHT);
		headerStyleRight.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderTop(CellStyle.BORDER_THIN);
		headerStyleRight.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		headerStyleRight.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		Font fontCenterCenter = wb.createFont();
		fontCenterCenter.setFontHeightInPoints((short) 11);
		fontCenterCenter.setFontName("Calibri");
		fontCenterCenter.setColor(IndexedColors.WHITE.getIndex());
		fontCenterCenter.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyleRight.setFont(fontCenterCenter);

		XSSFRow linhaTotal = sheet.getRow(7 + objRsRelatMes.size());
		if (linhaTotal == null) {
			linhaTotal = sheet.createRow(7 + objRsRelatMes.size());
		}

		XSSFCell celula1 = linhaTotal.createCell(0);
		celula1.setCellValue("Total:");
		celula1.setCellStyle(headerStyleLeft);

		XSSFCell celula2 = linhaTotal.createCell(1);
		celula2.setCellValue(this.valorTotalCartaoDebitoMes());
		celula2.setCellStyle(headerStyleRight);

		XSSFCell celula3 = linhaTotal.createCell(2);
		celula3.setCellValue(this.valorTotalCartaoCreditoMes());
		celula3.setCellStyle(headerStyleRight);

		XSSFCell celula4 = linhaTotal.createCell(3);
		celula4.setCellValue(this.valorTotalInternetMes());
		celula4.setCellStyle(headerStyleRight);

		XSSFCell celula5 = linhaTotal.createCell(4);
		celula5.setCellValue(this.valorTotalSuperlinhaMes());
		celula5.setCellStyle(headerStyleRight);

		XSSFCell celula6 = linhaTotal.createCell(5);
		celula6.setCellValue(this.valorTotalContaBancariaMes());
		celula6.setCellStyle(headerStyleRight);

		XSSFCell celula7 = linhaTotal.createCell(6);
		celula7.setCellValue(this.valorTotalTotalMes());
		celula7.setCellStyle(headerStyleRight);
	}

	private void formatarLinhaTotal(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyleLeft = wb.createCellStyle();
		headerStyleLeft.setAlignment(CellStyle.ALIGN_LEFT);
		headerStyleLeft.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderTop(CellStyle.BORDER_THIN);
		headerStyleLeft.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		headerStyleLeft.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		Font fontCenter = wb.createFont();
		fontCenter.setFontHeightInPoints((short) 11);
		fontCenter.setFontName("Calibri");
		fontCenter.setColor(IndexedColors.WHITE.getIndex());
		fontCenter.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyleLeft.setFont(fontCenter);

		XSSFCellStyle headerStyleRight = wb.createCellStyle();
		headerStyleRight.setAlignment(CellStyle.ALIGN_RIGHT);
		headerStyleRight.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderTop(CellStyle.BORDER_THIN);
		headerStyleRight.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		headerStyleRight.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		Font fontCenterCenter = wb.createFont();
		fontCenterCenter.setFontHeightInPoints((short) 11);
		fontCenterCenter.setFontName("Calibri");
		fontCenterCenter.setColor(IndexedColors.WHITE.getIndex());
		fontCenterCenter.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyleRight.setFont(fontCenterCenter);

		XSSFRow linhaTotal = sheet.getRow(12 + objRsRelatMes.size() + objRsRelat.size());
		if (linhaTotal == null) {
			linhaTotal = sheet.createRow(12 + objRsRelatMes.size() + objRsRelat.size());
		}

		XSSFCell celula1 = linhaTotal.createCell(0);
		celula1.setCellValue("Total:");
		celula1.setCellStyle(headerStyleLeft);

		XSSFCell celula2 = linhaTotal.createCell(1);
		celula2.setCellValue(this.valorTotalCartaoDebito());
		celula2.setCellStyle(headerStyleRight);

		XSSFCell celula3 = linhaTotal.createCell(2);
		celula3.setCellValue(this.valorTotalCartaoCredito());
		celula3.setCellStyle(headerStyleRight);

		XSSFCell celula4 = linhaTotal.createCell(3);
		celula4.setCellValue(this.valorTotalInternet());
		celula4.setCellStyle(headerStyleRight);

		XSSFCell celula5 = linhaTotal.createCell(4);
		celula5.setCellValue(this.valorTotalSuperlinha());
		celula5.setCellStyle(headerStyleRight);

		XSSFCell celula6 = linhaTotal.createCell(5);
		celula6.setCellValue(this.valorTotalContaBancaria());
		celula6.setCellStyle(headerStyleRight);

		XSSFCell celula7 = linhaTotal.createCell(6);
		celula7.setCellValue(this.valorTotalTotal());
		celula7.setCellStyle(headerStyleRight);
	}

	private void formatarLinhaTotalAcum(XSSFWorkbook wb, XSSFSheet sheet) {
		XSSFCellStyle headerStyleLeft = wb.createCellStyle();
		headerStyleLeft.setAlignment(CellStyle.ALIGN_LEFT);
		headerStyleLeft.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleLeft.setBorderTop(CellStyle.BORDER_THIN);
		headerStyleLeft.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		headerStyleLeft.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		Font fontCenter = wb.createFont();
		fontCenter.setFontHeightInPoints((short) 11);
		fontCenter.setFontName("Calibri");
		fontCenter.setColor(IndexedColors.WHITE.getIndex());
		fontCenter.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyleLeft.setFont(fontCenter);

		XSSFCellStyle headerStyleRight = wb.createCellStyle();
		headerStyleRight.setAlignment(CellStyle.ALIGN_RIGHT);
		headerStyleRight.setBorderBottom(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderLeft(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderRight(CellStyle.BORDER_THIN);
		headerStyleRight.setBorderTop(CellStyle.BORDER_THIN);
		headerStyleRight.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		headerStyleRight.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		Font fontCenterCenter = wb.createFont();
		fontCenterCenter.setFontHeightInPoints((short) 11);
		fontCenterCenter.setFontName("Calibri");
		fontCenterCenter.setColor(IndexedColors.WHITE.getIndex());
		fontCenterCenter.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headerStyleRight.setFont(fontCenterCenter);

		// DEFINIR LINHA
		XSSFRow linhaTotal = sheet.getRow(17 + objRsRelatMes.size() + objRsRelat.size() + objRsRelatAcum.size());
		if (linhaTotal == null) {
			linhaTotal = sheet.createRow(17 + objRsRelatMes.size() + objRsRelat.size() + objRsRelatAcum.size());
		}

		XSSFCell celula1 = linhaTotal.createCell(0);
		celula1.setCellValue("Total:");
		celula1.setCellStyle(headerStyleLeft);

		XSSFCell celula2 = linhaTotal.createCell(1);
		celula2.setCellValue(this.valorTotalCartaoDebitoAcum());
		celula2.setCellStyle(headerStyleRight);

		XSSFCell celula3 = linhaTotal.createCell(2);
		celula3.setCellValue(this.valorTotalCartaoCreditoAcum());
		celula3.setCellStyle(headerStyleRight);

		XSSFCell celula4 = linhaTotal.createCell(3);
		celula4.setCellValue(this.valorTotalInternetAcum());
		celula4.setCellStyle(headerStyleRight);

		XSSFCell celula5 = linhaTotal.createCell(4);
		celula5.setCellValue(this.valorTotalSuperlinhaAcum());
		celula5.setCellStyle(headerStyleRight);

		XSSFCell celula6 = linhaTotal.createCell(5);
		celula6.setCellValue(this.valorTotalContaBancariaAcum());
		celula6.setCellStyle(headerStyleRight);

		XSSFCell celula7 = linhaTotal.createCell(6);
		celula7.setCellValue(this.valorTotalTotalAcum());
		celula7.setCellStyle(headerStyleRight);
	}

	private void criarRodape(XSSFWorkbook wb, XSSFSheet sheet) {
		Font font;
		/* Criação, inclusão e formatação dos dados do footer do excel */
		XSSFRow ultimaLinha = sheet.createRow(sheet.getLastRowNum() + 1);

		XSSFCellStyle footerStyle = wb.createCellStyle();
		footerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		footerStyle.setWrapText(true);
		font = footerStyle.getFont();
		font.setFontHeight((short) 150);
		font.setFontName("Arial");
		font.setColor(IndexedColors.BLACK.getIndex());
		font.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		footerStyle.setFont(font);

		XSSFCell cell1 = ultimaLinha.createCell(0);
		cell1.setCellValue(now());
		cell1.setCellStyle(footerStyle);

		XSSFCell cell2 = ultimaLinha.createCell(3);
		cell2.setCellValue("Superintêndencia de Ocorrências Especiais");
		cell2.setCellStyle(footerStyle);

		CellRangeAddress region1 = new CellRangeAddress(ultimaLinha.getRowNum(), ultimaLinha.getRowNum(), 0, 2);
		CellRangeAddress region2 = new CellRangeAddress(ultimaLinha.getRowNum(), ultimaLinha.getRowNum(), 3, 6);
		sheet.addMergedRegion(region1);
		sheet.addMergedRegion(region2);
	}

	// Recuperação Mes
	private String valorTotalCartaoDebitoMes() {
		this.valorTotal = 0.00;
		objRsRelatMes.forEach((item) -> {
			this.valorTotal += Double.parseDouble(item.getCARTAO_DEBITO());
		});
		return formataNumero(Double.toString(this.valorTotal));
	}

	private String valorTotalCartaoCreditoMes() {
		this.valorTotal = 0.00;
		objRsRelatMes.forEach((item) -> {
			this.valorTotal += Double.parseDouble(item.getCARTAO_CREDITO());
		});
		return formataNumero(Double.toString(this.valorTotal));
	}

	private String valorTotalInternetMes() {
		this.valorTotal = 0.00;
		objRsRelatMes.forEach((item) -> {
			this.valorTotal += Double.parseDouble(item.getINTERNET_BANKING());
		});
		return formataNumero(Double.toString(this.valorTotal));
	}

	private String valorTotalSuperlinhaMes() {
		this.valorTotal = 0.00;
		objRsRelatMes.forEach((item) -> {
			this.valorTotal += Double.parseDouble(item.getSUPERLINHA());
		});
		return formataNumero(Double.toString(this.valorTotal));
	}

	private String valorTotalContaBancariaMes() {
		this.valorTotal = 0.00;
		objRsRelatMes.forEach((item) -> {
			this.valorTotal += Double.parseDouble(item.getCONTA_BANCARIA());
		});
		return formataNumero(Double.toString(this.valorTotal));
	}

	private String valorTotalTotalMes() {
		this.valorTotal = 0.00;
		objRsRelatMes.forEach((item) -> {
			this.valorTotal += Double.parseDouble(item.getTOTAL_MES());
		});
		return formataNumero(Double.toString(this.valorTotal));
	}

	// Recuperação de Outros Periodos
	private String valorTotalCartaoDebito() {
		this.valorTotal = 0.00;
		objRsRelat.forEach((item) -> {
			this.valorTotal += Double.parseDouble(item.getCARTAO_DEBITO());
		});
		return formataNumero(Double.toString(this.valorTotal));
	}

	private String valorTotalCartaoCredito() {
		this.valorTotal = 0.00;
		objRsRelat.forEach((item) -> {
			this.valorTotal += Double.parseDouble(item.getCARTAO_CREDITO());
		});
		return formataNumero(Double.toString(this.valorTotal));
	}

	private String valorTotalInternet() {
		this.valorTotal = 0.00;
		objRsRelat.forEach((item) -> {
			this.valorTotal += Double.parseDouble(item.getINTERNET_BANKING());
		});
		return formataNumero(Double.toString(this.valorTotal));
	}

	private String valorTotalSuperlinha() {
		this.valorTotal = 0.00;
		objRsRelat.forEach((item) -> {
			this.valorTotal += Double.parseDouble(item.getSUPERLINHA());
		});
		return formataNumero(Double.toString(this.valorTotal));
	}

	private String valorTotalContaBancaria() {
		this.valorTotal = 0.00;
		objRsRelat.forEach((item) -> {
			this.valorTotal += Double.parseDouble(item.getCONTA_BANCARIA());
		});
		return formataNumero(Double.toString(this.valorTotal));
	}

	private String valorTotalTotal() {
		this.valorTotal = 0.00;
		objRsRelat.forEach((item) -> {
			this.valorTotal += Double.parseDouble(item.getTOTAL_MES());
		});
		return formataNumero(Double.toString(this.valorTotal));
	}

	// Recuperação Acumulada
	private String valorTotalCartaoDebitoAcum() {
		this.valorTotal = 0.00;
		objRsRelatAcum.forEach((item) -> {
			this.valorTotal += Double.parseDouble(item.getCARTAO_DEBITO());
		});
		return formataNumero(Double.toString(this.valorTotal));
	}

	private String valorTotalCartaoCreditoAcum() {
		this.valorTotal = 0.00;
		objRsRelatAcum.forEach((item) -> {
			this.valorTotal += Double.parseDouble(item.getCARTAO_CREDITO());
		});
		return formataNumero(Double.toString(this.valorTotal));
	}

	private String valorTotalInternetAcum() {
		this.valorTotal = 0.00;
		objRsRelatAcum.forEach((item) -> {
			this.valorTotal += Double.parseDouble(item.getINTERNET_BANKING());
		});
		return formataNumero(Double.toString(this.valorTotal));
	}

	private String valorTotalSuperlinhaAcum() {
		this.valorTotal = 0.00;
		objRsRelatAcum.forEach((item) -> {
			this.valorTotal += Double.parseDouble(item.getSUPERLINHA());
		});
		return formataNumero(Double.toString(this.valorTotal));
	}

	private String valorTotalContaBancariaAcum() {
		this.valorTotal = 0.00;
		objRsRelatAcum.forEach((item) -> {
			this.valorTotal += Double.parseDouble(item.getCONTA_BANCARIA());
		});
		return formataNumero(Double.toString(this.valorTotal));
	}

	private String valorTotalTotalAcum() {
		this.valorTotal = 0.00;
		objRsRelatAcum.forEach((item) -> {
			this.valorTotal += Double.parseDouble(item.getTOTAL_MES());
		});
		return formataNumero(Double.toString(this.valorTotal));
	}
}
