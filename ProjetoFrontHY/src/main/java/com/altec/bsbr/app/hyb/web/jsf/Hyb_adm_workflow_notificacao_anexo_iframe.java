package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

@ManagedBean(name="hyb_adm_workflow_notificacao_anexo_iframe")
@ViewScoped
public class Hyb_adm_workflow_notificacao_anexo_iframe {
	
	private String txtHdAction;
	private String txtHdInfArq;
	private String txtHdNmArquivo;
	private String txtNrSeqNoti;
	private String strMsg;
	private String strErro;
	
	public Hyb_adm_workflow_notificacao_anexo_iframe() {
		this.setStrErro("");
	}
	
	public Hyb_adm_workflow_notificacao_anexo_iframe(String txtHdAction, String txtHdInfArq, String txtHdNmArquivo,
			String txtNrSeqNoti) {
		this.txtHdAction = txtHdAction;
		this.txtHdInfArq = txtHdInfArq;
		this.txtHdNmArquivo = txtHdNmArquivo;
		this.txtNrSeqNoti = txtNrSeqNoti;
	}
	
	public String getTxtHdAction() {
		return txtHdAction;
	}
	
	public void setTxtHdAction(String txtHdAction) {
		this.txtHdAction = txtHdAction;
	}
	
	public String getTxtHdInfArq() {
		return txtHdInfArq;
	}
	
	public void setTxtHdInfArq(String txtHdInfArq) {
		this.txtHdInfArq = txtHdInfArq;
	}
	
	public String getTxtHdNmArquivo() {
		return txtHdNmArquivo;
	}
	
	public void setTxtHdNmArquivo(String txtHdNmArquivo) {
		this.txtHdNmArquivo = txtHdNmArquivo;
	}
	
	public String getTxtNrSeqNoti() {
		return txtNrSeqNoti;
	}
	
	public void setTxtNrSeqNoti(String txtNrSeqNoti) {
		this.txtNrSeqNoti = txtNrSeqNoti;
	}
	
	public void setTxtHdActionD() {
		this.setTxtHdAction("D");
	}
	
	public void setTxtHdInfArqStr() {
		String txtHdInfArqStr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("txtHdInfArqStr").toString();
		this.setTxtHdInfArq(txtHdInfArqStr);
	}
	
	public void setTxtHdNmArquivoStr() {
		String txtHdNmArquivoStr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("txtHdNmArquivoStr").toString();
		this.setTxtHdNmArquivo(txtHdNmArquivoStr);
	}
	
	public void settxtNrSeqNotiStr() {
		String txtNrSeqNotiStr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("txtNrSeqNotiStr").toString();
		this.setTxtNrSeqNoti(txtNrSeqNotiStr);
	}
	
	public String getStrMsg() {
		return strMsg;
	}
	
	public void setStrMsg(String strMsg) {
		this.strMsg = strMsg;
	}
	
	public String getStrErro() {
		return strErro;
	}

	public void setStrErro(String strErro) {
		this.strErro = strErro;
	}

	public void formSubmit() {
		if (this.getTxtHdAction() == "D") {
			// TODO - Chamada da fun��o objAdmNoti.ApagarArqAnex(strNrSeqOs, strNmArqAnex, strErro)
			VerificaErro();
			this.setStrMsg("Exclus�o efetuada com sucesso!");
			RequestContext.getCurrentInstance().execute("alert(" + this.getStrMsg() + ");");
		}
	}
	
	public void VerificaErro() {
		if (strErro != "") {
			try {
				ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
				context.redirect(context.getRequestContextPath() + "hy_erro.asp?strErro=" + this.getStrErro());				
			} catch(IOException e) {
				System.out.println(e);
			}
		}
	}
	

}
