package com.altec.bsbr.app.hyb.dto;

public class ObjCatgNv3 {

	private String value;
	private String CdN3;
	private String NuCntC;	
	
	public ObjCatgNv3() {
		
	}
	
	public ObjCatgNv3(String value, String cdN3, String nuCntC) {
		super();
		this.value = value;
		CdN3 = cdN3;
		NuCntC = nuCntC;
	}
	
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getCdN3() {
		return CdN3;
	}
	public void setCdN3(String cdN3) {
		CdN3 = cdN3;
	}
	public String getNuCntC() {
		return NuCntC;
	}
	public void setNuCntC(String nuCntC) {
		NuCntC = nuCntC;
	}
	
	
	
       
}