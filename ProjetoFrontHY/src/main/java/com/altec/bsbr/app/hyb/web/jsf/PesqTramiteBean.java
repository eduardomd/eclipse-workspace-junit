package com.altec.bsbr.app.hyb.web.jsf;


import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.PesqTramiteModel;
import com.altec.bsbr.app.jab.hyb.webclient.XHYTramite.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYTramite.XHYTramiteEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import java.text.DateFormat;

@Component("hyb_pesq_tramite_bean")
@Scope("session")

public class PesqTramiteBean extends BasicBBean {
	
	private static final long serialVersionUID = 1L;

	private Date txtDataini = null;
	private Date txtDatafim = null;
	private String cboDiv = "0";
	private Date today;
	private PesqTramiteModel selectedOs = new PesqTramiteModel();
	private Map<String, String> cboDivs = new HashMap<String, String>();
	private List<PesqTramiteModel> vrstOS = new ArrayList<PesqTramiteModel>();
	
	@Autowired
	private XHYTramiteEndPoint admTram;
	
	@PostConstruct
	public void init() {
		today = Calendar.getInstance().getTime();	
		this.txtDatafim = null;
		this.txtDataini = null;
		System.out.println("kodo   S");
		RequestContext.getCurrentInstance().execute("document.getElementById('frm:txtPerInicio_input').value =''; document.getElementById('frm:txtPerFim_input').value =''; document.getElementById('frm:cboDiv_label').innerHTML='Selecione...';");
	}
	
	public void Pesquisar()
	{
		this.vrstOS.clear();
		//Map<String, String> requestParamMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		String retorno = "";
		/*String dtIn = convertData(requestParamMap.get("dataIn"));
		String dtFm = convertData(requestParamMap.get("dataFm"));		
		String cboDiv = requestParamMap.get("divCod");
			
		System.out.println("Data In - " + dtIn);
		System.out.println("Data Fm - " + dtFm);
		System.out.println("Codigo - " + cboDiv);
	*/
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		System.out.println(convertData(txtDatafim));
		//retorno =  admTram.consultarOSPeriodo(dtIn, dtFm , cboDiv);
		try {
			retorno =  admTram.consultarOSPeriodo(convertData(txtDataini), convertData(txtDatafim) , cboDiv);
		
			System.out.println(retorno);
			
			JSONObject pesqTramites = new JSONObject(retorno);
			JSONArray pcursor = pesqTramites.getJSONArray("PCURSOR");
		    
			vrstOS = new ArrayList<PesqTramiteModel>();
			for(int i = 0; i < pcursor.length(); i++)
			{
				JSONObject item = pcursor.getJSONObject(i);
				/*
				String strOs = (item.isNull("OS") == true ? "" : item.get("OS").toString());
				int strNrTra = (item.isNull("NR_TRA") == true ? 0 : (int) item.get("NR_TRA")); 
				String strDtRg = (item.isNull("DT_REG") == true ? "" : item.get("DT_REG").toString()); 
				String strDtRetnPlan = (item.isNull("DT_RETN_PLAN") == true ? "" : item.get("DT_RETN_PLAN").toString());
				String strDtRetn =  (item.isNull("DT_RETN") == true ? "" : item.get("DT_RETN").toString());
				int strFlag = (item.isNull("FLAG") == true ? 0 : (int) item.get("FLAG"));
				 
				*/
				PesqTramiteModel tramGeral = new PesqTramiteModel(); 
				tramGeral.setOS( item.isNull("OS") == true ? "" : item.get("OS").toString());
				int nr_RA = item.isNull("NR_TRA") == true ? 0 : (int) item.get("NR_TRA");
				tramGeral.setNR_TRA(Integer.toString(nr_RA));
				tramGeral.setDT_REG(item.isNull("DT_REG") == true ? "" : item.get("DT_REG").toString());
				tramGeral.setDT_RETN_PLAN(item.isNull("DT_RETN_PLAN") == true ? "" : item.get("DT_RETN_PLAN").toString());
				tramGeral.setDT_RETN(item.isNull("DT_RETN") == true ? "" : item.get("DT_RETN").toString());
				int nFlag = item.isNull("FLAG") == true ? 0 : (int) item.get("FLAG");
				tramGeral.setFlag(Integer.toString(nFlag));
				
				//vrstOS.add(new PesqTramiteModel(strOs, Integer.toString(strNrTra), strDtRetn, strDtRg, Integer.toString(strFlag), strDtRetnPlan ));
				vrstOS.add(tramGeral);
			
			}

		RequestContext.getCurrentInstance().execute("document.getElementById('frm:txtPerInicio_input').value =''; document.getElementById('frm:txtPerFim_input').value =''; document.getElementById('frm:cboDiv_label').innerHTML='Selecione...';");
			
		} 
		catch (WebServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//System.out.println(vrstOS);
	}
	
	
	public void EditarTramite() throws IOException {
		
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		
		System.out.println(selectedOs);
		System.out.println(txtDataini);
		System.out.println(txtDatafim);
		System.out.println(vrstOS.size());
		//System.out.println(selectedOs.getDT_RETN().isEmpty());
		//System.out.println(selectedOs.getDT_RETN());
		
		
		
		if((selectedOs == null) && (txtDataini ==null) && (txtDatafim == null) && (vrstOS.size() == 0))
		{
			System.out.println("Preencha os campos.");
			RequestContext.getCurrentInstance().execute("document.getElementById('frm:txtPerInicio_input').value =''; document.getElementById('frm:txtPerFim_input').value =''; document.getElementById('frm:cboDiv_label').innerHTML='Selecione...';");
			
		} else if((selectedOs == null) && (vrstOS.size() != 0))
		{
			RequestContext.getCurrentInstance().execute("alert('Favor selecionar um item.')");
			RequestContext.getCurrentInstance().execute("document.getElementById('frm:txtPerInicio_input').value =''; document.getElementById('frm:txtPerFim_input').value =''; document.getElementById('frm:cboDiv_label').innerHTML='Selecione...';");
			
		} else if(!selectedOs.getDT_RETN().isEmpty())
		{
			RequestContext.getCurrentInstance().execute("alert('OS não pode ser editada, porque está com Data de Ret. Real!')");
			RequestContext.getCurrentInstance().execute("document.getElementById('frm:txtPerInicio_input').value =''; document.getElementById('frm:txtPerFim_input').value =''; document.getElementById('frm:cboDiv_label').innerHTML='Selecione...';");
	
		}
		else 
		
		{
			System.out.println("Chegou aqui " +selectedOs.getOS());
			ec.redirect("hyb_cad_tramite_menu.xhtml?strNrOs="+selectedOs.getOS()+"&NrTramite="+selectedOs.getNR_TRA()+"&strReadOnly=true&strTitulo=");
		}
		
		//PesqTramiteModel test = ((PesqTramiteModel) event.getObject());
	
	}
	
	public void ArquivarOs() throws IOException {
		
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		
		if((selectedOs == null) && (txtDataini ==null) && (txtDatafim == null) && (vrstOS.size() == 0))
		{
			System.out.println("Preencha os campos.");
			RequestContext.getCurrentInstance().execute("document.getElementById('frm:txtPerInicio_input').value =''; document.getElementById('frm:txtPerFim_input').value =''; document.getElementById('frm:cboDiv_label').innerHTML='Selecione...';");
		} else if((selectedOs == null) && (vrstOS.size() != 0))
		{
			RequestContext.getCurrentInstance().execute("alert('Favor selecionar um item.')");
			RequestContext.getCurrentInstance().execute("document.getElementById('frm:txtPerInicio_input').value =''; document.getElementById('frm:txtPerFim_input').value =''; document.getElementById('frm:cboDiv_label').innerHTML='Selecione...';");
			
		} else if(selectedOs.getDT_RETN().isEmpty()) {
			System.out.println(selectedOs.getDT_RETN());
			RequestContext.getCurrentInstance().execute("alert('Existe trâmite sem Data Retorno!')");
			RequestContext.getCurrentInstance().execute("document.getElementById('frm:txtPerInicio_input').value =''; document.getElementById('frm:txtPerFim_input').value =''; document.getElementById('frm:cboDiv_label').innerHTML='Selecione...';");
			
		}
		else
		{
			System.out.println("Chegou aqui " +selectedOs.getOS());
			ec.redirect("hyb_arquivamentoos.xhtml?strNrSeqOs="+selectedOs.getOS());
		}
		
	}
	
	public void Imprimir() throws IOException {
		
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		
		if((selectedOs == null) && (txtDataini ==null) && (txtDatafim == null) && (vrstOS.size() == 0))
		{
			System.out.println("Preencha os campos.");
		} else if(selectedOs == null)
		{	
			System.out.println(selectedOs);
			RequestContext.getCurrentInstance().execute("alert('Favor selecionar um item.')");
			
		} 
		else
		{
			System.out.println("Chegou aqui " +selectedOs.getOS());
			RequestContext.getCurrentInstance().execute("window.open('hyb_imprimiros_tramite.xhtml?strNrSeqOs="+selectedOs.getOS()+"','', 'height:500px; width:700px')");
			//ec.redirect("hyb_imprimiros_tramite.xhtml?strNrSeqOs="+selectedOs.getOS());
		}
		
	}
	
	public Map getCboDivs() {
		cboDivs.put("GOI", "1390404");
		cboDivs.put("GOE", "1520232");
		cboDivs.put("Prevenção", "1390405");
		
		return cboDivs;
	}
	


	public Date getToday() {
		return today;
	}

	public void setToday(Date today) {
		this.today = today;
	}
	
	public String convertData(Date dataInput) {
		String parsedDate;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		parsedDate = formatter.format(dataInput);
	
		return parsedDate;
	}
	
	public void cleanSession() {
        System.out.println("\n --CLEAN SESSION --");
        FacesContext context = FacesContext.getCurrentInstance(); 
        context.getExternalContext().getSessionMap().remove("PesqTramiteBean");

        try {
               reload();
        } catch (IOException e) {
               e.printStackTrace();
        }
        
  }
	
	public void reload() throws IOException {
	    ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
	    ec.redirect("hyb_pesq_tramite.xhtml");
	}
	
	public XHYTramiteEndPoint getAdmTram() {
		return admTram;
	}

	public void setAdmTram(XHYTramiteEndPoint admTram) {
		this.admTram = admTram;
	}

	public Date getTxtDataini() {
		return txtDataini;
	}

	public void setTxtDataini(Date txtDataini) {
		this.txtDataini = txtDataini;
	}

	public Date getTxtDatafim() {
		return txtDatafim;
	}

	public void setTxtDatafim(Date txtDatafim) {
		this.txtDatafim = txtDatafim;
	}

	public String getCboDiv() {
		return cboDiv;
	}

	public void setCboDiv(String cboDiv) {
		this.cboDiv = cboDiv;
	}

	public PesqTramiteModel getSelectedOs() {
		return selectedOs;
	}

	public void setSelectedOs(PesqTramiteModel selectedOs) {
		this.selectedOs = selectedOs;
	}

	public List<PesqTramiteModel> getVrstOS() {
		return vrstOS;
	}

	public void setVrstOS(List<PesqTramiteModel> vrstOs) {
		this.vrstOS = vrstOs;
	}

	public void setCboDivs(Map<String, String> cboDivs) {
		this.cboDivs = cboDivs;
	}
	
	
}
