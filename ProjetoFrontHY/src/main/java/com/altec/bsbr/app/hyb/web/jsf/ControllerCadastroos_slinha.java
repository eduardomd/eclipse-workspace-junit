package com.altec.bsbr.app.hyb.web.jsf;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean(name = "slinhaOS")
@ViewScoped
public class ControllerCadastroos_slinha implements Serializable{
	//private static final longstrNrCanal, strNrEvento, strNrSeqDetalhe
	//private static final long strErro
	private  final String strNrTitular = "984284";
	private  final String longstrConta = "9865";
	private  final String longstrNrOS = "512";
	private  final String  strCPF_CNPJ = "123.456.789-01";
	private  final String longstrTitular = "Jo�o Marcelo";
	
	public ControllerCadastroos_slinha() {
		
	}
	
	
	public String getLongstrTitular() {
		return longstrTitular;
	}
	public String getStrNrTitular() {
		return strNrTitular;
	}
	public String getLongstrConta() {
		return longstrConta;
	}
	public String getLongstrNrOS() {
		return longstrNrOS;
	}
	public String getStrCPF_CNPJ() {
		return strCPF_CNPJ;
	}
	
}
