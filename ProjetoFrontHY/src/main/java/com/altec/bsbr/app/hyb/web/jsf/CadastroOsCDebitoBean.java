package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.CadastroOsCDebitoModel;
import com.altec.bsbr.app.hyb.dto.CadastroOsCDebitoPontoVendaModel;
import com.altec.bsbr.app.hyb.dto.CadastroOsSLinhaObjCanais;
import com.altec.bsbr.app.hyb.dto.DropDownPesqOSModel;
import com.altec.bsbr.app.hyb.dto.OperAuxModel;
import com.altec.bsbr.app.hyb.dto.OperCorpModel;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsCanais.XHYCadOsCanaisEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsEventos.XHYCadOsEventosEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsUnidEnvol.XHYCadOsUnidEnvolEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Component("CadastroOsCDebitoBean")
@Scope("session")
public class CadastroOsCDebitoBean extends BasicBBean {

	private static final long serialVersionUID = 1L;

	@Autowired
	private XHYCadOsEventosEndPoint cadOsEventos;
	
	@Autowired
	private XHYCadOsUnidEnvolEndPoint cadOsUnidEnvol;
	
	private CadastroOsCDebitoModel cadastroCDebito;
		
	private Boolean showPontoVenda;
	private Boolean showCartaoSeguranca;
	
	private String qsCanal;
	private String strNrSeqId;
	private String qsEvento;
	
	private List<DropDownPesqOSModel> objRsCbo;
	private List<DropDownPesqOSModel> objRsLocal;
	private List<CadastroOsCDebitoPontoVendaModel> lstPontoVenda;
	
	private String cboClasFraudeSelecionado;
	private String cboLocalSelecionado;
	private String cboSensorSelecionado;
	
	private String cboCSeg;
	
	// INC	
	@Autowired
	private XHYCadOsCanaisEndPoint cadOsCanais;
	
	private List<CadastroOsSLinhaObjCanais> objCanais;	
	private String selectRdoContestacao;

	private List<OperCorpModel> objRsOperCorp;
	private List<OperAuxModel> objRsOperAux;
	private String selectedOperCorp;
	private String selectedOperAux;
	private String strDtPeriFixa;
	private String strDtPeriIni;
	private String strDtPeriFim;
	private int intQntLancDbto = 0;
	private int ibtQntLancCrdt = 0;
	private int intQntLancCrdt = 0;
	private double intValAux = 0;
	private boolean destExists;
	private boolean isCredito;	
	private double intQntValDbto;
	private double intQntValCrdt;
	private Locale ptBR = new Locale("pt", "BR");
		
	@PostConstruct
	public void init() {
		if (FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("qsCanal") != null) 
			this.qsCanal = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("qsCanal").toString();
		if (FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("NrSeqDetalhe") != null) 
			this.strNrSeqId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("NrSeqDetalhe").toString();
		if (FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("qsEvento") != null) 
			this.qsEvento = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("qsEvento").toString();				
		
		System.out.println("init CadastroOsCDebitoBean:!!!!!!!!!!!!!!!!!!!!!!!!!! - "+qsCanal+" - "+strNrSeqId+" - "+qsEvento);
		
		this.lstPontoVenda = new ArrayList<CadastroOsCDebitoPontoVendaModel>();
		if (!this.strNrSeqId.isEmpty())
			consultarFraudeCanalDetalhe();
		carregaLocaFragil();	
		carregaClafFrau();
		
		//inc
		this.isCredito = false;
		this.selectRdoContestacao = "1";
		carregaComboOper(1);
		carregaComboOperAux(1);		
		consultarOperacaoCanal();
	
	}
	
	public void consultarFraudeCanalDetalhe() {
		try {
			JSONObject json = new JSONObject(
					cadOsEventos.consultarFraudeCanalDetalhe(Long.valueOf(this.strNrSeqId)));
			JSONArray pcursor = json.getJSONArray("PCURSOR");

			for(int i = 0; i < pcursor.length(); i++) {
				this.cadastroCDebito = new CadastroOsCDebitoModel(
						pcursor.getJSONObject(i).isNull("NR_SEQU_ORDE_SERV") ? "" : pcursor.getJSONObject(i).getString("NR_SEQU_ORDE_SERV"),	 
						pcursor.getJSONObject(i).isNull("NR_SEQU_FRAU_EVEN") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("NR_SEQU_FRAU_EVEN")),	 
						pcursor.getJSONObject(i).isNull("NM_PARP") ? "" : pcursor.getJSONObject(i).getString("NM_PARP"),	 
						pcursor.getJSONObject(i).isNull("NR_SEQU_PARP_PROC") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("NR_SEQU_PARP_PROC")),	 
						pcursor.getJSONObject(i).isNull("NR_SEQU_CNTA_BNCR") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("NR_SEQU_CNTA_BNCR")),	 
						pcursor.getJSONObject(i).isNull("NR_SEQU_CRTO") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("NR_SEQU_CRTO")),	 
						pcursor.getJSONObject(i).isNull("CD_BANC") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("CD_BANC")),	 
						pcursor.getJSONObject(i).isNull("NR_CNTA") ? "" : String.valueOf(pcursor.getJSONObject(i).getLong("NR_CNTA")),	 
						pcursor.getJSONObject(i).isNull("NR_CRTO") ? "" : String.valueOf(pcursor.getJSONObject(i).getLong("NR_CRTO")),	 
						pcursor.getJSONObject(i).isNull("TP_CNTA_BNCR") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("TP_CNTA_BNCR")),	 
						pcursor.getJSONObject(i).isNull("CD_CNAL") ? "" : pcursor.getJSONObject(i).getString("CD_CNAL"),	 
						pcursor.getJSONObject(i).isNull("NR_CPF_CNPJ_TITL") ? "" : pcursor.getJSONObject(i).getString("NR_CPF_CNPJ_TITL"),	 
						pcursor.getJSONObject(i).isNull("DT_EMIS") ? "" : pcursor.getJSONObject(i).getString("DT_EMIS"),	 
						pcursor.getJSONObject(i).isNull("DT_ATIV") ? "" : pcursor.getJSONObject(i).getString("DT_ATIV"),	 
						pcursor.getJSONObject(i).isNull("DT_CANC") ? "" : pcursor.getJSONObject(i).getString("DT_CANC"),	 
						pcursor.getJSONObject(i).isNull("NM_LOG_RECU_SOLI") ? "" : pcursor.getJSONObject(i).getString("NM_LOG_RECU_SOLI"),	 
						pcursor.getJSONObject(i).isNull("DT_ABER_CNTA_BNCR") ? "" : pcursor.getJSONObject(i).getString("DT_ABER_CNTA_BNCR"),	 
						pcursor.getJSONObject(i).isNull("NM_GERE_RESP_CNTA_BNCR") ? "" : pcursor.getJSONObject(i).getString("NM_GERE_RESP_CNTA_BNCR"),	 
						pcursor.getJSONObject(i).isNull("NM_SITU_CNTA_BNCR") ? "" : pcursor.getJSONObject(i).getString("NM_SITU_CNTA_BNCR"),	 
						pcursor.getJSONObject(i).isNull("NM_ENDE_ENVI") ? "" : pcursor.getJSONObject(i).getString("NM_ENDE_ENVI"),	 
						pcursor.getJSONObject(i).isNull("IN_ITSS_COME") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("IN_ITSS_COME")),	 
						pcursor.getJSONObject(i).isNull("IN_CLIE_AJDA_TERC") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("IN_CLIE_AJDA_TERC")),	 
						pcursor.getJSONObject(i).isNull("CD_AGEN_AJDA_TERC") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("CD_AGEN_AJDA_TERC")),	 
						pcursor.getJSONObject(i).isNull("IN_CRTO_RECB") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("IN_CRTO_RECB")),	 
						pcursor.getJSONObject(i).isNull("IN_CRTO_RETD") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("IN_CRTO_RETD")),	 
						pcursor.getJSONObject(i).isNull("IN_CRTO_SEGR") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("IN_CRTO_SEGR")),	 
						pcursor.getJSONObject(i).isNull("DT_SOLI_CRTO_SEGR") ? "" : pcursor.getJSONObject(i).getString("DT_SOLI_CRTO_SEGR"),	 
						pcursor.getJSONObject(i).isNull("DT_CANC_CRTO_SEGR") ? "" : pcursor.getJSONObject(i).getString("DT_CANC_CRTO_SEGR"),	 
						pcursor.getJSONObject(i).isNull("DT_ATIV_CRTO_SEGR") ? "" : pcursor.getJSONObject(i).getString("DT_ATIV_CRTO_SEGR"),	 
						pcursor.getJSONObject(i).isNull("DT_PRMR_FRAU") ? "" : String.valueOf(pcursor.getJSONObject(i).getLong("DT_PRMR_FRAU")),	 
						pcursor.getJSONObject(i).isNull("NM_LOCA_ATIV_CRTO_SEGR") ? "" : pcursor.getJSONObject(i).getString("NM_LOCA_ATIV_CRTO_SEGR"),
						pcursor.getJSONObject(i).isNull("NR_SEQU_LOCA_FRAG_CRTO") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("NR_SEQU_LOCA_FRAG_CRTO")),
						pcursor.getJSONObject(i).isNull("NM_AGEN_FRAG_CRTO") ? "" : pcursor.getJSONObject(i).getString("NM_AGEN_FRAG_CRTO"), 
						pcursor.getJSONObject(i).isNull("DT_OCOR_FRAG_CRTO") ? "" : String.valueOf(pcursor.getJSONObject(i).getLong("DT_OCOR_FRAG_CRTO")), 
						pcursor.getJSONObject(i).isNull("NR_TERN_FRAG_CRTO") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("NR_TERN_FRAG_CRTO")), 
						pcursor.getJSONObject(i).isNull("IN_EXIS_SENS") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("IN_EXIS_SENS")), 
						pcursor.getJSONObject(i).isNull("DS_OUTR_FRAG_CRTO") ? "" : pcursor.getJSONObject(i).getString("DS_OUTR_FRAG_CRTO"), 
						pcursor.getJSONObject(i).isNull("NR_CLAF_FRAU") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("NR_CLAF_FRAU")), 
						pcursor.getJSONObject(i).isNull("NR_QNTD_EVEN") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("NR_QNTD_EVEN")), 
						pcursor.getJSONObject(i).isNull("CD_CENT_CUST_ORIG") ? "" : pcursor.getJSONObject(i).getString("CD_CENT_CUST_ORIG"), 
						pcursor.getJSONObject(i).isNull("CD_CENT_CUST_DEST") ? "" : pcursor.getJSONObject(i).getString("CD_CENT_CUST_DEST"), 
						pcursor.getJSONObject(i).isNull("CD_CENT_CUST_OPNT") ? "" : pcursor.getJSONObject(i).getString("CD_CENT_CUST_OPNT"), 
						pcursor.getJSONObject(i).isNull("CONTABILIZADO") ? "" : pcursor.getJSONObject(i).getString("CONTABILIZADO")
					);
				
				if (!pcursor.getJSONObject(i).isNull("HR_INIC_FRAG_CRTO"))
					this.cadastroCDebito.setStrFragHrInicio(new SimpleDateFormat("HH:mm").format(new Date(pcursor.getJSONObject(i).getLong("HR_INIC_FRAG_CRTO"))));
				
				if (!pcursor.getJSONObject(i).isNull("HR_FINA_FRAG_CRTO"))
					this.cadastroCDebito.setStrFragHrFinal(new SimpleDateFormat("HH:mm").format(new Date(pcursor.getJSONObject(i).getLong("HR_FINA_FRAG_CRTO"))));
				
				if (!pcursor.getJSONObject(i).isNull("CD_AGEN_FRAG_CRTO")) {
					String cda = "0000".concat(String.valueOf(pcursor.getJSONObject(i).getInt("CD_AGEN_FRAG_CRTO")));
					this.cadastroCDebito.setStrFragCodAgen(cda.substring(cda.length() - 4));
				}
				
				this.showCartaoSeguranca = "1".equals(this.cadastroCDebito.getBlnCrtoSegr()); 
				this.cboCSeg = this.cadastroCDebito.getBlnCrtoSegr();
				
				this.cboSensorSelecionado = this.cadastroCDebito.getBlnFragSensor();
				
				if (!this.cadastroCDebito.getStrDtFrauCrto().isEmpty())
					this.cadastroCDebito.setStrDtFrauCrto(new SimpleDateFormat("dd/MM/yyyy").format(new Date(Long.valueOf(this.cadastroCDebito.getStrDtFrauCrto()))));
				if (!this.cadastroCDebito.getStrFragDtOcorr().isEmpty())
					this.cadastroCDebito.setStrFragDtOcorr(new SimpleDateFormat("dd/MM/yyyy").format(new Date(Long.valueOf(this.cadastroCDebito.getStrFragDtOcorr()))));				
				
			}
					
				
		} catch (Exception e) {
			e.printStackTrace();
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	public void carregaLocaFragil() {
		try {
			this.objRsLocal = new ArrayList<DropDownPesqOSModel>();
			
			JSONObject json = new JSONObject(
					cadOsEventos.consultarLocalFragilizacao());
			JSONArray pcursor = json.getJSONArray("PCURSOR");

			for(int i = 0; i < pcursor.length(); i++) {
				this.objRsLocal.add(
						new DropDownPesqOSModel(
								pcursor.getJSONObject(i).isNull("CODIGO") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("CODIGO")),
								pcursor.getJSONObject(i).isNull("NOME") ? "" : pcursor.getJSONObject(i).getString("NOME")));
				
				if ((Integer.valueOf("0" + this.cadastroCDebito.getIntFragLocal())) == Integer.valueOf("0" + this.objRsLocal.get(i).getCODIGO())) 
					cboLocalSelecionado = this.objRsLocal.get(i).getCODIGO();
				
				System.out.println("cboLocalSelecionado!!@@ "+cboLocalSelecionado+" - getIntFragLocal() "+this.cadastroCDebito.getIntFragLocal()+" - this.objRsLocal.get(i).getCODIGO() "+this.objRsLocal.get(i).getCODIGO());
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	public void carregaClafFrau() {
		try {
			this.objRsCbo = new ArrayList<DropDownPesqOSModel>();
			this.cboClasFraudeSelecionado = "";
			
			JSONObject json = new JSONObject(
					cadOsEventos.consultarClassificacaoFraude());
			JSONArray pcursor = json.getJSONArray("PCURSOR");

			for(int i = 0; i < pcursor.length(); i++) { 
				this.objRsCbo.add(
						new DropDownPesqOSModel(pcursor.getJSONObject(i).isNull("NR_CLAF_FRAU") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("NR_CLAF_FRAU")),
												pcursor.getJSONObject(i).isNull("DS_CLAF_FRAU") ? "" : pcursor.getJSONObject(i).getString("DS_CLAF_FRAU")));
				
				if ((Integer.valueOf("0" + this.cadastroCDebito.getIntFragClafFrau())) == pcursor.getJSONObject(i).getInt("NR_CLAF_FRAU")) 
					cboClasFraudeSelecionado = String.valueOf(pcursor.getJSONObject(i).getInt("NR_CLAF_FRAU")); 
			}
			
			this.showPontoVenda = "1".equals(this.cboClasFraudeSelecionado);
			if (this.showPontoVenda && 
				!this.cadastroCDebito.getStrAgenClieAjud().isEmpty() && 
				!this.cadastroCDebito.getStrAgenClieAjud().equals("0000"))
				this.pesquisarPV();
				
			
		} catch (Exception e) {
			e.printStackTrace();
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	public void pesquisarPV() {
		System.out.println("pesquisar pv!! "+this.cadastroCDebito.getStrAgenClieAjud());

		try {
			this.lstPontoVenda.clear();
			JSONObject json = new JSONObject(
					cadOsUnidEnvol.fnSelPVNr("", this.cadastroCDebito.getStrAgenClieAjud(), ""));
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			
			if (pcursor.length() <= 0) {
				RequestContext.getCurrentInstance().execute("alert('Ponto de venda - "+this.cadastroCDebito.getStrAgenClieAjud()+" não encontrado!');");
			}
			else {
//				for(int i = 0; i < pcursor.length(); i++) -> caso queira usar list, mude 0 pra i
				this.lstPontoVenda.add(
						new CadastroOsCDebitoPontoVendaModel(pcursor.getJSONObject(0).isNull("NM_REDE") ? "-" : pcursor.getJSONObject(0).getString("NM_REDE"), 
															 pcursor.getJSONObject(0).isNull("NM_REGI") ? "-" : pcursor.getJSONObject(0).getString("NM_REGI"), 
															 pcursor.getJSONObject(0).isNull("CD_UOR") ? "" : String.valueOf(pcursor.getJSONObject(0).getInt("CD_UOR")), 
															 pcursor.getJSONObject(0).isNull("NM_UOR") ? "-" : pcursor.getJSONObject(0).getString("NM_UOR"), 
														     pcursor.getJSONObject(0).isNull("TP_UOR") ? "" : String.valueOf(pcursor.getJSONObject(0).getInt("TP_UOR"))));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} 
		
	}
	
	public String formatDateWS(String dtString) throws ParseException {
		SimpleDateFormat oldFormat = new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyy", Locale.US);
		SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss"); 
		Date dt = oldFormat.parse(dtString);
		return newFormat.format(dt);
	}
	
	//salvarCDebitoemoteCommand
	public void salvarCDebito() {
		try {
			System.out.println("oi salvarCDebito!!!");
			if ("1".equals(this.cboClasFraudeSelecionado))
				if (this.lstPontoVenda.isEmpty() || this.lstPontoVenda.get(0).getCD_UOR().isEmpty()) {
					RequestContext.getCurrentInstance().execute("alert('Selecione um Ponto de Venda.'); document.getElementById('frmEventoDetalheCDebito:txtAgenClieAjud').focus();");
					/*throw new Exception("");*/
				} 
			
			String txtQtdeOcor = "1";
			String strDtOcorrFormat = "";
			String strHrInicFormat = "";
			String strHrFimFormat = "";

			if(!this.cadastroCDebito.getStrFragHrInicio().isEmpty()) {
				Date hrInic = new SimpleDateFormat("HH:mm").parse(this.cadastroCDebito.getStrFragHrInicio());
				strHrInicFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(hrInic);			
			}
			
			if(!this.cadastroCDebito.getStrFragHrFinal().isEmpty()) {
				Date hrFim = new SimpleDateFormat("HH:mm").parse(this.cadastroCDebito.getStrFragHrFinal());
				strHrFimFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(hrFim);
			}
			System.out.println("cadastroCDebito model "+ToStringBuilder.reflectionToString(cadastroCDebito));
			String retorno = "";
			retorno = cadOsEventos.gravarFraudeCanalDetalhe2(
					this.strNrSeqId, 
					this.cadastroCDebito.getStrSeqConta(), // pNr_Sequ_Cnta_Bncr chave FK_FRAU_CNAL_03 
					this.cadastroCDebito.getStrSeqCartao(), // pNR_SEQU_CRTO chave  FK_FRAU_CNAL_02
					this.cadastroCDebito.getStrCdCanal(), 
					"", 
					"", 
					"", 
					"", 
					this.cadastroCDebito.getBlnClieAjud(),
					this.cadastroCDebito.getStrAgenClieAjud(), 
					this.cadastroCDebito.getBlnCrtoRece(), 
					this.cadastroCDebito.getBlnCrtoReti(), 
					this.cadastroCDebito.getIntFragLocal(), 
					strHrInicFormat, 
					strHrFimFormat, 
					this.cadastroCDebito.getStrFragCodAgen(), 
					this.cadastroCDebito.getStrFragNmAgen(), 
					this.cadastroCDebito.getStrFragDtOcorr().isEmpty() ? "" : this.formatDateWS(this.cadastroCDebito.getStrFragDtOcorr()), 
					this.cadastroCDebito.getStrFragNrTerm(), 
					this.cboSensorSelecionado, 
					this.cadastroCDebito.getStrFragOutros(), 
					"", 
					"", 
					"", 
					"", 
					"", 
					"", 
					"", 
					"", 
					"", 
					"", 
					"", 
					"", 
					"", 
					this.cadastroCDebito.getStrSeqEvento(), 
					this.cadastroCDebito.getBlnInteComl(),
					this.cboClasFraudeSelecionado, 
					txtQtdeOcor,
					this.cadastroCDebito.getStrCdCentroOrigem(), 
					this.cadastroCDebito.getStrCdCentroDestino(), 
					this.cadastroCDebito.getStrCdCentroOperante()	 							
					);
				
			if (!"True".equals(retorno)) 
				RequestContext.getCurrentInstance().execute("VerErro('"+retorno+"');");
			
//			String intTpPessoa = this.cadastroCDebito.getStrCPF_CNPJ().length() > 11 ? "2" : "1";
//			
//			//formato diferente...
//			if(!this.cadastroCDebito.getStrDtFrauCrto().isEmpty()) {
//				SimpleDateFormat oldFormat = new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyy", Locale.US);
//				SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss"); 
//				Date dt = oldFormat.parse(dtString);
//				return newFormat.format(dt);
//				
//				this.cadastroCDebito.getStrDtFrauCrto()
//			}
//			
//			cadOsEventos.atualizarCartaoParticipante(
//					this.cadastroCDebito.getStrNrTitular(), 
//					this.cadastroCDebito.getStrCdCanal(), 
//					this.cadastroCDebito.getStrSeqEvento(), 
//					this.strNrSeqId.isEmpty() ? "0" : this.strNrSeqId, 
//					this.cadastroCDebito.getStrSeqCartao().isEmpty() ? "0" : this.cadastroCDebito.getStrSeqCartao(), 
//					this.cadastroCDebito.getStrSeqConta().isEmpty() ? "0" : this.cadastroCDebito.getStrSeqConta(), 
//					"0", 
//					this.cadastroCDebito.getStrCPF_CNPJ(),
//					intTpPessoa, 
//					this.cadastroCDebito.getStrDtEmissao().isEmpty() ? "" : this.formatDateWS(this.cadastroCDebito.getStrDtEmissao()), 
//					this.cadastroCDebito.getStrDtAtivacao().isEmpty() ? "" : this.formatDateWS(this.cadastroCDebito.getStrDtAtivacao()), 
//					this.cadastroCDebito.getStrDtCancel().isEmpty() ? "" : this.formatDateWS(this.cadastroCDebito.getStrDtCancel()), 
//					this.cadastroCDebito.getStrLoginColab(), 
//					this.cadastroCDebito.getStrEndEnviado(), 
//					"", 
//					this.cadastroCDebito.getStrAgencia().isEmpty() ? "0" : this.cadastroCDebito.getStrAgencia(), 
//					this.cadastroCDebito.getStrConta().isEmpty() ? "0" : this.cadastroCDebito.getStrConta(), 
//					this.cadastroCDebito.getStrDtFrauCrto(), 
//					this.cadastroCDebito.getBlnCrtoSegr(), 
//					this.cadastroCDebito.getStrTpConta(), 
//					this.cadastroCDebito.getStrDtAbertura().isEmpty() ? "" : this.formatDateWS(this.cadastroCDebito.getStrDtAbertura()), 
//					this.cadastroCDebito.getStrNmGerente(), 
//					this.cadastroCDebito.getStrStatusCnt(), 
//					this.cadastroCDebito.getStrDtSoliCrto().isEmpty() ? "" : this.formatDateWS(this.cadastroCDebito.getStrDtSoliCrto()), 
//					this.cadastroCDebito.getStrDtAtivCrto().isEmpty() ? "" : this.formatDateWS(this.cadastroCDebito.getStrDtAtivCrto()), 
//					this.cadastroCDebito.getStrDtCancCrto().isEmpty() ? "" : this.formatDateWS(this.cadastroCDebito.getStrDtCancCrto()));
//			
//			if (!"True".equals(retorno)) 
//				RequestContext.getCurrentInstance().execute("VerErro('"+retorno+"');");
			
			RequestContext.getCurrentInstance().execute("validarCentros('3');");
			//RequestContext.getCurrentInstance().execute("alert('Detalhe Salvo com sucesso.');");			
				
		} catch (Exception e) {
			e.printStackTrace();
			RequestContext.getCurrentInstance().execute("VerErro('"+e.getMessage()+"');");
//			if (!e.getMessage().isEmpty())
//				try {
//					FacesContext.getCurrentInstance().getExternalContext()
//							.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
//				} catch (IOException e1) {
//					// TODO Auto-generated catch block
//					e1.printStackTrace();
//				}
		}
	}

	// INC 
	public void salvarOperacao() {
		try {
			for(CadastroOsSLinhaObjCanais canal : this.objCanais) {
				cadOsCanais.salvarOperacao(canal.getNR_SEQU_TRAN_FRAU_CNAL(), 
										   canal.getStrDtOper(), 
										   canal.getStrHrOper(), 
										   canal.getStrVlOper().replace(".", "").replace(",", "."), 
										   this.cadastroCDebito.getStrCdCentroOrigem(), 
										   this.cadastroCDebito.getStrCdCentroDestino(), 
										   this.cadastroCDebito.getStrCdCentroOperante(), 
										   this.cadastroCDebito.getStrNrQntdEven());
			}
			
			//Consultar de novo
			this.consultarOperacaoCanal();
			FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("frmEventoDetalheCDebito:tbInserirOp");
			RequestContext.getCurrentInstance().execute("alert('Detalhe Salvo com sucesso.');");
		} catch (Exception e) {
			e.printStackTrace();
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}		
	}
	
	public void clearBean() {
		String type = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("pageType").toString();
		String nrSeq = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("nrSeq").toString();
		switch(type) {
			case "FE":
				System.out.println("nrSeq - "+nrSeq+" - FE "+FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("CadastroOsDetalheProdutoFEBean"));
				if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("CadastroOsDetalheProdutoFEBean") != null)
					FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CadastroOsDetalheProdutoFEBean");
				RequestContext.getCurrentInstance().execute("document.getElementById('modal-detalhe-produto-fo-fe').src = 'hyb_cadastroos_detalheprodutofe.xhtml?strNrSeqTranFrau="+nrSeq+"'");
				RequestContext.getCurrentInstance().execute("PF('detalhe-produto-fo-fe').show();");
				break;
			
			case "FO":
				System.out.println("nrSeq - "+nrSeq+" - FO "+FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("CadastroOsDetalheProdutoFOBean"));
				if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("CadastroOsDetalheProdutoFOBean") != null)
					FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CadastroOsDetalheProdutoFOBean");
				RequestContext.getCurrentInstance().execute("document.getElementById('modal-detalhe-produto-fo-fe').src = 'hyb_cadastroos_detalheprodutofo.xhtml?strNrSeqTranFrau="+nrSeq+"'");
				RequestContext.getCurrentInstance().execute("PF('detalhe-produto-fo-fe').show();");
				break;
			
			default:
				System.out.println("clearBean - não encontrado");
				break;
		}
	}

	public void consultarOperacaoCanal() {
		try {
			this.intQntLancDbto = 0;
			this.intQntLancCrdt = 0;
			
			JSONObject json = new JSONObject(cadOsCanais.retornaOperacaoCanal(this.strNrSeqId));
			
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			
			System.out.println("CURS OR: " + pcursor);
			
			objCanais = new ArrayList<CadastroOsSLinhaObjCanais>();

			for (int i = 0; i < pcursor.length(); i++) {
				JSONObject item = pcursor.getJSONObject(i);
			
				String nR_SEQU_TIPO_OPER_TRAN = item.isNull("NR_SEQU_TIPO_OPER_TRAN") ? "" : item.get("NR_SEQU_TIPO_OPER_TRAN").toString();
				String cD_TRAN = item.isNull("CD_TRAN") ? "" : item.get("CD_TRAN").toString();
				String nM_OPER = item.isNull("NM_OPER") ? "" : item.get("NM_OPER").toString();
				String nM_SUB_OPER = item.isNull("NM_SUB_OPER") ? "" : item.get("NM_SUB_OPER").toString();
				String cD_PROD_AUXI = item.isNull("CD_PROD_AUXI") ? "" : item.get("CD_PROD_AUXI").toString();
				String nM_PROD_AUXI = item.isNull("NM_PROD_AUXI") ? "" : item.get("NM_PROD_AUXI").toString();
				String vL_TRAN = item.isNull("VL_TRAN") ? "0" : item.get("VL_TRAN").toString();
				String tP_CNTE = item.isNull("TP_CNTE") ? "" : item.get("TP_CNTE").toString();
				String dT_TRAN = item.isNull("DT_TRAN") ? "" : item.get("DT_TRAN").toString();
				String hR_TRAN = item.isNull("HR_TRAN") ? "" : item.get("HR_TRAN").toString();
				String dT_OCOR_FIXA = item.isNull("DT_OCOR_FIXA") ? "" : item.get("DT_OCOR_FIXA").toString();
				String dT_OCOR_PERI_INIC = item.isNull("DT_OCOR_PERI_INIC") ? "" : item.get("DT_OCOR_PERI_INIC").toString();
				String dT_OCOR_PERI_FINA = item.isNull("DT_OCOR_PERI_FINA") ? "" : item.get("DT_OCOR_PERI_FINA").toString();
				String cARD = item.isNull("CARD") ? "" : item.get("CARD").toString();
				String nR_SEQU_TRAN_FRAU_CNAL = item.isNull("NR_SEQU_TRAN_FRAU_CNAL") ? "" : item.get("NR_SEQU_TRAN_FRAU_CNAL").toString();
				String dESC_TRAN = item.isNull("DESC_TRAN") ? "" : item.get("DESC_TRAN").toString();
				String dESC_CNTE = item.isNull("DESC_CNTE") ? "" : item.get("DESC_CNTE").toString();
				
				String strDtOper;
				String strHrOper;
				String strVlOper;
				
				
				if (item.isNull("VL_TRAN")) {
					this.intValAux = 0;
					strVlOper = "";
				} else {
					this.intValAux = Double.parseDouble(item.get("VL_TRAN").toString().replace(",", "."));
					strVlOper = formataValorRecebido(item.get("VL_TRAN").toString());
				}
				
				if (tP_CNTE.equals("1")) {
					this.intQntLancDbto += 1;
				} else if (tP_CNTE.equals("2")) {
					this.intQntLancCrdt += 1;
				}
				
				if (dT_TRAN.equals("01/01/1900")) {
					strDtOper = "";
				} else {
					strDtOper = dT_TRAN;
				}
				
				if (hR_TRAN.equals("00:00")) {
					strHrOper = "";
				} else {
					strHrOper = hR_TRAN;
				}
				
				if( !item.isNull("DT_TRAN") && strDtOper.equals("") && !dT_OCOR_FIXA.equals("")) {
					strDtOper = dT_OCOR_FIXA;
				}
				
				CadastroOsSLinhaObjCanais modelCanais = new CadastroOsSLinhaObjCanais(nR_SEQU_TIPO_OPER_TRAN, cD_TRAN, nM_OPER, nM_SUB_OPER, cD_PROD_AUXI, nM_PROD_AUXI, vL_TRAN, tP_CNTE, dT_TRAN, hR_TRAN, dT_OCOR_FIXA, dT_OCOR_PERI_INIC, dT_OCOR_PERI_FINA, cARD, nR_SEQU_TRAN_FRAU_CNAL, dESC_TRAN, dESC_CNTE, strDtOper, strHrOper, strVlOper);
				objCanais.add(modelCanais);
				
				this.strDtPeriFixa = dT_OCOR_FIXA;
				this.strDtPeriIni = dT_OCOR_PERI_INIC;
				this.strDtPeriFim = dT_OCOR_PERI_FINA;
			}
			
			atualizarValores();
			
		} catch (JSONException | com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsCanais.WebServiceException e) {
			e.printStackTrace();
		}
	}
	
	public void verifUnidadeEnvolvCPV() {
		try {
			String tipoProduto = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("tipoProduto").toString();
			
			
			if (!cadastroCDebito.getStrCdCentroOrigem().equals("6494")) {
				JSONObject json = new JSONObject();
				json = new JSONObject(cadOsUnidEnvol.fnSelPVNr("1", cadastroCDebito.getStrCdCentroOrigem(), ""));
				JSONArray pcursor = json.getJSONArray("PCURSOR");
				
				if(pcursor.length() <= 0) {
					RequestContext.getCurrentInstance().execute("confirmarCentroOrigem("+tipoProduto+");");
				}else {
					//ret = true;
					verifUnidadeEnvolvCPVDest();
				}
			}else {
				verifUnidadeEnvolvCPVDest();
			}
			
			
		} catch (JSONException | com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsUnidEnvol.WebServiceException e) {
			e.printStackTrace();
		}
	}
	
	public void verifUnidadeEnvolvCPVDest() {
			try {
				
				String tipoProduto = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("tipoProduto").toString();
				
				if(!this.cadastroCDebito.getStrCdCentroDestino().equals("6494")) {
					
				
					JSONObject json = new JSONObject();
					json = new JSONObject(cadOsUnidEnvol.fnSelPVNr("1", this.cadastroCDebito.getStrCdCentroDestino(), ""));
					JSONArray pcursor = json.getJSONArray("PCURSOR");
					if (pcursor.length() <= 0) {
						this.destExists = false;
						RequestContext.getCurrentInstance().execute("confirmarCentroDest("+tipoProduto+");");
					} else {
						adicionarProduto(tipoProduto);
					}
					
				
				}else {
					adicionarProduto(tipoProduto);
				}
				
			} catch (JSONException | com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsUnidEnvol.WebServiceException e) {
				e.printStackTrace();
			}
	}
	
	public void carregaComboOper(Integer intContestacao) {
		this.objRsOperCorp = new ArrayList<OperCorpModel>();
		try {
			JSONObject json = new JSONObject();
			json = new JSONObject(cadOsCanais.consultarOperacao(this.qsCanal, intContestacao));
			//NM_OPER
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			for (int i = 0; i < pcursor.length(); i++) 
				this.objRsOperCorp.add(
						new OperCorpModel(pcursor.getJSONObject(i).isNull("NR_SEQU_TIPO_OPER_TRAN") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("NR_SEQU_TIPO_OPER_TRAN")),
									  pcursor.getJSONObject(i).isNull("CD_TRAN") ? "" : pcursor.getJSONObject(i).getString("CD_TRAN"),
									  pcursor.getJSONObject(i).isNull("NM_SUB_OPER") ? "" : pcursor.getJSONObject(i).getString("NM_SUB_OPER"),
										pcursor.getJSONObject(i).isNull("NM_OPER") ? "" : pcursor.getJSONObject(i).getString("NM_OPER"))
						);

				
		} catch (Exception e) {
			e.printStackTrace();
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}			
	}
	
	public void carregaComboOperAux(Integer intContestacao) {
		this.objRsOperAux = new ArrayList<OperAuxModel>();
		try {
			JSONObject json = new JSONObject();
			json = new JSONObject(cadOsCanais.consultarOperacaoAux(this.qsCanal, intContestacao));			
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			for (int i = 0; i < pcursor.length(); i++) 
				this.objRsOperAux.add(
						new OperAuxModel(pcursor.getJSONObject(i).isNull("CD_PROD_AUXI") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("CD_PROD_AUXI")),
									  pcursor.getJSONObject(i).isNull("NM_PROD_AUXI") ? "" : pcursor.getJSONObject(i).getString("NM_PROD_AUXI"))
						);

			System.out.println("objRsOperAux "+this.objRsOperAux.size());
				
		} catch (Exception e) {
			e.printStackTrace();
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}					
	}	
	
	public void handleIsCredito(AjaxBehaviorEvent abe) {
		this.isCredito = !this.isCredito;
		if (this.isCredito) {
			carregaComboOper(2);
			carregaComboOperAux(2);
		} 
		else {
			carregaComboOper(1);
			carregaComboOperAux(1);
		}
	}
	
	public void adicionarProduto(String tipoProduto) {
		//1-Corp 	2-Aux     3-Chamada ao SalvarOperacao
		System.out.println("adicionarProduto tipoProduto "+tipoProduto);
		if(tipoProduto.isEmpty()) {
			tipoProduto = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("tipoProduto").toString();
		}
		
		if("1".equals(tipoProduto) || "2".equals(tipoProduto)) {			
			String intTpContestacao = "";
			String strTipoContestacao = "";
			
			String strDescOperacao;
			
			if(tipoProduto.equals("1") && this.selectedOperCorp.isEmpty()){
				RequestContext.getCurrentInstance().execute("alert('Selecione uma Operação')");
			} else if (tipoProduto.equals("2") && this.selectedOperAux.isEmpty()) {
				//alert
				RequestContext.getCurrentInstance().execute("alert('Selecione uma Operação')");
			} else {
				
				try {
					
					if(tipoProduto.equals("1")) {					
						
						strDescOperacao = this.objRsOperCorp
												.stream()
												.filter(item -> item.getNR_SEQU_TIPO_OPER_TRAN().equals(this.selectedOperCorp))
												.collect(Collectors.toList())
												.get(0).getLabel().substring(7);
						
						System.out.println("DESCOPERACAO ->" + strDescOperacao);
					} else {					
						strDescOperacao = this.objRsOperAux
												.stream()
												.filter(item -> item.getCD_PROD_AUXI().equals(this.selectedOperAux))
												.collect(Collectors.toList())
												.get(0).getNM_PROD_AUXI();
					}
		
					// Checked
					if (this.isCredito) {
						intTpContestacao = "2"; // Credito
						strTipoContestacao = "Crédito";
					} else {
						strTipoContestacao = "Débito";
						intTpContestacao = "1"; // Debito
					}
					
					
					CadastroOsSLinhaObjCanais canaisModel = new CadastroOsSLinhaObjCanais();
					
					JSONObject json = new JSONObject();
					System.out.println(strNrSeqId +","+ this.selectedOperCorp+","+ this.selectedOperAux+","+ "1"+","+ intTpContestacao+","+ cadastroCDebito.getStrCdCentroOrigem()+","+ cadastroCDebito.getStrCdCentroDestino()+","+ cadastroCDebito.getStrCdCentroOperante()+","+ cadastroCDebito.getStrNrQntdEven());
					
					if(tipoProduto.equals("1")) {
						json = new JSONObject(cadOsCanais.inserirOperacaoRetSeqTranFrau(strNrSeqId, this.selectedOperCorp, "", "1", intTpContestacao, cadastroCDebito.getStrCdCentroOrigem(), cadastroCDebito.getStrCdCentroDestino() , cadastroCDebito.getStrCdCentroOperante(), cadastroCDebito.getStrNrQntdEven()));
					}else {
						json = new JSONObject(cadOsCanais.inserirOperacaoRetSeqTranFrau(strNrSeqId, "", this.selectedOperAux, "2", intTpContestacao, cadastroCDebito.getStrCdCentroOrigem(), cadastroCDebito.getStrCdCentroDestino() , cadastroCDebito.getStrCdCentroOperante(), cadastroCDebito.getStrNrQntdEven()));
					}
					
					JSONArray pcursor = json.getJSONArray("PCURSOR");
					JSONObject item = pcursor.getJSONObject(0);
					
					if (this.selectRdoContestacao.equals("1")) {
						this.intQntLancDbto += 1;
					} else if (this.selectRdoContestacao.equals("2")) {
						this.intQntLancCrdt += 1;
					}
					
					canaisModel.setNR_SEQU_TRAN_FRAU_CNAL(item.get("NR_SEQU_TRAN_FRAU_CNAL").toString());
					canaisModel.setDESC_TRAN(strDescOperacao);
					canaisModel.setDESC_CNTE(strTipoContestacao);
					canaisModel.setTP_CNTE(intTpContestacao);
					
					objCanais.add(canaisModel);
					this.atualizarValores();
					System.out.println("CLEAR RENDER IDS AQUI");
					FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("frmEventoDetalheCDebito:tbInserirOp");
					
				} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsCanais.WebServiceException  e) {
					e.printStackTrace();
					try {
						FacesContext.getCurrentInstance().getExternalContext()
								.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
		}
		else if ("3".equals(tipoProduto)){
			this.salvarOperacao();			
		}
	}
	
	public void atualizarValores() {
		double valDbt = 0;
		double valCrdt = 0;
		
		for (int i = 0; i < objCanais.size(); i++) {
			System.out.println("CNTE: " + objCanais.get(i).getTP_CNTE());
			if (objCanais.get(i).getTP_CNTE() != null) {
				if (objCanais.get(i).getTP_CNTE().equals("2")) {
					if (objCanais.get(i).getStrVlOper() == null || objCanais.get(i).getStrVlOper().equals("")) {
						valCrdt += 0;
					} else {
						valCrdt += Double.parseDouble(objCanais.get(i).getStrVlOper().toString().replace(".", "").replace(",", "."));
					}
					
				} else {
					if (objCanais.get(i).getStrVlOper() == null || objCanais.get(i).getStrVlOper().equals("")) {
						valDbt += 0;
					} else {
						valDbt += Double.parseDouble(objCanais.get(i).getStrVlOper().toString().replace(".", "").replace(",", "."));
					}
				}
			}
		}
		
		this.intQntValCrdt = valCrdt;
		this.intQntValDbto = valDbt;
	}
	
	public String formataValorRecebido(String valorRecebido) {
		String valor = NumberFormat.getCurrencyInstance(ptBR).format(Double.parseDouble(valorRecebido.replace(",", ".")));
		return valor.replace("R$", "").trim();
	}
	
	public void excluirOperacao() {
		String nrSeqFrauCanal = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqTranFrau");
		int index = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("index"));
		String descCnte = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("desc_cnte").toString();
				
		try {
			cadOsCanais.excluirOperacao(nrSeqFrauCanal);
			
			if (descCnte.equals("Débito") && this.intQntLancDbto >= 0) {
				this.intQntLancDbto -= 1;
			} else if (descCnte.equals("Crédito") && this.intQntLancCrdt >= 0) {
				this.intQntLancCrdt -= 1;
			}
			
			//objCanais.clear();
			objCanais.remove(index);
			this.atualizarValores();
		} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsCanais.WebServiceException e) {
			e.printStackTrace();
		}
	}
	
	public void handleChangeShowCartao(AjaxBehaviorEvent abe) {
		this.showCartaoSeguranca = !this.showCartaoSeguranca;
		System.out.println("showCartaoSeguranca!!!!!!!!");
	}
	
	public void handleChangeShowPontoVenda(AjaxBehaviorEvent abe) {
		System.out.println("showPontoVenda!!!");		
		this.showPontoVenda = this.cboClasFraudeSelecionado.equals("1");
		this.cadastroCDebito.setStrAgenClieAjud("");
		this.lstPontoVenda.clear();
	}
	
	public List<DropDownPesqOSModel> getObjRsLocal() {
		return objRsLocal;
	}

	public void setObjRsLocal(List<DropDownPesqOSModel> objRsLocal) {
		this.objRsLocal = objRsLocal;
	}

	public String getCboLocalSelecionado() {
		return cboLocalSelecionado;
	}

	public void setCboLocalSelecionado(String cboLocalSelecionado) {
		this.cboLocalSelecionado = cboLocalSelecionado;
	}

	public CadastroOsCDebitoModel getCadastroCDebito() {
		return cadastroCDebito;
	}

	public void setCadastroCDebito(CadastroOsCDebitoModel cadastroCDebito) {
		this.cadastroCDebito = cadastroCDebito;
	}

	public List<CadastroOsCDebitoPontoVendaModel> getLstPontoVenda() {
		return lstPontoVenda;
	}

	public void setLstPontoVenda(List<CadastroOsCDebitoPontoVendaModel> lstPontoVenda) {
		this.lstPontoVenda = lstPontoVenda;
	}

	public Boolean getShowPontoVenda() {
		return showPontoVenda;
	}

	public void setShowPontoVenda(Boolean showPontoVenda) {
		this.showPontoVenda = showPontoVenda;
	}

	public Boolean getShowCartaoSeguranca() {
		return showCartaoSeguranca;
	}

	public void setShowCartaoSeguranca(Boolean showCartaoSeguranca) {
		this.showCartaoSeguranca = showCartaoSeguranca;
	}

	public String getQsCanal() {
		return qsCanal;
	}

	public void setQsCanal(String qsCanal) {
		this.qsCanal = qsCanal;
	}

	public String getStrNrSeqId() {
		return strNrSeqId;
	}

	public void setStrNrSeqId(String strNrSeqId) {
		this.strNrSeqId = strNrSeqId;
	}

	public String getQsEvento() {
		return qsEvento;
	}

	public void setQsEvento(String qsEvento) {
		this.qsEvento = qsEvento;
	}

	public List<DropDownPesqOSModel> getObjRsCbo() {
		return objRsCbo;
	}

	public void setObjRsCbo(List<DropDownPesqOSModel> objRsCbo) {
		this.objRsCbo = objRsCbo;
	}

	public String getCboClasFraudeSelecionado() {
		return cboClasFraudeSelecionado;
	}

	public void setCboClasFraudeSelecionado(String cboClasFraudeSelecionado) {
		this.cboClasFraudeSelecionado = cboClasFraudeSelecionado;
	}

	public String getCboCSeg() {
		return cboCSeg;
	}

	public void setCboCSeg(String cboCSeg) {
		this.cboCSeg = cboCSeg;
	}

	public String getCboSensorSelecionado() {
		return cboSensorSelecionado;
	}

	public void setCboSensorSelecionado(String cboSensorSelecionado) {
		this.cboSensorSelecionado = cboSensorSelecionado;
	}

	public List<CadastroOsSLinhaObjCanais> getObjCanais() {
		return objCanais;
	}

	public void setObjCanais(List<CadastroOsSLinhaObjCanais> objCanais) {
		this.objCanais = objCanais;
	}

	public String getSelectRdoContestacao() {
		return selectRdoContestacao;
	}

	public void setSelectRdoContestacao(String selectRdoContestacao) {
		this.selectRdoContestacao = selectRdoContestacao;
	}

	public List<OperCorpModel> getObjRsOperCorp() {
		return objRsOperCorp;
	}

	public void setObjRsOperCorp(List<OperCorpModel> objRsOperCorp) {
		this.objRsOperCorp = objRsOperCorp;
	}

	public List<OperAuxModel> getObjRsOperAux() {
		return objRsOperAux;
	}

	public void setObjRsOperAux(List<OperAuxModel> objRsOperAux) {
		this.objRsOperAux = objRsOperAux;
	}

	public String getSelectedOperCorp() {
		return selectedOperCorp;
	}

	public void setSelectedOperCorp(String selectedOperCorp) {
		this.selectedOperCorp = selectedOperCorp;
	}

	public String getSelectedOperAux() {
		return selectedOperAux;
	}

	public void setSelectedOperAux(String selectedOperAux) {
		this.selectedOperAux = selectedOperAux;
	}

	public String getStrDtPeriFixa() {
		return strDtPeriFixa;
	}

	public void setStrDtPeriFixa(String strDtPeriFixa) {
		this.strDtPeriFixa = strDtPeriFixa;
	}

	public String getStrDtPeriIni() {
		return strDtPeriIni;
	}

	public void setStrDtPeriIni(String strDtPeriIni) {
		this.strDtPeriIni = strDtPeriIni;
	}

	public String getStrDtPeriFim() {
		return strDtPeriFim;
	}

	public void setStrDtPeriFim(String strDtPeriFim) {
		this.strDtPeriFim = strDtPeriFim;
	}

	public int getIntQntLancDbto() {
		return intQntLancDbto;
	}

	public void setIntQntLancDbto(int intQntLancDbto) {
		this.intQntLancDbto = intQntLancDbto;
	}

	public int getIbtQntLancCrdt() {
		return ibtQntLancCrdt;
	}

	public void setIbtQntLancCrdt(int ibtQntLancCrdt) {
		this.ibtQntLancCrdt = ibtQntLancCrdt;
	}

	public int getIntQntLancCrdt() {
		return intQntLancCrdt;
	}

	public void setIntQntLancCrdt(int intQntLancCrdt) {
		this.intQntLancCrdt = intQntLancCrdt;
	}

	public double getIntValAux() {
		return intValAux;
	}

	public void setIntValAux(double intValAux) {
		this.intValAux = intValAux;
	}

	public boolean isDestExists() {
		return destExists;
	}

	public void setDestExists(boolean destExists) {
		this.destExists = destExists;
	}

	public boolean isCredito() {
		return isCredito;
	}

	public void setCredito(boolean isCredito) {
		this.isCredito = isCredito;
	}

	public double getIntQntValDbto() {
		return intQntValDbto;
	}

	public void setIntQntValDbto(double intQntValDbto) {
		this.intQntValDbto = intQntValDbto;
	}

	public double getIntQntValCrdt() {
		return intQntValCrdt;
	}

	public void setIntQntValCrdt(double intQntValCrdt) {
		this.intQntValCrdt = intQntValCrdt;
	}

	
}