package com.altec.bsbr.app.hyb.dto;

public class CadastroOsFo {

	private String frauCanal;
	private String seqConta;
	private String seqCartao;
	private String canal;
	private String seqEvento;
	private String tipoConta;
	private String dtAbertura;
	private String agencia;
	private String gerente;
	private String status;
	
	public CadastroOsFo() {
		
	}
	
	public CadastroOsFo(String frauCanal, String seqConta, String seqCartao, String canal, String seqEvento,
			String tipoConta, String dtAbertura, String agencia, String gerente, String status) {
		super();
		this.frauCanal = frauCanal;
		this.seqConta = seqConta;
		this.seqCartao = seqCartao;
		this.canal = canal;
		this.seqEvento = seqEvento;
		this.tipoConta = tipoConta;
		this.dtAbertura = dtAbertura;
		this.agencia = agencia;
		this.gerente = gerente;
		this.status = status;
	}

	public String getFrauCanal() {
		return frauCanal;
	}

	public void setFrauCanal(String frauCanal) {
		this.frauCanal = frauCanal;
	}

	public String getSeqConta() {
		return seqConta;
	}

	public void setSeqConta(String seqConta) {
		this.seqConta = seqConta;
	}

	public String getSeqCartao() {
		return seqCartao;
	}

	public void setSeqCartao(String seqCartao) {
		this.seqCartao = seqCartao;
	}

	public String getCanal() {
		return canal;
	}

	public void setCanal(String canal) {
		this.canal = canal;
	}

	public String getSeqEvento() {
		return seqEvento;
	}

	public void setSeqEvento(String seqEvento) {
		this.seqEvento = seqEvento;
	}

	public String getTipoConta() {
		return tipoConta;
	}

	public void setTipoConta(String tipoConta) {
		this.tipoConta = tipoConta;
	}

	public String getDtAbertura() {
		return dtAbertura;
	}

	public void setDtAbertura(String dtAbertura) {
		this.dtAbertura = dtAbertura;
	}

	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public String getGerente() {
		return gerente;
	}

	public void setGerente(String gerente) {
		this.gerente = gerente;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
