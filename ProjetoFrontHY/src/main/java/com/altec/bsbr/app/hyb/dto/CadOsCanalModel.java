package com.altec.bsbr.app.hyb.dto;

public class CadOsCanalModel{

	private String codigo;
	private String pgDestino;
	private String nome;

	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getPgDestino() {
		return pgDestino;
	}
	public void setPgDestino(String pgDestino) {
		this.pgDestino = pgDestino;
	}

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
