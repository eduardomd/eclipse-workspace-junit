package com.altec.bsbr.app.hyb.dto;

public class RelosEstatisticoSegmentoPessoa {
	private String NM_SEGM;
	private String TP_PESS_CLIE;
	
	public RelosEstatisticoSegmentoPessoa() {}

	public String getNM_SEGM() {
		return NM_SEGM;
	}

	public void setNM_SEGM(String nM_SEGM) {
		NM_SEGM = nM_SEGM;
	}

	public String getTP_PESS_CLIE() {
		return TP_PESS_CLIE;
	}

	public void setTP_PESS_CLIE(String tP_PESS_CLIE) {
		TP_PESS_CLIE = tP_PESS_CLIE;
	}
	
	
}
