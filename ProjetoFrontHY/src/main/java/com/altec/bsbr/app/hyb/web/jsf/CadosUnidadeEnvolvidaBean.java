package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.xml.parsers.ParserConfigurationException;

import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import com.altec.bsbr.app.hyb.dto.CadosEquipatribLanchr;
import com.altec.bsbr.app.hyb.dto.CadosUnidadeEnvolvida;
import com.altec.bsbr.app.hyb.dto.UnidadeEnvolvida;
import com.altec.bsbr.app.hyb.dto.UsuarioIncModel;
import com.altec.bsbr.app.hyb.web.util.LogIncUtil;
import com.altec.bsbr.app.hyb.web.util.XHYUsuarioIncService;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmLog.XHYAdmLogEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsUnidEnvol.XHYCadOsUnidEnvolEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;

@Component("cadosUnidadeEnvolvidaBean")
@Scope("session")
public class CadosUnidadeEnvolvidaBean extends BasicBBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public CadosUnidadeEnvolvida cadosUnidadeEnvolvida;
	// public ClsCadOsUnidEnvolv objCadOsUnidEnvolv ;

	@Autowired
	private XHYCadOsUnidEnvolEndPoint objCadOsUnidEnvolv;
	@Autowired
	private XHYUsuarioIncService usuarioService;
	@Autowired
	private XHYAdmLogEndPoint log;

	private UsuarioIncModel usuario;

	private boolean txtCdPvDisable;
	private boolean txtCodDeptoDisable;
	private String imgDeptoCursor;
	private String imgPVCursor;
	private String strParamTP_UPOR;
	private String strParamCD_UPOR;
	private String strParamNM_UPOR;
	private UnidadeEnvolvida unidadeSel;

	private List<UnidadeEnvolvida> objRs;
	public List<UnidadeEnvolvida> unidadeEnvolida;
	public List<String> strTpUorList;
	public List<String> strCdUorList;
	public String xmlReturn;
	String strNrSeqOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
			.get("strNrSeqOs").toString();

	@PostConstruct
	public void init() {
		
		objRs = new ArrayList<UnidadeEnvolvida>();
		consultar();

		if (objRs.size() == 1) {
			unidadeSel = objRs.get(0);
		}
		instanciarUsuario();

		System.out.println("CPF ATUAL " + usuario.getCpfUsuario());
		System.out.println("CD AREA ATUAL " + usuario.getCdAreaUsuario());
	}

	public void cleanSession() {
		System.out.println("\n --CLEAN SESSION --");
		FacesContext context = FacesContext.getCurrentInstance();
		context.getExternalContext().getSessionMap().remove("cadosUnidadeEnvolvidaBean");
	}

	public void instanciarUsuario() {
		try {
			usuario = usuarioService.getDadosUsuario();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void consultar() {
		String retorno = "";
		JSONObject jsonRetorno;
		JSONArray pcursor;

		try {
			retorno = objCadOsUnidEnvolv.consultarUnidEnvolvOs(strNrSeqOs);
			retorno = retorno.replaceAll("null", "\"\"");
			jsonRetorno = new JSONObject(retorno);
			pcursor = jsonRetorno.getJSONArray("PCURSOR");
			if (pcursor.length() == 0) {
				String strMsg = "Ponto de Venda " + strParamCD_UPOR;
				RequestContext.getCurrentInstance().execute("alert('" + strMsg + " não encontrado!');");
			} else {
				objRs.clear();
			}

			for (int i = 0; i < pcursor.length(); i++) {
				JSONObject curr = pcursor.getJSONObject(i);
				UnidadeEnvolvida unidade = new UnidadeEnvolvida();

				unidade.setCD_UOR(curr.get("CD_UOR").toString().isEmpty() ? "-" : curr.get("CD_UOR").toString());
				unidade.setNM_REDE(curr.getString("NM_REDE").isEmpty() ? "-" : curr.getString("NM_REDE"));
				unidade.setNM_REGI(curr.getString("NM_REGI").isEmpty() ? "-" : curr.getString("NM_REGI"));
				unidade.setNM_UOR(curr.getString("NM_UOR"));
				unidade.setTP_UOR(curr.get("TP_UOR").toString().isEmpty() ? "-" : curr.get("TP_UOR").toString());
				this.objRs.add(unidade);

			}

		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

	}

	public void getElementosParaPesquisa() throws SAXException, IOException, ParserConfigurationException {

		strTpUorList = new ArrayList<String>();
		strCdUorList = new ArrayList<String>();
		unidadeSel = new UnidadeEnvolvida();

		String strXml;
		String strAcao = "CPV";

		if (strParamCD_UPOR.length() == 0 && strParamNM_UPOR.length() < 3) {
			RequestContext.getCurrentInstance().execute("alert('Entre com o Código ou o Nome da Unidade!');");
			return;
		}
		if (strParamTP_UPOR == null) {
			strParamTP_UPOR = "";
		}
		if (strParamCD_UPOR == null) {
			strParamCD_UPOR = "";
		}
		if (strParamNM_UPOR == null) {
			strParamNM_UPOR = "";
		}

		this.cadosUnidadeEnvolvidaExe(strAcao, strParamTP_UPOR, strParamCD_UPOR, strParamNM_UPOR);

	}

	public void getElementosParaSalvar() throws SAXException, IOException, ParserConfigurationException {

		String strXml;
		String strAcao = "SPV";

		cadosUnidadeEnvolvida = new CadosUnidadeEnvolvida();

		cadosUnidadeEnvolvida.setStrNrSeqOs(strNrSeqOs);

		this.cadosUnidadeEnvolvidaExe(strAcao, strParamTP_UPOR, strParamCD_UPOR, strParamNM_UPOR);

	}

	public void cadosUnidadeEnvolvidaExe(String strAcao, String strTpUOR, String strCdUOR, String strNmUOR)
			throws SAXException, IOException, ParserConfigurationException {

		String retorno = "";
		JSONObject jsonRetorno;
		JSONArray pcursor;

		if (strAcao != null && strAcao != "") {

			switch (strAcao) {
			case "CPV":
				try {
					retorno = objCadOsUnidEnvolv.fnSelPVNr(strTpUOR, strCdUOR, strNmUOR);
					retorno = retorno.replaceAll("null", "\"\"");
					jsonRetorno = new JSONObject(retorno);
					pcursor = jsonRetorno.getJSONArray("PCURSOR");
					if (pcursor.length() == 0) {
						String strMsg = "Ponto de Venda " + strParamCD_UPOR;
						RequestContext.getCurrentInstance().execute("alert('" + strMsg + " não encontrado!');");
					} else {
						objRs.clear();

						for (int i = 0; i < pcursor.length(); i++) {
							JSONObject curr = pcursor.getJSONObject(i);
							UnidadeEnvolvida unidade = new UnidadeEnvolvida();

							unidade.setCD_UOR(
									curr.get("CD_UOR").toString().isEmpty() ? "-" : curr.get("CD_UOR").toString());
							unidade.setNM_REDE(curr.getString("NM_REDE").isEmpty() ? "-" : curr.getString("NM_REDE"));
							unidade.setNM_REGI(curr.getString("NM_REGI").isEmpty() ? "-" : curr.getString("NM_REGI"));
							unidade.setNM_UOR(curr.getString("NM_UOR"));
							unidade.setTP_UOR(
									curr.get("TP_UOR").toString().isEmpty() ? "-" : curr.get("TP_UOR").toString());
							this.objRs.add(unidade);

						}
					}

				} catch (Exception e) {
					try {
						FacesContext.getCurrentInstance().getExternalContext()
								.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}

				unidadeEnvolida = objRs;

				break;

			case "CDP":
				retorno = "";

				try {
					retorno = objCadOsUnidEnvolv.fnSelAdmCentral(strCdUOR);
					retorno = retorno.replaceAll("null", "\"\"");
					jsonRetorno = new JSONObject(retorno);
					pcursor = jsonRetorno.getJSONArray("PCURSOR");
					if (pcursor.length() == 0) {
						String strMsg = "Ponto de Venda " + strParamCD_UPOR;
						RequestContext.getCurrentInstance().execute("alert('" + strMsg + " não encontrado!');");
					} else {
						objRs.clear();

						for (int i = 0; i < pcursor.length(); i++) {
							JSONObject curr = pcursor.getJSONObject(i);
							UnidadeEnvolvida unidade = new UnidadeEnvolvida();

							unidade.setCD_UOR(
									curr.get("CD_UOR").toString().isEmpty() ? "-" : curr.get("CD_UOR").toString());
							unidade.setNM_REDE(curr.getString("NM_REDE"));
							unidade.setNM_REGI(curr.getString("NM_REGI"));
							unidade.setNM_UOR(curr.getString("NM_UOR"));
							unidade.setTP_UOR(
									curr.get("TP_UOR").toString().isEmpty() ? "-" : curr.get("TP_UOR").toString());
							this.objRs.add(unidade);

						}
					}

				} catch (Exception e) {
					try {
						FacesContext.getCurrentInstance().getExternalContext()
								.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}

				break;
			case "SPV":
				if (unidadeSel == null) {
					System.out.println("Entrei aqui unidadeSel = null");
					unidadeSel = new UnidadeEnvolvida();
					unidadeSel.setTP_UOR("");
					unidadeSel.setCD_UOR("");
					unidadeSel.setNM_UOR("");
				}
				try {
					retorno = objCadOsUnidEnvolv.salvarUnidEnvolv(cadosUnidadeEnvolvida.getStrNrSeqOs(),
							unidadeSel.getTP_UOR(), unidadeSel.getCD_UOR(), unidadeSel.getNM_UOR());
					RequestContext.getCurrentInstance().execute("alert('Unidade Envolvida salva com sucesso!');");
					log.incluirAcaoLog(strNrSeqOs, Integer.toString(LogIncUtil.SALVAR_UNIDADE_ENVOLVIDA_OS),
							usuario.getCpfUsuario(), "");

					objRs.clear();
					objRs.add(unidadeSel);

				} catch (Exception e) {
					try {
						FacesContext.getCurrentInstance().getExternalContext()
								.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}

			}
		}

	}

	public void deletar() {

	}
	// -----------------------------------------------------------------------------------

	public String getstrNrSeqOs() {
		return cadosUnidadeEnvolvida.getStrNrSeqOs();
	}

	public boolean isTxtCdPvDisable() {
		return txtCdPvDisable;
	}

	public void setTxtCdPvDisable(boolean txtCdPvDisable) {
		this.txtCdPvDisable = txtCdPvDisable;
	}

	public boolean isTxtCodDeptoDisable() {
		return txtCodDeptoDisable;
	}

	public void setTxtCodDeptoDisable(boolean txtCodDeptoDisable) {
		this.txtCodDeptoDisable = txtCodDeptoDisable;
	}

	public String getImgDeptoCursor() {
		return imgDeptoCursor;
	}

	public void setImgDeptoCursor(String imgDeptoCursor) {
		this.imgDeptoCursor = imgDeptoCursor;
	}

	public String getImgPVCursor() {
		return imgPVCursor;
	}

	public void setImgPVCursor(String imgPVCursor) {
		this.imgPVCursor = imgPVCursor;
	}

	public List<UnidadeEnvolvida> getUnidadeEnvolida() {
		return unidadeEnvolida;
	}

	public void setUnidadeEnvolida(List<UnidadeEnvolvida> unidadeEnvolida) {
		this.unidadeEnvolida = unidadeEnvolida;
	}

	public List<String> getStrTpUorList() {
		return strTpUorList;
	}

	public void setStrTpUorList(List<String> strTpUorList) {
		this.strTpUorList = strTpUorList;
	}

	public List<String> getStrCdUorList() {
		return strCdUorList;
	}

	public void setStrCdUorList(List<String> strCdUorList) {
		this.strCdUorList = strCdUorList;
	}

	public String getXmlReturn() {
		return xmlReturn;
	}

	public void setXmlReturn(String xmlReturn) {
		this.xmlReturn = xmlReturn;
	}

	public String getStrParamTP_UPOR() {
		return strParamTP_UPOR;
	}

	public void setStrParamTP_UPOR(String strParamTP_UPOR) {
		this.strParamTP_UPOR = strParamTP_UPOR;
	}

	public String getStrParamCD_UPOR() {
		return strParamCD_UPOR;
	}

	public void setStrParamCD_UPOR(String strParamCD_UPOR) {
		this.strParamCD_UPOR = strParamCD_UPOR;
	}

	public String getStrParamNM_UPOR() {
		return strParamNM_UPOR;
	}

	public void setStrParamNM_UPOR(String strParamNM_UPOR) {
		this.strParamNM_UPOR = strParamNM_UPOR;
	}

	public UnidadeEnvolvida getUnidadeSel() {
		return unidadeSel;
	}

	public void setUnidadeSel(UnidadeEnvolvida unidadeSel) {
		this.unidadeSel = unidadeSel;
	}

	public List<UnidadeEnvolvida> getObjRs() {
		return objRs;
	}

	public void setObjRs(List<UnidadeEnvolvida> objRs) {
		this.objRs = objRs;
	}

	public String getStrNrSeqOs() {
		return strNrSeqOs;
	}

	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}

}
