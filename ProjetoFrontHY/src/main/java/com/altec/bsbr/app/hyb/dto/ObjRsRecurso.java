package com.altec.bsbr.app.hyb.dto;

public class ObjRsRecurso {

	private String CPF;
	private String maskCPF;
	private String NOME;
	private String MATRICULA;
	private String CARGO;
	private String TIPO_RECURSO;
	private String AREA;
	private String EXCLUSAO;

	public String getCPF() {
		return CPF;
	}
	public void setCPF(String cPF) {
		CPF = cPF;
	}
	public String getmaskCPF() {
		return CPF.substring(0, 3)+"."+CPF.substring(3, 6)+"."+CPF.substring(6, 9)+"-"+CPF.substring(9);
	}
	public String getNOME() {
		return NOME;
	}
	public void setNOME(String nOME) {
		NOME = nOME;
	}
	public String getMATRICULA() {
		return MATRICULA;
	}
	public void setMATRICULA(String mATRICULA) {
		MATRICULA = mATRICULA;
	}
	public String getCARGO() {
		return CARGO;
	}
	public void setCARGO(String cARGO) {
		CARGO = cARGO;
	}
	public String getTIPO_RECURSO() {
		return TIPO_RECURSO;
	}
	public void setTIPO_RECURSO(String tIPO_RECURSO) {
		TIPO_RECURSO = tIPO_RECURSO;
	}
	public String getAREA() {
		return AREA;
	}
	public void setAREA(String aREA) {
		AREA = aREA;
	}
	public String getEXCLUSAO() {
		return EXCLUSAO;
	}
	public void setEXCLUSAO(String eXCLUSAO) {
		EXCLUSAO = eXCLUSAO;
	}
	
}
