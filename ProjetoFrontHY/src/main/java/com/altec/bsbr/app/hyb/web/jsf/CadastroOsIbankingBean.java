package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.Map;
import java.util.stream.Collectors;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.servlet.http.HttpServlet;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ibm.icu.text.SimpleDateFormat;
import com.ibm.icu.util.Calendar;
import com.ibm.icu.util.TimeZone;
import com.altec.bsbr.app.hyb.dto.CadastroOsIbanking;
import com.altec.bsbr.app.hyb.dto.CadastroOsSLinhaIBanking;
import com.altec.bsbr.app.hyb.dto.CadastroOsSLinhaObjCanais;
import com.altec.bsbr.app.hyb.dto.OperAuxModel;
import com.altec.bsbr.app.hyb.dto.OperCorpModel;
import com.altec.bsbr.app.hyb.web.util.Util;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsCanais.XHYCadOsCanaisEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsEventos.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsEventos.XHYCadOsEventosEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsUnidEnvol.XHYCadOsUnidEnvolEndPoint;
import com.altec.bsbr.app.hyb.web.jsf.ClsCadOsEventos;
import com.altec.bsbr.app.hyb.web.jsf.ContaIBanking;

@Component("cadastroOsIbankingBean")
@Scope("session")
public class CadastroOsIbankingBean extends BasicBBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String CD_CANAL;
	private String strNrSeqDetalhe;

	
	private boolean blnSalvar;
	private String strErro;
	private String strQtdeOcor;
	private CadastroOsIbanking objRsEvento;
	private boolean showDatas;

	private boolean origExists;
	private boolean destExists;
	private boolean isCredito;
	
	private String selectedOperCorp;
	private String selectRdoContestacao;
	private String strNrCanal;
	private String strNrEvento;
	private String selectedOperAux;
	private String strDtPeriFixa;
	private String strDtPeriIni;
	private String strDtPeriFim;
	
	
	private double intQntValDbto;
	private double intQntValCrdt;
	private int intQntLancDbto;
	private int ibtQntLancCrdt;
	private int intQntLancCrdt;
	private double intValAux;
	
	private List<OperCorpModel> objRsOperCorp;
	private List<OperAuxModel> objRsOperAux;
	private List<CadastroOsSLinhaObjCanais> objCanais;
	
	private Locale ptBR;
	
	
	@Autowired
	private XHYCadOsEventosEndPoint objEvento;
	
	@Autowired
	private XHYCadOsUnidEnvolEndPoint cadOsUnidEnvolv;
	
	@Autowired
	private XHYCadOsCanaisEndPoint cadOsCanais;
	
	private List<CadastroOsSLinhaIBanking> iBanking;

	@PostConstruct
	public void init() {
		
		ptBR = new Locale("pt", "BR");
		
		intQntLancDbto = 0;
		ibtQntLancCrdt = 0;
		intQntLancCrdt = 0;
		intValAux = 0;
		strNrSeqDetalhe = "";
		CD_CANAL = "";
		
		iBanking = new ArrayList<CadastroOsSLinhaIBanking>();

		this.strNrCanal = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("qsCanal").toString();
		this.strNrEvento = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("qsEvento").toString();
		this.CD_CANAL = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("qsCanal").toString();
		this.strNrSeqDetalhe =  FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("NrSeqDetalhe").toString();
		
		
		consultarFraudeCanalDetalhe();
		consultarContasAcessadas();
		consultarOperacaoCanal();
		carregaComboOper(1);
		carregaComboOperAux(1);
		
		
		this.selectRdoContestacao = "1";
		this.showDatas = !this.objRsEvento.getBlnCrtoSegr().equals("0"); 
		
		
		
		
	}
	public void validaDatasPrint(String id) {
		String data = "";
		
		switch(id) {
			case "frmEventoDetalheIBanking:txtDtSolicCrto_input":
				data = objRsEvento.getStrDtSoliCrto();
				break;
			case "frmEventoDetalheIBanking:txtDtAtivCrto_input":
				data = objRsEvento.getStrDtAtivCrto();
				break;
			case "frmEventoDetalheIBanking:txtDtCancCrto_input":
				data = objRsEvento.getStrDtCancCrto();
				break;
			case "frmEventoDetalheIBanking:txtDtFrauCrto_input":
				data = objRsEvento.getStrDtFrauCrto();
				break;
		
		
		}
		if(!data.contains("/")) {
		
			data = converterData(data);
		}
		 
		validaDatas(id, data);
	}
	public String converterData(String dataAconverter) {
		Date dt = null;
		String data = "";
		try {
			dt = new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyy", Locale.US).parse(dataAconverter);
			 data = new SimpleDateFormat("dd/MM/yyyy").format(dt); 
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return data;
		
	}
	public void validaDatas(String objId, String objValue) {
		
		String[]arrDtSolicCrto;
		String[]arrDtAtivCrto;
		String[]arrDtCancCrto;
		String[]arrDtFrauCrto;
		
		//Data Solicitação
		if (objId.equals("frmEventoDetalheIBanking:txtDtSolicCrto_input"))
	    {
			
	        arrDtSolicCrto = objValue.split("/");  
	        if(objRsEvento.getStrDtAtivCrto().contains("/")) {
	        	arrDtAtivCrto = objRsEvento.getStrDtAtivCrto().split("/");
	                            
	        } else { 
	        	objRsEvento.setStrDtAtivCrto(converterData(objRsEvento.getStrDtAtivCrto()));
	        	arrDtAtivCrto = objRsEvento.getStrDtAtivCrto().split("/");
	        }
	        
	        if((Integer.valueOf(arrDtSolicCrto[2] + arrDtSolicCrto[1] + arrDtSolicCrto[0])) > (Integer.valueOf(arrDtAtivCrto[2] + arrDtAtivCrto[1] + arrDtAtivCrto[0])))
	        {
				RequestContext.getCurrentInstance().execute("alert('Data inválida!\\nA data da solicitação deve ser menor ou igual à data de ativação!')");
				RequestContext.getCurrentInstance().execute("document.getElementById('frmEventoDetalheIBanking:txtDtSolicCrto_input').value = '' ");
				objRsEvento.setStrDtSoliCrto("");
	           // obj.focus(); 
	            return;         
	        }
	    }
		
		//Data Ativação
		if (objId.equals("frmEventoDetalheIBanking:txtDtAtivCrto_input"))
	    {
			
			if(objRsEvento.getStrDtSoliCrto().contains("/")) {
				arrDtSolicCrto = objRsEvento.getStrDtSoliCrto().split("/");
	        
			} else { 
	        	objRsEvento.setStrDtSoliCrto(converterData(objRsEvento.getStrDtSoliCrto()));
	        	arrDtSolicCrto = objRsEvento.getStrDtSoliCrto().split("/");
	        }
			
	        arrDtAtivCrto =  objValue.split("/"); 
	                            
	        if((Integer.valueOf(arrDtAtivCrto[2] + arrDtAtivCrto[1] + arrDtAtivCrto[0])) < (Integer.valueOf(arrDtSolicCrto[2] + arrDtSolicCrto[1] + arrDtSolicCrto[0])))
	        {
				RequestContext.getCurrentInstance().execute("alert('Data inválida!\\nA data de ativação deve ser maior ou igual à data de solicitação!')");
				RequestContext.getCurrentInstance().execute("document.getElementById('frmEventoDetalheIBanking:txtDtAtivCrto_input').value = '' ");
				objRsEvento.setStrDtAtivCrto("");
	           // obj.focus(); 
	            return;         
	        }
	    }
		
		//Data Cancelamento
		if (objId.equals("frmEventoDetalheIBanking:txtDtCancCrto_input"))
	    {
			
			
			if(objRsEvento.getStrDtAtivCrto().contains("/")) {
	        	arrDtAtivCrto = objRsEvento.getStrDtAtivCrto().split("/");
	                            
	        } else { 
	        	objRsEvento.setStrDtAtivCrto(converterData(objRsEvento.getStrDtAtivCrto()));
	        	arrDtAtivCrto = objRsEvento.getStrDtAtivCrto().split("/");
	        }
	        
	        arrDtCancCrto =  objValue.split("/"); 
	                            
	        if((Integer.valueOf(arrDtCancCrto[2] + arrDtCancCrto[1] + arrDtCancCrto[0])) < (Integer.valueOf(arrDtAtivCrto[2] + arrDtAtivCrto[1] + arrDtAtivCrto[0])))
	        {
				RequestContext.getCurrentInstance().execute("alert('Data inválida!\\nA data do cancelamento deve ser maior ou igual à data de ativação!')");
				RequestContext.getCurrentInstance().execute("document.getElementById('frmEventoDetalheIBanking:txtDtCancCrto_input').value = '' ");
				objRsEvento.setStrDtCancCrto("");
	           // obj.focus(); 
	            return;         
	        }
	    }
		
		//Data Fraude
		
		if (objId.equals("frmEventoDetalheIBanking:txtDtFrauCrto_input"))
	    {
			
			
			if(objRsEvento.getStrDtAtivCrto().contains("/")) {
	        	arrDtAtivCrto = objRsEvento.getStrDtAtivCrto().split("/");
	                            
	        } else { 
	        	objRsEvento.setStrDtAtivCrto(converterData(objRsEvento.getStrDtAtivCrto()));
	        	arrDtAtivCrto = objRsEvento.getStrDtAtivCrto().split("/");
	        }
	        arrDtFrauCrto =  objValue.split("/"); 
	                            
	        if((Integer.valueOf(arrDtFrauCrto[2] + arrDtFrauCrto[1] + arrDtFrauCrto[0])) < (Integer.valueOf(arrDtAtivCrto[2] + arrDtAtivCrto[1] + arrDtAtivCrto[0])))
	        {
				RequestContext.getCurrentInstance().execute("alert('Data inválida!\\nA data da fraude deve ser maior ou igual à data de ativação!')");
				RequestContext.getCurrentInstance().execute("document.getElementById('frmEventoDetalheIBanking:txtDtFrauCrto_input').value = '' ");
				objRsEvento.setStrDtFrauCrto("");
	           // obj.focus(); 
	            return;         
	        }
	    }
		
		
	
	}
	
	public void consultarFraudeCanalDetalhe() {
		String retorno = "";
		
		try {
			retorno = objEvento.consultarFraudeCanalDetalhe(Long.parseLong(this.strNrSeqDetalhe));
			JSONObject jsonRetorno = new JSONObject(retorno);
			JSONArray pcursor = jsonRetorno.getJSONArray("PCURSOR");
			
				JSONObject curr = pcursor.getJSONObject(0);
				objRsEvento = new CadastroOsIbanking();
				objRsEvento.setStrSeqEvento(curr.isNull("NR_SEQU_FRAU_EVEN") ? "" : curr.get("NR_SEQU_FRAU_EVEN").toString());
				objRsEvento.setStrNrSeqOs(curr.isNull("NR_SEQU_ORDE_SERV") ? "" : curr.get("NR_SEQU_ORDE_SERV").toString());
				objRsEvento.setStrTitular(curr.isNull("NM_PARP") ? "" : curr.get("NM_PARP").toString());
				objRsEvento.setStrSeqConta(curr.isNull("NR_SEQU_CNTA_BNCR") ? "" : curr.get("NR_SEQU_CNTA_BNCR").toString());
				objRsEvento.setStrCartao(curr.isNull("NR_SEQU_CRTO") ? "" : curr.get("NR_SEQU_CRTO").toString());
				String temp = "0000" + curr.get("CD_BANC").toString();
				objRsEvento.setStrAgencia(curr.isNull("CD_BANC") ? "" : curr.get("CD_BANC").toString().isEmpty() ?  "": temp.substring(temp.length()-4, temp.length()));
				objRsEvento.setStrConta(curr.isNull("NR_CNTA") ? "" : curr.get("NR_CNTA").toString());
				objRsEvento.setStrCartao(curr.isNull("NR_CRTO") ? "" : curr.get("NR_CRTO").toString());
				objRsEvento.setStrCdCanal(curr.isNull("CD_CNAL") ? "" : curr.get("CD_CNAL").toString());
				objRsEvento.setStrCPF_CNPJ(curr.isNull("NR_CPF_CNPJ_TITL") ? "" : curr.get("NR_CPF_CNPJ_TITL").toString());
				objRsEvento.setBlnInteComl(curr.isNull("IN_ITSS_COME") ? "0" : curr.get("IN_ITSS_COME").toString());
				objRsEvento.setStrIPCliente(curr.isNull("NR_ENDE_IP_CLIE") ? "" : curr.get("NR_ENDE_IP_CLIE").toString());
				objRsEvento.setStrIPFraude(curr.isNull("NR_ENDE_IP_FRAU") ? "" : curr.get("NR_ENDE_IP_FRAU").toString());
				objRsEvento.setBlnCrtoSegr(curr.isNull("IN_CRTO_SEGR") ? "0" : curr.get("IN_CRTO_SEGR").toString());
				objRsEvento.setStrDtSoliCrto(curr.isNull("DT_SOLI_CRTO_SEGR") ? "" : curr.get("DT_SOLI_CRTO_SEGR").toString());
				objRsEvento.setStrDtCancCrto(curr.isNull("DT_CANC_CRTO_SEGR") ? "" : curr.get("DT_CANC_CRTO_SEGR").toString());
				objRsEvento.setStrDtAtivCrto(curr.isNull("DT_ATIV_CRTO_SEGR") ? "" : curr.get("DT_ATIV_CRTO_SEGR").toString());
				String dtFrau = "";
				if(!curr.isNull("DT_PRMR_FRAU")) {
					
					dtFrau = curr.get("DT_PRMR_FRAU").toString();
					Timestamp t = new Timestamp(Long.parseLong(dtFrau));
					SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
					Date date = new Date(t.getTime());
					dtFrau = sf.format(date).toString();
				}
				objRsEvento.setStrDtFrauCrto(dtFrau);
				objRsEvento.setIntInfrPopup(curr.isNull("IN_INFO_PPUP") ? 0 : (Integer.valueOf(curr.get("IN_INFO_PPUP").toString())));
				objRsEvento.setStrNrQntdEven(curr.isNull("NR_QNTD_EVEN") ? "" : curr.get("NR_QNTD_EVEN").toString());
				objRsEvento.setStrCdCentroOrigem(curr.isNull("CD_CENT_CUST_ORIG") ? "" : curr.get("CD_CENT_CUST_ORIG").toString());
				objRsEvento.setStrCdCentroDestino(curr.isNull("CD_CENT_CUST_DEST") ? "" : curr.get("CD_CENT_CUST_DEST").toString());
				objRsEvento.setStrCdCentroOperante(curr.isNull("CD_CENT_CUST_OPNT") ? "6494" : curr.get("CD_CENT_CUST_OPNT").toString());
				objRsEvento.setStrContabilizado(curr.isNull("CONTABILIZADO") ? "" : curr.get("CONTABILIZADO").toString());
				
			
			
			
		} catch (Exception e) {
				try {
					FacesContext.getCurrentInstance().getExternalContext()
							.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
	
	public String consultarContasAcessadas() {
		String retorno = "";
		String strRetorno = "";
		try {
			retorno = objEvento.consultarContaIBanking(Long.parseLong(this.strNrSeqDetalhe));
			JSONObject jsonRetorno = new JSONObject(retorno);
			JSONArray pcursor = jsonRetorno.getJSONArray("PCURSOR");
			iBanking = new ArrayList<CadastroOsSLinhaIBanking>();

			for (int i = 0; i < pcursor.length(); i++) {
				JSONObject item = pcursor.getJSONObject(i);
			
				String frauCnal = item.isNull("NR_SEQU_FRAU_CNAL") ? "" : item.get("NR_SEQU_FRAU_CNAL").toString();
				String cntaAces = item.isNull("NR_CNTA_ACES") ? "" : item.get("NR_CNTA_ACES").toString();
				
				CadastroOsSLinhaIBanking modelBank = new CadastroOsSLinhaIBanking(frauCnal, cntaAces);
				
				iBanking.add(modelBank);
			}
			
		} catch (Exception e) {
				try {
					FacesContext.getCurrentInstance().getExternalContext()
							.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		


		return strRetorno;
	}

	public void fillExcluirContaIPExe() {

		String pConta = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("conta");
		String pCanal = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("canal");

		try {
		objEvento.excluirContaIBanking(Long.parseLong(pCanal),Long.parseLong(pConta));
		consultarContasAcessadas();
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	public void salvar() {
		String resposta = "";
		try {
		salvarContaIp();
	
		
		
		 resposta = objEvento.gravarFraudeCanalDetalhe(this.getstrNrSeqDetalhe(), 
				objRsEvento.getStrSeqConta().isEmpty()? "0":objRsEvento.getStrSeqConta(),
				objRsEvento.getStrCartao().isEmpty()? "0":objRsEvento.getStrCartao(),
				objRsEvento.getStrCdCanal(),
				String.valueOf(objRsEvento.getIntInfrPopup()),
				objRsEvento.getStrIPCliente(), 
				objRsEvento.getStrIPFraude(),
				"", 
				"", 
				"", 
				"", 
				"", 
				"", 
				"", 
				"", 
				"",
				"", 
				"", 
				"", 
				"", 
				"", 
				"", 
				"", 
				"", 
				"", 
				"", 
				"", 
				"", 
				"", 
				"", 
				"", 
				"", 
				"", 
				"", 
				objRsEvento.getStrSeqEvento(), 
				(objRsEvento.getBlnInteComl() == "false" || objRsEvento.getBlnInteComl().isEmpty()) ? "0": "1", 
				objRsEvento.getStrNrQntdEven());
		
		if(!resposta.equals("True")) {
			RequestContext.getCurrentInstance().execute("VerErro('"+resposta+"')");
			return;
		}
		resposta = objEvento.atualizarContaParticipante(objRsEvento.getStrSeqConta().isEmpty()? "0":objRsEvento.getStrSeqConta(),
				objRsEvento.getStrDtFrauCrto().contains("/")?formatarDataBack(objRsEvento.getStrDtFrauCrto(),"dd/MM/yyyy"):formatarDataBack(objRsEvento.getStrDtFrauCrto(),"EEE MMM d HH:mm:ss zzz yyy"),
				objRsEvento.getBlnCrtoSegr().isEmpty()?"0":objRsEvento.getBlnCrtoSegr(), 
				objRsEvento.getStrDtSoliCrto().contains("/")?formatarDataBack(objRsEvento.getStrDtSoliCrto(),"dd/MM/yyyy"):formatarDataBack(objRsEvento.getStrDtSoliCrto(),"EEE MMM d HH:mm:ss zzz yyy"), 
				objRsEvento.getStrDtAtivCrto().contains("/")?formatarDataBack(objRsEvento.getStrDtAtivCrto(),"dd/MM/yyyy"):formatarDataBack(objRsEvento.getStrDtAtivCrto(),"EEE MMM d HH:mm:ss zzz yyy"), 
				objRsEvento.getStrDtCancCrto().contains("/")?formatarDataBack(objRsEvento.getStrDtCancCrto(),"dd/MM/yyyy"):formatarDataBack(objRsEvento.getStrDtCancCrto(),"EEE MMM d HH:mm:ss zzz yyy"), 
				"", 
				"", 
				"", 
				"", 
				"", 
				objRsEvento.getStrAgencia().isEmpty()?"0":objRsEvento.getStrAgencia());
		RequestContext.getCurrentInstance().execute("alert('Detalhe Salvo com sucesso.')");
		consultarContasAcessadas();
		if(!resposta.equals("True")) {
			RequestContext.getCurrentInstance().execute("VerErro('"+resposta+"')");
			return;
		}
		
		
		
		} catch (Exception e) {
				// perguntar se quer visualizar erro
			if(resposta.isEmpty()) {
				resposta = e.getMessage();
			}
			RequestContext.getCurrentInstance().execute("VerErro("+resposta+")");
			
		}
	}
	
	public void salvarOperacao() {
        try {
                for(CadastroOsSLinhaObjCanais canal : this.objCanais) {
                       cadOsCanais.salvarOperacao(canal.getNR_SEQU_TRAN_FRAU_CNAL(), 
                                                                         canal.getStrDtOper(), 
                                                                         canal.getStrHrOper(), 
                                                                          canal.getStrVlOper().replace(".", "").replace(",", "."), 
                                                                         this.objRsEvento.getStrCdCentroOrigem(), 
                                                                         this.objRsEvento.getStrCdCentroDestino(), 
                                                                         this.objRsEvento.getStrCdCentroOperante(), 
                                                                         this.objRsEvento.getStrNrQntdEven());
                }
                
                //Consultar de novo
                this.consultarOperacaoCanal();
                FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("frmEventoDetalheCDebito:tbInserirOp");
        } catch (Exception e) {
                e.printStackTrace();
                try {
                       FacesContext.getCurrentInstance().getExternalContext()
                                       .redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
                } catch (IOException e1) {
                       // TODO Auto-generated catch block
                       e1.printStackTrace();
                }
        }               
}

	public String formatarDataBack(String d,String formato) {
		Date dt = null;
		try {
			dt = new SimpleDateFormat(formato, Locale.US).parse(d);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(dt);
	}
	
	public void redirecionaParaErro(Exception e) {
		
	}
	
	
	public void salvarContaIp() {
		try {
			for(int i =0 ; i < iBanking.size(); i++) {
				objEvento.gravarContaIBanking(Long.parseLong(this.getstrNrSeqDetalhe()),Long.parseLong(iBanking.get(i).getNR_CNTA_ACES()));
			}
			consultarContasAcessadas();
		} catch (Exception e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	public void verifUnidadeEnvolvCPV() {
		try {
			String tipoProduto = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("tipoProduto").toString();
			
			
			if (!objRsEvento.getStrCdCentroOrigem().equals("6494")) {
				JSONObject json = new JSONObject();
				json = new JSONObject(cadOsUnidEnvolv.fnSelPVNr("1", objRsEvento.getStrCdCentroOrigem(), ""));
				JSONArray pcursor = json.getJSONArray("PCURSOR");
				
				if(pcursor.length() <= 0) {
					RequestContext.getCurrentInstance().execute("confirmarCentroOrigem("+tipoProduto+");");
				}else {
					//ret = true;
					verifUnidadeEnvolvCPVDest();
				}
			}else {
				verifUnidadeEnvolvCPVDest();
			}
			
			
		} catch (JSONException | com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsUnidEnvol.WebServiceException e) {
			e.printStackTrace();
		}
	}
	
	public void verifUnidadeEnvolvCPVDest() {
		try {
			
			String tipoProduto = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("tipoProduto").toString();
			
			if(!this.objRsEvento.getStrCdCentroDestino().equals("6494")) {
				
			
				JSONObject json = new JSONObject();
				json = new JSONObject(cadOsUnidEnvolv.fnSelPVNr("1", this.objRsEvento.getStrCdCentroDestino(), ""));
				JSONArray pcursor = json.getJSONArray("PCURSOR");
				if (pcursor.length() <= 0) {
					this.destExists = false;
					RequestContext.getCurrentInstance().execute("confirmarCentroDest("+tipoProduto+");");
				} else {
					adicionarProduto(tipoProduto);
				}
				
			
			}else {
				adicionarProduto(tipoProduto);
			}
			
		} catch (JSONException | com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsUnidEnvol.WebServiceException e) {
			e.printStackTrace();
		}
	}
	
	
	
	public void consultarOperacaoCanal() {
		try {
			this.intQntLancDbto = 0;
			this.intQntLancCrdt = 0;
			
			JSONObject json = new JSONObject(cadOsCanais.retornaOperacaoCanal(this.strNrSeqDetalhe));
			
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			objCanais = new ArrayList<CadastroOsSLinhaObjCanais>();

			for (int i = 0; i < pcursor.length(); i++) {
				JSONObject item = pcursor.getJSONObject(i);
			
				String nR_SEQU_TIPO_OPER_TRAN = item.isNull("NR_SEQU_TIPO_OPER_TRAN") ? "" : item.get("NR_SEQU_TIPO_OPER_TRAN").toString();
				String cD_TRAN = item.isNull("CD_TRAN") ? "" : item.get("CD_TRAN").toString();
				String nM_OPER = item.isNull("NM_OPER") ? "" : item.get("NM_OPER").toString();
				String nM_SUB_OPER = item.isNull("NM_SUB_OPER") ? "" : item.get("NM_SUB_OPER").toString();
				String cD_PROD_AUXI = item.isNull("CD_PROD_AUXI") ? "" : item.get("CD_PROD_AUXI").toString();
				String nM_PROD_AUXI = item.isNull("NM_PROD_AUXI") ? "" : item.get("NM_PROD_AUXI").toString();
				String vL_TRAN = item.isNull("VL_TRAN") ? "0" : item.get("VL_TRAN").toString();
				String tP_CNTE = item.isNull("TP_CNTE") ? "" : item.get("TP_CNTE").toString();
				String dT_TRAN = item.isNull("DT_TRAN") ? "" : item.get("DT_TRAN").toString();
				String hR_TRAN = item.isNull("HR_TRAN") ? "" : item.get("HR_TRAN").toString();
				String dT_OCOR_FIXA = item.isNull("DT_OCOR_FIXA") ? "" : item.get("DT_OCOR_FIXA").toString();
				String dT_OCOR_PERI_INIC = item.isNull("DT_OCOR_PERI_INIC") ? "" : item.get("DT_OCOR_PERI_INIC").toString();
				String dT_OCOR_PERI_FINA = item.isNull("DT_OCOR_PERI_FINA") ? "" : item.get("DT_OCOR_PERI_FINA").toString();
				String cARD = item.isNull("CARD") ? "" : item.get("CARD").toString();
				String nR_SEQU_TRAN_FRAU_CNAL = item.isNull("NR_SEQU_TRAN_FRAU_CNAL") ? "" : item.get("NR_SEQU_TRAN_FRAU_CNAL").toString();
				String dESC_TRAN = item.isNull("DESC_TRAN") ? "" : item.get("DESC_TRAN").toString();
				String dESC_CNTE = item.isNull("DESC_CNTE") ? "" : item.get("DESC_CNTE").toString();
				
				String strDtOper;
				String strHrOper;
				String strVlOper;
				
				
				if (item.isNull("VL_TRAN")) {
					this.intValAux = 0;
					strVlOper = "";
				} else {
					this.intValAux = Double.parseDouble(item.get("VL_TRAN").toString().replace(",", "."));
					strVlOper = formataValorRecebido(item.get("VL_TRAN").toString());
				}
				
				if (tP_CNTE.equals("1")) {
					this.intQntLancDbto += 1;
				} else if (tP_CNTE.equals("2")) {
					this.intQntLancCrdt += 1;
				}
				
				if (dT_TRAN.equals("01/01/1900")) {
					strDtOper = "";
				} else {
					strDtOper = dT_TRAN;
				}
				
				if (hR_TRAN.equals("00:00")) {
					strHrOper = "";
				} else {
					strHrOper = hR_TRAN;
				}
				
				if( !item.isNull("DT_TRAN") && strDtOper.equals("") && !dT_OCOR_FIXA.equals("")) {
					strDtOper = dT_OCOR_FIXA;
				}
				
				CadastroOsSLinhaObjCanais modelCanais = new CadastroOsSLinhaObjCanais(nR_SEQU_TIPO_OPER_TRAN, cD_TRAN, nM_OPER, nM_SUB_OPER, cD_PROD_AUXI, nM_PROD_AUXI, vL_TRAN, tP_CNTE, dT_TRAN, hR_TRAN, dT_OCOR_FIXA, dT_OCOR_PERI_INIC, dT_OCOR_PERI_FINA, cARD, nR_SEQU_TRAN_FRAU_CNAL, dESC_TRAN, dESC_CNTE, strDtOper, strHrOper, strVlOper);
				objCanais.add(modelCanais);
				
				this.strDtPeriFixa = dT_OCOR_FIXA;
				this.strDtPeriIni = dT_OCOR_PERI_INIC;
				this.strDtPeriFim = dT_OCOR_PERI_FINA;
			}
			
			atualizarValores();
			
		} catch (JSONException | com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsCanais.WebServiceException e) {
			e.printStackTrace();
		}
	}

	
	public String formataValorRecebido(String valorRecebido) {
		String valor = NumberFormat.getCurrencyInstance(ptBR).format(Double.parseDouble(valorRecebido.replace(",", ".")));
		return valor.replace("R$", "").trim();
	}
	
	
	public void insereContasAcessadas() {
		CadastroOsSLinhaIBanking modelBank = new CadastroOsSLinhaIBanking("", "");
		iBanking.add(modelBank);
	}
	
	public void getConfirmarCentros() {
		//String confirmOrig = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("centroConfirm").toString();
		//System.out.println("test: " + confirmOrig);
//		return Boolean.parseBoolean(confirmOrig);
		//RequestContext.getCurrentInstance().execute("confirmarCentroModal("+confirmOrig+");");
	}
	
	public void adicionarProduto(String tipoProduto) {
		//1-Corp 	2-Aux
				
		if(tipoProduto.isEmpty()) {
			tipoProduto = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("tipoProduto").toString();
		}
		
		String intTpContestacao = "";
		String strTipoContestacao = "";
		
		String strDescOperacao;
		
		if(tipoProduto.equals("1") && this.selectedOperCorp.isEmpty()){
			RequestContext.getCurrentInstance().execute("alert('Selecione uma Operação')");
		} else if (tipoProduto.equals("2") && this.selectedOperAux.isEmpty()) {
			//alert
			RequestContext.getCurrentInstance().execute("alert('Selecione uma Operação')");
		} else {
			
			try {
				
				if(tipoProduto.equals("1")) {					
					
					strDescOperacao = this.objRsOperCorp
											.stream()
											.filter(item -> item.getNR_SEQU_TIPO_OPER_TRAN().equals(this.selectedOperCorp))
											.collect(Collectors.toList())
											.get(0).getLabel().substring(7);
					
					System.out.println("DESCOPERACAO ->" + strDescOperacao);
				} else {					
					strDescOperacao = this.objRsOperAux
											.stream()
											.filter(item -> item.getCD_PROD_AUXI().equals(this.selectedOperAux))
											.collect(Collectors.toList())
											.get(0).getNM_PROD_AUXI();
				}

				// Checked
				if (this.isCredito) {
					intTpContestacao = "2"; // Credito
					strTipoContestacao = "Crédito";
				} else {
					strTipoContestacao = "Débito";
					intTpContestacao = "1"; // Debito
				}
				
				
				CadastroOsSLinhaObjCanais canaisModel = new CadastroOsSLinhaObjCanais();
				
				JSONObject json = new JSONObject();
				System.out.println(strNrSeqDetalhe +","+ this.selectedOperCorp+","+ this.selectedOperAux+","+ "1"+","+ intTpContestacao+","+ this.objRsEvento.getStrCdCentroOrigem()+","+ this.objRsEvento.getStrCdCentroDestino()+","+ this.objRsEvento.getStrCdCentroOperante()+","+ this.objRsEvento.getStrNrQntdEven());
				
				if(tipoProduto.equals("1")) {
					json = new JSONObject(cadOsCanais.inserirOperacaoRetSeqTranFrau(strNrSeqDetalhe, this.selectedOperCorp, "", "1", intTpContestacao, this.objRsEvento.getStrCdCentroOrigem(), this.objRsEvento.getStrCdCentroDestino() , this.objRsEvento.getStrCdCentroOperante(), this.objRsEvento.getStrNrQntdEven()));
				}else {
					json = new JSONObject(cadOsCanais.inserirOperacaoRetSeqTranFrau(strNrSeqDetalhe, "", this.selectedOperAux, "2", intTpContestacao, this.objRsEvento.getStrCdCentroOrigem(), this.objRsEvento.getStrCdCentroDestino() , this.objRsEvento.getStrCdCentroOperante(), this.objRsEvento.getStrNrQntdEven()));
				}
				
				JSONArray pcursor = json.getJSONArray("PCURSOR");
				JSONObject item = pcursor.getJSONObject(0);
				
				if (this.selectRdoContestacao.equals("1")) {
					this.intQntLancDbto += 1;
				} else if (this.selectRdoContestacao.equals("2")) {
					this.intQntLancCrdt += 1;
				}
				
				canaisModel.setNR_SEQU_TRAN_FRAU_CNAL(item.get("NR_SEQU_TRAN_FRAU_CNAL").toString());
				canaisModel.setDESC_TRAN(strDescOperacao);
				canaisModel.setDESC_CNTE(strTipoContestacao);
				canaisModel.setTP_CNTE(intTpContestacao);
				
				objCanais.add(canaisModel);
				this.atualizarValores();
				System.out.println("CLEAR RENDER IDS AQUI");
				FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("frmEventoDetalheIBanking:tbInserirOp");
				
			} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsCanais.WebServiceException  e) {
				e.printStackTrace();
				try {
					FacesContext.getCurrentInstance().getExternalContext()
							.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}		
		
	}
	
	public void excluirOperacao() {
		String nrSeqFrauCanal = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqTranFrau");
		int index = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("index"));
		String descCnte = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("desc_cnte").toString();
				
		try {
			cadOsCanais.excluirOperacao(nrSeqFrauCanal);
			
			if (descCnte.equals("Débito") && this.intQntLancDbto >= 0) {
				this.intQntLancDbto -= 1;
			} else if (descCnte.equals("Crédito") && this.intQntLancCrdt >= 0) {
				this.intQntLancCrdt -= 1;
			}
			
			//objCanais.clear();
			objCanais.remove(index);
			this.atualizarValores();
		} catch (com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsCanais.WebServiceException e) {
			e.printStackTrace();
		}
	}
	
	public void carregaComboOper(Integer intContestacao) {
		this.objRsOperCorp = new ArrayList<OperCorpModel>();
		try {
			JSONObject json = new JSONObject();
			json = new JSONObject(cadOsCanais.consultarOperacao(this.strNrCanal, intContestacao));
			//NM_OPER
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			for (int i = 0; i < pcursor.length(); i++) 
				this.objRsOperCorp.add(
						new OperCorpModel(pcursor.getJSONObject(i).isNull("NR_SEQU_TIPO_OPER_TRAN") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("NR_SEQU_TIPO_OPER_TRAN")),
									  pcursor.getJSONObject(i).isNull("CD_TRAN") ? "" : pcursor.getJSONObject(i).getString("CD_TRAN"),
									  pcursor.getJSONObject(i).isNull("NM_SUB_OPER") ? "" : pcursor.getJSONObject(i).getString("NM_SUB_OPER"),
										pcursor.getJSONObject(i).isNull("NM_OPER") ? "" : pcursor.getJSONObject(i).getString("NM_OPER"))
						);

				
		} catch (Exception e) {
			e.printStackTrace();
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}			
	}
	
	public void carregaComboOperAux(Integer intContestacao) {
		this.objRsOperAux = new ArrayList<OperAuxModel>();
		try {
			JSONObject json = new JSONObject();
			json = new JSONObject(cadOsCanais.consultarOperacaoAux(this.strNrCanal, intContestacao));			
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			for (int i = 0; i < pcursor.length(); i++) 
				this.objRsOperAux.add(
						new OperAuxModel(pcursor.getJSONObject(i).isNull("CD_PROD_AUXI") ? "" : String.valueOf(pcursor.getJSONObject(i).getInt("CD_PROD_AUXI")),
									  pcursor.getJSONObject(i).isNull("NM_PROD_AUXI") ? "" : pcursor.getJSONObject(i).getString("NM_PROD_AUXI"))
						);

			System.out.println("objRsOperAux "+this.objRsOperAux.size());
				
		} catch (Exception e) {
			e.printStackTrace();
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}					
	}
	
	public void handleIsCredito(AjaxBehaviorEvent abe) {
		this.isCredito = !this.isCredito;
		if (this.isCredito) {
			carregaComboOper(2);
			carregaComboOperAux(2);
		} 
		else {
			carregaComboOper(1);
			carregaComboOperAux(1);
		}
	}
	
	public void atualizarValores() {
		double valDbt = 0;
		double valCrdt = 0;
		
		for (int i = 0; i < objCanais.size(); i++) {
			System.out.println("CNTE: " + objCanais.get(i).getTP_CNTE());
			if (objCanais.get(i).getTP_CNTE() != null) {
				if (objCanais.get(i).getTP_CNTE().equals("2")) {
					if (objCanais.get(i).getStrVlOper() == null || objCanais.get(i).getStrVlOper().equals("")) {
						valCrdt += 0;
					} else {
						valCrdt += Double.parseDouble(objCanais.get(i).getStrVlOper().toString().replace(".", "").replace(",", "."));
					}
					
				} else {
					if (objCanais.get(i).getStrVlOper() == null || objCanais.get(i).getStrVlOper().equals("")) {
						valDbt += 0;
					} else {
						valDbt += Double.parseDouble(objCanais.get(i).getStrVlOper().toString().replace(".", "").replace(",", "."));
					}
				}
			}
		}
		
		this.intQntValCrdt = valCrdt;
		this.intQntValDbto = valDbt;
	}
	
	public void clearBean() {
		String type = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("pageType").toString();
		String nrSeq = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("nrSeq").toString();
		switch(type) {
			case "FE":
				System.out.println("nrSeq - "+nrSeq+" - FE "+FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("CadastroOsDetalheProdutoFEBean"));
				if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("CadastroOsDetalheProdutoFEBean") != null)
					FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CadastroOsDetalheProdutoFEBean");
				RequestContext.getCurrentInstance().execute("document.getElementById('modal-detalhe-produto-fo-fe').src = 'hyb_cadastroos_detalheprodutofe.xhtml?strNrSeqTranFrau="+nrSeq+"'");
				RequestContext.getCurrentInstance().execute("PF('detalhe-produto-fo-fe').show();");
				break;
			
			case "FO":
				System.out.println("nrSeq - "+nrSeq+" - FO "+FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("CadastroOsDetalheProdutoFOBean"));
				if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("CadastroOsDetalheProdutoFOBean") != null)
					FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CadastroOsDetalheProdutoFOBean");
				RequestContext.getCurrentInstance().execute("document.getElementById('modal-detalhe-produto-fo-fe').src = 'hyb_cadastroos_detalheprodutofo.xhtml?strNrSeqTranFrau="+nrSeq+"'");
				RequestContext.getCurrentInstance().execute("PF('detalhe-produto-fo-fe').show();");
				break;
			
			default:
				System.out.println("clearBean - não encontrado");
				break;
		}
	}
	
	
	public String getCD_CANAL() {
		return CD_CANAL;
	}

	public void setCD_CANAL(String cD_CANAL) {
		CD_CANAL = cD_CANAL;
	}

	public String getstrNrSeqDetalhe() {
		return strNrSeqDetalhe;
	}

	public void setstrNrSeqDetalhe(String strNrSeqDetalhe) {
		this.strNrSeqDetalhe = strNrSeqDetalhe;
	}


	public boolean isBlnSalvar() {
		return blnSalvar;
	}

	public void setBlnSalvar(boolean blnSalvar) {
		this.blnSalvar = blnSalvar;
	}

	public String getStrErro() {
		return strErro;
	}

	public void setStrErro(String strErro) {
		this.strErro = strErro;
	}

	public String getStrQtdeOcor() {
		return strQtdeOcor;
	}

	public void setStrQtdeOcor(String strQtdeOcor) {
		this.strQtdeOcor = strQtdeOcor;
	}

	public CadastroOsIbanking getObjRsEvento() {
		return objRsEvento;
	}

	public void setObjRsEvento(CadastroOsIbanking objRsEvento) {
		this.objRsEvento = objRsEvento;
	}

	public List<CadastroOsSLinhaIBanking> getiBanking() {
		return iBanking;
	}

	public void setiBanking(List<CadastroOsSLinhaIBanking> iBanking) {
		this.iBanking = iBanking;
	}

	public boolean isShowDatas() {
		return showDatas;
	}

	public void setShowDatas(boolean showDatas) {
		this.showDatas = showDatas;
	}

	public String getStrNrSeqDetalhe() {
		return strNrSeqDetalhe;
	}

	public void setStrNrSeqDetalhe(String strNrSeqDetalhe) {
		this.strNrSeqDetalhe = strNrSeqDetalhe;
	}

	public boolean isOrigExists() {
		return origExists;
	}

	public void setOrigExists(boolean origExists) {
		this.origExists = origExists;
	}

	public boolean isDestExists() {
		return destExists;
	}

	public void setDestExists(boolean destExists) {
		this.destExists = destExists;
	}

	public boolean isCredito() {
		return isCredito;
	}

	public void setCredito(boolean isCredito) {
		this.isCredito = isCredito;
	}

	public double getIntQntValDbto() {
		return intQntValDbto;
	}

	public void setIntQntValDbto(double intQntValDbto) {
		this.intQntValDbto = intQntValDbto;
	}

	public double getIntQntValCrdt() {
		return intQntValCrdt;
	}

	public void setIntQntValCrdt(double intQntValCrdt) {
		this.intQntValCrdt = intQntValCrdt;
	}

	public int getIntQntLancDbto() {
		return intQntLancDbto;
	}

	public void setIntQntLancDbto(int intQntLancDbto) {
		this.intQntLancDbto = intQntLancDbto;
	}

	public int getIbtQntLancCrdt() {
		return ibtQntLancCrdt;
	}

	public void setIbtQntLancCrdt(int ibtQntLancCrdt) {
		this.ibtQntLancCrdt = ibtQntLancCrdt;
	}

	public int getIntQntLancCrdt() {
		return intQntLancCrdt;
	}

	public void setIntQntLancCrdt(int intQntLancCrdt) {
		this.intQntLancCrdt = intQntLancCrdt;
	}

	public double getIntValAux() {
		return intValAux;
	}

	public void setIntValAux(double intValAux) {
		this.intValAux = intValAux;
	}

	public String getStrNrCanal() {
		return strNrCanal;
	}

	public void setStrNrCanal(String strNrCanal) {
		this.strNrCanal = strNrCanal;
	}

	public String getStrNrEvento() {
		return strNrEvento;
	}

	public void setStrNrEvento(String strNrEvento) {
		this.strNrEvento = strNrEvento;
	}

	public String getSelectedOperCorp() {
		return selectedOperCorp;
	}

	public void setSelectedOperCorp(String selectedOperCorp) {
		this.selectedOperCorp = selectedOperCorp;
	}

	public String getSelectRdoContestacao() {
		return selectRdoContestacao;
	}

	public void setSelectRdoContestacao(String selectRdoContestacao) {
		this.selectRdoContestacao = selectRdoContestacao;
	}

	public List<OperCorpModel> getObjRsOperCorp() {
		return objRsOperCorp;
	}

	public void setObjRsOperCorp(List<OperCorpModel> objRsOperCorp) {
		this.objRsOperCorp = objRsOperCorp;
	}

	public List<CadastroOsSLinhaObjCanais> getObjCanais() {
		return objCanais;
	}

	public void setObjCanais(List<CadastroOsSLinhaObjCanais> objCanais) {
		this.objCanais = objCanais;
	}

	public XHYCadOsEventosEndPoint getObjEvento() {
		return objEvento;
	}

	public void setObjEvento(XHYCadOsEventosEndPoint objEvento) {
		this.objEvento = objEvento;
	}

	public List<OperAuxModel> getObjRsOperAux() {
		return objRsOperAux;
	}

	public void setObjRsOperAux(List<OperAuxModel> objRsOperAux) {
		this.objRsOperAux = objRsOperAux;
	}

	public String getSelectedOperAux() {
		return selectedOperAux;
	}

	public void setSelectedOperAux(String selectedOperAux) {
		this.selectedOperAux = selectedOperAux;
	}
	public String getStrDtPeriFixa() {
		return strDtPeriFixa;
	}
	public void setStrDtPeriFixa(String strDtPeriFixa) {
		this.strDtPeriFixa = strDtPeriFixa;
	}
	public String getStrDtPeriIni() {
		return strDtPeriIni;
	}
	public void setStrDtPeriIni(String strDtPeriIni) {
		this.strDtPeriIni = strDtPeriIni;
	}
	public String getStrDtPeriFim() {
		return strDtPeriFim;
	}
	public void setStrDtPeriFim(String strDtPeriFim) {
		this.strDtPeriFim = strDtPeriFim;
	}
	
	
	
	
	
}
