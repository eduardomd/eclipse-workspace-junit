package com.altec.bsbr.app.hyb.web.jsf;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsInfGerais.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsInfGerais.XHYCadOsInfGeraisEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYTreinamento.XHYTreinamentoEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;

@Component("uploadprocessa")
@Scope("session")
public class Hyb_uploadprocessa_bean extends BasicBBean {

	private static final long serialVersionUID = 1L;

	private UploadedFile arquivoUpload;

	// Caminho servidor
	private String strCaminhoBasico;

	private String strCaminhoMudado;
	private String strNomeArquivo;

	/* Query String */
	private String strTpAnexo;
	private String strNrSeqOs;
	private String strSeqTrei;

	private String dir = System.getProperty("user.dir");

	@PostConstruct
	public void init() {
		this.strNrSeqOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
				.get("qstrSeqOs") == null ? ""
						: FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
								.get("qstrSeqOs").toString();
		this.strTpAnexo = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
				.get("qstrTpAnexo") == null ? ""
						: FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
								.get("qstrTpAnexo").toString();
		this.strSeqTrei = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
				.get("qstrSeqTrei") == null ? ""
						: FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
								.get("qstrSeqTrei").toString();

		System.out.println("File: " + dir + this.strNrSeqOs);

		if (strTpAnexo.equals("I")) {
			File file = new File(dir + this.strNrSeqOs);
			if (!file.exists()) {
				file.mkdir();
			}
		} else {
			File file = new File(dir + this.strSeqTrei);
			if (!file.exists()) {
				file.mkdir();
			}
		}

	}

	@Autowired
	private XHYCadOsInfGeraisEndPoint infGerais;

	@Autowired
	private XHYTreinamentoEndPoint objTreinamento;

	
	private void carregarParametros() {
		
	}
	
	public void formSubmit(FileUploadEvent event) {

		System.out.println(this.strTpAnexo + " - " + this.strNrSeqOs + " - " + this.strSeqTrei);

		this.arquivoUpload = event.getFile();

		try {
			System.out.println("ANEXO: " + strTpAnexo);
			System.out.println("FILE: " + arquivoUpload.getFileName());

			if (arquivoUpload != null) {

				if (strTpAnexo.equals("I")) {
					this.strCaminhoBasico = dir + this.strNrSeqOs;
					this.strCaminhoMudado = strCaminhoBasico + strNrSeqOs + this.strNrSeqOs;
				} else {
					// URL de Treinamento
					this.strCaminhoBasico = dir;
					this.strCaminhoMudado = strCaminhoBasico + strSeqTrei;
				}

				this.strNomeArquivo = arquivoUpload.getFileName();

				// Create DIrectory
				// URL_server/file_name

				File fileBasico = new File(strCaminhoBasico, this.strNomeArquivo);
				System.out.println("PATH: " + fileBasico.getPath());
				System.out.println("FILE NAME: " + this.strNomeArquivo);

				OutputStream out = new FileOutputStream(fileBasico);
				out.write(arquivoUpload.getContents());
				out.close();

				// Criar pasta Seguranca
				// File fileMudado = new File(strCaminhoMudado);

				System.out.println("Anexo: " + this.strTpAnexo);

				if (this.strTpAnexo.equals("T")) {
					objTreinamento.inserirAnexo(strSeqTrei, strNomeArquivo);
				} else {
					try {
						infGerais.incluirArqAnex(this.strNrSeqOs.toString(), this.strNomeArquivo.toString(),
								this.strTpAnexo.toString());
					} catch (WebServiceException e) {
						e.printStackTrace();
					}
				}

				System.out.println("O arquivo '" + this.strNomeArquivo + "' foi salvo com sucesso no path '"
						+ strCaminhoBasico + "'"); // apenas para debug

				RequestContext.getCurrentInstance().execute("alert('O arquivo foi salvo com sucesso!')");
			}
		} catch (Exception e) {
			System.out.println("Aconteceu o seguinte erro no Upload de arquivos: " + e.getMessage());
		}
	}

	public UploadedFile getArquivoUpload() {
		return arquivoUpload;
	}

	public void setArquivoUpload(UploadedFile arquivoUpload) {
		this.arquivoUpload = arquivoUpload;
	}

	public String getStrCaminhoBasico() {
		return strCaminhoBasico;
	}

	public void setStrCaminhoBasico(String strCaminhoBasico) {
		this.strCaminhoBasico = strCaminhoBasico;
	}

	public String getStrCaminhoMudado() {
		return strCaminhoMudado;
	}

	public void setStrCaminhoMudado(String strCaminhoMudado) {
		this.strCaminhoMudado = strCaminhoMudado;
	}

	public String getStrNomeArquivo() {
		return strNomeArquivo;
	}

	public void setStrNomeArquivo(String strNomeArquivo) {
		this.strNomeArquivo = strNomeArquivo;
	}

	public String getStrTpAnexo() {
		return strTpAnexo;
	}

	public void setStrTpAnexo(String strTpAnexo) {
		this.strTpAnexo = strTpAnexo;
	}

	public String getStrNrSeqOs() {
		return strNrSeqOs;
	}

	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}

	public String getStrSeqTrei() {
		return strSeqTrei;
	}

	public void setStrSeqTrei(String strSeqTrei) {
		this.strSeqTrei = strSeqTrei;
	}

	public XHYCadOsInfGeraisEndPoint getInfGerais() {
		return infGerais;
	}

	public void setInfGerais(XHYCadOsInfGeraisEndPoint infGerais) {
		this.infGerais = infGerais;
	}

}
