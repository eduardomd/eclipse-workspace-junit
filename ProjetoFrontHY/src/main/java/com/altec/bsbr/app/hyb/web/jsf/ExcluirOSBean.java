package com.altec.bsbr.app.hyb.web.jsf;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;

import org.springframework.beans.factory.annotation.Autowired;

import com.altec.bsbr.app.hyb.web.util.XHYUsuarioIncService;

@ManagedBean(name="excluirOSBean")
@ViewScoped
public class ExcluirOSBean {

	private Object strNrSeqOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs");

	@Autowired 
	private XHYUsuarioIncService userServ;
	
	public void verificarPermissao(ComponentSystemEvent event) {
		String[] perfis = {"SUPERINT","GER"};
		
		userServ.verificarPermissao(perfis);
	}
	
	
	public Object getStrNrSeqOs() {
		return strNrSeqOs;
	}
	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}	
	
}
