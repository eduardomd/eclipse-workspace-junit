package com.altec.bsbr.app.hyb.web.jsf;

import java.io.InputStream;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

@ManagedBean(name="DownloadBean")
@ViewScoped
public class DownloadBean implements Serializable {
     
	private static final long serialVersionUID = 1L;
	
	private StreamedContent arquivo;
	
	/* final private String PATH_ARQUIVO_DOWNLOAD = "\\\\centdados4\\hyserver\\treinamento\\29712_temp\\ocesp_relatorio.xls"; */ 
	final private String PATH_ARQUIVO_DOWNLOAD = "/resources/images/documento.gif";
	final private String NOME_ARQUIVO_DOWNLOAD = "download_documento_teste.gif";
	
	public DownloadBean() {
			
        InputStream stream = FacesContext.getCurrentInstance()
        								 .getExternalContext()
        								 .getResourceAsStream(PATH_ARQUIVO_DOWNLOAD);
        
        setArquivo(new DefaultStreamedContent(stream, "image/gif", NOME_ARQUIVO_DOWNLOAD));
	}

	public StreamedContent getArquivo() {
		return arquivo;
	}

	public void setArquivo(StreamedContent arquivo) {
		this.arquivo = arquivo;
	}

}