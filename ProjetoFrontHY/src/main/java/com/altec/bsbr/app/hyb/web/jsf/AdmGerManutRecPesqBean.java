package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;

import java.util.ArrayList;
import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.altec.bsbr.fw.web.jsf.BasicBBean;

import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmGerRecurso.XHYAdmGerRecursoEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmLog.XHYAdmLogEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmGerRecurso.WebServiceException;
import com.altec.bsbr.app.hyb.dto.AdmGerManutRecPesqModel;
//import com.altec.bsbr.app.hyb.dto.ObjRsRecurso;
//import com.altec.bsbr.app.hyb.dto.objRsArea;
import com.altec.bsbr.app.hyb.dto.UsuarioIncModel;
import com.altec.bsbr.app.hyb.web.util.LogIncUtil;
import com.altec.bsbr.app.hyb.web.util.XHYUsuarioIncService;

@Component("admGerManutRecPesqBean")
@Scope("session")
public class AdmGerManutRecPesqBean extends BasicBBean {
	@Autowired
	private XHYAdmGerRecursoEndPoint admGerRecurso;
	
	@Autowired
	private XHYUsuarioIncService usuarioService;

	private UsuarioIncModel usuario;
	
	@Autowired
	private XHYAdmLogEndPoint log;

	private static final long serialVersionUID = 1L;

	// final Boolean MOCK = true;

	private List<AdmGerManutRecPesqModel> objRsRecurso;
	private LinkedHashMap<String, String> cboArea;

	private AdmGerManutRecPesqModel admGerManutRecPesq;

	private AdmGerManutRecPesqModel radioSelected;

	private String txtCpf;
	private String txtNome;
	private String cboAreaSelecionada;


	@PostConstruct
	public void init() {
		cleanSession();
		instanciarUsuario();
		CarregaCombo();

	}

	public void CarregaCombo() {

		cboArea = new LinkedHashMap<>();

		try {
			String retornoArea = admGerRecurso.consultarArea();

			JSONObject consultaArea = new JSONObject(retornoArea);
			JSONArray pCursorArea = consultaArea.getJSONArray("PCURSOR");

			for (int i = 0; i < pCursorArea.length(); i++) {
				JSONObject f = pCursorArea.getJSONObject(i);

				cboArea.put(f.get("CODIGO").toString(), f.get("NOME").toString());

			} // close for

		} catch (Exception e) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
         } catch (IOException e1) {
               // TODO Auto-generated catch block
               e1.printStackTrace();
         }
  }

	}
	
	public void instanciarUsuario() {
		try {
			usuario = usuarioService.getDadosUsuario();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void AdmGerManutRecPesq() {
		objRsRecurso = new ArrayList<AdmGerManutRecPesqModel>();

		String retorno = "";
		try {
			retorno = admGerRecurso.consultarRecurso(this.getTxtCpf(), this.getTxtNome(), this.getCboAreaSelecionada());

			JSONObject consultaRecurso = new JSONObject(retorno);
			JSONArray pcursor = consultaRecurso.getJSONArray("PCURSOR");

			for (int i = 0; i < pcursor.length(); i++) {

				JSONObject f = pcursor.getJSONObject(i);

				admGerManutRecPesq = new AdmGerManutRecPesqModel();

				admGerManutRecPesq.setCdbCpf(f.isNull("CPF") == true ? "" : f.get("CPF").toString());
				admGerManutRecPesq.setCdbNome(f.isNull("NOME") == true ? "" : f.get("NOME").toString());
				admGerManutRecPesq.setCdbMatricula(f.isNull("MATRICULA") == true ? "" : f.get("MATRICULA").toString());
				admGerManutRecPesq.setCdbCargo(f.isNull("CARGO") == true ? "" : f.get("CARGO").toString());
				admGerManutRecPesq
						.setCdbTipoRecurso(f.isNull("TIPO_RECURSO") == true ? "" : f.get("TIPO_RECURSO").toString());
				admGerManutRecPesq.setCdbArea(f.isNull("AREA") == true ? "" : f.get("AREA").toString());
				admGerManutRecPesq.setCdbExclusao(f.isNull("EXCLUSAO") == true ? "" : f.get("EXCLUSAO").toString());

				objRsRecurso.add(admGerManutRecPesq);

			} // close for


		} catch (Exception e) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
         } catch (IOException e1) {
               // TODO Auto-generated catch block
               e1.printStackTrace();
         }
  }
		this.setTxtCpf("");
		this.setTxtNome("");

	}

	public void alterarRecurso() throws IOException {
		if(this.getRadioSelected() == null) {
			RequestContext.getCurrentInstance().execute("alert('Selecione um recurso!');");			
		}else {
			ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
			ec.redirect("hyb_adm_ger_manut_rec_cad.xhtml?strCpf=" + getRadioSelected().getCdbCpf().replaceAll("[^0-9]", ""));
		}
	}
	
	public void excluirRecurso() {
		
		
		String recursoExcluir = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("excluirRecurso").toString();
		
		String retorno = "";
		try {
			admGerRecurso.apagarRecurso(recursoExcluir.replaceAll("[^0-9]", ""));
			this.objRsRecurso.clear();
			AdmGerManutRecPesq();
			log.incluirAcaoLog("", Integer.toString(LogIncUtil.DELETAR_RECURSO), usuario.getCpfUsuario(), "");
			RequestContext.getCurrentInstance().execute("alert('Recurso excluído com sucesso!');");
		} catch (Exception e) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
         } catch (IOException e1) {
               // TODO Auto-generated catch block
               e1.printStackTrace();
         }
  }


	}
	
	
    public void cleanSession() {
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getSessionMap().remove("admGerManutRecPesqBean");
  }


	public List<AdmGerManutRecPesqModel> getObjRsRecurso() {
		return objRsRecurso;
	}

	public void setObjRsRecurso(List<AdmGerManutRecPesqModel> objRsRecurso) {
		this.objRsRecurso = objRsRecurso;
	}

	public LinkedHashMap<String, String> getCboArea() {
		return cboArea;
	}

	public void setCboArea(LinkedHashMap<String, String> cboArea) {
		this.cboArea = cboArea;
	}

	public String getTxtCpf() {
		return txtCpf;
	}

	public void setTxtCpf(String txtCpf) {
		this.txtCpf = txtCpf.replaceAll("[^0-9]", "");
	}

	public String getTxtNome() {
		return txtNome;
	}

	public void setTxtNome(String txtNome) {
		this.txtNome = txtNome;
	}

	public String getCboAreaSelecionada() {
		return cboAreaSelecionada;
	}

	public void setCboAreaSelecionada(String cboAreaSelecionada) {
		this.cboAreaSelecionada = cboAreaSelecionada.split("=")[0];
	}

	public AdmGerManutRecPesqModel getRadioSelected() {
		return radioSelected;
	}

	public void setRadioSelected(AdmGerManutRecPesqModel radioSelected) {
		this.radioSelected = radioSelected;
	}

}