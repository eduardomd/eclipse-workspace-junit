package com.altec.bsbr.app.hyb.dto;

public class EncerrarOsParticipantesModel {

	private int CODIGO;
	private int POSICAO;
	private String NOME;
	private int TIPO_PARTICIP;
	private String CPF_CNPJ;
	private String DESC_TIPO;
	private String CD_PENALIDADE;
	private String CD_MOTIVO;
	
	public EncerrarOsParticipantesModel(int CODIGO, int POSICAO, String NOME, int TIPO_PARTICIP, String CPF_CNPJ, String DESC_TIPO, String CD_PENALIDADE, String CD_MOTIVO) {
		this.CODIGO = CODIGO;
		this.POSICAO = POSICAO;
		this.NOME = NOME;
		this.TIPO_PARTICIP = TIPO_PARTICIP;
		this.CPF_CNPJ = CPF_CNPJ;
		this.DESC_TIPO = DESC_TIPO;
		this.CD_PENALIDADE = CD_PENALIDADE;
		this.CD_MOTIVO = CD_MOTIVO;
	}
	
	public EncerrarOsParticipantesModel() {}

	public int getCODIGO() {
		return CODIGO;
	}

	public void setCODIGO(int CODIGO) {
		this.CODIGO = CODIGO;
	}

	public int getPOSICAO() {
		return POSICAO;
	}

	public void setPOSICAO(int POSICAO) {
		this.POSICAO = POSICAO;
	}

	public String getNOME() {
		return NOME;
	}

	public void setNOME(String NOME) {
		this.NOME = NOME;
	}

	public int getTIPO_PARTICIP() {
		return TIPO_PARTICIP;
	}

	public void setTIPO_PARTICIP(int TIPO_PARTICIP) {
		this.TIPO_PARTICIP = TIPO_PARTICIP;
	}

	public String getCPF_CNPJ() {
		return CPF_CNPJ;
	}

	public void setCPF_CNPJ(String CPF_CNPJ) {
		this.CPF_CNPJ = CPF_CNPJ;
	}

	public String getDESC_TIPO() {
		return DESC_TIPO;
	}

	public void setDESC_TIPO(String CPF_CNPJ) {
		this.DESC_TIPO = CPF_CNPJ;
	}

	public String getCD_PENALIDADE() {
		return CD_PENALIDADE;
	}

	public void setCD_PENALIDADE(String CD_PENALIDADE) {
		this.CD_PENALIDADE = CD_PENALIDADE;
	}

	public String getCD_MOTIVO() {
		return CD_MOTIVO;
	}

	public void setCD_MOTIVO(String CD_MOTIVO) {
		this.CD_MOTIVO = CD_MOTIVO;
	};
	
	
}
