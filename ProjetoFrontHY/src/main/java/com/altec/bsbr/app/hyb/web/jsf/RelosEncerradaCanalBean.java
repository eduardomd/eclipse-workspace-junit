package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.altec.bsbr.fw.web.jsf.BasicBBean;

import com.altec.bsbr.app.hyb.dto.RelosEncerradaCanalModel;

@Component("relosEncerradaCanalBean")
@Scope("session")
public class RelosEncerradaCanalBean extends BasicBBean {
	
	private static final long serialVersionUID = 1L;
	
	private List<RelosEncerradaCanalModel> objRelat = new ArrayList<RelosEncerradaCanalModel>();
	private Object objRsRelat;
	private String strErro = "";
	
	private String strArea;
	private String strTpRel;
	private String strNmRelat;
	private String strDtIni;
	private String strDtFim;
	private int intCanal;
	// private int intMaxCols;
	private String strTitulo = "Dados Gerais OS";
	
	/*
	private String strResultOs;
	private String strResultEspecCartao;
	private String strResultEspecPOC;
	private String strResultCartaoSeg;
	private String strResultDeb;
	private String strResultCred;
	private String strResultExcl;
	private String strResultRecup;
	private String strResultContDeb;
	private String strResultContCred;
	*/
	
	@SuppressWarnings("unchecked")
	@PostConstruct
    public void init() {		
		Map<String, String> request = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
						
		// strArea = request.get("pArea");
	    // strTpRel = request.get("pTpRel");
	    // strNmRelat = request.get("pNmRel");
	    // strDtIni = request.get("pDtIni");
	    // strDtFim = request.get("pDtFim");
	    // intCanal = Integer.parseInt(request.get("pCdCanal"));
		
		
		// ---- mock
	    strArea = "�rea";
	    strTpRel = "Canais";
	    strNmRelat = "Superintend�ncia de Ocorr�ncias Especiais";
	    strDtIni = "14/01/2019";
	    strDtFim = "19/01/2019";
	    intCanal = 37;
		
		for(int c = 1; c <= 5; c++) {
			objRelat.add(new RelosEncerradaCanalModel(c, "11/01/2019", "12/01/2019", "13/01/2019", "agencia " + c, "C/C " + c, 
					"cliente " + c, "PF PJ " + c, (c * 3.23), (c * 3.9), "analista " + c, (c * 7.3), 250.3, "poupanca " + c, 
					"clienteAjudado " + c, "nomeAgencia " + c, "cartaoRecebido " + c, "cartaoRetido " + c, "interesse " + c, 
					"fragilizacao " + c, "horario " + c, c + 10, "descricaoAgencia " + c, "01/01/2019", c + 30,
					"possuiSensor " + c, "outros " + c, "nomeEstabelecimento " + c, "solicitacao " + c, "ativacao " + c,
					"cancelamento " + c, "indeferidas " + c, "clienteInformou " + c, "ressarcido " + c, "tipoProduto " + c,
					"operacoes " + c, "percentual " + c, (c * 2.1), (c + c), "ipCliente " + c, "ipFraude " + c, "telefoneCliente " + c, "telefoneFraude " + c));
		}		
		// ----
		
		// objRsRelat = objRelat.fnRelRiscoOper(strDtIni, strDtFim, strCodArea, strErro);
		
		/*
		if(intCanal == 37) {
			intMaxCols = 10;
		}
		else {
			intMaxCols = 11;
		}
		*/
	}

	public void VerificaErro(String strErro) {
		try {
			if(strErro != "") {
				FacesContext.getCurrentInstance().getExternalContext().redirect("hy_erro.xhtml?strErro=" + strErro);
			}
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public List<RelosEncerradaCanalModel> getObjRelat() {
		return objRelat;
	}

	public void setObjRelat(List<RelosEncerradaCanalModel> objRelat) {
		this.objRelat = objRelat;
	}

	public Object getObjRsRelat() {
		return objRsRelat;
	}

	public void setObjRsRelat(Object objRsRelat) {
		this.objRsRelat = objRsRelat;
	}

	public String getStrDtIni() {
		return strDtIni;
	}

	public void setStrDtIni(String strDtIni) {
		this.strDtIni = strDtIni;
	}

	public String getStrDtFim() {
		return strDtFim;
	}

	public void setStrDtFim(String strDtFim) {
		this.strDtFim = strDtFim;
	}

	public int getIntCanal() {
		return intCanal;
	}

	public void setIntCanal(int intCanal) {
		this.intCanal = intCanal;
	}

	public String getStrTitulo() {
		return strTitulo;
	}

	public void setStrTitulo(String strTitulo) {
		this.strTitulo = strTitulo;
	}

	public String getStrNmRelat() {
		return strNmRelat;
	}

	public void setStrNmRelat(String strNmRelat) {
		this.strNmRelat = strNmRelat;
	}
}

