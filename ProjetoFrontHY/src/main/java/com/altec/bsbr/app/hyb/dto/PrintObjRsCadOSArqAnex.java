package com.altec.bsbr.app.hyb.dto;

import java.io.File;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
@Component("objRsCadOSArqAnex")
@Scope("request")

public class PrintObjRsCadOSArqAnex {
	
	private String NM_CAMI_ARQU_ANEX;
	
	public PrintObjRsCadOSArqAnex() {}

	public String getNM_CAMI_ARQU_ANEX() {
		return NM_CAMI_ARQU_ANEX;
	}

	public void setNM_CAMI_ARQU_ANEX(String nM_CAMI_ARQU_ANEX) {
		NM_CAMI_ARQU_ANEX = nM_CAMI_ARQU_ANEX;
	}
	
	public String getNomeArquivo() {
		return new File(this.NM_CAMI_ARQU_ANEX).getName();
	}
	
	public String getCaminhoArquivo() {
		return new File(this.NM_CAMI_ARQU_ANEX).getPath();
	}
}