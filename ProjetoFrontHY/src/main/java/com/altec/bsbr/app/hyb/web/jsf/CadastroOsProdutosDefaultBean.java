package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.altec.bsbr.app.hyb.dto.CadastroOsDefaultModel;
import com.altec.bsbr.app.hyb.web.util.CadOsEventosUtil;
import com.altec.bsbr.app.hyb.web.util.VerificaErroUtil;

import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.altec.bsbr.fw.web.jsf.BasicBBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


@Component("CadastroOsProdutosDefaultBean")
@Scope("session")
public class CadastroOsProdutosDefaultBean extends BasicBBean {

	/*
	 * #include file="../Inc/HY_Usuario.inc" #include file="../Inc/Hy_Util.inc"
	 */

	private static final long serialVersionUID = 1L;
	private static final String empty = new String();
	private String qsCanal;
	private String qsEvento;
	private String NrSeqDetalhe;
	private ArrayList<CadastroOsDefaultModel> objRsEventoValida;
	private ArrayList<CadastroOsDefaultModel> objRsEvento;

	CadastroOsDefaultModel cadastroOsProdutosDefault = new CadastroOsDefaultModel();

	CadOsEventosUtil objEvento = new CadOsEventosUtil();

	public CadastroOsProdutosDefaultBean() throws IOException{
		
		// block to get params from url
		FacesContext fc = FacesContext.getCurrentInstance();
		@SuppressWarnings("unchecked")
		Map<String, String> params = fc.getExternalContext().getRequestParameterMap();

		qsCanal = params.get("qsCanal");
		qsEvento = params.get("qsEvento");
		NrSeqDetalhe = params.get("NrSeqDetalhe"); //número sequencial da Fraude Canal
		
		cadastroOsProdutosDefault.setCdCanal(qsCanal);
		cadastroOsProdutosDefault.setStrEvento(qsEvento);
		cadastroOsProdutosDefault.setStrNrSeqId(NrSeqDetalhe);
		
		objRsEventoValida = objEvento.ConsultarFraudeCanalDetalhe(cadastroOsProdutosDefault.getStrNrSeqId());

		if(!objRsEventoValida.get(0).getStrErro().equals(empty)) {
			objEvento = new CadOsEventosUtil();
			objRsEventoValida = null;
			
			System.out.println("Exibe mensagem de erro");
			return;
			//VerificaErroUtil.verificaErro("erro");
		}
		
		if(objRsEventoValida.isEmpty() == true) {
			System.out.println("empty");
			objEvento = new CadOsEventosUtil();
			objRsEventoValida = null;
		}else {
			System.out.println("mostrar dados");
			objRsEvento = objRsEventoValida;

			for (int i = 0; i < objRsEvento.size(); i++) {
			System.out.println("String número da OS: " + objRsEvento.get(0).getStrNrSeqOs());
			System.out.println("Nome do Titular: " + objRsEvento.get(0).getStrTitular());
			System.out.println("Nome do Titular: " + objRsEvento.get(0).getStrSeqConta());
			System.out.println("Número Sequencial do Cartão: " + objRsEvento.get(0).getStrSeqCartao());
			System.out.println("Código do Canal: " + objRsEvento.get(0).getCdCanal());
			System.out.println("Número Sequencial da Fraude Evento: " + objRsEvento.get(0).getStrSeqEvento());
			System.out.println("Número do CPF CNPJ: " + objRsEvento.get(0).getStrNrQntdEven());
			System.out.println("Centro de origem pCD_CTRO_ORIG: " + objRsEvento.get(0).getStrCdCentroOrigem());
			System.out.println("Centro de destino pCD_CTRO_DEST: " + objRsEvento.get(0).getStrCdCentroDestino());
			System.out.println("Centro operante pCD_CTRO_OPER: " + objRsEvento.get(0).getStrCdCentroOperante());
			System.out.println("Valida contablização: " + objRsEvento.get(0).getStrContabilizado());
			}
		}
	
		
	}

	public String getQsCanal() {
		return cadastroOsProdutosDefault.getCdCanal();
	}

	public String getQsEvento() {
		return cadastroOsProdutosDefault.getStrEvento();
	}

	public ArrayList<CadastroOsDefaultModel> getObjRsEvento() {
		return objRsEvento;
	}

	public void setObjRsEvento(ArrayList<CadastroOsDefaultModel> objRsEvento) {
		this.objRsEvento = objRsEvento;
	}

}
