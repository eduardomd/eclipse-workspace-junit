package com.altec.bsbr.app.hyb.dto;

public class PE61_PEM2340CAeaResponse {

	public PE61_PEM2340CAeaResponse() {}
	
	public PE61_PEM2340CAeaResponse(String dC_FORMATO, String pENUMDO, String pEEXPPO, String fECEXP) {
		super();
		DC_FORMATO = dC_FORMATO;
		PENUMDO = pENUMDO;
		PEEXPPO = pEEXPPO;
		FECEXP = fECEXP;
	}

	private String DC_FORMATO;

	private String PENUMDO;
	
	private String PEEXPPO;
	
	private String FECEXP;

	public String getDC_FORMATO() {
		return DC_FORMATO;
	}
	
	public void setDC_FORMATO(String dC_FORMATO) {
		DC_FORMATO = dC_FORMATO;
	}
	
	public String getPENUMDO() {
		return PENUMDO;
	}

	public void setPENUMDO(String pENUMDO) {
		PENUMDO = pENUMDO;
	}

	public String getPEEXPPO() {
		return PEEXPPO;
	}

	public void setPEEXPPO(String pEEXPPO) {
		PEEXPPO = pEEXPPO;
	}

	public String getFECEXP() {
		return FECEXP;
	}

	public void setFECEXP(String fECEXP) {
		FECEXP = fECEXP;
	}

}
