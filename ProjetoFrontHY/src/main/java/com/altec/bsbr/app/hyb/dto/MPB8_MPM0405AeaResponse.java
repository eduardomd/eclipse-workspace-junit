package com.altec.bsbr.app.hyb.dto;

public class MPB8_MPM0405AeaResponse {

	public MPB8_MPM0405AeaResponse() {

	}
	
	public MPB8_MPM0405AeaResponse(String dC_FORMATO, String cODENT, String cENTALT, String cUENTA, String fECALTA,
			String fACUSER, String fECBAJA) {
		super();
		DC_FORMATO = dC_FORMATO;
		CODENT = cODENT;
		CENTALT = cENTALT;
		CUENTA = cUENTA;
		FECALTA = fECALTA;
		FACUSER = fACUSER;
		FECBAJA = fECBAJA;
	}

	private String DC_FORMATO;

	private String CODENT;

	private String CENTALT;

	private String CUENTA;

	private String FECALTA;

	private String FACUSER;

	private String FECBAJA;
	
	public String getDC_FORMATO() {
		return DC_FORMATO;
	}
	
	public void setDC_FORMATO(String dC_FORMATO) {
		DC_FORMATO = dC_FORMATO;
	}

	public String getCODENT() {
		return CODENT;
	}

	public void setCODENT(String cODENT) {
		CODENT = cODENT;
	}

	public String getCENTALT() {
		return CENTALT;
	}

	public void setCENTALT(String cENTALT) {
		CENTALT = cENTALT;
	}

	public String getCUENTA() {
		return CUENTA;
	}

	public void setCUENTA(String cUENTA) {
		CUENTA = cUENTA;
	}

	public String getFECALTA() {
		return FECALTA;
	}

	public void setFECALTA(String fECALTA) {
		FECALTA = fECALTA;
	}

	public String getFACUSER() {
		return FACUSER;
	}

	public void setFACUSER(String fACUSER) {
		FACUSER = fACUSER;
	}

	public String getFECBAJA() {
		return FECBAJA;
	}

	public void setFECBAJA(String fECBAJA) {
		FECBAJA = fECBAJA;
	}
}
