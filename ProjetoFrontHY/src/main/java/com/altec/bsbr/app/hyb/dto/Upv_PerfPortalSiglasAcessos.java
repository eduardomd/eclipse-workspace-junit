package com.altec.bsbr.app.hyb.dto;

public class Upv_PerfPortalSiglasAcessos {

	public String strSiglaWindows;
	public String strStatus;
	public String strMatricula;
	public String strNome;
	
	public String strCargo;
	public String strPerfilAtual;
	public String strPerfilSugerido;
	public String strCbxPerfisPadrao;
	public String strTipoRec;
	

	public String getStrSiglaWindows() {
		return strSiglaWindows;
	}

	public void setStrSiglaWindows(String strSiglaWindow) {
		strSiglaWindows = strSiglaWindow;
	}	
	
	public String getStrStatus() {
		return strStatus;
	}

	public void setStrStatus(String strStatu) {
		strStatus = strStatu;
	}
	
	public String getStrMatricula() {
		return strMatricula;
	}

	public void setStrMatricula(String strMatricul) {
		strMatricula = strMatricul;
	}
	
	public String getStrNome() {
		return strNome;
	}

	public void setStrNome(String strNom) {
		strNome = strNom;
	}
	
	public String getStrCargo() {
		return strCargo;
	}

	public void setStrCargo(String strCarg) {
		strCargo = strCarg;
	}
	
	public String getStrPerfilAtual() {
		return strPerfilAtual;
	}

	public void setStrPerfilAtual(String strPerfilAtua) {
		strPerfilAtual = strPerfilAtua;
	}
	
	public String getStrPerfilSugerido() {
		return strPerfilSugerido;
	}

	public void setStrPerfilSugerido(String strPerfilSugerid) {
		strPerfilSugerido = strPerfilSugerid;
	}
	
	public String getStrCbxPerfisPadrao() {
		return strCbxPerfisPadrao;
	}

	public void setStrCbxPerfisPadrao(String strCbxPerfisPadra) {
		strCbxPerfisPadrao = strCbxPerfisPadra;
	}
	
	public String getStrTipoRec() {
		return strTipoRec;
	}

	public void setStrTipoRec(String strTipoRe) {
		strTipoRec = strTipoRe;
	}
}
