package com.altec.bsbr.app.hyb.dto;

public class CadastroOsDetalheEnvolvClie {

	private String os;
	private String matricula;
	private String nome;
	private String cpf_cnpj;
	
	private String rg;
	private String orgaoEmissor;
	private String dtEmissaoRg;
	private String profissao;
	private String renda;
	private String dtUltiAltrEnde;
	
	private String endereco;
	private String numero;
	private String complemento;
	private String bairro;
	private String cep;
	private String telefone;
	private String dddTelefone;
	private String numeroTelefone;
	private String celular;
	private String dddCelular;
	private String numeroCelular;
	
	private String dt_abertura;
	private String oper_vencer;
	private String oper_vencida;
	private String oper_creli;
	private String oper_preju;
	private String oper_passiva;
	private String reciprocidade;
	private String strTpPart;
	private String participacao;
	private int intRecCount;

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf_cnpj() {
		return cpf_cnpj;
	}

	public void setCpf_cnpj(String cpf_cnpj) {
		this.cpf_cnpj = cpf_cnpj;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getDt_abertura() {
		return dt_abertura;
	}

	public void setDt_abertura(String dt_abertura) {
		this.dt_abertura = dt_abertura;
	}

	public String getOper_vencer() {
		return oper_vencer;
	}

	public void setOper_vencer(String oper_vencer) {
		this.oper_vencer = oper_vencer;
	}

	public String getOper_vencida() {
		return oper_vencida;
	}

	public void setOper_vencida(String oper_vencida) {
		this.oper_vencida = oper_vencida;
	}

	public String getOper_creli() {
		return oper_creli;
	}

	public void setOper_creli(String oper_creli) {
		this.oper_creli = oper_creli;
	}

	public String getOper_preju() {
		return oper_preju;
	}

	public void setOper_preju(String oper_preju) {
		this.oper_preju = oper_preju;
	}

	public String getOper_passiva() {
		return oper_passiva;
	}

	public void setOper_passiva(String oper_passiva) {
		this.oper_passiva = oper_passiva;
	}

	public String getReciprocidade() {
		return reciprocidade;
	}

	public void setReciprocidade(String reciprocidade) {
		this.reciprocidade = reciprocidade;
	}

	public int getIntRecCount() {
		return intRecCount;
	}

	public void setIntRecCount(int intRecCount) {
		this.intRecCount = intRecCount;
	}
	
	public String VerificaErro(String error) {
		if (error.isEmpty() == false) {
			return "HY_Erro.asp?strErro=" + error;
		}
		return "";
	}

	public String getStrTpPart() {
		return strTpPart;
	}

	public void setStrTpPart(String strTpPart) {
		this.strTpPart = strTpPart;
	}

	public String getParticipacao() {
		return participacao;
	}

	public void setParticipacao(String participacao) {
		this.participacao = participacao;
	}

	public String getDddTelefone() {
		return dddTelefone;
	}

	public void setDddTelefone(String dddTelefone) {
		this.dddTelefone = dddTelefone;
	}

	public String getNumeroTelefone() {
		return numeroTelefone;
	}

	public void setNumeroTelefone(String numeroTelefone) {
		this.numeroTelefone = numeroTelefone;
	}

	public String getDddCelular() {
		return dddCelular;
	}

	public void setDddCelular(String dddCelular) {
		this.dddCelular = dddCelular;
	}

	public String getNumeroCelular() {
		return numeroCelular;
	}

	public void setNumeroCelular(String numeroCelular) {
		this.numeroCelular = numeroCelular;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getOrgaoEmissor() {
		return orgaoEmissor;
	}

	public void setOrgaoEmissor(String orgaoEmissor) {
		this.orgaoEmissor = orgaoEmissor;
	}

	public String getDtEmissaoRg() {
		return dtEmissaoRg;
	}

	public void setDtEmissaoRg(String dtEmissaoRg) {
		this.dtEmissaoRg = dtEmissaoRg;
	}

	public String getProfissao() {
		return profissao;
	}

	public void setProfissao(String profissao) {
		this.profissao = profissao;
	}

	public String getRenda() {
		return renda;
	}

	public void setRenda(String renda) {
		this.renda = renda;
	}

	public String getDtUltiAltrEnde() {
		return dtUltiAltrEnde;
	}

	public void setDtUltiAltrEnde(String dtUltiAltrEnde) {
		this.dtUltiAltrEnde = dtUltiAltrEnde;
	}
}
