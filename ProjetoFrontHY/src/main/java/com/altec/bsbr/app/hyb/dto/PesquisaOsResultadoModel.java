package com.altec.bsbr.app.hyb.dto;


public class PesquisaOsResultadoModel {
	
	private String CODIGO;
	private String CODIGO_SITUACAO;
	private String CODIGO_AREA;
	
	private String SITUACAO;
	private String DATA_ABERTURA;
	private String AREA;
	
	public PesquisaOsResultadoModel(String cODIGO, String cODIGO_SITUACAO, String cODIGO_AREA, String sITUACAO,
			String dATA_ABERTURA, String aREA) {
		CODIGO = cODIGO;
		CODIGO_SITUACAO = cODIGO_SITUACAO;
		CODIGO_AREA = cODIGO_AREA;
		SITUACAO = sITUACAO;
		DATA_ABERTURA = dATA_ABERTURA;
		AREA = aREA;
	}
	
	public String getCODIGO() {
		return CODIGO;
	}
	public void setCODIGO(String cODIGO) {
		CODIGO = cODIGO;
	}
	public String getCODIGO_SITUACAO() {
		return CODIGO_SITUACAO;
	}
	public void setCODIGO_SITUACAO(String cODIGO_SITUACAO) {
		CODIGO_SITUACAO = cODIGO_SITUACAO;
	}
	public String getCODIGO_AREA() {
		return CODIGO_AREA;
	}
	public void setCODIGO_AREA(String cODIGO_AREA) {
		CODIGO_AREA = cODIGO_AREA;
	}
	public String getSITUACAO() {
		return SITUACAO;
	}
	public void setSITUACAO(String sITUACAO) {
		SITUACAO = sITUACAO;
	}
	public String getDATA_ABERTURA() {
		return DATA_ABERTURA;
	}
	public void setDATA_ABERTURA(String dATA_ABERTURA) {
		DATA_ABERTURA = dATA_ABERTURA;
	}
	public String getAREA() {
		return AREA;
	}
	public void setAREA(String aREA) {
		AREA = aREA;
	}
	
	
}
