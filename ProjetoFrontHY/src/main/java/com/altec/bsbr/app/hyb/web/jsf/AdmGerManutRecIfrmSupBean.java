package com.altec.bsbr.app.hyb.web.jsf;

import java.text.ParseException;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.swing.text.MaskFormatter;

import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmGerRecurso.XHYAdmGerRecursoEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;



@Component("AdmGerManutRecIfrmSupBean")
@Scope("session")
public class AdmGerManutRecIfrmSupBean extends BasicBBean {

	private static final long serialVersionUID = 1L;
	
	@Autowired
	private XHYAdmGerRecursoEndPoint admGerRecursoEndPoint;
	
	private String cpfSuperior;
	private String nomeSuperior;
	private String retornoSuperior;
	
	@PostConstruct
    public void init() {
		recuperaParametros();
	}
	
	public void recuperaParametros() {
	
		if (FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("cpf") != null) {
			this.cpfSuperior = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("cpf").toString();
			
			try {
				MaskFormatter format = new MaskFormatter("###.###.###-##");
				format.setValueContainsLiteralCharacters(false);
				this.cpfSuperior = format.valueToString(this.cpfSuperior);
			} 
			catch(ParseException ex) {
				throw new RuntimeException(ex);
			}			
		}else {
			this.setCpf("");
        	this.setNome("");
		}
		
		if (FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("nome") != null)
			this.nomeSuperior = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("nome").toString();
		
		    	
    	
	}

    public void formSubmit() {    	
    	
    	/* Mock do backend, dicionario de cpf-nome pra simular consulta */
    	/*Map<String, String> mock = new HashMap<String, String>();
    	mock.put("11111111111", "Teste");
    	mock.put("12345678901", "Mock");*/
    	/*try {
    		this.cpf = (this.cpf.replace("-", "")).replace(".", "");
    		retorno = admGerRecursoEndPoint.consultarRecurso(this.cpf, "", "");
    		
    		JSONObject consultarRecurso = new JSONObject(retorno);
	        JSONArray pcursor = consultarRecurso.getJSONArray("PCURSOR");
	        
	        System.out.println("------ NOME --> " + pcursor.length());
	        
	        if(pcursor.length() == 0) {
	        	this.setCpf("");
	        	this.setNome("");
	        	RequestContext.getCurrentInstance().execute("alert('Superior não Encontrado !!')");
	        }else {
	        	System.out.println( pcursor.getJSONObject(0).getString("NOME"));
		        this.setNome(pcursor.getJSONObject(0).getString("NOME"));
		        RequestContext.getCurrentInstance().execute("alert('Superior Aqui !!')");
	        }
	        
	        System.out.println("cpf: " + this.cpf);
	        System.out.println("nome: " + this.nome);
	        
		} catch (Exception e) {
			e.printStackTrace();
		}*/
    	
    	
    	/*if (mock.get(this.cpf) != null) { 
    		this.nome = mock.get(this.cpf);
    	}
    	else {
    		RequestContext.getCurrentInstance().execute("alert('Superior não Encontrado !!')");
    	}*/
    		
    }
    
    public void pesquisar() {
    	System.out.println("PESQUISA ---");
    	System.out.println("CPF: "+ cpfSuperior);
    	System.out.println("NOME" + nomeSuperior);
    	
    	try {
    		System.out.println(this.cpfSuperior.replace("-", ""));
    		this.cpfSuperior = this.cpfSuperior.replace("-", "");
    		this.cpfSuperior = this.cpfSuperior.replace(".", "");
    		
    		retornoSuperior = admGerRecursoEndPoint.consultarRecurso(this.getCpf(), "", "");
    		
    		JSONObject consultarRecurso = new JSONObject(retornoSuperior);
	        JSONArray pcursor = consultarRecurso.getJSONArray("PCURSOR");
	        
	        
	        if(pcursor.length() == 0) {
	        	this.setCpf("");
	        	this.setNome("");
	        	RequestContext.getCurrentInstance().execute("alert('Superior não Encontrado !!')");
	        }else {
	        	System.out.println( pcursor.getJSONObject(0).getString("NOME"));
		        nomeSuperior =  pcursor.getJSONObject(0).getString("NOME");
		        System.out.println("nome : " + nomeSuperior);
	        }
	        
    	} catch (Exception e) {
			e.printStackTrace();
		}
    	
    }

	public String getCpf() {
		return cpfSuperior;
	}


	public String getNome() {
		return nomeSuperior;
	}

	public void setCpf(String cpf) {
		this.cpfSuperior = cpf;
	}


	public void setNome(String nome) {
		this.nomeSuperior = nome;
	}
	
}