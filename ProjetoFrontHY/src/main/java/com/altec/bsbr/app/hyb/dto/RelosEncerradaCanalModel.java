package com.altec.bsbr.app.hyb.dto;

public class RelosEncerradaCanalModel {

	private int nOS;
	private String dataAbertura;
	private String dataEvento;
	private String dataEncerramento;
	private String agencia;
	private String cc;
	private String cliente;
	private String pfPj;
	private double valorEnvolvido;
	private double valorPrejuizo;
	private String analista;
	private double saldoDisponivel;
	private double limites;
	private String poupanca;
	private String clienteAjudado;
	private String nomeAgencia;
	private String cartaoRecebido;
	private String cartaoRetido;
	private String interesse;
	private String fragilizacao;
	private String horario;
	private int codigoAgencia;
	private String descricaoAgencia;
	private String dataOcorrenciaPoc;
	private int nTerminal;
	private String possuiSensor;
	private String outros;
	private String nomeEstabelecimento;
	private String solicitacao;
	private String ativacao;
	private String cancelamento;
	private String indeferidas;
	private String clienteInformou;
	private String ressarcido;
	private String tipoProduto;
	private String operacoes;
	private String quantidade;
	private double valorTotal;
	private double percentual;
	private String ipCliente;
	private String ipFraude;
	private String telefoneCliente;
	private String telefoneFraude;
	
	
	public RelosEncerradaCanalModel(int nOS, String dataAbertura, String dataEvento, String dataEncerramento,
			String agencia, String cc, String cliente, String pfPj, double valorEnvolvido, double valorPrejuizo,
			String analista, double saldoDisponivel, double limites, String poupanca, String clienteAjudado,
			String nomeAgencia, String cartaoRecebido, String cartaoRetido, String interesse, String fragilizacao,
			String horario, int codigoAgencia, String descricaoAgencia, String dataOcorrenciaPoc, int nTerminal,
			String possuiSensor, String outros, String nomeEstabelecimento, String solicitacao, String ativacao,
			String cancelamento, String indeferidas, String clienteInformou, String ressarcido, String tipoProduto,
			String operacoes, String quantidade, double valorTotal, double percentual, String ipCliente,
			String ipFraude, String telefoneCliente, String telefoneFraude) {
		
		this.nOS = nOS;
		this.dataAbertura = dataAbertura;
		this.dataEvento = dataEvento;
		this.dataEncerramento = dataEncerramento;
		this.agencia = agencia;
		this.cc = cc;
		this.cliente = cliente;
		this.pfPj = pfPj;
		this.valorEnvolvido = valorEnvolvido;
		this.valorPrejuizo = valorPrejuizo;
		this.analista = analista;
		this.saldoDisponivel = saldoDisponivel;
		this.limites = limites;
		this.poupanca = poupanca;
		this.clienteAjudado = clienteAjudado;
		this.nomeAgencia = nomeAgencia;
		this.cartaoRecebido = cartaoRecebido;
		this.cartaoRetido = cartaoRetido;
		this.interesse = interesse;
		this.fragilizacao = fragilizacao;
		this.horario = horario;
		this.codigoAgencia = codigoAgencia;
		this.descricaoAgencia = descricaoAgencia;
		this.dataOcorrenciaPoc = dataOcorrenciaPoc;
		this.nTerminal = nTerminal;
		this.possuiSensor = possuiSensor;
		this.outros = outros;
		this.nomeEstabelecimento = nomeEstabelecimento;
		this.solicitacao = solicitacao;
		this.ativacao = ativacao;
		this.cancelamento = cancelamento;
		this.indeferidas = indeferidas;
		this.clienteInformou = clienteInformou;
		this.ressarcido = ressarcido;
		this.tipoProduto = tipoProduto;
		this.operacoes = operacoes;
		this.quantidade = quantidade;
		this.valorTotal = valorTotal;
		this.percentual = percentual;
		this.ipCliente = ipCliente;
		this.ipFraude = ipFraude;
		this.telefoneCliente = telefoneCliente;
		this.telefoneFraude = telefoneFraude;
	}

	public int getnOS() {
		return nOS;
	}


	public void setnOS(int nOS) {
		this.nOS = nOS;
	}


	public String getDataAbertura() {
		return dataAbertura;
	}


	public void setDataAbertura(String dataAbertura) {
		this.dataAbertura = dataAbertura;
	}


	public String getDataEvento() {
		return dataEvento;
	}


	public void setDataEvento(String dataEvento) {
		this.dataEvento = dataEvento;
	}


	public String getDataEncerramento() {
		return dataEncerramento;
	}


	public void setDataEncerramento(String dataEncerramento) {
		this.dataEncerramento = dataEncerramento;
	}


	public String getAgencia() {
		return agencia;
	}


	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}


	public String getCc() {
		return cc;
	}


	public void setCc(String cc) {
		this.cc = cc;
	}


	public String getCliente() {
		return cliente;
	}


	public void setCliente(String cliente) {
		this.cliente = cliente;
	}


	public String getPfPj() {
		return pfPj;
	}


	public void setPfPj(String pfPj) {
		this.pfPj = pfPj;
	}


	public double getValorEnvolvido() {
		return valorEnvolvido;
	}


	public void setValorEnvolvido(double valorEnvolvido) {
		this.valorEnvolvido = valorEnvolvido;
	}


	public double getValorPrejuizo() {
		return valorPrejuizo;
	}


	public void setValorPrejuizo(double valorPrejuizo) {
		this.valorPrejuizo = valorPrejuizo;
	}


	public String getAnalista() {
		return analista;
	}


	public void setAnalista(String analista) {
		this.analista = analista;
	}


	public double getSaldoDisponivel() {
		return saldoDisponivel;
	}


	public void setSaldoDisponivel(double saldoDisponivel) {
		this.saldoDisponivel = saldoDisponivel;
	}


	public double getLimites() {
		return limites;
	}


	public void setLimites(double limites) {
		this.limites = limites;
	}


	public String getPoupanca() {
		return poupanca;
	}


	public void setPoupanca(String poupanca) {
		this.poupanca = poupanca;
	}


	public String getClienteAjudado() {
		return clienteAjudado;
	}


	public void setClienteAjudado(String clienteAjudado) {
		this.clienteAjudado = clienteAjudado;
	}


	public String getNomeAgencia() {
		return nomeAgencia;
	}


	public void setNomeAgencia(String nomeAgencia) {
		this.nomeAgencia = nomeAgencia;
	}


	public String getCartaoRecebido() {
		return cartaoRecebido;
	}


	public void setCartaoRecebido(String cartaoRecebido) {
		this.cartaoRecebido = cartaoRecebido;
	}


	public String getCartaoRetido() {
		return cartaoRetido;
	}


	public void setCartaoRetido(String cartaoRetido) {
		this.cartaoRetido = cartaoRetido;
	}


	public String getInteresse() {
		return interesse;
	}


	public void setInteresse(String interesse) {
		this.interesse = interesse;
	}


	public String getFragilizacao() {
		return fragilizacao;
	}


	public void setFragilizacao(String fragilizacao) {
		this.fragilizacao = fragilizacao;
	}


	public String getHorario() {
		return horario;
	}


	public void setHorario(String horario) {
		this.horario = horario;
	}


	public int getCodigoAgencia() {
		return codigoAgencia;
	}


	public void setCodigoAgencia(int codigoAgencia) {
		this.codigoAgencia = codigoAgencia;
	}


	public String getDescricaoAgencia() {
		return descricaoAgencia;
	}


	public void setDescricaoAgencia(String descricaoAgencia) {
		this.descricaoAgencia = descricaoAgencia;
	}


	public String getDataOcorrenciaPoc() {
		return dataOcorrenciaPoc;
	}


	public void setDataOcorrenciaPoc(String dataOcorrenciaPoc) {
		this.dataOcorrenciaPoc = dataOcorrenciaPoc;
	}


	public int getnTerminal() {
		return nTerminal;
	}


	public void setnTerminal(int nTerminal) {
		this.nTerminal = nTerminal;
	}


	public String getPossuiSensor() {
		return possuiSensor;
	}


	public void setPossuiSensor(String possuiSensor) {
		this.possuiSensor = possuiSensor;
	}


	public String getOutros() {
		return outros;
	}


	public void setOutros(String outros) {
		this.outros = outros;
	}


	public String getNomeEstabelecimento() {
		return nomeEstabelecimento;
	}


	public void setNomeEstabelecimento(String nomeEstabelecimento) {
		this.nomeEstabelecimento = nomeEstabelecimento;
	}


	public String getSolicitacao() {
		return solicitacao;
	}


	public void setSolicitacao(String solicitacao) {
		this.solicitacao = solicitacao;
	}


	public String getAtivacao() {
		return ativacao;
	}


	public void setAtivacao(String ativacao) {
		this.ativacao = ativacao;
	}


	public String getCancelamento() {
		return cancelamento;
	}


	public void setCancelamento(String cancelamento) {
		this.cancelamento = cancelamento;
	}


	public String getIndeferidas() {
		return indeferidas;
	}


	public void setIndeferidas(String indeferidas) {
		this.indeferidas = indeferidas;
	}


	public String getClienteInformou() {
		return clienteInformou;
	}


	public void setClienteInformou(String clienteInformou) {
		this.clienteInformou = clienteInformou;
	}


	public String getRessarcido() {
		return ressarcido;
	}


	public void setRessarcido(String ressarcido) {
		this.ressarcido = ressarcido;
	}


	public String getTipoProduto() {
		return tipoProduto;
	}


	public void setTipoProduto(String tipoProduto) {
		this.tipoProduto = tipoProduto;
	}


	public String getOperacoes() {
		return operacoes;
	}


	public void setOperacoes(String operacoes) {
		this.operacoes = operacoes;
	}


	public String getQuantidade() {
		return quantidade;
	}


	public void setQuantidade(String quantidade) {
		this.quantidade = quantidade;
	}


	public double getValorTotal() {
		return valorTotal;
	}


	public void setValorTotal(double valorTotal) {
		this.valorTotal = valorTotal;
	}


	public double getPercentual() {
		return percentual;
	}


	public void setPercentual(double percentual) {
		this.percentual = percentual;
	}


	public String getIpCliente() {
		return ipCliente;
	}


	public void setIpCliente(String ipCliente) {
		this.ipCliente = ipCliente;
	}


	public String getIpFraude() {
		return ipFraude;
	}


	public void setIpFraude(String ipFraude) {
		this.ipFraude = ipFraude;
	}


	public String getTelefoneCliente() {
		return telefoneCliente;
	}


	public void setTelefoneCliente(String telefoneCliente) {
		this.telefoneCliente = telefoneCliente;
	}


	public String getTelefoneFraude() {
		return telefoneFraude;
	}


	public void setTelefoneFraude(String telefoneFraude) {
		this.telefoneFraude = telefoneFraude;
	}


}
