package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.ObjRsOs;
import com.altec.bsbr.app.hyb.dto.UnidadeEnvolvida;
import com.altec.bsbr.app.hyb.dto.UsuarioIncModel;
import com.altec.bsbr.app.hyb.web.util.LogIncUtil;
import com.altec.bsbr.app.hyb.web.util.XHYUsuarioIncService;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmLog.XHYAdmLogEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsDadosIniciais.XHYCadOsDadosIniciaisEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsEquipAtrib.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsEquipAtrib.XHYCadOsEquipAtribEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;

@Component("CadOsBean")
@Scope("session")
public class CadOsBean extends BasicBBean{
	private static final long serialVersionUID = 1L;
	
	@Autowired
	private XHYUsuarioIncService usuario;
	
	@Autowired
	private XHYCadOsDadosIniciaisEndPoint objCadOsDadosIniciais;
	
	@Autowired
	private XHYCadOsEquipAtribEndPoint objCadOsEquipAtrib;
	
	@Autowired
	private XHYAdmLogEndPoint log;
	
	private UsuarioIncModel usuarioModel;
	
	private ObjRsOs objRs;
	private List<ObjRsOs> objsRsOs;
	
	private String strAction;
	private String strCdArea;
	private String strCpfUsu;
	private Map<String, String> params;
	
	@PostConstruct
	public void init() {
		strAction = "";
		strCdArea = "";
		strCpfUsu = "";
		objsRsOs = new ArrayList<ObjRsOs>();
		
		FacesContext fc = FacesContext.getCurrentInstance();
		this.params = fc.getExternalContext().getRequestParameterMap();
		strAction = params.getOrDefault("strAction", "");
		
		pegarDadosUsuario();
		completarAcao();
	}
	
	public void pegarDadosUsuario() {
		if (usuario != null) {			
			try {
				usuarioModel = usuario.getDadosUsuario();
				strCdArea = usuarioModel.getCdAreaUsuario();
				strCpfUsu = usuarioModel.getCpfUsuario();
			
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void completarAcao() {
		if(strAction.equals("I")) {
			System.out.println("Entrou na condição");
			String retorno = "";
			JSONObject jsonRetorno;
			JSONArray pcursor;
			try {
				retorno = objCadOsDadosIniciais.inserirNovaOs(strCdArea);
				System.out.println("RETORNO " + retorno);
				jsonRetorno = new JSONObject(retorno);
				pcursor = jsonRetorno.getJSONArray("PCURSOR");
				
				for (int i = 0; i < pcursor.length(); i++) {
					JSONObject curr = pcursor.getJSONObject(i);
					objRs = new ObjRsOs();
					objRs.setCODIGO(curr.isNull("CODIGO") ? "" : curr.get("CODIGO").toString());
					this.objsRsOs.add(objRs);
					System.out.println("Código " + objRs.getCODIGO());
				}
				
				objCadOsEquipAtrib.adicionarRecurso(objRs.getCODIGO(), strCpfUsu);
				
				log.incluirAcaoLog(objRs.getCODIGO(), Integer.toString(LogIncUtil.CRIAR_NOVA_OS),
						strCpfUsu, "");
				RequestContext.getCurrentInstance().execute("window.location.href = 'hyb_cadosmenu.xhtml?&strNrSeqOs="+objRs.getCODIGO()+"&strReadOnly=0&strTitulo=Nova OS'");
				
					System.out.println("funcionou");			
			} catch (Exception e) {
				try {
					FacesContext.getCurrentInstance().getExternalContext()
							.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
	}
	
}
