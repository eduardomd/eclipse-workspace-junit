package com.altec.bsbr.app.hyb.dto;

public class CadastroOsPesquisaEnvolv {
	private String cboColaborador;
	private int intEntidade;
	
	public String getCboColaborador() {
		return cboColaborador;
	}

	public void setCboColaborador(String cboColaborador) {
		this.cboColaborador = cboColaborador;
	}

	public int getIntEntidade() {
		return intEntidade;
	}

	public void setIntEntidade(int intEntidade) {
		this.intEntidade = intEntidade;
	}
}
