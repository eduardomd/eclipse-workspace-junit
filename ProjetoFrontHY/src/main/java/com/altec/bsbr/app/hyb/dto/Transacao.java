package com.altec.bsbr.app.hyb.dto;

public class Transacao {

	private String prCheq;
	private String dtDesbloqueio;
	
	public Transacao() {
		
	}
	
	public String getPrCheq() {
		return prCheq;
	}
	public void setPrCheq(String prCheq) {
		this.prCheq = prCheq;
	}
	public String getDtDesbloqueio() {
		return dtDesbloqueio;
	}
	public void setDtDesbloqueio(String dtDesbloqueio) {
		this.dtDesbloqueio = dtDesbloqueio;
	}
	
	
}
