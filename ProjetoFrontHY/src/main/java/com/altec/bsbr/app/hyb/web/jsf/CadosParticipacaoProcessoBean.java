package com.altec.bsbr.app.hyb.web.jsf;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.CadosParticipacaoProcesso;
import com.altec.bsbr.app.hyb.dto.ObjRsParticip;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsParticip.XHYCadOsParticipEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;

@Component("cadosParticipacaoProcessoBeanBean")
@Scope("session")
public class CadosParticipacaoProcessoBean extends BasicBBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Autowired
	private XHYCadOsParticipEndPoint cadOsParticipEndPoint;
	
	private String strNrSeqOs;
	private List<ObjRsParticip> objRsParticipColaboradores = new ArrayList<ObjRsParticip>();
	private List<ObjRsParticip> objRsParticipClientes= new ArrayList<ObjRsParticip>();
	private List<ObjRsParticip> objRsParticipNaoClientes= new ArrayList<ObjRsParticip>();
	
	private CadosParticipacaoProcesso participacaoProcesso = new CadosParticipacaoProcesso();
	
	private String strReadOnly;
	private boolean desabilitarCampos = false;
	

	@PostConstruct
	public void init() {
		
		RequestContext.getCurrentInstance().execute("parent.PF('statusDialog').show()");
		RequestContext.getCurrentInstance().execute("console.log('AGUARDE.....')");
		if(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs") != null) {
			strNrSeqOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs").toString();
		}
		else if(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("frmParticipante:modalEquipe:nros") != null) {
    		strNrSeqOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("frmParticipante:modalEquipe:nros").toString();
    		System.out.println("strNrSeqOs:" + strNrSeqOs);
		}
		System.out.println("paramss strNrSeqOs os" + strNrSeqOs);
		if (this.strNrSeqOs != null && !strNrSeqOs.isEmpty()) {
			fillTblPart(Long.valueOf(1));
			fillTblPart(Long.valueOf(2));
			fillTblPart(Long.valueOf(4));
		}
		/*if (FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs") != null) {
			this.strNrSeqOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs").toString();
			
		}*/
		
		if(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strReadOnly") != null) {
			this.strReadOnly = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strReadOnly").toString();
		}
		
		if ((this.strReadOnly != null) && !this.strReadOnly.isEmpty()) {
			if (this.strReadOnly.equals("1") ) {
				setDesabilitarCampos(true);
			} 
		}
		RequestContext.getCurrentInstance().execute("parent.PF('statusDialog').hide()");
		RequestContext.getCurrentInstance().execute("console.log('Fim')");
	}
	    
    public void fillTblPart(Long intTipo) {
    	
        System.out.println("fillTblPart  "+ intTipo +" ->> LONG !!!!!");
        System.out.println(this.strNrSeqOs + ":::: this.strNrSeqOs");
        		
        if (intTipo == 1) {
        	objRsParticipColaboradores.clear();
        } else if (intTipo == 2) {
        	objRsParticipClientes.clear();
        }else {
        	objRsParticipNaoClientes.clear();
        }
        System.out.println("FILLL ---@@@");

        try {
        	System.out.println("this.strNrSeqOs" + this.strNrSeqOs + "," + intTipo.intValue());
			JSONObject json = new JSONObject(cadOsParticipEndPoint.consultarCliente(this.strNrSeqOs, intTipo.intValue()));
			JSONArray pcursor = json.getJSONArray("PCURSOR");
			
			for (int i = 0; i < pcursor.length(); i++) {
				JSONObject item = pcursor.getJSONObject(i);
				
				ObjRsParticip particip = new ObjRsParticip();
				particip.setTP_PESSOA(item.isNull("TP_PESSOA")? "" : item.get("TP_PESSOA").toString());
				particip.setSQ_PARP(item.isNull("SQ_PARP")? "" : item.get("SQ_PARP").toString());				
				particip.setCPF_CNPJ(item.isNull("CPF_CNPJ")? "" : item.get("CPF_CNPJ").toString());
				particip.setNOME(item.isNull("NOME")? "" : item.get("NOME").toString());
				particip.setMATRICULA(item.isNull("MATRICULA")? "" :item.get("MATRICULA").toString());
				particip.setCARGO(item.isNull("CARGO")? "" : item.get("CARGO").toString());
				particip.setPARTICIPACAO(item.isNull("PARTICIPACAO")? "" : item.get("PARTICIPACAO").toString());
				particip.setSEGMENTO(item.isNull("SEGMENTO")? "" : item.get("SEGMENTO").toString());
				
				if (intTipo == 1) {
					objRsParticipColaboradores.add(particip);
                    
                }
				else if (intTipo == 2) {
					objRsParticipClientes.add(particip);
                    
                }
                else {
                	objRsParticipNaoClientes.add(particip);
                }
			}
        } catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    public void excluir() {
        try {
        	String sq_parp = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("sq_parp").toString();
        	
        	cadOsParticipEndPoint.excluirParticipante(sq_parp);        	
        	RequestContext.getCurrentInstance().execute("alert('Participante excluído com sucesso!')");
        	this.reloadObjects();
		} catch (Exception e) {
			e.printStackTrace();
			RequestContext.getCurrentInstance().execute("alert('Não foi possível excluir o participante')");
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
    }

    public String clearBean() {
    	String beanName = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("beanName").toString();
    	String pageName = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("pageName").toString();
    	System.out.println("beanName:: "+beanName);
    	System.out.println("pageName:: "+pageName);
		if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(beanName) != null)
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove(beanName);	
		
		return pageName+"&faces-redirect=true";
    }
    
    public void reloadObjects() {
    	System.out.println("::: " + this.strNrSeqOs + " fim");
    	this.objRsParticipClientes.clear();
    	this.objRsParticipColaboradores.clear();
    	this.objRsParticipNaoClientes.clear();
		fillTblPart(Long.valueOf(1));
		fillTblPart(Long.valueOf(2));
		fillTblPart(Long.valueOf(4));
    }
    
    public void closeLoading(){
    	System.out.println("closeLoading");
    	RequestContext.getCurrentInstance().execute("parent.PF('statusDialog').hide();");
    }
    
    public void openLoading() {
    	System.out.println("openLoading");
    	RequestContext.getCurrentInstance().execute("parent.PF('statusDialog').show();");
    }    
    
	public String getStrNrSeqOs() {
		return strNrSeqOs;
	}

	public void setStrNrSeqOs(String strNrSeqOs) {
		this.strNrSeqOs = strNrSeqOs;
	}

	public List<ObjRsParticip> getObjRsParticipColaboradores() {
		return objRsParticipColaboradores;
	}

	public void setObjRsParticipColaboradores(List<ObjRsParticip> objRsParticipColaboradores) {
		this.objRsParticipColaboradores = objRsParticipColaboradores;
	}

	public List<ObjRsParticip> getObjRsParticipClientes() {
		return objRsParticipClientes;
	}

	public void setObjRsParticipClientes(List<ObjRsParticip> objRsParticipClientes) {
		this.objRsParticipClientes = objRsParticipClientes;
	}

	public List<ObjRsParticip> getObjRsParticipNaoClientes() {
		return objRsParticipNaoClientes;
	}
	
	public CadosParticipacaoProcesso getParticipacaoProcesso() {
		return participacaoProcesso;
	}

	public void setObjRsParticipNaoClientes(List<ObjRsParticip> objRsParticipNaoClientes) {
		this.objRsParticipNaoClientes = objRsParticipNaoClientes;
	}

	public String getStrReadOnly() {
		return strReadOnly;
	}

	public void setStrReadOnly(String strReadOnly) {
		this.strReadOnly = strReadOnly;
	}
	
	public boolean isDesabilitarCampos() {
		return desabilitarCampos;
	}

	public void setDesabilitarCampos(boolean desabilitarCampos) {
		this.desabilitarCampos = desabilitarCampos;
	}
	
	

  
}
