package com.altec.bsbr.app.hyb.web.jsf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmGer.XHYAdmGerEndPoint;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmGer.WebServiceException;

import com.altec.bsbr.app.hyb.dto.AdmGerManutPCorpModel;
import com.altec.bsbr.app.hyb.dto.AdmGerManutPCorpCanaisModel;

import com.altec.bsbr.fw.web.jsf.BasicBBean;

@Component("AdmGerManutPCorpBean")
@Scope("request")
public class AdmGerManutPCorpBean extends BasicBBean{

	private static final long serialVersionUID = 1L;
	
	private static final String TABLE_CARREGANDO = "Carregando...";
	private static final String TABLE_VAZIO = "Sem registro"; 
		
	@Autowired
	private XHYAdmGerEndPoint admGetPCorp;

    private AdmGerManutPCorpModel objRsAdmPCorplModel;
    private AdmGerManutPCorpCanaisModel objRsAdmPCorpCanaislModel;

    private LazyDataModel<AdmGerManutPCorpModel> objRsAdmPCorp;
    private List<AdmGerManutPCorpCanaisModel> objRsAdmCanaisPCorp;
    
	private String txtDescricao = "";
	private String retorno;
    private String emptyMessage;
    
    private Map<String, Boolean> manutPCorpChecked;
      
    @PostConstruct
    public void init() {
    	emptyMessage = TABLE_CARREGANDO;
    	montarListaCanais();
    }

    public void montarListaCanais() {
    	objRsAdmCanaisPCorp = new ArrayList<AdmGerManutPCorpCanaisModel>();
    	
		JSONObject objJSON;
		JSONArray pcursor;

		try {			
 			retorno = admGetPCorp.consultarCanal();
 	        
 			objJSON = new JSONObject(retorno);
 	        pcursor = objJSON.getJSONArray("PCURSOR");

 	        for (int i = 0; i < pcursor.length(); i++) {
 	        	JSONObject f = pcursor.getJSONObject(i);
 	        	objRsAdmPCorpCanaislModel = new AdmGerManutPCorpCanaisModel();	            
 	        	objRsAdmPCorpCanaislModel.setCODIGO(f.isNull("CODIGO") ? 0 : f.getInt("CODIGO"));
 	        	objRsAdmPCorpCanaislModel.setNOME(f.isNull("NOME") ? "" : f.getString("NOME"));
 	        	objRsAdmPCorpCanaislModel.setEXCLUSAO(f.getInt("EXCLUSAO") == 1 ? true : false);
 	 	        
 	        	objRsAdmCanaisPCorp.add( new AdmGerManutPCorpCanaisModel(
 	            		objRsAdmPCorpCanaislModel.getCODIGO(), 
 	            		objRsAdmPCorpCanaislModel.getNOME(), 
 	            		objRsAdmPCorpCanaislModel.getEXCLUSAO())
 	    		); 
 	            
 	        } 
 	        
 		} catch (WebServiceException e) {
 		    // TODO Auto-generated catch block
 		    e.printStackTrace();
 		    FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "hyb_erro.xhtml?&strErro=" + e.getMessage());
        }
    }

    public void montarListaPCorp_Lazy() {
    	
    	setEmptyMessage(TABLE_VAZIO);
    	
    	objRsAdmPCorp = new LazyDataModel<AdmGerManutPCorpModel>() {

			private static final long serialVersionUID = 1L;
			
			@Override
			public List<AdmGerManutPCorpModel> load(int first, int pageSize, String sortField,
                                                    SortOrder sortOrder, Map<String, Object> filters) {
												
				List<AdmGerManutPCorpModel> admGerManutPCorpModelTemp = montarListaPCorp();
				
				setRowCount(admGerManutPCorpModelTemp.size());
				setPageSize(pageSize);
				
				List<AdmGerManutPCorpModel> result = new ArrayList<AdmGerManutPCorpModel>();
				
				if(manutPCorpChecked == null)
					manutPCorpChecked = new HashMap<String, Boolean>();
				
				manutPCorpChecked.clear();
				int criarConexao = 0;
				
				for (int i = first; i < (first + pageSize); i++) {
					
					result.add(admGerManutPCorpModelTemp.get(i));
					
					for (int j = 0; j < objRsAdmCanaisPCorp.size(); j++) {
						
						String chave = objRsAdmCanaisPCorp.get(j).getCODIGO() + "_" + admGerManutPCorpModelTemp.get(i).getCODIGO();
												
						manutPCorpChecked.put(chave, checarCanalPCorp(criarConexao, objRsAdmCanaisPCorp.get(j).getCODIGO(), admGerManutPCorpModelTemp.get(i).getCODIGO()));					
					
						criarConexao++;
					}
				}
				
				System.out.println("Size MAP:: " + manutPCorpChecked.size());
				
				return result;
			}
		};
    }
    
    private List<AdmGerManutPCorpModel> montarListaPCorp() {
    	List<AdmGerManutPCorpModel> objRsAdmPCorpTemp = new ArrayList<AdmGerManutPCorpModel>();
		
		JSONObject objJSON;
		JSONArray pcursor;
		try {			
 			if (txtDescricao.isEmpty())
 				retorno = admGetPCorp.consultarPCorp();
 			else
 				retorno = admGetPCorp.pesquisarPCorp(txtDescricao);
 	        
 			objJSON = new JSONObject(retorno);
 	        pcursor = objJSON.getJSONArray("PCURSOR");
 	        
 	        
 	        for (int i = 0; i < pcursor.length(); i++) {
 	        	JSONObject f = pcursor.getJSONObject(i);
 	        	objRsAdmPCorplModel = new AdmGerManutPCorpModel();
 	        	objRsAdmPCorplModel.setCODIGO(f.isNull("CODIGO") ? 0 : f.getInt("CODIGO"));
 	        	objRsAdmPCorplModel.setNOME(f.isNull("NOME") ? "" : f.getString("NOME"));
 	        	objRsAdmPCorplModel.setSUBNOME(f.isNull("SUBNOME") ? "" : f.getString("SUBNOME"));
 	        	objRsAdmPCorplModel.setSLA(f.isNull("SLA") ? 0 : f.getInt("SLA"));
 	        	objRsAdmPCorplModel.setOPERACAODEB(f.isNull("OPERACAO") ? false : (f.getInt("OPERACAO") == 1 || f.getInt("OPERACAO") == 3) ? true : false);
 	        	objRsAdmPCorplModel.setOPERACAOCRED(f.isNull("OPERACAO") ? false : (f.getInt("OPERACAO") == 2 || f.getInt("OPERACAO") == 3) ? true : false);
 	        	objRsAdmPCorplModel.setVISU(f.getInt("VISU") == 1 ? true : false);
 	        	 	            
 	        	objRsAdmPCorpTemp.add( new AdmGerManutPCorpModel(
 	        			objRsAdmPCorplModel.getCODIGO(), 
 	        			objRsAdmPCorplModel.getNOME(),  
 	        			objRsAdmPCorplModel.getSUBNOME(),  
 	        			objRsAdmPCorplModel.getSLA(),
 	        			objRsAdmPCorplModel.getOPERACAODEB(), 
 	        			objRsAdmPCorplModel.getOPERACAOCRED(), 
 	        			objRsAdmPCorplModel.getVISU())
 	    		); 
 	            
 	        } 
 		} catch (WebServiceException e) {
 		    // TODO Auto-generated catch block
 		    e.printStackTrace();
 		    //FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "hyb_erro.xhtml?&strErro=" + e.getMessage());
        }
		
		return objRsAdmPCorpTemp;
    }
    
    private boolean checarCanalPCorp(int index, int codigoCanal, int codigoPCorp) {	
		boolean relac = false;
		
		try {
 	        
    		String txtcodigoPCorp = String.valueOf(codigoPCorp);
    		String txtcodigoCanal = String.valueOf(codigoCanal);
    		String retornoCanalPCorp = admGetPCorp.verificaCanalPCorp(index, txtcodigoCanal, txtcodigoPCorp);
 	        		
    		JSONObject verificaCanalPCorp = new JSONObject(retornoCanalPCorp);
    		JSONArray arr = verificaCanalPCorp.getJSONArray("PCURSOR");
    		JSONObject c = arr.getJSONObject(0);
 	        			        		 	        		
    		relac = (c.getInt("RELAC") == 0 ? false : true );
    		
 	        
 		} catch (WebServiceException e) {
 		    // TODO Auto-generated catch block
 		    e.printStackTrace();
 		    //FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "hyb_erro.xhtml?&strErro=" + e.getMessage());
        }
		
		return relac;
    }
    
    public void getObjRsAdmPenalJSON() {
    	JSONArray arr = new JSONArray(getObjRsAdmPCorp());
    	System.out.println(arr.toString());
    }
    
    public void salvar() {
    	String[] PCorp = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("arrPCorp").toString().split("@");
    	String[] canaisPCorp = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("arrCanaisPCorp").toString().split("@");

		String[] pCorpItens;
		String[] canaisPCorpItens;
		
		boolean validarAlteracao = false;
		
		try {
			for (int i=0; i<PCorp.length; i++) {
				pCorpItens = PCorp[i].split("\\|");	
				
				int intTipoOp = 0;
			    
		    	if (Boolean.parseBoolean(pCorpItens[2]) == true && Boolean.parseBoolean(pCorpItens[3]) == false)
		    		intTipoOp = 1;
		    	
		    	if (Boolean.parseBoolean(pCorpItens[2]) == false && Boolean.parseBoolean(pCorpItens[3]) == true)
		    		intTipoOp = 2;
		    	
		    	if (Boolean.parseBoolean(pCorpItens[2]) == true && Boolean.parseBoolean(pCorpItens[3]) == true)
		    		intTipoOp = 3;
		    	
		    	validarAlteracao = admGetPCorp.alterarPCorp(String.valueOf(pCorpItens[0]), String.valueOf(pCorpItens[1]), String.valueOf(intTipoOp));
		    	
//		    	if (!validarAlteracao){
//				    try {
//			    		FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=NÃ£o foi possÃ­vel atualizar " + pCorpItens[0] + pCorpItens[1]);
//					} catch (Exception ex) {
//					    // TODO Auto-generated catch block
//					    ex.printStackTrace();
//					}
//		    	}
			}
    		
    		for (int i=0; i<canaisPCorp.length; i++) {
        		String[] canaisPCorpItens1;
    			canaisPCorpItens = canaisPCorp[i].split("\\|");
    			canaisPCorpItens1 = canaisPCorpItens[0].split("_");
    			
    			if (Boolean.parseBoolean(canaisPCorpItens[1])) 
    				validarAlteracao = admGetPCorp.inserirCanalPCorp(String.valueOf(canaisPCorpItens1[0]),String.valueOf(canaisPCorpItens1[1]));
    			else
    				validarAlteracao = admGetPCorp.apagarCanalPCorp(String.valueOf(canaisPCorpItens1[0]),String.valueOf(canaisPCorpItens1[1]));
		    	
//		    	if (!validarAlteracao){
//				    try {
//			    		FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=NÃ£o foi possÃ­vel atualizar " + canaisPCorpItens1[0] + canaisPCorpItens1[1]);
//					} catch (Exception ex) {
//					    // TODO Auto-generated catch block
//					    ex.printStackTrace();
//					}
//		    	}
    		}    		

			RequestContext.getCurrentInstance().execute("alert('AlteraÃ§Ãµes efetuadas com sucesso!!!!!')"); 

			montarListaCanais();
			montarListaPCorp_Lazy();

		} catch (Exception e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
			try {
	    		FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
			} catch (Exception ex) {
			    // TODO Auto-generated catch block
			    ex.printStackTrace();
			}
		}  
    }
    
    public boolean isCanalPCorpChecked(int codigoCanal, int codigoPAux) {
		String chave = codigoCanal + "_" + codigoPAux;
		return manutPCorpChecked.get(chave);
    }
    
	public String getTxtDescricao() {
		return txtDescricao;
	}

	public void setTxtDescricao(String txtDescricao) {
		this.txtDescricao = txtDescricao;
	}

	public List<AdmGerManutPCorpCanaisModel> getObjRsAdmCanaisPCorp() {
		return objRsAdmCanaisPCorp;
	}


	public void setObjRsAdmCanaisPCorp(List<AdmGerManutPCorpCanaisModel> objRsAdmCanaisPCorp) {
		this.objRsAdmCanaisPCorp = objRsAdmCanaisPCorp;
	}
	
	public String getEmptyMessage() {
		return emptyMessage;
	}

	public void setEmptyMessage(String emptyMessage) {
		this.emptyMessage = emptyMessage;
	}

	public LazyDataModel<AdmGerManutPCorpModel> getObjRsAdmPCorp() {
		return objRsAdmPCorp;
	}
}
