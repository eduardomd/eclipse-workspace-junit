package com.altec.bsbr.app.hyb.web.util;

import java.io.IOException;
import java.util.ArrayList;

import com.altec.bsbr.app.hyb.dto.CadastroOsDefaultModel;

public class CadOsEventosUtil {

	public ArrayList<CadastroOsDefaultModel> ConsultarFraudeCanalDetalhe(String strNrSeqId) throws IOException {

		ArrayList<CadastroOsDefaultModel> retornoConsultar = new ArrayList<CadastroOsDefaultModel>();


		boolean strErro = false;

		if (strErro == true) {
			retornoConsultar.add(new CadastroOsDefaultModel(
					null, // strNrSeqOs - String número da OS;
					null, // strTitular - Nome do Titular
					null, // strSeqConta - Número Sequencial do Conta
					null, // strSeqCartao - Número Sequencial do Cartão
					null, // strCdCanal - Código do Canal
					null, // strSeqEvento - Número Sequencial da Fraude Evento
					null, // strCpfCnpj - Número do CPF CNPJ
					null, // strNrQntdEven - Quantidade de eventos
					null, // strCdCentroOrigem - Centro de origem pCD_CTRO_ORIG
					null, // strCdCentroDestino - Centro de destino pCD_CTRO_DEST
					null, // strCdCentroOperante - Centro operante pCD_CTRO_OPER
					null, // strContabilizado - valida contablização
					"Erro de consulta" // strErro - retorno de erro se existir
					));
		}else {
			retornoConsultar.add(new CadastroOsDefaultModel(
					
					"111111", // strNrSeqOs - String número da OS;
					"Nome Titular", // strTitular - Nome do Titular
					"22222", // strSeqConta - Número Sequencial do Conta
					"33333", // strSeqCartao - Número Sequencial do Cartão
					"4321", // strCdCanal - Código do Canal
					"44444", // strSeqEvento - Número Sequencial da Fraude Evento
					"123.456.789-00", // strCpfCnpj - Número do CPF CNPJ
					"2", // strNrQntdEven - Quantidade de eventos
					"5555", // strCdCentroOrigem - Centro de origem pCD_CTRO_ORIG
					"6666", // strCdCentroDestino - Centro de destino pCD_CTRO_DEST
					"7777", // strCdCentroOperante - Centro operante pCD_CTRO_OPER
					"Sim", // strContabilizado - valida contablização
					"" // strErro - retorno de erro se existir
			));

		}

		return retornoConsultar;

	}

}
