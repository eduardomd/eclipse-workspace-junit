package com.altec.bsbr.app.hyb.web.jsf; 

import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.ObjRsRiscoPotencial;
import com.altec.bsbr.app.hyb.dto.ObjRsRiscoPotencialContaLesado;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsRiscPoten.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYCadOsRiscPoten.XHYCadOsRiscPotenEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean;


@Component("cadOsRiscoPotencialBean")
@Scope("session")
public class ClsCadOsRiscPoten extends BasicBBean {
 
	private static final long serialVersionUID = 1L;
	private ObjRsRiscoPotencial objRsRiscoPotencial;
	private List<ObjRsRiscoPotencialContaLesado> objRsContaLesado;
	private String strNrOs;
	private String dblValPerdEfet;
	
	@Autowired 
	XHYCadOsRiscPotenEndPoint cadOsRiscPoten;
	
	@PostConstruct
	public void init() {
		
		if(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs") != null) {
			this.strNrOs = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqOs").toString();
			this.setupObjRsRiscoPotencial();
		}
		System.out.println("this.strNrOs" + this.strNrOs);
		
	}
	
	public String verifZero(String str) {
		if(!str.isEmpty()) {
			String newStr = str.replace(".", "");
			String[] splitStr = newStr.split(",");
			String firstNum = splitStr[0];
			String secondNum = splitStr[1];
		
			System.out.println("FIRST NUM: " + firstNum);
		
			if (secondNum.equals("00")) {
				return firstNum;
			} else if(secondNum.substring(secondNum.length()-1).equals("0")) {
				return firstNum + "." + secondNum.substring(0,1);
			} else {
				return firstNum + "." + secondNum;
			}
		}
		return str;
		
	}
	
	public String formataValorParaEnvio(String valorRecebido) {
		String valor = valorRecebido.replace(",", "").replace(".", "");
		int size = valor.length() - 2;
		return valor.substring(0, size).concat(".").concat(valor.substring(size, valor.length()));
	}
	
	public void salvar() {
		
		String vlEnvolv = verifZero(objRsRiscoPotencial.getVL_ENVOL());
		String vlRecr = verifZero(objRsRiscoPotencial.getVL_RECR());
		String vlExclEvit = verifZero(objRsRiscoPotencial.getVL_EXCL_EVIT());
		String vlTrst = verifZero(objRsRiscoPotencial.getVL_TRST());
		String vlNega = verifZero(objRsRiscoPotencial.getVL_NEGA());
		String dblValPerd = verifZero(this.dblValPerdEfet);
		
		System.out.println("NUMBERS : " + vlEnvolv + " - " + vlRecr + " - " + vlExclEvit + " - " + vlTrst + " - " + vlNega);
		
		try {
			cadOsRiscPoten.fnUpdValIniRiscPoten(strNrOs,
												vlEnvolv,
												vlRecr,
												vlExclEvit,
												"0",
												dblValPerd,
												vlTrst,
												vlTrst,
												vlNega);
			
			RequestContext.getCurrentInstance().execute("alert('Dados atualizados com sucesso.')");
			
		} catch (WebServiceException e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
            } catch (IOException e1) {
               e1.printStackTrace();
            }
		}
		
	}
	
	public void setupObjRsRiscoPotencial() {
		
		try {
			String retorno = cadOsRiscPoten.fnSelValIniRiscPoten(this.strNrOs);
            
            JSONObject consultarContas = new JSONObject(retorno);
            JSONArray pcursor = consultarContas.getJSONArray("PCURSOR");
            
            objRsRiscoPotencial = new ObjRsRiscoPotencial();
            
            Locale ptBR = new Locale("pt", "BR");
            
            if (pcursor.length() > 0) {
            	JSONObject item = pcursor.getJSONObject(0);
            	
            	String vlTran = item.isNull("VL_TRAN") ? "0" : item.get("VL_TRAN").toString();
        		String vlRecr = item.isNull("VL_RECR") ? "0" : item.get("VL_RECR").toString();
        		String vlExclEvit = item.isNull("VL_EXCL_EVIT") ? "0" : item.get("VL_EXCL_EVIT").toString();
        		String vlNega = item.isNull("VL_NEGA") ? "0" : item.get("VL_NEGA").toString();
        		String vlTrst = item.isNull("VL_TRST") ? "0" : item.get("VL_TRST").toString();
        		String vlEnvol = item.isNull("VL_ENVOL") ? "0" : item.get("VL_ENVOL").toString();
        		String vlperdEfet = item.isNull("VL_PERD_EFET") ? "0" : item.get("VL_PERD_EFET").toString();
        		String vlPrej = item.isNull("VL_PREJ") ? "0" : item.get("VL_PREJ").toString();
        		System.out.println("EXCLUIR VAL: " + item.get("VL_EXCL_EVIT").toString());
        		objRsRiscoPotencial.setVL_TRAN(NumberFormat.getCurrencyInstance(ptBR).format(Double.parseDouble(vlTran)).replace("R$", "").trim());
        		objRsRiscoPotencial.setVL_RECR(NumberFormat.getCurrencyInstance(ptBR).format(Double.parseDouble(vlRecr)).replace("R$", "").trim());
        		objRsRiscoPotencial.setVL_EXCL_EVIT(NumberFormat.getCurrencyInstance(ptBR).format(Double.parseDouble(vlExclEvit)).replace("R$", "").trim());
        		objRsRiscoPotencial.setVL_NEGA(NumberFormat.getCurrencyInstance(ptBR).format(Double.parseDouble(vlNega)).replace("R$", "").trim());
        		objRsRiscoPotencial.setVL_TRST(NumberFormat.getCurrencyInstance(ptBR).format(Double.parseDouble(vlTrst)).replace("R$", "").trim());
        		objRsRiscoPotencial.setVL_ENVOL(NumberFormat.getCurrencyInstance(ptBR).format(Double.parseDouble(vlEnvol)).replace("R$", "").trim());
        		objRsRiscoPotencial.setVL_PERD_EFET(NumberFormat.getCurrencyInstance(ptBR).format(Double.parseDouble(vlperdEfet)).replace("R$", "").trim());
        		objRsRiscoPotencial.setVL_PREJ(NumberFormat.getCurrencyInstance(ptBR).format(Double.parseDouble(vlPrej)).replace("R$", "").trim());
        		System.out.println("EXCL : " + vlExclEvit + " - " + objRsRiscoPotencial.getVL_EXCL_EVIT() + " - " + "RECR : " + vlRecr);
        		
        		this.dblValPerdEfet = NumberFormat.getCurrencyInstance(ptBR).format(Double.parseDouble(vlperdEfet) + Double.parseDouble(vlRecr)).replace("R$", "").trim();
        		
            }
            
		} catch (WebServiceException e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
            } catch (IOException e1) {
               e1.printStackTrace();
            }
		}
		
	}
	
	public void setupObjRsContaLesado() {
		try {
			String retorno = cadOsRiscPoten.fnSelContasLesado(this.strNrOs);
            
            JSONObject consultarContas = new JSONObject(retorno);
            JSONArray pcursor = consultarContas.getJSONArray("PCURSOR");
            
            objRsContaLesado = new ArrayList<ObjRsRiscoPotencialContaLesado>();
            
            System.out.println("CURSsOR : " + pcursor);
            if (pcursor.length() > 0) {
            	for (int i = 0; i < pcursor.length(); i++) {
	            	JSONObject item = pcursor.getJSONObject(i);
	            	
	        		String nmParp = item.isNull("NM_PARP") ? "" : item.get("NM_PARP").toString();
	        		String CdBanc = item.isNull("CD_BANC") ? "" : item.get("CD_BANC").toString();
	        		String nrCnta = item.isNull("NR_CNTA") ? "" : item.get("NR_CNTA").toString();
	        		
	        		
	        		objRsContaLesado.add(new ObjRsRiscoPotencialContaLesado(nmParp, CdBanc, nrCnta));

            	}
            }
            
		} catch (WebServiceException e) {
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("hyb_erro.xhtml?&strErro=" + e.getMessage());
            } catch (IOException e1) {
               e1.printStackTrace();
            }
		}
	}

	public ObjRsRiscoPotencial getObjRsRiscoPotencial() {
		return objRsRiscoPotencial;
	}

	public void setObjRsRiscoPotencial(ObjRsRiscoPotencial objRsRiscoPotencial) {
		this.objRsRiscoPotencial = objRsRiscoPotencial;
	}

	public List<ObjRsRiscoPotencialContaLesado> getObjRsContaLesado() {
		return objRsContaLesado;
	}

	public void setObjRsContaLesado(List<ObjRsRiscoPotencialContaLesado> objRsContaLesado) {
		this.objRsContaLesado = objRsContaLesado;
	}

	public String getStrNrOs() {
		return strNrOs;
	}

	public void setStrNrOs(String strNrOs) {
		this.strNrOs = strNrOs;
	}

	public String getDblValPerdEfet() {
		return dblValPerdEfet;
	}

	public void setDblValPerdEfet(String dblValPerdEfet) {
		this.dblValPerdEfet = dblValPerdEfet;
	}

	public XHYCadOsRiscPotenEndPoint getCadOsRiscPoten() {
		return cadOsRiscPoten;
	}

	public void setCadOsRiscPoten(XHYCadOsRiscPotenEndPoint cadOsRiscPoten) {
		this.cadOsRiscPoten = cadOsRiscPoten;
	}
	
}
 