package com.altec.bsbr.app.hyb.dto;

import java.util.ArrayList;
import java.util.List;

//Apagar ao integrar o backend
public class simuBack {
	
	private String TP_DESI = "AD";
	private String NR_MATR_PRSV = "NR_MATR_PRSV";
	private String NM_RECU_OCOR_ESPC = "NM_RECU_OCOR_ESPC";
	private String NM_DESIGNACAO = "NM_DESIGNACAO";
	private String CD_CARG = "Gerente";
	
	//Simula retorno do banco
	private List<String[]> listaRetorno = new ArrayList<String[]>();
	
	public simuBack(){
		String[] coluna = {"AD", "1", "Joel", "A", "B70555"};
		listaRetorno.add(coluna);
		String[] coluna2 = {"PSD", "2", "Jose", "B", "ax2"};
		listaRetorno.add(coluna2);
		String[] coluna3 = {"NA", "3", "Carla", "C", "B70555"};
		listaRetorno.add(coluna3);
		String[] coluna4 = {"AA", "4", "Paula", "D", "ax2"};
		listaRetorno.add(coluna4);
	}
	
	public List<String[]> ConsultarEquipeOs(){
		return listaRetorno;
	}
	
	
	
	public String getTP_DESI() {
		return TP_DESI;
	}
	public void setTP_DESI(String tP_DESI) {
		TP_DESI = tP_DESI;
	}
	public String getNR_MATR_PRSV() {
		return NR_MATR_PRSV;
	}
	public void setNR_MATR_PRSV(String nR_MATR_PRSV) {
		NR_MATR_PRSV = nR_MATR_PRSV;
	}
	public String getNM_RECU_OCOR_ESPC() {
		return NM_RECU_OCOR_ESPC;
	}
	public void setNM_RECU_OCOR_ESPC(String nM_RECU_OCOR_ESPC) {
		NM_RECU_OCOR_ESPC = nM_RECU_OCOR_ESPC;
	}
	public String getNM_DESIGNACAO() {
		return NM_DESIGNACAO;
	}
	public void setNM_DESIGNACAO(String nM_DESIGNACAO) {
		NM_DESIGNACAO = nM_DESIGNACAO;
	}
	public String getCD_CARG() {
		return CD_CARG;
	}
	public void setCD_CARG(String cD_CARG) {
		CD_CARG = cD_CARG;
	}
	
	
	
}
