package com.altec.bsbr.app.hyb.dto;

public class RelCartaApresentacaoModel {
	private String dataAtual;
	private String horaAtual;
	private String dataAbertura;	
	private String dataEncerramento;	
	private String situacao;	
	private String area;
	
	public RelCartaApresentacaoModel(String dataAtual, String horaAtual, String dataAbertura, String dataEncerramento, String situacao, String area) {
		this.dataAtual = dataAtual;
		this.horaAtual = horaAtual;
		this.dataAbertura = dataAbertura;
		this.dataEncerramento = dataEncerramento;
		this.situacao = situacao;
		this.area = area;
	}

	public RelCartaApresentacaoModel() {
	}

	public String getDataAtual() {
		return dataAtual;
	}

	public void setDataAtual(String dataAtual) {
		this.dataAtual = dataAtual;
	}

	public String getHoraAtual() {
		return horaAtual;
	}

	public void setHoraAtual(String horaAtual) {
		this.horaAtual = horaAtual;
	}

	public String getDataAbertura() {
		return dataAbertura;
	}

	public void setDataAbertura(String dataAbertura) {
		this.dataAbertura = dataAbertura;
	}

	public String getDataEncerramento() {
		return dataEncerramento;
	}

	public void setDataEncerramento(String dataEncerramento) {
		this.dataEncerramento = dataEncerramento;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

}
