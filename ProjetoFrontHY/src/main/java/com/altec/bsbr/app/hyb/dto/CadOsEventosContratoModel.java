package com.altec.bsbr.app.hyb.dto;

public class CadOsEventosContratoModel {

	private String contratoNrSequCntr; 
	private String contratoNrCntr;
	private String contratoNrSequParpProc;
	private String contratoNrSequFrauCnal;
	private String contratoCdEntiCntr;
	private String contratoNmProdCorp;
	private String contratoNrSequOrdeServ;
	private String contratoCdProd;
	private String contratoTpOperParp;
	private String contratoQtdLanc;
	
	public CadOsEventosContratoModel() {
		
	}
	
	public CadOsEventosContratoModel(String nrSequCntr, String nrCntr, String nmProdCorp, String cdProd) {
		this.contratoNrSequCntr = nrSequCntr;
		this.contratoNrCntr = nrCntr;
		this.contratoNmProdCorp = nmProdCorp;
		this.contratoCdProd = cdProd;
	}

	public String getContratoNrSequCntr() {
		return contratoNrSequCntr;
	}
	public void setContratoNrSequCntr(String contratoNrSequCntr) {
		this.contratoNrSequCntr = contratoNrSequCntr;
	}
	public String getContratoNrCntr() {
		return contratoNrCntr;
	}
	public void setContratoNrCntr(String contratoNrCntr) {
		this.contratoNrCntr = contratoNrCntr;
	}
	public String getContratoNrSequParpProc() {
		return contratoNrSequParpProc;
	}
	public void setContratoNrSequParpProc(String contratoNrSequParpProc) {
		this.contratoNrSequParpProc = contratoNrSequParpProc;
	}
	public String getContratoNrSequFrauCnal() {
		return contratoNrSequFrauCnal;
	}
	public void setContratoNrSequFrauCnal(String contratoNrSequFrauCnal) {
		this.contratoNrSequFrauCnal = contratoNrSequFrauCnal;
	}
	public String getContratoCdEntiCntr() {
		return contratoCdEntiCntr;
	}
	public void setContratoCdEntiCntr(String contratoCdEntiCntr) {
		this.contratoCdEntiCntr = contratoCdEntiCntr;
	}
	public String getContratoNmProdCorp() {
		return contratoNmProdCorp;
	}
	public void setContratoNmProdCorp(String contratoNmProdCorp) {
		this.contratoNmProdCorp = contratoNmProdCorp;
	}
	public String getContratoNrSequOrdeServ() {
		return contratoNrSequOrdeServ;
	}
	public void setContratoNrSequOrdeServ(String contratoNrSequOrdeServ) {
		this.contratoNrSequOrdeServ = contratoNrSequOrdeServ;
	}
	public String getContratoCdProd() {
		return contratoCdProd;
	}
	public void setContratoCdProd(String contratoCdProd) {
		this.contratoCdProd = contratoCdProd;
	}
	public String getContratoTpOperParp() {
		return contratoTpOperParp;
	}
	public void setContratoTpOperParp(String contratoTpOperParp) {
		this.contratoTpOperParp = contratoTpOperParp;
	}
	public String getContratoQtdLanc() {
		return contratoQtdLanc;
	}
	public void setContratoQtdLanc(String contratoQtdLanc) {
		this.contratoQtdLanc = contratoQtdLanc;
	}
}
