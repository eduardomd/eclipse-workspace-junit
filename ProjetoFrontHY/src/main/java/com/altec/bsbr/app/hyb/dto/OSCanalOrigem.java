package com.altec.bsbr.app.hyb.dto;

public class OSCanalOrigem {
	private String strErro;
	private int CD_CNAL_ORIG;
	private String CODIGO;
	private String TP_OFCI;
	private String TP_MONI_LYNX;
	private String IN_MINSTR;
	private boolean IN_OMINTE;
	private boolean IN_OMINTR;
	private boolean IN_OMINSTR;
	private boolean IN_OMTELE;
	private boolean IN_OMMALT;
	private boolean IN_OMDENC;
	private boolean IN_OMDEPT;
	private boolean IN_OMAGEN;
	private String NOME;
	
	public OSCanalOrigem(){}

	public String getStrErro() {
		return strErro;
	}

	public void setStrErro(String strErro) {
		this.strErro = strErro;
	}

	public int getCD_CNAL_ORIG() {
		return CD_CNAL_ORIG;
	}

	public void setCD_CNAL_ORIG(int cD_CNAL_ORIG) {
		CD_CNAL_ORIG = cD_CNAL_ORIG;
	}

	public String getCODIGO() {
		return CODIGO;
	}

	public void setCODIGO(String cODIGO) {
		CODIGO = cODIGO;
	}

	public String getTP_OFCI() {
		return TP_OFCI;
	}

	public void setTP_OFCI(String tP_OFCI) {
		TP_OFCI = tP_OFCI;
	}

	public String getTP_MONI_LYNX() {
		return TP_MONI_LYNX;
	}

	public void setTP_MONI_LYNX(String tP_MONI_LYNX) {
		TP_MONI_LYNX = tP_MONI_LYNX;
	}

	public String getIN_MINSTR() {
		return IN_MINSTR;
	}

	public void setIN_MINSTR(String iN_MINSTR) {
		IN_MINSTR = iN_MINSTR;
	}

	public boolean isIN_OMINTE() {
		return IN_OMINTE;
	}

	public void setIN_OMINTE(boolean iN_OMINTE) {
		IN_OMINTE = iN_OMINTE;
	}

	public boolean isIN_OMINTR() {
		return IN_OMINTR;
	}

	public void setIN_OMINTR(boolean iN_OMINTR) {
		IN_OMINTR = iN_OMINTR;
	}

	public boolean isIN_OMINSTR() {
		return IN_OMINSTR;
	}

	public void setIN_OMINSTR(boolean iN_OMINSTR) {
		IN_OMINSTR = iN_OMINSTR;
	}

	public boolean isIN_OMTELE() {
		return IN_OMTELE;
	}

	public void setIN_OMTELE(boolean iN_OMTELE) {
		IN_OMTELE = iN_OMTELE;
	}

	public boolean isIN_OMMALT() {
		return IN_OMMALT;
	}

	public void setIN_OMMALT(boolean iN_OMMALT) {
		IN_OMMALT = iN_OMMALT;
	}

	public boolean isIN_OMDENC() {
		return IN_OMDENC;
	}

	public void setIN_OMDENC(boolean iN_OMDENC) {
		IN_OMDENC = iN_OMDENC;
	}

	public boolean isIN_OMDEPT() {
		return IN_OMDEPT;
	}

	public void setIN_OMDEPT(boolean iN_OMDEPT) {
		IN_OMDEPT = iN_OMDEPT;
	}

	public boolean isIN_OMAGEN() {
		return IN_OMAGEN;
	}

	public void setIN_OMAGEN(boolean iN_OMAGEN) {
		IN_OMAGEN = iN_OMAGEN;
	}

	public String getNOME() {
		return NOME;
	}

	public void setNOME(String nOME) {
		NOME = nOME;
	}
	
	
}
