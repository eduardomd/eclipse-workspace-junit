package com.altec.bsbr.app.hyb.web.jsf;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.altec.bsbr.app.hyb.dto.RsNotiAnexo;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmNotificacao.WebServiceException;
import com.altec.bsbr.app.jab.hyb.webclient.XHYAdmNotificacao.XHYAdmNotificacaoEndPoint;
import com.altec.bsbr.fw.web.jsf.BasicBBean; 

@Component("AdmWorkflowNotificacaoReenviaIframeBean")
@Scope("request")
public class AdmWorkflowNotificacaoReenviaIframeBean extends BasicBBean {
    
	final Boolean MOCK = false;
	
	private static final long serialVersionUID = 1L;
	private List<RsNotiAnexo> objRsNotiAnexo;
	private Object strNrSeqNoti = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("strNrSeqNoti");
	
	@Autowired
	private XHYAdmNotificacaoEndPoint admnoti;
	
	public List<RsNotiAnexo> getObjRsNotiAnexo() {
		return objRsNotiAnexo;
	}

	public String mockRandomExtension() {
		String[] extensions = new String[]{"pdf", "zip", "doc", "xls", "ppt", "pps", "msg", "jpgjpeggifbmppng"}; 
		Integer randomNum = ThreadLocalRandom.current().nextInt(1, extensions.length);
		return extensions[randomNum];
	}
		
	@PostConstruct
	public void init() {
		objRsNotiAnexo = new ArrayList<RsNotiAnexo>();
		
		/* Mock do backend */
		/* if feito caso testem a pagina sem a querystring, para facilitar o teste no browser*/
//		if (this.MOCK) 
//			for(int i = 1; i <= (strNrSeqNoti == null ? 5 : Integer.parseInt(strNrSeqNoti.toString())); i++) {
//				RsNotiAnexo notianexo = new RsNotiAnexo();
//				notianexo.setNM_CAMI_ARQU("C:\\path\\para\\arquivo\\teste"+i+"."+mockRandomExtension());
//				objRsNotiAnexo.add(notianexo);
//			}
		
		try {
			String retorno = admnoti.consultarAnexo(Long.valueOf(strNrSeqNoti.toString()));
//            System.out.println("ConsultarAnexo retorno:  " + retorno);
            
            JSONObject jsonRetorno = new JSONObject(retorno);
            JSONArray pcursor = jsonRetorno.getJSONArray("PCURSOR");
            
            for(int i = 0; i < pcursor.length(); i++) {
            	JSONObject curr = pcursor.getJSONObject(i);
            	RsNotiAnexo rsnoti = new RsNotiAnexo();
            	rsnoti.setNM_CAMI_ARQU(curr.getString("NM_CAMI_ARQU"));
            	objRsNotiAnexo.add(rsnoti);
            }

	    } catch (WebServiceException e) {
	    	// TODO Auto-generated catch block
	    	e.printStackTrace();
        }
	}

}