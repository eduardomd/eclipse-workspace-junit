package com.altec.bsbr.app.hyb.web.util;
/*
[ ***************************************************************
[ Avanade - Equipe de Desenvolvimento
[ ***************************************************************
[ Desenvolvedor.....: Priscilla Crisafulli
[ Descricao.........: Util para ser utilizado em p�ginas
[ Obs: Imigra��o de tecnologia antiga para java
[ Dt Inicio.........: 14/12/2018
[ ***************************************************************
[ Alteracao.........:
[ Descricao.........:
[ Dt Alteracao......:
[ ***************************************************************
*/

import java.io.IOException;
import java.io.StringReader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.dom4j.Document;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.xml.sax.InputSource;

import com.altec.bsbr.app.jab.hyb.webclient.XHYTransacao.XHYTransacaoEndPoint;


public class Util {
	
	//Exibe Mensagem
	public static void fgMessageBox (String strMensagem) {
		
	    strMensagem = strMensagem.replaceAll("\n", "\t");
	    strMensagem = strMensagem.replaceAll("\r","\n");
	    strMensagem = strMensagem.replaceAll("'", "");
	    System.out.println(strMensagem);
	    RequestContext.getCurrentInstance().execute("alert('" + strMensagem.toString() + "');");
	}
	public static void verificaErro(String strErro)throws IOException {

		if (!strErro.isEmpty()) {
			ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
			ec.redirect("hy_erro.xhtml?&strErro=" + strErro);
		}

	}
	
	public static String formataCPF(String strCPF) {
	 return formatacaoString("###.###.###-##",strCPF);
	}

	public static String formataCNPJ(String strCNPJ) {
		strCNPJ = strCNPJ.substring(strCNPJ.length()-13);
		return formatacaoString("##.###.###/####-##",strCNPJ);

	}
	public static String wait(int intSec) throws InterruptedException {
	    TimeUnit.SECONDS.sleep(intSec);
		return "";
	}
	public static String formataCEP(String cep) {
		if(cep != "") {
			cep = "00000000" + cep;
			cep = cep.substring(cep.length()-8);
			return formatacaoString("#####-###",cep);
		}
		return "";
	}

	public static String formataTelefone(String strTel) {
	  if(strTel != null){
		  return "("+ strTel.substring(0, 4) + ")" + strTel.substring(4);
	  }
	  return "";
	}

	//Parametros: String de data e mascara que se encontra a string(EX: "dd/MM/yyyy")
	public static String formataData(String strData,String mascara) {
	  if(strData != null) {
		  DateTimeFormatter formatter = DateTimeFormatter.ofPattern(mascara);
		  LocalDate localdate = LocalDate.parse(strData,formatter);
		  String day = String.valueOf(localdate.getDayOfMonth());
		  String month = String.valueOf(localdate.getMonthValue());
		  String year = String.valueOf(localdate.getYear());
		  if(day.length() == 1) {
			  day = "0" + day;
		  }
		  if(month.length() == 1) {
			  month = "0" + month;
		  }
		  return day + "/" + month + "/" + year;
		  
	  }
	  return "";
	}
	
	public static String dataToStr(String strData) {
	  if(strData != null) {
		  return strData.substring(6,10) + strData.substring(3, 5) + strData.substring(0, 2);
	  }	
		return "";
	}

	//Essa fun��o n�o precisa ser convertida j� que existe Operador Condicional Ternario
	/*public String IIf(String condition,String value1, String value2) throws ScriptException {
		
		return "";
	}*/
	
	public static String getNameSegmento(String strNrSegmentacao) throws Exception {

	Document objDOM;
	List<Node> objNodeList;
	ClsTransacao objSegmento = new ClsTransacao();
	Node objNode;
	String strXML = "";
	String msgErro = "";
	String strNameSegmento = "";

	strXML = objSegmento.JCGG(strNrSegmentacao, msgErro);

	if(msgErro.replaceAll("\\s+","") != "") {
		return "";
	}

	SAXReader reader = new SAXReader();
	objDOM = reader.read(new StringReader(strXML));
	
	if(objDOM != null ) {
	    objNodeList = objDOM.selectNodes("//iColl[@id='listDataJCGG']/kColl[field[@id='DC_FORMATO'][@value='TCMGEN1']]");
		
		if(objNodeList.size() > 0) {
			for(Node node :objNodeList) {
				strNameSegmento = XmlUtil.getText(node, "DATOS1");
				strNameSegmento = strNameSegmento.substring(0, strNameSegmento.length()-34);
				return strNameSegmento;
			} 
		} else {
			return "";
		}
	} else {
		return "";
	}
	return "";

	}

	//Fun��o VBSCRIPT n�o utilizada em nenhum arquivo
	/*public String formataNumero(String strValor, int intDecimais) {
		//Aparentemente nenhum lugar est� usando essa fun��o
		if (strValor == null || strValor == "") {
			//exit function
		}
		
	 return"";// FormatNumber(strValor, intDecimais);

	}*/

	public static String formataRenavam(String strRenavan) {

		String strAux;
		strAux = strRenavan;
		
		if(strRenavan.replaceAll("\\s+","").length() > 0) {
			strAux = "000000000000" + strAux;
			strAux = strAux.substring(strAux.length()-12);
		}
		
		return strAux;
	
	}
	
	
	@SuppressWarnings("unchecked")
	public static String getCdSegmentacao(String strPeNumPe) throws Exception {
    
		Document objDOM = null;
		ClsTransacao objTransacao = new ClsTransacao(); 
		String strErro = "";
		String strXML = "";
		Node objNode;
		List<Node>objNodeList;
		String strNrSegmentacao = "";
		strXML = objTransacao.PE95(strPeNumPe, strErro);
		
		
		 try {
			    SAXReader reader=new SAXReader();
			    objDOM = reader.read(new StringReader(strXML));
		}
		 catch (  Exception e) {
			    e.printStackTrace();
		}
		
		if(objDOM != null ) {
			System.out.println("OLA");
			System.out.println(objDOM.selectNodes("/Employees/Employee"));
		    objNodeList = objDOM.selectNodes("//iColl[@id='listDataPE95']/kColl[field[@id='DC_FORMATO'][@value='PEM931C']][field[@id='CLASEG'][@value='PRI']]");
		    if(objNodeList.size() > 0 ) {
				for(Node node : objNodeList) {
		            strNrSegmentacao = XmlUtil.getText(node, "SEGCLA");
				} 
		    }
		    System.out.println(strNrSegmentacao);  
		    return strNrSegmentacao.replaceAll("\\s+","");
		            
		} else {
		    return "";
		}

	}

	
	
//	public String getPENUMPE(String strCpfCnpj, String strTipo, String strNomePart) throws Exception {
//		String xmlPeNumPe = "";
//
//		System.out.println(strCpfCnpj + ", " + strTipo + ", " +  strNomePart);
//		
//		xmlPeNumPe = transacaoEndPoint.pel1("", strCpfCnpj, strTipo, "");
//		
//		
//		/*ClsTransacao objTransacao = new ClsTransacao(); 
//		String strErro="";
//		Document objDOM;
//		List<Node> objNodeList;
//		Node objNode;
//		String xmlNome = "";
//		String strXML = "";
//	
//		strXML = objTransacao.PEL1("", strCpfCnpj, strTipo, "", strErro);
//	
//		if(strErro.replaceAll("\\s+","") != "") {
//			return "Ocorreu um erro na consulta a transa��o PEL1: " + strErro;
//		}
//		
//		SAXReader reader = new SAXReader();
//		objDOM = reader.read(new StringReader(strXML));
//		
//		if(objDOM != null ) {
//			objNodeList = objDOM.selectNodes("//iColl[@id='listDataPEL1']/kColl[field[@id='DC_FORMATO'][@value='PEM0004']]");
//		} else {
//			return "Ocorreu um erro na leitura do retorno da transa��o PEL1";
//		}
//	
//		if(objNodeList.size() > 0 ) {
//			for(Node node : objNodeList) {
//				xmlNome = XmlUtil.getText(node, "PENOMPE") + XmlUtil.getText(node, "PEPRIAP") +
//						XmlUtil.getText(node, "PESEGAP");
//		        xmlPeNumPe = XmlUtil.getText(node, "PENUMPE");
//				
//				if(strNomePart == xmlNome) {
//					break;
//				}
//			
//			} 
//		}*/
//		
//		
//		
//		
//		return xmlPeNumPe;
//
//	}
	
	
	private static String formatacaoString(String formato, String texto) {
		int j = 0;
		for(int i = 0; i < formato.length(); i++) {
			if(formato.charAt(i) == '#') {
				formato = formato.replaceFirst(String.valueOf(formato.charAt(i)), String.valueOf(texto.charAt(j)));
				j++;
			}
		}
		return formato;
	}
}
