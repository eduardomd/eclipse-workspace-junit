package com.altec.bsbr.app.hyb.dto;

public class LinhaNego1Model {
	
	private String codigoLinhaNego1;
	private String nomeLinhaNego1;
	
	public LinhaNego1Model(String codigoLinhaNego1, String nomeLinhaNego1) {
		this.codigoLinhaNego1 = codigoLinhaNego1;
		this.nomeLinhaNego1 = nomeLinhaNego1; 
	}

	public String getCodigoLinhaNego1() {
		return codigoLinhaNego1;
	}

	public void setCodigoLinhaNego1(String codigoLinhaNego1) {
		this.codigoLinhaNego1 = codigoLinhaNego1;
	}

	public String getNomeLinhaNego1() {
		return nomeLinhaNego1;
	}

	public void setNomeLinhaNego1(String nomeLinhaNego1) {
		this.nomeLinhaNego1 = nomeLinhaNego1;
	}
	
	
	
}
