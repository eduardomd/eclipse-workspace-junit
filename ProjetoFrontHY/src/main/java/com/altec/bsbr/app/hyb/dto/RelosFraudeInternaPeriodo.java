package com.altec.bsbr.app.hyb.dto;

public class RelosFraudeInternaPeriodo {
	
	private String ROWNUM;
	private String NM_EVEN;
	private String QNT_AGEN;
	private String VL_ENVO_AGEN;
	private String VL_RECR_AGEN;
	private String VL_PERD_EFET_AGEN;
	private String QNT_ADMC;
	private String VL_ENVO_ADMC;
	private String VL_RECR_ADMC;
	private String VL_PERD_EFET_ADMC;
	private String QNTD_TOTAL;
	private String VL_ENVO_TOTAL;
	private String VL_RECR_TOTAL;
	private String VL_PERD_EFET_TOTAL;
	
	private String VL_ENVO_AGEN_FORMATADO;
	private String VL_RECR_AGEN_FORMATADO;
	private String VL_PERD_EFET_AGEN_FORMATADO;
	private String VL_ENVO_ADMC_FORMATADO;
	private String VL_RECR_ADMC_FORMATADO;
	private String VL_PERD_EFET_ADMC_FORMATADO;
	private String VL_ENVO_TOTAL_FORMATADO;
	private String VL_RECR_TOTAL_FORMATADO;
	private String VL_PERD_EFET_TOTAL_FORMATADO;
	
	public RelosFraudeInternaPeriodo(
			String ROWNUM,
			String NM_EVEN,
			String QNT_AGEN,
			String VL_ENVO_AGEN,
			String VL_RECR_AGEN,
			String VL_PERD_EFET_AGEN,
			String QNT_ADMC,
			String VL_ENVO_ADMC,
			String VL_RECR_ADMC,
			String VL_PERD_EFET_ADMC,
			String QNTD_TOTAL,
			String VL_ENVO_TOTAL,
			String VL_RECR_TOTAL,
			String VL_PERD_EFET_TOTAL,
			String VL_ENVO_AGEN_FORMATADO,
			String VL_RECR_AGEN_FORMATADO,
			String VL_PERD_EFET_AGEN_FORMATADO,
			String VL_ENVO_ADMC_FORMATADO,
			String VL_RECR_ADMC_FORMATADO,
			String VL_PERD_EFET_ADMC_FORMATADO ,
			String VL_ENVO_TOTAL_FORMATADO,
			String VL_RECR_TOTAL_FORMATADO,
			String VL_PERD_EFET_TOTAL_FORMATADO){
		this.ROWNUM = ROWNUM;
		this.NM_EVEN = NM_EVEN;
		this.QNT_AGEN = QNT_AGEN;
		this.VL_ENVO_AGEN = VL_ENVO_AGEN;
		this.VL_RECR_AGEN = VL_RECR_AGEN;
		this.VL_PERD_EFET_AGEN = VL_PERD_EFET_AGEN;
		this.QNT_ADMC = QNT_ADMC;
		this.VL_ENVO_ADMC = VL_ENVO_ADMC;
		this.VL_RECR_ADMC = VL_RECR_ADMC;
		this.VL_PERD_EFET_ADMC = VL_PERD_EFET_ADMC;
		this.QNTD_TOTAL = QNTD_TOTAL;
		this.VL_ENVO_TOTAL = VL_ENVO_TOTAL;
		this.VL_RECR_TOTAL = VL_RECR_TOTAL;
		this.VL_PERD_EFET_TOTAL = VL_PERD_EFET_TOTAL;
		this.setVL_ENVO_AGEN_FORMATADO(VL_ENVO_AGEN_FORMATADO);
		this.setVL_RECR_AGEN_FORMATADO(VL_RECR_AGEN_FORMATADO);
		this.setVL_PERD_EFET_AGEN_FORMATADO(VL_PERD_EFET_AGEN_FORMATADO);
		this.setVL_ENVO_ADMC_FORMATADO(VL_ENVO_ADMC_FORMATADO);
		this.setVL_RECR_ADMC_FORMATADO(VL_RECR_ADMC_FORMATADO);
		this.setVL_PERD_EFET_ADMC_FORMATADO(VL_PERD_EFET_ADMC_FORMATADO);
		this.setVL_ENVO_TOTAL_FORMATADO(VL_ENVO_TOTAL_FORMATADO);
		this.setVL_RECR_TOTAL_FORMATADO(VL_RECR_TOTAL_FORMATADO);
		this.setVL_PERD_EFET_TOTAL_FORMATADO(VL_PERD_EFET_TOTAL_FORMATADO);
	}
	
	public String getROWNUM() {
		return ROWNUM;
	}
	public void setROWNUM(String rOWNUM) {
		ROWNUM = rOWNUM;
	}
	public String getNM_EVEN() {
		return NM_EVEN;
	}
	public void setNM_EVEN(String nM_EVEN) {
		NM_EVEN = nM_EVEN;
	}
	public String getQNT_AGEN() {
		return QNT_AGEN;
	}
	public void setQNT_AGEN(String qNT_AGEN) {
		QNT_AGEN = qNT_AGEN;
	}
	public String getVL_ENVO_AGEN() {
		return VL_ENVO_AGEN;
	}
	public void setVL_ENVO_AGEN(String vL_ENVO_AGEN) {
		VL_ENVO_AGEN = vL_ENVO_AGEN;
	}
	public String getVL_RECR_AGEN() {
		return VL_RECR_AGEN;
	}
	public void setVL_RECR_AGEN(String vL_RECR_AGEN) {
		VL_RECR_AGEN = vL_RECR_AGEN;
	}
	public String getVL_PERD_EFET_AGEN() {
		return VL_PERD_EFET_AGEN;
	}
	public void setVL_PERD_EFET_AGEN(String vL_PERD_EFET_AGEN) {
		VL_PERD_EFET_AGEN = vL_PERD_EFET_AGEN;
	}
	public String getQNT_ADMC() {
		return QNT_ADMC;
	}
	public void setQNT_ADMC(String qNT_ADMC) {
		QNT_ADMC = qNT_ADMC;
	}
	public String getVL_ENVO_ADMC() {
		return VL_ENVO_ADMC;
	}
	public void setVL_ENVO_ADMC(String vL_ENVO_ADMC) {
		VL_ENVO_ADMC = vL_ENVO_ADMC;
	}
	public String getVL_RECR_ADMC() {
		return VL_RECR_ADMC;
	}
	public void setVL_RECR_ADMC(String vL_RECR_ADMC) {
		VL_RECR_ADMC = vL_RECR_ADMC;
	}
	public String getVL_PERD_EFET_ADMC() {
		return VL_PERD_EFET_ADMC;
	}
	public void setVL_PERD_EFET_ADMC(String vL_PERD_EFET_ADMC) {
		VL_PERD_EFET_ADMC = vL_PERD_EFET_ADMC;
	}
	public String getQNTD_TOTAL() {
		return QNTD_TOTAL;
	}
	public void setQNTD_TOTAL(String qNTD_TOTAL) {
		QNTD_TOTAL = qNTD_TOTAL;
	}
	public String getVL_ENVO_TOTAL() {
		return VL_ENVO_TOTAL;
	}
	public void setVL_ENVO_TOTAL(String vL_ENVO_TOTAL) {
		VL_ENVO_TOTAL = vL_ENVO_TOTAL;
	}
	public String getVL_RECR_TOTAL() {
		return VL_RECR_TOTAL;
	}
	public void setVL_RECR_TOTAL(String vL_RECR_TOTAL) {
		VL_RECR_TOTAL = vL_RECR_TOTAL;
	}
	public String getVL_PERD_EFET_TOTAL() {
		return VL_PERD_EFET_TOTAL;
	}
	public void setVL_PERD_EFET_TOTAL(String vL_PERD_EFET_TOTAL) {
		VL_PERD_EFET_TOTAL = vL_PERD_EFET_TOTAL;
	}

	public String getVL_ENVO_AGEN_FORMATADO() {
		return VL_ENVO_AGEN_FORMATADO;
	}

	public void setVL_ENVO_AGEN_FORMATADO(String vL_ENVO_AGEN_FORMATADO) {
		VL_ENVO_AGEN_FORMATADO = vL_ENVO_AGEN_FORMATADO;
	}

	public String getVL_RECR_AGEN_FORMATADO() {
		return VL_RECR_AGEN_FORMATADO;
	}

	public void setVL_RECR_AGEN_FORMATADO(String vL_RECR_AGEN_FORMATADO) {
		VL_RECR_AGEN_FORMATADO = vL_RECR_AGEN_FORMATADO;
	}

	public String getVL_PERD_EFET_AGEN_FORMATADO() {
		return VL_PERD_EFET_AGEN_FORMATADO;
	}

	public void setVL_PERD_EFET_AGEN_FORMATADO(String vL_PERD_EFET_AGEN_FORMATADO) {
		VL_PERD_EFET_AGEN_FORMATADO = vL_PERD_EFET_AGEN_FORMATADO;
	}

	public String getVL_ENVO_ADMC_FORMATADO() {
		return VL_ENVO_ADMC_FORMATADO;
	}

	public void setVL_ENVO_ADMC_FORMATADO(String vL_ENVO_ADMC_FORMATADO) {
		VL_ENVO_ADMC_FORMATADO = vL_ENVO_ADMC_FORMATADO;
	}

	public String getVL_RECR_ADMC_FORMATADO() {
		return VL_RECR_ADMC_FORMATADO;
	}

	public void setVL_RECR_ADMC_FORMATADO(String vL_RECR_ADMC_FORMATADO) {
		VL_RECR_ADMC_FORMATADO = vL_RECR_ADMC_FORMATADO;
	}

	public String getVL_PERD_EFET_ADMC_FORMATADO() {
		return VL_PERD_EFET_ADMC_FORMATADO;
	}

	public void setVL_PERD_EFET_ADMC_FORMATADO(String vL_PERD_EFET_ADMC_FORMATADO) {
		VL_PERD_EFET_ADMC_FORMATADO = vL_PERD_EFET_ADMC_FORMATADO;
	}

	public String getVL_ENVO_TOTAL_FORMATADO() {
		return VL_ENVO_TOTAL_FORMATADO;
	}

	public void setVL_ENVO_TOTAL_FORMATADO(String vL_ENVO_TOTAL_FORMATADO) {
		VL_ENVO_TOTAL_FORMATADO = vL_ENVO_TOTAL_FORMATADO;
	}

	public String getVL_RECR_TOTAL_FORMATADO() {
		return VL_RECR_TOTAL_FORMATADO;
	}

	public void setVL_RECR_TOTAL_FORMATADO(String vL_RECR_TOTAL_FORMATADO) {
		VL_RECR_TOTAL_FORMATADO = vL_RECR_TOTAL_FORMATADO;
	}

	public String getVL_PERD_EFET_TOTAL_FORMATADO() {
		return VL_PERD_EFET_TOTAL_FORMATADO;
	}

	public void setVL_PERD_EFET_TOTAL_FORMATADO(String vL_PERD_EFET_TOTAL_FORMATADO) {
		VL_PERD_EFET_TOTAL_FORMATADO = vL_PERD_EFET_TOTAL_FORMATADO;
	}

}
