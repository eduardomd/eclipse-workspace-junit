package com.altec.bsbr.app.hyb.dto;

public class RelosEvento {
	
	private String NR_SEQU_ORDE_SERV;
	private String CNAL_ORIG;
	private String STATUS;
	private String NM_PARP;
	private String TP_PARP;
	private String EVENTO;
	private String CANAL;
	private String TX_OUTR_PRPC;
	private String DT_PREV_ENCE_ORDE_SERV;
	private String CD_AREA;

	public RelosEvento() {
	}
	
	public RelosEvento(
			String NR_SEQU_ORDE_SERV,
			String CNAL_ORIG,
			String STATUS,
			String NM_PARP,
			String TP_PARP,
			String EVENTO,
			String CANAL,
			String TX_OUTR_PRPC,
			String DT_PREV_ENCE_ORDE_SERV,
			String CD_AREA) {
		this.NR_SEQU_ORDE_SERV = NR_SEQU_ORDE_SERV;
		this.CNAL_ORIG = CNAL_ORIG;
		this.STATUS = STATUS;
		this.NM_PARP = NM_PARP;
		this.TP_PARP = TP_PARP;
		this.EVENTO = EVENTO;
		this.CANAL = CANAL;
		this.TX_OUTR_PRPC = TX_OUTR_PRPC;
		this.DT_PREV_ENCE_ORDE_SERV = DT_PREV_ENCE_ORDE_SERV;
		this.CD_AREA = CD_AREA;
		
	}

	public String getNR_SEQU_ORDE_SERV() {
		return NR_SEQU_ORDE_SERV;
	}

	public void setNR_SEQU_ORDE_SERV(String nR_SEQU_ORDE_SERV) {
		NR_SEQU_ORDE_SERV = nR_SEQU_ORDE_SERV;
	}

	public String getCNAL_ORIG() {
		return CNAL_ORIG;
	}

	public void setCNAL_ORIG(String cNAL_ORIG) {
		CNAL_ORIG = cNAL_ORIG;
	}

	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}

	public String getNM_PARP() {
		return NM_PARP;
	}

	public void setNM_PARP(String nM_PARP) {
		NM_PARP = nM_PARP;
	}

	public String getTP_PARP() {
		return TP_PARP;
	}

	public void setTP_PARP(String tP_PARP) {
		TP_PARP = tP_PARP;
	}

	public String getEVENTO() {
		return EVENTO;
	}

	public void setEVENTO(String eVENTO) {
		EVENTO = eVENTO;
	}

	public String getCANAL() {
		return CANAL;
	}

	public void setCANAL(String cANAL) {
		CANAL = cANAL;
	}

	public String getTX_OUTR_PRPC() {
		return TX_OUTR_PRPC;
	}

	public void setTX_OUTR_PRPC(String tX_OUTR_PRPC) {
		TX_OUTR_PRPC = tX_OUTR_PRPC;
	}

	public String getDT_PREV_ENCE_ORDE_SERV() {
		return DT_PREV_ENCE_ORDE_SERV;
	}

	public void setDT_PREV_ENCE_ORDE_SERV(String dT_PREV_ENCE_ORDE_SERV) {
		DT_PREV_ENCE_ORDE_SERV = dT_PREV_ENCE_ORDE_SERV;
	}

	public String getCD_AREA() {
		return CD_AREA;
	}

	public void setCD_AREA(String cD_AREA) {
		CD_AREA = cD_AREA;
	}

}
