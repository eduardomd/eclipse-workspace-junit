package com.altec.bsbr.app.hyb.dto;

public class ObjRsRelCapaOs {
	private String dataAtual;
	private String horaAtual;
	private String dtAbertura;
	private String txAbertura;  
	private String txEncerramento;
	
	public ObjRsRelCapaOs() {
		
	}
	
	public ObjRsRelCapaOs(String dataAtual, String horaAtual, String dtAbertura, String txAbertura,
			String txEncerramento) {
		this.dataAtual = dataAtual;
		this.horaAtual = horaAtual;
		this.dtAbertura = dtAbertura;
		this.txAbertura = txAbertura;
		this.txEncerramento = txEncerramento;
	}
	public String getDataAtual() {
		return dataAtual;
	}
	public void setDataAtual(String dataAtual) {
		this.dataAtual = dataAtual;
	}
	public String getHoraAtual() {
		return horaAtual;
	}
	public void setHoraAtual(String horaAtual) {
		this.horaAtual = horaAtual;
	}
	public String getDtAbertura() {
		return dtAbertura;
	}
	public void setDtAbertura(String dtAbertura) {
		this.dtAbertura = dtAbertura;
	}
	public String getTxAbertura() {
		return txAbertura;
	}
	public void setTxAbertura(String txAbertura) {
		this.txAbertura = txAbertura;
	}
	public String getTxEncerramento() {
		return txEncerramento;
	}
	public void setTxEncerramento(String txEncerramento) {
		this.txEncerramento = txEncerramento;
	}
	
}
