// JScript source code
function HideShowTD(idObjTD1, idObjTD2, idObjCbo)
{
    var td1;
    var td2;
    var cbo1;
    
    td1    = document.getElementById(idObjTD1);
    td2    = document.getElementById(idObjTD2);
    cbo1   = document.getElementById(idObjCbo);
    
    if (cbo1.value == 1)
    {
        td1.style.display = "block";
        td2.style.display = "none";
    }
    else
    {
        td1.style.display = "none";
        td2.style.display = "block";
    }
}

function HideShowTR(idCboOrigem)
{
    var cbo = document.getElementById(idCboOrigem)

    if(cbo.value == 9)
    {        
        trOficio.style.display = "block";
        trLynx.style.display = "none";
        trMonitInst.style.display = "none";
        trOutrosMeios.style.display = "none";
    }
    else if(cbo.value == 6)
    {        
        trOficio.style.display = "none";
        trLynx.style.display = "block";
        trMonitInst.style.display = "none";
        trOutrosMeios.style.display = "none";
    }
    else if(cbo.value == 7)
    {        
        trOficio.style.display = "none";
        trLynx.style.display = "none";
        trMonitInst.style.display = "block";
        trOutrosMeios.style.display = "none";
    }
    else if(cbo.value == 10)
    {        
        trOficio.style.display = "none";
        trLynx.style.display = "none";
        trMonitInst.style.display = "none";
        trOutrosMeios.style.display = "block";
    }
    else
    {        
        trOficio.style.display = "none";
        trLynx.style.display = "none";
        trMonitInst.style.display = "none";
        trOutrosMeios.style.display = "none";
    } 
}


function ExibirObj(strIds) 
{		
    var arrIds = strIds.split(';');
	
    for(var i = 0; i < arrIds.length; i++) 
    {	
	    d = document.getElementById(arrIds[i]); 
	
	    if(d.style.display == 'none')
		    d.style.display = '';
	    else
			d.style.display = 'none';				
    }
}


function HideShowElement(strElementId, strImgId)
{
    var objElement = document.getElementById(strElementId);
    var objImg = document.getElementById(strImgId);
 
    if (objElement)
    { 
        if((objElement.style.display == "none"))
        {
            objElement.style.display = "block";            
                       
            if(objImg)
            {
                objImg.src = "../imagens/btn_menos.gif";
                objImg.alt = "Ocultar";
                
            }            
        }
        else
        {
            objElement.style.display = "none";

            if(objImg)
            {
                objImg.src = "../imagens/btn_mais.gif";
                objImg.alt = "Exibir";                
                                    
            }
        }
    }
    
}

function MostraDivColaborador(strIdObj, strDivId1, strDivId2)
{
    var cbo = document.getElementById(strIdObj);
    var div1 = document.getElementById(strDivId1);
    var div2 = document.getElementById(strDivId2);
    
    if(cbo || div1 || div2)
    {
        if(cbo.value == 1)
        {            
            div1.style.display = 'block';
            div2.style.display = 'none';
        }
        else
        {
            div1.style.display = 'none';
            div2.style.display = 'block';
        }
    }
}

/***************************** Funcoes usadas na parte no novo layout   *********/

function AbreUrl(strUrl)
{
    window.open(strUrl, '_self', '');        
}

/******************************************************************************************/


function PesquisaEnvolvidos(entidade, NrSeqOs)
{
    window.showModalDialog('HY_CadastroOs_PesquisaEnvolv_IFrm.asp?entidade='+entidade+'&NrSeqOs='+NrSeqOs,null,'dialogHeight:300px;dialogWidth:650px;center: Yes; resizable:no; status: No; scroll:No;');
}

function PesquisaContaCD(produto)
{
    window.showModalDialog('HY_CadastroOS_PesquisaContaCD.asp?produto='+produto,null,'dialogHeight:300px;dialogWidth:500px;center: Yes; resizable:no; status: No;');
}

function MostraDetalhesCartaoDebito(titular, cartao, id, os)
{
    parent.rodape.location.href = 'Botoes.asp?strBotao=Detalhe';
    window.open('HY_CadastroOs_CDebito.asp?titular='+titular+'&cartao='+cartao+'&id='+id+'&os='+os , '_self', '');        
}

function MostraDetalhesCartaoCredito(titular, cartao, id, os)
{
    parent.rodape.location.href = 'Botoes.asp?strBotao=Detalhe';
    window.open('HY_CadastroOs_CCredito.asp?titular='+titular+'&cartao='+cartao+'&id='+id+'&os='+os , '_self', '');        
}

function MostraDetalhesIBanking(titular, conta, id, os)
{
    parent.rodape.location.href = 'Botoes.asp?strBotao=Detalhe';
    window.open('HY_CadastroOs_IBanking.asp?titular='+titular+'&conta='+conta+'&id='+id+'&os='+os , '_self', '');        
}

function MostraDetalhesSLinha(titular, conta, id, os)
{
    parent.rodape.location.href = 'Botoes.asp?strBotao=Detalhe';
    window.open('HY_CadastroOs_SLinha.asp?titular='+titular+'&conta='+conta+'&id='+id+'&os='+os , '_self', '');            
}

function MostraDetalhesFO(titular, conta, id, os)
{
    parent.rodape.location.href = 'Botoes.asp?strBotao=Detalhe';
    window.open('HY_CadastroOs_FO.asp?titular='+titular+'&conta='+conta+'&id='+id+'&os='+os , '_self', '');        
}

function MostraDetalheNotificacao()
{    
    parent.principal.location.href = 'HY_Adm_WorkFlow_DetalheNotific.asp';    
}

function MostraDetalhesEnvolvClie(nome, id, segmentacao, participacao, os)
{   
    parent.rodape.location.href = 'Botoes.asp?strBotao=Detalhe';
    window.open('HY_CadastroOs_DetalheEnvolvClie.asp?segmentacao='+segmentacao+'&nome='+nome+'&id='+id+'&participacao='+participacao+'&os='+os , '_self', '');        
}

function MostraDetalhesEnvolvFunc(matricula, nome, id, participacao, os)
{
    parent.rodape.location.href = 'Botoes.asp?strBotao=Detalhe';
    window.open('HY_CadastroOs_DetalheEnvolvFunc.asp?matricula='+matricula+'&nome='+nome+'&id='+id+'&participacao='+participacao+'&os='+os , '_self', '');    
}

function MostraDetalheRiscoPotencial(os)
{
    parent.rodape.location.href = 'Botoes.asp?strBotao=DetalheRO';
    window.open('HY_CadastroOS_DetalheRiscoPot.asp?os='+os, '_self', '');
}

function MostraDetalheRiscoPotencialAn(os, idProd, nomeProd)
{
   //parent.rodape.location.href = 'Botoes.asp?strBotao=';
    window.open('HY_CadastroOS_DetalheRiscoPot_An.asp?os='+os+'&idProd='+idProd+'&nomeProd='+nomeProd, '_self', '');
}

function MostraPesquisaProduto(titular, cartao, id, os)
{
    window.showModalDialog('HY_CadastroOS_PesquisaProduto.asp?titular='+titular+'&cartao='+cartao+'&id='+id+'&os='+os , null,'dialogHeight:283px;dialogWidth:550px; center: Yes; resizable:no; status: No;');
}


/**********************  Funcoes para os combos do Sub Topico EVENTOS (prototipo) *******************************************/

function MostraComboEventos1(cboId)
{
    var cbo = document.getElementById(cboId);
    
    if(cbo.value == 2)
    {
        OcultaTodos1();
        OcultaTodos2();
        //OcultaTodos3();
        //MostraDivEventos1('divAssaltos');
        
        
        if(document.getElementById('cboNivel2'))
        {   
            if(document.getElementById('cboNivel21'))
                document.getElementById('tdNivel21').style.display = 'none';
            
            OcultaTodos1();
            document.getElementById('tdNivel2').style.display = 'block';
        }
    }
    else if(cbo.value == 3)
    {
        OcultaTodos1();
        OcultaTodos2();             
        
        if(document.getElementById('cboNivel21'))
        {   
            OcultaTodos1();
            if(document.getElementById('cboNivel2'))
                document.getElementById('tdNivel2').style.display = 'none';
            
            document.getElementById('tdNivel21').style.display = 'block';
        }
        
        MostraDivEventos1('divMonitIn');
    }
    else if(cbo.value == 4)
    {
        OcultaTodos1();
        OcultaTodos2();
        //OcultaTodos3();
        MostraDivEventos1('divFraudes');
        MostraDivEventos1('divPrevOutros');
    }
    else if(cbo.value == 5)
    {
        OcultaTodos1();
        OcultaTodos2();
        //OcultaTodos3();
        MostraDivEventos1('divOpCredito');
    }
    else if(cbo.value == 6)
    {
        OcultaTodos1();
        OcultaTodos2();
        //OcultaTodos3();
        MostraDivEventos1('divPrevOutros');
        MostraDivEventos1('divOutros');
    }
    else if(cbo.value == 7)
    {
        OcultaTodos1();
        OcultaTodos2();
        //OcultaTodos3();
        MostraDivEventos1('divAssaltos');
    }
    else if(cbo.value == 8)
    {
        OcultaTodos1();
        OcultaTodos2();
        //OcultaTodos3();
        MostraDivEventos1('divComportamental');
    }
    else if(cbo.value == 0)
    {
        OcultaTodos1();
        OcultaTodos2();
        //OcultaTodos3();
    }
    
    if(cbo.value != 2 && cbo.value != 3)
    {
        if(document.getElementById('cboNivel2'))
            document.getElementById('tdNivel2').style.display = 'none';
            
        if(document.getElementById('cboNivel21'))
            document.getElementById('tdNivel21').style.display = 'none';
        
        if(document.getElementById('cboNivel3'))
            document.getElementById('tdNivel3').style.display = 'none';               
    }
    
}

function OcultaTodos1()
{        
    if(document.getElementById('divAssaltos')) 
        document.getElementById('divAssaltos').style.display = 'none';
        
    if(document.getElementById('divComportamental')) 
        document.getElementById('divComportamental').style.display = 'none';
        
    if(document.getElementById('divFraudes')) 
        document.getElementById('divFraudes').style.display = 'none';
        
    if(document.getElementById('divOpCredito')) 
        document.getElementById('divOpCredito').style.display = 'none';
        
    if(document.getElementById('divOutros')) 
        document.getElementById('divOutros').style.display = 'none';
        
    if(document.getElementById('divTreinamento')) 
        document.getElementById('divTreinamento').style.display = 'none';
    
    if(document.getElementById('divFalhas')) 
        document.getElementById('divFalhas').style.display = 'none';    
    
    if(document.getElementById('divFE')) 
        document.getElementById('divFE').style.display = 'none';
                    
    if(document.getElementById('divPrevMonit')) 
        document.getElementById('divPrevMonit').style.display = 'none';
        
    if(document.getElementById('divMonitIn')) 
        document.getElementById('divMonitIn').style.display = 'none';
        
    if(document.getElementById('divPrevOutros')) 
        document.getElementById('divPrevOutros').style.display = 'none';
}

function MostraDivEventos1(div)
{
    if(document.getElementById(div))
    {
        if(document.getElementById(div).style.display == 'none')
        {        
            OcultaTodos1();                    
            document.getElementById(div).style.display = 'block';
        }
    }
}


function MostraComboEventos2(cboId)
{
    var cbo = document.getElementById(cboId);
    
    if(cbo.value == 1)
    {
        OcultaTodos2();
        //OcultaTodos3();
        MostraDivEventos2('divFECartaoDebito');
        //MostraDivEventos2('divMonitIn');
        MostraDivEventos2('divMonitExCDebito');        
    }
    else if(cbo.value == 2)
    {
        OcultaTodos2();
        //OcultaTodos3();
        MostraDivEventos2('divFECartaoCredito');
        MostraDivEventos2('divMonitExCCredito');
                
    }
    else if(cbo.value == 3)
    {
        OcultaTodos2();
        //OcultaTodos3();
        MostraDivEventos2('divFEInternetBanking');
        //MostraDivEventos3('divMonitExIBanking');            
    }
    else if(cbo.value == 4)
    {
        OcultaTodos2();
        //OcultaTodos3();
        MostraDivEventos2('divFESuperLinha');
        MostraDivEventos2('divMonitExSLinha');        
    }
    else if(cbo.value == 5)
    {
        OcultaTodos2();
        //OcultaTodos3();
        MostraDivEventos2('divFOContaBancaria');
        MostraDivEventos2('divMonitExCBancaria'); 
    }
    else if(cbo.value == 0)
    {
        OcultaTodos2();
        //OcultaTodos3();       
    }
    

}

function OcultaTodos2()
{
    if(document.getElementById('divFECartaoDebito')) 
        document.getElementById('divFECartaoDebito').style.display = 'none';
        
    if(document.getElementById('divFECartaoCredito')) 
        document.getElementById('divFECartaoCredito').style.display = 'none';
        
    if(document.getElementById('divFEInternetBanking')) 
        document.getElementById('divFEInternetBanking').style.display = 'none';
        
    if(document.getElementById('divFESuperLinha')) 
        document.getElementById('divFESuperLinha').style.display = 'none';
    
    if(document.getElementById('divFOContaBancaria')) 
        document.getElementById('divFOContaBancaria').style.display = 'none';
    
    if(document.getElementById('divMonitExCBancaria')) 
        document.getElementById('divMonitExCBancaria').style.display = 'none';        
    
    if(document.getElementById('divMonitIn')) 
        document.getElementById('divMonitIn').style.display = 'none';
        
    if(document.getElementById('divMonitEx')) 
        document.getElementById('divMonitEx').style.display = 'none';
        
    if(document.getElementById('divMonitExCDebito')) 
        document.getElementById('divMonitExCDebito').style.display = 'none';
        
    if(document.getElementById('divMonitExCCredito')) 
        document.getElementById('divMonitExCCredito').style.display = 'none';
        
    if(document.getElementById('divMonitExIBanking')) 
        document.getElementById('divMonitExIBanking').style.display = 'none';
        
    if(document.getElementById('divMonitExSLinha')) 
        document.getElementById('divMonitExSLinha').style.display = 'none';
    
    if(document.getElementById('divMonitExCBancaria')) 
        document.getElementById('divMonitExCBancaria').style.display = 'none';      
}

function MostraDivEventos2(div)
{
    if(document.getElementById(div))
    {
        if(document.getElementById(div).style.display == 'none')
        {        
            OcultaTodos2();                    
            document.getElementById(div).style.display = 'block';
        }
    }
}


/**********************************************************************************************************/


function MostraDivEquipe(div, intQntRegEquipe)
{
    var intCont;
    
    if(document.getElementById(div).style.display == 'none')
    {        
        for(intCont = 1; intCont <= intQntRegEquipe; intCont++)
        {   
            document.getElementById('divEquipe' + intCont.toString()).style.display = 'none';            
        }
        
        document.getElementById(div).style.display = 'block';
    }
    else
    {
        document.getElementById(div).style.display = 'none';
    }
}

function MostraDivEndereco(strIdObj, strDivId1, strDivId2)
{
    var cbo = document.getElementById(strIdObj);
    var div1 = document.getElementById(strDivId1);
    var div2 = document.getElementById(strDivId2);
    
    if(cbo || div1 || div2)
    {
        if(cbo.value == 1)
        {            
            div1.style.display = 'block';
            div2.style.display = 'none';
        }
        else
        {
            div1.style.display = 'none';
            div2.style.display = 'block';
        }
    }    
}

function AlternaDiv(div1, div2)
{
    var d1 = document.getElementById(div1);
    var d2 = document.getElementById(div2);
    
    if(d1 || d2)
    {   
        if(d1.style.display == 'block')
        {
            d1.style.display = 'none';
            d2.style.display = 'none';    
        }
        else
        {
            d1.style.display = 'block';
            d2.style.display = 'none';
        }
    }   
}

function MostraDivFE(idDiv)
{
    var div1 = document.getElementById('divFE11');
    var div2 = document.getElementById('divFE22');
    var div3 = document.getElementById('divFE33');    
    
    if (document.getElementById(idDiv).style.display == 'block')
    {
        document.getElementById(idDiv).style.display = 'none';
        div1.style.display = 'none';
        div2.style.display = 'none';
        div3.style.display = 'none';
    }
    else
    {
        div1.style.display = 'none';
        div2.style.display = 'none';
        div3.style.display = 'none';
        document.getElementById(idDiv).style.display = 'block';
    }
}

function MostraDivMonitEx(idDiv)
{
    var div1 = document.getElementById('divMonitExCDebito');
    var div2 = document.getElementById('divMonitExIBanking');
    var div3 = document.getElementById('divMonitExSLinha');    
    
    if (document.getElementById(idDiv).style.display == 'block')
    {
        document.getElementById(idDiv).style.display = 'none';
        div1.style.display = 'none';
        div2.style.display = 'none';
        div3.style.display = 'none';
    }
    else
    {
        div1.style.display = 'none';
        div2.style.display = 'none';
        div3.style.display = 'none';
        document.getElementById(idDiv).style.display = 'block';
    }
}

function MostraDivPrev(div1, div2, div3)
{
    var d1 = document.getElementById(div1);
    var d2 = document.getElementById(div2);
    var d3 = document.getElementById(div3);
    
    if(d1 || d2 || d3)
    {   
        d1.style.display = 'none';
        d2.style.display = 'none';    
        d3.style.display = 'none';
        
        d1.style.display = 'block';
        
    }   
}

function MostraDivFragilizacao(cboId)
{
    var cbo = document.getElementById(cboId)
    
    if(cbo)
    {   
        document.getElementById('divFragilAg').style.display = 'none';
        document.getElementById('divFragilREB').style.display = 'none';
        document.getElementById('divFragil24h').style.display = 'none';
        document.getElementById('divFragilPOS').style.display = 'none';
    
        if(cbo.value==1)
        {
            document.getElementById('divFragilAg').style.display = 'block';
        }
        else if(cbo.value==4)
        {
            document.getElementById('divFragilREB').style.display = 'block';
        }
        else if(cbo.value==2)
        {
            document.getElementById('divFragil24h').style.display = 'block';
        }
        else if(cbo.value==3)
        {
            document.getElementById('divFragilPOS').style.display = 'block';
        }
    
    }

}

/***********************************************************************
Objetivo:   Funcao para imprimir os relatorios.

************************************************************************/
function MostraRelFraudes()
{
   window.showModalDialog('HY_ImprimirFraudes.asp',null,'dialogHeight:600px;dialogWidth:800px;dialogTop: 150px; dialogLeft: 250px; edge: Sunken; center: Yes; help: Yes; resizable:no; status: No;');
}

function MostraRelMovCntaCorrente()
{
   window.showModalDialog('HY_ImprimirMovCntaCorrente.asp',null,'dialogHeight:600px;dialogWidth:800px;dialogTop: 150px; dialogLeft: 250px; edge: Sunken; center: Yes; help: Yes; resizable:no; status: No;');
}

function MostraRelPenalidades()
{
   window.showModalDialog('HY_ImprimirPenalidades.asp',null,'dialogHeight:600px;dialogWidth:800px;dialogTop: 150px; dialogLeft: 250px; edge: Sunken; center: Yes; help: Yes; resizable:no; status: No;');
}

function MostraRelDescNormativos()
{
   window.showModalDialog('HY_ImprimirDescNormativos.asp',null,'dialogHeight:600px;dialogWidth:800px;dialogTop: 150px; dialogLeft: 250px; edge: Sunken; center: Yes; help: Yes; resizable:no; status: No;');
}

/***************************************************************************************/


function TravaForm()
{
    objForm = document.forms[0];
    var arrInput = new Array();
    var arrTextArea = new Array()
    var arrSelect = new Array();
    var arrImg = new Array();
    var i;
    
    if(!objForm)
    {
        alert('Erro javascript.\nFormul�rio inv�lido!');        
    }
    else
    {   
        arrInput    = objForm.getElementsByTagName('input');
        arrTextArea = objForm.getElementsByTagName('textarea');
        arrSelect   = objForm.getElementsByTagName('select');
        arrImg      = objForm.getElementsByTagName('img');
        
        for(i = 0; i < arrInput.length; i++)        
        {
            arrInput[i].disabled = true;
        }
        
        for(i = 0; i < arrTextArea.length; i++)        
        {
            arrTextArea[i].disabled = true;
        }
        
        for(i = 0; i < arrSelect.length; i++)        
        {
            arrSelect[i].disabled = true;
        }
        
         for(i = 0; i < arrImg.length; i++)        
        {
            arrImg[i].onclick = '';
            arrImg[i].style.cursor = '';
        }
    }
}